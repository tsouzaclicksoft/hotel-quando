class AdminPreview < ActionMailer::Preview
  AdminMailer.instance_methods(false).each do | method |
    begin
      define_method(method) do
        arguments_array = []
        AdminMailer.instance_method(method).parameters[0].each do | arg |
          next if arg == :req
          arguments_array << arg.to_s.classify.constantize.last
        end
        AdminMailer.send(method, *arguments_array)
      end
    rescue
    end
  end
end
