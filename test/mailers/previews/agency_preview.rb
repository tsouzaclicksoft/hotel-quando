class AgencyPreview < ActionMailer::Preview
  AgencyMailer.instance_methods(false).each do | method |
    begin
      define_method(method) do
        arguments_array = []
        AgencyMailer.instance_method(method).parameters[0].each do | arg |
          next if arg == :req
          arguments_array << arg.to_s.classify.constantize.last
        end
        AgencyMailer.send(method, *arguments_array)
      end
    rescue
    end
  end

  def send_booking_confirmation_notice
    AgencyMailer.send_booking_confirmation_notice(Company.last.bookings.last)
  end
end
