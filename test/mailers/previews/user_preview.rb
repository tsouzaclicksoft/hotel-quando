class UserPreview < ActionMailer::Preview
  # Using a little metaprograming to avoid defining all UserMailer methods here
  # note that the paramenters must match a model name (if it does not, just define the method
  # after the next block)
  UserMailer.instance_methods(false).each do | method |
    begin
      define_method(method) do
        arguments_array = []
        UserMailer.instance_method(method).parameters[0].each do | arg |
          next if arg == :req
          arguments_array << arg.to_s.classify.constantize.last
        end
        UserMailer.send(method, *arguments_array)
      end
    rescue
    end
  end

end
