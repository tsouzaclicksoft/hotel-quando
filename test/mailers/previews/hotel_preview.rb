class HotelPreview < ActionMailer::Preview

  HotelMailer.instance_methods(false).each do | method |
    begin
      define_method(method) do
        arguments_array = []
        HotelMailer.instance_method(method).parameters[0].each do | arg |
          next if arg == :req
          arguments_array << arg.to_s.classify.constantize.last
        end
        HotelMailer.send(method, *arguments_array)
      end
    rescue
    end
  end

  def send_login_and_password
    HotelMailer.send_login_and_password(Hotel.first)
  end

  def noshow_successfull_charged_notice
    HotelMailer.noshow_successfull_charged_notice(Booking.first)
  end

  def no_show_with_charge_error_notice
    HotelMailer.no_show_with_charge_error_notice(Booking.first)
  end


end
