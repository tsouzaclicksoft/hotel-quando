module Services
  module Base
    extend ActiveSupport::Concern

    included do
      include Virtus.model
      def self.call(*args)
        new(*args).call
      end
    end
  end
end
