# encoding: utf-8
class BookingCancellationSrv
  include PayPal::SDK::Core::Logging
  include Services::Base
  include Shared::CurrencyCurrentHelper
  class CouldNotCancellPaypalTransactionError < StandardError; end
  class CouldNotCancellMaxiPagoTransactionError < StandardError; end
  class CouldNotCancelOmnibeesBookingError < StandardError; end

  attribute :booking
  attribute :agency_cancelation
  attribute :admin_cancelation

  def call
    response = OpenStruct.new
    response.success = false
    @payment_cancellation_response = ''
    @payment = booking.payments.last
    begin
      cancel_booking
      response.success = true
      if booking.promotional_code_log && User.where(refer_code: (booking.promotional_code_log.promotional_code.code)).exists?
        booking.user.deduct_credit_to_balance_user(booking)
      end
      if booking.can_use_credit_balance?
        booking.user.return_credit_balance_to_user(booking)
      end
      response.message = I18n.t(:booking_canceled_with_success)
    rescue Booking::BookingHotelMinimumHoursOfNoticeError
      response.message = I18n.t(:booking_cancel_failure_because_of_hotel_minimum_hours_of_notice)
    rescue CouldNotCancelOmnibeesBookingError
      AdminMailer.error_cancelling_on_omnibees(booking).deliver
      response.message = I18n.t(:booking_cancel_error_message_try_again)
    rescue => e
      if @payment_cancellation_response.present?
        # save the maxipago response if there is one
        @payment.maxipago_response += @payment_cancellation_response.body
        @payment.save!
      end
      response.message = I18n.t(:booking_cancel_error_message_try_again)
    end
    return response
  end

  private
    def cancel_booking
      ActiveRecord::Base.transaction(requires_new: true) do
        booking.cancel(admin_cancelation)
        booking.update!(canceled_by_agency: true) if agency_cancelation
        booking.update!(canceled_by_admin: true) if admin_cancelation
        if booking.confirmed_by_maxipago?
          cancel_booking_payment
        end
      end
      cancel_booking_omnibees_or_hoteisnet! if booking.is_omnibees_or_hoteisnet? && booking.omnibees_code.present?
    end

    def cancel_booking_omnibees_or_hoteisnet!
      # 1 = Hoteisnet || 2 = Omnibees
      choice = ChooseOmnibeesOrHoteisnet.choose(booking.pack_in_hours, booking.offer.checkin_timestamp)
      if choice == 1
        client = Hotelnet::Client.new
      else
        client = Omnibees::Client.new
      end

      cancellation_xml = client.cancel_booking({ booking: booking })
      booking.cancellation_request_xml = client.last_request_xml
      booking.cancellation_response_xml = cancellation_xml
      booking.save!

      if choice == 1
        cancellation_status = Hotelnet::DTO::HotelReservationToCancel.parse(cancellation_xml)
      else
        cancellation_status = Omnibees::DTO::HotelReservationToCancel.parse(cancellation_xml)
      end

      raise CouldNotCancelOmnibeesBookingError unless cancellation_status.is_cancelled?
    end

    def cancel_booking_payment
      if @payment.paypal_payment_id.present?
        begin
          paypal_payment = PayPal::SDK::REST::DataTypes::Payment.find(@payment.paypal_payment_id)
          sale = paypal_payment.transactions[0].related_resources[0].sale
          if @payment.status == Payment::ACCEPTED_STATUSES[:authorized]
            charge_value = booking.booking_tax
          elsif @payment.status == Payment::ACCEPTED_STATUSES[:captured]
            charge_value = booking.total_price
          end
          # refund = sale.refund( :amount => { :total => "%.2f" % calculate_to_currency_at_authorized_at(booking.hotel.currency_symbol, charge_value, booking.currency_code, @payment.authorized_at), :currency => booking.currency_code } )
          refund = sale.refund( :amount => { total: paypal_payment.transactions[0].amount.total, currency: paypal_payment.transactions[0].amount.currency } )
          # Check refund status
          if refund.success?
            #logger.info "Refund[#{refund.id}] Success"
            @payment.paypal_refund_id = refund.id
            @payment.save!
          else
            #logger.error "Unable to Refund"
            #logger.error refund.error.inspect
            @payment.maxipago_response += refund.error.inspect.to_s
            @payment.save!
            raise CouldNotCancellPaypalTransactionError.new
          end
        rescue ResourceNotFound => error
          Rails.logger.error(error)
          Airbrake.notify(error)
          # It will throw ResourceNotFound exception if the payment not found
          #logger.error "Authorization Not Found"
        end
      elsif @payment.epayco_ref.present?
        UserMailer.booking_canceled_send_to_epayco(@payment).deliver
      elsif @payment.payu_transaction_id.present? || @payment.payu_order_id.present?
        UserMailer.booking_refund_send_to_payu(@payment).deliver
      elsif !@payment.affiliate.nil?
        @payment.update!(status: Payment::ACCEPTED_STATUSES[:canceled])
      elsif @payment.maxipago_order_id.present?
        @payment_cancellation_response = MaxiPagoInterface.cancel_transaction({
                                              payment: @payment
                                            })
        @payment.maxipago_response += @payment_cancellation_response.body
        @payment.save!
        raise CouldNotCancellMaxiPagoTransactionError.new unless @payment_cancellation_response.transaction_canceled
      end
      @payment.update!(status: Payment::ACCEPTED_STATUSES[:canceled])
    end
end
