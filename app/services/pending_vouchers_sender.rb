# encoding: utf-8
class PendingVouchersSender
  module PendingBookingsVoucherScope
    def pending_to_send_voucher
      without_easy_taxi_voucher.
      in_valid_status.
      in_the_future
    end

    def in_valid_status
      by_status([
        Booking::ACCEPTED_STATUSES[:confirmed],
        Booking::ACCEPTED_STATUSES[:confirmed_and_captured],
        Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced],
        Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
      ])
    end

    def in_the_future
      where("checkin_date >= ?", Date.current)
    end

    def without_easy_taxi_voucher
      booking_ids = EasyTaxiVoucher.linked.pluck(:booking_id)
      if booking_ids.empty?
        self
      else
        where("id NOT IN (?)", EasyTaxiVoucher.linked.pluck(:booking_id))
      end
    end

  end

  def run
    Booking.all.extending(PendingBookingsVoucherScope).pending_to_send_voucher.includes(:hotel).each do | booking |
      available_voucher = EasyTaxiVoucher.available_on(booking.checkin_date).order('expiration_date ASC').first
      if available_voucher && VoucherConsumer.consume(available_voucher, booking)
        VoucherMailer.easy_taxi_linked(available_voucher.id).deliver
      end
    end
  end

end
