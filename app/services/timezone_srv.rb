class TimezoneSrv
	#lat = number | lng = number | tmstmp = number

  def self.get(lat,lng,tmstp)
    response = Net::HTTP.get_response(URI("https://maps.googleapis.com/maps/api/timezone/json?location=#{lat},#{lng}&timestamp=#{tmstp}&key=#{ENV['GOOGLE_TIMEZONE_API_KEY']}"))
    return JSON.parse(response.body)
  end

end
