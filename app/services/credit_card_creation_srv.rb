# encoding: utf-8
class CreditCardCreationSrv
  include Services::Base

  attribute :credit_card_owner
  attribute :credit_card_attributes

  def call
    build_credit_card_for(credit_card_owner)
    generate_maxipago_token_for(credit_card_owner) if credit_card_owner.maxipago_token.blank?
    generate_credit_card_token
    @credit_card.save!
  ensure
    return @credit_card
  end

  private
    def generate_maxipago_token_for(client)
      # We are appending an "e" to the beginning of the id if the client is a company
      # We are appending an "t" to the beginning of the id if the client is a traveler
      # to avoid conflicts with the normal users ids
      if client.is_a? Company
        params_for_token_generation = {
                                        client_id: "e#{client.id.to_s}",
                                        client_first_name: client.responsible_first_name,
                                        client_last_name: client.responsible_last_name
                                      }
      elsif client.is_a? Traveler
        params_for_token_generation = {
                                        client_id: "t#{client.id.to_s}",
                                        client_first_name: client.first_name,
                                        client_last_name: client.last_name
                                      }
      else
        params_for_token_generation = {
                                        client_id: "#{client.id.to_s}",
                                        client_first_name: client.first_name,
                                        client_last_name: client.last_name
                                      }
      end
      client.maxipago_token = MaxiPagoInterface.generate_client_token_for(params_for_token_generation)
      client.save!
    end

    def build_credit_card_for(client)
      @credit_card = CreditCard.new(credit_card_attributes)
      @credit_card.owner = client
    end

    def generate_credit_card_token
      @credit_card.valid?
      raise unless @credit_card.errors.keys.length == 1 && @credit_card.errors.keys[0] == :maxipago_token
      @credit_card.maxipago_token = MaxiPagoInterface.generate_credit_card_token_for({
        credit_card: @credit_card
      })
    end
end
