# encoding: utf-8

class AzulPendingBookingsSender
  AZUL_SERVER_DOMAIN_NAME   = ENV['AZUL_SERVER_DOMAIN_NAME']
  AZUL_SERVER_PORT          = ENV["AZUL_SERVER_PORT"].to_i
  AZUL_SERVER_FTP_LOGIN     = ENV["AZUL_SERVER_FTP_LOGIN"]
  AZUL_SERVER_FTP_PASSWORD  = ENV["AZUL_SERVER_FTP_PASSWORD"]

  module PendingBookingsScope
    def pending_to_send
      paid.
      uses_tudo_azul.
      not_notified_yet.
      checkin_date_before_yesterday
    end

    def not_notified_yet
      where(tudo_azul_notified_at: nil)
    end

    def checkin_date_before_yesterday
      where("checkin_date <= ?", Date.yesterday)
    end
  end

  def run
    today = DateTime.current
    file_bookings_hash = create_file_hash today

    if file_bookings_hash[:pending_bookings].count > 0
      file_path = Rails.root.to_s + "/tmp/#{file_bookings_hash[:file_name]}"
      file = File.new(file_path, "w")
      file.write(file_bookings_hash[:header] + "\n")
      file_bookings_hash[:pending_bookings].each do | pending_booking |
        file.write(pending_booking + "\n")
      end
      file.write(file_bookings_hash[:footer])
      file.close

      Net::SFTP.start(AZUL_SERVER_DOMAIN_NAME, AZUL_SERVER_FTP_LOGIN, :password => AZUL_SERVER_FTP_PASSWORD, port: AZUL_SERVER_PORT) do |sftp|
        sftp.dir.foreach("/") do |entry|
          Rails.logger.info entry.longname
        end
        sftp.upload!(file_path, "/input/#{file_bookings_hash[:file_name]}")
      end

      Booking.where(id: file_bookings_hash[:bookings_ids_to_save]).update_all(tudo_azul_notified_at: today)
      return true
    else
      return false
    end
  end

  def create_file_hash today
    file_name = "NON_AIR_TRN_#{today.strftime('%Y%m%d')}_#{today.strftime('%H_%M_%S')}_HOTELQDO.trn"

    header_hash = {
      record_type: 'H',
      date: today.strftime("%Y%m%d"),
      partner_code: 'HOTELQDO'
    }

    pending_bookings_array = []
    bookings_ids_to_save = []
    Booking.all.extending(PendingBookingsScope).pending_to_send.includes(:user).each do | booking |
      booking_hash = {
        record_type: 'D',
        transaction_type: '_GNA',
        ffp_number: '',
        first_name: booking.user.first_name,
        last_name: booking.user.last_name,
        transaction_code: '1',
        partner_transaction_date: booking.created_at.strftime("%Y%m%d"),
        partner_transaction_time: booking.created_at.strftime("%H%M"),
        partner_transaction_time_zone: 'AMERICA/SAO_PAULO',
        location_code: '',
        value: booking.total_price.to_i,
        revenue: '',
        description: 'Pontuacao HotelQuando',
        partner_transaction_identifier: booking.id,
        reversal_transaction_identifier: '',
        cpf: booking.user.simple_cpf,
        field_02: '', field_03: '', field_04: '', field_05: '', field_06: '', field_07: '',
        field_08: '', field_09: '', field_10: '', field_11: '', field_12: '', field_13: '',
        field_14: '', field_15: '', field_16: '', field_17: '', field_18: '', field_19: '',
        field_20: '', field_21: '', field_22: '', field_23: '', field_24: '', field_25: '',
        field_26: '', field_27: '', field_28: '', field_29: '', field_30: ''
      }

      bookings_ids_to_save << booking.id

      pending_bookings_array << booking_hash.map{|k,v| "#{v}"}.join('|')
    end

    footer_hash = {
      record_type: 'T',
      record_count: pending_bookings_array.count
    }

    {
      file_name: file_name,
      header: header_hash.map{|k,v| "#{v}"}.join('|'),
      pending_bookings: pending_bookings_array,
      footer: footer_hash.map{|k,v| "#{v}"}.join('|'),
      bookings_ids_to_save: bookings_ids_to_save
    }
  end
end
