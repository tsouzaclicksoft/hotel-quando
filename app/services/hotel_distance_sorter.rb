class HotelDistanceSorter
	
  def self.sort(hotels, ref_coordinates)
		hotels.map {|hotel| hotel[:distance] = hotel[:hotel].distance_to(ref_coordinates).round(2)}
		if hotels.present? && hotels.first[:business_room].present?
      hotels.sort_by {|hotel| [hotel[:distance], hotel[:business_room][:maximum_capacity], hotel[:min_price]]}
    else
			hotels.sort_by {|hotel| hotel[:distance]}
    end
	end

end
