class LocationSrv

  def self.get(address)
    addressEncode = URI.encode("#{address}")
    response = Net::HTTP.get_response(URI("http://maps.google.com/maps/api/geocode/json?address=#{addressEncode}&sensor=false"))
    return JSON.parse(response.body)
  end

end
