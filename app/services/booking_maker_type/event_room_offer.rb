class BookingMakerType::EventRoomOffer
  def initialize(booking)
    @booking = booking
  end

  def try_create
    time_around = EventRoomOffer::ROOM_CLEANING_TIME-1.minute

    conflicting_available_offers_ids = @booking.event_room_offer.get_conflicting_offers_ids_with_status_in(EventRoomOffer::ACCEPTED_STATUS[:available])
    conflicting_offers_query = if conflicting_available_offers_ids.blank?
      ""
    else
      %Q{
        UPDATE event_room_offers
        SET status=#{EventRoomOffer::ACCEPTED_STATUS[:suspended]}
        WHERE id IN (#{conflicting_available_offers_ids.join(",")});
      }
    end

    conflicting_removed_by_hotel_offers_ids = @booking.event_room_offer.get_conflicting_offers_ids_with_status_in(EventRoomOffer::ACCEPTED_STATUS[:removed_by_hotel])
    conflicting_offers_query += if conflicting_removed_by_hotel_offers_ids.blank?
      ""
    else
      %Q{
        UPDATE event_room_offers
        SET status=#{EventRoomOffer::ACCEPTED_STATUS[:ghost]}
        WHERE id IN (#{conflicting_removed_by_hotel_offers_ids.join(",")});
      }
    end

    ghosts_query = @booking.event_room_offer.generate_insert_ghosts_query_for_conflicting_offers

    # HERE we have a race condition because at this exact point
    # any conflicting offer can be created and it will not be marked as suspended
    # Since the bookings table has a rule protecting overlap of checkin/checkout for the sabe room
    # we have a 'fail safe' situation

    query = %Q{

      INSERT INTO bookings (
        "status",
        "created_by_agency",
        "guest_email",
        "booking_tax",
        "is_active",
        "checkin_date",
        "guest_name",
        "note",
        "number_of_people",
        "hotel_comments",
        "pack_in_hours",
        "user_id",
        "company_id",
        "event_room_offer_id",
        "business_room_id",
        "date_interval",
        "created_at",
        "hotel_id",
        "cached_offer_price",
        "cached_offer_no_show_value"
      ) VALUES (
        '#{@booking.status}',
        #{@booking.created_by_agency?},
        '#{@booking.guest_email}',
        '#{@booking.booking_tax}',
        true,
        '#{@booking.event_room_offer.checkin_timestamp.strftime("%Y-%m-%d")}',
        '#{@booking.guest_name}',
        '#{@booking.note}',
        '#{@booking.number_of_people.to_i}',
        '#{@booking.hotel_comments}',
        '#{@booking.event_room_offer.pack_in_hours}',
        '#{@booking.user_id}',
        #{@booking.company_id || 'NULL'},
        '#{@booking.event_room_offer_id}',
        '#{@booking.business_room_id}',
        '(#{(@booking.event_room_offer.checkin_timestamp.to_datetime - time_around).strftime("%Y-%m-%d %H:%M:%S")},
        #{(@booking.event_room_offer.checkout_timestamp.to_datetime + time_around).strftime("%Y-%m-%d %H:%M:%S")})',
        '#{DateTime.now.utc.strftime("%Y-%m-%d %H:%M:%S")}',
        '#{@booking.event_room_offer.hotel_id}',
        '#{@booking.event_room_offer.price.to_f}',
        '#{@booking.event_room_offer.no_show_value.to_f}'
      );
      UPDATE event_room_offers SET status=#{EventRoomOffer::ACCEPTED_STATUS[:reserved]} WHERE id=#{@booking.event_room_offer.id};
      #{conflicting_offers_query}
      #{ghosts_query}

      }.squish
    Booking.transaction do
      begin
        !!(ActiveRecord::Base.connection.execute(query))
      rescue
        raise ActiveRecord::Rollback
      end
    end
  end
end