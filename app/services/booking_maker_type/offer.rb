class BookingMakerType::Offer
  def initialize(booking)
    @booking = booking
  end

  def try_create
    time_around = Offer::ROOM_CLEANING_TIME-1.minute
    guest_name = ActiveRecord::Base.connection.quote(@booking.guest_name)
    phone_number = ActiveRecord::Base.connection.quote(@booking.phone_number)
    note = ActiveRecord::Base.connection.quote(@booking.note)
    hotel_comments = ActiveRecord::Base.connection.quote(@booking.hotel_comments)

    if @booking.is_omnibees_or_hoteisnet?
      query = %Q{

        INSERT INTO bookings (
          "status",
          "created_by_agency",
          "guest_email",
          "booking_tax",
          "is_active",
          "checkin_date",
          "guest_name",
          "phone_number",
          "note",
          "number_of_people",
          "hotel_comments",
          "pack_in_hours",
          "user_id",
          "traveler_id",
          "consultant_id",
          "company_id",
          "omnibees_offer_id",
          "room_type_id",
          "room_id",
          "date_interval",
          "created_at",
          "hotel_id",
          "cached_room_type_initial_capacity",
          "cached_room_type_maximum_capacity",
          "cached_offer_extra_price_per_person",
          "cached_offer_price",
          "cached_offer_no_show_value",
          "affiliate_id",
          "created_by_affiliate",
          "have_promotional_code",
          "currency_code",
          "using_balance",
          "balance_value_used",
          "is_app_mobile",
          "executive_responsible_id"
        ) VALUES (
          '#{@booking.status}',
          #{@booking.created_by_agency?},
          '#{@booking.guest_email}',
          '#{@booking.booking_tax}',
          true,
          '#{@booking.offer.checkin_timestamp.strftime("%Y-%m-%d")}',
          #{guest_name},
          #{phone_number},
          #{note},
          '#{@booking.number_of_people.to_i}',
          #{hotel_comments},
          '#{@booking.offer.pack_in_hours}',
          '#{@booking.user_id}',
          #{@booking.traveler_id || 'NULL'},
          #{@booking.consultant_id || 'NULL'},
          #{@booking.company_id || 'NULL'},
          '#{@booking.omnibees_offer_id}',
          '#{@booking.room_type_id}',
          '#{@booking.room_id}',
          '(#{(@booking.offer.checkin_timestamp.to_datetime - time_around).strftime("%Y-%m-%d %H:%M:%S")},
          #{(@booking.offer.checkout_timestamp.to_datetime + time_around).strftime("%Y-%m-%d %H:%M:%S")})',
          '#{DateTime.now.utc.strftime("%Y-%m-%d %H:%M:%S")}',
          '#{@booking.offer.hotel_id}',
          '#{@booking.offer.room_type_initial_capacity}',
          '#{@booking.offer.room_type_maximum_capacity}',
          '#{@booking.offer.extra_price_per_person.to_f}',
          '#{@booking.offer.price.to_f}',
          '#{@booking.offer.no_show_value.to_f}',
          #{@booking.affiliate_id || 'NULL'},
          '#{@booking.created_by_affiliate?}',
          #{@booking.have_promotional_code},
          '#{@booking.currency_code}',
          #{@booking.using_balance},
          '#{@booking.discount_value_balance}',
          #{@booking.is_app_mobile},
          #{@booking.executive_responsible_id || 'NULL'}
        );}.squish
    else

      conflicting_available_offers_ids = @booking.offer.get_conflicting_offers_ids_with_status_in(Offer::ACCEPTED_STATUS[:available])
      conflicting_offers_query = if conflicting_available_offers_ids.blank?
        ""
      else
        %Q{
          UPDATE offers
          SET status=#{Offer::ACCEPTED_STATUS[:suspended]}
          WHERE id IN (#{conflicting_available_offers_ids.join(",")});
        }
      end

      conflicting_removed_by_hotel_offers_ids = @booking.offer.get_conflicting_offers_ids_with_status_in(Offer::ACCEPTED_STATUS[:removed_by_hotel])
      conflicting_offers_query += if conflicting_removed_by_hotel_offers_ids.blank?
        ""
      else
        %Q{
          UPDATE offers
          SET status=#{Offer::ACCEPTED_STATUS[:ghost]}
          WHERE id IN (#{conflicting_removed_by_hotel_offers_ids.join(",")});
        }
      end

      ghosts_query = @booking.offer.generate_insert_ghosts_query_for_conflicting_offers

      # HERE we have a race condition because at this exact point
      # any conflicting offer can be created and it will not be marked as suspended
      # Since the bookings table has a rule protecting overlap of checkin/checkout for the sabe room
      # we have a 'fail safe' situation
      query = %Q{

        INSERT INTO bookings (
          "status",
          "created_by_agency",
          "guest_email",
          "booking_tax",
          "is_active",
          "checkin_date",
          "guest_name",
          "phone_number",
          "note",
          "number_of_people",
          "hotel_comments",
          "pack_in_hours",
          "user_id",
          "traveler_id",
          "consultant_id",
          "company_id",
          "offer_id",
          "room_type_id",
          "room_id",
          "date_interval",
          "created_at",
          "hotel_id",
          "cached_room_type_initial_capacity",
          "cached_room_type_maximum_capacity",
          "cached_offer_extra_price_per_person",
          "cached_offer_price",
          "cached_offer_no_show_value",
          "affiliate_id",
          "created_by_affiliate",
          "have_promotional_code",
          "currency_code",
          "using_balance",
          "balance_value_used",
          "is_app_mobile",
          "executive_responsible_id"
        ) VALUES (
          '#{@booking.status}',
          #{@booking.created_by_agency?},
          '#{@booking.guest_email}',
          '#{@booking.booking_tax}',
          true,
          '#{@booking.offer.checkin_timestamp.strftime("%Y-%m-%d")}',
          #{guest_name},
          #{phone_number},
          #{note},
          '#{@booking.number_of_people.to_i}',
          #{hotel_comments},
          '#{@booking.offer.pack_in_hours}',
          '#{@booking.user_id}',
          #{@booking.traveler_id || 'NULL'},
          #{@booking.consultant_id || 'NULL'},
          #{@booking.company_id || 'NULL'},
          '#{@booking.offer_id}',
          '#{@booking.room_type_id}',
          '#{@booking.room_id}',
          '(#{(@booking.offer.checkin_timestamp.to_datetime - time_around).strftime("%Y-%m-%d %H:%M:%S")},
          #{(@booking.offer.checkout_timestamp.to_datetime + time_around).strftime("%Y-%m-%d %H:%M:%S")})',
          '#{DateTime.now.utc.strftime("%Y-%m-%d %H:%M:%S")}',
          '#{@booking.offer.hotel_id}',
          '#{@booking.offer.room_type_initial_capacity}',
          '#{@booking.offer.room_type_maximum_capacity}',
          '#{@booking.offer.extra_price_per_person.to_f}',
          '#{@booking.offer.price.to_f}',
          '#{@booking.offer.no_show_value.to_f}',
          #{@booking.affiliate_id || 'NULL'},
          '#{@booking.created_by_affiliate?}',
          #{@booking.have_promotional_code},
          '#{@booking.currency_code}',
          #{@booking.using_balance},
          '#{@booking.discount_value_balance}',
          #{@booking.is_app_mobile},
          #{@booking.executive_responsible_id || 'NULL'}

        );
        UPDATE offers SET status=#{Offer::ACCEPTED_STATUS[:reserved]} WHERE id=#{@booking.offer.id};
        #{conflicting_offers_query}
        #{ghosts_query}

        }.squish
    end
    Booking.transaction do
      begin
        !!(ActiveRecord::Base.connection.execute(query))
      rescue => ex
        Rails.logger.error(ex)
        Airbrake.notify(ex, parameters: {booking: @booking.to_json})
        raise ActiveRecord::Rollback
      end
    end
  end
end