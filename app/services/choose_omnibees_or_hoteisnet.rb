class ChooseOmnibeesOrHoteisnet
	
  ### will return 1 for hoteisnet and 2 for omnibees integration
  ### hoteisnet will consider only 12h and a space between 14h to 23h otherwise will be omnibees
  def self.choose_based_on_time_now(pack_in_hours, check_in_datetime)
    if ((pack_in_hours == 12) && check_in_datetime.strftime("%H").between?('14', '23')) || pack_in_hours > 12
      return 1
    end
=begin
    elsif pack_in_hours == 9
      if check_in_datetime.strftime("%Y%m%d") == Time.zone.now.strftime("%Y%m%d")
        if check_in_datetime.strftime("%H").between?('14', '22')
          return 1
        end
      elsif check_in_datetime.strftime("%H").between?('14', '23') #|| check_in_datetime.strftime("%H").between?('00', '03')
        return 1
      end
    end
=end
    return 2
  end

  def self.choose(pack_in_hours, check_in_datetime)
    if ((pack_in_hours == 12) && check_in_datetime.strftime("%H").between?('14', '23')) || pack_in_hours > 12
      return 1
    #elsif pack_in_hours == 9 && check_in_datetime.strftime("%H").between?('14', '23')  #|| check_in_datetime.strftime("%H").between?('0', '3') ) )
    #  return 1
    end
    return 2
  end

  def self.is_hoteisnet?(pack_in_hours, check_in_datetime)
    if ((pack_in_hours == 12) && check_in_datetime.strftime("%H").between?('14', '23')) || pack_in_hours > 12
      return true
    #elsif pack_in_hours == 9 && check_in_datetime.strftime("%H").between?('14', '23') #|| check_in_datetime.strftime("%H").between?('0', '3') ) )
    #  return true
    else 
      return false
    end
  end

end
