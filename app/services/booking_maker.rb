# encoding: utf-8
class BookingMaker
  include Services::Base

  attribute :offer
  attribute :booking

  def call
    response = OpenStruct.new
    response.success = false
    current_public_user = User.find(booking.user_id)
    user = User::BudgetDecorator.decorate(current_public_user)
    offer_class_name = offer.class.name
    offer_class = offer_class_name.eql?("OmnibeesOffer") ? "Offer".constantize : offer_class_name.constantize
    fill_offer_and_room_id(offer_class_name)
    if booking.valid?
      set_booking_cached_prices(booking)
      creating_booking = case offer_class_name
      when "MeetingRoomOffer" then BookingMakerType::MeetingRoomOffer.new(booking)
      when "EventRoomOffer" then BookingMakerType::EventRoomOffer.new(booking)
      else BookingMakerType::Offer.new(booking)
      end
      begin
        creating_booking.try_create
        created_booking = case offer_class_name
        when "MeetingRoomOffer" then Booking.where(business_room_id: booking.business_room_id, meeting_room_offer_id: booking.meeting_room_offer_id, user_id: booking.user_id).last
        when "EventRoomOffer" then Booking.where(business_room_id: booking.business_room_id, event_room_offer_id: booking.event_room_offer_id, user_id: booking.user_id).last
        else Booking.where(room_id: booking.room_id, offer_id: booking.offer_id, user_id: booking.user_id).last
        end
        if created_booking
          AgencyMailer.send_booking_creation_notice(created_booking).deliver if created_booking.created_by_agency?
        end
#        if created_booking.created_by_affiliate?
#          AffiliateMailer.send_booking_creation_notice(created_booking).deliver
#        end
        response.success = created_booking.present?
        response.booking = created_booking
      rescue => ex
        Rails.logger.error(ex)
        Airbrake.notify(ex)
        response.message = I18n.t(:message_booking_creation_error)
      end
    else
      #render :new
    end
    return response
  end

  private
    def fill_offer_and_room_id(offer_type)
      case offer_type
      when "Offer"
        booking.offer_id = offer.id
        booking.room_type_id = offer.room_type_id
        booking.room_id = offer.room_id
      when "OmnibeesOffer"
        booking.omnibees_offer_id = offer.id
        booking.room_type_id = offer.room_type_id
        booking.room = Room.create!(room_type: offer.room_type, hotel: offer.hotel, number: "OF-#{DateTime.now.to_i}-#{offer.id}")
      when "MeetingRoomOffer"
        booking.meeting_room_offer_id = offer.id
        booking.business_room_id = offer.meeting_room_id
      when "EventRoomOffer"
        booking.event_room_offer_id = offer.id
        booking.business_room_id = offer.event_room_id
      else
        raise 'Offer type not valid to create booking'
      end
    end

    def set_booking_cached_prices(booking)
      if booking.offer_id.present? || booking.omnibees_offer_id.present?
        booking.cached_room_type_initial_capacity = @offer.room_type_initial_capacity
        booking.cached_room_type_maximum_capacity = @offer.room_type_maximum_capacity
        booking.cached_offer_extra_price_per_person = @offer.extra_price_per_person
      end
      booking.cached_offer_price = @offer.price
    end
end
