class VoucherConsumer
  def self.consume(voucher, booking)
    new(voucher, booking).consume
  end

  def initialize(voucher, booking)
    @voucher = voucher
    @booking = booking
  end

  def consume
    can_be_consumed? and update_voucher
  end

  def can_be_consumed?
    !@booking.cancelable?
  end

  def update_voucher
    @voucher.update_attributes(booking_id: @booking.id, linked_at: DateTime.now)
  end
end
