class OnlinePayment

  def self.hotel_accept?(hotel)
  	return hotel.accept_online_payment && hotel.is_brazilian 
  end

  def self.booking_accept?(booking)
    return (booking.hotel.accept_online_payment || booking.is_really_hoteisnet? || booking.have_promotional_code || booking.traveler_id.present?) && booking.hotel.is_brazilian
  end

end