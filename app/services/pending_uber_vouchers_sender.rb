# encoding: utf-8
class PendingUberVouchersSender
  module PendingBookingsVoucherScope
    def pending_to_send_voucher
      without_uber_voucher.
      in_valid_status.
      in_date_early_partnership.
      in_valid_cities
    end

    def in_valid_cities
    	valid_cities_id = City.where(name: (["São Paulo","Rio de Janeiro","Belo Horizonte","Brasília"])).pluck(:id)
    	hotels_id = Hotel.where(city_id: valid_cities_id).pluck(:id)
    	where(hotel_id: hotels_id)
    end

    def in_valid_status
      by_status([
        Booking::ACCEPTED_STATUSES[:confirmed],
        Booking::ACCEPTED_STATUSES[:confirmed_and_captured],
        Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced],
        Booking::ACCEPTED_STATUSES[:no_show_paid_with_success],
        Booking::ACCEPTED_STATUSES[:confirmed_without_card]
      ])
    end

    def in_date_early_partnership
      where("checkin_date >= ?", "2015-08-25")
    end

    def without_uber_voucher
      booking_ids = UberVoucher.linked.pluck(:booking_id)
      if booking_ids.empty?
        self
      else
        where("id NOT IN (?)", UberVoucher.linked.pluck(:booking_id))
      end
    end

  end

  def run
    Booking.all.extending(PendingBookingsVoucherScope).pending_to_send_voucher.includes(:hotel).each do | booking |
      available_voucher = UberVoucher.available_on(booking.checkin_date).order('expiration_date ASC').first
      if available_voucher && VoucherConsumer.consume(available_voucher, booking)
        VoucherMailer.uber_linked(available_voucher.id).deliver
      end
    end
  end

end
