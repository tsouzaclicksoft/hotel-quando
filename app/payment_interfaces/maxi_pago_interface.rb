# encoding: utf-8

require 'ostruct'

module MaxiPagoInterface
  class InvalidArgumentsError < StandardError; end;
  class CannotGenerateCustomerTokenError < StandardError; end;
  class CannotVoidTransactionError < StandardError; end;
  class CannotCancelPaymentsThatWereNotAuthorized < StandardError; end;
  class CannotRefundWithoutAValueError < StandardError; end;
  class MalformedMaxipagoResponseError < StandardError; end;

  class << self
    def direct_sale(options)
      check_hash_required_keys(options, [:payment])
      client = get_maxipago_client()
      client.use(get_transaction_request)
      if options[:no_show]
        response = client.execute({
          command: "sale",
          processor_id: MP_PROCESSOR_ID,
          customer_id: options[:payment].credit_card.owner.maxipago_token,
          reference_num: options[:payment].id,
          token: options[:payment].credit_card.maxipago_token,
          charge_total: options[:payment].booking.no_show_value_without_tax
        })
      else
        response = client.execute({
          command: "sale",
          processor_id: MP_PROCESSOR_ID,
          customer_id: options[:payment].credit_card.owner.maxipago_token,
          reference_num: options[:payment].id,
          token: options[:payment].credit_card.maxipago_token,
          charge_total: options[:payment].booking.total_price
        })
      end

      response_hash = Hash.from_xml(response[:body])
      response_object = OpenStruct.new
      begin
        response_object.body = response[:body]
        response_object.order_id = response_hash["transaction_response"]["orderID"]
        response_object.transaction_id = response_hash["transaction_response"]["transactionID"]
        response_object.success = (response_hash["transaction_response"]["responseCode"] == "0")
      rescue
        raise MaxiPagoInterface::MalformedMaxipagoResponseError, response[:body]
      end

      response_object
    end

    def authorize_transaction(options)
      # now this method is direct sale too but only charged booking tax
      check_hash_required_keys(options, [:payment])
      client = get_maxipago_client()
      client.use(get_transaction_request)
       response = client.execute({
          command: "sale",
          processor_id: MP_PROCESSOR_ID,
          customer_id: options[:payment].credit_card.owner.maxipago_token,
          reference_num: options[:payment].id,
          token: options[:payment].credit_card.maxipago_token,
          charge_total: options[:payment].booking.booking_tax
        })
=begin
      response = client.execute({
        command: "authorization",
        processor_id: MP_PROCESSOR_ID,
        customer_id: options[:payment].credit_card.owner.maxipago_token,
        reference_num: options[:payment].id,
        token: options[:payment].credit_card.maxipago_token,
        charge_total: options[:payment].booking.total_price
      })
=end
      response_hash = Hash.from_xml(response[:body])
      response_object = OpenStruct.new
      begin
        response_object.body = response[:body]
        response_object.order_id = response_hash["transaction_response"]["orderID"]
        response_object.transaction_id = response_hash["transaction_response"]["transactionID"]
        response_object.is_authorized = (response_hash["transaction_response"]["responseCode"] == "0")
      rescue
        raise MaxiPagoInterface::MalformedMaxipagoResponseError, response[:body]
      end
      response_object
    end

    def capture_transaction(options)
      check_hash_required_keys(options, [:payment])
      client = get_maxipago_client()
      client.use(get_transaction_request)
      response = client.execute({
        command: "capture",
        order_id: options[:payment].maxipago_order_id,
        reference_num: options[:payment].id,
        charge_total: options[:payment].booking.total_price
      })

      response_hash = Hash.from_xml(response[:body])
      response_object = OpenStruct.new
      response_object.body = response[:body]
      response_object.is_captured = (response_hash["transaction_response"]["responseCode"] == "0")
      response_object
    end

    def cancel_transaction(options)
      raise CannotCancelPaymentsThatWereNotAuthorized.new if options[:payment].authorized_at.nil?
      if options[:payment].authorized_at.to_date == Date.today
        void_transaction(options)
      else
        reverse_transaction(options)
      end
    end

    def void_transaction(options)
      if options[:payment].status != Payment::ACCEPTED_STATUSES[:authorized]
        raise CannotVoidTransactionError.new('Only transactions that were authorized today can be voided') unless options[:payment].authorized_at.to_date == Date.today
      end
      check_hash_required_keys(options, [:payment])
      client = get_maxipago_client()
      client.use(get_transaction_request)
      response = client.execute({
        command: "void",
        transaction_id: options[:payment].maxipago_transaction_id
      })

      response_hash = Hash.from_xml(response[:body])
      response_object = OpenStruct.new
      response_object.body = response[:body]
      response_object.transaction_canceled = (response_hash["transaction_response"]["responseCode"] == "0")
      response_object
    end

    def reverse_transaction(options)
      raise CannotVoidTransactionError.new('Only transactions that were not authorized today can be returned') unless options[:payment].authorized_at.to_date != Date.today
      check_hash_required_keys(options, [:payment])
      client = get_maxipago_client()
      client.use(get_transaction_request)
      payment_is_authorized = options[:payment].status == Payment::ACCEPTED_STATUSES[:authorized]
      if payment_is_authorized
        charge_total = options[:payment].booking.booking_tax
      else
        charge_total = options[:payment].booking.total_price
      end
      response = client.execute({
        command: "reversal",
        order_id: options[:payment].maxipago_order_id,
        reference_num: options[:payment].id,
        charge_total: charge_total
      })

      response_hash = Hash.from_xml(response[:body])
      response_object = OpenStruct.new
      response_object.body = response[:body]
      response_object.transaction_canceled = (response_hash["transaction_response"]["responseCode"] == "0")
      response_object
    end

    def refund(options)
      check_hash_required_keys(options, [:payment, :value_to_refund])
      raise CannotRefundWithoutAValueError.new('Can only make a refund if there is a value to refund') unless (options[:value_to_refund] > 0)
      client = get_maxipago_client()
      client.use(get_transaction_request)
      response = client.execute({
        command: "reversal",
        order_id: options[:payment].maxipago_order_id,
        reference_num: options[:payment].id,
        charge_total: options[:value_to_refund]
      })

      response_hash = Hash.from_xml(response[:body])
      response_object = OpenStruct.new
      response_object.body = response[:body]
      response_object.success = (response_hash["transaction_response"]["responseCode"] == "0")
      response_object
    end

    def generate_credit_card_token_for(options)
      check_hash_required_keys(options, [:credit_card])
      client = get_maxipago_client()
      client.use(get_api_request)
      response = client.execute({
        command: "add_card_onfile",
        customer_id: options[:credit_card].owner.maxipago_token,
        credit_card_number: options[:credit_card].temporary_number.to_s.gsub(" ", ""),
        expiration_month: options[:credit_card].expiration_month.to_s.rjust(2, '0'),
        expiration_year: options[:credit_card].expiration_year,
        billing_name: options[:credit_card].billing_name,
        billing_address1: options[:credit_card].billing_address1,
        billing_city: options[:credit_card].billing_city,
        billing_state: options[:credit_card].billing_state,
        billing_zip: options[:credit_card].billing_zipcode,
        billing_country: options[:credit_card].billing_country,
        billing_phone: options[:credit_card].billing_phone,
        billing_email: options[:credit_card].owner.email,
        onfile_permissions: "ongoing"
      })

      response_hash = Hash.from_xml(response[:body])
      if response_hash["api_response"]["errorCode"] != "0"
        raise CannotGenerateCustomerTokenError.new(response_hash["api_response"]["errorMessage"])
      end

      response_hash["api_response"]["result"]["token"]
    end

    def generate_client_token_for(options)
      client = get_maxipago_client()
      client.use(get_api_request)
      if options[:user].present?
        response = client.execute({
          command: "add_consumer",
          customer_id_ext: options[:user].id.to_s,
          firstname: options[:user].first_name,
          lastname: options[:user].last_name
        })
      else
        check_hash_required_keys(options, [:client_id, :client_first_name, :client_last_name])
        response = client.execute({
          command: "add_consumer",
          customer_id_ext: options[:client_id].to_s,
          firstname: options[:client_first_name],
          lastname: options[:client_last_name]
        })
      end
      response_hash = Hash.from_xml(response[:body])
      if response_hash["api_response"]["errorCode"] != "0"
        raise CannotGenerateCustomerTokenError.new(response_hash["api_response"]["errorMessage"])
      end

      response_hash["api_response"]["result"]["customerId"]
    end

    def direct_sale_without_token(options)
      check_hash_required_keys(options, [:id, :total_price, :number, :expMonth, :expYear, :cvv])
      client = get_maxipago_client()
      client.use(get_transaction_request)
      response = client.execute({
        merchantId: MP_ID,
        merchantKey: MP_APIKEY,
        command: "sale",
        processor_id: MP_PROCESSOR_ID,
        reference_num: options[:id],
        charge_total: options[:total_price].to_f,
        number:  options[:number],
        exp_month: options[:expMonth],
        exp_year: options[:expYear],
        cvv_number: options[:cvv]
      })

      response_hash = Hash.from_xml(response[:body])
      response_object = OpenStruct.new
      begin
        response_object.body = response[:body]
        response_object.order_id = response_hash["transaction_response"]["orderID"]
        response_object.transaction_id = response_hash["transaction_response"]["transactionID"]
        response_object.success = (response_hash["transaction_response"]["responseCode"] == "0")
      rescue
        raise MaxiPagoInterface::MalformedMaxipagoResponseError, response[:body]
      end

      response_object
    end

    private
      def get_maxipago_client
        Maxipago::Client.new
      end
      def check_hash_required_keys(hash, required_keys)
        required_keys.each do |required_key|
          raise InvalidArgumentsError.new("Required hash key: " + required_key.to_s) unless hash.has_key?(required_key)
        end
      end
      def get_transaction_request
        Maxipago::RequestBuilder::TransactionRequest.new(MP_ID, MP_APIKEY)
      end

      def get_api_request
        Maxipago::RequestBuilder::ApiRequest.new(MP_ID, MP_APIKEY)
      end
  end
end
