# encoding: utf-8

require 'ostruct'
require 'rest-client'
require 'digest/md5'

module PayuInterface

  class << self

    def payment(options)

      signature = generate_signature(options[:referenceCode], options[:value], options[:currency]);
      deviceSessionId = generate_device_session_id(options[:session_id]);
      notifyUrl = get_notifyUrl

      payload = raw_create_billing(options, signature, deviceSessionId, notifyUrl)
      payment = RestClient.post ENV['PAYU_URL_PAYMENT'], payload, headers
      return from_json(payment);

    end

    def get_notifyUrl

      user= ENV['PAYU_USER_ACCESS_API_HQ']
      password= ENV['PAYU_PASSWORD_ACCESS_API_HQ']

      if Rails.env.development?
        return url = "http://f1abddef.ngrok.io/api/v2/payu-send-notification?user=#{user}&password=#{password}"
      elsif Rails.env.staging?
        return url = "http://staging.hotelquando.com.br/api/v2/payu-send-notification?user=#{user}&password=#{password}"
      elsif Rails.env.production?
        return url = "https://www.hotelquando.com.br/api/v2/payu-send-notification?user=#{user}&password=#{password}"
      end
    end

    def get_public_ip
      begin
        return RestClient.get 'https://api.ipify.org';
      rescue Exception => e
        return '0.0.0.0'
      end

    end

    private
      def raw_create_billing(options, signature, deviceSessionId, notifyUrl)

        if options[:dniType] == "PPN"
          options[:dniType] = "PP"
        end


        return {
                 "language" => "es",
                 "command" => "SUBMIT_TRANSACTION",
                 "merchant" => {
                    "apiKey" => ENV['PAYU_API_KEY'],
                    "apiLogin" => ENV['PAYU_API_LOGIN']
                 },
                 "transaction" => {
                    "order" => {
                       "accountId" => ENV['PAYU_ACCOUNT_ID'],
                       "referenceCode" => options[:referenceCode],
                       "description" => options[:description],
                       "language" => "es",
                       "signature" => signature,
                       "notifyUrl" => notifyUrl,
                       "additionalValues" => {
                          "TX_VALUE" => {
                             "value" => options[:value].to_f,
                             "currency" => options[:currency]
                       }
                       },
                       "buyer" => {
                          "fullName" => options[:fullName],
                          "emailAddress" => options[:emailAddress],
                          "contactPhone" => options[:contactPhone],
                          "dniNumber" => options[:dniNumber],
                          "shippingAddress" => {
                             "street1" => options[:user][:street],
                             "city" => options[:user][:city],
                             "country" => "CO"
                          }
                       }
                    },
                    "payer" => {
                       "fullName" => options[:fullName],
                       "emailAddress" => options[:emailAddress],
                       "contactPhone" => options[:contactPhone],
                       "dniNumber" => options[:dniNumber],
                       "dniType" => options[:dniType],
                       "billingAddress" => {
                          "street1" => options[:user][:street],
                          "city" => options[:user][:city],
                          "country" => "CO"
                       }
                    },
                    "creditCard" => {
                       "number" => options[:number],
                       "securityCode" => options[:securityCode],
                       "expirationDate" => options[:expirationDate],
                       "name" => options[:fullName]
                    },
                    "type" => "AUTHORIZATION_AND_CAPTURE",
                    "paymentMethod" => options[:paymentMethod],
                    "paymentCountry" => "CO",
                    "deviceSessionId" => deviceSessionId,
                    "ipAddress" => get_public_ip,
                    "cookie" => options[:cookie],
                    "userAgent" => options[:userAgent]
                 },
                 "test" => false
              }.to_json
      end

      def headers
        return { content_type: 'application/json', accept: 'application/json' }
      end

      def from_json(json)
        return ActiveSupport::JSON.decode(json)
      end

      def generate_signature(reference_code, value, currency)
        s = ENV['PAYU_API_KEY'] + '~' + ENV['PAYU_MERCHANT_ID'] + '~' + reference_code + '~' + value + '~' + currency;
        return Digest::MD5.hexdigest(s)
      end

      def generate_device_session_id(session_id)

        epoch_mirco = Time.now.to_f;
        epoch_full = Time.now.to_i;

        epoch_fraction = epoch_mirco - epoch_full;

        uniq_device_id = session_id + epoch_fraction.to_s;

        return Digest::MD5.hexdigest(uniq_device_id);
      end
  end
end
