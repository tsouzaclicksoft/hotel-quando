require 'ostruct'

class OffersSearch::Hotels < OffersSearch::Base

  validates :checkin_date, presence: true
  validates :checkin_hour, presence: true
  validates :number_of_guests, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :pack_in_hours, presence: true
  validates :room_category, presence: true

  attr_reader :checkin_date, :checkin_hour, :number_of_guests, :latitude, :longitude, :pack_in_hours, :room_category

  def initialize(params)
    super
    @checkin_date = params[:checkin_date]
    @checkin_hour = params[:checkin_hour]
    @number_of_guests = params[:number_of_guests]
    @latitude = params[:latitude]
    @longitude = params[:longitude]
    @pack_in_hours = params[:pack_in_hours]
    @room_category = params[:room_category]
    @search_by = params[:search_by]
  end

  private
  def data_sources
    choice = ChooseOmnibeesOrHoteisnet.choose_based_on_time_now( @params[:pack_in_hours], @params[:check_in_datetime] )
    if @search_by == 'meetingRoom'
      [DataSources::OfferPerBusinessRoom::HQ.new(@params)]
    elsif choice == 1
      [DataSources::MinOfferPricePerHotel::Hotelnet.new(@params), DataSources::MinOfferPricePerHotel::HQ.new(@params)]
    elsif choice == 2
        [DataSources::MinOfferPricePerHotel::Omnibees.new(@params), DataSources::MinOfferPricePerHotel::HQ.new(@params)]
    elsif @params[:is_affiliate] && @params[:room_category] == 2
        [DataSources::OfferPerBusinessRoom::HQ.new(@params)]
    end
  end
end
