require 'ostruct'

class OffersSearch::Base
  include ::ActiveModel::Validations

  SearchResult = Struct.new("SearchResult", :exacts, :similars)

  def initialize(params)
    @params = params
  end

  def results
    return @results if @results
    @results = SearchResult.new([], [])
    get_results_from_datasources
    @results
  end

  def results!
    raise ArgumentError, self.errors.messages.to_json if self.invalid?
    results
  end

  private
  def get_results_from_datasources
    data_sources.each do |data_source|
      @results.similars += data_source.results.similars
      @results.exacts += data_source.results.exacts
    end
  end

  def data_sources
    raise "Abstract method. Implement on children classes."
  end

end
