require 'ostruct'

class OffersSearch::RoomTypes < OffersSearch::Base

  private
  def data_sources
    if hotel.is_omnibees_or_hoteisnet?
      # if pack_in_hours = 12 set client to hoteisnet for 3, 6 or 9 packs set omnibees
      choice = ChooseOmnibeesOrHoteisnet.choose_based_on_time_now( @params[:pack_in_hours], @params[:check_in_datetime] )
      if choice == 1
        if @params[:pack_in_hours] > 12
          [DataSources::MinOfferPerRoomType::Hotelnet.new(@params)]
        else
          [DataSources::MinOfferPerRoomType::Hotelnet.new(@params), DataSources::MinOfferPerRoomType::HQ.new(@params)]
        end
      elsif choice == 2
        [DataSources::MinOfferPerRoomType::Omnibees.new(@params), DataSources::MinOfferPerRoomType::HQ.new(@params)]
      end
    else
      [DataSources::MinOfferPerRoomType::HQ.new(@params)]
    end
  end

  def hotel
    @hotel ||= Hotel.find(@params[:hotel_id])
  end

end
