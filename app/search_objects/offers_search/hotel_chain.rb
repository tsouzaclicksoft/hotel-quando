require 'ostruct'

class OffersSearch::HotelChain < OffersSearch::Base
  validates :checkin_date, presence: true
  validates :checkin_hour, presence: true
  validates :number_of_guests, presence: true
  validates :hotel_ids, presence: true
  validates :pack_in_hours, presence: true
  validates :room_category, presence: true

  attr_reader :checkin_date, :checkin_hour, :number_of_guests, :hotel_ids, :pack_in_hours, :room_category

  def initialize(params)
    super
    @checkin_date = params[:checkin_date]
    @checkin_hour = params[:checkin_hour]
    @number_of_guests = params[:number_of_guests]
    @pack_in_hours = params[:pack_in_hours]
    @room_category = params[:room_category]
  end

  def data_sources
    choice = ChooseOmnibeesOrHoteisnet.choose_based_on_time_now( @params[:pack_in_hours], @params[:check_in_datetime] )
    if @params[:room_category].to_i == 2
      [DataSources::OfferPerBusinessRoomForHotelChain::HQ.new(@params)]
    elsif @params[:room_category].to_i == 3 && choice == 1
      [DataSources::MinOfferPriceForHotelChain::Hotelnet.new(@params), DataSources::MinOfferPriceForHotelChain::HQ.new(@params)]
    elsif @params[:room_category].to_i == 3 && choice == 2
      [DataSources::MinOfferPriceForHotelChain::Omnibees.new(@params), DataSources::MinOfferPriceForHotelChain::HQ.new(@params)]
    elsif choice == 1
      [DataSources::MinOfferPriceForHotelChain::Hotelnet.new(@params), DataSources::MinOfferPriceForHotelChain::HQ.new(@params)]
    elsif choice == 2
      [DataSources::MinOfferPriceForHotelChain::Omnibees.new(@params), DataSources::MinOfferPriceForHotelChain::HQ.new(@params)]
    end
  end
end
