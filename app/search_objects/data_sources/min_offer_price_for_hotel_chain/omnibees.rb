class DataSources::MinOfferPriceForHotelChain::Omnibees < DataSources::MinOfferPriceForHotelChain::Base
  def initialize(params)
    super
  end

  def generate_exacts_results
    begin
      hotels = hotels_search(@params[:hotel_ids]).omnibees
      return [] if hotels.empty?
      xml_availabilities = ::Omnibees::Client.new.get_offers({
        number_of_guests: @params[:number_of_guests],
        check_in_datetime: @params[:check_in_datetime],
        pack_in_hours: @params[:pack_in_hours],
        hotel_codes: hotels.map(&:omnibees_code)
      })
    rescue Exception => ex
      Airbrake.notify(ex)
      return []
    end
    offers = ::Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_availabilities).to_offers(@params)

    min_offer_per_hotel_id = {}

    offers.each do |offer|
      min_offer_per_hotel_id[offer.hotel_id] ||= offer
      if min_offer_per_hotel_id[offer.hotel_id].price > offer.price
        min_offer_per_hotel_id[offer.hotel_id] = offer
      end
    end

    min_offer_per_hotel_id.values.map do |offer|
      { hotel: offer.hotel, min_offer: offer }
    end
  end

  def generate_similar_results
    []
  end
end
