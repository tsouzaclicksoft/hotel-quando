class DataSources::MinOfferPriceForHotelChain::HQ < DataSources::MinOfferPriceForHotelChain::Base

  def generate_exacts_results
    @exact_hotel_ids = []
    offers = Offer.by_number_of_people(@params[:number_of_guests])
                  .where('checkin_timestamp = (?) AND pack_in_hours = (?) AND hotel_id IN (?) AND status = (?)',
                    @params[:check_in_datetime].strftime("%Y-%m-%d %H:%M:%S +0000"),
                    @params[:pack_in_hours],
                    @params[:hotel_ids],
                    Offer::ACCEPTED_STATUS[:available])

    offers_grouped_by_hotel = offers.group_by { |offer| offer.hotel_id }
    minimum_offer_price_per_hotels_id_exact_match_array = offers_grouped_by_hotel.map do |hotel_id, offers|
      offer_with_min_price = (offers.sort_by { |offer| offer.price }).first
      [hotel_id, offer_with_min_price]
    end

    minimum_offer_price_per_hotels_id_exact_match_array.map do |hotel_price_and_offer|
      @exact_hotel_ids << hotel_price_and_offer[0]
      { hotel: Hotel.find(hotel_price_and_offer[0]), min_offer: hotel_price_and_offer[1] }
    end
  end

  def generate_similar_results
    []
  end

end
