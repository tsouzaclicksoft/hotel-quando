class DataSources::OfferPerBusinessRoom::HQ < DataSources::OfferPerBusinessRoom::Base
  
    def generate_exacts_results
      @exact_hotel_ids = []
=begin # code with meeting room and event room working
      offers = offer_class.by_number_of_people(@params[:number_of_guests])
                            .where('checkin_timestamp = (?) AND pack_in_hours = (?) AND hotel_id IN (?) AND status = (?)',
                              @params[:check_in_datetime].strftime("%Y-%m-%d %H:%M:%S +0000"),
                              @params[:pack_in_hours],
                              nearest_hotels.map(&:id),
                              offer_class::ACCEPTED_STATUS[:available])

      if @params[:room_category] == Hotel::ROOM_CATEGORY[:meeting_room_offer]
        minimum_offer_price_per_hotels_id_exact_match_array = offers.group(:meeting_room_id ,:hotel_id, :id).pluck(:hotel_id, "MIN(price)", :meeting_room_id, :id)
        minimum_offer_price_per_hotels_id_exact_match_array.map do |hotel_and_price|
          @exact_hotel_ids << hotel_and_price[0]
          { hotel: hotel_by_id(hotel_and_price[0]), min_price: hotel_and_price[1], business_room: MeetingRoom.find(hotel_and_price[2]), offer_id: hotel_and_price[3] }
        end
      elsif @params[:room_category] == Hotel::ROOM_CATEGORY[:event_room_offer]
        minimum_offer_price_per_hotels_id_exact_match_array = offers.group(:event_room_id ,:hotel_id, :id).pluck(:hotel_id, "MIN(price)", :event_room_id, :id)
        minimum_offer_price_per_hotels_id_exact_match_array.map do |hotel_and_price|
          @exact_hotel_ids << hotel_and_price[0]
          { hotel: hotel_by_id(hotel_and_price[0]), min_price: hotel_and_price[1], business_room: EventRoom.find(hotel_and_price[2]), offer_id: hotel_and_price[3] }
        end 
      end
=end
  
      offers = MeetingRoomOffer.by_number_of_people(@params[:number_of_guests])
                             .where('checkin_timestamp = (?) AND pack_in_hours = (?) AND hotel_id IN (?) AND status = (?)',
                                @params[:check_in_datetime].strftime("%Y-%m-%d %H:%M:%S +0000"),
                                @params[:pack_in_hours],
                                nearest_hotels.map(&:id),
                                MeetingRoomOffer::ACCEPTED_STATUS[:available])
  
      minimum_offer_price_per_hotels_id_exact_match_array = offers.group(:meeting_room_id ,:hotel_id, :id).pluck(:hotel_id, "MIN(price)", :meeting_room_id, :id)
      minimum_offer_price_per_hotels_id_exact_match_array.map do |hotel_and_price|
        @exact_hotel_ids << hotel_and_price[0]
        { hotel: hotel_by_id(hotel_and_price[0]), min_price: hotel_and_price[1], business_room: MeetingRoom.find(hotel_and_price[2]), offer_id: hotel_and_price[3] }
      end
  
  
  
  
    end
  
    def generate_similar_results
      hotel_ids_to_search = nearest_hotels.map(&:id) - @exact_hotel_ids
      return [] if hotel_ids_to_search.empty?
=begin # code with meeting room and event room working
      offers = offer_class.by_number_of_people(@params[:number_of_guests])
                              .where('hotel_id IN (?) AND status = (?)', hotel_ids_to_search, offer_class::ACCEPTED_STATUS[:available])
                              .get_similar_offers_from_pack_in_hours_and_checkin(@params[:pack_in_hours].to_i, @params[:check_in_datetime])

      if @params[:room_category] == Hotel::ROOM_CATEGORY[:meeting_room_offer]
        hotels_ids_with_minimum_offer_price_array_similar_match = offers.group(:meeting_room_id ,:hotel_id, :id).pluck(:hotel_id, "MIN(price)", :meeting_room_id, :id)
        hotels_ids_with_minimum_offer_price_array_similar_match.map do | hotel_and_price |
          { hotel: hotel_by_id(hotel_and_price[0]), min_price: hotel_and_price[1], business_room: MeetingRoom.find(hotel_and_price[2]), offer_id: hotel_and_price[3] }
        end
      elsif @params[:room_category] == Hotel::ROOM_CATEGORY[:event_room_offer]
        hotels_ids_with_minimum_offer_price_array_similar_match = offers.group(:event_room_id ,:hotel_id, :id).pluck(:hotel_id, "MIN(price)", :event_room_id, :id)
        hotels_ids_with_minimum_offer_price_array_similar_match.map do | hotel_and_price |
          { hotel: hotel_by_id(hotel_and_price[0]), min_price: hotel_and_price[1], business_room: EventRoom.find(hotel_and_price[2]), offer_id: hotel_and_price[3] }
        end
      end
=end
  
      offers = MeetingRoomOffer.by_number_of_people(@params[:number_of_guests])
                               .where('hotel_id IN (?) AND status = (?)', hotel_ids_to_search, MeetingRoomOffer::ACCEPTED_STATUS[:available])
                               .get_similar_offers_from_pack_in_hours_and_checkin(@params[:pack_in_hours].to_i, @params[:check_in_datetime])
  
      hotels_ids_with_minimum_offer_price_array_similar_match = offers.group(:meeting_room_id ,:hotel_id, :id).pluck(:hotel_id, "MIN(price)", :meeting_room_id, :id)
      hotels_ids_with_minimum_offer_price_array_similar_match.map do | hotel_and_price |
        { hotel: hotel_by_id(hotel_and_price[0]), min_price: hotel_and_price[1], business_room: MeetingRoom.find(hotel_and_price[2]), offer_id: hotel_and_price[3] }
      end
  
    end
  
=begin
  def offer_class
    Hotel::ROOM_CATEGORY_REVERSED[@params[:room_category]].to_s.classify.constantize
  end
=end
end
  