class DataSources::MinOfferPerRoomType::Hotelnet < DataSources::MinOfferPerRoomType::Base

  protected
  def generate_exacts_results
    begin
      xml_availabilities = ::Hotelnet::Client.new.get_offers({
        number_of_guests: @params[:number_of_guests],
        check_in_datetime: @params[:check_in_datetime],
        pack_in_hours: @params[:pack_in_hours],
        hotel_codes: [hotel.omnibees_code]
      })
      offers = ::Hotelnet::DTO::HotelAvailabilitiesToOffers.parse(xml_availabilities).to_offers(@params)
    rescue Exception => ex
      Airbrake.notify(ex)
      return []
    end

    offers.map do |offer|
      { min_offer: offer, room_type: offer.room_type }
    end
  end

  def generate_similar_results
    []
  end

  def generate_exact_available_offers_by_room_type
    xml_availabilities = ::Hotelnet::Client.new.get_offers({
      number_of_guests: @params[:number_of_guests],
      check_in_datetime: @params[:check_in_datetime],
      pack_in_hours: @params[:pack_in_hours],
      hotel_codes: [hotel.omnibees_code]
    })
    offers = ::Hotelnet::DTO::HotelAvailabilitiesToOffers.parse(xml_availabilities).to_offers(@params)
    offers_grouped_by_room_type = offers.group_by { |offer| offer.room_type_id }
    exact_offers_by_room_type = offers_grouped_by_room_type
  end

  def generate_similar_available_offers_by_room_type
    []
  end
end
