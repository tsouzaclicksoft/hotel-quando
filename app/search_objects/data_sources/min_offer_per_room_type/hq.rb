class DataSources::MinOfferPerRoomType::HQ < DataSources::MinOfferPerRoomType::Base

  protected
  def generate_exacts_results
    offers = hotel.offers.includes(:room_type => [:photos]).by_number_of_people(@params[:number_of_guests])
                         .where('checkin_timestamp = (?) AND pack_in_hours = (?) AND status = (?)',
                           @params[:check_in_datetime].strftime("%Y-%m-%d %H:%M:%S +0000"),
                           @params[:pack_in_hours],
                           Offer::ACCEPTED_STATUS[:available]).includes(:room_type).to_a

    offers_grouped_by_room_type = offers.group_by { |offer| offer.room_type_id }

    exact_offers = offers_grouped_by_room_type.map do |room_type_id, offers|
      offer_with_min_price = (offers.sort_by { |offer| offer.price }).first
      offer_with_min_price
    end

    #it returns offers only with the selected fields
    exact_offers.map do |offer|
      { min_offer: offer, room_type: offer.room_type }
    end
  end

  def generate_similar_results
    offers = hotel.offers.by_number_of_people(@params[:number_of_guests])
                         .where('status = (?)', Offer::ACCEPTED_STATUS[:available])
                         .get_similar_offers_from_pack_in_hours_and_checkin(@params[:pack_in_hours].to_i, @params[:check_in_datetime]).includes(:room_type).to_a

    offers_grouped_by_room_type = offers.group_by { |offer| offer.room_type_id }

    similar_offers = offers_grouped_by_room_type.map do |room_type_id, offers|
      offer_with_min_price = (offers.sort_by { |offer| offer.price }).first
      offer_with_min_price
    end

    #it returns offers only with the selected fields
    similar_offers.map do |offer|
      { min_offer: offer, room_type: offer.room_type }
    end
  end

  def generate_exact_available_offers_by_room_type
    offers = hotel.offers.includes(:room_type => [:photos]).by_number_of_people(@params[:number_of_guests])
                         .where('checkin_timestamp = (?) AND pack_in_hours = (?) AND status = (?)',
                           @params[:check_in_datetime].strftime("%Y-%m-%d %H:%M:%S +0000"),
                           @params[:pack_in_hours],
                           Offer::ACCEPTED_STATUS[:available]).includes(:room_type).to_a

    offers_grouped_by_room_type = offers.group_by { |offer| offer.room_type_id }
    exact_offers_by_room_type = offers_grouped_by_room_type
  end

  def generate_similar_available_offers_by_room_type
    offers = hotel.offers.by_number_of_people(@params[:number_of_guests])
                         .where('status = (?)', Offer::ACCEPTED_STATUS[:available])
                         .get_similar_offers_from_pack_in_hours_and_checkin(@params[:pack_in_hours].to_i, @params[:check_in_datetime]).includes(:room_type).to_a

    offers_grouped_by_room_type = offers.group_by { |offer| offer.room_type_id }
    similar_offers_by_room_type = offers_grouped_by_room_type
  end

end
