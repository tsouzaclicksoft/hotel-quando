class DataSources::MinOfferPerRoomType::Base
  def initialize(params)
    @params = HashWithIndifferentAccess.new(params)
    check_params!
  end

  def results
    return @results if @results
    @results = OpenStruct.new
    fill_results()
    @results
  end

  def fill_similar_results
    @results.similars = generate_similar_results()
  end

  private
  def fill_results
    @results.exacts = generate_exacts_results()
    @results.similars = []
    if @results.exacts.empty?
      @results.similars = generate_similar_results()
    end
    @results.exact_offers_by_room_type = generate_exact_available_offers_by_room_type()
    @results.similar_offers_by_room_type = []
    if @results.exact_offers_by_room_type.empty?
      @results.similar_offers_by_room_type = generate_similar_available_offers_by_room_type()
    end
  end

  def hotel
    @hotel ||= Hotel.find(@params[:hotel_id])
  end

  protected
  def generate_exacts_results
    raise 'Abstract method. Please implement on child class'
  end

  def generate_similar_results
    raise 'Abstract method. Please implement on child class'
  end

  def generate_exact_available_offers_by_room_type
    raise 'Abstract method. Please implement on child class'
  end

  def generate_similar_available_offers_by_room_type
    raise 'Abstract method. Please implement on child class'
  end

  def check_params!
    required_params = [:number_of_guests, :hotel_id, :check_in_datetime, :pack_in_hours]
    unless required_params.all? { |k| @params.has_key?(k) }
      raise "Please check all required params #{required_params.join(',')}"
    end
  end
end
