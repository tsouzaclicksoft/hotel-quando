class DataSources::MinOfferPricePerHotel::HQ < DataSources::MinOfferPricePerHotel::Base

  def generate_exacts_results
    @exact_hotel_ids = []
    offers = Offer.by_number_of_people(@params[:number_of_guests])
                  .where('checkin_timestamp = (?) AND pack_in_hours = (?) AND hotel_id IN (?) AND status = (?)',
                    @params[:check_in_datetime].strftime("%Y-%m-%d %H:%M:%S +0000"),
                    @params[:pack_in_hours],
                    nearest_hotels.map(&:id),
                    Offer::ACCEPTED_STATUS[:available]).includes(hotel: {room_types: :photos}).includes(hotel: :hotel_amenity)


    offers_grouped_by_hotel = offers.group_by { |offer| offer.hotel_id }

    minimum_offer_price_per_hotels_id_exact_match_array = offers_grouped_by_hotel.map do |hotel_id, offers|
      offer_with_min_price = (offers.sort_by { |offer| offer.price }).first
      offer_with_min_price.hotel = hotel_by_id(offer_with_min_price.hotel_id)
      [hotel_id, offer_with_min_price]
    end

    minimum_offer_price_per_hotels_id_exact_match_array.map do |hotel_price_and_offer|
      @exact_hotel_ids << hotel_price_and_offer[0]
      { hotel: hotel_by_id(hotel_price_and_offer[0]), min_offer: hotel_price_and_offer[1] }
    end
  end

  def generate_similar_results
    hotel_ids_to_search = nearest_hotels.map(&:id) - @exact_hotel_ids
    return [] if hotel_ids_to_search.empty?
    return [] if @params[:pack_in_hours] > 12
    offers = Offer.group(:id, :hotel_id).by_number_of_people(@params[:number_of_guests])
                  .where('hotel_id IN (?) AND status = (?)', hotel_ids_to_search, Offer::ACCEPTED_STATUS[:available])
                  .get_similar_offers_from_pack_in_hours_and_checkin(@params[:pack_in_hours].to_i, @params[:check_in_datetime]).includes(hotel: {room_types: :photos}).includes(hotel: :hotel_amenity)

    offers_grouped_by_hotel = offers.group_by { |offer| offer.hotel_id }

    hotels_ids_with_minimum_offer_price_array_similar_match = offers_grouped_by_hotel.map do |hotel_id, offers|
      offer_with_min_price = (offers.sort_by { |offer| offer.price }).first
      [hotel_id, offer_with_min_price]
    end

    hotels_ids_with_minimum_offer_price_array_similar_match.map do | hotel_price_and_offer |
      { hotel: hotel_by_id(hotel_price_and_offer[0]), min_offer: hotel_price_and_offer[1] }
    end
  end

end
