class DataSources::MinOfferPricePerHotel::Base
  def initialize(params)
    @params = HashWithIndifferentAccess.new(params)
    @nearest_hotels_query = NearestHotelsQuery.new(params)
    check_params!
  end

  def results
    return @results if @results
    @results = OpenStruct.new
    fill_results()
    @results
  end

  private
  def fill_results
    @results.exacts = generate_exacts_results()
    @results.similars = generate_similar_results()
  end

  def nearest_hotels
    @nearest_hotels_query.hotel_list
  end

  def hotel_by_id(id)
    @nearest_hotels_query.hotel_by_id(id)
  end

  protected
  def generate_exacts_results
    raise 'Abstract method. Please implement on child class'
  end

  def generate_similar_results
    hotel_ids_to_search = nearest_hotels.map(&:id) - @exact_hotel_ids
    raise 'Abstract method. Please implement on child class'
  end


  def check_params!
    required_params = [:number_of_guests, :latitude, :longitude, :check_in_datetime, :pack_in_hours]
    unless required_params.all? { |k| @params.has_key?(k) }
      raise "Please check all required params #{required_params.join(',')}"
    end
  end
end
