class DataSources::MinOfferPricePerHotel::Hotelnet < DataSources::MinOfferPricePerHotel::Base
  def initialize(params)
    super
    @nearest_hotels_query.only_omnibees!
  end

  def generate_exacts_results
    begin
      return [] if nearest_hotels.empty?
      xml_availabilities = ::Hotelnet::Client.new.get_offers({
        number_of_guests: @params[:number_of_guests],
        check_in_datetime: @params[:check_in_datetime],
        pack_in_hours: @params[:pack_in_hours],
        hotel_codes: nearest_hotels.map(&:omnibees_code)
      })
    rescue Exception => ex
      Airbrake.notify(ex)
      return []
    end


    @params[:hotel_array_to_update] = []
    offers = ::Hotelnet::DTO::HotelAvailabilitiesToOffers.parse(xml_availabilities).to_offers(@params)
    
    UpdateHotelTaxFromIntegrationsWorker.perform_async(@params[:hotel_array_to_update].uniq!) unless @params[:hotel_array_to_update].blank?
    min_offer_per_hotel_id = {}

    offers.each do |offer|
      min_offer_per_hotel_id[offer.hotel_id] ||= offer
      if min_offer_per_hotel_id[offer.hotel_id].price > offer.price
        min_offer_per_hotel_id[offer.hotel_id] = offer
      end
    end

    min_offer_per_hotel_id.values.map do |offer|
      { hotel: offer.hotel, min_offer: offer }
    end
  end

  def generate_similar_results
    []
  end
end
