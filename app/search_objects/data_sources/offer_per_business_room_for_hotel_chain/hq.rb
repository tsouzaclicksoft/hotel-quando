class DataSources::OfferPerBusinessRoomForHotelChain::HQ < DataSources::OfferPerBusinessRoomForHotelChain::Base

  def generate_exacts_results
    @exact_hotel_ids = []
    offers = MeetingRoomOffer.by_number_of_people(@params[:number_of_guests])
                           .where('checkin_timestamp = (?) AND pack_in_hours = (?) AND hotel_id IN (?) AND status = (?)',
                              @params[:check_in_datetime].strftime("%Y-%m-%d %H:%M:%S +0000"),
                              @params[:pack_in_hours],
                              @params[:hotel_ids],
                              MeetingRoomOffer::ACCEPTED_STATUS[:available])
    minimum_offer_price_per_hotels_id_exact_match_array = offers.group(:meeting_room_id ,:hotel_id, :id).pluck(:hotel_id, "MIN(price)", :meeting_room_id, :id)
    minimum_offer_price_per_hotels_id_exact_match_array.map do |hotel_and_price|
      @exact_hotel_ids << hotel_and_price[0]
      { hotel: hotel_and_price[0], min_price: hotel_and_price[1], business_room: MeetingRoom.find(hotel_and_price[2]), offer_id: hotel_and_price[3] }
    end
  end

  def generate_similar_results
    []
  end
end
