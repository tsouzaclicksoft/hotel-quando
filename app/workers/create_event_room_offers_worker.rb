# coding: utf-8

class CreateEventRoomOffersWorker
  include Sidekiq::Worker

  sidekiq_options retry: true
  sidekiq_options queue: "create_event_room_offers"

  def perform(log_id, hotel_id, params_hash)
    @log_id = log_id
    @hotel = Hotel.find(hotel_id)
    @selected_pricing_policy = @hotel.business_room_pricing_policies.includes(:business_rooms, :business_room_prices).find(params_hash['pricing_policy_id'])
    @formated_params = get_formated_params(params_hash)
    @event_rooms_array = get_event_rooms_array
    offer_log = EventRoomOffersCreatorLog.find(log_id)
    offer_log.logged_room_types_and_prices_used = @event_rooms_array.to_json
    offer_log.save!
    update_query = generate_update_query()
    insert_query = generate_insert_query()
    execute_query(update_query)
    execute_query(insert_query)
    register_log(log_id)
  end

  def get_formated_params(raw_params)
    formated_params = {
      :initial_date => raw_params['initial_date'].to_date,
      :final_date => raw_params['final_date'].to_date,
      :selected_week_days_hash => {},
      :selected_hours_per_pack => {},
      :rooms_ids => raw_params["event_rooms_ids"].map(&:to_i)
    }

    raw_params["selected_days_hash"].keys.each do |day_code|
      formated_params[:selected_week_days_hash][day_code.to_i] = true
    end

    formated_params[:selected_week_days_codes_array] = []

    raw_params["selected_days_hash"].keys.each do |day_code|
      formated_params[:selected_week_days_codes_array] << day_code.to_i
    end

    raw_params["pack"].each do |pack, hours|
      formated_params[:selected_hours_per_pack][pack.to_i] = hours.map(&:to_i)
    end

    formated_params
  end

  def get_event_rooms_array
    event_rooms_array = []
    @selected_pricing_policy.business_room_prices.each do |business_room_price|
      new_event_room = {
        :id => business_room_price.business_room_id,
        :maximum_capacity => business_room_price.business_room.maximum_capacity,
        #:rooms => Room.by_room_type_id(business_room_price.room_type_id).where('id in (?)', @formated_params[:rooms_ids]).pluck(:id),
        :price_per_pack => {
          2 => business_room_price.pack_price_2h,
          4 => business_room_price.pack_price_4h,
          8 => business_room_price.pack_price_8h
        },
        :no_show_penalty_per_pack => {
          2 => business_room_price.pack_price_2h*((business_room_price.penalty_for_2h_pack_in_percentage)/100),
          4 => business_room_price.pack_price_4h*((business_room_price.penalty_for_4h_pack_in_percentage)/100),
          8 => business_room_price.pack_price_8h*((business_room_price.penalty_for_8h_pack_in_percentage)/100)
        }
      } 

      EventRoomOffer::ACCEPTED_LENGTH_OF_PACKS.each do |pack_size|
        new_event_room[:price_per_pack].delete(pack_size) if new_event_room[:price_per_pack][pack_size] == 0
        new_event_room[:no_show_penalty_per_pack].delete(pack_size) if new_event_room[:no_show_penalty_per_pack][pack_size] == 0
      end
      #next if new_event_room[:rooms].length == 0
      event_rooms_array << new_event_room
    end
    event_rooms_array
  end

  def generate_update_query
    update_query = ""
    threads = []
    @event_rooms_array.each do |event_room_info_hash|
      @formated_params[:selected_hours_per_pack].each do | pack_size, checkin_hours|
        threads << Thread.new do
          update_query_for_available_and_suspended_status = ""
          update_query_for_ghost_status = ""
          update_query_for_reserved_status = ""
          update_query_for_removed_by_hotel_status = ""

          initial_date = @formated_params[:initial_date].strftime("%Y-%m-%d 00:00:00")
          final_date = @formated_params[:final_date].strftime("%Y-%m-%d 23:59:59")
          unless event_room_info_hash[:price_per_pack][pack_size] == 0 || event_room_info_hash[:price_per_pack][pack_size].to_s == ''
            default_set_values = {
              price: event_room_info_hash[:price_per_pack][pack_size],
              no_show_value: event_room_info_hash[:no_show_penalty_per_pack][pack_size],
              log_id: @log_id
            }

            update_query_for_available_and_suspended_status = %Q{
              #{update_query_for_available_and_suspended_status}
              #{update_query_for_offers_with_status(default_set_values, [EventRoomOffer::ACCEPTED_STATUS[:available], EventRoomOffer::ACCEPTED_STATUS[:suspended]], pack_size, event_room_info_hash, checkin_hours)}
            }
            update_query_for_ghost_status = %Q{
              #{update_query_for_ghost_status}
              #{update_query_for_offers_with_status(default_set_values.merge(status: EventRoomOffer::ACCEPTED_STATUS[:suspended]), [EventRoomOffer::ACCEPTED_STATUS[:ghost]], pack_size, event_room_info_hash, checkin_hours)}
            }
            update_query_for_removed_by_hotel_status = %Q{
              #{update_query_for_removed_by_hotel_status}
              #{update_query_for_offers_with_status(default_set_values.merge(status: EventRoomOffer::ACCEPTED_STATUS[:available]), [EventRoomOffer::ACCEPTED_STATUS[:removed_by_hotel]], pack_size, event_room_info_hash, checkin_hours)}
            }
            update_query_for_reserved_status = %Q{
              #{update_query_for_reserved_status}
              #{update_query_for_offers_with_status({log_id: @log_id}, [EventRoomOffer::ACCEPTED_STATUS[:reserved]], pack_size, event_room_info_hash, checkin_hours)}
            }


            Thread.current[:update_query_for_available_and_suspended_status] = update_query_for_available_and_suspended_status
            Thread.current[:update_query_for_ghost_status] = update_query_for_ghost_status
            Thread.current[:update_query_for_reserved_status] = update_query_for_reserved_status
            Thread.current[:update_query_for_removed_by_hotel_status] = update_query_for_removed_by_hotel_status
          end #of unless
        end #of Tread.new
      end #of @formated_params.each
    end #of room_types_array.each

    threads.each do |t|
      t.join
      update_query += "#{t[:update_query_for_removed_by_hotel_status]}#{t[:update_query_for_reserved_status]}#{t[:update_query_for_available_and_suspended_status]}#{t[:update_query_for_ghost_status]}"
    end

    update_query
  end

  def update_query_for_offers_with_status(update_set_values, status_array, pack_size, event_room_info_hash, checkin_hours)
    formated_initial_date = @formated_params[:initial_date].strftime("%Y-%m-%d %H:%M:%S")
    formated_final_date = @formated_params[:final_date].strftime("%Y-%m-%d 23:59:59")
    %Q{
      UPDATE event_room_offers
      SET
        #{update_set_values.map { |k,v| "#{k}='#{v}'"}.join(",")}
      WHERE
        (checkin_timestamp BETWEEN '#{formated_initial_date}' AND '#{formated_final_date}') AND
        (EXTRACT(HOUR FROM checkin_timestamp)) IN (#{checkin_hours.join(',')}) AND
        (checkin_timestamp_day_of_week IN (#{@formated_params[:selected_week_days_codes_array].join(",")})) AND
        (pack_in_hours='#{pack_size}') AND
        (event_room_id='#{event_room_info_hash[:id]}') AND
        (status IN (#{status_array.join(",")}));
    }
  end

  def generate_insert_query
    days_in_range = @formated_params[:final_date] - @formated_params[:initial_date]
    first_date_string = @formated_params[:initial_date].strftime("%Y-%m-%d")
    valid_rooms_ids = EventRoom.where("id IN (?) AND hotel_id = ?", @formated_params[:rooms_ids], @hotel.id).pluck(:id)
    return false if valid_rooms_ids.empty?
    # FOR COMPATIBILITY WITH OLDER RELEASES checkin_timestamp_day_of_week is from 1 TO 7 (ruby cday)
    # {1 => 'monday', 2 => 'tuesday', 3 => 'wednesday', 4 => 'thursday', 5 => 'friday', 6 => 'saturday', 7 => 'sunday'}
    # in postgres extract(dow from DATE) the configuration is (0 - 6; Sunday is 0)

    insert_query = %Q{
    INSERT INTO event_room_offers (
      checkin_timestamp,
      checkin_timestamp_day_of_week,
      checkout_timestamp,
      pack_in_hours,
      event_room_id,
      price,
      status,
      event_room_maximum_capacity,
      no_show_value,
      hotel_id,
      log_id)

      SELECT
        "selected_days"."date" + ("selected_hours_per_pack"."hour" || ' hour')::INTERVAL as checkin_timestamp,
        CASE
          WHEN extract(dow from "selected_days"."date") != 0 THEN extract(dow from "selected_days"."date")
          WHEN extract(dow from "selected_days"."date") = 0 THEN 7
        END as checkin_timestamp_day_of_week,
        "selected_days"."date" + ("selected_hours_per_pack"."hour" || ' hour')::INTERVAL + ("selected_hours_per_pack"."pack_size" || ' hour')::INTERVAL as checkout_timestamp,
        "selected_hours_per_pack"."pack_size" as pack_in_hours,
        "selected_rooms"."id" as event_room_id,
        CASE
          WHEN "selected_hours_per_pack"."pack_size" = 2 THEN "business_room_prices"."pack_price_2h"
          WHEN "selected_hours_per_pack"."pack_size" = 4 THEN "business_room_prices"."pack_price_4h"
          WHEN "selected_hours_per_pack"."pack_size" = 8 THEN "business_room_prices"."pack_price_8h"
        END as price,
        #{EventRoomOffer::ACCEPTED_STATUS[:available]} as status,
        "business_rooms"."maximum_capacity" as event_room_maximum_capacity,
        CASE
          WHEN "selected_hours_per_pack"."pack_size" = 2 THEN "business_room_prices"."pack_price_2h"
          WHEN "selected_hours_per_pack"."pack_size" = 4 THEN "business_room_prices"."pack_price_4h"
          WHEN "selected_hours_per_pack"."pack_size" = 8 THEN "business_room_prices"."pack_price_8h"
        END as no_show_value,
        #{@hotel.id} as hotel_id,
        #{@log_id} as log_id
      FROM
        (
        SELECT
          (CAST('#{first_date_string}' AS DATE) + i) as date
        FROM
          generate_series(0, #{days_in_range}, 1) i
        WHERE
          CASE
            WHEN extract(dow from (CAST('#{first_date_string}' AS DATE) + i)) != 0 THEN extract(dow from (CAST('#{first_date_string}' AS DATE) + i))
            WHEN extract(dow from (CAST('#{first_date_string}' AS DATE) + i)) = 0 THEN 7
          END IN (#{@formated_params[:selected_week_days_codes_array].join(",")})
        ) as selected_days,
        (
        SELECT
          i as hour,
          j as pack_size
        FROM
          generate_series(0, 24) i,
          unnest(ARRAY[#{EventRoomOffer::ACCEPTED_LENGTH_OF_PACKS.join(",")}]) j
        WHERE
    }

    @formated_params[:selected_hours_per_pack].each do | pack_size, checkin_hours|
      insert_query = "#{insert_query} (j = #{pack_size} AND i IN (#{checkin_hours.join(",")})) OR"
    end
    insert_query.chop! # removing last or
    insert_query.chop!
    insert_query += %Q{
        ) as selected_hours_per_pack,
        (
        SELECT id
        FROM business_rooms
        WHERE id IN (#{valid_rooms_ids.join(",")})
        ) as selected_rooms
      INNER JOIN business_room_prices ON
        "business_room_prices"."business_room_id" = "selected_rooms"."id" AND
        "business_room_prices"."business_room_pricing_policy_id" = #{@selected_pricing_policy.id}
      INNER JOIN business_rooms ON "business_rooms"."id" = "selected_rooms"."id"
      WHERE
        CASE
          WHEN "selected_hours_per_pack"."pack_size" = 2 THEN "business_room_prices"."pack_price_2h"
          WHEN "selected_hours_per_pack"."pack_size" = 4 THEN "business_room_prices"."pack_price_4h"
          WHEN "selected_hours_per_pack"."pack_size" = 8 THEN "business_room_prices"."pack_price_8h"
        END != 0
    }
    insert_query
  end

  def write_temp_file(string)
    File.open("#{Rails.root}/tmp/queries.txt", 'w') do |file|
      file.write(string)
    end
  end

  def register_log(log_id)
    register_hash = EventRoomOffer.where("log_id=(?)", log_id)
                         .select("count(*) as quantity, status")
                         .group("status")
                         .inject({}) do |hash, element|
                            hash[EventRoomOffer::ACCEPTED_STATUS_REVERSED[element.status]] = element.quantity
                            hash
                         end
    (register_log = EventRoomOffersCreatorLog.where("id=(?)", log_id).first).result = register_hash
    register_log.save
  end

  private
    def execute_query(query)
      ActiveRecord::Base.connection.execute(query)
    end
end
