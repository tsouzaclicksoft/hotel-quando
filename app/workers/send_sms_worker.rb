# coding: utf-8

class SendSmsWorker
  include Sidekiq::Worker

  sidekiq_options retry: 1
  sidekiq_options queue: "send_sms"

  def perform(booking_id, type)
    require "uri"
    require "net/http"

    begin
      @booking = Booking.find(booking_id)
      @hotel_address = @booking.hotel.address.split('-').first.strip
      @phone_number = @booking.phone_number.gsub(/\D/, '')
      if @booking.user.locale == "pt_br" && @phone_number.length < 12
        @phone_number = "55" + @phone_number
      end
      
      @payload = { 
        "from" => "HotelQuando", 
        "to" => @phone_number, 
        "text" => text(type)
      }

      # will send info to Sms only if production environment is running
      # to test Sms integration in development, please set rack_env=production on ENV vars 
      # or comment this if condition to put the commands out to run in staging or test
      logger.info(ENV["RACK_ENV"])
      Logger.new("#{Rails.root}/log/my.log").info(ENV["RACK_ENV"]) 

      if ENV["RACK_ENV"] == 'production'
        response = post
        logger.info(response)
        Logger.new("#{Rails.root}/log/my.log").info(response) 
      end
    rescue => ex
      logger.info(ex)
      Logger.new("#{Rails.root}/log/my.log").info(ex) 
    end
  end

  private

    def post
      uri = URI('https://api.infobip.com/sms/1/text/single')
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json', 'Accept' => 'application/json', 'Authorization' => 'Basic aG90ZWxxdWFuZG86SFFpYjIxMTEyMDE2'})
      response = http.request(request, @payload.to_json)
      res = JSON.parse(response.body)
      res
    end

    def text(type)
      I18n.locale = @booking.user.locale
      checkin_today_or_tomorrow = @booking.offer.checkin_timestamp.hour == 0 ? I18n.t(:tomorrow) : I18n.t(:today)
      checkout_today_or_tomorrow = @booking.offer.checkout_timestamp.hour == 0 ? I18n.t(:tomorrow) : I18n.t(:today)
      hq_or_white_mark = @booking.created_by_affiliate ? I18n.t(:by_hours_booking) : "HotelQuando"
      booking_hq = @booking.created_by_affiliate ? I18n.t(:by_hours_booking) : I18n.t(:booking_hq)
      return I18n.t(type,
        hq_or_white_mark: hq_or_white_mark,
        booking_hq: booking_hq,
        hotel_name: @booking.hotel.name,
        hotel_address: @hotel_address,
        checkin: I18n.l(@booking.offer.checkin_timestamp),
        checkout: I18n.l(@booking.offer.checkout_timestamp),
        checkin_today_or_tomorrow: checkin_today_or_tomorrow,
        checkout_today_or_tomorrow: checkout_today_or_tomorrow,
        checkin_hour: @booking.offer.checkin_timestamp.strftime('%H:%M'),
        checkout_hour: @booking.offer.checkout_timestamp.strftime('%H:%M')
      )
    end

end
