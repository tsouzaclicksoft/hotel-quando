# coding: utf-8

class ProcessAllOffersRemovalWorker
  include Sidekiq::Worker
  sidekiq_options retry: true
  sidekiq_options queue: "process_all_offers_removal"

  # Receiving number of removed offers and number of unchanged offer in parameters, because it is
  # already being calculated in the offer removal intermediate step view
  def perform(log_id, hotel_id, number_of_removed_offers = -1, number_of_unchanged_offers = -1)

    if number_of_removed_offers == -1
      calculate_number_of_removed_offers = true
    else
      calculate_number_of_removed_offers = false
    end

    if number_of_unchanged_offers == -1
      calculate_number_of_unchanged_offers = true
    else
      calculate_number_of_unchanged_offers = false
    end

    offers_to_remove = Offer.where("hotel_id = (?) AND status = #{Offer::ACCEPTED_STATUS[:available]}", hotel_id)
    offers_to_update_to_ghost_status = Offer.where("hotel_id = (?) AND status = #{Offer::ACCEPTED_STATUS[:suspended]}", hotel_id)
    if calculate_number_of_removed_offers
      number_of_removed_offers += offers_to_remove.count + offers_to_update_to_ghost_status.count
    end
    if calculate_number_of_unchanged_offers
      number_of_unchanged_offers += Offer.where("hotel_id = (?) AND status = #{Offer::ACCEPTED_STATUS[:reserved]}", hotel_id, ).count
    end
    offers_to_update_to_ghost_status.update_all("status = #{Offer::ACCEPTED_STATUS[:ghost]}")
    offers_to_remove.update_all(status: Offer::ACCEPTED_STATUS[:removed_by_hotel].to_s)

    log = RoomOffersCreatorLog.find(log_id)
    log.result = {removed: number_of_removed_offers, unchanged: number_of_unchanged_offers}
    log.save
  end
end
