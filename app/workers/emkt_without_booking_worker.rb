# coding: utf-8

class EmktWithoutBookingWorker
  include Sidekiq::Worker

  sidekiq_options retry: true
  sidekiq_options queue: "emkt"

  def perform
    date = DateTime.now
    hour = (date.in_time_zone 'America/Sao_Paulo').hour

    Emkt.where(emkt_type: 2, hour_send: hour, activate: Emkt::EMKT_ACTIVATE[:yes]).order(:order_send).each do |emkt|
      unless emkt.city.blank?
        User.where('city = ? AND emkt_flux_type = ? AND emkt_flux = ?', emkt.city, User::EMKT_FLUX_TYPES[:without_bookings], emkt.order_send).each do |user|
          next if emkt.day_of_week_send != Date.current.wday
          news = Newsletter.where(email: user.email).first
          begin
            EmktMailer.emkt(user, emkt).deliver
            user.emkt_flux = (emkt.order_send+1)
            unless news.nil?
              news.emkt_flux = (emkt.order_send+1)
              news.save(:validate => false)
              news.update(emkt_flux: 2) if validate_news_emkt_flux(news)          
            end
          rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => ex
            Rails.logger.error(ex)
            Airbrake.notify(ex)
          end
          user.save(:validate => false)
          user.update(emkt_flux: 2) if validate_user_emkt_flux(user)
        end
        Newsletter.where('city = ? AND emkt_flux_type = ? AND emkt_flux = ?', emkt.city, User::EMKT_FLUX_TYPES[:without_bookings], emkt.order_send).each do |user|
          next if emkt.day_of_week_send != Date.current.wday
          news = Newsletter.where(email: user.email).first
          begin
            EmktMailer.emkt(user, emkt).deliver
            user.emkt_flux = (emkt.order_send+1)
            unless news.nil?
              news.emkt_flux = (emkt.order_send+1)
              news.save(:validate => false)
              news.update(emkt_flux: 2) if validate_news_emkt_flux(news)
            end
          rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => ex
            Rails.logger.error(ex)
            Airbrake.notify(ex)
          end
          user.save(:validate => false)
          user.update(emkt_flux: 2) if validate_user_emkt_flux(user)
        end
      else
        User.where('country = ? AND emkt_flux_type = ? AND emkt_flux = ?', emkt.country, User::EMKT_FLUX_TYPES[:without_bookings], emkt.order_send).each do |user|
          next if emkt.day_of_week_send != Date.current.wday
          news = Newsletter.where(email: user.email).first
          begin
            EmktMailer.emkt(user, emkt).deliver
            user.emkt_flux = (emkt.order_send+1)
            unless news.nil?
              news.emkt_flux = (emkt.order_send+1)
              news.save(:validate => false)
              news.update(emkt_flux: 2) if validate_news_emkt_flux(news)
            end
          rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => ex
            Rails.logger.error(ex)
            Airbrake.notify(ex)
          end
          user.save(:validate => false)
          user.update(emkt_flux: 2) if validate_user_emkt_flux(user)
        end

        Newsletter.where('city = ? AND emkt_flux_type = ? AND emkt_flux = ?', emkt.city, User::EMKT_FLUX_TYPES[:without_bookings], emkt.order_send).each do |user|
          next if emkt.day_of_week_send != Date.current.wday
          news = Newsletter.where(email: user.email).first
          begin
            EmktMailer.emkt(user, emkt).deliver
            user.emkt_flux = (emkt.order_send+1)
            unless news.nil?
              news.emkt_flux = (emkt.order_send+1)
              news.save(:validate => false)
              news.update(emkt_flux: 2) if validate_news_emkt_flux(news)
            end
          rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => ex
            Rails.logger.error(ex)
            Airbrake.notify(ex)
          end
          user.save(:validate => false)
          user.update(emkt_flux: 2) if validate_user_emkt_flux(user)
        end
      end
    end
  end

  def validate_user_emkt_flux(user)
    !(Emkt.where("order_send = ? AND city = ?", user.emkt_flux, user.city ).exists?)
  end

  def validate_news_emkt_flux(news)
    !(Emkt.where("order_send = ? AND city = ?", news.emkt_flux, news.city.split(',')[0]).exists?)
  end
end
