# coding: utf-8

class UpdateHotelTaxFromIntegrationsWorker
  include Sidekiq::Worker

  sidekiq_options retry: 1
  sidekiq_options queue: "update_hotel_tax_from_integrations"

  def perform(hotels_to_update)
    begin
      hotels_to_update.each{|e| Hotel.find(e[0]).update(service_tax: e[1])}
    rescue => ex
      logger.info(ex)
      Logger.new("#{Rails.root}/log/my.log").info(ex) 
    end
  end

  private

end