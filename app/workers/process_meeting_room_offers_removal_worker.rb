# coding: utf-8

class ProcessMeetingRoomOffersRemovalWorker
  include Sidekiq::Worker
  include Hotel::CheckinDatesHelper
  sidekiq_options retry: true
  sidekiq_options queue: "process_meeting_room_offers_removal"

  # Receiving number of removed offers and number of unchanged offer in parameters, because it is
  # already being calculated in the offer removal intermediate step view
  def perform(log_id, hotel_id, params_hash, number_of_removed_offers = -1, number_of_unchanged_offers = -1)
    checkin_dates = prepare_checkin_dates_from_params(params_hash)
    checkin_dates_with_time = []
    checkin_dates.each do | date |
      checkin_dates_with_time << date + 0.hours
    end

    if number_of_removed_offers == -1
      calculate_number_of_removed_offers = true
    else
      calculate_number_of_removed_offers = false
    end

    if number_of_unchanged_offers == -1
      calculate_number_of_unchanged_offers = true
    else
      calculate_number_of_unchanged_offers = false
    end

    checkin_dates_with_time.each do | checkin_date_with_time |
      offers_to_remove = MeetingRoomOffer.where("hotel_id = (?) AND status = #{MeetingRoomOffer::ACCEPTED_STATUS[:available]} AND meeting_room_id IN (?) AND pack_in_hours IN (?) AND (checkin_timestamp BETWEEN (?) AND (?))",\
                                      hotel_id, params_hash['meeting_rooms_ids'], params_hash['selected_pack_lengths'], checkin_date_with_time.strftime("%Y-%m-%d %H:%M:%S"), (checkin_date_with_time + 1.day - 1.second).strftime("%Y-%m-%d %H:%M:%S"))
      offers_to_update_to_ghost_status = MeetingRoomOffer.where("hotel_id = (?) AND status = #{MeetingRoomOffer::ACCEPTED_STATUS[:suspended]} AND meeting_room_id IN (?) AND pack_in_hours IN (?) AND (checkin_timestamp BETWEEN (?) AND (?))",\
                                                      hotel_id, params_hash['meeting_rooms_ids'], params_hash['selected_pack_lengths'], checkin_date_with_time.strftime("%Y-%m-%d %H:%M:%S"), (checkin_date_with_time + 1.day - 1.second).strftime("%Y-%m-%d %H:%M:%S"))
      if calculate_number_of_removed_offers
        number_of_removed_offers += offers_to_remove.count + offers_to_update_to_ghost_status.count
      end
      if calculate_number_of_unchanged_offers
        number_of_unchanged_offers += MeetingRoomOffer.where("hotel_id = (?) AND status = #{MeetingRoomOffer::ACCEPTED_STATUS[:reserved]} AND meeting_room_id IN (?) AND pack_in_hours IN (?) AND (checkin_timestamp BETWEEN (?) AND (?))",\
                                                  hotel_id, params_hash['meeting_rooms_ids'], params_hash['selected_pack_lengths'], checkin_date_with_time.strftime("%Y-%m-%d %H:%M:%S"), (checkin_date_with_time + 1.day - 1.second).strftime("%Y-%m-%d %H:%M:%S"))\
                                                 .count
      end
      offers_to_update_to_ghost_status.update_all("status = #{MeetingRoomOffer::ACCEPTED_STATUS[:ghost]}")
      offers_to_remove.update_all(status: MeetingRoomOffer::ACCEPTED_STATUS[:removed_by_hotel].to_s)
    end
    log = OffersCreatorLog.find(log_id)
    log.result = {removed: number_of_removed_offers, unchanged: number_of_unchanged_offers}
    log.save
  end
end
