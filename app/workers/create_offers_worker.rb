# coding: utf-8

class CreateOffersWorker
  include Sidekiq::Worker

  sidekiq_options retry: true
  sidekiq_options queue: "create_offers"

  def perform(log_id, hotel_id, params_hash)
    @log_id = log_id
    @hotel = Hotel.find(hotel_id)
    @selected_pricing_policy = @hotel.pricing_policies.includes(:room_types, :room_type_pricing_policies).find(params_hash['pricing_policy_id'])
    @formated_params = get_formated_params(params_hash)
    @room_types_array = get_room_types_array
    offer_log = RoomOffersCreatorLog.find(log_id)
    offer_log.logged_room_types_and_prices_used = @room_types_array.to_json
    offer_log.save!
    update_query = generate_update_query()
    insert_query = generate_insert_query()
    execute_query(update_query)
    execute_query(insert_query)
    register_log(log_id)
  end

  def get_formated_params(raw_params)
    formated_params = {
      :initial_date => raw_params['initial_date'].to_date,
      :final_date => raw_params['final_date'].to_date,
      :selected_week_days_hash => {},
      :selected_hours_per_pack => {},
      :rooms_ids => raw_params["rooms_ids"].map(&:to_i)
    }

    raw_params["selected_days_hash"].keys.each do |day_code|
      formated_params[:selected_week_days_hash][day_code.to_i] = true
    end

    formated_params[:selected_week_days_codes_array] = []

    raw_params["selected_days_hash"].keys.each do |day_code|
      formated_params[:selected_week_days_codes_array] << day_code.to_i
    end

    raw_params["pack"].each do |pack, hours|
      formated_params[:selected_hours_per_pack][pack.to_i] = hours.map(&:to_i)
    end

    formated_params
  end

  def get_room_types_array
    room_types_array = []
    @selected_pricing_policy.room_type_pricing_policies.each do |room_type_pricing_policy|
      new_room_type = {
        :id => room_type_pricing_policy.room_type_id,
        :extra_price_per_person => {
          3 => room_type_pricing_policy.extra_price_per_person_for_pack_price_3h,
          6 => room_type_pricing_policy.extra_price_per_person_for_pack_price_6h,
          9 => room_type_pricing_policy.extra_price_per_person_for_pack_price_9h,
          12 => room_type_pricing_policy.extra_price_per_person_for_pack_price_12h
        },
        :initial_capacity => room_type_pricing_policy.room_type.initial_capacity,
        :maximum_capacity => room_type_pricing_policy.room_type.maximum_capacity,
        :rooms => Room.by_room_type_id(room_type_pricing_policy.room_type_id).where('id in (?)', @formated_params[:rooms_ids]).pluck(:id),
        :price_per_pack => {
          3 => room_type_pricing_policy.pack_price_3h,
          6 => room_type_pricing_policy.pack_price_6h,
          9 => room_type_pricing_policy.pack_price_9h,
          12 => room_type_pricing_policy.pack_price_12h
        },
        :no_show_penalty_per_pack => {
          3 => room_type_pricing_policy.pack_price_3h*((room_type_pricing_policy.penalty_for_3h_pack_in_percentage)/100),
          6 => room_type_pricing_policy.pack_price_6h*((room_type_pricing_policy.penalty_for_6h_pack_in_percentage)/100),
          9 => room_type_pricing_policy.pack_price_9h*((room_type_pricing_policy.penalty_for_9h_pack_in_percentage)/100),
          12 => room_type_pricing_policy.pack_price_12h*((room_type_pricing_policy.penalty_for_12h_pack_in_percentage)/100)
        }
      } 

      Offer::ACCEPTED_LENGTH_OF_PACKS.each do |pack_size|
        new_room_type[:price_per_pack].delete(pack_size) if new_room_type[:price_per_pack][pack_size] == 0
        new_room_type[:no_show_penalty_per_pack].delete(pack_size) if new_room_type[:no_show_penalty_per_pack][pack_size] == 0
      end
      next if new_room_type[:rooms].length == 0
      room_types_array << new_room_type
    end
    room_types_array
  end

  def generate_update_query
    update_query = ""
    threads = []
    @room_types_array.each do |room_type_info_hash|
      @formated_params[:selected_hours_per_pack].each do | pack_size, checkin_hours|
        threads << Thread.new do
          update_query_for_available_and_suspended_status = ""
          update_query_for_ghost_status = ""
          update_query_for_reserved_status = ""
          update_query_for_removed_by_hotel_status = ""

          initial_date = @formated_params[:initial_date].strftime("%Y-%m-%d 00:00:00")
          final_date = @formated_params[:final_date].strftime("%Y-%m-%d 23:59:59")
          unless room_type_info_hash[:price_per_pack][pack_size] == 0 || room_type_info_hash[:price_per_pack][pack_size].to_s == ''
            default_set_values = {
              price: room_type_info_hash[:price_per_pack][pack_size],
              extra_price_per_person: room_type_info_hash[:extra_price_per_person][pack_size],
              no_show_value: room_type_info_hash[:no_show_penalty_per_pack][pack_size],
              log_id: @log_id
            }

            update_query_for_available_and_suspended_status = %Q{
              #{update_query_for_available_and_suspended_status}
              #{update_query_for_offers_with_status(default_set_values, [Offer::ACCEPTED_STATUS[:available], Offer::ACCEPTED_STATUS[:suspended]], pack_size, room_type_info_hash, checkin_hours)}
            }
            update_query_for_ghost_status = %Q{
              #{update_query_for_ghost_status}
              #{update_query_for_offers_with_status(default_set_values.merge(status: Offer::ACCEPTED_STATUS[:suspended]), [Offer::ACCEPTED_STATUS[:ghost]], pack_size, room_type_info_hash, checkin_hours)}
            }
            update_query_for_removed_by_hotel_status = %Q{
              #{update_query_for_removed_by_hotel_status}
              #{update_query_for_offers_with_status(default_set_values.merge(status: Offer::ACCEPTED_STATUS[:available]), [Offer::ACCEPTED_STATUS[:removed_by_hotel]], pack_size, room_type_info_hash, checkin_hours)}
            }
            update_query_for_reserved_status = %Q{
              #{update_query_for_reserved_status}
              #{update_query_for_offers_with_status({log_id: @log_id}, [Offer::ACCEPTED_STATUS[:reserved]], pack_size, room_type_info_hash, checkin_hours)}
            }


            Thread.current[:update_query_for_available_and_suspended_status] = update_query_for_available_and_suspended_status
            Thread.current[:update_query_for_ghost_status] = update_query_for_ghost_status
            Thread.current[:update_query_for_reserved_status] = update_query_for_reserved_status
            Thread.current[:update_query_for_removed_by_hotel_status] = update_query_for_removed_by_hotel_status
          end #of unless
        end #of Tread.new
      end #of @formated_params.each
    end #of room_types_array.each

    threads.each do |t|
      t.join
      update_query += "#{t[:update_query_for_removed_by_hotel_status]}#{t[:update_query_for_reserved_status]}#{t[:update_query_for_available_and_suspended_status]}#{t[:update_query_for_ghost_status]}"
    end

    update_query
  end

  def update_query_for_offers_with_status(update_set_values, status_array, pack_size, room_type_info_hash, checkin_hours)
    formated_initial_date = @formated_params[:initial_date].strftime("%Y-%m-%d %H:%M:%S")
    formated_final_date = @formated_params[:final_date].strftime("%Y-%m-%d 23:59:59")
    %Q{
      UPDATE offers
      SET
        #{update_set_values.map { |k,v| "#{k}='#{v}'"}.join(",")}
      WHERE
        (checkin_timestamp BETWEEN '#{formated_initial_date}' AND '#{formated_final_date}') AND
        (EXTRACT(HOUR FROM checkin_timestamp)) IN (#{checkin_hours.join(',')}) AND
        (checkin_timestamp_day_of_week IN (#{@formated_params[:selected_week_days_codes_array].join(",")})) AND
        (pack_in_hours='#{pack_size}') AND
        (room_id IN (#{room_type_info_hash[:rooms].join(",")})) AND
        (room_type_id='#{room_type_info_hash[:id]}') AND
        (status IN (#{status_array.join(",")}));
    }
  end

  def generate_insert_query
    days_in_range = @formated_params[:final_date] - @formated_params[:initial_date]
    first_date_string = @formated_params[:initial_date].strftime("%Y-%m-%d")
    valid_rooms_ids = Room.where("id IN (?) AND hotel_id = ?", @formated_params[:rooms_ids], @hotel.id).pluck(:id)
    return false if valid_rooms_ids.empty?
    # FOR COMPATIBILITY WITH OLDER RELEASES checkin_timestamp_day_of_week is from 1 TO 7 (ruby cday)
    # {1 => 'monday', 2 => 'tuesday', 3 => 'wednesday', 4 => 'thursday', 5 => 'friday', 6 => 'saturday', 7 => 'sunday'}
    # in postgres extract(dow from DATE) the configuration is (0 - 6; Sunday is 0)

    insert_query = %Q{
    INSERT INTO offers (
      checkin_timestamp,
      checkin_timestamp_day_of_week,
      checkout_timestamp,
      pack_in_hours,
      room_id,
      room_type_id,
      price,
      extra_price_per_person,
      status,
      room_type_initial_capacity,
      room_type_maximum_capacity,
      no_show_value,
      hotel_id,
      log_id)

      SELECT
        "selected_days"."date" + ("selected_hours_per_pack"."hour" || ' hour')::INTERVAL as checkin_timestamp,
        CASE
          WHEN extract(dow from "selected_days"."date") != 0 THEN extract(dow from "selected_days"."date")
          WHEN extract(dow from "selected_days"."date") = 0 THEN 7
        END as checkin_timestamp_day_of_week,
        "selected_days"."date" + ("selected_hours_per_pack"."hour" || ' hour')::INTERVAL + ("selected_hours_per_pack"."pack_size" || ' hour')::INTERVAL as checkout_timestamp,
        "selected_hours_per_pack"."pack_size" as pack_in_hours,
        "selected_rooms"."id" as room_id,
        "selected_rooms"."room_type_id" as room_type_id,
        CASE
          WHEN "selected_hours_per_pack"."pack_size" = 3 THEN "room_type_pricing_policies"."pack_price_3h"
          WHEN "selected_hours_per_pack"."pack_size" = 6 THEN "room_type_pricing_policies"."pack_price_6h"
          WHEN "selected_hours_per_pack"."pack_size" = 9 THEN "room_type_pricing_policies"."pack_price_9h"
          WHEN "selected_hours_per_pack"."pack_size" = 12 THEN "room_type_pricing_policies"."pack_price_12h"
        END as price,
        CASE
          WHEN "selected_hours_per_pack"."pack_size" = 3 THEN "room_type_pricing_policies"."extra_price_per_person_for_pack_price_3h"
          WHEN "selected_hours_per_pack"."pack_size" = 6 THEN "room_type_pricing_policies"."extra_price_per_person_for_pack_price_6h"
          WHEN "selected_hours_per_pack"."pack_size" = 9 THEN "room_type_pricing_policies"."extra_price_per_person_for_pack_price_9h"
          WHEN "selected_hours_per_pack"."pack_size" = 12 THEN "room_type_pricing_policies"."extra_price_per_person_for_pack_price_12h"
        END as extra_price_per_person,
        #{Offer::ACCEPTED_STATUS[:available]} as status,
        "room_types"."initial_capacity" as room_type_initial_capacity,
        "room_types"."maximum_capacity" as room_type_maximum_capacity,
        CASE
          WHEN "selected_hours_per_pack"."pack_size" = 3 THEN "room_type_pricing_policies"."pack_price_3h"
          WHEN "selected_hours_per_pack"."pack_size" = 6 THEN "room_type_pricing_policies"."pack_price_6h"
          WHEN "selected_hours_per_pack"."pack_size" = 9 THEN "room_type_pricing_policies"."pack_price_9h"
          WHEN "selected_hours_per_pack"."pack_size" = 12 THEN "room_type_pricing_policies"."pack_price_12h"
        END as no_show_value,
        #{@hotel.id} as hotel_id,
        #{@log_id} as log_id
      FROM
        (
        SELECT
          (CAST('#{first_date_string}' AS DATE) + i) as date
        FROM
          generate_series(0, #{days_in_range}, 1) i
        WHERE
          CASE
            WHEN extract(dow from (CAST('#{first_date_string}' AS DATE) + i)) != 0 THEN extract(dow from (CAST('#{first_date_string}' AS DATE) + i))
            WHEN extract(dow from (CAST('#{first_date_string}' AS DATE) + i)) = 0 THEN 7
          END IN (#{@formated_params[:selected_week_days_codes_array].join(",")})
        ) as selected_days,
        (
        SELECT
          i as hour,
          j as pack_size
        FROM
          generate_series(0, 24) i,
          unnest(ARRAY[#{Offer::ACCEPTED_LENGTH_OF_PACKS.join(",")}]) j
        WHERE
    }

    @formated_params[:selected_hours_per_pack].each do | pack_size, checkin_hours|
      insert_query = "#{insert_query} (j = #{pack_size} AND i IN (#{checkin_hours.join(",")})) OR"
    end
    insert_query.chop! # removing last or
    insert_query.chop!
    insert_query += %Q{
        ) as selected_hours_per_pack,
        (
        SELECT id, room_type_id
        FROM rooms
        WHERE id IN (#{valid_rooms_ids.join(",")})
        ) as selected_rooms
      INNER JOIN room_type_pricing_policies ON
        "room_type_pricing_policies"."room_type_id" = "selected_rooms"."room_type_id" AND
        "room_type_pricing_policies"."pricing_policy_id" = #{@selected_pricing_policy.id}
      INNER JOIN room_types ON "room_types"."id" = "selected_rooms"."room_type_id"
      WHERE
        CASE
          WHEN "selected_hours_per_pack"."pack_size" = 3 THEN "room_type_pricing_policies"."pack_price_3h"
          WHEN "selected_hours_per_pack"."pack_size" = 6 THEN "room_type_pricing_policies"."pack_price_6h"
          WHEN "selected_hours_per_pack"."pack_size" = 9 THEN "room_type_pricing_policies"."pack_price_9h"
          WHEN "selected_hours_per_pack"."pack_size" = 12 THEN "room_type_pricing_policies"."pack_price_12h"
        END != 0
    }
    insert_query
  end

  def write_temp_file(string)
    File.open("#{Rails.root}/tmp/queries.txt", 'w') do |file|
      file.write(string)
    end
  end

  def register_log(log_id)
    register_hash = Offer.where("log_id=(?)", log_id)
                         .select("count(*) as quantity, status")
                         .group("status")
                         .inject({}) do |hash, element|
                            hash[Offer::ACCEPTED_STATUS_REVERSED[element.status]] = element.quantity
                            hash
                         end
    (register_log = RoomOffersCreatorLog.where("id=(?)", log_id).first).result = register_hash
    register_log.save
  end

  private
    def execute_query(query)
      ActiveRecord::Base.connection.execute(query)
    end
end
