# coding: utf-8

class SendUserDataRdsWorker
  include Sidekiq::Worker

  sidekiq_options retry: 1
  sidekiq_options queue: "send_user_data_rds"

  def perform(user, hotel, cidade, identifier, user_name=nil, country=nil, locale=nil)
    require "uri"
    require "net/http"

    begin
      #### user could be int (representing user_id) or string (representing user_email)
      if user.class.to_s == 'String'
        @name  = user_name || ''
        @country = country || ''
        @locale = locale || ''
        @phone = ''
        @email = user || ''
      else
        @user  = User.find(user)
        @phone = @user.phone_number.blank? ? '' : @user.phone_number
        @name  = @user.name
        @email = @user.email
        @country = @user.country
        @locale = @user.locale
      end


      @hotel  = hotel || ''
      @cidade = cidade.split(/[-,]/).first || ''
      if country.present?
        @pais = @country
      else
        @pais = cidade.split(',').last.gsub(/^\s/, "") || ''
      end

      @payload = {
        "token_rdstation" => "779cf0ae4b4eb49d66889c3d1d760179",
        "identificador" => identifier,
        "email" => @email,
        "nome" => @name,
        "celular" => @phone,
        "hotel" => @hotel,
        "cidade" => @cidade,
        "pais" => @pais,
        "idioma" => @locale,
        "c_utmz" => "43135652.1441366582.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)"
      }

      # will send info to RDS only if production environment is running
      # to test RDS integration in development, please set rack_env=production on ENV vars
      # or comment this if condition to put the commands out to run in staging or test
      logger.info(ENV["RACK_ENV"])
      Logger.new("#{Rails.root}/log/my.log").info(ENV["RACK_ENV"])

      if ENV["RACK_ENV"] == 'production'
        response = post
        logger.info(response)
        Logger.new("#{Rails.root}/log/my.log").info(response)
      end
    rescue => ex
      logger.info(ex)
      Logger.new("#{Rails.root}/log/my.log").info(ex)
    end
  end

  private

    def post
      uri = URI('https://www.rdstation.com.br/api/1.3/conversions')
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new(uri.path, {'Content-Type' => 'application/json'})
      response = http.request(request, @payload.to_json)
      res = JSON.parse(response.body)
      res
    end

end
