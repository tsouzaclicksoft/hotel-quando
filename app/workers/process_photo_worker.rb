# coding: utf-8

class ProcessPhotoWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false
  sidekiq_options queue: "process_photo"

  def perform(photo_id)
    Rails.logger.info "Started ProcessPhotoWorker - #{photo_id} in: " + DateTime.now.in_time_zone("Brasilia").to_s
    photo = Photo.find(photo_id)
    photo.process_direct_upload! unless photo_id.nil?
    Rails.logger.info "Finished ProcessPhotoWorker - #{photo_id} in: " + DateTime.now.in_time_zone("Brasilia").to_s
  end
end
