class GoalsExecutiveResponsiblesReport < BaseReport
  
  def goals
    @object
  end

  def attributes
    ["Id","E-mail Executivo","Contatos a fazer","Reuniões a fazer","Novas empresas a cadastrar","Empresas de fato cadastradas","Reservas a fazer",
      "Contatos feitos","Reuniões feitas","Qtda Empresas novas cadastradas", "Qtda Empresas de fato cadastradas","Reservas feitas",
      "Meta de contatos concluída?","Meta de reuniões concluída?","Meta de novas empresas concluída?",
      "Meta de novas empresas de fato cadastradas concluída?","Meta de reservas concluída?",
      "Todas as metas foram concluídas?", "Metas Expiradas?"]
  end

  def attribute_values
    all_attribute_values = []
    attributes = [:id, :executive_responsible, :amount_contacts, :amount_meeting, :amount_new_companies,
      :amount_companies_actives, :amount_reservation, :amount_contacts_done, :amount_meeting_done, :amount_new_companies_done,
      :amount_companies_actives_done,:amount_reservation_done, :completed_contacts, :completed_meeting, :completed_new_companies,
      :completed_companies_actives, :completed_reservation, :goal_accomplished, :goal_expired]

    goals.each do |goal|
      values = attributes.map do |attribute|
        a = case value = goal.send(attribute)
        when nil then 'NULL'
        when true then 'Sim'
        when false then 'Nao'
        else
          if value.instance_of? ExecutiveResponsible
            value.email
          else
            value
          end
        end
      end

      all_attribute_values << values
    end
    all_attribute_values
  end

end
