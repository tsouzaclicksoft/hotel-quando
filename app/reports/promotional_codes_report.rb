class PromotionalCodesReport < BaseReport

  def promotional_codes
    @object
  end

  def attributes
    ["Código promocional"]
  end

  def attribute_values
    all_attribute_values = []
    attributes = [:code]

    promotional_codes.each do |promotional_code|
      values = attributes.map do |attribute|
        a = case value = promotional_code.send(attribute)
        when nil then 'NULL'
        when true then '1'
        when false then '0'
        else
            value
        end
      end
      all_attribute_values << values
    end
    all_attribute_values
  end

end
