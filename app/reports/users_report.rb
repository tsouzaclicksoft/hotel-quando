class UsersReport < BaseReport

  def users
    @object
  end

  def attributes
    ["Id","Nome","CPF","Passaporte","Genero","Pais","Estado","Cidade","Logradouro","Numero",
    	"Complemento","Email","Telefone","Nascimento","Idioma","Empresa","Vezes acessado",
    	"Ultimo acesso","Criado","Maxipago Token","Ativo","Criado por Afiliado","Afiliado", "Idioma" ,"Tipo", "CNPJ"]
  end

  def attribute_values
    all_attribute_values = []
    attributes = [:id, :name, :cpf, :passport, :gender, :country, :state, :city, :street, :number,
     	:complement,:email, :phone_number, :birth_date, :locale, :company_id, :sign_in_count,
     	:last_sign_in_at, :created_at, :maxipago_token, :is_active, :created_by_affiliate, :affiliate_id, :locale]

    users.each do |user|
      values = attributes.map do |attribute|
        a = case value = user.send(attribute)
        when nil then ''
        when true then 'Sim'
        when false then 'Nao'
        else
          if attribute == :birth_date
            value.strftime("%m/%d/%Y")
          elsif attribute == :last_sign_in_at || attribute == :created_at
            value.strftime("%m/%d/%Y %k:%M:%S")
          else
            value
          end
        end
      end
      if user.is_a_agency?
        values << 'Agência'
        values << user.agency_detail.cnpj
      elsif user.is_a_company?
        values << 'Empresa'
        values << user.company_detail.cnpj
      else
        values << 'Consumidor'
      end

      all_attribute_values << values
    end
    all_attribute_values
  end

end
