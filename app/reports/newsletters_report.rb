class NewslettersReport < BaseReport

  def newsletters
    @object
  end

  def attributes
    ["Id", "Email", "Cidade", "Criado"]
  end

  def attribute_values
    all_attribute_values = []
    attributes = [:id, :email, :city, :created_at]

    newsletters.each do |newsletter|
      values = attributes.map do |attribute|
        a = case value = newsletter.send(attribute)
        when nil then 'NULL'
        when true then '1'
        when false then '0'
        else
          if attribute == :created_at
            value.strftime("%d/%m/%Y %k:%M:%S")
          else
            value
          end
        end
      end
      all_attribute_values << values
    end
    all_attribute_values
  end

end
