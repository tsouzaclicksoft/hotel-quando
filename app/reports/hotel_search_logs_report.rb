class HotelSearchLogsReport < BaseReport

  def hotel_search_logs
    @object
  end

  def attributes
    ["Id", "Data da busca", "Checkin", "Pacote", "Pessoas", "Localização", "Email", "Hotéis exatos", "Hotéis similares"]
  end

  def attribute_values
    all_attribute_values = []
    hotel_search_logs.each do |log|
      values = []
      values << log.id
      values << log.created_at.strftime('%d/%m/%Y - %H:%M')
      values << "#{log.checkin_date} - #{log.checkin_hour}h"
      values << log.length_of_pack
      values << log.number_of_people
      values << log.address
      values << log.try(:email)
      values << log.exact_match_hotels_ids.length
      values << log.similar_match_hotels_ids.length
      all_attribute_values << values
    end
    all_attribute_values
  end
end
