class BookingsCompaniesReport < BaseReport

  def bookings
    @object
  end

  def attributes
    ["Consultor Responsável", "Centro de Custo", "Viajante", "Hotel", "Data de Check-in", "Horário de Check-in", "Pacote de Horas", "Valor Total", "Status"]
  end

  def attribute_values
    all_attribute_values = []
    bookings.each do |booking|
      values = []
      values << booking.try(:consultant).try(:name)
      values << booking.try(:traveler).try(:cost_center).try(:name)
      values << booking.try(:traveler).try(:name)
      values << booking.hotel.name
      values << booking.checkin_date.strftime("%d/%m/%Y")
      values << booking.offer.checkin_timestamp.strftime("%k:%M:%S")
      values << booking.pack_in_hours
      values << booking.total_price.to_f
      status = booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced] ? I18n.t(:invoiced) : I18n.t(Booking::ACCEPTED_STATUS_REVERSED[booking.status])
      values << status
    	all_attribute_values << values
    end
    all_attribute_values
  end

end