class BaseReport
  def initialize(object)
    @object = object
  end

  def export
    Axlsx::Package.new do |package|
      title_style = package.workbook.styles.add_style(bg_color:  '0073c5',
                                                      border:    Axlsx::STYLE_THIN_BORDER,
                                                      alignment: { horizontal: :center },
                                                      b:         true,
                                                      sz:        17)

      values_style = package.workbook.styles.add_style(sz:        15,
                                                       alignment: { horizontal: :center })

      package.workbook.add_worksheet(name: "Report") do |sheet|
        sheet.add_row attributes, style: title_style
        attribute_values.each do |values|
          sheet.add_row values, style: values_style
        end
      end

      package
    end
  end
end
