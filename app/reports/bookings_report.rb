class BookingsReport < BaseReport
  def bookings
    @object
  end
  def attributes
    ["Id", "Nome do titular", "Criada em", "Afiliado", "Hotel", "Executivo responsável", "Centro de custos", "Oferta", "Tipo de quarto", "Quarto", "Valor total", "Taxa", "Status", "Pacote em horas", "NPS", "Pontos multiplus", "Cancelada pelo admin", "Origem", "CPF", "Passaporte", "Código promocional","Tipo"]

  end

  def attribute_values
    all_attribute_values = []
    attributes = [:id, :guest_name, :created_at, :affiliate, :hotel, :executive_responsible,:traveler, :offer, :room_type, :room, :total_price, :booking_tax, :status, :pack_in_hours, :recommendation_scale, :multiplus_points, :canceled_by_admin, :is_app_mobile]
    bookings.each do |booking|
      values = attributes.map do |attribute|
        a = case value = booking.send(attribute)
        when nil then 'NULL'
          if attribute == :room_type && booking.meeting_room_offer_id.present?
            'Sala de reunião'
          else
            'NULL'
          end
        when true then
          if attribute == :is_app_mobile
            "App"
          else
            '1'
          end
        when false then '0'
          if attribute == :is_app_mobile
            "Web"
          else
            '1'
          end
        else
          if value.instance_of? Hotel
            value.name
          elsif (value.instance_of? Offer) || (value.instance_of? MeetingRoomOffer)
            value.checkin_timestamp.strftime("%m/%d/%Y %k:%M:%S")
          elsif value.instance_of? RoomType
            value.name_pt_br
          elsif value.instance_of? Affiliate
            value.name
          elsif value.instance_of? Room
            value.number
          elsif value.instance_of? ExecutiveResponsible
            value.name
          elsif value.instance_of? BigDecimal
            value.to_f
          elsif value.instance_of? Traveler
            value.try(:cost_center).try(:name)
          elsif attribute == :status
            I18n.t Booking::ACCEPTED_STATUS_REVERSED[value]
          elsif attribute == :created_at
            value.strftime("%m/%d/%Y %k:%M:%S")
          else
            value
          end
        end
      end
      if booking.user.blank?
        cpf = ''
        passport = ''
        promotional_code_log = ''
      else
        cpf = booking.user.cpf.present? ? booking.user.cpf.to_s : "NULL"
        passport = booking.user.passport.present? ? booking.user.passport.to_s : "NULL"
      end
      promotional_code_log = booking.promotional_code_log.try(:promotional_code).try(:code) ? booking.promotional_code_log.promotional_code.code : "NULL"
      values << cpf
      values << passport
      values << promotional_code_log
      if booking.executive_responsible_id.present?
        values <<  'Executivo'
      elsif booking.user.try(:agency_detail).try(:id).present?
        values <<  'Agência'
      elsif booking.user.try(:company_detail).try(:id).present?
        values <<  'Empresa'
      else
        values <<  'Consumidor'
      end
      all_attribute_values << values
    end
    


    all_attribute_values
  end

end
