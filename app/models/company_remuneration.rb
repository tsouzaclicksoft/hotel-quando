class CompanyRemuneration < ActiveRecord::Base

	validates :remuneration_by_reservation, presence: true
	validates :remuneration_by_new_company, presence: true
end
