class PricingPolicy < ActiveRecord::Base
  ACCEPTED_PERCENTAGE_PROFILE_NAMES = ['aggressive', 'normal', 'conservative', 'custom']

  # relations
  has_many                      :room_type_pricing_policies, dependent: :destroy
  has_many                      :room_types, through: :room_type_pricing_policies
  belongs_to                    :hotel
  accepts_nested_attributes_for :room_type_pricing_policies, allow_destroy: false

  # validations
  validates                     :name, presence: true
  validates_uniqueness_of       :name, scope: :hotel_id
  validates                     :percentage_profile_name, inclusion: { in: PricingPolicy::ACCEPTED_PERCENTAGE_PROFILE_NAMES }
  # validate                      :percentage_for_3h_cannot_be_less_than_default_pricing_policy
  # validate                      :percentage_for_6h_cannot_be_less_than_default_pricing_policy
  # validate                      :percentage_for_9h_cannot_be_less_than_default_pricing_policy
  # validate                      :percentage_for_12h_cannot_be_less_than_default_pricing_policy


  # hooks

  # class methods
  scope :by_hotel_id, -> hotel_id { where(hotel_id: hotel_id) }

  # instance methods
  def completed?
    room_type_pricing_policies = self.room_type_pricing_policies
    Offer::ACCEPTED_LENGTH_OF_PACKS.each do | length_of_pack |
      if room_type_pricing_policies.send("find_by_pack_price_#{length_of_pack}h",0)
        return false
      end
    end
    hotel_room_types = Hotel.find(self.hotel_id).room_types
    hotel_room_types.each do | room_type |
      unless room_type_pricing_policies.find_by_room_type_id(room_type.id)
        return false
      end
    end
    return true
  end

  def percentage_for_3h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_3h > HotelQuando::Constants::DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['3_hours']
      errors.add(:percentage_for_3h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_6h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_6h > HotelQuando::Constants::DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['6_hours']
      errors.add(:percentage_for_6h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_9h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_9h > HotelQuando::Constants::DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['9_hours']
      errors.add(:percentage_for_9h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_12h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_12h > HotelQuando::Constants::DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['12_hours']
      errors.add(:percentage_for_12h, :cannot_be_less_than_default_pricing_policy)
    end
  end

end
