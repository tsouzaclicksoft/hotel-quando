  # encoding: utf-8
class User < ActiveRecord::Base
''  # Array used to let the user select his locale, it is formatd like this to make it easier to make selects in html
  ACCEPTED_LOCALES = [['pt_br', 'Português (Brasil)'], ['en', 'English'], ['es', 'Español']]
  ACCEPTED_GENDERS = ['male', 'female']
  ACCEPTED_TYPES = { consumer: 1, agency: 2, company: 3}
  ACCEPTED_TYPES_REVERSED = Hash[ACCEPTED_TYPES.map {|k,v| [v, k]}]
  EMKT_FLUX_TYPES = { with_bookings: 1, without_bookings: 2 }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable,
  :validatable

  def should_validate_identifications
    if is_a_company_employee?
      return false if self.passport.present? && self.cpf.blank?
      true
    else
      return false
    end
  end

  # associations
  belongs_to :affiliate
  belongs_to :company
  belongs_to :department
  belongs_to :traveler
  has_many   :bookings
  has_many   :payments
  has_many   :refunds
  has_many   :credit_cards
  has_one    :agency_detail
  has_one    :company_detail
  has_many   :referred_friend_logs
  has_many   :travel_requests
  
  accepts_nested_attributes_for :agency_detail, :allow_destroy => true
  accepts_nested_attributes_for :company_detail, :allow_destroy => true
  attr_accessor :skip_validation_for_agency_and_company

  # validations
  validates :name, presence: true
  validates :email, presence: true
  validates :refer_code, uniqueness: true, allow_blank: true
#  validates :street, presence: true, if: "new_record? && skip_validation_for_agency_and_company?"
  validates :city, presence: true, if: "new_record? && skip_validation_for_agency_and_company?"
#  validates :state, presence: true, if: "new_record? && skip_validation_for_agency_and_company?"
  validates :country, presence: true, if: "new_record? && skip_validation_for_agency_and_company?"
#  validates :postal_code, presence: true, if: "new_record? && skip_validation_for_agency_and_company?"
  #validates :birth_date, presence: true, if: 'self.new_record?' && '!self.is_a_company_employee?'
  validates :gender, inclusion: {in: ACCEPTED_GENDERS}, if: 'gender.present?'
  validate  :validate_cpf, if: 'self.bookings.uses_tudo_azul.count > 0'
  validate  :should_have_name_and_last_name, if: 'is_a_consumer?'
  #validate  :should_have_cnpj
  #validate :validate_identifications, if: "should_validate_identifications"

  # class methods
  monetize :monthly_budget_centavos, allow_nil: true
  monetize :budget_for_24_hours_pack_centavos, allow_nil: true

  scope :by_refer_code, -> refer_code { where("refer_code ILIKE '%#{refer_code}%'") }
  scope :by_name, -> name { where("name ILIKE '%#{name}%'") }
  scope :by_agency_name, -> name { joins(:agency_detail).where("agency_details.name ILIKE '%#{name}%'") }
  scope :by_company_name, -> name { joins(:company_detail).where("company_details.name ILIKE '%#{name}%'") }
  scope :by_email, -> email { where(email: email) }
  scope :by_cpf, -> cpf { where(cpf: cpf) }
  scope :by_passport, -> passport { where(passport: passport) }
  scope :by_gender, -> gender { where(gender: gender) }
  scope :by_country, -> country { where("country ILIKE '%#{country}%'") }
  scope :by_state, -> state { where("state ILIKE '%#{state}%'") }
  scope :by_city, -> city { where("city ILIKE '%#{city}%'") }
  scope :by_max_birth_date, -> birth_date { where("birth_date <= DATE '#{birth_date.to_date}'") }
  scope :by_min_birth_date, -> birth_date { where("birth_date >= DATE '#{birth_date.to_date}'") }

  # Hooks
  before_validation :set_new_password, if: 'is_a_company_employee? && new_record? && !password.present?'
  #after_create :generate_refer_code
  before_validation :set_new_password_to_traveler, if: "new_record? && !password.present? && traveler_id.present?"
  after_create :send_login_and_password, if: "traveler_id.present?"

  def skip_validation_for_agency_and_company?
    !skip_validation_for_agency_and_company
  end

  # Instance methods

  def add_credit_to_balance_user(booking)
    user_own_refer_code = User.where(refer_code: (booking.promotional_code_log.promotional_code.code)).first
    promotional_code = booking.promotional_code_log.promotional_code
    code = promotional_code.code
    mbm_config = promotional_code.member_get_member_config
    if user_own_refer_code.present? && mbm_config.present?
      if user_own_refer_code.refer_code == code && mbm_config.is_active
        if promotional_code.bookings.paid.count > mbm_config.number_of_first_bookings
          user_own_refer_code.balance += mbm_config.indicators_credit
        else
          user_own_refer_code.balance += mbm_config.indicators_credit_first_bookings
        end
        user_own_refer_code.save
      end
    end
  end

  def set_new_password_to_traveler
    self.password = Devise.friendly_token.first(6)
    self.password_confirmation = self.password
  end

  def send_login_and_password
    UserMailer.send_login_and_password(self).deliver
  end

  def return_credit_balance_to_user(booking)
    value = booking.balance_value_used
    self.balance += value
    self.save
  end

  def deduct_credit_when_use_in_booking(booking)
    value = booking.balance_value_used
    if value <= self.balance
      self.balance -= value
    else 
      self.balance = 0
    end
    self.save
  end

  def deduct_credit_to_balance_user(booking)
    user_own_refer_code = User.where(refer_code: (booking.promotional_code_log.promotional_code.code)).first
    promotional_code = booking.promotional_code_log.promotional_code
    code = promotional_code.code
    mbm_config = promotional_code.member_get_member_config
    if user_own_refer_code.present? && mbm_config.present?
      if user_own_refer_code.refer_code == code && mbm_config.is_active
        if promotional_code.bookings.paid.count >= mbm_config.number_of_first_bookings
          user_own_refer_code.balance -= mbm_config.indicators_credit
        else
          user_own_refer_code.balance -= mbm_config.indicators_credit_first_bookings
        end
        user_own_refer_code.balance = 0 if user_own_refer_code.balance < 0
        user_own_refer_code.save
      end
    end
  end

  def generate_refer_code
    if self.refer_code.blank? && self.first_name.present?
      self.refer_code = loop do
        random_code = self.first_name.downcase + SecureRandom.urlsafe_base64(4, false)
        break random_code unless User.exists?(refer_code: random_code)
      end
      self.save
    end
  end


  def have_any_confirmed_booking?
    self.bookings.paid.exists?
  end

  def complete_for_payment? # will be used in payments
    self.cpf.present? || self.passport.present?
  end

  def need_to_complete_address?
    self.street.blank? || self.city.blank? || self.state.blank? || self.country.blank? || (self.postal_code.blank? && self.country != "CO")
  end

  def validate_identifications
    if (self.cpf.blank?) && (self.passport.blank?)
      errors.add(:cpf, I18n.t(:include_at_least_one_identification))
      errors.add(:passport, I18n.t(:include_at_least_one_identification))
    else
      errors.add(:cpf, I18n.t(:cpf_not_valid)) unless self.valid_cpf?
    end
  end

  def validate_cpf
    if self.cpf.blank?
      errors.add(:cpf, I18n.t(:include_at_least_one_identification))
    else
      errors.add(:cpf, I18n.t(:cpf_not_valid)) unless self.valid_cpf?
    end
  end

  def simple_cpf
    cpf.gsub('.','').gsub('-','')
  end

  def valid_cpf?
    !!( CpfValidator::Cpf.valid? self.cpf )
  end

  def should_have_name_and_last_name
    errors.add(:name, I18n.t(:must_have_lastname)) if self.name.to_s.split(' ').length < 2
  end

  def merge attributes
    attributes.each do |key, val|
      self[key] = val
    end
  end

  def first_name
    name.split(" ").first
  end

  def last_name
    name_parts = name.split(" ")
    name_parts.shift
    name_parts.join(" ")
  end

  def name
    if self.is_a_agency?
      self.agency_detail.name
    elsif self.is_a_company?
      self.company_detail.name
    else
      read_attribute(:name)
    end
  end

  def email
    read_attribute(:email)
  end

  def is_a_company_employee?
    self.company_id.present?
  end

  def is_a_agency?
    self.agency_detail.present?
  end

  def is_a_company?
    self.company_detail.present?
  end

  def is_a_consumer?
    !self.is_a_agency? && !self.is_a_company?
  end

  def self.without_booking
    User.where('id NOT IN (SELECT DISTINCT(user_id) FROM bookings)').pluck(:email)
  end

  def self.with_booking
    User.includes(:bookings).where.not(bookings: { id: nil }).pluck(:email).group_by{|x| x}
  end

  private
    def set_new_password
      self.password = Devise.friendly_token.first(6)
      self.password_confirmation = self.password
    end

    def update_aside_authorization?
      ( CONTROL_ATTRIBUTES & @changed_attributes.keys.map(&:to_sym) ).blank?
    end
end
