class Hotel::OmnibeesConfig
  include Virtus.model

  [ 3, 6, 9, 12, 13, 14, 15, 16, 17, 18, 36, 37, 38, 39, 40, 41, 42, 60, 61, 62, 63, 64, 65, 66 ].each do |pack|
    attribute(:"accepted_hours_for_#{pack}h_pack", Array[Integer], :default => (0..23).to_a)
    attribute(:"accepts_#{pack}h_pack", Boolean, :default => true)
  end

  def self.model_name
    str = 'omnibees_config'
    def str.param_key
      self
    end
    str
  end

  def accepts_pack?(pack)
    send("accepts_#{pack}h_pack")
  end

  def accepted_hours_for(pack)
    send("accepted_hours_for_#{pack}h_pack")
  end
end
