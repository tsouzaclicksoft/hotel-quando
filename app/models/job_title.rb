class JobTitle < ActiveRecord::Base
  # Validations
  validates :name, presence: true
  validates :company_detail, presence: true
  validates_numericality_of :budget_value, greater_than_or_equal_to: 0
  validates :money_sign, presence: true
  validates :budget_value, presence: true

  validates_uniqueness_of :name, :scope => :company_detail_id
  # Relations
  belongs_to 	:company_detail
  has_many  	:travelers
  #has_many  :photos
  #accepts_nested_attributes_for :photos, :allow_destroy => true

  # Instance methods
end
