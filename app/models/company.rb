class Company < ActiveRecord::Base

  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :authentication_keys => [:login]

  # Validations
  validates :email, presence: true
  validates :name, presence: true
  validates :cnpj, presence: true
  validates :responsible_name, presence: true
  validate  :responsible_should_have_name_and_last_name
  validates :login, presence: true, uniqueness: true

  # Relations
  has_many :invoices
  has_many :users
  has_many :bookings
  has_many :credit_cards
  has_many :departments
  belongs_to :agency

  # Hooks
  before_validation :set_new_password, if: "new_record? && !password.present?"

  # Instance methods
  def set_new_password
    self.password = Devise.friendly_token.first(6)
    self.password_confirmation = self.password
  end

  def responsible_first_name
    responsible_name.split(" ").first
  end

  def responsible_last_name
    name_parts = responsible_name.split(" ")
    name_parts.shift
    name_parts.join(" ")
  end

  def activate!
    self.update!(is_active: true)
  end

  def deactivate!
    self.update!(is_active: false)
  end

  private
    def responsible_should_have_name_and_last_name
      errors.add(:responsible_name, I18n.t(:must_have_lastname)) if self.responsible_name.to_s.split(' ').length < 2
    end

  alias_method :employees, :users

end
