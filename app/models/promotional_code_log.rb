class PromotionalCodeLog < ActiveRecord::Base
	# relations
	belongs_to :promotional_code
	belongs_to :booking

	# validations
	validates  	:booking_id, presence: true, uniqueness: true, if: "new_record?"
	validates  	:promotional_code_id, presence: true

	# class methods
	scope :by_promotional_code, -> code { joins(:promotional_code).where("promotional_codes.code = '#{code}'") }
end
