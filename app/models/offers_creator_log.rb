class OffersCreatorLog < ActiveRecord::Base
  serialize :result
  serialize :form_params

  # relations
  belongs_to :hotel

  # validations
  validates  :hotel_id, presence: true

end