# encoding: utf-8

class Refund < ActiveRecord::Base
  ACCEPTED_STATUSES = { success: 1, error: 2 }
  ACCEPTED_STATUS_REVERSED = Hash[ACCEPTED_STATUSES.map {|k,v| [v, k]}]


  # associations 
  belongs_to  :user
  belongs_to  :traveler
  belongs_to  :payment


  # validations
  validates :status, inclusion: { in: Refund::ACCEPTED_STATUSES.values }
  validates :user, presence: true
  validates :payment, presence: true
  validates :value, presence: true

  # scopes

  # hooks
  before_save if: "status_changed? && (status == #{Refund::ACCEPTED_STATUSES[:success]})" do
    self.authorized_at = DateTime.now
  end

end
