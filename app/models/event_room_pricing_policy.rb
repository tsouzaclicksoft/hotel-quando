class EventRoomPricingPolicy < BusinessRoomPricingPolicy
	# validations
  validate                      :percentage_for_2h_cannot_be_less_than_default_pricing_policy
  validate                      :percentage_for_4h_cannot_be_less_than_default_pricing_policy
  validate                      :percentage_for_8h_cannot_be_less_than_default_pricing_policy
  

  # instance methods
  def completed?
    business_room_prices = self.business_room_prices
    EventRoomOffer::ACCEPTED_LENGTH_OF_PACKS.each do | length_of_pack |
      if business_room_prices.send("find_by_pack_price_#{length_of_pack}h",0)
        return false
      end
    end
    hotel_event_rooms = Hotel.find(self.hotel_id).event_rooms
    hotel_event_rooms.each do | event_room |
      unless business_room_prices.find_by_business_room_id(event_room.id)
        return false
      end
    end
    return true
  end
  
end