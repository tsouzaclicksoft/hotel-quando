class AgencyDetail < ActiveRecord::Base
  # Validations
  #validates :name, presence: true
  Email_Format = /\A^$|[-_0-9a-z.+]+@([-a-z0-9.]+)+\.[a-z]{2,4}\z/i
  validates_format_of :sign_email, with: Email_Format
  validates           :street, presence: true, if: "new_record?"
  validates           :city, presence: true, if: "new_record?"
  validates           :state, presence: true, if: "new_record? && country != 'CO'"
  validates           :country, presence: true, if: "new_record?"
  validates           :postal_code, presence: true, if: "new_record? && country != 'CO'"
  validates           :number, presence: true, if: "new_record?"

  # Relations
  belongs_to 	:user
  has_many 		:company_details
  has_many 		:consultants
  has_many 		:photos
  accepts_nested_attributes_for :photos, :allow_destroy => true

  # Instance methods
  def email_for_notice
    self.sign_email.present? ? self.sign_email : self.user.email
  end

end
