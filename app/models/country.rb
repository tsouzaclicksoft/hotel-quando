# encoding: utf-8
class Country < ActiveRecord::Base

  # relations
 	has_many :states
 	has_many :booking_taxes
 	has_many :executive_responsibles

end
