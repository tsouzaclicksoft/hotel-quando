class Consultant < ActiveRecord::Base
  # Validations
  validates :name, presence: true
  validates :agency_detail, presence: true
  validate  :should_have_name_and_last_name

  # Relations
  belongs_to 	:agency_detail
  has_many    :bookings 

  # Instance methods
  def should_have_name_and_last_name
    errors.add(:name, I18n.t(:must_have_lastname)) if self.name.to_s.split(' ').length < 2
  end
  
  def first_name
    name.split(" ").first
  end

  def last_name
    name_parts = name.split(" ")
    name_parts.shift
    name_parts.join(" ")
  end
  
end
