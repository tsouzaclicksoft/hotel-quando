class City < ActiveRecord::Base

  # relations
  belongs_to :state
  has_many   :hotels
  has_many	 :booking_packs
  has_many   :places

  # class methods
  scope :by_name, -> name { where(name: name) }

  validates_uniqueness_of :name, :scope => :state_id

end
