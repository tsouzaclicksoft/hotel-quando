class HotelGroup < ActiveRecord::Base
	# relations
	belongs_to :campaign
	belongs_to :hotel

	# validations
	validates  	:hotel_id, presence: true
	validates  	:campaign_id, presence: true
end
