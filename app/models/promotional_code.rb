class PromotionalCode < ActiveRecord::Base
  include Shared::CurrencyCurrentHelper

  ACCEPTED_STATUSES = {active: 1, suspended: 2, expired: 3, used: 4}
  ACCEPTED_STATUS_REVERSED = Hash[ACCEPTED_STATUSES.map {|k,v| [v, k]}]

  ACCEPTED_USER_TYPE = {all_users: 1, users_without_booking: 2}
  ACCEPTED_USER_TYPE_REVERSED = Hash[ACCEPTED_USER_TYPE.map {|k,v| [v, k]}]

  ACCEPTED_DISCOUNT_TYPE = {percentage: 1, money_value: 2}
  ACCEPTED_DISCOUNT_TYPE_REVERSED = Hash[ACCEPTED_DISCOUNT_TYPE.map {|k,v| [v, k]}]

  ACCEPTED_DISCOUNT_IN = {offer_price: 1, booking_tax: 2}
  ACCEPTED_DISCOUNT_IN_REVERSED = Hash[ACCEPTED_DISCOUNT_IN.map {|k,v| [v, k]}]

  # relations
  belongs_to :campaign
  belongs_to :member_get_member_config
  has_many :promotional_code_logs
  has_many :referred_friend_logs

  # validations

  validates                     :campaign_id, presence: true
  validates                     :code, presence: true, uniqueness: true
  validates                     :status, presence: true
  validates                     :status, inclusion: { in: PromotionalCode::ACCEPTED_STATUSES.values }
  validates                     :user_type, presence: true
  validates                     :user_type, inclusion: { in: PromotionalCode::ACCEPTED_USER_TYPE.values }
  validates                     :discount_in, presence: true
  validates                     :discount_in, inclusion: { in: PromotionalCode::ACCEPTED_DISCOUNT_IN.values }
  validates                     :discount_type, presence: true
  validates                     :discount_type, inclusion: { in: PromotionalCode::ACCEPTED_DISCOUNT_TYPE.values }
  validates                     :discount, presence: true
  validates                     :usage_limit, presence: true
  validates                     :start_date, presence: true, if: "new_record?"
  validates                     :expiration_date, presence: true, if: "new_record?"
  validates_numericality_of     :discount, greater_than: 0
  validates_numericality_of     :usage_limit, greater_than: 0
  validates                     :currency_code, presence: true, if: 'discount_type == 2'
  validate                      :validate_start_date, if: "new_record?"
  validate                      :validate_start_date_less_than_today, if: "new_record?"
  validate                      :validate_expiration_date_less_than_today, if: "new_record?"
  validate                      :validate_discount_less_than_99, if: "discount_type == #{ACCEPTED_DISCOUNT_TYPE[:percentage]} && new_record?"

  # class methods
    scope :by_id, -> id { where(id: id)}
    scope :by_campaign_id, -> id { where(campaign_id: id)}
    scope :by_code, -> id { where(code: id)}
    scope :by_status, -> id { where(status: id)}

  # instance_methods
  def bookings
    bookings_id = self.promotional_code_logs.pluck(:booking_id)
    Booking.where(id: bookings_id)
  end

  def self.create_member_get_member_code(user)
    mbm_config = MemberGetMemberConfig.last
    if mbm_config.is_active
      promotional_code = self.new(
          campaign_id: mbm_config.campaign_id,
          code: user.refer_code,
          status: PromotionalCode::ACCEPTED_STATUSES[:active],
          user_type: PromotionalCode::ACCEPTED_USER_TYPE[:users_without_booking],
          discount_type: mbm_config.discount_type_to_referred_first_bookings,
          discount: mbm_config.discount_to_referred_first_bookings,
          usage_limit: 1,
          start_date: (DateTime.now).strftime("%Y-%m-%d"),
          expiration_date: (DateTime.now + mbm_config.expiration_date_in_months.month).strftime("%Y-%m-%d"),
          currency_code: mbm_config.currency_code_discount,
          member_get_member_config_id: mbm_config.id,
          avaiable_for_packs: mbm_config.avaiable_for_packs,
          avaiable_for_days_of_the_week: mbm_config.avaiable_for_days_of_the_week
        )
      promotional_code.save
      promotional_code
    end
  end

  def booking_value_with_discount(value, promotional_code_log_currency, hotel_currency_code)
    if self.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:percentage]
      value_with_discount = value - (value * self.discount) / 100.0
    else
      if self.currency_code.nil?
        value_with_discount = value - self.discount
      else
        value_with_discount = value - calcule_discount_with_currency(promotional_code_log_currency, hotel_currency_code)
      end

    end
    return self.discount_in == PromotionalCode::ACCEPTED_DISCOUNT_IN[:offer_price] ? value_with_discount : value
  end

  def active_when_booking_expire(promotional_code_log)
    if self.status == PromotionalCode::ACCEPTED_STATUSES[:used]
      promotional_code_log.destroy
      self.status = PromotionalCode::ACCEPTED_STATUSES[:active]
      self.save!
    end
  end

  def expire
    self.status = PromotionalCode::ACCEPTED_STATUSES[:expired]
    if self.save!
      return true
    else
      return false
    end
  end

  def start
    self.status = PromotionalCode::ACCEPTED_STATUSES[:active]
    if self.save!
      return true
    else
      return false
    end
  end

  def calcule_discount_with_currency(promotional_code_log_currency, hotel_currency_code)
    currency_by_hotel = Currency.by_money_sign(hotel_currency_code).order(created_at: :desc).first
    currency_by_self = Currency.by_money_sign(self.currency_code).order(created_at: :desc).first

    if promotional_code_log_currency == currency_by_hotel.money_sign && currency_by_hotel.money_sign == "US$" && currency_by_self.money_sign == promotional_code_log_currency
      discount_convert_to_currency = self.discount
    elsif currency_by_hotel.money_sign == "US$"
      discount_convert_to_currency = self.discount / currency_by_self.value
    else
      hotel_currency_in_dollar = self.discount / currency_by_self.value
      discount_convert_to_currency = hotel_currency_in_dollar * currency_by_hotel.value
    end

    discount_convert_to_currency
  end

  def validate_start_date
    if self.start_date.present?
      errors.add(:start_date, '1') if self.start_date > self.expiration_date
    end
  end

  def validate_start_date_less_than_today
    if self.start_date.present?
      errors.add(:start_date_less_than_today, '2') if self.start_date < Date.today
    end
  end

  def validate_expiration_date_less_than_today
    if self.expiration_date.present?
      errors.add(:expiration_date_less_than_today, '3') if self.expiration_date < Date.today
    end
  end

  def validate_discount_less_than_99
    errors.add(:discount, I18n.t(:must_be_less_than_99)) if self.discount > 99
  end
end
