# encoding: utf-8

class CreditCard < ActiveRecord::Base
  # fields
  attr_accessor :temporary_number # because the complete number will not be saved to the database

  # maxipago doesn't use security code when
  # creating a token for CC
  attr_accessor :security_code

  # associations
  belongs_to :user
  belongs_to :company
  belongs_to :traveler

  # validations
  #validates :flag_name, presence: true
  #validates :billing_name, presence: true
  #validates :billing_address1, presence: true
  #validates :billing_city, presence: true
  #validates :billing_state, presence: true
  #validates :billing_zipcode, presence: true
  #validates :billing_country, presence: true
  #validates :billing_phone, presence: true
  #validates :last_four_digits, presence: true
  validates :card_number, presence: true
  validates :cvv, presence: true
  validates :expiration_year, presence: true
  validates :expiration_month, presence: true
  #validates :user, presence: true, unless: 'traveler.present?'
  validates :traveler, presence: true
  #validates :company, presence: true, unless: 'user.present?'
  #validates :maxipago_token, presence: true
  #validate  :validate_temporary_number, unless: 'temporary_number.nil?'
  #validate  :validate_state_based_on_country
  #validate  :billing_zipcode_length_should_be_less_than_ten_characters_long
  #validate  :complete_for_payment?
  #validate  :owner_should_have_name_and_last_name
  #validate  :expiration_date_should_not_be_in_the_past

  def owner
    if self.user.present?
      @owner ||= self.user
    elsif self.company.present?
      @owner ||= self.company
    else
      @owner ||= self.traveler
    end
    @owner
  end

  def owner=(value)
    if value.is_a? User
      self.user = value
    elsif value.is_a? Company
      self.company = value
    else
      self.traveler = value
    end
    @owner = value
  end

  def temporary_number=(value)
    unless value.blank?
      self.last_four_digits = value.to_s.last(4).to_i
      self.flag_name = CreditCardValidator::Validator.card_type(value.to_s)
    end
    @temporary_number = value
  end

  def billing_phone
    if read_attribute(:billing_phone)
      read_attribute(:billing_phone).gsub(/\D/, '')
    else
      nil
    end
  end

  def billing_zipcode
    if read_attribute(:billing_zipcode)
      read_attribute(:billing_zipcode).gsub(/\D/, '')
    else
      nil
    end
  end

  def validate_temporary_number
    unless CreditCardValidator::Validator.valid?(temporary_number.to_s)
      errors.add(:temporary_number, :invalid)
    end
  end

  def complete_for_payment?
    if self.owner_cpf.present?
      self.validate_cpf
    else
      unless self.owner_passport.present?
        errors.add(:owner_passport, :blank)
        errors.add(:owner_cpf, :blank)
      end
    end
  end

  protected

  def expiration_date_should_not_be_in_the_past
    date_to_test = 1.year.ago
    begin
      date_to_test = Date.new(self.expiration_year, self.expiration_month)
    rescue
    end
    if (self.expiration_month) && (self.expiration_year) && (date_to_test < Date.today.beginning_of_month)
      errors.add(:expiration_month, I18n.t(:is_in_the_past))
      errors.add(:expiration_year, I18n.t(:is_in_the_past))
    end
  end

  def validate_state_based_on_country
    if self.billing_country == "BR" && self.billing_state == 'ZZ'
      errors.add(:billing_state, :invalid)
    elsif !(self.billing_country == "BR") && !(self.billing_state == 'ZZ')
      errors.add(:billing_state, :invalid)
    end
  end

  def owner_should_have_name_and_last_name
    errors.add(:billing_name, I18n.t(:must_have_lastname)) if self.billing_name.to_s.split(' ').length < 2
  end

  def billing_zipcode_length_should_be_less_than_ten_characters_long
    if billing_zipcode.present?
      errors.add(:billing_zipcode, :invalid) if billing_zipcode.length > 9
    end
  end

  def validate_cpf
    if self.valid_cpf?
      true
    else
      errors.add(:owner_cpf, :invalid)
      false
    end
  end

  def valid_cpf?
    !!( CpfValidator::Cpf.valid? self.owner_cpf )
  end
end
