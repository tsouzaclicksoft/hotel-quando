class Place < ActiveRecord::Base
  PLACE_TYPES = { airport: 1, city: 2, region: 3 }
  PLACE_TYPES_REVERSED = Hash[PLACE_TYPES.map {|k,v| [v, k]}]

  has_many :hotels, counter_cache: true
  has_many :photos
  accepts_nested_attributes_for :photos, :allow_destroy => true
  belongs_to :city

  validates_presence_of :name_pt_br, :name_es, :name_en, :city, :place_type, :latitude, :longitude
  validates_uniqueness_of :name_pt_br, :name_es, :name_en, if: "new_record?"
end
