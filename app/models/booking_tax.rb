class BookingTax < ActiveRecord::Base
# validations
  validates :value, presence: true
  validates :country_id, presence: true
  validates :currency_symbol, presence: true

# associations
  belongs_to :country

end
