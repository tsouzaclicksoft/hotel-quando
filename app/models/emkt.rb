class Emkt < ActiveRecord::Base
  EMKT_TYPES = { with_bookings: 1, without_bookings: 2 }
  EMKT_TYPES_REVERSED = Hash[EMKT_TYPES.map {|k,v| [v, k]}]
  EMKT_ACTIVATE = { yes: 1, no: 0 }
  EMKT_DAY_OF_WEEK = { monday: 1, tuesday: 2, wednesday: 3,  thursday: 4, friday: 5, saturday: 6,  sunday: 7 }

  ACCEPTED_TYPES= ['Usuários com reserva', 'Usuários sem reserva']

  has_attached_file :header
  has_attached_file :box_1
  has_attached_file :box_2
  has_attached_file :box_3_1
  has_attached_file :box_3_2
  has_attached_file :box_3_3
  has_attached_file :box_3_4
  has_attached_file :box_3_5
  has_attached_file :box_3_6
  has_attached_file :footer

  validates_presence_of :name_pt_br, :emkt_type
end
