class EventRoomOffersCreatorLog < OffersCreatorLog
  def number_of_offers_to_be_generated
	  number_of_days = (form_params['initial_date'].to_date..form_params['final_date'].to_date).inject(0) do |memo, date|
	    wday = (date.wday == 0) ? '7' : date.wday.to_s
	    if form_params[:selected_days_hash].has_key?(wday)
	      memo + 1
	    else
	      memo
	    end
	  end
	  rooms = form_params[:rooms_ids].count 
	  horarios = EventRoomOffer::ACCEPTED_LENGTH_OF_PACKS.inject(0) do |memo, pack|
	    if form_params[:selected_pack_lengths].include?(pack.to_s)
	      memo + form_params[:pack][pack.to_s].count
	    else
	      memo
	    end
	  end
	  number_of_days * rooms * horarios
	end

end
