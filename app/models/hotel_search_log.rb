class HotelSearchLog < ActiveRecord::Base
  belongs_to :search_logs_user

  def email
    search_logs_user.try(:email).to_s
  end

  def exact_match_hotels_ids
    read_attribute(:exact_match_hotels_ids).blank? ? [] : read_attribute(:exact_match_hotels_ids).split("-").map(&:to_i)
  end

  def similar_match_hotels_ids
    read_attribute(:similar_match_hotels_ids).blank? ? [] : read_attribute(:similar_match_hotels_ids).split("-").map(&:to_i)
  end
end
