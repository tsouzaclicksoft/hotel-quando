class BusinessRoomPrice < ActiveRecord::Base

  # relations
  belongs_to                    :business_room_pricing_policy
  belongs_to                    :business_room

  # validations
  validates                     :pack_price_1h, presence: true, if: 'business_room.type.eql?("MeetingRoom")'
  validates                     :pack_price_2h, presence: true
  validates                     :pack_price_3h, presence: true, if: 'business_room.type.eql?("MeetingRoom")'
  validates                     :pack_price_4h, presence: true
  validates                     :pack_price_5h, presence: true, if: 'business_room.type.eql?("MeetingRoom")'
  validates                     :pack_price_6h, presence: true, if: 'business_room.type.eql?("MeetingRoom")'
  validates                     :pack_price_7h, presence: true, if: 'business_room.type.eql?("MeetingRoom")'
  validates                     :pack_price_8h, presence: true
  validates                     :pack_price_24h, presence: true
  validates                     :business_room_id, presence: true
  validates_uniqueness_of       :business_room_pricing_policy_id, scope: :business_room_id
  validates_numericality_of     :pack_price_1h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_2h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_3h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_4h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_5h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_6h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_7h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_8h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_24h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_1h, less_than_or_equal_to: 9999.99
  validates_numericality_of     :pack_price_2h, less_than_or_equal_to: 9999.99
  validates_numericality_of     :pack_price_3h, less_than_or_equal_to: 9999.99
  validates_numericality_of     :pack_price_4h, less_than_or_equal_to: 9999.99
  validates_numericality_of     :pack_price_5h, less_than_or_equal_to: 9999.99
  validates_numericality_of     :pack_price_6h, less_than_or_equal_to: 9999.99
  validates_numericality_of     :pack_price_7h, less_than_or_equal_to: 9999.99
  validates_numericality_of     :pack_price_8h, less_than_or_equal_to: 9999.99
  validates_numericality_of     :pack_price_24h, less_than_or_equal_to: 9999.99

  # hooks

  # class methods
  scope :by_business_room_id, -> business_room_id { where(business_room_id: business_room_id) }

  # instance methods
  def all_prices_are_zero?
    type = self.business_room.type + "Offer"
    all_prices_are_zero = true
    type.constantize::ACCEPTED_LENGTH_OF_PACKS.each do | length |
      unless self.send("pack_price_#{length}h") == 0
        all_prices_are_zero = false
      end
    end
    all_prices_are_zero
  end
end