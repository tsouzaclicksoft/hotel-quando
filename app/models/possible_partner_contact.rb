class PossiblePartnerContact < ActiveRecord::Base
  TYPES_OF_CONTACT = {hotel_contact: 1, company_contact: 2}
  # VALUE_OF_PERCENTAGE = {38, 43, 60, 65}

  # validations
  validate  :should_have_at_least_one_contact
  validates :name, presence: true
  validates :contact_name, presence: true
  validates :message, presence: true, if: 'type_of_contact == 2'

  #  hooks
  after_create :send_new_contact_notice

  # class_methods
  scope :by_type, -> type { where(type_of_contact: type) }
  scope :by_locale, -> locale { where(locale: locale) }
  scope :by_admin_visualized, -> admin_visualized { where(admin_visualized: admin_visualized) }

  # instace_methods
  protected

  def send_new_contact_notice
    AdminMailer.send_new_possible_partner_contact(self).deliver
  end

  private

  def should_have_at_least_one_contact
    if self.phone.blank? && self.email_for_contact.blank?
      errors.add(:phone, 'the possible partner should have at least one contact')
      errors.add(:email_for_contact, 'the possible partner should have at least one contact')
      false
    end
  end
end
