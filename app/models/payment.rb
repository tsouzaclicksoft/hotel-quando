# encoding: utf-8

class Payment < ActiveRecord::Base
  ACCEPTED_STATUSES = { waiting_for_maxipago_response: 1, authorized: 2, not_authorized: 3, captured: 4, canceled: 5, refunded: 6, error_refunding: 7, maxipago_response_error: 8 }
  ACCEPTED_STATUS_REVERSED = Hash[ACCEPTED_STATUSES.map {|k,v| [v, k]}]
  CREDIT_CARD_OPERATION_TAX_IN_PERCENTAGE = 3.0
  audited only: [:status, :active, :maxipago_response, :paypal_payment_id, :paypal_transaction_id, :paypal_refund_id, :epayco_ref, :payu_order_id, :payu_transaction_id, :maxipago_order_id]

  # associations 
  belongs_to  :user
  belongs_to  :affiliate
  belongs_to  :traveler
  belongs_to  :booking
  belongs_to  :credit_card
  has_many    :refunds

  # validations
  validates :status, inclusion: { in: Payment::ACCEPTED_STATUSES.values }
  validates :booking, presence: true
  #validates :credit_card, presence: true
  validates :user, presence: true, unless: 'affiliate_id.present?'
  validates :affiliate, presence: true, unless: 'user_id.present?'

  # scopes
  scope :authorized, -> { where(status: ACCEPTED_STATUSES[:authorized]) }
  scope :by_id, -> id { where(id: id) }
  scope :by_booking_id, -> booking_id { where(booking_id: booking_id) }
  scope :by_user_id, -> user_id { where(user_id: user_id) }
  scope :by_credit_card_id, -> credit_card_id { where(credit_card_id: credit_card_id) }
  scope :by_status, -> status { where(status: status) }
  scope :authorized_or_captured, -> { where(status: [ACCEPTED_STATUSES[:authorized], Payment::ACCEPTED_STATUSES[:captured]]) }

  # hooks
  before_save if: "status_changed? && self.authorized_at.nil? && ((status == #{Payment::ACCEPTED_STATUSES[:authorized]}) || (status == #{Payment::ACCEPTED_STATUSES[:captured]}))" do
    self.authorized_at = DateTime.now
  end

  before_save if: "status_changed? && status == #{Payment::ACCEPTED_STATUSES[:canceled]}" do
    self.canceled_at = DateTime.now
  end

end
