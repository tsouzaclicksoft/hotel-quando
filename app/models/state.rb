class State < ActiveRecord::Base

  # class methods
  has_many :cities
  belongs_to :country

  # class methods
  scope :by_name, -> name { where("name ILIKE '%#{name}%'")}

end
