class TravelRequest < ActiveRecord::Base

	ACCEPTED_REQUEST_STATUS = {completed: 1, pending_agency: 2, pending_request: 3, canceled: 4}

	ACCEPTED_RESERVATION_STATUS = {issued: 1, waiting_for_issue: 2}

	ACCEPTED_PAYMENT_STATUS = { waiting_for_payment: 1, confirmed: 2, canceled: 3, 
		card_payment_error: 4, billed: 5, confirmed_and_paid: 6, waiting_for_payment_method: 7, expired:255}

	ACCEPTED_TYPE_PAYMENT = { credit_card: 1, billed: 1}

	CURRENCY_SYMBOL = { 'R$' => 1, 'US$' => 2, '€' => 3, '£' => 4, 'COP' => 5,
                      'ARS' => 6, '₹' => 7, '$(MXN)' => 8, 'S/.' => 9, '$(CLP)' => 10, 'ECS' => 11}
  	CURRENCY_SYMBOL_REVERSED = Hash[CURRENCY_SYMBOL.map {|k,v| [v, k]}]

	# associations
	belongs_to :user
	belongs_to :hq_consultant
	has_many   :airs
	
	accepts_nested_attributes_for :airs

	# validations
	validates  :request_status, inclusion: { in: TravelRequest::ACCEPTED_REQUEST_STATUS.values }
	validates  :payment_status, inclusion: { in: TravelRequest::ACCEPTED_PAYMENT_STATUS.values }


end
