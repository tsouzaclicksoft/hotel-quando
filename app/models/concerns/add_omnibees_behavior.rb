
module AddOmnibeesBehavior
  extend ActiveSupport::Concern

  included do
    # scopes
    scope :omnibees, -> { where("omnibees_code IS NOT NULL AND omnibees_code != ''") }
    before_save :sanitize_omnibees_code
  end

  # this method can be sunset - removed - using new one: is_omnibees_or_hoteisnet?
  def is_omnibees?
    !omnibees_code.blank?
  end

  def is_omnibees_or_hoteisnet?
    !omnibees_code.blank?
  end

  def sanitize_omnibees_code
    self.omnibees_code = self.omnibees_code.to_s.strip
  end
end

