module DirectUploadFileProcessing
  extend ActiveSupport::Concern

  included do
    after_save do |resource|
      resource.photos
      .select { |mi| mi.processing_status == 'waiting_for_direct_url_processing' }
      .each do |photo|
        ProcessPhotoWorker.perform_async(photo.id.to_s)
      end
    end
  end

  def process_photos!
    self.photos.each do |photo|
      photo.process_direct_upload! if photo.processing_status == 'waiting_for_direct_url_processing'
    end
  end
end