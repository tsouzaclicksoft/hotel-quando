class Currency < ActiveRecord::Base

  scope :dollar, -> { find_by(money_sign: "US$")}
  scope :by_money_sign, ->(money_sing) { where(money_sign: money_sing) }
  scope :in_day, -> (day) { where(:created_at => day.beginning_of_day..day.end_of_day) }
  scope :group_by_name, -> { all.group_by{ | currency | currency.name} }
  scope :by_code_or_money_sign, -> (code_or_money_sign) { where("money_sign = ? OR code = ?", code_or_money_sign, code_or_money_sign)}

# validations
  validates :name, :country, :value, :money_sign, :code, presence: true


end
