class SearchLogsUser < ActiveRecord::Base

  # relations
  has_many   :hotel_search_logs

  # scopes
  scope :by_email, -> email { where(email: email) }

  # validations
  validates :email, presence: true, format: { with: /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/ }

end
