class EasyTaxiVoucher < ActiveRecord::Base

  # validations
  validates_uniqueness_of :code
  validates_uniqueness_of :booking_id, allow_blank: true

  # class methods
  scope :not_linked, -> { where(booking_id: nil) }
  scope :linked, -> { where("booking_id IS NOT NULL") }
  scope :expires_after, -> date { where("expiration_date >= ?", date.in_time_zone) }
  scope :available_on, -> date { not_linked.expires_after(date) }

  # relations
  belongs_to :booking
end
