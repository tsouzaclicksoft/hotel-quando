class BookingPackContractingCompany < ActiveRecord::Base
	# relations
	belongs_to      :booking_pack

  # validations
  validates   :booking_pack_id, presence: true
  validates   :email, presence: true
  validates   :name, presence: true
  validates   :cnpj, presence: true
  validates   :responsible_name, presence: true

  # class methods
  scope :by_booking_pack_id, -> id { where(booking_pack_id: id)}

  # Instance methods
  def room_type
    RoomType.find(self.cached_room_type_id)
  end  
end