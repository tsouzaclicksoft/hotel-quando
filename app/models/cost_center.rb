class CostCenter < ActiveRecord::Base
  # Validations
  validates :name, presence: true
  validates :company_detail, presence: true

  # Relations
  belongs_to 	:company_detail
  has_many  	:travelers
  #has_many  :photos
  #accepts_nested_attributes_for :photos, :allow_destroy => true

  # Instance methods
end
