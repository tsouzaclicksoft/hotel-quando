class Newsletter < ActiveRecord::Base
  require 'csv'
  # Validations
  Email_Format = /\A^$|[-_0-9a-z.+]+@([-a-z0-9.]+)+\.[a-z]{2,4}\z/i
  validates :email, presence: true
  validates_format_of :email, with: Email_Format
  validates :city, presence: true
  validates :country, presence: true

   # class methods
  scope :by_id, -> id { where(id: id)}
  scope :by_email, -> email { where(email: email)}
  scope :by_city, -> city { where("city ILIKE '%#{city}%'")}

  def self.import(file)
    CSV.foreach(file.path, headers: true, :encoding => 'utf-8') do |row|
      unless Newsletter.where(email: row["Email"]).exists?
        news = Newsletter.new(email: row["Email"], city: row['Cidade'], contry: row['País'])
        news.save
      end
    end # end CSV.foreach
  end
end
