class RoomType < ActiveRecord::Base
  include DirectUploadFileProcessing
  include AddOmnibeesBehavior

  ACCEPTED_MAXIMUM_CAPACITY = 30

  # relations
  has_many   :booking_packs
  has_many   :rooms
  has_many   :photos
  has_many   :offers
  has_many   :bookings
  has_many   :room_type_pricing_policy
  belongs_to :hotel
  accepts_nested_attributes_for :photos, :allow_destroy => true

  has_many :active_rooms, -> { where is_active: true }, class_name: 'Room'


  # validations

  validates :name_pt_br, presence: true
  # validates :description_en, presence: true
  # validates :description_pt_br, presence: true
  validates :initial_capacity, presence: true
  validates :maximum_capacity, presence: true
  validates :initial_capacity, inclusion: { in: (1..10) }
  validates :hotel_id, presence: true
  validates :single_bed_quantity, presence: true
  validates :double_bed_quantity, presence: true
  #validates_uniqueness_of :name_pt_br, scope: :hotel_id
  validates_numericality_of :maximum_capacity, less_than_or_equal_to: 30
  validates_numericality_of :maximum_capacity, greater_than_or_equal_to: ->(room_type) { room_type.initial_capacity }
  validates_numericality_of :single_bed_quantity, less_than_or_equal_to: 30
  validates_numericality_of :single_bed_quantity, greater_than_or_equal_to: 0
  validates_numericality_of :double_bed_quantity, less_than_or_equal_to: 15
  validates_numericality_of :double_bed_quantity, greater_than_or_equal_to: 0
  validate                  :room_type_should_have_at_least_one_bed

  # hooks
  before_validation :set_initial_capacity

  # class methods
  scope :by_hotel_id, -> hotel_id { where(hotel_id: hotel_id) }
  scope :with_active_rooms, -> { where(active_rooms.count > 0) }

  def rooms_with_active_offer_count_per_day(month_number, year)
    initial_timestamp = Time.new(year, month_number).utc.beginning_of_month.strftime("%Y-%m-%d %H:%M:%S")
    last_timestamp = Time.new(year, month_number).utc.end_of_month.strftime("%Y-%m-%d %H:%M:%S")
    query = %Q{
      SELECT COUNT(room_id) as room_count, day_checkin_timestamp
      FROM (
        SELECT room_id, extract(day from checkin_timestamp) AS day_checkin_timestamp
        FROM offers
        WHERE
          (offers.checkin_timestamp BETWEEN '#{initial_timestamp}' AND '#{last_timestamp}') AND
          offers.hotel_id = #{hotel_id} AND
          offers.status = #{Offer::ACCEPTED_STATUS[:available]} AND
          offers.room_type_id = #{id}
          GROUP BY extract(day from checkin_timestamp), room_id
      ) t
      GROUP BY day_checkin_timestamp
    }

    Offer.connection.execute(query).to_a.inject(Hash.new(0)) do |memo, row_hash|
      memo[row_hash["day_checkin_timestamp"].to_i] = row_hash["room_count"]
      memo
    end
  end

  def bed_configuration_text_pt_br
    if double_bed_quantity > 0
      single_text = " e #{single_bed_quantity} de Solteiro" if single_bed_quantity > 0
      noun = double_bed_quantity > 1 ? 'Camas' : 'Cama'
      full_text = "#{double_bed_quantity} #{noun} de Casal#{single_text}"
    else
      noun = single_bed_quantity > 1 ? 'Camas' : 'Cama'
      full_text = "#{single_bed_quantity} #{noun} de Solteiro"
    end
  end

  def bed_configuration_text_en
    single_bed_noun = single_bed_quantity > 1 ? 'Beds' : 'Bed'

    if double_bed_quantity > 0
      single_text = " and #{single_bed_quantity} Single #{single_bed_noun}" if single_bed_quantity > 0
      noun = double_bed_quantity > 1 ? 'Beds' : 'Bed'
      double_text = "#{double_bed_quantity} Matrimonial #{noun}"
      full_text = "#{double_text}#{single_text}"
    else
      "#{single_bed_quantity} Single #{single_bed_noun}"
    end

  end

  def bed_configuration_text_es
    single_noun = single_bed_quantity > 1 ? 'individuales' : 'individual'
    if double_bed_quantity > 0
      single_text = " e #{single_bed_quantity} #{single_noun}" if single_bed_quantity > 0
      noun = double_bed_quantity > 1 ? 'Camas' : 'Cama'
      full_text = "#{double_bed_quantity} #{noun} doble#{single_text}"
    else
      noun = single_bed_quantity > 1 ? 'Camas' : 'Cama'
      full_text = "#{single_bed_quantity} #{noun} #{single_noun}"
    end
  end

  def full_name_pt_br
    "#{name_pt_br} - #{bed_configuration_text_pt_br}"
  end

  def full_name_en
    "#{name_en} - #{bed_configuration_text_en}"
  end

  def full_name_es
    "#{name_es} - #{bed_configuration_text_es}"
  end

  # instance methods
  private

  def set_initial_capacity
    self.initial_capacity = 1
  end

  def room_type_should_have_at_least_one_bed
    if (self.double_bed_quantity == 0) && (self.single_bed_quantity == 0)
      errors.add(:single_bed_quantity, :should_have_at_least_one_bed)
      errors.add(:double_bed_quantity, :should_have_at_least_one_bed)
    end
  end

end
