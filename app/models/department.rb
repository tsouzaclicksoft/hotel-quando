# encoding: utf-8
class Department < ActiveRecord::Base
  # Validations
  validates :name, presence: true
  validates :company, presence: true

  # Relations
  has_many :employees, foreign_key: 'department_id', :class_name => "User"
  belongs_to :company
end
