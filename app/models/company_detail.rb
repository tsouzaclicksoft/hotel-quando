class CompanyDetail < ActiveRecord::Base
  # Validations
  #validates :name, presence: true
  validates           :street, presence: true, if: "new_record? && agency_detail_id.blank?"
  validates           :city, presence: true, if: "new_record? && agency_detail_id.blank?"
  validates           :state, presence: true, if: "new_record? && country != 'CO' && agency_detail_id.blank?"
  validates           :country, presence: true, if: "new_record? && agency_detail_id.blank?"
  validates           :postal_code, presence: true, if: "new_record? && country != 'CO' && agency_detail_id.blank?"
  #validates           :number, presence: true, if: "new_record?"

  # Relations
  belongs_to 	:user
  belongs_to 	:agency_detail
  has_many 		:cost_centers
  has_many    :job_titles
  has_many 		:travelers
  belongs_to  :executive_responsible
  has_one     :company_universe

  #has_many  :photos
  #accepts_nested_attributes_for :photos, :allow_destroy => 

  #validates :cnpj, presence: true, uniqueness: true, if: "new_record?"

  # Instance methods

  # class methods
  scope :with_cnpj, -> { where.not(cnpj: [nil, '']) }
  scope :by_name, -> value { where("name ILIKE '%#{value}%'")}
  scope :by_executive_service, -> executive { where("executive_service ILIKE '%#{executive}%'")}
  scope :by_cnpj, -> cnpj { where(cnpj: cnpj)}
  scope :by_created_at, -> initial_date, final_date = nil { where("created_at BETWEEN '#{(initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}' AND '#{(final_date.try(:to_date) || initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}'")}
  scope :by_executive_responsible, -> value { joins(:company_universe).where("company_universes.executive_responsible_id = #{value.split(', ')[0]}")}

  def self.getAllCompanyDetails(params)
     return CompanyDetail.where(agency_detail_id: nil).includes(:user).page(params[:page]).per(40)
  end

  def self.getComapaniesOrdenedInMonth(company_details)
    @company_details = company_details
    lastMonth = DateTime.now - 1.month
    delayedMonth = DateTime.now - 2.month
    return @company_details.map {|c| {

      actualMonth: Booking.where("user_id = (?) AND created_at BETWEEN (?) AND (?) AND status <> 1 AND status <> 3 AND status <> 255", c.user_id, DateTime.now.beginning_of_month, DateTime.now.end_of_month).count(:id),
      lastMonth: Booking.where("user_id = (?) AND created_at BETWEEN (?) AND (?) AND status <> 1 AND status <> 3 AND status <> 255", c.user_id, lastMonth.beginning_of_month , lastMonth.end_of_month).count(:id),
      delayedMonth: Booking.where("user_id = (?) AND created_at BETWEEN (?) AND (?) AND status <> 1 AND status <> 3 AND status <> 255", c.user_id, delayedMonth.beginning_of_month, delayedMonth.end_of_month).count(:id) ,
      company: c, 
      subtract: Booking.where("user_id = (?) AND created_at BETWEEN (?) AND (?) AND status <> 1 AND status <> 3 AND status <> 255", c.user_id, DateTime.now.beginning_of_month, DateTime.now.end_of_month).count(:id) - Booking.where("user_id = (?) AND created_at BETWEEN (?) AND (?) AND status <> 1 AND status <> 3 AND status <> 255", c.user_id, lastMonth.beginning_of_month, lastMonth.end_of_month).count(:id)
    }}.sort_by  {|c| c[:subtract]}

  end

  def self.update_company_detail_equals_cnpj(id_executive, company_detail)
    company_detail.executive_responsible_id = id_executive
    company_detail.save
  end

end
