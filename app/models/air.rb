class Air < ActiveRecord::Base

	ACCEPTED_CLASSES = { economy: 1, premium_economy: 2, business: 3, first_class: 4}	
	
	# associations
	belongs_to :travel_request
	belongs_to :traveler
	has_one :air_taxe
	
	accepts_nested_attributes_for :air_taxe

	# validations
	validates  :flight_class, inclusion: { in: Air::ACCEPTED_CLASSES.values }

end
