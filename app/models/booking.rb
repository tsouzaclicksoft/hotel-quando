# encoding: utf-8
class Booking < ActiveRecord::Base
  include AddOmnibeesBehavior
  include Shared::CurrencyCurrentHelper
  audited only: [:updated_at, :expired_at, :status, :canceled_by_admin]

  ACCEPTED_STATUSES = { waiting_for_payment: 1, confirmed: 2, canceled: 3, no_show_paid_with_success: 4, no_show_with_payment_error: 5, confirmed_and_captured: 6, no_show_with_payment_pending: 7, confirmed_and_invoiced: 8, confirmed_without_card: 9, requested_reservation: 10, requested_and_paid_reservation: 11 , invoiced_by_agency: 12, expired:255}
  ACCEPTED_STATUS_REVERSED = Hash[ACCEPTED_STATUSES.map {|k,v| [v, k]}]
  CANCELLABLE_STATUSES = [ Booking::ACCEPTED_STATUSES[:waiting_for_payment], Booking::ACCEPTED_STATUSES[:confirmed], Booking::ACCEPTED_STATUSES[:confirmed_and_captured], Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced], Booking::ACCEPTED_STATUSES[:confirmed_without_card], Booking::ACCEPTED_STATUSES[:invoiced_by_agency] ]
  BOOKING_TAXES = { fee_consumer_for_pack_3h: 1, fee_consumer_for_pack_6h: 2, fee_consumer_for_pack_9h: 3, fee_consumer_for_pack_12h: 4, financial_costs_percentage_to_public: 5, financial_costs_percentage_to_affiliate: 6}
  BOOKING_TAXES_REVERSED = Hash[BOOKING_TAXES.map {|k,v| [v, k]}]

  # HotelQuando send the hotel's invoice on the 10th day of every month
  # So we allow Hotel to set a booking as no-show only until the
  # final of the 4th day of every month
  LIMIT_DAY_TO_SET_NOSHOW = 4

  class DontKnowIfBookingStatusIsActiveError < StandardError; end
  class BookingHotelMinimumHoursOfNoticeError < StandardError; end
  class BookingStatusNotCancellableError < StandardError; end
  class BookingNotConfirmedError < StandardError; end
  class NotInTheTimeToSetAsNoShowError < StandardError; end

  lastMonth = Date.today - 1.month
  delayedMonth = Date.today - 2.month

  # associations
  belongs_to :affiliate
  belongs_to :invoice
  belongs_to :hotel
  belongs_to :company
  belongs_to :user
  belongs_to :traveler
  belongs_to :consultant
  belongs_to :meeting_room_offer
  belongs_to :event_room_offer
  belongs_to :offer
  belongs_to :business_room
  belongs_to :room
  belongs_to :omnibees_offer
  belongs_to :room_type
  has_many   :payments
  has_many   :refunds, through: :payments
  has_one    :easy_taxi_voucher
  has_one    :uber_voucher
  has_one    :promotional_code_log
  belongs_to :executive_responsible

  # hooks
  before_save :set_booking_active_status

  # class methods
  scope :by_consultant_id, -> id { where(consultant_id: id)}
  scope :by_client_id, -> id { joins(traveler: :cost_center).where("cost_centers.company_detail_id = #{id}") }
  scope :by_cost_center_id, -> id { joins(:traveler).where("travelers.cost_center_id = #{id}")}
  scope :by_traveler_id, -> id { where(traveler_id: id)}
  scope :by_id, -> id { where(id: id)}
  scope :by_actual_month, -> { where("created_at BETWEEN (?) AND (?) AND status <> 1 AND status <> 3 AND status <> 255", Date.today.beginning_of_month.strftime('%Y-%m-%d %H:%M:%S'), Date.today.end_of_month.strftime('%Y-%m-%d %H:%M:%S'))}
  scope :by_last_month, -> { where("created_at BETWEEN (?) AND (?) AND status <> 1 AND status <> 3 AND status <> 255", lastMonth.beginning_of_month.strftime('%Y-%m-%d %H:%M:%S') , lastMonth.end_of_month.strftime('%Y-%m-%d %H:%M:%S'))}
  scope :by_delayed_month, -> {where("created_at BETWEEN (?) AND (?) AND status <> 1 AND status <> 3 AND status <> 255", delayedMonth.beginning_of_month.strftime('%Y-%m-%d %H:%M:%S'), delayedMonth.end_of_month.strftime('%Y-%m-%d %H:%M:%S'))}
  scope :by_hotel_id, -> id { where(hotel_id: id)}
  scope :by_user_id, -> id { where(user_id: id)}
  scope :by_company_id, -> id { where(company_id: id)}
  scope :by_guest_name, -> guest_name { where("guest_name ILIKE '%#{guest_name}%'")}
  scope :by_status, -> status { where(status: status.to_s.gsub('[','').gsub(']','').split(",").map(&:strip)) }
  scope :by_created_at, -> initial_date, final_date = nil { where("created_at BETWEEN '#{(initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}' AND '#{(final_date.try(:to_date) || initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}'")}
  scope :by_checkin_date, -> initial_date, final_date = nil { where("checkin_date BETWEEN '#{(initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}' AND '#{(final_date.try(:to_date) || initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}'")}
  scope :by_checkout_date, -> initial_date, final_date = nil { joins(:offer).where("offers.checkout_timestamp BETWEEN '#{initial_date.to_time.beginning_of_day.strftime('%Y-%m-%d %H:%M:%S')}' AND '#{(final_date.try(:to_time) || initial_date.to_time).end_of_day.strftime('%Y-%m-%d %H:%M:%S')}'")}
  scope :by_business_room_id, -> business_room_id { where(business_room_id: business_room_id)}
  scope :by_room_type_id, -> room_type_id { where(room_type_id: room_type_id)}
  scope :by_pack_length, -> pack_length { where(pack_in_hours: pack_length)}
  scope :by_checkout_delay, -> checkout_delay { where(delayed_checkout_flag: checkout_delay)}
  scope :by_refund_error, -> refund_error { where(refund_error: refund_error)}
  scope :by_agency_creation, -> agency_creation { where(created_by_agency: agency_creation) }
  scope :by_room_numbers, -> room_numbers_array do
    room_numbers = room_numbers_array.to_s.gsub('[','').gsub(']','').split(",").map(&:strip)
    possible_rooms_ids = Room.where("number IN (?)", room_numbers).pluck(:id).to_a
    possible_rooms_ids.empty? ? none : where("room_id IN (?)", possible_rooms_ids)
  end
  scope :paid, -> { by_status([
    ACCEPTED_STATUSES[:confirmed],
    ACCEPTED_STATUSES[:confirmed_and_captured],
    ACCEPTED_STATUSES[:confirmed_without_card],
    ACCEPTED_STATUSES[:confirmed_and_invoiced],
    ACCEPTED_STATUSES[:no_show_paid_with_success],
    ACCEPTED_STATUSES[:invoiced_by_agency]
  ]) }
  scope :uses_tudo_azul, -> { where(uses_tudo_azul: true) }

  scope :by_multiplus_code, -> mutiplus_code { where.not(multiplus_code: nil) if mutiplus_code.eql?('true') }

  scope :by_meeting_room_offer, -> meeting_room_offer { where.not(meeting_room_offer_id: nil) if meeting_room_offer.eql?('true')}
  scope :by_event_room_offer, -> event_room_offer { where.not(event_room_offer_id: nil) if event_room_offer.eql?('true')}
  scope :by_room_offer, -> room_offer { where(meeting_room_offer_id: nil, event_room_offer_id: nil) if room_offer.eql?('true')}
  scope :conflicting_with_offer, -> omnibees_offer do
    where("room_type_id = ? AND is_active = true AND (bookings.date_interval @> ?::timestamp OR bookings.date_interval @> ?::timestamp)",
    omnibees_offer.room_type_id, omnibees_offer.checkin_timestamp.utc, omnibees_offer.checkout_timestamp.utc)
  end

  # validations
  validate                  :number_of_people_should_be_less_than_or_equal_to_offer_room_type_capacity, if: 'business_room.nil?'
  validates                 :status, inclusion: { in: Booking::ACCEPTED_STATUSES.values }
  validates                 :guest_name, presence: true
  validates                 :currency_code, presence: true
  validates                 :phone_number, presence: true, if: "new_record?"
  validates                 :status, presence: true
  validates                 :user_id, presence: true
  validates                 :business_room_id, presence: true, if: 'room.nil? && room_type.nil?'
  validates                 :room_id, presence: true , if: 'business_room.nil?'
  validates                 :room_type_id  , presence: true, if: 'business_room.nil?'
  validates                 :meeting_room_offer, presence: true, if: 'offer.nil? && omnibees_offer.nil? && event_room_offer.nil?'
  validates                 :event_room_offer, presence: true, if: 'offer.nil? && omnibees_offer.nil? && meeting_room_offer.nil?'
  validates                 :offer, presence: true, if: 'omnibees_offer.nil? && event_room_offer.nil? && meeting_room_offer.nil?'
  validates                 :omnibees_offer, presence: true, if: 'offer.nil? && event_room_offer.nil? && meeting_room_offer.nil?'
  validates_numericality_of :number_of_people, greater_than_or_equal_to: 1
  validates_numericality_of :balance_value_used, greater_than_or_equal_to: 0

  # instance_methods
  def can_use_credit_and_have_founds_balance?
    self.can_use_credit_balance? && self.user.balance > 0
  end

  def can_use_credit_balance?
    self.using_balance && self.currency_code == "BRL"
  end

  def guest
    self.traveler_id.present? ? self.traveler : self.user
  end

  def have_discount_in_offer_price?
    self.status == Booking::ACCEPTED_STATUSES[:confirmed] && self.promotional_code_log && self.promotional_code_log.promotional_code.discount_in == PromotionalCode::ACCEPTED_DISCOUNT_IN[:offer_price]
  end

  def will_be_paid_online?
    if self.is_really_hoteisnet? && self.offer.hotel.country == "CO"
      return false
    elsif self.offer.hotel.accept_online_payment
      return true
    else
      return false
    end
    #(self.hotel.accept_online_payment && !self.is_really_omnibees?) || self.is_really_hoteisnet? || self.have_promotional_code || self.traveler_id.present? || self.can_use_credit_and_have_founds_balance?
    #(self.hotel.accept_online_payment && !self.is_really_omnibees?) || self.is_really_hoteisnet? || self.traveler_id.present? || self.can_use_credit_and_have_founds_balance?
  end

  def booking_tax_in_br_currency
    country_id = Country.where(iso_initials: self.offer.hotel.country).first.id
    if self.is_meeting_offer?
      tax = BookingTax.where(name: "fee_consumer_for_pack_3h").where(country_id: country_id).first.value
    else
      if self.pack_in_hours > 12
        tax = BookingTax.where(name: "fee_consumer_for_pack_12h").where(country_id: country_id).first.value
      else
        tax = BookingTax.where(name: "fee_consumer_for_pack_#{self.pack_in_hours}h").where(country_id: country_id).first.value
      end
    end
    calculate_currency_tax(Hotel::CURRENCY_SYMBOL_REVERSED[self.offer.hotel.currency_symbol], tax)
  end


  def set_booking_active_status
    if self.status.in? [Booking::ACCEPTED_STATUSES[:waiting_for_payment], Booking::ACCEPTED_STATUSES[:confirmed], Booking::ACCEPTED_STATUSES[:confirmed_and_captured], Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced], Booking::ACCEPTED_STATUSES[:confirmed_without_card], Booking::ACCEPTED_STATUSES[:requested_reservation], Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation], ACCEPTED_STATUSES[:invoiced_by_agency]  ]
      self.is_active = true
    elsif self.status.in? [Booking::ACCEPTED_STATUSES[:canceled], Booking::ACCEPTED_STATUSES[:expired], Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], Booking::ACCEPTED_STATUSES[:no_show_with_payment_error], Booking::ACCEPTED_STATUSES[:no_show_with_payment_pending] ]
      self.is_active = false
    else
      raise DontKnowIfBookingStatusIsActiveError.new('Dont know if this booking is active or not, plese review the booking model')
    end
    true
  end

  def tudo_azul_associate!(cpf = nil)
    self.uses_tudo_azul = true
    self.user.cpf = cpf if cpf.present?
    self.save!
    self.user.save!
  end

  def tudo_azul_unassociate!
    self.uses_tudo_azul = false
    self.save!
  end

  def offer
    super || omnibees_offer || meeting_room_offer || event_room_offer || offer_nil_quick_fix
  end

  def offer_nil_quick_fix
    o = Offer.new
    time_around = Offer::ROOM_CLEANING_TIME-1.minute
    o.checkin_timestamp = self.date_interval.first + time_around
    o.checkout_timestamp = self.date_interval.last - time_around
    o.pack_in_hours = self.pack_in_hours
    o.price = self.cached_offer_price
    o.status = 5
    o.room_type_initial_capacity = self.cached_room_type_initial_capacity
    o.room_type_maximum_capacity = self.cached_room_type_maximum_capacity
    o.no_show_value = self.cached_offer_no_show_value
    o.room_id = self.room_id
    o.room_type_id = self.room_type_id
    o.hotel_id = self.hotel_id
    o.log_id = 0
    o.checkin_timestamp_day_of_week = o.checkin_timestamp.to_date.cwday
    o.extra_price_per_person = self.cached_offer_extra_price_per_person
    o
  end

  def offer=(offer)
    if offer.is_a?(OmnibeesOffer)
      self.omnibees_offer = offer
    elsif offer.is_a?(MeetingRoomOffer)
      self.meeting_room_offer = offer
    elsif offer.is_a?(EventRoomOffer)
      self.event_room_offer = offer
    else
      super
    end
    self.cached_offer_price = offer.price.to_f
    self.pack_in_hours = offer.pack_in_hours
    self.cached_room_type_initial_capacity = offer.room_type.initial_capacity
    self.cached_room_type_maximum_capacity = offer.room_type.maximum_capacity
    self.cached_offer_extra_price_per_person = offer.extra_price_per_person.to_f
    self.cached_offer_no_show_value = offer.no_show_value.to_f
  rescue
  end

  def can_accuse_delayed_checkout?
    if (status != ACCEPTED_STATUSES[:confirmed]) && (status != ACCEPTED_STATUSES[:confirmed_and_captured]) && (status != Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) && (status != Booking::ACCEPTED_STATUSES[:confirmed_without_card]) && (status != Booking::ACCEPTED_STATUSES[:invoiced_by_agency])
      return false
    elsif (self.offer.checkout_timestamp.strftime('%Y-%m-%d %H:%M:%S -0300').to_time + 1.hour) > Time.now
      return false
    elsif booking_with_100_percent_discount
      return false
    else
      checkout_next_month_limit = offer.checkout_timestamp.next_month.strftime("%Y-%m-#{LIMIT_DAY_TO_SET_NOSHOW} 23:59:59 -0300").to_time
      return Time.now.between?(offer.checkout_timestamp.strftime('%Y-%m-%d %H:%M:%S -0300').to_time, checkout_next_month_limit)
    end
  end

  def accuse_delayed_checkout
    raise unless self.can_accuse_delayed_checkout?
    self.delayed_checkout_flag = true
    self.save!
  end

  def guest_delayed_checkout?
    return self.delayed_checkout_flag
  end

  def company_mark_up_in_br_currency
    if self.company.markup
      return self.company.markup_value
    else
      return 0
    end
  end

  def booking_tax_financial_costs_percentage_to_public
    BookingTax.where(name: "financial_costs_percentage_to_public").where(country_id: Country.where(iso_initials: self.offer.hotel.country).first.id).first.value
  end

  def booking_tax_financial_costs_percentage_to_affiliate
    BookingTax.where(name: "financial_costs_percentage_to_affiliate").where(country_id: Country.where(iso_initials: self.offer.hotel.country).first.id).first.value
  end

  def booking_tax_to_affiliates
    if will_be_paid_online?
      return total_price_without_tax * booking_tax_financial_costs_percentage_to_affiliate / 100 + booking_tax_in_br_currency
    else
      return booking_tax_in_br_currency
    end
  end

  def booking_tax_to_public
    if is_really_hoteisnet? && self.offer.hotel.country == "CO"
      return booking_tax_in_br_currency
    elsif self.offer.hotel.accept_online_payment
      return total_price_without_tax * booking_tax_financial_costs_percentage_to_public / 100 + booking_tax_in_br_currency
    else
      return booking_tax_in_br_currency
    end
  end

  def booking_tax_to_companies
    if self.offer.hotel.accept_invoiced_payment && self.offer.hotel.accept_invoiced_by_agency
      return 0
    else
      return booking_tax_in_br_currency
    end
  end


  def booking_tax
    if self.new_record? && !self.booking_tax_changed?
      if self.traveler_id.present?
        booking_tax_to_companies
      elsif self.created_by_affiliate?
        booking_tax_to_affiliates
      else
        booking_tax_to_public
      end
    else
      if self.promotional_code_log
        if self.promotional_code_log.promotional_code.discount_in == PromotionalCode::ACCEPTED_DISCOUNT_IN[:offer_price]
          read_attribute(:booking_tax)
        else
          if read_attribute(:booking_tax) <= self.promotional_code_log.promotional_code.discount
            read_attribute(:booking_tax) - read_attribute(:booking_tax)
          else
            read_attribute(:booking_tax) - self.promotional_code_log.promotional_code.discount
          end
        end
      else
        read_attribute(:booking_tax)
      end
    end
  end

  def no_show_value
    if self.promotional_code_log
      promotional_code = self.promotional_code_log.promotional_code
      no_show_value_without_discount = self.no_show_value_without_tax + self.booking_tax
      return  promotional_code.booking_value_with_discount(no_show_value_without_discount, promotional_code_log.currency_code, Hotel::CURRENCY_SYMBOL_REVERSED[self.hotel.currency_symbol])
    elsif self.can_use_credit_balance?
      return (self.no_show_value_without_tax - self.discount_value_balance) + booking_tax
    else      
      return self.no_show_value_without_tax + self.booking_tax
    end
  end

  def no_show_value_for_hotels_if_booking_is_invoiced
      if self.is_meeting_offer?
        value_to_reduce = 0
      elsif self.is_event_offer?
        value_to_reduce = 0
      else
        value_to_reduce = self.no_show_value * HotelQuando::Constants::DEFAULT_REDUCE_PRICE_PERCENTAGE_FOR_EACH_PACK[ self.pack_in_hours.to_s + '_hours'].to_f / 100.0
      end
    return self.no_show_value - value_to_reduce
  end

  def no_show_value_without_tax # doing this because there is a lot of places that use the no_show_value method to show the final value to charge or show to the user
    # this is being used in the reports
    return self.cached_offer_no_show_value
  end


  def no_show_value_without_tax_with_discount
    promotional_code = self.promotional_code_log.promotional_code
    if promotional_code.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:money_value]
      if promotional_code.discount > 15
        return no_show_value_without_tax - 15
      else
        return no_show_value_without_tax - promotional_code.discount
      end
    end
  end

  def no_show_value_if_booking_is_invoiced_without_tax
    return self.no_show_value_for_hotels_if_booking_is_invoiced - self.booking_tax
  end

  def initial_price
    return self.cached_offer_price
  end

  def initial_price_with_discount
    promotional_code = self.promotional_code_log.promotional_code
    price_without_discount = self.cached_offer_price
    return  promotional_code.booking_value_with_discount(price_without_discount, promotional_code_log.currency_code, Hotel::CURRENCY_SYMBOL_REVERSED[self.hotel.currency_symbol])
  end

  def initial_price_for_hotels_if_booking_is_invoiced
    if self.promotional_code_log
      value_to_reduce = self.initial_price_with_discount * HotelQuando::Constants::DEFAULT_REDUCE_PRICE_PERCENTAGE_FOR_EACH_PACK[ self.pack_in_hours.to_s + '_hours'].to_f / 100.0
      return self.initial_price_with_discount - value_to_reduce
    else
      value_to_reduce = self.initial_price * HotelQuando::Constants::DEFAULT_REDUCE_PRICE_PERCENTAGE_FOR_EACH_PACK[ self.pack_in_hours.to_s + '_hours'].to_f / 100.0
      return self.initial_price - value_to_reduce
    end
  end

  def extra_people_price
    return self.total_price_without_discount - self.initial_price - self.booking_tax
  end

  def extra_people_price_with_discount
    price = self.total_price - self.initial_price_with_discount - self.booking_tax
      if price != 0
        return price
      else
        return 0
      end
  end

  def extra_people_price_for_hotels_if_booking_is_invoiced
    if self.promotional_code_log
      value_to_reduce = self.extra_people_price_with_discount * HotelQuando::Constants::DEFAULT_REDUCE_PRICE_PERCENTAGE_FOR_EACH_PACK[ self.pack_in_hours.to_s + '_hours'].to_f / 100.0
      return self.extra_people_price_with_discount - value_to_reduce
    else
      value_to_reduce = self.extra_people_price * HotelQuando::Constants::DEFAULT_REDUCE_PRICE_PERCENTAGE_FOR_EACH_PACK[ self.pack_in_hours.to_s + '_hours'].to_f / 100.0
      return self.extra_people_price - value_to_reduce
    end
  end

  def total_price_without_discount
    total_price = self.cached_offer_price
    unless self.is_omnibees?
      if self.offer_id.present?
        if self.number_of_people <= self.cached_room_type_initial_capacity
          total_price = self.cached_offer_price
        elsif self.number_of_people <= self.cached_room_type_maximum_capacity
          exceeding_number_of_people = self.number_of_people - self.cached_room_type_initial_capacity
          total_price = (self.cached_offer_price + (exceeding_number_of_people * self.cached_offer_extra_price_per_person.to_f))
        else
          return -1
        end
      end
    end
    return total_price + booking_tax
  end

  def total_price_with_extra_person_not_tax
    total_price = self.cached_offer_price
    unless self.is_omnibees?
      if self.offer_id.present?
        if self.number_of_people <= self.cached_room_type_initial_capacity
          total_price = self.cached_offer_price
        elsif self.number_of_people <= self.cached_room_type_maximum_capacity
          exceeding_number_of_people = self.number_of_people - self.cached_room_type_initial_capacity
          total_price = (self.cached_offer_price + (exceeding_number_of_people * self.cached_offer_extra_price_per_person.to_f))
        else
          return -1
        end
      end
    end
    return total_price
  end

  def total_price_without_tax_and_without_discount
    total_price_without_discount - booking_tax
  end

  def total_price_if_discount_is_greater_than_15
    promotional_code = self.promotional_code_log.promotional_code
    if promotional_code.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:money_value]
      if promotional_code.discount > 15
        return total_price_without_tax + promotional_code.discount - 15
      else
        return total_price_without_tax
      end
    else
      discount_money_value = (total_price_without_discount * promotional_code.discount) / 100.0
      if discount_money_value > 15
        return total_price_without_tax + discount_money_value - 15
      else
        return total_price_without_tax
      end
    end

  end

  def total_price_for_hotels_if_booking_is_invoiced
    if self.is_meeting_offer?
      value_to_reduce = 0
    elsif self.is_event_offer?
      value_to_reduce = 0
    else
      value_to_reduce = self.total_price * HotelQuando::Constants::DEFAULT_REDUCE_PRICE_PERCENTAGE_FOR_EACH_PACK[ self.pack_in_hours.to_s + '_hours'].to_f / 100.0
    end
    return self.total_price - value_to_reduce
  end

  def total_price_if_status_is_invoiced_without_tax
    total_price_for_hotels_if_booking_is_invoiced - booking_tax
  end



  def total_price
    total_price = self.cached_offer_price
    unless self.is_omnibees?
      if self.offer_id.present?
        if self.number_of_people <= self.cached_room_type_initial_capacity
          total_price = self.cached_offer_price
        elsif self.number_of_people <= self.cached_room_type_maximum_capacity
          exceeding_number_of_people = self.number_of_people - self.cached_room_type_initial_capacity
          total_price = (self.cached_offer_price + (exceeding_number_of_people * self.cached_offer_extra_price_per_person.to_f))
        else
          return -1
        end
      end
    end
    if self.promotional_code_log && self.promotional_code_log.promotional_code.discount_in == PromotionalCode::ACCEPTED_DISCOUNT_IN[:offer_price]
      promotional_code = self.promotional_code_log.promotional_code
      total_price_without_discount = total_price + booking_tax
      return  promotional_code.booking_value_with_discount(total_price_without_discount, promotional_code_log.currency_code, Hotel::CURRENCY_SYMBOL_REVERSED[self.hotel.currency_symbol])
    elsif self.can_use_credit_balance?
      return total_price_without_tax_but_with_discount_balance + booking_tax
    else
      if self.hotel.is_brazilian
        return total_price + booking_tax
      else
        return total_price
      end
    end
  end

  def total_price_without_tax_and_discount_balance
    return total_price_without_tax_but_with_discount_balance + discount_value_balance
  end

  def total_price_without_tax_but_with_discount_balance
    total_price = total_price_with_extra_person_not_tax
    return total_price - discount_value_balance
  end

  def discount_value_balance
    total_price = total_price_with_extra_person_not_tax
    if self.can_use_credit_balance?
      if self.payments.last
        discount_value = self.balance_value_used
      else
        if self.user.balance > total_price
          discount_value = total_price
        else
          discount_value = self.user.balance
        end
      end
    else
      discount_value = 0
    end
    return discount_value
  end


  def total_price_without_tax
    total_price = self.cached_offer_price
    unless self.is_omnibees_or_hoteisnet?
      if self.offer_id.present?
        if self.number_of_people <= self.cached_room_type_initial_capacity
          total_price = self.cached_offer_price
        elsif self.number_of_people <= self.cached_room_type_maximum_capacity
          exceeding_number_of_people = self.number_of_people - self.cached_room_type_initial_capacity
          total_price = (self.cached_offer_price + (exceeding_number_of_people * self.cached_offer_extra_price_per_person.to_f))
        else
          return -1
        end
      end
    end
    if self.promotional_code_log
      promotional_code = self.promotional_code_log.promotional_code
      return promotional_code.booking_value_with_discount(total_price, promotional_code_log.currency_code, Hotel::CURRENCY_SYMBOL_REVERSED[self.hotel.currency_symbol])
    elsif self.can_use_credit_balance?
      return total_price_without_tax_but_with_discount_balance
    else
      return total_price
    end
  end

  def initial_price_without_iss
    return (self.initial_price * 100) / (self.hotel.iss_in_percentage + 100)
  end

  def extra_people_price_without_iss
    return (self.extra_people_price * 100) / (self.hotel.iss_in_percentage + 100)
  end

  def total_price_without_iss_and_without_tax
    return ((self.total_price_without_tax * 100) / (self.hotel.iss_in_percentage + 100))
  end

  def extra_price_per_person_without_iss
    return (self.cached_offer_extra_price_per_person * 100) / (self.hotel.iss_in_percentage + 100)
  end

  def extra_price_per_person
    return self.cached_offer_extra_price_per_person
  end

  # def hotel_quando_comission
  #   if /no_show/ === ACCEPTED_STATUS_REVERSED[self.status].to_s
  #     comission = (self.hotel.comission_in_percentage.to_f / 100) * no_show_value
  #   else
  #     comission = (self.hotel.comission_in_percentage.to_f / 100) * total_price_without_iss_and_without_tax
  #   end
  #   comission + booking_tax
  # end

  # def hotel_quando_comission_without_tax
  #   hotel_quando_comission - booking_tax
  # end

  def no_show_refund_value
    if self.pack_in_hours < 24
      return 0
    else
      return (self.total_price - self.no_show_value)
    end
  end

  def expire
    begin
      Booking.transaction do
        self.lock!
        raise unless (self.reload.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment])
        unless self.is_omnibees?
          self.release_offers
        end
        self.update!(status: Booking::ACCEPTED_STATUSES[:expired], expired_at: DateTime.now)
      end
      if self.created_by_agency?
        AgencyMailer.send_booking_expiration_notice(self).deliver
      else
        UserMailer.send_booking_expiration_notice(self).deliver
      end
      true
    rescue
      false
    end
  end

  def cancelable?
    if ((self.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (self.reload.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (self.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]) || (self.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (self.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_without_card])) && (self.hotel.accepts_booking_cancellation?)
      if (self.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - self.hotel.minimum_hours_of_notice.hours) > (Time.now)
        true
      else
        false
      end
    else
      false
    end
  end

  def cancelable_to_admin?
    if ((self.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (self.reload.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (self.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]) || (self.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (self.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_without_card]))
      if (self.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - 1.hour) > (Time.now)
        true
      else
        false
      end
    else
      false
    end
  end

  def cancel(admin = false)
    self_status = self.status
    payment_cancellation_response = ''
    payment = self.payments.last

    # If requires_new: true is not passed to the transaction, we cant test that the database will not
    # be changed in the case of a exception, because rspec already wrapps everything into one transaction
    Booking.transaction(requires_new: true) do
      self.lock!
      raise BookingStatusNotCancellableError.new unless (self.status.in?(CANCELLABLE_STATUSES))
      if admin
        raise BookingHotelMinimumHoursOfNoticeError.new if (self.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - 1.hour) < (Time.now)
      else
        raise BookingHotelMinimumHoursOfNoticeError.new if (!(self.hotel.accepts_booking_cancellation?) || (self.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - self.hotel.minimum_hours_of_notice.hours) < (Time.now))
      end
      self.release_offers unless self.is_omnibees?
      self.update!(status: Booking::ACCEPTED_STATUSES[:canceled], is_active: false)
      change_emkt_flux_on_cancellation(self.user)
      if (self_status == ACCEPTED_STATUSES[:confirmed]) || (self_status == ACCEPTED_STATUSES[:confirmed_and_captured]) || (self_status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (self_status == Booking::ACCEPTED_STATUSES[:confirmed_without_card]) || (self_status == Booking::ACCEPTED_STATUSES[:invoiced_by_agency])
        unless payment
          AdminMailer.send_booking_cancellation_notice(self).deliver
        end
        HotelMailer.send_booking_cancellation_notice(self).deliver
      end

    end

    UserMailer.send_booking_cancellation_notice(self).deliver

  end

  def set_as_no_show(options)
    raise Exception.new("Reserva não pode ser setada como no-show") unless can_be_set_as_no_show?
    next_status = if options[:payment] == :success
      Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
    elsif options[:payment] == :error
      Booking::ACCEPTED_STATUSES[:no_show_with_payment_error]
    elsif options[:payment] == :pending
      Booking::ACCEPTED_STATUSES[:no_show_with_payment_pending]
    else
      raise "Option not recognized"
    end
    if options[:refund] == :error
      self.update!(refund_error: true)
    end
    self.update!(status: next_status)
  end

  def can_be_set_as_no_show?
    return false if booking_with_100_percent_discount
    return false if ((status != ACCEPTED_STATUSES[:confirmed]) && (status != ACCEPTED_STATUSES[:confirmed_and_captured]) && (status != ACCEPTED_STATUSES[:confirmed_and_invoiced]) && (status != ACCEPTED_STATUSES[:confirmed_without_card]) && (status != ACCEPTED_STATUSES[:invoiced_by_agency]) ) || (self.guest_delayed_checkout?) # if the guest delayed checkout, how could he made a no-show?
    normalized_time_now = Time.now.strftime('%Y-%m-%d %H:%M:%S +0000').to_time.utc
    checkin_next_month_limit = offer.checkin_timestamp.next_month.strftime("%Y-%m-#{LIMIT_DAY_TO_SET_NOSHOW} 23:59:59 +0000").to_time.utc
    normalized_time_now.between?(offer.checkin_timestamp, checkin_next_month_limit)
  end

  def confirmed_by_maxipago?
    if self.payments.first
      return true
    else
      return false
    end
  end

  def booking_with_100_percent_discount
    self.promotional_code_log && self.promotional_code_log.promotional_code.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:percentage] && self.promotional_code_log.promotional_code.discount.eql?(100)
  end

  def is_omnibees?
    self.omnibees_offer.present?
  end

  def is_omnibees_or_hoteisnet?
    self.omnibees_offer.present?
  end

  def is_really_hoteisnet?
    if self.is_omnibees_or_hoteisnet?
      ChooseOmnibeesOrHoteisnet.is_hoteisnet?(self.pack_in_hours, self.offer.checkin_timestamp)
    else
      return false
    end
  end

  def is_really_omnibees?
    if self.is_omnibees_or_hoteisnet?
      !is_really_hoteisnet?
    else
      return false
    end
  end

  def is_meeting_offer?
    self.meeting_room_offer.present?
  end

  def is_event_offer?
    self.event_room_offer.present?
  end

  def type_room_name
    if self.offer_id.present? || is_omnibees_or_hoteisnet?
      return "room"
    else
      return self.business_room.class.name.underscore
    end
  end

  def try_create(offer_price_in_booking_creation_time)
    # this method dont used more, now we use BookingMaker service
    time_around = Offer::ROOM_CLEANING_TIME-1.minute
    if is_omnibees?
      query = %Q{

        INSERT INTO bookings (
          "status",
          "created_by_agency",
          "guest_email",
          "booking_tax",
          "is_active",
          "checkin_date",
          "guest_name",
          "note",
          "number_of_people",
          "hotel_comments",
          "pack_in_hours",
          "user_id",
          "company_id",
          "omnibees_offer_id",
          "room_type_id",
          "room_id",
          "date_interval",
          "created_at",
          "hotel_id",
          "cached_room_type_initial_capacity",
          "cached_room_type_maximum_capacity",
          "cached_offer_extra_price_per_person",
          "cached_offer_price",
          "cached_offer_no_show_value"
        ) VALUES (
          '#{self.status}',
          #{self.created_by_agency?},
          '#{self.guest_email}',
          '#{self.booking_tax}',
          true,
          '#{offer.checkin_timestamp.strftime("%Y-%m-%d")}',
          '#{self.guest_name}',
          '#{self.note}',
          '#{self.number_of_people.to_i}',
          '#{self.hotel_comments}',
          '#{offer.pack_in_hours}',
          '#{self.user_id}',
          #{self.company_id || 'NULL'},
          '#{self.omnibees_offer_id}',
          '#{self.room_type_id}',
          '#{self.room_id}',
          '(#{(offer.checkin_timestamp.to_datetime - time_around).strftime("%Y-%m-%d %H:%M:%S")},
          #{(offer.checkout_timestamp.to_datetime + time_around).strftime("%Y-%m-%d %H:%M:%S")})',
          '#{DateTime.now.utc.strftime("%Y-%m-%d %H:%M:%S")}',
          '#{offer.hotel_id}',
          '#{offer.room_type_initial_capacity}',
          '#{offer.room_type_maximum_capacity}',
          '#{offer.extra_price_per_person.to_f}',
          '#{offer.price.to_f}',
          '#{offer.no_show_value.to_f}'
        );}.squish
    else

      conflicting_available_offers_ids = offer.get_conflicting_offers_ids_with_status_in(Offer::ACCEPTED_STATUS[:available])
      conflicting_offers_query = if conflicting_available_offers_ids.blank?
        ""
      else
        %Q{
          UPDATE offers
          SET status=#{Offer::ACCEPTED_STATUS[:suspended]}
          WHERE id IN (#{conflicting_available_offers_ids.join(",")});
        }
      end

      conflicting_removed_by_hotel_offers_ids = offer.get_conflicting_offers_ids_with_status_in(Offer::ACCEPTED_STATUS[:removed_by_hotel])
      conflicting_offers_query += if conflicting_removed_by_hotel_offers_ids.blank?
        ""
      else
        %Q{
          UPDATE offers
          SET status=#{Offer::ACCEPTED_STATUS[:ghost]}
          WHERE id IN (#{conflicting_removed_by_hotel_offers_ids.join(",")});
        }
      end

      ghosts_query = offer.generate_insert_ghosts_query_for_conflicting_offers

      # HERE we have a race condition because at this exact point
      # any conflicting offer can be created and it will not be marked as suspended
      # Since the bookings table has a rule protecting overlap of checkin/checkout for the sabe room
      # we have a 'fail safe' situation

      query = %Q{

        INSERT INTO bookings (
          "status",
          "created_by_agency",
          "guest_email",
          "booking_tax",
          "is_active",
          "checkin_date",
          "guest_name",
          "note",
          "number_of_people",
          "hotel_comments",
          "pack_in_hours",
          "user_id",
          "company_id",
          "offer_id",
          "room_type_id",
          "room_id",
          "date_interval",
          "created_at",
          "hotel_id",
          "cached_room_type_initial_capacity",
          "cached_room_type_maximum_capacity",
          "cached_offer_extra_price_per_person",
          "cached_offer_price",
          "cached_offer_no_show_value"
        ) VALUES (
          '#{self.status}',
          #{self.created_by_agency?},
          '#{self.guest_email}',
          '#{self.booking_tax}',
          true,
          '#{offer.checkin_timestamp.strftime("%Y-%m-%d")}',
          '#{self.guest_name}',
          '#{self.note}',
          '#{self.number_of_people.to_i}',
          '#{self.hotel_comments}',
          '#{offer.pack_in_hours}',
          '#{self.user_id}',
          #{self.company_id || 'NULL'},
          '#{self.offer_id}',
          '#{self.room_type_id}',
          '#{self.room_id}',
          '(#{(offer.checkin_timestamp.to_datetime - time_around).strftime("%Y-%m-%d %H:%M:%S")},
          #{(offer.checkout_timestamp.to_datetime + time_around).strftime("%Y-%m-%d %H:%M:%S")})',
          '#{DateTime.now.utc.strftime("%Y-%m-%d %H:%M:%S")}',
          '#{offer.hotel_id}',
          '#{offer.room_type_initial_capacity}',
          '#{offer.room_type_maximum_capacity}',
          '#{offer.extra_price_per_person.to_f}',
          '#{offer.price.to_f}',
          '#{offer.no_show_value.to_f}'
        );
        UPDATE offers SET status=#{Offer::ACCEPTED_STATUS[:reserved]} WHERE id=#{offer.id};
        #{conflicting_offers_query}
        #{ghosts_query}

        }.squish
    end
    Booking.transaction do
      begin
        !!(ActiveRecord::Base.connection.execute(query))
      rescue
        raise ActiveRecord::Rollback
      end
    end
  end

  def merge attributes
    attributes.each do |key, val|
      self[key] = val
    end
  end

  def number_of_people_should_be_less_than_or_equal_to_offer_room_type_capacity
    max_capacity = new_record? ? self.offer.room_type_maximum_capacity : self.cached_room_type_maximum_capacity

    if self.number_of_people > max_capacity
      errors.add(:number_of_people, :should_be_less_than_or_equal_to_room_type_capacity)
      false
    end
  end

  protected
    # Method used to change the offers status the booking suspended to available
    def release_offers
      # First we get the current offer type (meeting_offer, event_offer or offer)
      offer_class = self.offer.class.name.constantize
      offer_id = self.try(:offer_id) || self.try(:meeting_room_offer_id) || self.try(:event_room_offer_id)
      # we get the current booking offer
      self_offer = offer_class.find(offer_id)
      # then we get it's conflicting offers (those who were suspended)
      conflicting_offers_ids = self_offer.get_conflicting_offers_ids_with_status_in([offer_class::ACCEPTED_STATUS[:suspended]])
      conflicting_offers = offer_class.find(conflicting_offers_ids)
      # We can change the booking offer to available without any validations because there can't be
      # any other booking invalidating this offer (if there were any booked offer invalidating this one,
      # this booking should not even be created)
      self_offer.status = offer_class::ACCEPTED_STATUS[:available]
      self_offer.save!
      ids_of_offers_to_update_array = []
      # For each conflicting offer we need to check if there are any reserved offer that is conflicting
      # with the offer
      # Example:
      # Current booking offer attributes: Checkin: 12:00
      # Checkout: 15:00
      # Booking interval: 12:00 - 15:59 (using the room cleaning time of 1.hour)
      #
      # And we have another booking with the offer checkin for 16:00
      # and the checkout for 19:00
      # this booking interval is 16:00 - 19:59
      #
      # Let's suppose that the first booking invalidate these offers:
      # - offer1: 11:00 -> 14:59 (considering the room cleaning time)
      # - offer2: 13:00 -> 16:59
      # - offer3: 14:00 -> 17:59
      # - offer4: 11:00 -> 17:59
      # We can change the offer1 status to available because there aren't any conflicting reserved
      # offers with it.
      # The second booking is invalidating offer2, offer3 and offer4, so we can't change their
      # status to available.
      # Graphically:
      # starting at 11:00
      # the offers already include the room cleaning time
      #
      # | |-|-|-|-| | | | |             (current booking)
      # |-|-|-|-| | | | | |             (offer1)
      # |-|-|-|-|-|-|-| | |             (offer4)
      # | | | | | |-|-|-|-|             (second booking)
      # | | |-|-|-|-| | | |             (offer2)
      # | | | |-|-|-|-| | |             (offer3)
      #
      # (each '|-|' represents one hour of the offer and '| |' represents one hour that
      # isnt occupied by the offer)
      # We can see that the first booking overlaps with offers 1,2,3 and 4
      # and the second booking overlaps with offers 2,3 and 4

      conflicting_offers.each do | conflicting_offer |
        next if conflicting_offer.checkin_timestamp < (Time.now + 1.hour).strftime('%Y/%m/%d %H:%M +0000').to_datetime
        if conflicting_offer.get_conflicting_offers_ids_with_status_in([offer_class::ACCEPTED_STATUS[:reserved]]).length == 0
          ids_of_offers_to_update_array << conflicting_offer.id
        end
      end
      offer_class.where(id: ids_of_offers_to_update_array).update_all(status: offer_class::ACCEPTED_STATUS[:available])
    end

    def change_emkt_flux_on_cancellation(user)
      confirmed_booking = user.bookings.where("status = ? OR status = ? OR status = ? OR status = ?", 2, 4, 6, 8).count
      if confirmed_booking == 0
        user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:without_bookings])
      end
    end
end
