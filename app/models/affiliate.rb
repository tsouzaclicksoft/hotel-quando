class Affiliate < ActiveRecord::Base
  include DirectUploadFileProcessing
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :authentication_keys => [:login]

  ACCEPTED_TABROOM = { rooms: 1, meeting_rooms: 2 }
  ACCEPTED_TABROOM_REVERSED = Hash[ACCEPTED_TABROOM.map {|k,v| [v, k]}]

  # Validations
  Email_Format = /\A^$|[-_0-9a-z.+]+@([-a-z0-9.]+)+\.[a-z]{2,4}\z/i
  validates :email, presence: true
  validates_format_of :email, with: Email_Format
  validates :name, presence: true
  validates :cnpj, presence: true
  validates :responsible_name, presence: true
  validate  :responsible_should_have_name_and_last_name
  validates :login, presence: true, uniqueness: true
  validates :country, presence: true, if: "new_record?"

  # Relations
  has_many :invoices
  has_many :bookings
  has_many :users
  has_many :photos
  belongs_to :hotel
  has_many  :payments
  belongs_to :hotel_chain

  # Hooks
  before_validation :set_new_password, if: "new_record? && !password.present?"
  accepts_nested_attributes_for :photos, :allow_destroy => true

  # Instance methods
  def set_new_password
    if self.password.nil?
      self.password = Devise.friendly_token.first(6)
      self.password_confirmation = self.password
    end
  end

  def is_a_hotel_affiliate?
    self.hotel_id.present?
  end

  def responsible_first_name
    responsible_name.split(" ").first
  end

  def responsible_last_name
    name_parts = responsible_name.split(" ")
    name_parts.shift
    name_parts.join(" ")
  end

  def activate!
    self.update!(is_active: true)
  end

  def deactivate!
    self.update!(is_active: false)
  end

  def logo_photo_url
    photo = photos.find { |photo| photo.is_main }
    photo ||= photos.first
    if photo
      return photo.direct_image_url
    else
      return nil
    end
  end

  def background_bedroom_photo_url
    photo = photos.where(is_main: false, title_pt_br: "quartos").last
    photo ||= photos.first
    if photo
      return photo.direct_image_url
    else
      return nil
    end 
  end

  def background_meetingroom_photo_url
    photo = photos.where(is_main: false, title_pt_br: "salas").last
    photo ||= photos.first
    if photo
      return photo.direct_image_url
    else
      return nil
    end 
  end

  def accepts_all_tab_room
    return self.tab_room_accepts.include?(ACCEPTED_TABROOM[:rooms].to_s) && self.tab_room_accepts.include?(ACCEPTED_TABROOM[:meeting_rooms].to_s)
  end

  def accepts_only_rooms
    return !accepts_all_tab_room && self.tab_room_accepts.include?(ACCEPTED_TABROOM[:rooms].to_s)
  end

  def accepts_only_meeting_rooms
    return !accepts_all_tab_room && self.tab_room_accepts.include?(ACCEPTED_TABROOM[:meeting_rooms].to_s)
  end

  private
    def responsible_should_have_name_and_last_name
      errors.add(:responsible_name, I18n.t(:must_have_lastname)) if self.responsible_name.to_s.split(' ').length < 2
    end


end
