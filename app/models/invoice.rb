class Invoice < ActiveRecord::Base
	# relations
  belongs_to :company
	has_many :bookings

	# validations
	validates  	:company_name, presence: true
  validates  	:cnpj, presence: true
  validates  	:street, presence: true
  validates  	:number, presence: true
  validates  	:district, presence: true
  validates  	:city, presence: true
  validates  	:state, presence: true
  validates  	:country, presence: true
  validates  	:cep, presence: true
	validates  	:company_id, presence: true
end
