class Room < ActiveRecord::Base

  # relations
  belongs_to :hotel
  belongs_to :room_type
  has_many   :offers
  has_many   :bookings

  # validations
  validates :number, presence: true
  validates :room_type_id, presence: true
  validates_uniqueness_of :number, :scope => :room_type_id

  # class methods
  scope :by_number, -> number { where(number: number) }
  scope :by_room_type_id, -> room_type_id { where(room_type_id: room_type_id) }
  scope :by_hotel_id, -> hotel_id { where(hotel_id: hotel_id) }
  scope :actives, -> { where(is_active: true) }

  def self.save_rooms_from_numbers_array_and_return_invalids(numbers_array, room_type_id, hotel_id)
    existing_numbers = []
    numbers_array.each do |number|
      room = Room.new
      room.number = number.gsub(/^0+/, "")
      room.room_type_id = room_type_id
      room.hotel_id = hotel_id
      unless (room.save)
        existing_numbers << room.number
      end
    end
    existing_numbers.delete_if { |item| item == ""}
    existing_numbers.uniq.sort
  end

  def self.create_rooms_from_quantity(quantity, room_type_id, hotel_id)
    created_rooms = 0
    rooms_to_create = quantity.to_i
    numbers = Room.where(room_type_id: room_type_id).pluck(:number).to_a.map {|e| e.gsub(/\D/, '')}
    last_number = numbers.map(&:to_i).sort.last
    last_number ||= 0
    last_number += 1
    while (created_rooms < rooms_to_create) do
      begin
        room = Room.new
        room.number = last_number
        last_number += 1
        room.room_type_id = room_type_id
        room.hotel_id = hotel_id
        room.save!
        created_rooms += 1
      rescue
        retry
      end
    end
  end

end
