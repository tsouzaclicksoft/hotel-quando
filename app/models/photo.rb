class Photo < ActiveRecord::Base

  # relations
  has_attached_file :file
  belongs_to :room_type
  belongs_to :hotel
  belongs_to :business_room
  belongs_to :agency
  belongs_to :agency_detail
  belongs_to :place
  belongs_to :affiliate

  # hooks
  before_destroy :cancel_destroy_if_is_a_main_photo
  after_save :set_other_photos_is_main_false, if: 'is_main && !hotel.nil?'

  def process_direct_upload!
    begin
      self.file = open(direct_image_url)
      self.processing_status = 'processed'
    rescue Exception => e
      self.processing_status = 'error'
      Rails.logger.error "Error while processing picture: #{e.to_s}"
    ensure
      self.save!
    end
  end

  private
  def cancel_destroy_if_is_a_main_photo
    reload
    return !is_main
  end

  def set_other_photos_is_main_false
    Photo.where('hotel_id = ? AND id != ?', hotel_id, id).update_all(is_main: false)
  end

end
