class CompanyAttendant < ActiveRecord::Base

	devise :database_authenticatable, :recoverable, :rememberable, :trackable,
	:validatable, :authentication_keys => [:login]

	Email_Format = /\A^$|[-_0-9a-z.+]+@([-a-z0-9.]+)+\.[a-z]{2,4}\z/i
	validates :name, presence: true
	validates :email, presence: true
	validates_format_of :email, with: Email_Format
	validates :login, presence: true, uniqueness: true

	# Hooks
    before_validation :set_new_password, if: "new_record? && !password.present?"

    def set_new_password
    	self.password = Devise.friendly_token.first(6)
    	self.password_confirmation = self.password
    end

    def activate!
    	self.update!(is_active: true)
    end

    def deactivate!
    	self.update!(is_active: false)
    end

end
