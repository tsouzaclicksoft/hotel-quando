class ExecutiveResponsible < ActiveRecord::Base
	
	devise :database_authenticatable, :recoverable, :rememberable, :trackable,
	:validatable, :authentication_keys => [:login]

	has_many :company_universes
    has_many :goals_executive_responsibles
    has_many :company_details
    has_many :bookings

    belongs_to :country

	Email_Format = /\A^$|[-_0-9a-z.+]+@([-a-z0-9.]+)+\.[a-z]{2,4}\z/i
	validates :name, presence: true
	validates :email, presence: true
	validates_format_of :email, with: Email_Format
	validates :login, presence: true, uniqueness: true
    validates :country, presence: true

    validates :amount_goal_contacts, presence: true
    validates :amount_goal_meeting, presence: true
    validates :amount_goal_new_companies, presence: true
    validates :amount_goal_companies_actives, presence: true
    validates :amount_goal_reservation, presence: true

    validates_numericality_of :amount_goal_contacts, greater_than_or_equal_to: 0
    validates_numericality_of :amount_goal_meeting, greater_than_or_equal_to: 0
    validates_numericality_of :amount_goal_new_companies, greater_than_or_equal_to: 0
    validates_numericality_of :amount_goal_companies_actives, greater_than_or_equal_to: 0
    validates_numericality_of :amount_goal_reservation, greater_than_or_equal_to: 0

    # Hooks
    before_validation :set_new_password, if: "new_record? && !password.present?"

    def set_new_password
    	self.password = Devise.friendly_token.first(6)
    	self.password_confirmation = self.password
    end

    def activate!
    	self.update!(is_active: true)
    end

    def deactivate!
    	self.update!(is_active: false)
    end


end
