class MeetingRoomOffer < ActiveRecord::Base
  ACCEPTED_LENGTH_OF_PACKS = HotelQuando::Constants::MEETING_ROOM_LENGTH_OF_PACKS
  ACCEPTED_STATUS = {available: 1, suspended: 2, reserved: 3, removed_by_hotel: 4, expired: 5, waiting_removal: 6, ghost: 99}

  ACCEPTED_STATUS_REVERSED = Hash[ACCEPTED_STATUS.map {|k,v| [v, k]}]
  ROOM_CLEANING_TIME = 1.hour

  # relations
  belongs_to :hotel
  belongs_to :meeting_room
  has_many   :bookings

  # validations
  validates                     :checkin_timestamp, presence: true
  validates                     :pack_in_hours, presence: true
  validates                     :price, presence: true
  validates                     :no_show_value, presence: true
  validates                     :meeting_room_id, presence: true
  validates                     :meeting_room_maximum_capacity, presence: true
  validates                     :pack_in_hours, inclusion: { in: ACCEPTED_LENGTH_OF_PACKS }
  validates                     :status, inclusion: { in: MeetingRoomOffer::ACCEPTED_STATUS.values }
  validates_numericality_of     :price, greater_than: 0
  validates_numericality_of     :no_show_value, greater_than: 0

  # hooks
  before_validation :set_checkout_timestamp

  def checkin_timestamp
    if read_attribute(:checkin_timestamp)
      read_attribute(:checkin_timestamp).utc
    else
      nil
    end
  end

  def checkin_timestamp=(value)
    if value == nil
      write_attribute(:checkin_timestamp, nil)
    else
      write_attribute(:checkin_timestamp, value.strftime("%Y-%m-%d %H:%M:%S"))
    end
  end

  def checkout_timestamp
    if read_attribute(:checkout_timestamp)
      read_attribute(:checkout_timestamp).utc
    else
      nil
    end
  end

  def checkout_timestamp=(value)
    if value == nil
      write_attribute(:checkout_timestamp, nil)
    else
      write_attribute(:checkout_timestamp, value.strftime("%Y-%m-%d %H:%M:%S"))
    end
  end

  # scopes
  scope :reserved, -> { where(status: ACCEPTED_STATUS[:reserved]) }
  scope :by_hotel_id, -> hotel_id { where(hotel_id: hotel_id) }
  scope :by_status, -> statuses { where("status in (?)", statuses) }
  scope :by_pack_in_hours, -> packs_in_hours { where("pack_in_hours in (?)", packs_in_hours) }
  scope :by_checkin_range, -> initial_date, final_date {
    where("checkin_timestamp BETWEEN ? AND ?",
      initial_date.to_date.strftime("%Y-%m-%d 00:00:00"),
      final_date.to_date.strftime("%Y-%m-%d 23:59:59"))
  }
  scope :by_meeting_room_id, -> meeting_rooms_ids { where("meeting_room_id in (?)", meeting_rooms_ids) }
  scope :by_month_of_a_year, -> month, year { where(checkin_timestamp: Time.new(year, month).utc.all_month) }
  scope :by_number_of_people, -> number_of_people do
    if number_of_people <= 9
      where('meeting_room_maximum_capacity BETWEEN ? AND ?', number_of_people, 30)
    elsif number_of_people == 10
      where('meeting_room_maximum_capacity BETWEEN ? AND ?', number_of_people, 40)
    elsif number_of_people == 20
      where('meeting_room_maximum_capacity BETWEEN ? AND ?', number_of_people, 50)
    elsif number_of_people == 50
      where('meeting_room_maximum_capacity BETWEEN ? AND ?', number_of_people, 99)
    else
      where('meeting_room_maximum_capacity >= (?)', number_of_people)
    end
  end


  #scope :by_number_of_people, -> number_of_people { where('meeting_room_maximum_capacity >= (?)', number_of_people)}
  # class methods
  # We must override create_record from active record so we can use
  # offer.create, or offer.save for a new record
  # ActiveRecord do the insert query as INSERT INTO... blablabla... RETURNING id
  # but due the rule we've created to not raise execption on duplicate unique keys on offers
  # it will not return the id and trown a error
  # so we overrid the create_record to work and return the last id
  # http://stackoverflow.com/questions/2944297/postgresql-function-for-last-inserted-id
  def create_record(attribute_names = @attributes.keys)
    attributes_values = arel_attributes_with_values_for_create(attribute_names)
    insert_query = %Q{
      INSERT INTO meeting_room_offers (
        checkin_timestamp,
        checkin_timestamp_day_of_week,
        checkout_timestamp,
        pack_in_hours,
        meeting_room_id,
        price,
        status,
        meeting_room_maximum_capacity,
        no_show_value,
        hotel_id,
        log_id
      ) VALUES (
        '#{checkin_timestamp.strftime("%Y-%m-%d %H:%M:%S +0000")}',
        '#{checkin_timestamp.to_date.cwday}',
        '#{checkout_timestamp.strftime("%Y-%m-%d %H:%M:%S +0000")}',
        '#{pack_in_hours}',
        '#{meeting_room_id}',
        '#{price}',
        '#{status}',
        '#{meeting_room_maximum_capacity}',
        '#{no_show_value}',
        '#{hotel_id}',
        '#{log_id}'
      );
      SELECT currval('meeting_room_offers_id_seq');
    }

    insert_result = MeetingRoomOffer.connection.execute(insert_query.squish)
    self.id = insert_result[0]["currval"]
    @new_record = false
    id
  end

  def self.offers_count_per_day
    offers_count_per_day_in_float = group("extract(day from checkin_timestamp)").count("id")
    return_hash = Hash.new(0)
    offers_count_per_day_in_float.each do |day_in_float, offers_count|
      return_hash[day_in_float.to_i] = offers_count
    end
    return_hash
  end

  def self.get_similar_offers_from_pack_in_hours_and_checkin(pack_in_hours, checkin_datetime)
    base_query = where('NOT(pack_in_hours = (?) AND checkin_timestamp = (?))', pack_in_hours, checkin_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"))
    if pack_in_hours == 1
      maximum_checkin_datetime = (checkin_datetime + 1.hour).strftime("%Y-%m-%d %H:%M:%S +0000")
      query = base_query.where('(((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 1) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 2) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 3))',\
                               (checkin_datetime - 1.hour).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 2.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 3.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime)
    elsif pack_in_hours == 2
      maximum_checkin_datetime = (checkin_datetime + 2.hours).strftime("%Y-%m-%d %H:%M:%S +0000")
      query = base_query.where('(((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 1) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 2) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 3))',\
                               (checkin_datetime - 2.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 3.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 4.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime)
    elsif pack_in_hours == 3
      maximum_checkin_datetime = (checkin_datetime + 3.hours).strftime("%Y-%m-%d %H:%M:%S +0000")
      query = base_query.where('(((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 2) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 3) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 4))',\
                               (checkin_datetime - 3.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 4.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 5.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime)
    elsif pack_in_hours == 4
      maximum_checkin_datetime = (checkin_datetime + 4.hours).strftime("%Y-%m-%d %H:%M:%S +0000")
      query = base_query.where('(((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 3) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 4) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 5))',\
                               (checkin_datetime - 4.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 5.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 6.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime)
    elsif pack_in_hours == 5
      maximum_checkin_datetime = (checkin_datetime + 5.hours).strftime("%Y-%m-%d %H:%M:%S +0000")
      query = base_query.where('(((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 4) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 5) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 6))',\
                               (checkin_datetime - 5.hour).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 6.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 7.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime)
    elsif pack_in_hours == 6
      maximum_checkin_datetime = (checkin_datetime + 6.hours).strftime("%Y-%m-%d %H:%M:%S +0000")
      query = base_query.where('(((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 5) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 6) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 7))',\
                               (checkin_datetime - 6.hour).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 7.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 8.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime)
    elsif pack_in_hours == 7
      maximum_checkin_datetime = (checkin_datetime + 7.hours).strftime("%Y-%m-%d %H:%M:%S +0000")
      query = base_query.where('(((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 6) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 7) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 8))',\
                               (checkin_datetime - 7.hour).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 8.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 9.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime)
    elsif pack_in_hours == 8
      maximum_checkin_datetime = (checkin_datetime + 8.hours).strftime("%Y-%m-%d %H:%M:%S +0000")
      query = base_query.where('(((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 6) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 7) OR '\
                               '((checkin_timestamp BETWEEN (?) AND (?)) AND pack_in_hours = 8))',\
                               (checkin_datetime - 8.hour).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 9.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime,\
                               (checkin_datetime - 10.hours).strftime("%Y-%m-%d %H:%M:%S +0000"), maximum_checkin_datetime)
    end
    query.where("checkin_timestamp > '#{(DateTime.now + 1.hours).strftime('%Y-%m-%d %H:%M:%S +0000')}'")
  end

  # instance methods
  def get_number_of_common_hours_to_checkin_and_checkout(checkin_timestamp, checkout_timestamp)
    checkin_timestamp = checkin_timestamp.to_time
    checkout_timestamp = checkout_timestamp.to_time
    if self.checkin_timestamp > checkin_timestamp
      if self.checkout_timestamp < checkout_timestamp
        number_of_seconds = self.checkin_timestamp - self.checkout_timestamp
      else
        number_of_seconds = self.checkin_timestamp - checkout_timestamp
      end
    else
      if self.checkout_timestamp < checkout_timestamp
        number_of_seconds = checkin_timestamp - self.checkout_timestamp
      else
        number_of_seconds = checkin_timestamp - checkout_timestamp
      end
    end
    if number_of_seconds < 0
      number_of_seconds = -number_of_seconds
    end
    return ((number_of_seconds/60)/60)
  end

  def get_number_of_exceeding_hours_to_checkin_and_checkout(checkin_timestamp, checkout_timestamp)
    number_of_seconds_of_offer_based_on_checkin_and_checkout = checkout_timestamp - checkin_timestamp
    number_of_seconds_of_self = self.checkout_timestamp - self.checkin_timestamp
    if number_of_seconds_of_self <= number_of_seconds_of_offer_based_on_checkin_and_checkout
      return 0
    else
      return ((number_of_seconds_of_self - number_of_seconds_of_offer_based_on_checkin_and_checkout)/60)/60
    end
  end

  def get_conflicting_offers_ids_with_status_in(status_array)
    id = self.id || 0
    confliting_offers = MeetingRoomOffer.where("id != (?) AND hotel_id = (?) AND meeting_room_id = (?) AND status IN (?) "\
                                    "AND ("\
                                    "(pack_in_hours = '8' AND (checkin_timestamp BETWEEN (?) AND (?))) "\
                                    "OR "\
                                    "(pack_in_hours = '7' AND (checkin_timestamp BETWEEN (?) AND (?))) "\
                                    "OR "\
                                    "(pack_in_hours = '6' AND (checkin_timestamp BETWEEN (?) AND (?))) "\
                                    "OR "\
                                    "(pack_in_hours = '5' AND (checkin_timestamp BETWEEN (?) AND (?))) "\
                                    "OR "\
                                    "(pack_in_hours = '4' AND (checkin_timestamp BETWEEN (?) AND (?))) "\
                                    "OR "\
                                    "(pack_in_hours = '3' AND (checkin_timestamp BETWEEN (?) AND (?))) "\
                                    "OR "\
                                    "(pack_in_hours = '2' AND (checkin_timestamp BETWEEN (?) AND (?))) "\
                                    "OR "\
                                    "(pack_in_hours = '1' AND (checkin_timestamp BETWEEN (?) AND (?)))"\
                                    ")",\
                                    id, self.hotel_id, self.meeting_room_id, status_array,\
                                    (self.checkin_timestamp - (8.hours + ROOM_CLEANING_TIME - 1.second)), (self.checkout_timestamp + (ROOM_CLEANING_TIME - 1.second)),\
                                    (self.checkin_timestamp - (7.hours + ROOM_CLEANING_TIME - 1.second)), (self.checkout_timestamp + (ROOM_CLEANING_TIME - 1.second)),\
                                    (self.checkin_timestamp - (6.hours + ROOM_CLEANING_TIME - 1.second)), (self.checkout_timestamp + (ROOM_CLEANING_TIME - 1.second)),\
                                    (self.checkin_timestamp - (5.hours + ROOM_CLEANING_TIME - 1.second)), (self.checkout_timestamp + (ROOM_CLEANING_TIME - 1.second)),\
                                    (self.checkin_timestamp - (4.hours + ROOM_CLEANING_TIME - 1.second)), (self.checkout_timestamp + (ROOM_CLEANING_TIME - 1.second)),\
                                    (self.checkin_timestamp - (3.hours + ROOM_CLEANING_TIME - 1.second)), (self.checkout_timestamp + (ROOM_CLEANING_TIME - 1.second)),\
                                    (self.checkin_timestamp - (2.hours + ROOM_CLEANING_TIME - 1.second)), (self.checkout_timestamp + (ROOM_CLEANING_TIME - 1.second)),\
                                    (self.checkin_timestamp - (1.hour + ROOM_CLEANING_TIME - 1.second)), (self.checkout_timestamp + (ROOM_CLEANING_TIME - 1.second))).pluck(:id)
  end

  def generate_insert_ghosts_query_for_conflicting_offers
    create_ghosts_query = %Q{
      INSERT INTO meeting_room_offers (
        "checkin_timestamp",
        "checkout_timestamp",
        "pack_in_hours",
        "price",
        "status",
        "meeting_room_maximum_capacity",
        "no_show_value",
        "meeting_room_id",
        "hotel_id",
        "log_id"
      )

      SELECT
        "selected_days_and_packs"."datetime" as checkin_timestamp,
        "selected_days_and_packs"."datetime" + ("selected_days_and_packs"."pack_size" || ' hour')::INTERVAL as checkout_timestamp,
        "selected_days_and_packs"."pack_size" as pack_in_hours,
        '0' as price,
        '#{MeetingRoomOffer::ACCEPTED_STATUS[:ghost]}' as status,
        '1' as meeting_room_maximum_capacity,
        '0' as no_show_value,
        '#{meeting_room_id}' as meeting_room_id,
        '#{hotel_id}' as hotel_id,
        '#{log_id}' as log_id
      FROM
        (
    }

    # we ignore the last hour subtracting 1 second.
    # The final conflic datetime for:
    # pack: 6
    # checkin: 12h
    # room_cleaning_time: 4.hours
    # is 12 AM + 6 hours + 4 hours - 1 second = 21:59:59
    # An offer for the 6h pack at 22h can be reserved since the checkout of the offer at 12:00 will be at 18h
    # Considering the cleaning time (4 hours) the next guy can enter the room at 22:00
    final_conflicting_datetime   = (checkout_timestamp + MeetingRoomOffer::ROOM_CLEANING_TIME - 1.second)

    MeetingRoomOffer::ACCEPTED_LENGTH_OF_PACKS.each do |pack_size|
      # we ignore the first hour of the cleaning time adding 1 hour.
      # The initial conflict datetime for a pack of 3 hours
      # offer checkin: 12AM
      # room_cleaning_time: 2.hours
      # is 12 - 3 -2 + 1 = 8AM
      # An offer for the 3h pack at 7AM can be reserved since her checkout will be at 10AM
      # Considering the cleaning time (2 hours) the next guy can enter the room at 12AM
      initial_conflicting_datetime = (checkin_timestamp - pack_size.hour - MeetingRoomOffer::ROOM_CLEANING_TIME + 1.hour)

      create_ghosts_query = %Q{#{create_ghosts_query}(
        SELECT
          i as datetime,
          #{pack_size} as pack_size
        FROM
           generate_series('#{initial_conflicting_datetime.strftime("%Y-%m-%d %H:00:00")}'::timestamp, '#{final_conflicting_datetime.strftime("%Y-%m-%d %H:%M:%S")}'::timestamp, interval '1 hour') i
        )
        UNION}
    end

    #remove last UNION
    5.times { create_ghosts_query.chop! }

    "#{create_ghosts_query}) as selected_days_and_packs;"
  end

  def available?
    self.status == MeetingRoomOffer::ACCEPTED_STATUS[:available]
  end

  # this method can be sunset - removed - using new one: is_omnibees_or_hoteisnet?
  def is_omnibees?
    false
  end

  def is_omnibees_or_hoteisnet?
    false
  end

  def room_category
    return Hotel::ROOM_CATEGORY[:meeting_room_offer]
  end

  private

  def set_checkout_timestamp
    if self.checkin_timestamp && self.pack_in_hours && !self.checkout_timestamp
      self.checkout_timestamp = self.checkin_timestamp + self.pack_in_hours.hours
    end
  end

end
