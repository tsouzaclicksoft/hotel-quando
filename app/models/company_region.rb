class CompanyRegion < ActiveRecord::Base
	belongs_to :city

	validates :name, presence: true
	validates :city, presence: true


  def activate!
    self.update!(is_active: true)
  end

  def deactivate!
    self.update!(is_active: false)
  end
end
