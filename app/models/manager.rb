class Manager < ActiveRecord::Base
	devise :database_authenticatable, :recoverable, :rememberable, :trackable,
	:validatable, :authentication_keys => [:login]

	enum role: [:master, :hotel_manager, :b2b_commercial, :b2b_management, :attendant]

  # validations

  Email_Format = /\A^$|[-_0-9a-z.+]+@([-a-z0-9.]+)+\.[a-z]{2,4}\z/i
  validates_format_of :email, with: Email_Format
  validates :login, presence: true, uniqueness: true
  validates :name, presence: true
  validates :email, presence: true
  validates :role, presence: true
  validates :country, presence: true
  validates :locale, presence: true


  # Hooks
  before_validation :set_new_password, if: "new_record? && !password.present?"

  def set_new_password
    self.password = Devise.friendly_token.first(6)
    self.password_confirmation = self.password
  end
		
  def activate!
  	self.update!(is_active: true)
  end

  def deactivate!
  	self.update!(is_active: false)
  end
end