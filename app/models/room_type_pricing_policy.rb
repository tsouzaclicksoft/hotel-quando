class RoomTypePricingPolicy < ActiveRecord::Base

  # relations
  belongs_to                    :pricing_policy
  belongs_to                    :room_type

  # validations
  validates                     :pack_price_3h, presence: true
  validates                     :pack_price_6h, presence: true
  validates                     :pack_price_9h, presence: true
  validates                     :pack_price_12h, presence: true
  validates                     :pack_price_24h, presence: true
  validates                     :extra_price_per_person_for_pack_price_3h, presence: true
  validates                     :extra_price_per_person_for_pack_price_6h, presence: true
  validates                     :extra_price_per_person_for_pack_price_9h, presence: true
  validates                     :extra_price_per_person_for_pack_price_12h, presence: true
  validates                     :extra_price_per_person_for_pack_price_24h, presence: true
  validates                     :room_type_id, presence: true
  validates_uniqueness_of       :pricing_policy_id, scope: :room_type_id
  validates_numericality_of     :pack_price_3h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_6h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_9h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_12h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_24h, greater_than_or_equal_to: 0
  validates_numericality_of     :extra_price_per_person_for_pack_price_3h, greater_than_or_equal_to: 0
  validates_numericality_of     :extra_price_per_person_for_pack_price_6h, greater_than_or_equal_to: 0
  validates_numericality_of     :extra_price_per_person_for_pack_price_9h, greater_than_or_equal_to: 0
  validates_numericality_of     :extra_price_per_person_for_pack_price_12h, greater_than_or_equal_to: 0
  validates_numericality_of     :extra_price_per_person_for_pack_price_24h, greater_than_or_equal_to: 0
  validates_numericality_of     :pack_price_3h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :pack_price_6h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :pack_price_9h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :pack_price_12h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :pack_price_24h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :extra_price_per_person_for_pack_price_3h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :extra_price_per_person_for_pack_price_6h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :extra_price_per_person_for_pack_price_9h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :extra_price_per_person_for_pack_price_12h, less_than_or_equal_to: 99999999.99
  validates_numericality_of     :extra_price_per_person_for_pack_price_24h, less_than_or_equal_to: 99999999.99

  # hooks

  # class methods
  scope :by_room_type_id, -> room_type_id { where(room_type_id: room_type_id) }

  # instance methods
  def all_prices_are_zero?
    all_prices_are_zero = true
    Offer::ACCEPTED_LENGTH_OF_PACKS.each do | length |
      unless self.send("pack_price_#{length}h") == 0
        all_prices_are_zero = false
      end
    end
    all_prices_are_zero
  end
end
