class OmnibeesOffer < ActiveRecord::Base

  # relations
  belongs_to :hotel
  belongs_to :room
  belongs_to :room_type
  has_one   :booking

  # validations
  validates                     :checkin_timestamp, presence: true
  validates                     :pack_in_hours, presence: true
  validates                     :price, presence: true, numericality: { greater_than: 0 }
  validates                     :room_type_id, presence: true
  validates                     :hotel_id, presence: true
  validates                     :pack_in_hours, inclusion: { in: HotelQuando::Constants::LENGTH_OF_PACKS_OF_HOTELNET }

  # hooks
  before_validation :set_checkout_timestamp

  # this method can be sunset - removed - using new one: is_omnibees_or_hoteisnet?
  # offer methods
  def is_omnibees?
    true
  end

  def is_omnibees_or_hoteisnet?
    true
  end

  def room_type_initial_capacity
    room_type.initial_capacity
  end

  def room_type_maximum_capacity
    room_type.maximum_capacity
  end

  def extra_price_per_person
    0 #price is already full
  end

  def no_show_value
    price #pay full price
  end

  def checkin_timestamp
    if read_attribute(:checkin_timestamp)
      read_attribute(:checkin_timestamp).utc
    else
      nil
    end
  end

  def checkin_timestamp=(value)
    if value == nil
      write_attribute(:checkin_timestamp, nil)
    else
      write_attribute(:checkin_timestamp, value.strftime("%Y-%m-%d %H:%M:%S"))
    end
    set_checkout_timestamp
  end

  def pack_in_hours=(value)
    super
    set_checkout_timestamp
  end

  def checkout_timestamp
    if read_attribute(:checkout_timestamp)
      read_attribute(:checkout_timestamp).utc
    else
      nil
    end
  end

  def checkout_timestamp=(value)
    if value == nil
      write_attribute(:checkout_timestamp, nil)
    else
      write_attribute(:checkout_timestamp, value.strftime("%Y-%m-%d %H:%M:%S"))
    end
  end

  def available?
    booking.nil?
  end

  private

  def set_checkout_timestamp
    if self.checkin_timestamp && self.pack_in_hours
      self.checkout_timestamp = self.checkin_timestamp + self.pack_in_hours.hours
    end
  end

end
