class ReferredFriendLog < ActiveRecord::Base
	# associations 
	belongs_to :user
	belongs_to :promotional_code

	# validations
	validates :user_id, presence: true
	validates :promotional_code_id, presence: true
	validates :friend_email, presence: true
	validates :friend_name, presence: true

	# class methods
  scope :by_id, -> id { where(id: id)}
  scope :by_promotional_code_id, -> id { where(promotional_code_id: id)}
  scope :by_user_id, -> id { where(user_id: id)}

  # instance_methods

  
end
