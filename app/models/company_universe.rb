class CompanyUniverse < ActiveRecord::Base

	belongs_to :city
	belongs_to :company_region
	belongs_to :executive_responsible
  has_one    :company_detail

	validates :name, presence: true, uniqueness: true, allow_blank: true, allow_nil: true
	validates :social_name, presence: true
	validates :cnpj, uniqueness: true, allow_blank: true, allow_nil: true
	validates :city, presence: true
	validates :company_region, presence: true
	validates :executive_responsible, presence: true

	validates :sent_mail_date, presence: true, if: "was_sent_mail?"
	validates :talked_phone_date, presence: true, if: "we_talked_phone?"
	validates :already_face_to_face_meeting_date, presence: true, if: "we_already_face_to_face_meeting?"
	validates :company_registered_date, presence: true, if: "company_already_registered?"

	Email_Format = /\A^$|[-_0-9a-z.+]+@([-a-z0-9.]+)+\.[a-z]{2,4}\z/i
	validates_format_of :email_first_contact, with: Email_Format
	validates_format_of :email_second_contact, with: Email_Format
	validates_format_of :email_other_contact, with: Email_Format


  # class methods
  scope :with_cnpj, -> { where.not(cnpj: [nil, '']) }
  scope :by_name, -> name { where("name ILIKE '%#{name}%'")}
  scope :by_social_name, -> social_name { where("social_name ILIKE '%#{social_name}%'")}
  scope :by_was_sent_mail, -> was_sent_mail { where(was_sent_mail: was_sent_mail) }
  scope :by_we_talked_phone, -> we_talked_phone { where(we_talked_phone: we_talked_phone) }
  scope :by_we_already_face_to_face_meeting, -> we_already_face_to_face_meeting { where(we_already_face_to_face_meeting: we_already_face_to_face_meeting) }
  scope :by_city, -> value { joins(:city).where("cities.id = #{value.split(', ')[0]}")}
  scope :by_region, -> value { joins(:company_region).where("company_regions.id = #{value.split(', ')[0]}")}
  scope :by_cnpj, -> cnpj { where(cnpj: cnpj)}
  scope :by_cnpj, -> cnpj { where(cnpj: cnpj)}

  scope :by_talked_phone_date, -> initial_date, final_date = nil { where(we_talked_phone: true).where("talked_phone_date BETWEEN '#{(initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}' AND '#{(final_date.try(:to_date) || initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}'")}
  scope :by_already_face_to_face_meeting_date, -> initial_date, final_date = nil { where(we_already_face_to_face_meeting: true).where("already_face_to_face_meeting_date BETWEEN '#{(initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}' AND '#{(final_date.try(:to_date) || initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}'")}
  scope :by_company_registered_date, -> initial_date, final_date = nil { where(company_already_registered: true).where("company_registered_date BETWEEN '#{(initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}' AND '#{(final_date.try(:to_date) || initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}'")}
  scope :by_follow_up_date, -> initial_date, final_date = nil { where("follow_up_date BETWEEN '#{(initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}' AND '#{(final_date.try(:to_date) || initial_date.try(:to_date) || Date.today).strftime('%Y-%m-%d')}'")}

  def self.update_company_universe_equals_cnpj(cnpj, company_detail_id)
  	companyUniverse = CompanyUniverse.with_cnpj.where(cnpj: cnpj).first
  	if companyUniverse != nil
  		if not companyUniverse.company_already_registered
  			companyUniverse.company_already_registered = true
  			companyUniverse.company_registered_date = Date.today
  			companyUniverse.company_detail_id = company_detail_id
  			companyUniverse.save
  		end
  	end
  end
  
  def activate!
    self.update!(is_active: true)
  end

  def deactivate!
    self.update!(is_active: false)
  end

end
