class BusinessRoom < ActiveRecord::Base
  include DirectUploadFileProcessing
  # relations
  has_many   :photos
  has_many   :bookings
  has_many   :business_room_prices
  belongs_to :hotel
  accepts_nested_attributes_for :photos, :allow_destroy => true

  # validations
  validates :name_pt_br, presence: true
  validates :name_en, presence: true
  validates :name_es, presence: true
  validates :description_pt_br, presence: true
  validates :description_en, presence: true
  validates :description_es, presence: true
  validates :hotel_id, presence: true

  # class methods
  scope :by_hotel_id, -> hotel_id { where(hotel_id: hotel_id) }

  # instance methods

  def first_photo_url
    photo = nil
    photo ||= photos.first
    if photo
      return photo.file.url(:big)
    else
      return nil
    end
  end

  def is_a_meeting_room?
    self.type.eql?("MeetingRoom")
  end

  def is_a_event_room?
    self.type.eql?("EventRoom")
  end

end
