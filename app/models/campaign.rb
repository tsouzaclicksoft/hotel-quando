class Campaign < ActiveRecord::Base
	# relations
	has_many :promotional_codes
	has_many :hotel_groups
	has_many :hotels, through: :hotel_groups
	accepts_nested_attributes_for :hotel_groups, :allow_destroy => true

	# validations
	validates  	:name, presence: true
end
