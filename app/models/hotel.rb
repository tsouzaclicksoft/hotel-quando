class Hotel < ActiveRecord::Base
  include DirectUploadFileProcessing
  include AddOmnibeesBehavior

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :trackable, :authentication_keys => [:login], :reset_password_keys => [:login]

  ACCEPTED_CATEGORIES = ['5 estrelas', '4 estrelas', '3 estrelas', '2 estrelas', '1 estrela']
  ROOM_CATEGORY = { offer: 1, meeting_room_offer: 2, event_room_offer: 3 }
  ROOM_CATEGORY_REVERSED = Hash[ROOM_CATEGORY.map {|k,v| [v, k]}]

  CURRENCY_SYMBOL = { 'R$' => 1, 'US$' => 2, '€' => 3, '£' => 4, 'COP' => 5,
                      'ARS' => 6, '₹' => 7, '$(MXN)' => 8, 'S/.' => 9, '$(CLP)' => 10, 'ECS' => 11}
  CURRENCY_SYMBOL_REVERSED = Hash[CURRENCY_SYMBOL.map {|k,v| [v, k]}]

  # acessors
  serialize :omnibees_config, Hash
  def omnibees_config
    ::Hotel::OmnibeesConfig.new(super)
  end
  # relations
  has_many                      :affiliates
  has_many                      :booking_packs
  has_many                      :business_rooms
  has_many                      :meeting_rooms
  has_many                      :event_rooms
  has_many                      :room_types
  has_many                      :rooms, through: :room_types
  has_many                      :offers
  has_many                      :meeting_room_offers
  has_many                      :event_room_offers
  has_many                      :offers_creator_logs
  has_many                      :room_offers_creator_logs
  has_many                      :meeting_room_offers_creator_logs
  has_many                      :event_room_offers_creator_logs
  has_many                      :bookings
  has_many                      :pricing_policies
  has_many                      :business_room_pricing_policies
  has_many                      :meeting_room_pricing_policies
  has_many                      :event_room_pricing_policies
  has_many                      :photos
  accepts_nested_attributes_for :photos, :allow_destroy => true
  belongs_to                    :city
  has_one                       :hotel_amenity
  accepts_nested_attributes_for :hotel_amenity, :allow_destroy => true
  has_many                      :hotel_groups
  belongs_to :hotel_chain
  belongs_to                    :places

  # validations
  validates                     :name, presence: true
  validates                     :login, presence: true, uniqueness: true
  validates                     :email, presence: true
  validates                     :description_en, presence: true
  validates                     :description_pt_br, presence: true
  validates                     :description_es, presence: true
  validates                     :payment_method_pt_br, presence: true
  validates                     :payment_method_en, presence: true
  validates                     :payment_method_es, presence: true
  validates                     :minimum_hours_of_notice, presence: true
  validates                     :category, inclusion: { in: ACCEPTED_CATEGORIES }
  validates                     :latitude, presence: true
  validates                     :longitude, presence: true
  validates                     :city, presence: true
  validates                     :state, presence: true
  validates                     :country, presence: true, if: 'new_record?'
  validates                     :street, presence: true
  validates                     :number, presence: true
  validates                     :currency_symbol, presence: true
  validates_numericality_of     :minimum_hours_of_notice, greater_than_or_equal_to: -1
  validates_numericality_of     :iss_in_percentage, greater_than_or_equal_to: 0, less_than_or_equal_to: 10
  validates_numericality_of     :service_tax, greater_than_or_equal_to: 0, less_than_or_equal_to: 10

  # hooks
  before_validation :set_new_password, if: "new_record? && !password.present?"
  after_create :send_login_and_password
  reverse_geocoded_by :latitude, :longitude

  # class methods


  # instance methods
  def set_new_password
    self.password = Devise.friendly_token.first(6)
    self.password_confirmation = self.password
  end

  def is_brazilian
    self.currency_symbol == CURRENCY_SYMBOL['R$']
  end

  def accepts_booking_cancellation?
    if self.minimum_hours_of_notice == -1
      return false
    else
      return true
    end
  end

  def minimum_available_offer_price_for_each_pack
    query = %Q{
      SELECT pack_in_hours, min(price) as min_price
      FROM offers
      WHERE hotel_id=#{self.id} AND status=#{Offer::ACCEPTED_STATUS[:available]}
      GROUP BY pack_in_hours
    }

    Offer.connection.execute(query).to_a.inject(Hash.new()) do |memo, row_hash|
      memo[row_hash["pack_in_hours"]] = row_hash["min_price"].to_f
      memo
    end
  end

  def minimum_available_meeting_offer_price_for_each_pack
    query = %Q{
      SELECT pack_in_hours, min(price) as min_price
      FROM meeting_room_offers
      WHERE hotel_id=1 AND status=#{MeetingRoomOffer::ACCEPTED_STATUS[:available]}
      GROUP BY pack_in_hours
    }

    MeetingRoomOffer.connection.execute(query).to_a.inject(Hash.new()) do |memo, row_hash|
      memo[row_hash["pack_in_hours"]] = row_hash["min_price"].to_f
      memo
    end
  end

  def minimum_available_event_offer_price_for_each_pack
    query = %Q{
      SELECT pack_in_hours, min(price) as min_price
      FROM event_room_offers
      WHERE hotel_id=1 AND status=#{EventRoomOffer::ACCEPTED_STATUS[:available]}
      GROUP BY pack_in_hours
    }

    EventRoomOffer.connection.execute(query).to_a.inject(Hash.new()) do |memo, row_hash|
      memo[row_hash["pack_in_hours"]] = row_hash["min_price"].to_f
      memo
    end
  end

  def emails_for_notice
    self_emails_for_notice = [self.email]
    if self.notice_emails
      self_emails_for_notice += self.notice_emails.split(',').map(&:strip)
    end
    return self_emails_for_notice.uniq
  end

  def emails_for_notice_meeting_room
    self_emails_for_notice_meeting_room = [self.email]
    if self.notice_emails_meeting_room
      self_emails_for_notice_meeting_room += self.notice_emails_meeting_room.split(',').map(&:strip)
    end
    return self_emails_for_notice_meeting_room.uniq
  end

  def send_login_and_password
    HotelMailer.send_login_and_password(self).deliver
  end

  def main_photo_url
    photo = photos.find { |photo| photo.is_main }
    photo ||= photos.first
    if photo
      return photo.file.url(:big)
    else
      return nil
    end
  end

  def address
    return "#{self.street}, #{self.number} #{self.complement} - #{self.city.name} - #{self.state}"
  end

  def to_s
    name
  end
end
