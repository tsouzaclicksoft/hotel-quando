class HotelChain < ActiveRecord::Base
  # relations
  has_many                      :hotels
  has_one                       :affiliate

  # validations
  validates   :name, presence: true
end
