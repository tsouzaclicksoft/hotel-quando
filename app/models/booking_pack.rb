class BookingPack < ActiveRecord::Base
	# relations
	belongs_to      :city
  belongs_to      :hotel
  belongs_to      :room_type
	has_many        :booking_pack_contracting_companies

  # validations
  validates     	:city, presence: true
  validates     	:hotel, presence: true
  validates     	:room_type, presence: true
  validates     	:bank_of_hours, presence: true
  validates     	:number_of_pack_12h, presence: true
  validates     	:price, presence: true

  # class methods
  scope :by_id, -> id { where(id: id)}
  scope :by_city_id, -> id { where(city_id: id)}
  scope :by_hotel_id, -> id { where(hotel_id: id)}
end