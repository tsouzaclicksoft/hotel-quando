class Agency < ActiveRecord::Base
  include DirectUploadFileProcessing
  # Include devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :registerable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :authentication_keys => [:login]

  # Validations
  Email_Format = /\A^$|[-_0-9a-z.+]+@([-a-z0-9.]+)+\.[a-z]{2,4}\z/i
  validates :name, presence: true
  validates :email, presence: true
  validates_format_of :sign_email, with: Email_Format
  validates :login, presence: true, uniqueness: true

  # Relations
  has_many :clients, foreign_key: 'agency_id', :class_name => "Company"
  has_many  :photos
  accepts_nested_attributes_for :photos, :allow_destroy => true

  # Hooks
  before_validation :set_new_password, if: "new_record? && !password.present?"

  # Instance methods
  
  def set_new_password
    self.password = Devise.friendly_token.first(6)
    self.password_confirmation = self.password
  end

  def activate!
    self.update!(is_active: true)
  end

  def deactivate!
    self.update!(is_active: false)
  end

  def bookings
    @bookings ||= Booking.where(company_id: self.clients.pluck(:id))
    @bookings
  end

end
