class MemberGetMemberConfig < ActiveRecord::Base
  include Shared::CurrencyCurrentHelper


  # relations
  belongs_to :campaign
  has_many :promotional_codes

  # validations

  validates                     :campaign_id, presence: true
  validates                     :indicators_credit, presence: true
  validates                     :indicators_credit_first_bookings, presence: true
  validates                     :discount_type_to_referred, presence: true
  validates                     :discount_type_to_referred_first_bookings, presence: true
  validates                     :discount_to_referred, presence: true
  validates                     :discount_to_referred_first_bookings, presence: true
  validates                     :expiration_date_in_months, presence: true
  validates                     :number_of_first_bookings, presence: true
  validates                     :currency_code_discount, presence: true, if: 'discount_to_referred_first_bookings == 2 && discount_type_to_referred == 2'
  validates                     :currency_code_credit, presence: true
  
  validates                     :discount_type_to_referred, inclusion: { in: PromotionalCode::ACCEPTED_DISCOUNT_TYPE.values }
  validates                     :discount_type_to_referred_first_bookings, inclusion: { in: PromotionalCode::ACCEPTED_DISCOUNT_TYPE.values }
  
  validates_numericality_of     :discount_to_referred, greater_than: 0
  validates_numericality_of     :discount_to_referred_first_bookings, greater_than: 0
  validates_numericality_of     :indicators_credit, greater_than: 0
  validates_numericality_of     :indicators_credit_first_bookings, greater_than: 0
  validates_numericality_of     :number_of_first_bookings, greater_than: 0

  # class methods
    scope :by_id, -> id { where(id: id)}

  # instance_methods

  def indicators_credit_value(promotional_code)
    if promotional_code == nil
      indicators_credit_first_bookings
    else
      if promotional_code.bookings.paid.count > number_of_first_bookings
        indicators_credit
      else
        indicators_credit_first_bookings
      end
    end
  end

  def discount_to_referred_value(promotional_code)
    if promotional_code == nil
      discount_to_referred_first_bookings
    else
      if promotional_code.bookings.paid.count > number_of_first_bookings
        discount_to_referred
      else
        discount_to_referred_first_bookings
      end
    end
  end
  
end
