class BusinessRoomPricingPolicy < ActiveRecord::Base
  ACCEPTED_PERCENTAGE_PROFILE_NAMES = ['aggressive', 'normal', 'conservative', 'custom']

  # relations
  has_many                      :business_room_prices, dependent: :destroy
  has_many                      :business_rooms, through: :business_room_prices
  belongs_to                    :hotel
  accepts_nested_attributes_for :business_room_prices, allow_destroy: false

  # validations
  validates                     :name, presence: true
  validates_uniqueness_of       :name, scope: :hotel_id
  validates                     :percentage_profile_name, inclusion: { in: BusinessRoomPricingPolicy::ACCEPTED_PERCENTAGE_PROFILE_NAMES }

  # hooks

  # class methods
  scope :by_hotel_id, -> hotel_id { where(hotel_id: hotel_id) }

def percentage_for_1h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_1h > HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['1_hours']
      errors.add(:percentage_for_1h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_2h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_2h > HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['2_hours']
      errors.add(:percentage_for_2h, :cannot_be_less_than_default_pricing_policy)
    end
  end
  
  def percentage_for_3h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_3h > HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['3_hours']
      errors.add(:percentage_for_3h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_4h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_4h > HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['4_hours']
      errors.add(:percentage_for_4h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_5h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_5h > HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['5_hours']
      errors.add(:percentage_for_5h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_6h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_6h > HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['6_hours']
      errors.add(:percentage_for_6h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_7h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_7h > HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['7_hours']
      errors.add(:percentage_for_7h, :cannot_be_less_than_default_pricing_policy)
    end
  end

  def percentage_for_8h_cannot_be_less_than_default_pricing_policy
    if self.percentage_for_8h > HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK['8_hours']
      errors.add(:percentage_for_8h, :cannot_be_less_than_default_pricing_policy)
    end
  end


end
