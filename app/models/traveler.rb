class Traveler < ActiveRecord::Base
  # Validations
  # validates :country, presence: true
  # validates :state, presence: true
  # validates :city, presence: true
  # validates :street, presence: true
  # validates :postal_code, presence: true
  validates :name, presence: true
  validates :email, presence: true
  validates :phone_number, presence: true
  validates :cost_center, presence: true
  validate  :should_have_name_and_last_name
  validate  :complete_for_payment? 

  # Relations
  belongs_to  :cost_center
  belongs_to  :company_detail
  belongs_to  :job_title
  has_one     :user
  has_many    :credit_cards
  has_many    :bookings
  has_many    :payments
  has_many    :refunds
  has_many    :airs
  
  #has_many  :photos
  #accepts_nested_attributes_for :photos, :allow_destroy => true

  # Instance methods
  def name_with_job_title
    if self.job_title.present?
      return self.name + " - " + self.job_title.name    
    else
      return self.name
    end

  end

  def need_to_complete_address?
    if self.try(:company_detail).try(:country) == "CO" || self.try(:company_detail).try(:agency_detail).try(:country) == "CO"
      return false
    else
      self.street.blank? || self.city.blank? || self.state.blank? || self.country.blank? || self.postal_code.blank?
    end
  end
  
  def should_have_name_and_last_name
    errors.add(:name, I18n.t(:must_have_lastname)) if self.name.to_s.split(' ').length < 2
  end
  
  def first_name
    name.split(" ").first
  end

  def last_name
    name_parts = name.split(" ")
    name_parts.shift
    name_parts.join(" ")
  end

  def complete_for_payment?
    if self.try(:company_detail).try(:country) == "CO" || self.try(:company_detail).try(:agency_detail).try(:country) == "CO"
      return false
    else    
      if self.cpf.present?
        self.validate_cpf
      else
        unless self.passport.present?
          errors.add(:passport, :blank)
          errors.add(:cpf, :blank)
        end
      end
    end
  end

  def validate_cpf
    if self.valid_cpf?
      true
    else
      errors.add(:cpf, :invalid)
      false
    end
  end

  def valid_cpf?
    !!( CpfValidator::Cpf.valid? self.cpf )
  end
end
