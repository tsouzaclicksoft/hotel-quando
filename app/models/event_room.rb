class EventRoom < BusinessRoom

  # relations
  has_many   :event_room_offers


  def rooms_with_active_offer_count_per_day(month_number, year)
    initial_timestamp = Time.new(year, month_number).utc.beginning_of_month.strftime("%Y-%m-%d %H:%M:%S")
    last_timestamp = Time.new(year, month_number).utc.end_of_month.strftime("%Y-%m-%d %H:%M:%S")
    query = %Q{
      SELECT COUNT(event_room_id) as room_count, day_checkin_timestamp
      FROM (
        SELECT event_room_id, extract(day from checkin_timestamp) AS day_checkin_timestamp
        FROM event_room_offers
        WHERE
          (event_room_offers.checkin_timestamp BETWEEN '#{initial_timestamp}' AND '#{last_timestamp}') AND
          event_room_offers.hotel_id = #{hotel_id} AND
          event_room_offers.status = #{EventRoomOffer::ACCEPTED_STATUS[:available]} AND
          event_room_offers.event_room_id = #{id}
          GROUP BY extract(day from checkin_timestamp), event_room_id
      ) t
      GROUP BY day_checkin_timestamp
    }

    EventRoomOffer.connection.execute(query).to_a.inject(Hash.new(0)) do |memo, row_hash|
      memo[row_hash["day_checkin_timestamp"].to_i] = row_hash["room_count"]
      memo
    end
  end
end
