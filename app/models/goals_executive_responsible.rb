class GoalsExecutiveResponsible < ActiveRecord::Base

	belongs_to :executive_responsible

	scope :by_executive_responsible, -> value { joins(:executive_responsible).where("executive_responsibles.id = #{value.split(', ')[0]}")}

	def self.getGoalExecutive(id)
		return GoalsExecutiveResponsible.where(executive_responsible_id: id)
				.where("created_at BETWEEN (?) AND (?)", DateTime.current.beginning_of_month, DateTime.current.end_of_month)
				.first
	end

	def self.get_dones_goals_daily(executive)
		return CompanyUniverse.where(executive_responsible: executive)
						.where(we_talked_phone: true)
						.where("talked_phone_date BETWEEN (?) AND (?)", DateTime.current.beginning_of_day, DateTime.current.end_of_day)
						.count(:id)
		
	end

	def self.get_dones_goal_meeting_weeks(executive)
		return CompanyUniverse.where(executive_responsible: executive)
						.where(we_already_face_to_face_meeting: true)
						.where("already_face_to_face_meeting_date BETWEEN (?) AND (?)", DateTime.current.beginning_of_week, DateTime.current.end_of_week)
						.count(:id)
	end

	def self.get_dones_goal_company_weeks(executive)
		return CompanyUniverse.where(executive_responsible: executive)
						.where(company_already_registered: true)
						.where("company_registered_date BETWEEN (?) AND (?)", DateTime.current.beginning_of_week, DateTime.current.end_of_week)
						.count(:id)
	end

	def self.new_goals_executive_responsible(executive)
		@executive = executive
		@goals = GoalsExecutiveResponsible.new
		@goals.executive_responsible_id = @executive.id
		@goals.amount_contacts = @executive.amount_goal_contacts
		@goals.amount_meeting = @executive.amount_goal_meeting
		@goals.amount_new_companies = @executive.amount_goal_new_companies
		@goals.amount_companies_actives = @executive.amount_goal_companies_actives
		@goals.amount_reservation = @executive.amount_goal_reservation
		@goals.save
	end

	def self.update_goals_executive_responsible(params)
		if(getGoalExecutive(params[:id]) != nil)
			@goals = getGoalExecutive(params[:id])
			@goals.amount_contacts = params[:executive_responsible][:amount_goal_contacts]
			@goals.amount_meeting = params[:executive_responsible][:amount_goal_meeting]
			@goals.amount_new_companies = params[:executive_responsible][:amount_goal_new_companies]
			@goals.amount_companies_actives = params[:executive_responsible][:amount_goal_companies_actives]
			@goals.amount_reservation = params[:executive_responsible][:amount_goal_reservation]
			@goals.save
		end
	end

	def self.verify_goals_in_progress(executive)
		goal = getGoalExecutive(executive.id)
		if goal != nil
			goal.amount_contacts_done = count_we_talked_phone_in_month(executive)
			goal.amount_meeting_done = count_we_already_meeting_in_month(executive)
			goal.amount_new_companies_done = count_new_companies_in_month(executive)
			goal.amount_companies_actives_done = count_companies_with_bookings_in_month(executive)
			goal.amount_reservation_done = count_amount_reservation_in_month(executive)

			self.verify_goals_done(goal)

			goal.save
		end
	end

	def self.verify_goals_done(goal)
		if goal.amount_contacts_done >= goal.amount_contacts
			goal.completed_contacts = true
		else
			goal.completed_contacts = false
		end

		if goal.amount_meeting_done >= goal.amount_meeting
			goal.completed_meeting = true
		else
			goal.completed_meeting = false
		end

		if goal.amount_new_companies_done >= goal.amount_new_companies
			goal.completed_new_companies = true
		else
			goal.completed_new_companies = false
		end

		if goal.amount_companies_actives_done >= goal.amount_companies_actives
			goal.completed_companies_actives = true
		else
			goal.completed_companies_actives = false
		end

		if goal.amount_reservation_done >= goal.amount_reservation
			goal.completed_reservation = true
		else
			goal.completed_reservation = false
		end

		self.verify_goals_completed(goal)

	end

	def self.verify_goals_completed(goal)
		if goal.completed_contacts && goal.completed_meeting && goal.completed_new_companies && goal.completed_companies_actives && goal.completed_reservation
			goal.goal_accomplished = true
			goal.goal_accomplished_date = Date.today
			goal.save
		end
	end

	def self.count_we_talked_phone_in_month(executive)
		return CompanyUniverse.where(executive_responsible: executive)
						.where(we_talked_phone: true)
						.where("talked_phone_date BETWEEN (?) AND (?)", DateTime.current.beginning_of_month, DateTime.current.end_of_month)
						.count(:id)
	end

	def self.count_we_already_meeting_in_month(executive)
		return CompanyUniverse.where(executive_responsible: executive)
						.where(we_already_face_to_face_meeting: true)
						.where("already_face_to_face_meeting_date BETWEEN (?) AND (?)", DateTime.current.beginning_of_month, DateTime.current.end_of_month)
						.count(:id)
	end



	def self.count_new_companies_in_month(executive)
		return CompanyUniverse.where(company_already_registered: true)
						.where(executive_responsible: executive)
						.where("company_registered_date BETWEEN (?) AND (?)", DateTime.current.beginning_of_month, DateTime.current.end_of_month)
						.count(:id)
	end

	def self.count_companies_with_bookings_in_month(executive)

		companies = CompanyUniverse.where(company_already_registered: true)
						.where(executive_responsible: executive)
						.where("created_at BETWEEN (?) AND (?)", DateTime.current.beginning_of_month, DateTime.current.end_of_month)
						.map.each { |c| c}

		count_companies = 0
		companies.each do | company |
			
			company_detail = CompanyDetail.where(id: company.company_detail_id).first
			bookings = Booking.where("user_id = (?) AND created_at BETWEEN (?) AND (?) ", 
			company_detail.user_id, DateTime.current.beginning_of_month, DateTime.current.end_of_month).count(:id)

			if bookings > 0
				count_companies += 1
			end

		end

		return count_companies
	end

	def self.count_amount_reservation_in_month(executive)
		companies = CompanyUniverse.where(company_already_registered: true)
						.where(executive_responsible: executive)
						.where("created_at BETWEEN (?) AND (?)", DateTime.current.beginning_of_month, DateTime.current.end_of_month)
						.map.each { |c| c}

		count = 0
		companies.each do | company |
			
			company_detail = CompanyDetail.where(id: company.company_detail_id).first
			count += Booking.where("user_id = (?) AND created_at BETWEEN (?) AND (?) ", 
			company_detail.user_id, DateTime.current.beginning_of_month, DateTime.current.end_of_month).count(:id)

		end

		return count
	end

	def self.count_days_to_finish_the_month

		today = Date.today

		all_days = (today..today.end_of_month).map.each { |d| d }

		all_days.delete_if { |day| day.saturday? || day.sunday? }

		return all_days.size
	end

	def self.count_weeks_month

		today = Date.today

		all_days = (today..today.end_of_month).map.each { |d| d }
		
		count = 0
		all_days.each do | day |
			
			if day.sunday?
				count += 1
			end
		end

		return count == 0 ? count = 1 : count

	end

end
