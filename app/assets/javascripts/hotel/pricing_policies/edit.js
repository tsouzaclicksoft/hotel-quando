$(document).ready(function(){

  $(".form-actions.wizard-actions.form-buttons:nth-child(2n+1)").click(function(){
    $('html').animate({ scrollTop: 0 }, "fast");
  });

  $(".form-actions.wizard-actions.form-buttons:nth-child(2n+1)").mouseenter(function(){
    $(".form-actions.wizard-actions.form-buttons:nth-child(2n+1) button").addClass('yay');
  });

  $(".form-actions.wizard-actions.form-buttons:nth-child(2n+1)").mouseleave(function(){
    $(".form-actions.wizard-actions.form-buttons:nth-child(2n+1) button").removeClass('yay');
  });
});
