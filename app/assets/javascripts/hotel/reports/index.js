yojs.define("hq.hotel.reports.index", function(){
  $("body").append("<div class='js-tooltip tooltip'></div>");
  yojs.call("hq.hotel.reports.configBookingsPerDayFlot");
  yojs.call("hq.hotel.reports.configBookingsPerPackAndRoomTypeFlot");
  yojs.call("hq.hotel.reports.configAdvanceTimeForBookingPerPackFlot");
});

yojs.define("hq.hotel.reports.configBookingsPerDayFlot", function(){

  var bookingsPerDayData = $(".js-bookings-per-day-placeholder").data().flot;

  $.plot(".js-bookings-per-day-placeholder", [ bookingsPerDayData ], {
    series: {
      bars: {
        show: true,
        barWidth: 0.4,
        align: "center",
        lineWidth: 1
      }
    },
    xaxis: {
      tickLength: 0,
      tickSize: 1
    },
    grid: {
      borderColor: "#CCC",
      hoverable: true
    }
  });

  $(".js-bookings-per-day-placeholder").bind("plothover", function (event, pos, item) {
    if (item) {
      var bookingsNumber = item.datapoint[1].toFixed(0);
      var day = item.datapoint[0].toFixed(0);
      $(".js-tooltip").html('reservas: <b>' + bookingsNumber + '</b><br>dia: <b>' + day + '</b>')
        .css({top: item.pageY, left: item.pageX+6})
        .fadeIn(250);
    } else {
      $(".js-tooltip").stop(true).fadeOut(50);
    }
  });
});

yojs.define("hq.hotel.reports.configBookingsPerPackAndRoomTypeFlot", function(){
  /*
  bookingsPerPackAndRoomTypeData is an array with 2 items, first is the proper data
  for plotting, the second is the array of labels.
  */
  var bookingsPerPackAndRoomTypeData = $(".js-bookings-per-pack-and-room-type-placeholder").data().flot;
  var dataWithLabels = [];
  $.each(bookingsPerPackAndRoomTypeData[0], function(index, value) {
    var object = {}
    object.data = bookingsPerPackAndRoomTypeData[0][index];
    object.label = bookingsPerPackAndRoomTypeData[1][index];
    dataWithLabels.push(object);
  });
      
  $.plot($(".js-bookings-per-pack-and-room-type-placeholder"), dataWithLabels, {
    series: {
      stack: true,
      lines: { show: false },
      bars: { show: true, 
              barWidth: 0.4, 
              horizontal: true,
              lineWidth: 1
      }
    },
    legend: {
      container: $(".js-bookings-per-pack-and-room-type-legend-container"),
    },
    yaxis: {
      mode: "categories",
      tickLength: 0, 
    },
    grid: {
      borderColor: "#CCC",
      hoverable: true
    }
  });

  $(".js-bookings-per-pack-and-room-type-placeholder").bind("plothover", function (event, pos, item) {
    if (item) {
      var bookingsNumber = item.series.data[item.dataIndex][0].toFixed(0);
      $(".js-tooltip").html('reservas: <b>' + bookingsNumber + '</b><br>tipo de quarto: <b>' + item.series.label + '</b>')
        .css({top: item.pageY+1, left: item.pageX-5})
        .fadeIn(250);
    } else {
      $(".js-tooltip").stop(true).fadeOut(50);
    }
  });
});

yojs.define("hq.hotel.reports.configAdvanceTimeForBookingPerPackFlot", function(){
  var advanceTimeForBookingPerPackData = $(".js-advance-time-for-booking-per-pack-placeholder").data().flot;

  $.plot(".js-advance-time-for-booking-per-pack-placeholder", [ advanceTimeForBookingPerPackData ], {
    series: {
      bars: {
        show: true,
        barWidth: 0.4,
        align: "center",
        horizontal: true ,
        lineWidth: 1
      }
    },
    yaxis: {
      mode: "categories",
      tickLength: 0
    },
    grid: {
      borderColor: "#CCC",
      hoverable: true
    }
  });

  $(".js-advance-time-for-booking-per-pack-placeholder").bind("plothover", function (event, pos, item) {
    if (item) {
      var averageAdvanceTime = item.datapoint[0].toFixed(1);
      var pack = item.series.data[item.dataIndex][1];
      $(".js-tooltip").html('antecedência: <b>' + averageAdvanceTime + '</b> dias' + '<br>pacote: <b>' + pack + '</b>')
        .css({top: item.pageY+12, left: item.pageX-5})
        .fadeIn(250);
    } else {
      $(".js-tooltip").stop(true).fadeOut(50);
    }
  });
});
