yojs.define("hq.hotel.bookings.rooms", function() {
  $('.js-date-picker').datepicker({
    autoclose:true, 
    language:'pt-BR'
  }).next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});
