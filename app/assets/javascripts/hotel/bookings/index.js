yojs.define("hq.hotel.bookings.index", function() {
  $('.js-date-picker').datepicker({
    autoclose:true, 
    language:'pt-BR'
  }).next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});
