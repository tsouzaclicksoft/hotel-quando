yojs.define("hq.hotel.meeting_room_offers.show_all", function() {
 var locale = yojs.get("locale");
	$.fn.datepicker.dates['es'] = {
  		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
  		daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
  		daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá"],
  		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
  		monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "sep", "Oct", "Nov", "Dic"],
  		today: "Hoy",
  		monthsTitle: "Meses",
  		clear: "Limpiar",
  		format: "dd/mm/yyyy"
  	};

  $('.js-date-picker').datepicker({autoclose:true, language: locale}).next().on(ace.click_event, function(){
    $(this).prev().focus();
  });

});
