yojs.define("hq.hotel", function() {
  $('.js-chosen-select').chosen();
  yojs.call("components.masks.init");
  yojs.call("components.bootbox.init");
  yojs.call("components.directs3upload.configureDirectS3Upload");
  yojs.call("components.jqueryPlaceholder.init");

  if($(".browserIE").length > 0){
    $(".js-btn-scroll-up").remove();
  }

  $('body.browserIE input.js-date-picker').removeClass('form-control');

	if($('li.rooms > .submenu li').hasClass('active')){
	  	$('li.rooms > .submenu').show();
	  	$('li.rooms').addClass('open');
	  }
	if($('li.meeting-rooms > .submenu li').hasClass('active')){
	  	$('li.meeting-rooms > .submenu').show();
	  	$('li.meeting-rooms').addClass('open');
	  }
	/*if($('li.event-rooms > .submenu li').hasClass('active')){
	  	$('li.event-rooms > .submenu').show();
	  	$('li.event-rooms').addClass('open');
	  }*/

})
