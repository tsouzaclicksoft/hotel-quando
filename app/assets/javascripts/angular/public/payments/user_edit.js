HotelQuandoApp.controller('PublicPaymentUsersEditCtrl', ['$scope', '$window', function($scope, $window) {

  $scope.countryList = yojs.get('countryList');
  
  var initialize = function() {
    autocomplete = new $window.google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete')),{ types: ['(cities)']}
    );
    $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
      $scope.$digest();
    });
  }

  $('.js-autocomplete').onload = initialize();  

  angular.element(document).ready(function () {
    $('#user_country').change();
  });

}]);
