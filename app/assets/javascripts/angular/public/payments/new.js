HotelQuandoApp.controller('PublicNewPaymentCtrl', ['$scope', '$window', function($scope, $window) {
  if(yojs.isDefined('cpfFieldVisible')){
    $scope.cpfVisible = yojs.get('cpfFieldVisible');
  }
  $scope.stateToReturnIfUserChangeCountryBackToBr = '';
  $scope.selectedCreditCardId = '';
  $scope.checkedTermsofuse = true;

  if(yojs.isDefined('userCpf')){
    $scope.cpfToPutBack = yojs.get('userCpf');
  }

  if(yojs.isDefined('userPassport')){
    $scope.passportToPutBack = yojs.get('userPassport');
  }
  
  if (yojs.get('locale') == 'en'){
    $scope.cpfVisible = false;
  }

  if(yojs.isDefined('creditCardCountry')){
    $scope.creditCardCountry = yojs.get('creditCardCountry');
  }
  
  $scope.changeCreditCardFormVisibility = function(){
    if($('.js-use-existing-credit-card').val() == 'true'){
      $('.js-use-existing-credit-card').val(false);
      $scope.selectedCreditCardId = '';
    }
    else{
      $('.js-use-existing-credit-card').val(true);
    }
    $scope.creditCardFormVisible = !($scope.creditCardFormVisible);
  }

  $scope.changeCpfFieldVisibility = function(){
    $scope.cpfVisible = !$scope.cpfVisible;
    if($scope.cpfVisible){
      $scope.passportToPutBack = $('.js-credit-card-owner-passport').val();
      $('.js-credit-card-owner-passport').val('');
      $('.js-credit-card-owner-cpf').val($scope.cpfToPutBack);
    }
    else{
      $scope.cpfToPutBack = $('.js-credit-card-owner-cpf').val();
      $('.js-credit-card-owner-cpf').val('');
      $('.js-credit-card-owner-passport').val($scope.passportToPutBack);
    }
  }

  $scope.countryChanged = function(){
    if ($scope.creditCardCountry != 'BR') {

      var creditCardStateSelectVal = $('.js-credit-card-billing-state').val();
      if (!(creditCardStateSelectVal == 'ZZ')){
        $scope.stateToReturnIfUserChangeCountryBackToBr = creditCardStateSelectVal;
      }

      $scope.stateVisible = false;

      if ($('.js-credit-card-billing-state option[value="ZZ"]').length == 0){
        // Need to append a new option to the select, so we can set the value to ZZ
        // If we don't do this, the value of the select will be null
        $('.js-credit-card-billing-state').html($('.js-credit-card-billing-state').html() + '<option value="ZZ"></option>', '');
      }

      $('.js-credit-card-billing-state').val('ZZ');

      $scope.classForCityInput = 'col-sm-5';
      $scope.classForPhoneInput = 'col-sm-7';
    }
    else{
      $('.js-credit-card-billing-state').html($('.js-credit-card-billing-state').html().replace('<option value="ZZ"></option>', ''));
      $('.js-credit-card-billing-state').val(($scope.stateToReturnIfUserChangeCountryBackToBr || 'SP'));
      $scope.stateVisible = true;
      $scope.classForCityInput = 'col-sm-7';
      $scope.classForPhoneInput = 'col-sm-5';
    }
  }

  if ($scope.creditCardCountry == 'BR') {
    $scope.classForCityInput = 'col-sm-7';
    $scope.classForPhoneInput = 'col-sm-5';
  }
  else{
    $scope.classForCityInput = 'col-sm-5';
    $scope.classForPhoneInput = 'col-sm-7';
    $scope.countryChanged();
  }

}]);
