HotelQuandoApp.factory('publicSearchFactory', ['$http', function ($http){
  var tzArray = {};
    tzArray.getTZ = function(lat,lng,tmstp){
        return $http({
                method : "GET",
                url : "/timezone?location="+lat+","+lng+"&tmstp="+tmstp
              }).then(function mySucces(response) {
                //(dstOffset + rawOffset) and timeZoneId
                return(response.data.msg);
              }, function myError(response) {
                console.log("Couldn't get timezone, please choose the search address again.")
                return('');
              });

    }
    return tzArray;
}]);

HotelQuandoApp.controller('PublicSearchCtrl', ['$scope', '$window', 'publicSearchFactory', function($scope, $window, publicSearchFactory) {
  $scope.checkinHour = 00;
  $scope.checkoutHour = 03;
  $scope.numberOfPeople = 1;
  $scope.numberOfPeopleAir = 1;
  $scope.numberOfPeopleMeetingRoom = 1;
  $scope.address = '';
  $scope.timezoneSearched = '';
  $scope.timezone = {
    timezone_value: 0,
    timezone_id: '',
    timezone_name: ''
  };

  $scope.checkinHourMTR = 00;
  $scope.checkoutHourMTR = 01;
  $scope.selectedPackSizeMTR = 1;
  $scope.numberOfPeopleMeetingRoom = 1;

  $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
  $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
  $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0)
  $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)

  $scope.latitude = '';

  $scope.longitude = '';

  $scope.isSearchDateValid = false;

  $scope.isLatitudeAndLongitudeValid = false;

  $scope.selectedPackSize = 3;
  $scope.moreHours = false;
  $scope.justGo = false;
  $scope.roundTrip = true;

  $('.round-trip').css("background","white");

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  angular.element(document).ready(function () {
    $scope.showNextDayOrSameDayText();
    $scope.disableCheckinButtons();
    $scope.changeNumberOfPeople();
    if ($scope.latitude != "")
      $scope.timezone = $scope.getTimezoneFromGoogle($scope.latitude, $scope.longitude);
  });

  $scope.changeMoreHours = function(operation){
    if(operation == "plus"){
      $scope.moreHours = true;
      $scope.checkinHour = 18;
      $scope.checkoutHour = 7;
      $scope.selectedPackSize = 13
      $scope.disableCheckinButtons();
      $scope.disableCheckoutButtons();
      $scope.setSearchDate();
    }
    else if(operation == "minus"){
      $scope.moreHours = false;
      $scope.checkinHour = 0;
      $scope.checkoutHour = 3;
      $scope.selectedPackSize = 3;
      $scope.disableCheckinButtons();
      $scope.disableCheckoutButtons();
    }
  }

  $scope.changeTypeAir = function(operation){
    if(operation == "justGo"){
      $scope.justGo = true;
      $scope.roundTrip = false;
      $scope.isSearchDestinyDateValidAir = true;
      $scope.disableDateReturnAir();
    }
    else if(operation == "roundTrip"){
      $scope.justGo = false;
      $scope.roundTrip = true;
      $scope.isSearchDestinyDateValidAir = false;
      $scope.enableDateReturnAir();
    }
  }

  $scope.changeCheckinHour = function(operation){
    console.log("asdfasdf");
    if(operation == "minus"){
      $scope.checkinHour -= 1;
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0);
      if ($scope.checkoutHour == 0){
        $scope.checkoutHour = 23
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else if ($scope.selectedPackSize > 12 && $scope.checkoutHour == 7){
        $scope.checkoutHour;
        $scope.selectedPackSize += 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else {
        $scope.checkoutHour -= 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      }
    }
    else if(operation == "plus"){
      $scope.checkinHour += 1;
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0);
      if ($scope.checkoutHour == 23){
        $scope.checkoutHour = 0
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else if ($scope.selectedPackSize > 12 && $scope.checkoutHour == 12){
        $scope.checkoutHour;
        $scope.selectedPackSize -= 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else {
        $scope.checkoutHour += 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      }
    }
    $scope.disableCheckinButtons();
    $scope.disableCheckoutButtons();
    $scope.showNextDayOrSameDayText();
    $scope.setSearchDateValidity();
  }

  $scope.changeCheckinHourMTR = function(operation){
    if(operation == "minus"){
      $scope.checkinHourMTR -= 1;
      $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0);
      if ($scope.checkoutHourMTR == 0){
        $scope.checkoutHourMTR = 23
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else if ($scope.selectedPackSizeMTR > 12 && $scope.checkoutHourMTR == 7){
        $scope.checkoutHourMTR;
        $scope.selectedPackSizeMTR += 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else {
        $scope.checkoutHourMTR -= 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      }
    }
    else if(operation == "plus"){
      $scope.checkinHourMTR += 1;
      $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0);
      if ($scope.checkoutHourMTR == 23){
        $scope.checkoutHourMTR = 0
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else if ($scope.selectedPackSizeMTR > 12 && $scope.checkoutHourMTR == 12){
        $scope.checkoutHourMTR;
        $scope.selectedPackSizeMTR -= 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else {
        $scope.checkoutHourMTR += 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      }
    }
    $scope.disableCheckinButtonsMTR();
    $scope.disableCheckoutButtonsMTR();
    $scope.showNextDayOrSameDayText();
    $scope.setSearchDateValidityMTR();
  }

  $scope.changeCheckoutHour = function(operation){
    if(operation == "minus"){
      $scope.checkoutHour -= 1;
      $scope.selectedPackSize -= 1;
    }
    else if(operation == "plus"){
      $scope.checkoutHour += 1;
      $scope.selectedPackSize += 1;
    }
    $scope.disableCheckoutButtons();
  }

  $scope.changeCheckoutHourMTR = function(operation){
    if(operation == "minus"){
      $scope.checkoutHourMTR -= 1;
      $scope.selectedPackSizeMTR -= 1;
    }
    else if(operation == "plus"){
      $scope.checkoutHourMTR += 1;
      $scope.selectedPackSizeMTR += 1;
    }
    $scope.disableCheckoutButtonsMTR();
  }

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  $scope.set_checkin_checkout_hour = function(){
    var checkin, pack_size, checkout;
    pack_size = parseInt(getParameterByName("by_pack_length", window.location))
    checkin = parseInt(getParameterByName("by_checkin_hour", window.location))
    checkout = checkin + pack_size
    if( (checkin.toString() != 'NaN') && (pack_size.toString() != 'NaN') ){
      $scope.checkinHourDate = new Date().setHours(checkin, 0, 0)
      $scope.checkoutHourDate = new Date().setHours(checkout, 0, 0)
    }else{
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
      $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
    }
    $('.search_tab_rooms').addClass('active_tab')
    $('.search_tab_meeting_rooms').removeClass('active_tab')
    $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'none')
    $('.tab-content.tab-content-custom.any-time').css('display', 'inherit')
  }

  $scope.set_checkin_checkout_hourMTR = function(){
    var search = getParameterByName("by_search", window.location)
    if (search == 'meetingRoom'){
      $('.search_tab_rooms').removeClass('active_tab')
      var checkin, pack_size, checkout;
      pack_size = ( parseInt(getParameterByName("by_pack_lengthMTR", window.location)) || parseInt(getParameterByName("length_of_pack", window.location)) )
      checkin = ( parseInt(getParameterByName("by_checkin_hourMTR", window.location)) || parseInt(getParameterByName("checkin_hour", window.location)) )
      checkout = checkin + pack_size
      if( (checkin.toString() != 'NaN') && (pack_size.toString() != 'NaN') ){
        $scope.checkinHourDateMTR = new Date().setHours(checkin, 0, 0)
        $scope.checkoutHourDateMTR = new Date().setHours(checkout, 0, 0)
        $scope.checkinHourMTR = checkin
        $scope.selectedPackSizeMTR = pack_size
        $scope.checkoutHourMTR = checkout
        $scope.checkinDateMTR = getParameterByName("by_checkin_dateMTR", window.location)
      }else{
        $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0)
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)
      }
      $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'inherit')
      $('.tab-content.tab-content-custom.any-time').css('display', 'none')
      $('.search_tab_rooms').removeClass('active_tab')
      $('.search_tab_meeting_rooms').addClass('active_tab')
      $('.cpy-search-by').attr('value', 'meetingRoom')
      $('.search-hotels-wrapper').addClass('any-time-meeting-rooms-background')
      $('.search-hotels-wrapper').removeClass('any-time-rooms-background')
      $('.image-room-category').addClass('meeting-rooms-background')
      $('.image-room-category').removeClass('any-time-rooms-background')
      $('.describe-box-rooms').addClass('hide')
      $('.describe-box-meeting-room').removeClass('hide')
      $('.describe-any-time').addClass('hide')
      $('.describe-any-time-meeting_room').removeClass('hide')
    }else{
      $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'none')
      $('.tab-content.tab-content-custom.any-time').css('display', 'inherit')
      $('.search_tab_rooms').addClass('active_tab')
      $('.search_tab_meeting_rooms').removeClass('active_tab')
      $('.cpy-search-by').attr('value', 'rooms')
      $('.search-hotels-wrapper').removeClass('any-time-meeting-rooms-background')
      $('.search-hotels-wrapper').addClass('any-time-rooms-background')
      $('.image-room-category').removeClass('meeting-rooms-background')
      $('.image-room-category').addClass('any-time-rooms-background')
      $('.describe-box-rooms').removeClass('hide')
      $('.describe-box-meeting-room').addClass('hide')
      $('.describe-any-time').removeClass('hide')
      $('.describe-any-time-meeting_room').addClass('hide')
    }
  }

  $scope.changePackSize = function(){
    var checkout = $scope.checkinHour + $scope.selectedPackSize
    if (checkout >= 24){
      checkout -= 24
    }
    $scope.checkoutHour = checkout
    $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
    $scope.disableCheckinButtons();
    $scope.showNextDayOrSameDayText()
  }

  $scope.changePackSizeMTR = function(){
    var checkout_mtr = $scope.checkinHourMTR + $scope.selectedPackSizeMTR
    if (checkout_mtr >= 24){
      checkout_mtr -= 24
    }
    $scope.checkoutHourMTR = checkout_mtr
    $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)
    $scope.disableCheckinButtonsMTR();
    $scope.showNextDayOrSameDayText()
  }

  $scope.disableCheckinButtons = function(){
    if ($scope.moreHours == true  || $scope.moreHours == 'true'){
      if ($scope.checkinHour == 18){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHour == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
    else {
      if ($scope.checkinHour == 0){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHour == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
  }

  $scope.disableDateReturnAir = function(){
    $('.js-date-picker-destiny-air').css("background","#dddddd");
    $('.js-date-picker-destiny-air').prop('disabled', true)
    $('#inputReturnHourAir').css("background","#dddddd");
    $('#inputReturnHourAir').prop('disabled', true)

    $('.just-go').css("background","");
    $('.just-go').css("background","white");

    $('.round-trip').css("background","");
    $('.round-trip').css("background","#dddddd;");

  }

  $scope.enableDateReturnAir = function(){
    $('.js-date-picker-destiny-air').css("background","");
    $('.js-date-picker-destiny-air').prop('disabled', false)
    $('#inputReturnHourAir').css("background","");
    $('#inputReturnHourAir').prop('disabled', false)

    $('.just-go').css("background","");
    $('.just-go').css("background","#dddddd;");

    $('.round-trip').css("background","");
    $('.round-trip').css("background","white");
  }



  $scope.disableCheckinButtonsMTR = function(){
    if ($scope.moreHours == true  || $scope.moreHours == 'true'){
      if ($scope.checkinHourMTR == 18){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHourMTR == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
    else {
      if ($scope.checkinHourMTR == 0){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHourMTR == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
  }

  $scope.disableCheckoutButtons = function(){
    var difference_hours = ($scope.checkoutHour + 24) - $scope.checkinHour
    if ($scope.checkoutHour == 12){
      if ($scope.checkinHour == 23){
        $('.minus-checkout-hour').prop('disabled', true)
        $('.plus-checkout-hour').prop('disabled', true)
      } else {
        $('.minus-checkout-hour').prop('disabled', false)
        $('.plus-checkout-hour').prop('disabled', true)
      }
    } else if ($scope.checkoutHour == 7){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 13 && $scope.selectedPackSize < 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 12 && $scope.selectedPackSize > 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else {
      $('.minus-checkout-hour').prop('disabled', false)
      $('.plus-checkout-hour').prop('disabled', false)
    }
  }

  $scope.disableCheckoutButtonsMTR = function(){
    var difference_hours = ($scope.checkoutHourMTR + 24) - $scope.checkinHourMTR
    if ($scope.checkoutHourMTR == 12){
      if ($scope.checkinHourMTR == 23){
        $('.minus-checkout-hour').prop('disabled', true)
        $('.plus-checkout-hour').prop('disabled', true)
      } else {
        $('.minus-checkout-hour').prop('disabled', false)
        $('.plus-checkout-hour').prop('disabled', true)
      }
    } else if ($scope.checkoutHourMTR == 7){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 13 && $scope.selectedPackSizeMTR < 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 12 && $scope.selectedPackSizeMTR > 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else {
      $('.minus-checkout-hour').prop('disabled', false)
      $('.plus-checkout-hour').prop('disabled', false)
    }
  }

  $scope.changeNumberOfPeople = function(operation){
    if(operation == "minus"){
      $scope.numberOfPeople -= 1
      if ($scope.numberOfPeople == 1){
        $('.minus-number-of-people').prop('disabled', true)
      }else{
        $('.plus-number-of-people').prop('disabled', false)
      }
    }
    else if(operation == "plus"){
      $scope.numberOfPeople += 1
      if ( $scope.numberOfPeople == 4){
        $('.plus-number-of-people').prop('disabled', true)
      }else{
        $('.minus-number-of-people').prop('disabled', false)
      }
    }
    else{
      if ($scope.numberOfPeople == 1){
        $('.minus-number-of-people').prop('disabled', true)
      }else if($scope.numberOfPeople == 4){
        $('.plus-number-of-people').prop('disabled', true)
      }else{
        $('.plus-number-of-people').prop('disabled', false)
        $('.minus-number-of-people').prop('disabled', false)
      }
    }
  }

  $scope.changeNumberOfPeopleAir = function(operation){
    if(operation == "minus"){
      $scope.numberOfPeopleAir -= 1
      if ($scope.numberOfPeopleAir == 1){
        $('.minus-number-of-people-air').prop('disabled', true)
      }else{
        $('.plus-number-of-people-air').prop('disabled', false)
      }
    }
    else if(operation == "plus"){
      $scope.numberOfPeopleAir += 1
      if ( $scope.numberOfPeopleAir == 4){
        $('.plus-number-of-people-air').prop('disabled', true)
      }else{
        $('.minus-number-of-people-air').prop('disabled', false)
      }
    }
    else{
      if ($scope.numberOfPeopleAir == 1){
        $('.minus-number-of-people-air').prop('disabled', true)
      }else if($scope.numberOfPeopleAir == 4){
        $('.plus-number-of-people').prop('disabled', true)
      }else{
        $('.plus-number-of-people-air').prop('disabled', false)
        $('.minus-number-of-people-air').prop('disabled', false)
      }
    }
  }

  $scope.changeNumberOfPeopleMeetingRoom = function(operation){
    if(operation == "minus"){
      $scope.numberOfPeopleMeetingRoom -= 1
      if ($scope.numberOfPeopleMeetingRoom == 1){
        $('#minus_mtr').prop('disabled', true)
      }else{
        $('#plus_mtr').prop('disabled', false)
      }
    }
    else if(operation == "plus"){
      $scope.numberOfPeopleMeetingRoom += 1
      if ( $scope.numberOfPeopleMeetingRoom == 15){
        $('#plus_mtr').prop('disabled', true)
      }else{
        $('#minus_mtr').prop('disabled', false)
      }
    }
    else{
      if ($scope.numberOfPeopleMeetingRoom == 1){
        $('#minus_mtr').prop('disabled', true)
      }else if($scope.numberOfPeopleMeetingRoom == 15){
        $('#plus_mtr').prop('disabled', true)
      }else{
        $('#plus_mtr').prop('disabled', false)
        $('#minus_mtr').prop('disabled', false)
      }
    }
  }

  $scope.showNextDayOrSameDayText = function(){
    if ($scope.selectedPackSize == 3 && $scope.checkinHour >= 21){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 6 && $scope.checkinHour >= 18){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 9 && $scope.checkinHour >= 15){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 12&& $scope.checkinHour >= 12){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else{
      $('.next-day').hide()
      $('.same-day').show()
    }
  }

  $scope.changeBackgroundAndClearForm = function(){
    var wrapper = $('.search-hotels-wrapper')
    //tab 1 = night room
    //tab 2 = morning room
    //tab 3 = any time room
    /*
    if ($scope.selectedTabRoom == 1){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper night-rooms-background');
      $scope.selectedPackSize = 12;
      $scope.numberOfPeople = 1;
      $scope.checkinHour = 18;
      $scope.checkoutHour = 06;
      $('.describe-night').show()
      $('.describe-day').hide()
      $('.describe-any-time').hide()
    }
    if ($scope.selectedTabRoom == 2){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper morning-rooms-background');
      $scope.selectedPackSize = 3;
      $scope.numberOfPeople = 1;
      $scope.checkinHour = 06;
      $scope.checkoutHour = 09;
      $('.describe-night').hide()
      $('.describe-day').show()
      $('.describe-any-time').hide()
    }
    if ($scope.selectedTabRoom == 3){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper any-time-rooms-background');
      $scope.selectedPackSize = 3;
      $scope.numberOfPeople = 1;
      $scope.checkinHour = 0;
      $scope.checkoutHour = 03;
      $('.describe-night').hide()
      $('.describe-day').hide()
      $('.describe-any-time').show()
    }
    */
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper any-time-rooms-background');
      $scope.selectedPackSize = 3;
      $scope.numberOfPeople = 1;
      $scope.checkinHour = 0;
      $scope.checkoutHour = 03;
      $('.describe-night').hide()
      $('.describe-day').hide()
      $('.describe-any-time').show()

    /*if ($scope.selectedTabRoom == 3){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper event-rooms-background');
      $scope.selectedPackSize = 2;
      $scope.numberOfPeople = 20;
    }*/
    $scope.address = '';
    $scope.timezone = {};
    $scope.timezoneOriginAir = {};
    $scope.timezoneDestinyAir = {};
    $scope.latitude = '';
    $scope.longitude = '';
    $scope.isSearchDateValid = false;
    $scope.isLatitudeAndLongitudeValid = false;
    $scope.isLatitudeAndLongitudeValidOriginAir = false;
    $scope.isLatitudeAndLongitudeValidDestinyAir = false;
    $scope.checkinDate = '';
    $scope.checkoutDate = '';
    $scope.disableCheckinButtons()
    $scope.changeNumberOfPeople()
    $scope.showNextDayOrSameDayText()
    $('.js-autocomplete').onload = initialize('js-autocomplete-3'); // + $scope.selectedTabRoom);

    $scope.latitudeOriginAir = '';
    $scope.longitudeOriginAir = '';
    $scope.addressOriginAir = '';

    $scope.latitudeDestinyAir = '';
    $scope.longitudeDestinyAir = '';
    $scope.addressDestinyAir = '';

  }

  $scope.changeTab = function(type){
    //$scope.selectedTabRoom = type
    //$scope.changeBackgroundAndClearForm()
  }

  $scope.setSearchDate = function(){
    var parsedDate;
    var locale = yojs.get("locale");
    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);
      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/' + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/' + parsedDate.getFullYear();
    }
    else parsedDate = $scope.checkinDate;

    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = dateStr.split('/');
      dateToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
      dateInt = dateToSearch.getTime() - minimumDateToSearch;
      // Check timezone (requesting our server) only if difference between +/- 24h
      if(typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateInt > -82799000) && (dateInt < 82801000) && ($scope.latitude != '') )
          $scope.getTimezoneFromGoogle($scope.latitude, $scope.longitude);
    }
    if($scope.moreHours == true || $scope.moreHours == 'true'){
      var dataFormat = (locale == "en" ? "mm/dd/yyyy" : "dd/mm/yyyy")
      var tomorrow = dateToSearch.setDate(dateToSearch.getDate() +1);
      var endDate = new Date(tomorrow);
      endDate.setDate(endDate.getDate()+2);
      var formatedTomorrow = dateToSearch.toLocaleDateString(locale);
      $scope.checkoutDate = formatedTomorrow;
      $('.cpy-checkout-date-calendar-input').datepicker('setStartDate', dateToSearch.toLocaleDateString(locale));
      $('.cpy-checkout-date-calendar-input').datepicker('setEndDate', endDate.toLocaleDateString(locale));
      $scope.parsedSelectedCheckoutDate = formatedTomorrow;
      $scope.selectedPackSize = ($scope.checkoutHour + 24) - $scope.checkinHour;
    }
    $scope.setSearchDateValidity();
  }

  $scope.setSearchDateMTR = function(){
    var parsedDate;
    var locale = yojs.get("locale");
    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDateMTR);
      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/' + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/' + parsedDate.getFullYear();
    }
    else parsedDate = $scope.checkinDateMTR;

    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = dateStr.split('/');
      dateToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR);
      dateInt = dateToSearch.getTime() - minimumDateToSearch;
      // Check timezone (requesting our server) only if difference between +/- 24h
      if(typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateInt > -82799000) && (dateInt < 82801000) && ($scope.latitude != '') )
          $scope.getTimezoneFromGoogleMTR($scope.latitude, $scope.longitude);
    }
    if($scope.moreHours == true || $scope.moreHours == 'true'){
      var dataFormat = (locale == "en" ? "mm/dd/yyyy" : "dd/mm/yyyy")
      var tomorrow = dateToSearch.setDate(dateToSearch.getDate() +1);
      var endDate = new Date(tomorrow);
      endDate.setDate(endDate.getDate()+2);
      var formatedTomorrow = dateToSearch.toLocaleDateString(locale);
      $scope.checkoutDateMTR = formatedTomorrow;
      $('.cpy-checkout-date-calendar-input').datepicker('setStartDate', dateToSearch.toLocaleDateString(locale));
      $('.cpy-checkout-date-calendar-input').datepicker('setEndDate', endDate.toLocaleDateString(locale));
      $scope.parsedSelectedCheckoutDateMTR = formatedTomorrow;
      $scope.selectedPackSizeMTR = ($scope.checkoutHourMTR + 24) - $scope.checkinHourMTR;;
    }
    $scope.setSearchDateValidityMTR();
  }

  $scope.setSearchOriginDateAir = function(){
    var parsedDate;
    var locale = yojs.get("locale");
    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.originDateAir);
      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/' + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/' + parsedDate.getFullYear();
    }
    else parsedDate = $scope.originDateAir;
    $scope.setSearchOriginDateValidityAir();
  }

  $scope.setSearchDestinyDateAir = function(){
    var parsedDate;
    var locale = yojs.get("locale");
    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.destinyDateAir);
      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/' + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/' + parsedDate.getFullYear();
    }
    else parsedDate = $scope.destinyDateAir;
    $scope.setSearchDestinyDateValidityAir();
  }


  $scope.setSearchEndDate = function(){
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    if(yojs.get("locale") == "en"){
      parsedCheckinDate = new Date($scope.checkinDate);
      parsedCheckinDate = ('0' + parsedCheckinDate.getDate()).slice(-2) + '/' + ('0' + (parsedCheckinDate.getMonth()+1)).slice(-2) + '/' + parsedCheckinDate.getFullYear();
      parsedCheckoutDate = new Date($scope.checkoutDate);
      parsedCheckoutDate = ('0' + parsedCheckoutDate.getDate()).slice(-2) + '/' + ('0' + (parsedCheckoutDate.getMonth()+1)).slice(-2) + '/' + parsedCheckoutDate.getFullYear();
    }
    else {
      parsedCheckinDate = $scope.checkinDate;
      parsedCheckoutDate = $scope.checkoutDate;
    }

    if(datePattern.test(parsedCheckinDate)){
      date = parsedCheckinDate.split('/');
      checkinToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
    }
    if(datePattern.test(parsedCheckoutDate)){
      date = parsedCheckoutDate.split('/');
      checkoutToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkoutHour);
    }


    var firstDate = new Date(checkinToSearch);
    var secondDate = new Date(checkoutToSearch);

    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    if (diffDays == 1){
      $scope.selectedPackSize = ($scope.checkoutHour + 24) - $scope.checkinHour
    } else if (diffDays == 2){
      $scope.selectedPackSize = ($scope.checkoutHour + 48) - $scope.checkinHour
    } else {
      $scope.selectedPackSize = ($scope.checkoutHour + 72) - $scope.checkinHour
    }
  }

  $scope.setSearchDateValidity = function(){
    if (typeof $scope.checkinDate == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.checkinDate);

      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour); // subtract 1 from month because javascript start month in 0 = jan
      } else {
        parsedDate = $scope.checkinDate;
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
      }
      $scope.parsedSelectedCheckinDate = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;

      // Consider timezone when search datetime is between +/- 23h from local time
      if (typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateToInt > -82799000) && (dateToInt < 82801000) && ($scope.latitude != '') )
          minimumDateToSearch += $scope.timezone.timezone_value + (new Date().getTimezoneOffset()*60000);
    }

    if(datePattern.test(parsedDate)){
      if(dateToSearch >= minimumDateToSearch)
        $scope.isSearchDateValid = true
      else $scope.isSearchDateValid = false;
    }
    else $scope.isSearchDateValid = false;
  };

  $scope.setSearchDateValidityMTR = function(){
    if (typeof $scope.checkinDateMTR == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.checkinDateMTR);

      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR); // subtract 1 from month because javascript start month in 0 = jan
      } else {
        parsedDate = $scope.checkinDateMTR;
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR);
      }
      $scope.parsedSelectedCheckinDateMTR = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;

      // Consider timezone when search datetime is between +/- 23h from local time
      if (typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateToInt > -82799000) && (dateToInt < 82801000) && ($scope.latitude != '') )
          minimumDateToSearch += $scope.timezone.timezone_value + (new Date().getTimezoneOffset()*60000);
    }

    if(datePattern.test(parsedDate)){
      if(dateToSearch >= minimumDateToSearch)
        $scope.isSearchDateValidMTR = true
      else $scope.isSearchDateValidMTR = false;
    }
    else $scope.isSearchDateValidMTR = false;
  };

  $scope.setSearchOriginDateValidityAir = function(){
    if (typeof $scope.originDateAir == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.originDateAir);

      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.originDateAir); // subtract 1 from month because javascript start month in 0 = jan
      } else {
        parsedDate = $scope.originDateAir;
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.originDateAir);
      }
      $scope.parsedSelectedOriginDateAir = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;

    }

    if(datePattern.test(parsedDate)){
        $scope.isSearchOriginDateValidAir = true
    }
    else $scope.isSearchOriginDateValidAir = false;
  };

  $scope.setSearchDestinyDateValidityAir = function(){
    if (typeof $scope.destinyDateAir == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.destinyDateAir);

      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR); // subtract 1 from month because javascript start month in 0 = jan
      } else {

        parsedDate = $scope.destinyDateAir;

        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.destinyDateAir);

      }
      $scope.parsedSelectedDestinyDateAir = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;

      // Consider timezone when search datetime is between +/- 23h from local time
      if (typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateToInt > -82799000) && (dateToInt < 82801000) && ($scope.latitude != '') )
          minimumDateToSearch += $scope.timezone.timezone_value + (new Date().getTimezoneOffset()*60000);
    }

    if(datePattern.test(parsedDate) || $scope.justGo){
        $scope.isSearchDestinyDateValidAir = true
    }
    else $scope.isSearchDestinyDateValidAir = false;
  };

  $scope.setLatitudeAndLongitudeValidity = function(){
    // Check if latitude and longitude are numeric
    var latitudeValid = ((!isNaN(parseFloat($scope.latitude))) && (isFinite($scope.latitude)))
    var longitudeValid = ((!isNaN(parseFloat($scope.longitude))) && (isFinite($scope.longitude)))
    $scope.isLatitudeAndLongitudeValid = (latitudeValid && longitudeValid)
  }

  $scope.setLatitudeAndLongitudeValidityOriginAir = function(){
    // Check if latitude and longitude are numeric
    var latitudeValid = ((!isNaN(parseFloat($scope.latitudeOriginAir))) && (isFinite($scope.latitudeOriginAir)))
    var longitudeValid = ((!isNaN(parseFloat($scope.longitudeOriginAir))) && (isFinite($scope.longitudeOriginAir)))
    $scope.isLatitudeAndLongitudeValidOriginAir = (latitudeValid && longitudeValid)
  }

  $scope.setLatitudeAndLongitudeValidityDestinyAir = function(){
    // Check if latitude and longitude are numeric
    var latitudeValid = ((!isNaN(parseFloat($scope.latitudeDestinyAir))) && (isFinite($scope.latitudeDestinyAir)))
    var longitudeValid = ((!isNaN(parseFloat($scope.longitudeDestinyAir))) && (isFinite($scope.longitudeDestinyAir)))
    $scope.isLatitudeAndLongitudeValidDestinyAir = (latitudeValid && longitudeValid)
  }

  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    return ($scope.isSearchDateValid && $scope.isLatitudeAndLongitudeValid);
  }

  $scope.isFormValidMTR = function(){
    $scope.setSearchDateValidityMTR();
    return ($scope.isSearchDateValidMTR);
  }

  $scope.isFormValidAir = function(){
    $scope.setSearchOriginDateValidityAir();
    $scope.setSearchDestinyDateValidityAir();

    return  $scope.origin_air != '' && $scope.origin_air != undefined &&
            $scope.destiny_air != '' && $scope.destiny_air != undefined &&
            $scope.addressOriginAir != '' && $scope.addressOriginAir != undefined &&
            $scope.addressDestinyAir != '' && $scope.addressDestinyAir != undefined &&
            $scope.isSearchOriginDateValidAir && $scope.isSearchDestinyDateValidAir &&
            $scope.number_contact != '' && $scope.number_contact != undefined;

  }

  $scope.resonsForDisabledSubmit = function(){
    if ((!$scope.isSearchDateValid) && (!$scope.isLatitudeAndLongitudeValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li><li>' + yojs.get('error_message_for_address') + '</li></ul>';
    }
    else {
      if(!($scope.isSearchDateValid)){
        return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
      }
      else {
        return '<ul><li>' + yojs.get('error_message_for_address') + '</li></ul>';
      }
    }
  }

  $scope.resonsForDisabledSubmitAir = function(){

    var listErrors = '';

    if ($scope.addressOriginAir == '' || $scope.addressOriginAir == undefined){
      listErrors += '<ul><li>' + yojs.get('the_origin_is_invalid') + '</li></ul>';
    }

    if ($scope.addressDestinyAir == '' || $scope.addressDestinyAir == undefined){
      listErrors += '<ul><li>' + yojs.get('the_destiny_is_invalid') + '</li></ul>';
    }

    if (!$scope.isSearchOriginDateValidAir){
      listErrors += '<ul><li>' + yojs.get('the_date_of_origin_is_invalid') + '</li></ul>';
    }

    if (!$scope.isSearchDestinyDateValidAir){
      listErrors += '<ul><li>' + yojs.get('the_date_of_destiny_is_invalid') + '</li></ul>';
    }

    if( $scope.number_contact == '' || $scope.number_contact == undefined){
      listErrors += '<ul><li>' + yojs.get('phone_is_obligatory') + '</li></ul>';
    }


    return listErrors;
  }

  $scope.getTimezoneFromGoogle = function(lat, lng) {
    if (lat != "") {
      if ($scope.checkinDate == "")
        var tmstp = Math.round(((new Date()).getTime()/1000))
      else {
        var d = $scope.checkinDate.split("/");
        // Get user timezone - local
        var splitDateTimeNow = new Date().toString().split(" ");
        timeZoneFormatted = splitDateTimeNow[splitDateTimeNow.length - 2] + " " + splitDateTimeNow[splitDateTimeNow.length - 1];
        // Format datetime with timezone to send to server
        if (yojs.get("locale") == "en")
              var tmstp = new Date(d[2], ( d[0]-1 ), d[1], ($scope.checkinHour).toString(), "0").getTime() / 1000
        else  var tmstp = new Date(d[2], ( d[1]-1 ), d[0], $scope.checkinHour.toString(), "0").getTime() / 1000;
      }
      publicSearchFactory.getTZ($scope.latitude, $scope.longitude, tmstp)
        .then(function(arrItems){
          $scope.timezone = arrItems;
          $scope.timezoneSearched = arrItems.timezone_id;
      });
    }
  }

  $scope.getTimezoneFromGoogleMTR = function(lat, lng) {
    if (lat != "") {
      if ($scope.checkinDateMTR == "")
        var tmstp = Math.round(((new Date()).getTime()/1000))
      else {
        var d = $scope.checkinDate.split("/");
        // Get user timezone - local
        var splitDateTimeNow = new Date().toString().split(" ");
        timeZoneFormatted = splitDateTimeNow[splitDateTimeNow.length - 2] + " " + splitDateTimeNow[splitDateTimeNow.length - 1];
        // Format datetime with timezone to send to server
        if (yojs.get("locale") == "en")
              var tmstp = new Date(d[2], ( d[0]-1 ), d[1], ($scope.checkinHourMTR).toString(), "0").getTime() / 1000
        else  var tmstp = new Date(d[2], ( d[1]-1 ), d[0], $scope.checkinHourMTR.toString(), "0").getTime() / 1000;
      }
      publicSearchFactory.getTZ($scope.latitude, $scope.longitude, tmstp)
        .then(function(arrItems){
          $scope.timezone = arrItems;
          $scope.timezoneSearched = arrItems.timezone_id;
      });
    }
  }

  var autocomplete;
  var initialize = function() {
    if (document.getElementById('js-autocomplete') != null) {
      autocomplete = new $window.google.maps.places.Autocomplete((document.getElementById('js-autocomplete')));
      $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
        $scope.setLatitudeAndLongitude();
        $scope.$digest();
      });
    }
  }

  var autocomplete_mr;
  var initialize_mr = function(){
    if (document.getElementById('js-autocomplete-meeting-room') != null){
      autocomplete_mr = new $window.google.maps.places.Autocomplete((document.getElementById('js-autocomplete-meeting-room')));
      $window.google.maps.event.addListener(autocomplete_mr, 'place_changed', function() {
        $scope.setLatitudeAndLongitude();
        $scope.$digest();
      });
    }
  }

  var autocomplete_air_origin;
  var initialize_air_origin = function(){
    if (document.getElementById('js-autocomplete-origin-air') != null){
      autocomplete_air_origin = new $window.google.maps.places.Autocomplete((document.getElementById('js-autocomplete-origin-air')));
      $window.google.maps.event.addListener(autocomplete_air_origin, 'place_changed', function() {
        $scope.setLatitudeAndLongitudeOriginAir();
        $scope.$digest();
      });
    }
  }

  var autocomplete_air_destiny;
  var initialize_air_destiny = function(){
    if (document.getElementById('js-autocomplete-destiny-air') != null){
      autocomplete_air_destiny = new $window.google.maps.places.Autocomplete((document.getElementById('js-autocomplete-destiny-air')));
      $window.google.maps.event.addListener(autocomplete_air_destiny, 'place_changed', function() {
        $scope.setLatitudeAndLongitudeDestinyAir();
        $scope.$digest();
      });
    }
  }

  $scope.setLatitudeAndLongitude = function() {
    try{

      var place = (autocomplete.getPlace() == undefined ? autocomplete_mr.getPlace() : autocomplete.getPlace())

      $scope.latitude = place.geometry.location.lat();
      $scope.longitude = place.geometry.location.lng();
      //$scope.cityName = place.address_components[2].short_name;
      //$scope.stateName = place.address_components[3].short_name;
      $scope.address = place.formatted_address
      $scope.setLatitudeAndLongitudeValidity();

      if ($scope.isLatitudeAndLongitudeValid)
        $scope.timezone = $scope.getTimezoneFromGoogle(place.geometry.location.lat(), place.geometry.location.lng())
      else $scope.timezone = {};

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'locality' || addressType == 'administrative_area_level_2') {
          var val = place.address_components[i].short_name;
          $scope.cityName = val;
        } else if (addressType == 'country') {
          var val = place.address_components[i].short_name;
          $scope.stateName = val;
        }
      }
    }
    catch(err){
      $scope.isLatitudeAndLongitudeValid = false
    }
  }

  $scope.setLatitudeAndLongitudeOriginAir = function() {
    try{

      var place;
      if (autocomplete_air_origin.getPlace() != undefined){
        place = autocomplete_air_origin.getPlace();
      }

      $scope.latitudeOriginAir = place.geometry.location.lat();
      $scope.longitudeOriginAir = place.geometry.location.lng();
      $scope.addressOriginAir = place.formatted_address
      $scope.setLatitudeAndLongitudeValidityOriginAir();

      if ($scope.isLatitudeAndLongitudeValidOriginAir)
        $scope.timezoneOriginAir = $scope.getTimezoneFromGoogle(place.geometry.location.lat(), place.geometry.location.lng())
      else $scope.timezoneOriginAir = {};

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'locality' || addressType == 'administrative_area_level_2') {
          var val = place.address_components[i].short_name;
          $scope.cityNameOriginAir = val;
        } else if (addressType == 'country') {
          var val = place.address_components[i].short_name;
          $scope.stateNameOriginAir = val;
        }
      }
    }
    catch(err){
      $scope.isLatitudeAndLongitudeValidOriginAir = false
    }
  }

  $scope.setLatitudeAndLongitudeDestinyAir = function() {
    try{

      var place;
      if (autocomplete_air_destiny.getPlace() != undefined){
        place = autocomplete_air_destiny.getPlace();
      }

      $scope.latitudeDestinyAir = place.geometry.location.lat();
      $scope.longitudeDestinyAir = place.geometry.location.lng();
      $scope.addressDestinyAir = place.formatted_address
      $scope.setLatitudeAndLongitudeValidityDestinyAir();

      if ($scope.isLatitudeAndLongitudeValidDestinyAir)
        $scope.timezoneDestinyAir = $scope.getTimezoneFromGoogle(place.geometry.location.lat(), place.geometry.location.lng())
      else $scope.timezoneDestinyAir = {};

      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'locality' || addressType == 'administrative_area_level_2') {
          var val = place.address_components[i].short_name;
          $scope.cityNameDestinyAir = val;
        } else if (addressType == 'country') {
          var val = place.address_components[i].short_name;
          $scope.stateNameDestinyAir = val;
        }
      }
    }
    catch(err){
      $scope.isLatitudeAndLongitudeValidDestinyAir = false
    }
  }

  $('.js-autocomplete').onload = initialize('js-autocomplete-3');// + $scope.selectedTabRoom);

  $('js-autocomplete-meeting-room').onload = initialize_mr();

  $('js-autocomplete-origin-air').onload = initialize_air_origin();

  $('js-autocomplete-destiny-air').onload = initialize_air_destiny();


  // the angularjs dont fill this fields
  $('.js-autocomplete').closest('form').submit(function(){
    $('.cpy-city-name').val($scope.cityName);
    $('.cpy-state-name').val($scope.stateName);
    $('.cpy-latitude-field').val($scope.latitude);
    $('.cpy-longitude-field').val($scope.longitude);
    $('.cpy-address').val($scope.address);
    $('.cpy-mobile').val($scope.mobile);
  })

  $('.js-autocomplete-origin-air').closest('form').submit(function(){
    $('.cpy-city-name-origin-air').val($scope.cityNameOriginAir);
    $('.cpy-state-name-origin-air').val($scope.stateNameOriginAir);
    $('.cpy-latitude-field-origin-air').val($scope.latitudeOriginAir);
    $('.cpy-longitude-field-origin-air').val($scope.longitudeOriginAir);
    $('.cpy-address-origin-air').val($scope.addressOriginAir);
  })

  $('.js-autocomplete-destiny-air').closest('form').submit(function(){
    $('.cpy-city-name-destiny-air').val($scope.cityNameDestinyAir);
    $('.cpy-state-name-destiny-air').val($scope.stateNameDestinyAir);
    $('.cpy-latitude-field-destiny-air').val($scope.latitudeDestinyAir);
    $('.cpy-longitude-field-destiny-air').val($scope.longitudeDestinyAir);
    $('.cpy-address-destiny-air').val($scope.addressDestinyAir);
  })

  $(".submit-air").click(function(){
     $('form').on('submit',function(event){
      event.preventDefault();
      $scope.event_submit_air = event;
      });
  });

  $("#sumit-air-ok").click(function(){
      $scope.event_submit_air.currentTarget.submit();
  });

}]);
