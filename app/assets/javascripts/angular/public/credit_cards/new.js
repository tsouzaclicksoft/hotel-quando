HotelQuandoApp.controller('TravelerCreditCardCtrl', ['$scope', function($scope) {
  $scope.selectedCreditCardId = '';

  $scope.changeCreditCard = function(){
    angular.forEach($scope.creditCardsTraveler, function(credit_card) {
      if (credit_card.id.toString() === $scope.selectedCreditCardId) {
        $('.card-number-value').text(credit_card.card_number)
        $('.expiration-month-value').text(credit_card.expiration_month)
        $('.expiration-year-value').text(credit_card.expiration_year)
        $('.cvv-value').text(credit_card.cvv)
        $('.show-credit-card').slideDown()
      }else{
        $('.show-credit-card').slideUp()
        $('.card-number-value').text('')
        $('.expiration-month-value').text('')
        $('.expiration-year-value').text('')
        $('.cvv-value').text('')
      }
    });
  }
}]);
