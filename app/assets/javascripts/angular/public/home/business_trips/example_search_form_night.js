HotelQuandoApp.controller('PublicBusinessTripsSearchExampleNightController', ['$scope', '$window', function($scope, $window) {
  $scope.checkinHour = 18;
  $scope.checkoutHour = 06;

  angular.element(document).ready(function () {
    $scope.disableCheckinButtons()
  });

  $scope.changeCheckinHour = function(operation){
    if(operation == "minus"){
      $scope.checkinHour -= 1
      if ($scope.checkoutHour == 0){
        $scope.checkoutHour = 23
      }else{
        $scope.checkoutHour -= 1
      }
    }
    else if(operation == "plus"){
      $scope.checkinHour += 1
      if ($scope.checkoutHour == 23){
        $scope.checkoutHour = 0
      }else{
        $scope.checkoutHour += 1
      }
    }
    $scope.disableCheckinButtons()
  }

  $scope.disableCheckinButtons = function(){
    if ($scope.checkinHour == 18){
      $('.example-night .minus-checkin-hour').prop('disabled', true)
      $('.example-night .plus-checkin-hour').prop('disabled', false)
    }else if($scope.checkinHour == 23){
      $('.example-night .plus-checkin-hour').prop('disabled', true)
      $('.example-night .minus-checkin-hour').prop('disabled', false)
    }else{
      $('.example-night .minus-checkin-hour').prop('disabled', false)
      $('.example-night .plus-checkin-hour').prop('disabled', false)
    }
  }

}]);
