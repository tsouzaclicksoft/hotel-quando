HotelQuandoApp.controller('PublicBusinessTripsSearchExampleDayController', ['$scope', '$window', function($scope, $window) {
  $scope.checkinHour = 06;
  $scope.checkoutHour = 09;
  $scope.selectedPackSize = 03;

  angular.element(document).ready(function () {
    $scope.showNextDayOrSameDayText()
    $scope.disableCheckinButtons()
  });

  $scope.changePackSize = function(){
    switch ($scope.selectedPackSize) {
      case 3:
        if ($scope.checkinHour > 15){
          $scope.checkinHour = 15
        }
        break
      case 6:
        if ($scope.checkinHour > 12){
          $scope.checkinHour = 12
        }
        break
      case 9:
        if ($scope.checkinHour > 9){
          $scope.checkinHour = 9
        }
        break
      case 12:
        if ($scope.checkinHour > 17){
          $scope.checkinHour = 17
        }
        break
    }

    var checkout = $scope.checkinHour + $scope.selectedPackSize
    if (checkout >= 24){
      checkout -= 24
    }
    $scope.checkoutHour = checkout
    $scope.disableCheckinButtons();
    $scope.showNextDayOrSameDayText()
  }


  $scope.changeCheckinHour = function(operation){
    if(operation == "minus"){
      $scope.checkinHour -= 1
      if ($scope.checkoutHour == 0){
        $scope.checkoutHour = 23
      }else{
        $scope.checkoutHour -= 1
      }
    }
    else if(operation == "plus"){
      $scope.checkinHour += 1
      if ($scope.checkoutHour == 23){
        $scope.checkoutHour = 0
      }else{
        $scope.checkoutHour += 1
      }
    }
    $scope.disableCheckinButtons()
    $scope.showNextDayOrSameDayText()
  }

  $scope.disableCheckinButtons = function(){
    switch ($scope.selectedPackSize){
      case 3:
        if ($scope.checkinHour == 06){
          $('.example-day .minus-checkin-hour').prop('disabled', true)
          $('.example-day .plus-checkin-hour').prop('disabled', false)
        }else if ($scope.checkinHour == 15){
          $('.example-day .plus-checkin-hour').prop('disabled', true)
          $('.example-day .minus-checkin-hour').prop('disabled', false)
        }else{
          $('.example-day .minus-checkin-hour').prop('disabled', false)
          $('.example-day .plus-checkin-hour').prop('disabled', false)
        }
        break
      case 6:
        if ($scope.checkinHour == 06){
          $('.example-day .minus-checkin-hour').prop('disabled', true)
          $('.example-day .plus-checkin-hour').prop('disabled', false)
        }else if ($scope.checkinHour == 12){
          $('.example-day .plus-checkin-hour').prop('disabled', true)
          $('.example-day .minus-checkin-hour').prop('disabled', false)
        }else{
          $('.example-day .minus-checkin-hour').prop('disabled', false)
          $('.example-day .plus-checkin-hour').prop('disabled', false)
        }
        break
      case 9:
        if ($scope.checkinHour == 06){
          $('.example-day .minus-checkin-hour').prop('disabled', true)
          $('.example-day .plus-checkin-hour').prop('disabled', false)
        }else if ($scope.checkinHour == 09){
          $('.example-day .plus-checkin-hour').prop('disabled', true)
          $('.example-day .minus-checkin-hour').prop('disabled', false)
        }else{
          $('.example-day .minus-checkin-hour').prop('disabled', false)
          $('.example-day .plus-checkin-hour').prop('disabled', false)
        }
        break
      case 12:
        if ($scope.checkinHour == 06){
          $('.example-day .minus-checkin-hour').prop('disabled', true)
          $('.example-day .plus-checkin-hour').prop('disabled', false)
        }else if ($scope.checkinHour == 17){
          $('.example-day .plus-checkin-hour').prop('disabled', true)
          $('.example-day .minus-checkin-hour').prop('disabled', false)
        }else{
          $('.example-day .minus-checkin-hour').prop('disabled', false)
          $('.example-day .plus-checkin-hour').prop('disabled', false)
        }
        break
    }
  }

  $scope.showNextDayOrSameDayText = function(){
    if ($scope.selectedPackSize == 3 && $scope.checkinHour >= 21){
      $('.example-day .next-day').show()
      $('.example-day .same-day').hide()
    }
    else if ($scope.selectedPackSize == 6 && $scope.checkinHour >= 18){
      $('.example-day .next-day').show()
      $('.example-day .same-day').hide()
    }
    else if ($scope.selectedPackSize == 9 && $scope.checkinHour >= 15){
      $('.example-day .next-day').show()
      $('.example-day .same-day').hide()
    }
    else if ($scope.selectedPackSize == 12&& $scope.checkinHour >= 12){
      $('.example-day .next-day').show()
      $('.example-day .same-day').hide()
    }
    else if ($scope.selectedTabRoom == 1){
      $('.example-day .next-day').show()
    }
    else{
      $('.example-day .next-day').hide()
      $('.example-day .same-day').show()
    }
  }

}]);
