HotelQuandoApp.controller('PublicBeOurPartnerController', ['$scope', '$window', function($scope, $window) {
  var blankStrPattern = /^\s*$/;
  var simpleMailPattern = /.+@.+\..+/;
  $scope.hotelname = yojs.get('hotelName');
  $scope.phone = yojs.get('phone');
  $scope.email = yojs.get('emailForContact');
  $scope.contactname = yojs.get('contactName');
  $scope.contactrole = yojs.get('contactRole');
  $scope.country = yojs.get('country');
  $scope.city = yojs.get('city');
  $scope.idskype = yojs.get('idSkype');
  // $scope.termsofuse = yojs.get('termsOfUse');
  $scope.RecapResponse = "";
  $scope.dailyAverage = {
    thirtyEight: "",
    fortyThree: "",
    sixty: "",
    sixtyFive: ""
  };

  $scope.setErrorClassForField = function(fieldName, type){
    var fieldWasNotModified = !($(type + '[ng-model='+fieldName.toLowerCase()+']').hasClass('ng-dirty'))
    if (($scope['is' + fieldName + 'Valid']()) || fieldWasNotModified){
      return ''
    }
    else {
      return 'field-with-error'
    }
  }
  $scope.isPhoneValid = function(){
    return (!blankStrPattern.test($scope.phone) || !blankStrPattern.test($scope.email))
  }

  $scope.isEmailValid = function(){
    return (simpleMailPattern.test($scope.email) || !blankStrPattern.test($scope.phone));
  }

  $scope.isContactNameValid = function(){
    return (!blankStrPattern.test($scope.contactname))
  }

  $scope.isContactRoleValid = function(){
    return (!blankStrPattern.test($scope.contactrole))
  }

  $scope.theContactsAreValid = function(){
    if (!blankStrPattern.test($scope.phone)){
      if (!blankStrPattern.test($scope.email)){
        return ($scope.isPhoneValid() && $scope.isEmailValid());
      }
      else {
        return $scope.isPhoneValid();
      }
    }
    else {
      return $scope.isEmailValid();
    }
  }

  $scope.isCountryValid = function(){
    return (!blankStrPattern.test($scope.country))
  }

  $scope.isCityValid = function(){
    return (!blankStrPattern.test($scope.city))
  }

  $scope.isIdSkypeValid = function(){
    return (!blankStrPattern.test($scope.idskype))
  }

  // $scope.isTermsOfUseValid = function(){
  //   return $scope.termsofuse
  // }

  $scope.messageForDisabledSubmit = function(){
    if ($scope.isFormValid()){
      return ''
    }
    else{
      if (yojs.get('locale') == 'pt-BR'){
        return_message = 'O botão enviar foi desabilitado, pois: <ul>';
        if (!$scope.isHotelNameValid()){
          return_message += '<li>O nome do hotel está em branco.</li>';
        }
        if (!$scope.isContactNameValid()){
          return_message += '<li>O nome do contato está em branco.</li>';
        }
        if (!$scope.isCountryValid()){
           return_message += '<li>O nome do páis está em branco.</li>';
        }
        if (!$scope.isCityValid()){
          return_message += '<li>O nome da cidade está em branco.</li>';
        }
        if (!$scope.isIdSkypeValid()){
          return_message += '<li>O id Skype está em branco.</li>';
        }
        if (!$scope.theContactsAreValid()){
          return_message += '<li>E-mail ou telefone deve ser inserido.</li>';
        }
        // if (!$scope.isTermsOfUseValid()){
        //   return_message += '<li>Aceite os termos de uso.</li>';
        // }
        if (!$scope.isRecaptchaValid()){
          return_message += '<li>Valide o reCaptcha.</li>';
        }
        return_message += '</ul>'
      } else {
        return_message = 'The send button was disabled for the following reasons: <ul>';
        if (!$scope.isHotelNameValid()){
          return_message += '<li>it is necessary that the hotel name is filled</li>';
        }
        if (!$scope.isContactNameValid()){
          return_message += '<li>it is necessary that the contact name is filled</li>';
        }
        if (!$scope.isCountryValid()){
           return_message += '<li>it is necessary that the country is filled</li>';
        }
        if (!$scope.isCityValid()){
          return_message += '<li>it is necessary that the city is filled</li>';
        }
        if (!$scope.isIdSkypeValid()){
          return_message += '<li>it is necessary that the id Skype is filled</li>';
        }
        if (!$scope.theContactsAreValid()){
          return_message += '<li>it is necessary that at least one valid contact is entered</li>';
        }
        // if (!$scope.isTermsOfUseValid()){
        //   return_message += '<li>Accept the terms of use</li>';
        // }
        if (!$scope.isRecaptchaValid()){
          return_message += '<li>the reCaptcha is not valid.</li>';
        }
        return_message += '</ul>'
      }

      // return_message = return_message.replace(/\//g,', ');
      // return_message = return_message.replace(/[,]\s$/g,'.');
      return return_message;
    }
  }

  $scope.isRecaptchaValid = function(response){
    if (!angular.isUndefined(response))
      $scope.RecapResponse = response;
    return (!blankStrPattern.test($scope.RecapResponse));
  }

  $scope.isHotelNameValid = function(){
    return (!blankStrPattern.test($scope.hotelname));
  }

  $scope.isFormValid = function(){
    return ($scope.isRecaptchaValid() && $scope.theContactsAreValid() && $scope.isHotelNameValid() && $scope.isContactNameValid());
  }

  $scope.submitButtonExtraClass = function(){
    return $scope.isFormValid() ? '' : 'submit-disabled'
  }

  $scope.simulePrice = function(){
    $scope.dailyAverage.thirtyEight = $scope.calculatePercentage(38).toFixed(2);
    $scope.dailyAverage.fortyThree = $scope.calculatePercentage(43).toFixed(2);
    $scope.dailyAverage.sixty = $scope.calculatePercentage(60).toFixed(2);
    $scope.dailyAverage.sixtyFive = $scope.calculatePercentage(65).toFixed(2);
  }

  $scope.calculatePercentage = function(percentage){
    return $(".dailyValue").maskMoney('unmasked')[0] / 100 * percentage;
  }

  // $scope.acceptTermsOfUse = function(){
  //   $scope.termsofuse = true;
  //   $('#js-modal-terms-of-use').modal("hide");
  // }

}]);
