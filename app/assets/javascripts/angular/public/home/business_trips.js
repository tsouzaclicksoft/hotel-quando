HotelQuandoApp.controller('PublicBusinessTripsController', ['$scope', '$window', function($scope, $window) {
  var blankStrPattern = /^\s*$/;
  var simpleMailPattern = /.+@.+\..+/;
  $scope.companyname = yojs.get('companyName');
  $scope.phone = yojs.get('phone');
  $scope.message = yojs.get('message');
  $scope.email = yojs.get('emailForContact');
  $scope.contactname = yojs.get('contactName');
  $scope.contactrole = yojs.get('contactRole');
  $scope.RecapResponse = "";

  $scope.setErrorClassForField = function(fieldName, type){
    var fieldWasNotModified = !($(type + '[ng-model='+fieldName.toLowerCase()+']').hasClass('ng-dirty'))
    if (($scope['is' + fieldName + 'Valid']()) || fieldWasNotModified){
      return ''
    }
    else {
      return 'field-with-error'
    }
  }
  $scope.isPhoneValid = function(){
    if (yojs.get('locale') == 'pt-BR'){
      return ($scope.phone.length == 14);
    }
    else{
      return ($scope.phone.length >= 10);
    }
  }

  $scope.isEmailValid = function(){
    return (simpleMailPattern.test($scope.email));
  }

  $scope.isContactNameValid = function(){
    return (!blankStrPattern.test($scope.contactname))
  }

  $scope.isContactRoleValid = function(){
    return (!blankStrPattern.test($scope.contactrole))
  }

  $scope.theContactsAreValid = function(){
    if (!blankStrPattern.test($scope.phone)){
      if (!blankStrPattern.test($scope.email)){
        return ($scope.isPhoneValid() && $scope.isEmailValid());
      }
      else {
        return $scope.isPhoneValid();
      }
    }
    else {
      return $scope.isEmailValid();
    }
  }

  $scope.messageForDisabledSubmit = function(){
    if ($scope.isFormValid()){
      return ''
    }
    else{
      if (yojs.get('locale') == 'pt-BR'){
        return_message = 'O botão enviar foi desabilitado, pois: <ul>';
        if (!$scope.isCompanyNameValid()){
          return_message += '<li>O nome da empresa está em branco.</li>';
        }
        if (!$scope.isContactNameValid()){
          return_message += '<li>O nome do contato está em branco.</li>';
        }
        if (!$scope.theContactsAreValid()){
          return_message += '<li>E-mail ou telefone deve ser inserido.</li>';
        }
        if (!$scope.isMessageValid()){
          return_message += '<li>A mensagem inserida é muito curta.</li>';
        }
        if (!$scope.isRecaptchaValid()){
          return_message += '<li>Valide o reCaptcha.</li>';
        }
        return_message += '</ul>';
      }
      else {
        return_message = 'The send button was disabled for the following reasons: <ul>';
        if (!$scope.isCompanyNameValid()){
          return_message += '<li>it is necessary that the company name is filled</li>';
        }
        if (!$scope.isContactNameValid()){
          return_message += '<li>it is necessary that the contact name is filled</li>';
        }
        if (!$scope.theContactsAreValid()){
          return_message += '<li>it is necessary that at least one valid contact is entered</li>';
        }
        if (!$scope.isMessageValid()){
          return_message += '<li>the provided message is too short</li>';
        }
        if (!$scope.isRecaptchaValid()){
          return_message += '<li>the reCaptcha is not valid.</li>';
        }
        return_message += '</ul>';
      }

      // return_message = return_message.replace(/\//g,', ');
      // return_message = return_message.replace(/[,]\s$/g,'.');
      return return_message;
    }
  }
  $scope.isRecaptchaValid = function(response){
    if (!angular.isUndefined(response))
      $scope.RecapResponse = response;
    return (!blankStrPattern.test($scope.RecapResponse));
  }

  $scope.isCompanyNameValid = function(){
    return (!blankStrPattern.test($scope.companyname));
  }

  $scope.isMessageValid = function(){
    return ((!blankStrPattern.test($scope.message)) && ($scope.message.length > 10));
  }

  $scope.isFormValid = function(){
    return ($scope.isRecaptchaValid() && $scope.theContactsAreValid() && $scope.isCompanyNameValid() && $scope.isMessageValid() && $scope.isContactNameValid());
  }

  $scope.submitButtonExtraClass = function(){
    return $scope.isFormValid() ? '' : 'submit-disabled'
  }

}]);
