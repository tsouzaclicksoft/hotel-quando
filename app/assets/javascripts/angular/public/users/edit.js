HotelQuandoApp.controller('PublicUsersEditCtrl', ['$scope', '$window', function($scope, $window) {
  var autocomplete;
  var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'long_name',
    country: 'short_name',
    postal_code: 'short_name'
  };
  
  function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place = $scope.autocomplete.getPlace();

    for (var component in componentForm) {
      document.getElementById(component).value = '';
      document.getElementById(component).disabled = false;
    }

    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
      var addressType = place.address_components[i].types[0];
      if (componentForm[addressType]) {
        var val = place.address_components[i][componentForm[addressType]];
        document.getElementById(addressType).value = val;
      }
    }
  }

  var initialize = function() {
    $scope.autocomplete = new google.maps.places.Autocomplete(
      (document.getElementById('autocomplete')), 
      {types: ['geocode']}
    );

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    $scope.autocomplete.addListener('place_changed', fillInAddress);

  }
  $('.autocomplete').onload = initialize();

}]);
