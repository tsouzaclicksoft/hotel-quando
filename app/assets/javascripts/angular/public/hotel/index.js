HotelQuandoApp.controller('PublicHotelsCtrl', ['$scope', '$window', function($scope, $window) {

  $scope.selectedMinimumPriceValue = 0;
  $scope.selectedMaximumPriceValue = 0;
  $scope.selectedMaximumDistanceValue = 0;
  $scope.selectedHotelCategory = 'all';
  $scope.filterCollapsed = true;

  $scope.colapseFilter = function(){
    $scope.filterCollapsed = true;
    $('html, body').animate({
      scrollTop: (100)
    }, 1000);
  }

  $scope.removeFilters = function(){
    $scope.selectedMinimumPriceValue = 0;
    $scope.selectedMaximumPriceValue = 0;
    $scope.selectedMaximumDistanceValue = 0;
    $scope.selectedHotelCategory = 'all';
    $('.js-minimum-price').val(0);
    $('.js-maximum-price').val(0);
    $('.js-maximum-distance').val(0);
    $('.js-minimum-price').maskMoney('mask');
    $('.js-maximum-price').maskMoney('mask');
    $('.js-maximum-distance').maskMoney('mask');
    $('.js-hotel-category').val('all');
  }

  $scope.anyHotelMatchFilter = function(){
    return ($('.js-hotel-row:not(.ng-hide)').length > 0)
  }

  $scope.anyExactHotelMatchFilter = function(){
    return (($('.js-hotel-row:not(.ng-hide)').length - $('.js-similar-hotel-row:not(.ng-hide)').length) > 0)
  }

  $scope.anySimilarHotelMatchFilter = function(){
    return ($('.js-similar-hotel-row:not(.ng-hide)').length > 0)
  }

  $scope.hotelExactMatchLength = function(){
    return ($('.js-hotel-row:not(.ng-hide)').length - $('.js-similar-hotel-row:not(.ng-hide)').length)
  }

  $scope.hotelSimilarMatchLength = function(){
    return ($('.js-similar-hotel-row:not(.ng-hide)').length)
  }

  $scope.hotelPluralize = function(count){
    var locale = yojs.get("locale");
    var text = "";
    if(locale == "pt-BR"){
      if(count != 1){
        text = ' hotéis';
      }else{
        text = ' hotel';
      }
    } else if(locale == "en"){
      if(count != 1){
        text = ' hotels';
      }else{
        text = ' hotel';
      }
    } else if(locale == "es"){
      if(count != 1){
        text = ' hoteles';
      }else{
        text = ' hotel';
      }
    }
    return text;
  }

  $scope.meetingRoomPluralize = function(count){
    var locale = yojs.get("locale");
    var text = "";
    if(locale == "pt-BR"){
      if(count != 1){
        text = ' salas de reunião';
      }else{
        text = ' sala de reunião';
      }
    } else if(locale == "en"){
      if(count != 1){
        text = ' meeting rooms';
      }else{
        text = ' meeting room';
      }
    }
    return text;
  }

  $scope.eventRoomPluralize = function(count){
    var locale = yojs.get("locale");
    var text = "";
    if(locale == "pt-BR"){
      if(count != 1){
        text = ' salões de evento';
      }else{
        text = ' salão de evento';
      }
    } else if(locale == "en"){
      if(count != 1){
        text = ' event rooms';
      }else{
        text = ' event room';
      }
    }
    return text;
  }

  $scope.hotelPriceAndCategoryMatchFilter = function(price, category){
    if ($scope.selectedHotelCategory == 'all'){
      return ($scope.hotelPriceMatchFilter(price));
    }
    else{
      return (($scope.hotelPriceMatchFilter(price)) && ($scope.selectedHotelCategory == category));
    }
  }

  $scope.hotelPriceAndCategoryAndDistanceMatchFilter = function(price, category, distance){
    // check if selected distance is numeric and not equal 0
    if (((!isNaN(parseFloat($scope.selectedMaximumDistanceValue))) && (isFinite($scope.selectedMaximumDistanceValue))) && !($scope.selectedMaximumDistanceValue <= 0)){
      return (($scope.hotelPriceAndCategoryMatchFilter(price, category)) && (distance <= $scope.selectedMaximumDistanceValue))
    }
    else{
      return ($scope.hotelPriceAndCategoryMatchFilter(price, category))
    }
  }

  $scope.hotelPriceMatchFilter = function(price){
    if ((!$scope.selectedMaximumPriceValue == 0) && (!$scope.selectedMinimumPriceValue == 0)){
      return ((price >= $scope.selectedMinimumPriceValue) && (price <= $scope.selectedMaximumPriceValue));
    }
    else{
      if (!$scope.selectedMaximumPriceValue == 0){
        return (price <= $scope.selectedMaximumPriceValue);
      }
      else{
        if (!$scope.selectedMinimumPriceValue == 0){
          return (price >= $scope.selectedMinimumPriceValue);
        }
        else{
          return true;
        }
      }
    }
  }

  $scope.filterSearchResults = function(){
    $scope.selectedMinimumPriceValue = $('.js-minimum-price').maskMoney('unmasked')[0];
    $scope.selectedMaximumPriceValue = $('.js-maximum-price').maskMoney('unmasked')[0];
    $scope.selectedMaximumDistanceValue = $('.js-maximum-distance').maskMoney('unmasked')[0];
    $scope.selectedHotelCategory = $('.js-hotel-category').val();
  }
}]);
