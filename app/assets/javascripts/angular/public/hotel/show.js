HotelQuandoApp.controller('PublicHotelCtrl', ['$scope', '$window', function($scope, $window) {

  $scope.showAllPhotos = yojs.get('allPhotosShown');

  $scope.showPhoto = function(photoNumber) {
    if($scope.showAllPhotos || yojs.get('allPhotosShown')) {
      return true;
    }
    else {
      if(photoNumber <= 3) {
        return true;
      }
      else {
        return false;
      }
    }
  }

  var seeMorePhotosButtonHtml = '<i class="glyphicon glyphicon-plus"></i>' + ' ' + yojs.get('seeMore');
  var seeLessPhotosButtonHtml = '<i class="glyphicon glyphicon-minus"></i>' + ' ' + yojs.get('seeLess');

  $scope.changePhotoExhibitionMode = function(){
    if(!$scope.isMobile()){
      if ($scope.showAllPhotos) {
        $('html, body').animate({
          scrollTop: (48)
        }, 1000);
        $('.js-photo-exhibition-toggle-button').html(seeMorePhotosButtonHtml);
      }
      else {
        $('html, body').animate({
          scrollTop: (142)
        }, 1000);
        $('.js-photo-exhibition-toggle-button').html(seeLessPhotosButtonHtml);
      }
    } else {
      if ($scope.showAllPhotos) {
        $('.js-photo-exhibition-toggle-button').html(seeMorePhotosButtonHtml);
      } else {
        $('.img').slice(3).each(function() {
          $(this).removeClass('animated fadeOut');
          $(this).addClass('animated fadeIn');
        });
        $('.js-photo-exhibition-toggle-button').html(seeLessPhotosButtonHtml);
      }
    }


    $scope.showAllPhotos = !$scope.showAllPhotos;
  }

  $scope.isMobile = function() {
    if( navigator.userAgent.match(/Android/i)
     || navigator.userAgent.match(/webOS/i)
     || navigator.userAgent.match(/iPhone/i)
     || navigator.userAgent.match(/iPad/i)
     || navigator.userAgent.match(/iPod/i)
     || navigator.userAgent.match(/BlackBerry/i)
     || navigator.userAgent.match(/Windows Phone/i)
    ){
      return true;
    } else {
      return false;
    }
  }
}]);


HotelQuandoApp.controller('PublicSearchHotelCtrl', ['$scope', '$window', function($scope, $window) {


  $scope.checkinHour = 00;
  $scope.checkoutHour = 03;
  $scope.numberOfPeople = 1;
  $scope.address = '';
  $scope.timezoneSearched = '';
  $scope.timezone = {
    timezone_value: 0,
    timezone_id: '',
    timezone_name: ''
  };
  $scope.checkinHourMTR = 00;
  $scope.checkoutHourMTR = 01;
  $scope.selectedPackSizeMTR = 1;
  $scope.numberOfPeopleMeetingRoom = 1;

  $scope.latitude = '';

  $scope.longitude = '';

  $scope.isSearchDateValid = false;

  $scope.isLatitudeAndLongitudeValid = false;

  $scope.selectedPackSize = 3;
  $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
  $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
  $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0)
  $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)

  $scope.moreHours = false;

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  angular.element(document).ready(function () {
    $scope.showNextDayOrSameDayText();
    $scope.disableCheckinButtons();
    $scope.changeNumberOfPeople();
  });

  $scope.changeMoreHours = function(operation){
    if(operation == "plus"){
      $scope.moreHours = true;
      $scope.checkinHour = 18;
      $scope.checkoutHour = 7;
      $scope.selectedPackSize = 13
      $scope.disableCheckinButtons();
      $scope.disableCheckoutButtons();
    }
    else if(operation == "minus"){
      $scope.moreHours = false;
      $scope.checkinHour = 0;
      $scope.checkoutHour = 3;
      $scope.selectedPackSize = 3;
      $scope.disableCheckinButtons();
      $scope.disableCheckoutButtons();
    }
  }

  $scope.changeCheckinHour = function(operation){
    if(operation == "minus"){
      $scope.checkinHour -= 1;
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0);
      if ($scope.checkoutHour == 0){
        $scope.checkoutHour = 23
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else if ($scope.selectedPackSize > 12 && $scope.checkoutHour == 7){
        $scope.checkoutHour;
        $scope.selectedPackSize += 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else {
        $scope.checkoutHour -= 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      }
    }
    else if(operation == "plus"){
      $scope.checkinHour += 1;
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0);
      if ($scope.checkoutHour == 23){
        $scope.checkoutHour = 0
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else if ($scope.selectedPackSize > 12 && $scope.checkoutHour == 12){
        $scope.checkoutHour;
        $scope.selectedPackSize -= 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else {
        $scope.checkoutHour += 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      }
    }
    $scope.disableCheckinButtons();
    $scope.disableCheckoutButtons();
    $scope.showNextDayOrSameDayText();
    $scope.setSearchDateValidity();
  }

  $scope.changeCheckinHourMTR = function(operation){
    if(operation == "minus"){
      $scope.checkinHourMTR -= 1;
      $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0);
      if ($scope.checkoutHourMTR == 0){
        $scope.checkoutHourMTR = 23
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else if ($scope.selectedPackSizeMTR > 12 && $scope.checkoutHourMTR == 7){
        $scope.checkoutHourMTR;
        $scope.selectedPackSizeMTR += 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else {
        $scope.checkoutHourMTR -= 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      }
    }
    else if(operation == "plus"){
      $scope.checkinHourMTR += 1;
      $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0);
      if ($scope.checkoutHourMTR == 23){
        $scope.checkoutHourMTR = 0
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else if ($scope.selectedPackSizeMTR > 12 && $scope.checkoutHourMTR == 12){
        $scope.checkoutHourMTR;
        $scope.selectedPackSizeMTR -= 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else {
        $scope.checkoutHourMTR += 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      }
    }
    $scope.disableCheckinButtonsMTR();
    $scope.disableCheckoutButtonsMTR();
    $scope.showNextDayOrSameDayText();
    $scope.setSearchDateValidityMTR();
  }

  $scope.changeCheckoutHour = function(operation){
    if(operation == "minus"){
      $scope.checkoutHour -= 1;
      $scope.selectedPackSize -= 1;
    }
    else if(operation == "plus"){
      $scope.checkoutHour += 1;
      $scope.selectedPackSize += 1;
    }
    $scope.disableCheckoutButtons();
  }

  $scope.changeCheckoutHourMTR = function(operation){
    if(operation == "minus"){
      $scope.checkoutHourMTR -= 1;
      $scope.selectedPackSizeMTR -= 1;
    }
    else if(operation == "plus"){
      $scope.checkoutHourMTR += 1;
      $scope.selectedPackSizeMTR += 1;
    }
    $scope.disableCheckoutButtonsMTR();
  }


  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  $scope.set_checkin_checkout_hour = function(){
    var checkin, pack_size, checkout;
    pack_size = parseInt(getParameterByName("length_of_pack", window.location))
    checkin = parseInt(getParameterByName("checkin_hour", window.location))
    checkout = checkin + pack_size
    if( (checkin.toString() != 'NaN') && (pack_size.toString() != 'NaN') ){
      $scope.checkinHourDate = new Date().setHours(checkin, 0, 0)
      $scope.checkoutHourDate = new Date().setHours(checkout, 0, 0)
    }else{
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
      $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
    }
  }

  $scope.set_checkin_checkout_hourMTR = function(){
    var checkin, pack_size, checkout;
    pack_size = ( parseInt(getParameterByName("by_pack_lengthMTR", window.location)) || parseInt(getParameterByName("length_of_pack", window.location)) )
    checkin = ( parseInt(getParameterByName("by_checkin_hourMTR", window.location)) || parseInt(getParameterByName("checkin_hour", window.location)) )
    checkout = checkin + pack_size
    if( (checkin.toString() != 'NaN') && (pack_size.toString() != 'NaN') ){
      $scope.checkinHourDateMTR = new Date().setHours(checkin, 0, 0)
      $scope.checkoutHourDateMTR = new Date().setHours(checkout, 0, 0)
      $scope.checkinHourMTR = checkin
      $scope.selectedPackSizeMTR = pack_size
      $scope.checkoutHourMTR = checkout
      $scope.checkinDateMTR = getParameterByName("checkin_date", window.location)
    }else{
      $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0)
      $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)
    }
  }

  $scope.changePackSize = function(){
    var checkout = $scope.checkinHour + $scope.selectedPackSize
    if (checkout >= 24){
      checkout -= 24
    }
    $scope.checkoutHour = checkout
    $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
    $scope.disableCheckinButtons();
    $scope.showNextDayOrSameDayText()
  }

  $scope.changePackSizeMTR = function(){
    var checkout = $scope.checkinHourMTR + $scope.selectedPackSizeMTR
    if (checkout >= 24){
      checkout -= 24
    }
    $scope.checkoutHourMTR = checkout
    $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)
    $scope.disableCheckinButtonsMTR();
    $scope.showNextDayOrSameDayText()
  }

  $scope.disableCheckinButtons = function(){
    if ($scope.moreHours == true  || $scope.moreHours == 'true'){
      if ($scope.checkinHour == 18){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHour == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
    else {
      if ($scope.checkinHour == 0){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHour == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
  }

  $scope.disableCheckinButtonsMTR = function(){
    if ($scope.moreHours == true  || $scope.moreHours == 'true'){
      if ($scope.checkinHourMTR == 18){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHourMTR == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
    else {
      if ($scope.checkinHourMTR == 0){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHourMTR == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
  }

  $scope.disableCheckoutButtons = function(){
    var difference_hours = ($scope.checkoutHour + 24) - $scope.checkinHour
    if ($scope.checkoutHour == 12){
      if ($scope.checkinHour == 23){
        $('.minus-checkout-hour').prop('disabled', true)
        $('.plus-checkout-hour').prop('disabled', true)
      } else {
        $('.minus-checkout-hour').prop('disabled', false)
        $('.plus-checkout-hour').prop('disabled', true)
      }
    } else if ($scope.checkoutHour == 7){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 13 && $scope.selectedPackSize < 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 12 && $scope.selectedPackSize > 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else {
      $('.minus-checkout-hour').prop('disabled', false)
      $('.plus-checkout-hour').prop('disabled', false)
    }
  }

  $scope.disableCheckoutButtonsMTR = function(){
    var difference_hours = ($scope.checkoutHourMTR + 24) - $scope.checkinHourMTR
    if ($scope.checkoutHourMTR == 12){
      if ($scope.checkinHourMTR == 23){
        $('.minus-checkout-hour').prop('disabled', true)
        $('.plus-checkout-hour').prop('disabled', true)
      } else {
        $('.minus-checkout-hour').prop('disabled', false)
        $('.plus-checkout-hour').prop('disabled', true)
      }
    } else if ($scope.checkoutHourMTR == 7){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 13 && $scope.selectedPackSizeMTR < 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 12 && $scope.selectedPackSizeMTR > 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else {
      $('.minus-checkout-hour').prop('disabled', false)
      $('.plus-checkout-hour').prop('disabled', false)
    }
  }

  $scope.changeNumberOfPeople = function(operation){
    if(operation == "minus"){
      $scope.numberOfPeople -= 1
      if ($scope.numberOfPeople == 1){
        $('.minus-number-of-people').prop('disabled', true)
      }else{
        $('.plus-number-of-people').prop('disabled', false)
      }
    }
    else if(operation == "plus"){
      $scope.numberOfPeople += 1
      if ( $scope.numberOfPeople == 4){
        $('.plus-number-of-people').prop('disabled', true)
      }else{
        $('.minus-number-of-people').prop('disabled', false)
      }
    }
    else{
      if ($scope.numberOfPeople == 1){
        $('.minus-number-of-people').prop('disabled', true)
      }else if($scope.numberOfPeople == 4){
        $('.plus-number-of-people').prop('disabled', true)
      }else{
        $('.plus-number-of-people').prop('disabled', false)
        $('.minus-number-of-people').prop('disabled', false)
      }
    }
  }

  $scope.showNextDayOrSameDayText = function(){
    if ($scope.selectedPackSize == 3 && $scope.checkinHour >= 21){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 6 && $scope.checkinHour >= 18){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 9 && $scope.checkinHour >= 15){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 12&& $scope.checkinHour >= 12){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else{
      $('.next-day').hide()
      $('.same-day').show()
    }
  }


  $scope.changeBackgroundAndClearForm = function(){
    var wrapper = $('.search-hotels-wrapper')
    //tab 1 = night room
    //tab 2 = morning room
    //tab 3 = any time room
    /*
    if ($scope.selectedTabRoom == 1){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper night-rooms-background');
      $scope.selectedPackSize = 12;
      $scope.numberOfPeople = 1;
      $scope.checkinHour = 18;
      $scope.checkoutHour = 06;
      $('.describe-night').show()
      $('.describe-day').hide()
      $('.describe-any-time').hide()
    }
    if ($scope.selectedTabRoom == 2){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper morning-rooms-background');
      $scope.selectedPackSize = 3;
      $scope.numberOfPeople = 1;
      $scope.checkinHour = 06;
      $scope.checkoutHour = 09;
      $('.describe-night').hide()
      $('.describe-day').show()
      $('.describe-any-time').hide()
    }
    if ($scope.selectedTabRoom == 3){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper any-time-rooms-background');
      $scope.selectedPackSize = 3;
      $scope.numberOfPeople = 1;
      $scope.checkinHour = 0;
      $scope.checkoutHour = 03;
      $('.describe-night').hide()
      $('.describe-day').hide()
      $('.describe-any-time').show()
    }
    */
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper any-time-rooms-background');
      $scope.selectedPackSize = 3;
      $scope.numberOfPeople = 1;
      $scope.checkinHour = 0;
      $scope.checkoutHour = 03;
      $('.describe-night').hide()
      $('.describe-day').hide()
      $('.describe-any-time').show()

    /*if ($scope.selectedTabRoom == 3){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper event-rooms-background');
      $scope.selectedPackSize = 2;
      $scope.numberOfPeople = 20;
    }*/
    $scope.address = '';
    $scope.timezone = {};
    $scope.latitude = '';
    $scope.longitude = '';
    $scope.isSearchDateValid = false;
    $scope.isLatitudeAndLongitudeValid = false;
    $scope.checkinDate = '';
    $scope.checkoutDate = '';
    $scope.disableCheckinButtons()
    $scope.changeNumberOfPeople()
    $scope.showNextDayOrSameDayText()
    $('.js-autocomplete').onload = initialize('js-autocomplete-3'); // + $scope.selectedTabRoom);
  }

  $scope.changeTab = function(type){
    //$scope.selectedTabRoom = type
    //$scope.changeBackgroundAndClearForm()
  }

  $scope.setSearchDate = function(){
    var parsedDate;
    var locale = yojs.get("locale");
    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);
      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/' + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/' + parsedDate.getFullYear();
    }
    else parsedDate = $scope.checkinDate;

    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = dateStr.split('/');
      dateToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
      dateInt = dateToSearch.getTime() - minimumDateToSearch;
      // Check timezone (requesting our server) only if difference between +/- 24h
      if(typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateInt > -82799000) && (dateInt < 82801000) && ($scope.latitude != '') )
          $scope.getTimezoneFromGoogle($scope.latitude, $scope.longitude);
    }
    if($scope.moreHours == true || $scope.moreHours == 'true'){
      var dataFormat = (locale == "en" ? "mm/dd/yyyy" : "dd/mm/yyyy")
      var tomorrow = dateToSearch.setDate(dateToSearch.getDate() +1);
      var endDate = new Date(tomorrow);
      endDate.setDate(endDate.getDate()+2);
      var formatedTomorrow = dateToSearch.toLocaleDateString(locale);
      $scope.checkoutDate = formatedTomorrow;
      $('.cpy-checkout-date-calendar-input').datepicker('setStartDate', dateToSearch.toLocaleDateString(locale));
      $('.cpy-checkout-date-calendar-input').datepicker('setEndDate', endDate.toLocaleDateString(locale));
      $scope.parsedSelectedCheckoutDate = formatedTomorrow;
      $scope.selectedPackSize = ($scope.checkoutHour + 24) - $scope.checkinHour;;
    }
    $scope.setSearchDateValidity();
  }

  $scope.setSearchDateMTR = function(){
    var parsedDate;
    var locale = yojs.get("locale");
    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDateMTR);
      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/' + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/' + parsedDate.getFullYear();
    }
    else parsedDate = $scope.checkinDateMTR;

    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = dateStr.split('/');
      dateToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR);
      dateInt = dateToSearch.getTime() - minimumDateToSearch;
      // Check timezone (requesting our server) only if difference between +/- 24h
      if(typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateInt > -82799000) && (dateInt < 82801000) && ($scope.latitude != '') )
          $scope.getTimezoneFromGoogleMTR($scope.latitude, $scope.longitude);
    }
    if($scope.moreHours == true || $scope.moreHours == 'true'){
      var dataFormat = (locale == "en" ? "mm/dd/yyyy" : "dd/mm/yyyy")
      var tomorrow = dateToSearch.setDate(dateToSearch.getDate() +1);
      var endDate = new Date(tomorrow);
      endDate.setDate(endDate.getDate()+2);
      var formatedTomorrow = dateToSearch.toLocaleDateString(locale);
      $scope.checkoutDateMTR = formatedTomorrow;
      $('.cpy-checkout-date-calendar-input').datepicker('setStartDate', dateToSearch.toLocaleDateString(locale));
      $('.cpy-checkout-date-calendar-input').datepicker('setEndDate', endDate.toLocaleDateString(locale));
      $scope.parsedSelectedCheckoutDateMTR = formatedTomorrow;
      $scope.selectedPackSizeMTR = ($scope.checkoutHourMTR + 24) - $scope.checkinHourMTR;;
    }
    $scope.setSearchDateValidityMTR();
  }

  $scope.setSearchEndDate = function(){
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    if(yojs.get("locale") == "en"){
      parsedCheckinDate = new Date($scope.checkinDate);
      parsedCheckinDate = ('0' + parsedCheckinDate.getDate()).slice(-2) + '/' + ('0' + (parsedCheckinDate.getMonth()+1)).slice(-2) + '/' + parsedCheckinDate.getFullYear();
      parsedCheckoutDate = new Date($scope.checkoutDate);
      parsedCheckoutDate = ('0' + parsedCheckoutDate.getDate()).slice(-2) + '/' + ('0' + (parsedCheckoutDate.getMonth()+1)).slice(-2) + '/' + parsedCheckoutDate.getFullYear();
    }
    else {
      parsedCheckinDate = $scope.checkinDate;
      parsedCheckoutDate = $scope.checkoutDate;
    }

    if(datePattern.test(parsedCheckinDate)){
      date = parsedCheckinDate.split('/');
      checkinToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
    }
    if(datePattern.test(parsedCheckoutDate)){
      date = parsedCheckoutDate.split('/');
      checkoutToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkoutHour);
    }


    var firstDate = new Date(checkinToSearch);
    var secondDate = new Date(checkoutToSearch);

    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    if (diffDays == 1){
      $scope.selectedPackSize = ($scope.checkoutHour + 24) - $scope.checkinHour
    } else if (diffDays == 2){
      $scope.selectedPackSize = ($scope.checkoutHour + 48) - $scope.checkinHour
    } else {
      $scope.selectedPackSize = ($scope.checkoutHour + 72) - $scope.checkinHour
    }
  }

  $scope.setSearchEndDateMTR = function(){
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    if(yojs.get("locale") == "en"){
      parsedCheckinDate = new Date($scope.checkinDate);
      parsedCheckinDate = ('0' + parsedCheckinDate.getDate()).slice(-2) + '/' + ('0' + (parsedCheckinDate.getMonth()+1)).slice(-2) + '/' + parsedCheckinDate.getFullYear();
      parsedCheckoutDate = new Date($scope.checkoutDate);
      parsedCheckoutDate = ('0' + parsedCheckoutDate.getDate()).slice(-2) + '/' + ('0' + (parsedCheckoutDate.getMonth()+1)).slice(-2) + '/' + parsedCheckoutDate.getFullYear();
    }
    else {
      parsedCheckinDate = $scope.checkinDate;
      parsedCheckoutDate = $scope.checkoutDate;
    }

    if(datePattern.test(parsedCheckinDate)){
      date = parsedCheckinDate.split('/');
      checkinToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
    }
    if(datePattern.test(parsedCheckoutDate)){
      date = parsedCheckoutDate.split('/');
      checkoutToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkoutHour);
    }


    var firstDate = new Date(checkinToSearch);
    var secondDate = new Date(checkoutToSearch);

    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    if (diffDays == 1){
      $scope.selectedPackSizeMTR = ($scope.checkoutHourMTR + 24) - $scope.checkinHourMTR
    } else if (diffDays == 2){
      $scope.selectedPackSizeMTR = ($scope.checkoutHourMTR + 48) - $scope.checkinHourMTR
    } else {
      $scope.selectedPackSizeMTR = ($scope.checkoutHourMTR + 72) - $scope.checkinHourMTR
    }
  }

  $scope.setSearchDateValidity = function(){
    if (typeof $scope.checkinDate == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.checkinDate);

      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour); // subtract 1 from month because javascript start month in 0 = jan
      } else {
        parsedDate = $scope.checkinDate;
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
      }
      $scope.parsedSelectedCheckinDate = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;

      // Consider timezone when search datetime is between +/- 23h from local time
      if (typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateToInt > -82799000) && (dateToInt < 82801000) && ($scope.latitude != '') )
          minimumDateToSearch += $scope.timezone.timezone_value + (new Date().getTimezoneOffset()*60000);
    }

    if(datePattern.test(parsedDate)){
      if(dateToSearch >= minimumDateToSearch)
        $scope.isSearchDateValid = true
      else $scope.isSearchDateValid = false;
    }
    else $scope.isSearchDateValid = false;
  };

  $scope.setSearchDateValidityMTR = function(){
    if (typeof $scope.checkinDateMTR == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.checkinDateMTR);

      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR); // subtract 1 from month because javascript start month in 0 = jan
      } else {
        parsedDate = $scope.checkinDateMTR;
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR);
      }
      $scope.parsedSelectedCheckinDateMTR = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;

      // Consider timezone when search datetime is between +/- 23h from local time
      if (typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateToInt > -82799000) && (dateToInt < 82801000) && ($scope.latitude != '') )
          minimumDateToSearch += $scope.timezone.timezone_value + (new Date().getTimezoneOffset()*60000);
    }

    if(datePattern.test(parsedDate)){
      if(dateToSearch >= minimumDateToSearch)
        $scope.isSearchDateValidMTR = true
      else $scope.isSearchDateValidMTR = false;
    }
    else $scope.isSearchDateValidMTR = false;
  };

  $scope.setLatitudeAndLongitudeValidity = function(){
    // Check if latitude and longitude are numeric
    var latitudeValid = ((!isNaN(parseFloat($scope.latitude))) && (isFinite($scope.latitude)))
    var longitudeValid = ((!isNaN(parseFloat($scope.longitude))) && (isFinite($scope.longitude)))
    $scope.isLatitudeAndLongitudeValid = (latitudeValid && longitudeValid)
  }

  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    console.log("isFormValid >>  + $scope.isSearchDateValid");
    return ($scope.isSearchDateValid);
  }

  $scope.isFormValidMTR = function(){
    $scope.setSearchDateValidityMTR();
    return ($scope.isSearchDateValidMTR);
  }

  $scope.resonsForDisabledSubmit = function(){
    if(!($scope.isSearchDateValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
    }
    else {
      return '<ul><li>' + yojs.get('error_message_for_address') + '</li></ul>';
    }
  }

  $scope.getTimezoneFromGoogle = function(lat, lng) {
    if (lat != "") {
      if ($scope.checkinDate == "")
        var tmstp = Math.round(((new Date()).getTime()/1000))
      else {
        var d = $scope.checkinDate.split("/");
        // Get user timezone - local
        var splitDateTimeNow = new Date().toString().split(" ");
        timeZoneFormatted = splitDateTimeNow[splitDateTimeNow.length - 2] + " " + splitDateTimeNow[splitDateTimeNow.length - 1];
        // Format datetime with timezone to send to server
        if (yojs.get("locale") == "en")
              var tmstp = new Date(d[2], ( d[0]-1 ), d[1], ($scope.checkinHour).toString(), "0").getTime() / 1000
        else  var tmstp = new Date(d[2], ( d[1]-1 ), d[0], $scope.checkinHour.toString(), "0").getTime() / 1000;
      }
      publicSearchFactory.getTZ($scope.latitude, $scope.longitude, tmstp)
        .then(function(arrItems){
          $scope.timezone = arrItems;
          $scope.timezoneSearched = arrItems.timezone_id;
      });
    }
  }

  $scope.getTimezoneFromGoogleMTR = function(lat, lng) {
    if (lat != "") {
      if ($scope.checkinDateMTR == "")
        var tmstp = Math.round(((new Date()).getTime()/1000))
      else {
        var d = $scope.checkinDate.split("/");
        // Get user timezone - local
        var splitDateTimeNow = new Date().toString().split(" ");
        timeZoneFormatted = splitDateTimeNow[splitDateTimeNow.length - 2] + " " + splitDateTimeNow[splitDateTimeNow.length - 1];
        // Format datetime with timezone to send to server
        if (yojs.get("locale") == "en")
              var tmstp = new Date(d[2], ( d[0]-1 ), d[1], ($scope.checkinHourMTR).toString(), "0").getTime() / 1000
        else  var tmstp = new Date(d[2], ( d[1]-1 ), d[0], $scope.checkinHourMTR.toString(), "0").getTime() / 1000;
      }
      publicSearchFactory.getTZ($scope.latitude, $scope.longitude, tmstp)
        .then(function(arrItems){
          $scope.timezone = arrItems;
          $scope.timezoneSearched = arrItems.timezone_id;
      });
    }
  }

}]);
