HotelQuandoApp.controller('PublicAffiliateHotelCtrl', ['$scope', '$window', function($scope, $window) {

  $scope.showAllPhotos = yojs.get('allPhotosShown');

  $scope.showPhoto = function(photoNumber) {
    if($scope.showAllPhotos || yojs.get('allPhotosShown')) {
      return true;
    }
    else {
      if(photoNumber <= 3) {
        return true;
      }
      else {
        return false;
      }
    }
  }

  var seeMorePhotosButtonHtml = '<i class="glyphicon glyphicon-plus"></i>' + ' ' + yojs.get('seeMore');
  var seeLessPhotosButtonHtml = '<i class="glyphicon glyphicon-minus"></i>' + ' ' + yojs.get('seeLess');

  $scope.changePhotoExhibitionMode = function(){
    if(!$scope.isMobile()){
      if ($scope.showAllPhotos) {
        $('html, body').animate({
          scrollTop: (48)
        }, 1000);
        $('.js-photo-exhibition-toggle-button').html(seeMorePhotosButtonHtml);
      }
      else {
        $('html, body').animate({
          scrollTop: (142)
        }, 1000);
        $('.js-photo-exhibition-toggle-button').html(seeLessPhotosButtonHtml);
      }
    } else {
      if ($scope.showAllPhotos) {
        $('.js-photo-exhibition-toggle-button').html(seeMorePhotosButtonHtml);
      } else {
        $('.img').slice(3).each(function() {
          $(this).removeClass('animated fadeOut');
          $(this).addClass('animated fadeIn');
        });
        $('.js-photo-exhibition-toggle-button').html(seeLessPhotosButtonHtml);
      }
    }


    $scope.showAllPhotos = !$scope.showAllPhotos;
  }

  $scope.isMobile = function() {
    if( navigator.userAgent.match(/Android/i)
     || navigator.userAgent.match(/webOS/i)
     || navigator.userAgent.match(/iPhone/i)
     || navigator.userAgent.match(/iPad/i)
     || navigator.userAgent.match(/iPod/i)
     || navigator.userAgent.match(/BlackBerry/i)
     || navigator.userAgent.match(/Windows Phone/i)
    ){
      return true;
    } else {
      return false;
    }
  }
}]);
