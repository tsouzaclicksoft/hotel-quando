HotelQuandoApp.controller('HotelRoomTypesFormCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.roomRegistrationDropdown = yojs.get('roomRegistrationDropdown');

  $scope.roomsLabel = function(){
    if($scope.roomRegistrationDropdown){
      return 'Quantidade de quartos'
    }
    else{
      return 'Números dos quartos'
    }
  }

  $scope.changeRoomsRegistrationType = function(){
    $scope.roomRegistrationDropdown = !$scope.roomRegistrationDropdown;
  }

  $scope.roomRegistrationMethod = function(){
    if($scope.roomRegistrationDropdown){
      return 'Dropdown'
    }
    else{
      return 'Text'
    }
  }

}]);
