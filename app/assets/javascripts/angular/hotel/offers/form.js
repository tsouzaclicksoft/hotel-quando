HotelQuandoApp.controller('HotelOffersFormCtrl', ['$scope', '$window', '$http', function($scope, $window, $http) {
  // we need this because ng-change on Date fields is triggered multiple times
  // per change. With this flagh we allow only 1 request
  var gettingOfferCountPerRoom = false;

  function getOfferCountPerRoom() {
    if (!gettingOfferCountPerRoom) {
      gettingOfferCountPerRoom = true;

      var initialDateStr = $(".js-initial-date").val();
      var finalDateStr = $(".js-final-date").val();
      var params = {
        by_checkin_range: {
          initial_date: initialDateStr,
          final_date: finalDateStr
        }
      };

      if (yojs.get('locale') == "pt-BR"){
        $http({
          url: Routes.count_per_room_hotel_offers_pt_br_path(params),
          method: "get"
        }).success(function(data, status, headers, config) {
          $scope.offersCountPerRoom = data;
          $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
        }).error(function(data, status, headers, config) {
          $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
          $window.alert('Desculpe. Não conseguimos contabilizar as ofertas por quarto.');
        });
      } else if (yojs.get('locale') == "es") {
        $http({
          url: Routes.count_per_room_hotel_offers_es_path(params),
          method: "get"
        }).success(function(data, status, headers, config) {
          $scope.offersCountPerRoom = data;
          $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
        }).error(function(data, status, headers, config) {
          $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
          $window.alert('Disculpe. No podemos contabilizar las ofertas por cuarto.');
        });        
      } else {
        $http({
          url: Routes.count_per_room_hotel_offers_en_path(params),
          method: "get"
        }).success(function(data, status, headers, config) {
          $scope.offersCountPerRoom = data;
          $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
        }).error(function(data, status, headers, config) {
          $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
          $window.alert('Sorry. We can not count offers per room.');
        });        
      }
    }
  }

  $scope.availablePacks = [
    { period: '3 horas', period_without_unit: '3', checked: true},
    { period: '6 horas', period_without_unit: '6', checked: true},
    { period: '9 horas', period_without_unit: '9', checked: true},
    { period: '12 horas', period_without_unit: '12', checked: true}
  ];

  $scope.priceTablesById = yojs.get('priceTablesByIdJson');
  $scope.roomTypesById = yojs.get('roomTypesByIdJson');
  $scope.isAnyDayOfWeekChecked = false;
  $scope.isAnyCheckinTimeChecked = false;
  $scope.intialDate = '';
  $scope.finalDate = '';

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  var firstIteration = true
  angular.forEach($scope.priceTablesById, function(value, key){
    if (firstIteration){
      $scope.selectedPriceTable = value;
      firstIteration = false;
    }
  })

  if (yojs.isDefined('offerCreation')){
    angular.forEach($scope.roomTypesById, function(value, key){
      value.checked = true;
      angular.forEach(value.active_rooms, function(value, key){
        value.checked = true;
      });
    });
  }

  $scope.selectedTablePriceDoesNotContainsRoomType = function(roomTypeId) {
    return $scope.selectedPriceTable.roomTypesId.indexOf(parseInt(roomTypeId)) == -1;
  };

  $scope.selectedTablePriceIsNotComplete = function() {
    var tablePriceIsNotComplete = false;
    angular.forEach($scope.roomTypesById, function(value, key){
      if ($scope.selectedTablePriceDoesNotContainsRoomType(value.id)){
        tablePriceIsNotComplete = true;
      };
    });
    return tablePriceIsNotComplete;
  }

  $scope.isAnyLengthOfPackSelected = function() {
    var anyLengthOfPackSelected = false;
    angular.forEach($scope.availablePacks, function(value, key){
      if (value.checked){
        anyLengthOfPackSelected = true;
      }
    })
    return anyLengthOfPackSelected;
  }

  $scope.isAnyRoomTypeSelected = function() {
    var anyRoomTypeSelected = false;
    angular.forEach($scope.roomTypesById, function(value, key){
      if ((value.checked) && (!$scope.selectedTablePriceDoesNotContainsRoomType(value.id))){
        anyRoomTypeSelected = true;
      }
    })
    return anyRoomTypeSelected;
  };

  $scope.isAnyRoomTypeSelectedForOfferRemoval = function() {
    var anyRoomTypeSelected = false;
    angular.forEach($scope.roomTypesById, function(value, key){
      if (value.checked){
        anyRoomTypeSelected = true;
      }
    })
    return anyRoomTypeSelected;
  };

  $scope.isAnyRoomSelected = function() {
    var anyRoomSelected = false;
    angular.forEach($scope.roomTypesById, function(value, key){
      if (value.checked){
        angular.forEach(value.active_rooms, function(value, key){
          if (value.checked){
            anyRoomSelected = true;
          }
        })
      }
    })
    return anyRoomSelected;
  };

  $scope.isFormDatesValids = function(){
    var initialDateStr = $(".js-initial-date").val();
    var finalDateStr = $(".js-final-date").val();
    var actualDate = new Date();
    actualDate.setHours(0,0,0,0);
    if(datePattern.test(initialDateStr) && datePattern.test(finalDateStr)){
      initialDate = new Date(initialDateStr.split('/')[2], (initialDateStr.split('/')[1] - 1), initialDateStr.split('/')[0]);
      finalDate = new Date(finalDateStr.split('/')[2], (finalDateStr.split('/')[1] - 1), finalDateStr.split('/')[0]);
      return ((initialDate >= actualDate) && (initialDate <= finalDate));
    }
    else{
      return false;
    }
  };

  $scope.isFormComplete = function(){
    $scope.formChanged();
    return $scope.isAnyRoomTypeSelected() && $scope.isAnyRoomSelected() && $scope.isFormDatesValids() && $scope.isAnyCheckinTimeChecked && $scope.isAnyDayOfWeekChecked && $scope.isAnyLengthOfPackSelected();
  };

  $scope.isFormCompleteForRemoval = function(){
    $scope.formChanged();
    // return $scope.isAnyRoomTypeSelected() && $scope.isAnyRoomSelected() && $scope.isFormDatesValids() && $scope.isAnyDayOfWeekChecked && $scope.isAnyLengthOfPackSelected();
    return $scope.isAnyRoomTypeSelectedForOfferRemoval() && $scope.isFormDatesValids() && $scope.isAnyDayOfWeekChecked && $scope.isAnyLengthOfPackSelected();
  };

  $scope.formChanged = function() {
    $scope.isAnyDayOfWeekChecked = ($("form input.js-days-of-week:checkbox:checked").length > 0);
    $scope.isAnyCheckinTimeChecked = ($("form input.js-checkin-times:checkbox:checked").length > 0);
  };

  $scope.dateInputChanged = function() {
    if ($scope.isFormDatesValids()) {
      getOfferCountPerRoom();
    }
  };

  $scope.zeroOffersForRoomType = function(roomType) {
    for (var i = roomType.active_rooms.length - 1; i >= 0; i--) {
      if ($scope.offersCountPerRoom[roomType.active_rooms[i].id]) {
        return false;
      }
    };
    return true;
  };

  $scope.roomsWithOffersNumberPerRoomType = function(roomType) {
    var numbers = [];
    for (var i = roomType.active_rooms.length - 1; i >= 0; i--) {
      if ($scope.offersCountPerRoom[roomType.active_rooms[i].id]) {
        numbers.push(roomType.active_rooms[i].number);
      }
    };
    return numbers;
  };

  $scope.selectedAllHoursByPackPeriod = {
    '3 horas': true,
    '6 horas': true,
    '9 horas': true,
    '12 horas': true
  };

}]);
