HotelQuandoApp.controller('HotelEventRoomOffersFormCtrl', ['$scope', '$window', '$http', function($scope, $window, $http) {
  // we need this because ng-change on Date fields is triggered multiple times
  // per change. With this flagh we allow only 1 request
  var gettingOfferCountPerRoom = false;

  function getOfferCountPerRoom() {
    if (!gettingOfferCountPerRoom) {
      gettingOfferCountPerRoom = true;

      var initialDateStr = $(".js-initial-date").val();
      var finalDateStr = $(".js-final-date").val();
      var params = {
        by_checkin_range: {
          initial_date: initialDateStr,
          final_date: finalDateStr
        }
      };

      $http({
        url: Routes.count_per_room_hotel_event_room_offers_path(params),
        method: "get"
      }).success(function(data, status, headers, config) {
        $scope.offersCountPerRoom = data;
        $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
      }).error(function(data, status, headers, config) {
        $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
        $window.alert('Desculpe. Não conseguimos contabilizar as ofertas por sala.');
      });
    }
  }

  $scope.availablePacks = [
    { period: '2 horas', period_without_unit: '2', checked: true},
    { period: '4 horas', period_without_unit: '4', checked: true},
    { period: '8 horas', period_without_unit: '8', checked: true}
  ];

  $scope.priceTablesById = yojs.get('priceTablesByIdJson');
  $scope.eventRoomsById = yojs.get('eventRoomsByIdJson');
  $scope.isAnyDayOfWeekChecked = false;
  $scope.isAnyCheckinTimeChecked = false;
  $scope.intialDate = '';
  $scope.finalDate = '';

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  var firstIteration = true
  angular.forEach($scope.priceTablesById, function(value, key){
    if (firstIteration){
      $scope.selectedPriceTable = value;
      firstIteration = false;
    }
  })

  if (yojs.isDefined('offerCreation')){
    angular.forEach($scope.eventRoomsById, function(value, key){
      value.checked = true;
    });
  }

  $scope.selectedTablePriceDoesNotContainsEventRoom = function(eventRoomId) {
    return $scope.selectedPriceTable.eventRoomsId.indexOf(parseInt(eventRoomId)) == -1;
  };

  $scope.selectedTablePriceIsNotComplete = function() {
    var tablePriceIsNotComplete = false;
    angular.forEach($scope.eventRoomsById, function(value, key){
      if ($scope.selectedTablePriceDoesNotContainsEventRoom(value.id)){
        tablePriceIsNotComplete = true;
      };
    });
    return tablePriceIsNotComplete;
  }

  $scope.isAnyLengthOfPackSelected = function() {
    var anyLengthOfPackSelected = false;
    angular.forEach($scope.availablePacks, function(value, key){
      if (value.checked){
        anyLengthOfPackSelected = true;
      }
    })
    return anyLengthOfPackSelected;
  }

  $scope.isAnyEventRoomSelected = function() {
    var anyEventRoomSelected = false;
    angular.forEach($scope.eventRoomsById, function(value, key){
      if ((value.checked) && (!$scope.selectedTablePriceDoesNotContainsEventRoom(value.id))){
        anyEventRoomSelected = true;
      }
    })
    return anyEventRoomSelected;
  };

  $scope.isAnyEventRoomSelectedForOfferRemoval = function() {
    var anyEventRoomSelected = false;
    angular.forEach($scope.eventRoomsById, function(value, key){
      if (value.checked){
        anyEventRoomSelected = true;
      }
    })
    return anyEventRoomSelected;
  };

  $scope.isFormDatesValids = function(){
    var initialDateStr = $(".js-initial-date").val();
    var finalDateStr = $(".js-final-date").val();
    var actualDate = new Date();
    actualDate.setHours(0,0,0,0);
    if(datePattern.test(initialDateStr) && datePattern.test(finalDateStr)){
      initialDate = new Date(initialDateStr.split('/')[2], (initialDateStr.split('/')[1] - 1), initialDateStr.split('/')[0]);
      finalDate = new Date(finalDateStr.split('/')[2], (finalDateStr.split('/')[1] - 1), finalDateStr.split('/')[0]);
      return ((initialDate >= actualDate) && (initialDate <= finalDate));
    }
    else{
      return false;
    }
  };

  $scope.isFormComplete = function(){
    $scope.formChanged();
    return $scope.isAnyEventRoomSelected() && $scope.isFormDatesValids() && $scope.isAnyCheckinTimeChecked && $scope.isAnyDayOfWeekChecked && $scope.isAnyLengthOfPackSelected();
  };

  $scope.isFormCompleteForRemoval = function(){
    $scope.formChanged();
    return $scope.isAnyEventRoomSelectedForOfferRemoval() && $scope.isFormDatesValids() && $scope.isAnyDayOfWeekChecked && $scope.isAnyLengthOfPackSelected();
  };

  $scope.formChanged = function() {
    $scope.isAnyDayOfWeekChecked = ($("form input.js-days-of-week:checkbox:checked").length > 0);
    $scope.isAnyCheckinTimeChecked = ($("form input.js-checkin-times:checkbox:checked").length > 0);
  };

  $scope.dateInputChanged = function() {
    if ($scope.isFormDatesValids()) {
      getOfferCountPerRoom();
    }
  };


  $scope.zeroOffersForEventRoom = function(eventRoom) {
    if ($scope.offersCountPerRoom[eventRoom.id]) {
      return false;
    }
    return true;
  };

  $scope.selectedAllHoursByPackPeriod = {
    '2 horas': true,
    '4 horas': true,
    '8 horas': true
  };

}]);
