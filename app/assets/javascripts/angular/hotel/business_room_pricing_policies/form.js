HotelQuandoApp.controller('HotelBusinessRoomPricingPoliciesFormCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.percentageForNormalProfile = {
    1: parseFloat(yojs.get('default1HourPackPercentage')),
    2: parseFloat(yojs.get('default2HoursPackPercentage')),
    3: parseFloat(yojs.get('default3HoursPackPercentage')),
    4: parseFloat(yojs.get('default4HoursPackPercentage')),
    5: parseFloat(yojs.get('default5HoursPackPercentage')),
    6: parseFloat(yojs.get('default6HoursPackPercentage')),
    7: parseFloat(yojs.get('default7HoursPackPercentage')),
    8: parseFloat(yojs.get('default8HoursPackPercentage')),
    24: 100
  };

  $scope.pricesPerRoomType = {};

  $scope.basePricesPerRoomType = {};

  $scope.selectedPercentages = angular.extend({}, $scope.percentageForNormalProfile);

  $scope.init = function(businessRoomPrices, businessRoomPricingPolicy) {
    $scope.selectedProfile = businessRoomPricingPolicy.percentage_profile_name;
    $scope.businessRoomPrices = businessRoomPrices;

    angular.forEach($scope.percentageForNormalProfile, function(value, pack) {
      $scope.selectedPercentages[pack] = parseFloat(businessRoomPricingPolicy['percentage_for_' + pack + 'h']).toFixed(1);
    });

    angular.forEach($scope.businessRoomPrices, function(brPrice) {
      // we receive always a float value. so we convert to BR with two decimals
      brPrice["base_price"] = brPrice["pack_price_24h"]

      // we need to use to fixed to work with maskmoney bug
      // console.log(rtPricingPolicy["pack_price_" + pack + "h"]);
      // rtPricingPolicy["pack_price_" + pack + "h"] = parseFloat(rtPricingPolicy["pack_price_" + pack + "h"]).toFixed(2);
      // rtPricingPolicy["extra_price_per_person_for_pack_price_" + pack + "h"] = parseFloat(rtPricingPolicy["extra_price_per_person_for_pack_price_" + pack + "h"]).toFixed(2);;
    });

  };

  $scope.chooseProfile = function(profileName) {
    var packSizeIterator;
    $scope.selectedProfile = profileName;
    if ($scope.selectedProfile == 'normal') {

      $scope.selectedPercentages = angular.extend({}, $scope.percentageForNormalProfile);

    } else if ($scope.selectedProfile == 'aggressive') {
      angular.forEach($scope.percentageForNormalProfile, function(value, pack) {
        $scope.selectedPercentages[pack] = value - 5;
      });
    } else if ($scope.selectedProfile == 'conservative') {
      angular.forEach($scope.percentageForNormalProfile, function(value, pack) {
        $scope.selectedPercentages[pack] = value + 5;
      });
    }
    $scope.updateAllTablePrice();
  };

  $scope.updateAllTablePrice = function() {
    for(var i = 0; i < $scope.businessRoomPrices.length; i++) {
      $scope.updateTablePrice(i);
    }
  };

  $scope.updateTablePrice = function(brPriceIndex) {
    angular.forEach($scope.selectedPercentages, function(percentage, pack) {
      var brPrice = $scope.businessRoomPrices[brPriceIndex];
      var packBasePrice = ((brPrice["base_price"] * percentage)/100.0).toFixed(2);
      brPrice["pack_price_" + pack + "h"] = packBasePrice;
    });
  };
}]);
