HotelQuandoApp.controller('HotelPricingPoliciesFormCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.percentageForNormalProfile = {
    3: parseInt(yojs.get('default3HoursPackPercentage')),
    6: parseInt(yojs.get('default6HoursPackPercentage')),
    9: parseInt(yojs.get('default9HoursPackPercentage')),
    12: parseInt(yojs.get('default12HoursPackPercentage')),
    24: 100
  };

  $scope.percentageForCustomProfile = {
    3: parseInt(yojs.get('custom3HoursPackPercentage')),
    6: parseInt(yojs.get('custom6HoursPackPercentage')),
    9: parseInt(yojs.get('custom9HoursPackPercentage')),
    12: parseInt(yojs.get('custom12HoursPackPercentage')),
    24: 100
  };

  $scope.pricesPerRoomType = {};
  $scope.extraPricesPerRoomType = {};

  $scope.basePricesPerRoomType = {};
  $scope.extraPricePerRoomType = {};

  $scope.selectedPercentages = angular.extend({}, $scope.percentageForNormalProfile);

  $scope.init = function(roomTypesPricingPolices, pricingPolicy) {
    $scope.selectedProfile = pricingPolicy.percentage_profile_name;
    $scope.roomTypesPricingPolices = roomTypesPricingPolices;

    angular.forEach($scope.percentageForNormalProfile, function(value, pack) {
      $scope.selectedPercentages[pack] = parseInt(pricingPolicy['percentage_for_' + pack + 'h']);
    });

    angular.forEach($scope.roomTypesPricingPolices, function(rtPricingPolicy) {
      // we receive always a float value. so we convert to BR with two decimals
      rtPricingPolicy["base_price"] = rtPricingPolicy["pack_price_24h"]
      rtPricingPolicy["base_extra_price"] = rtPricingPolicy["extra_price_per_person_for_pack_price_24h"]

      // we need to use to fixed to work with maskmoney bug
      // console.log(rtPricingPolicy["pack_price_" + pack + "h"]);
      // rtPricingPolicy["pack_price_" + pack + "h"] = parseFloat(rtPricingPolicy["pack_price_" + pack + "h"]).toFixed(2);
      // rtPricingPolicy["extra_price_per_person_for_pack_price_" + pack + "h"] = parseFloat(rtPricingPolicy["extra_price_per_person_for_pack_price_" + pack + "h"]).toFixed(2);;
    });

  };

  $scope.chooseProfile = function(profileName) {
    var packSizeIterator;
    $scope.selectedProfile = profileName;
    if ($scope.selectedProfile == 'normal') {

      $scope.selectedPercentages = angular.extend({}, $scope.percentageForNormalProfile);

    } else if ($scope.selectedProfile == 'aggressive') {
      angular.forEach($scope.percentageForNormalProfile, function(value, pack) {
        $scope.selectedPercentages[pack] = value - 5;
      });
    } else if ($scope.selectedProfile == 'conservative') {
      angular.forEach($scope.percentageForNormalProfile, function(value, pack) {
        $scope.selectedPercentages[pack] = value + 5;
      });
    } else if ($scope.selectedProfile == 'custom') {
      $scope.selectedPercentages = angular.extend({}, $scope.percentageForCustomProfile);
    }
    $scope.updateAllTablePrice();
  };

  $scope.updateAllTablePrice = function() {
    for(var i = 0; i < $scope.roomTypesPricingPolices.length; i++) {
      $scope.updateTablePrice(i);
    }
  };

  $scope.updateTablePrice = function(rtPpIndex) {
    angular.forEach($scope.selectedPercentages, function(percentage, pack) {
      var rtPricingPolicy = $scope.roomTypesPricingPolices[rtPpIndex];
      var packBasePrice = ((rtPricingPolicy["base_price"] * percentage)/100.0).toFixed(2);
      var packExtraPrice = ((rtPricingPolicy["base_extra_price"] * percentage)/100.0).toFixed(2);
      rtPricingPolicy["pack_price_" + pack + "h"] = packBasePrice;
      rtPricingPolicy["extra_price_per_person_for_pack_price_" + pack + "h"] = packExtraPrice;
    });
  };
}]);
