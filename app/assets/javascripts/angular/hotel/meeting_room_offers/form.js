HotelQuandoApp.controller('HotelMeetingRoomOffersFormCtrl', ['$scope', '$window', '$http', function($scope, $window, $http) {
  // we need this because ng-change on Date fields is triggered multiple times
  // per change. With this flagh we allow only 1 request
  var gettingOfferCountPerRoom = false;

  function getOfferCountPerRoom() {
    if (!gettingOfferCountPerRoom) {
      gettingOfferCountPerRoom = true;

      var initialDateStr = $(".js-initial-date").val();
      var finalDateStr = $(".js-final-date").val();
      var params = {
        by_checkin_range: {
          initial_date: initialDateStr,
          final_date: finalDateStr
        }
      };

      $http({
        url: Routes.count_per_room_hotel_meeting_room_offers_path(params),
        method: "get"
      }).success(function(data, status, headers, config) {
        $scope.offersCountPerRoom = data;
        $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
      }).error(function(data, status, headers, config) {
        $window.setTimeout(function() { gettingOfferCountPerRoom = false; }, 3000 );
        $window.alert('Desculpe. Não conseguimos contabilizar as ofertas por sala.');
      });
    }
  }

  $scope.availablePacks = [
    { period: '1 horas', period_without_unit: '1', checked: true},
    { period: '2 horas', period_without_unit: '2', checked: true},
    { period: '3 horas', period_without_unit: '3', checked: true},
    { period: '4 horas', period_without_unit: '4', checked: true},
    { period: '5 horas', period_without_unit: '5', checked: true},
    { period: '6 horas', period_without_unit: '6', checked: true},
    { period: '7 horas', period_without_unit: '7', checked: true},
    { period: '8 horas', period_without_unit: '8', checked: true}
  ];

  $scope.priceTablesById = yojs.get('priceTablesByIdJson');
  $scope.meetingRoomsById = yojs.get('meetingRoomsByIdJson');
  $scope.isAnyDayOfWeekChecked = false;
  $scope.isAnyCheckinTimeChecked = false;
  $scope.intialDate = '';
  $scope.finalDate = '';

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  var firstIteration = true
  angular.forEach($scope.priceTablesById, function(value, key){
    if (firstIteration){
      $scope.selectedPriceTable = value;
      firstIteration = false;
    }
  })

  if (yojs.isDefined('offerCreation')){
    angular.forEach($scope.meetingRoomsById, function(value, key){
      value.checked = true;
    });
  }

  $scope.selectedTablePriceDoesNotContainsMeetingRoom = function(meetingRoomId) {
    return $scope.selectedPriceTable.meetingRoomsId.indexOf(parseInt(meetingRoomId)) == -1;
  };

  $scope.selectedTablePriceIsNotComplete = function() {
    var tablePriceIsNotComplete = false;
    angular.forEach($scope.meetingRoomsById, function(value, key){
      if ($scope.selectedTablePriceDoesNotContainsMeetingRoom(value.id)){
        tablePriceIsNotComplete = true;
      };
    });
    return tablePriceIsNotComplete;
  }

  $scope.isAnyLengthOfPackSelected = function() {
    var anyLengthOfPackSelected = false;
    angular.forEach($scope.availablePacks, function(value, key){
      if (value.checked){
        anyLengthOfPackSelected = true;
      }
    })
    return anyLengthOfPackSelected;
  }

  $scope.isAnyMeetingRoomSelected = function() {
    var anyMeetingRoomSelected = false;
    angular.forEach($scope.meetingRoomsById, function(value, key){
      if ((value.checked) && (!$scope.selectedTablePriceDoesNotContainsMeetingRoom(value.id))){
        anyMeetingRoomSelected = true;
      }
    })
    return anyMeetingRoomSelected;
  };

  $scope.isAnyMeetingRoomSelectedForOfferRemoval = function() {
    var anyMeetingRoomSelected = false;
    angular.forEach($scope.meetingRoomsById, function(value, key){
      if (value.checked){
        anyMeetingRoomSelected = true;
      }
    })
    return anyMeetingRoomSelected;
  };

  $scope.isFormDatesValids = function(){
    var initialDateStr = $(".js-initial-date").val();
    var finalDateStr = $(".js-final-date").val();
    var actualDate = new Date();
    actualDate.setHours(0,0,0,0);
    if(datePattern.test(initialDateStr) && datePattern.test(finalDateStr)){
      initialDate = new Date(initialDateStr.split('/')[2], (initialDateStr.split('/')[1] - 1), initialDateStr.split('/')[0]);
      finalDate = new Date(finalDateStr.split('/')[2], (finalDateStr.split('/')[1] - 1), finalDateStr.split('/')[0]);
      return ((initialDate >= actualDate) && (initialDate <= finalDate));
    }
    else{
      return false;
    }
  };

  $scope.isFormComplete = function(){
    $scope.formChanged();
    return $scope.isAnyMeetingRoomSelected() && $scope.isFormDatesValids() && $scope.isAnyCheckinTimeChecked && $scope.isAnyDayOfWeekChecked && $scope.isAnyLengthOfPackSelected();
  };

  $scope.isFormCompleteForRemoval = function(){
    $scope.formChanged();
    return $scope.isAnyMeetingRoomSelectedForOfferRemoval() && $scope.isFormDatesValids() && $scope.isAnyDayOfWeekChecked && $scope.isAnyLengthOfPackSelected();
  };

  $scope.formChanged = function() {
    $scope.isAnyDayOfWeekChecked = ($("form input.js-days-of-week:checkbox:checked").length > 0);
    $scope.isAnyCheckinTimeChecked = ($("form input.js-checkin-times:checkbox:checked").length > 0);
  };

  $scope.dateInputChanged = function() {
    if ($scope.isFormDatesValids()) {
      getOfferCountPerRoom();
    }
  };


  $scope.zeroOffersForMeetingRoom = function(meetingRoom) {
    if ($scope.offersCountPerRoom[meetingRoom.id]) {
      return false;
    }
    return true;
  };

  $scope.selectedAllHoursByPackPeriod = {
    '1 horas': true,
    '2 horas': true,
    '3 horas': true,
    '4 horas': true,
    '5 horas': true,
    '6 horas': true,
    '7 horas': true,
    '8 horas': true
  };

}]);
