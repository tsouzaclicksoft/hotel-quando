HotelQuandoApp.controller('CountryAndStateSelectCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.stateShouldBeVisible = false;
  $scope.countryChanged = function(){
    if(($scope.country == 'BR')){
      $scope.stateShouldBeVisible = true;
    }
    else{
      $scope.state = '';
      $scope.stateShouldBeVisible = false;
    }
  }
}])
