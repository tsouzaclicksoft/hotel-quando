HotelQuandoApp.controller('PhotosCtrl', ['$scope', '$window', function($scope, $window) {

  function resetPhotoToBeAdded() {
    $scope.photoToBeAdded = {
      title_en: '',
      title_pt_br: '',
      title_es: '',
      direct_image_url: '',
      destroy: false
    };
  }

  resetPhotoToBeAdded();

  $scope.add = function(hideModal) {
    if ($scope.photoToBeAdded.direct_image_url) {
      $scope.photos.push($scope.photoToBeAdded);
      resetPhotoToBeAdded();
      if (hideModal) {
        $(".js-modal-add-photo").modal('hide')
      }
      $('.js-s3-uploader .js_image_preview').attr('src', '');
      $('.js-s3-uploader .js_file_name').text("");
    } else if($("body").hasClass("browserIE9") && $(".js-direct-upload-url").val() != "" && $(".js_image_preview").attr('src') != ""){
      $scope.photoToBeAdded.direct_image_url = $(".js-direct-upload-url").val();
      $scope.photos.push($scope.photoToBeAdded);
      resetPhotoToBeAdded();
      if (hideModal) {
        $(".js-modal-add-photo").modal('hide')
      }
      $('.js-s3-uploader .js_image_preview').attr('src', '');
      $('.js-s3-uploader .js_file_name').text("");
    } else {
      $window.alert("Por favor envie uma imagem.");
    }
  };

  $scope.remove = function(index) {
    $scope.photos[index].destroy = true;
  };

  $scope.removeUndo = function(index) {
    $scope.photos[index].destroy = false;
  };

  $scope.setPhotoAsMain = function(index) {
    for (var i = $scope.photos.length - 1; i >= 0; i--) {
      $scope.photos[i].is_main = false;
    };
    $scope.photos[index].is_main = true;
  }

  $scope.setPhotoAsBedroom = function(index) {
    for (var i = $scope.photos.length - 1; i >= 0; i--) {
      if($scope.photos[i].title_pt_br == "quartos"){
        $scope.photos[i].title_pt_br = '';
      }
    };
    $scope.photos[index].title_pt_br = 'quartos';
  }

  $scope.setPhotoAsMeetingroom = function(index) {
    for (var i = $scope.photos.length - 1; i >= 0; i--) {
      if($scope.photos[i].title_pt_br == "salas"){
        $scope.photos[i].title_pt_br = '';
      }
    };
    $scope.photos[index].title_pt_br = 'salas';
  }
}]);
