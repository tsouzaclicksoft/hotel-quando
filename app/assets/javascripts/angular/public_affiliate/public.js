HotelQuandoApp.factory('publicAffiliateSearchFactory', ['$http', function ($http){
  var tzArray = {};
    tzArray.getTZ = function(lat,lng,tmstp){            
        return $http({
                method : "GET",
                url : "/affiliate/timezone?location="+lat+","+lng+"&tmstp="+tmstp
              }).then(function mySucces(response) {
                //(dstOffset + rawOffset) and timeZoneId
                return(response.data.msg);
              }, function myError(response) {
                console.log("Couldn't get timezone, please choose the search address again.")
                return('');
              });

    }
    return tzArray;
}]);

HotelQuandoApp.controller('PublicAffiliateSearchCtrl', ['$scope', '$window', 'publicAffiliateSearchFactory', function($scope, $window, publicAffiliateSearchFactory) {
  $scope.checkinHour = 00;
  $scope.checkoutHour = 03;
  $scope.checkedCheckin = false;
  $scope.numberOfPeople = 1;
  $scope.address = '';
  $scope.timezoneSearched = '';
  $scope.timezone = {
    timezone_value: 0, 
    timezone_id: '', 
    timezone_name: ''
  };

  $scope.latitude = '';

  $scope.longitude = '';

  $scope.isSearchDateValid = false;

  $scope.isLatitudeAndLongitudeValid = false;

  //$scope.selectedPackSize = 3;
   
  //var tabNumber = yojs.get('tabNumber');

  //$scope.selectedTabRoom = parseInt(tabNumber);


  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  angular.element(document).ready(function () {
    if ($scope.latitude != "") 
      $scope.timezone = $scope.getTimezoneFromGoogle($scope.latitude, $scope.longitude);
  });


  $scope.changeBackgroundAndClearForm = function(){
    var wrapper = $('.search-hotels-wrapper')
    //tab 1 = bedroom
    //tab 2 = meeting room
    //tab 3 = event room
    // code updated, now only have 1 tab - room serach
    //$scope.selectedPackSize = 3;
    $scope.numberOfPeople = 1;
    /*
    if ($scope.selectedTabRoom == 1){
      //wrapper.css('background-image', '').attr('class','search-hotels-wrapper rooms-background');
      $scope.selectedPackSize = 3;
      $scope.numberOfPeople = 1;
    }
    if ($scope.selectedTabRoom == 2){
      //wrapper.css('background-image', '').attr('class','search-hotels-wrapper meeting-rooms-background');
      $scope.selectedPackSize = 1;
      $scope.numberOfPeople = 1;
    }
    */
    /*if ($scope.selectedTabRoom == 3){
      wrapper.css('background-image', '').attr('class','search-hotels-wrapper event-rooms-background');
      $scope.selectedPackSize = 2;
      $scope.numberOfPeople = 20;
    }*/
    $scope.checkinHour = 0;
    $scope.address = '';
    $scope.latitude = '';
    $scope.longitude = '';
    $scope.isSearchDateValid = false;
    $scope.isLatitudeAndLongitudeValid = false;
    $scope.checkinDate = '';
    $('.js-autocomplete').onload = initialize('js-autocomplete');
  } 

  /*
  $scope.changeTab = function(type){
    $scope.selectedTabRoom = type
    $scope.changeBackgroundAndClearForm()
  }
  */


  $scope.setSearchDate = function(){
    var parsedDate;

    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);
      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/' + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/' + parsedDate.getFullYear();
    }
    else parsedDate = $scope.checkinDate;

    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = dateStr.split('/');
      dateToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
      dateInt = dateToSearch.getTime() - minimumDateToSearch;
      // Check timezone (requesting our server) only if difference between +/- 24h
      if(typeof $scope.timezone != 'undefined') 
        if ( ($scope.timezone.timezone_name != '') && (dateInt > -82799000) && (dateInt < 82801000) && ($scope.latitude != '') )
          $scope.getTimezoneFromGoogle($scope.latitude, $scope.longitude);
    }
    $scope.setSearchDateValidity();
  }
  $scope.setSearchDateValidity = function(){
    if (typeof $scope.checkinDate == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.checkinDate);

      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');      
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour); // subtract 1 from month because javascript start month in 0 = jan
      } else {
        parsedDate = $scope.checkinDate;
        var date                = parsedDate.split('/');      
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
      }
      $scope.parsedSelectedCheckinDate = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;
      
      // Consider timezone when search datetime is between +/- 23h from local time 
      if (typeof $scope.timezone != 'undefined') 
        if ( ($scope.timezone.timezone_name != '') && (dateToInt > -82799000) && (dateToInt < 82801000) && ($scope.latitude != '') )
          minimumDateToSearch += $scope.timezone.timezone_value + (new Date().getTimezoneOffset()*60000);
    }

    if(datePattern.test(parsedDate)){
      if(dateToSearch >= minimumDateToSearch) 
        $scope.isSearchDateValid = true
      else $scope.isSearchDateValid = false;  
    }
    else $scope.isSearchDateValid = false;
  };

  $scope.setLatitudeAndLongitudeValidity = function(){
    // Check if latitude and longitude are numeric
    var latitudeValid = ((!isNaN(parseFloat($scope.latitude))) && (isFinite($scope.latitude)))
    var longitudeValid = ((!isNaN(parseFloat($scope.longitude))) && (isFinite($scope.longitude)))
    $scope.isLatitudeAndLongitudeValid = (latitudeValid && longitudeValid)
  }

  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    return ($scope.isSearchDateValid && $scope.isLatitudeAndLongitudeValid);
  }

  $scope.resonsForDisabledSubmit = function(){
    if ((!$scope.isSearchDateValid) && (!$scope.isLatitudeAndLongitudeValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li><li>' + yojs.get('error_message_for_address') + '</li></ul>';
    }
    else {
      if(!($scope.isSearchDateValid)){
        return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
      }
      else {
        return '<ul><li>' + yojs.get('error_message_for_address') + '</li></ul>';
      }
    }
  }

  $scope.getTimezoneFromGoogle = function(lat, lng) {
    if (lat != "") {
      if ($scope.checkinDate == "")
        var tmstp = Math.round(((new Date()).getTime()/1000))
      else {
        var d = $scope.checkinDate.split("/");
        // Get user timezone - local
        var splitDateTimeNow = new Date().toString().split(" ");
        timeZoneFormatted = splitDateTimeNow[splitDateTimeNow.length - 2] + " " + splitDateTimeNow[splitDateTimeNow.length - 1];
        // Format datetime with timezone to send to server
        if (yojs.get("locale") == "en") 
              var tmstp = new Date(d[2], ( d[0]-1 ), d[1], ($scope.checkinHour).toString(), "0").getTime() / 1000
        else  var tmstp = new Date(d[2], ( d[1]-1 ), d[0], $scope.checkinHour.toString(), "0").getTime() / 1000;
      }
      publicAffiliateSearchFactory.getTZ($scope.latitude, $scope.longitude, tmstp)
        .then(function(arrItems){
          $scope.timezone = arrItems;
          $scope.timezoneSearched = arrItems.timezone_id;
      });   
    }
  }

  var autocomplete;

  var initialize = function() {
    autocomplete = new $window.google.maps.places.Autocomplete( (document.getElementById('js-autocomplete')) );
    $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
      $scope.setLatitudeAndLongitude();
      $scope.$digest();
    });
  }

  $scope.setLatitudeAndLongitude = function() {
    try{
      var place = autocomplete.getPlace();
      $scope.latitude = place.geometry.location.lat();
      $scope.longitude = place.geometry.location.lng();
      //$scope.cityName = place.address_components[2].short_name;
      //$scope.stateName = place.address_components[3].short_name;
      $scope.address = place.formatted_address
      $scope.setLatitudeAndLongitudeValidity();

      if ($scope.isLatitudeAndLongitudeValid) 
        $scope.timezone = $scope.getTimezoneFromGoogle(place.geometry.location.lat(), place.geometry.location.lng())
      else $scope.timezone = {};

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'locality') {
          var val = place.address_components[i].short_name;
          $scope.cityName = val;
        } else if (addressType == 'country') {
          var val = place.address_components[i].short_name;
          $scope.stateName = val;
        }
      }
    }
    catch(err){
      $scope.isLatitudeAndLongitudeValid = false
    }
  }

  $('.js-autocomplete').onload = initialize('js-autocomplete');

}]);