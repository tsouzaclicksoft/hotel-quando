HotelQuandoApp.controller('PublicEpaycoPaymentCtrl', ['$scope',  '$window', function($scope, $window) {
  var blankStrPattern = /^\s*$/;
  var simpleMailPattern = /.+@.+\..+/;
  $scope.cardnumber = yojs.get('cardNumber');
  $scope.month = yojs.get('month');
  $scope.year = yojs.get('year');
  $scope.cvc = yojs.get('cvc');
  $scope.name = yojs.get('name');
  $scope.email = yojs.get('email');
  $scope.numberdoc = yojs.get('numberDoc');
  $scope.phone = yojs.get('phone');
  $scope.checkedtermsofuse = yojs.get('checkedTermsOfUse');

  $scope.initialYear = new Date().getFullYear();

$scope.messageForDisabledSubmit = function(){
    if ($scope.isFormValid()){
      return ''
    }
    else{
      if (yojs.get('locale') == 'pt-BR'){
        return_message = 'O botão Finalizar Reserva foi desabilitado, pois: <ul>';
        if (!$scope.isCardNumberValid()){
          return_message += '<li>É necessário um número válido de cartão</li>';
        }
        if (!$scope.isMonthValid()){
          return_message += '<li>Mês não pode ficar em branco</li>';
        }
        if (!$scope.isYearValid()){
          return_message += '<li>Ano não pode ficar em branco</li>';
        }
        if (!$scope.isCvcValid()){
          return_message += '<li>Insira um código CVC válido</li>';
        }
        if (!$scope.isEmailValid()){
          return_message += '<li>Email não pode ficar em branco</li>';
        }
        if (!$scope.isNameValid()){
          return_message += '<li>Nome e sobrenome não pode ficar em branco</li>';
        }
        if (!$scope.isNumerDocValid()){
          return_message += '<li>Documento não pode ficar em branco</li>';
        }
        if(!$scope.isPhoneValid()){
          return_message += '<li>Tefone não pode ficar em branco</li>';
        }
        if(!$scope.isTermsOfUseValid()){
          return_message += '<li>Aceite os termos de uso</li>';
        }
        return_message += '</ul>'
      }else if (yojs.get('locale') == 'es'){
        return_message = 'El botón Finalizar reserva se ha desactivado, porque: <ul>';
        if (!$scope.isCardNumberValid()){
          return_message += '<li>Se necesita un número de tarjeta válida</li>';
        }
        if (!$scope.isMonthValid()){
          return_message += '<li>Mes no puede estar en blanco</li>';
        }
        if (!$scope.isYearValid()){
          return_message += '<li>Año no puede estar en blanco</li>';
        }
        if (!$scope.isCvcValid()){
          return_message += '<li>introduzca un código CVC válido</li>';
        }
        if (!$scope.isEmailValid()){
          return_message += '<li>Email no puede estar en blanco</li>';
        }
        if (!$scope.isNameValid()){
          return_message += '<li>Nombre y apellidos no puede estar en blanco</li>';
        }
        if (!$scope.isNumerDocValid()){
          return_message += '<li>El documento no puede estar en blanco</li>';
        }
        if(!$scope.isPhoneValid()){
          return_message += '<li>Telefóno no puede estar en blanco</li>';
        }
        if(!$scope.isTermsOfUseValid()){
          return_message += '<li>Aceptar los términos de uso</li>';
        }
        return_message += '</ul>'
      } else {
        return_message = 'The Finish Reservation button was disabled for the following reasons: <ul>';
        if (!$scope.isCardNumberValid()){
          return_message += '<li>Valid card number is required</li>';
        }
        if (!$scope.isMonthValid()){
          return_message += "<li>it is necessary that the month is filled</li>";
        }
        if (!$scope.isYearValid()){
          return_message += '<li>it is necessary that the year is filled</li>';
        }
        if (!$scope.isCvcValid()){
          return_message += '<li>Valid CVC is required</li>';
        }
        if (!$scope.isEmailValid()){
          return_message += '<li>it is necessary that the email is filled</li>';
        }
        if (!$scope.isNameValid()){
          return_message += '<li>it is necessary that the name and lastname is filled</li>';
        }
        if (!$scope.isNumerDocValid()){
          return_message += '<li>it is necessary that the document is filled</li>';
        }
        if(!$scope.isPhoneValid()){
          return_message += '<li>it is necessary that the phone is filled</li>';
        }
        if(!$scope.isTermsOfUseValid()){
          return_message += '<li>Accept the terms of use</li>';
        }
        return_message += '</ul>'
      }

      return return_message;
    }
  }

  $scope.isCardNumberValid = function(){
    return (!blankStrPattern.test($scope.cardnumber));
  }

  $scope.isMonthValid = function(){
    return (!blankStrPattern.test($scope.month));
  }

  $scope.isYearValid = function(){
    return (!blankStrPattern.test($scope.year));
  }

  $scope.isCvcValid = function(){
    return ($scope.cvc.length > 2 && $scope.cvc.length < 5);
  }

  $scope.isEmailValid = function(){
    return (simpleMailPattern.test($scope.email));
  }

  $scope.isNameValid = function(){
    return $scope.name.split(" ").length >= 2;
  }

  $scope.isNumerDocValid = function(){
    return (!blankStrPattern.test($scope.numberdoc));
  }

  $scope.isTermsOfUseValid = function(){
    return $scope.checkedtermsofuse;
  }

  $scope.isPhoneValid = function(){
    return (!blankStrPattern.test($scope.phone));
  }

  $scope.isFormValid = function(){
    return ($scope.isCardNumberValid() && $scope.isMonthValid() && $scope.isYearValid() && $scope.isCvcValid() && $scope.isEmailValid() && $scope.isNameValid() && $scope.isNumerDocValid() && $scope.isTermsOfUseValid() && $scope.isPhoneValid());
  }

  $scope.submitButtonExtraClass = function(){
    return $scope.isFormValid() ? '' : 'submit-disabled'
  }

  $('#new_payment').submit(function(event) {
    event.preventDefault();

    var $form = $(this);
    $form.find("button").prop("disabled", true);
    ePayco.token.create($form, function(error, token) {
      $form.find("button").prop("disabled", false);
      if(!error) {
        $form.append($("<input type='hidden' name='token_ep'>").val(token));
        $form.get(0).submit();
      } else {
        $('.card-errors').text(error.data.description);
      }
    });
  });
}]);
