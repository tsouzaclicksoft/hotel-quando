HotelQuandoApp.controller('PublicMobileSearchCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.checkinHour = 0;
  $scope.numberOfPeople = 1;
  

  $scope.isSearchDateValid = false;

  $scope.selectedPackSize = 3;

	var tabNumber = yojs.get('tabNumber');

  $scope.selectedTabRoom = parseInt(tabNumber);

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  $scope.setSearchDateValidity = function(){
    var parsedDate;

    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);

      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'
                   + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'
                   + parsedDate.getFullYear();
    } else {
      parsedDate = $scope.checkinDate;
    }

    $scope.parsedSelectedCheckinDate = parsedDate;
    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = new Date(dateStr.split('/')[2], (dateStr.split('/')[1] - 1), dateStr.split('/')[0], $scope.checkinHour);
      $scope.isSearchDateValid = (date >= minimumDateToSearch);
    }
    else{
      $scope.isSearchDateValid = false;
    }
  };

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  $scope.set_checkin_checkout_hour = function(){
    var checkin, pack_size, checkout;
    pack_size = parseInt(getParameterByName("by_pack_length", window.location))
    checkin = parseInt(getParameterByName("by_checkin_hour", window.location))
    checkout = checkin + pack_size
    if( (checkin.toString() != 'NaN') && (pack_size.toString() != 'NaN') ){
      $scope.checkinHourDate = new Date().setHours(checkin, 0, 0)
      $scope.checkoutHourDate = new Date().setHours(checkout, 0, 0)
    }else{
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
      $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
    }
  }


  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    return ($scope.isSearchDateValid);
  }

  $scope.resonsForDisabledSubmit = function(){
    if ((!$scope.isSearchDateValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
    }
  }


}]);