HotelQuandoApp.controller('PublicAffiliateSearchCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.checkinHour = 00;
  $scope.checkoutHour = 03;
  $scope.numberOfPeople = 1;

  $scope.isSearchDateValid = false;

  $scope.selectedPackSize = 01;

  $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
  $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  angular.element(document).ready(function () {
    $scope.showNextDayOrSameDayText()
    $scope.changeNumberOfPeople()
    $scope.disableCheckinButtons()
  });

  $scope.changeCheckinHour = function(operation){
    if(operation == "minus"){
      $scope.checkinHour -= 1;
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0);
      if ($scope.checkoutHour == 0){
        $scope.checkoutHour = 23
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else if ($scope.selectedPackSize > 12 && $scope.checkoutHour == 7){
        $scope.checkoutHour;
        $scope.selectedPackSize += 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else {
        $scope.checkoutHour -= 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      }
    }
    else if(operation == "plus"){
      $scope.checkinHour += 1;
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0);
      if ($scope.checkoutHour == 23){
        $scope.checkoutHour = 0
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else if ($scope.selectedPackSize > 12 && $scope.checkoutHour == 12){
        $scope.checkoutHour;
        $scope.selectedPackSize -= 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      } else {
        $scope.checkoutHour += 1;
        $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0);
      }
    }
    $scope.disableCheckinButtons()
    $scope.showNextDayOrSameDayText()
  }

  $scope.changePackSize = function(){
    var checkout = $scope.checkinHour + $scope.selectedPackSize
    if (checkout >= 24){
      checkout -= 24
    }
    $scope.checkoutHour = checkout
    $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
    $scope.disableCheckinButtons();
    $scope.showNextDayOrSameDayText()
  }

  $scope.disableCheckinButtons = function(){
    if ($scope.checkinHour == 0){
      $('.minus-checkin-hour').prop('disabled', true)
      $('.plus-checkin-hour').prop('disabled', false)
    }else if($scope.checkinHour == 23){
      $('.plus-checkin-hour').prop('disabled', true)
      $('.minus-checkin-hour').prop('disabled', false)
    }else{
      $('.minus-checkin-hour').prop('disabled', false)
      $('.plus-checkin-hour').prop('disabled', false)
    }
  }

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  $scope.set_checkin_checkout_hour = function(){
    var checkin, pack_size, checkout;
    pack_size = parseInt(getParameterByName("by_pack_length", window.location))
    checkin = parseInt(getParameterByName("by_checkin_hour", window.location))
    checkout = checkin + pack_size
    if( (checkin.toString() != 'NaN') && (pack_size.toString() != 'NaN') ){
      $scope.checkinHourDate = new Date().setHours(checkin, 0, 0)
      $scope.checkoutHourDate = new Date().setHours(checkout, 0, 0)
    }else{
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
      $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
    }
  }


  $scope.changeNumberOfPeople = function(operation){
    if(operation == "minus"){
      $scope.numberOfPeople -= 1
      if ($scope.numberOfPeople == 1){
        $('.minus-number-of-people').prop('disabled', true)
        $('.plus-number-of-people').prop('disabled', false)
      }else{
        $('.plus-number-of-people').prop('disabled', false)
      }
    }
    else if(operation == "plus"){
      $scope.numberOfPeople += 1
      if ( $scope.numberOfPeople == 99){
        $('.plus-number-of-people').prop('disabled', true)
        $('.minus-number-of-people').prop('disabled', false)
      }else{
        $('.minus-number-of-people').prop('disabled', false)
      }
    }
    else{
      if ($scope.numberOfPeople == 1){
        $('.minus-number-of-people').prop('disabled', true)
      }else if($scope.numberOfPeople == 99){
        $('.plus-number-of-people').prop('disabled', true)
      }else{
        $('.plus-number-of-people').prop('disabled', false)
        $('.minus-number-of-people').prop('disabled', false)
      }
    }
  }
  $scope.showNextDayOrSameDayText = function(){
    if ($scope.selectedPackSize == 3 && $scope.checkinHour >= 21){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 6 && $scope.checkinHour >= 18){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 9 && $scope.checkinHour >= 15){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 12&& $scope.checkinHour >= 12){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else{
      $('.next-day').hide()
      $('.same-day').show()
    }
  }

  $scope.setSearchDateValidity = function(){
    var parsedDate;

    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);

      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'
                   + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'
                   + parsedDate.getFullYear();
    } else {
      parsedDate = $scope.checkinDate;
    }

    $scope.parsedSelectedCheckinDate = parsedDate;
    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = new Date(dateStr.split('/')[2], (dateStr.split('/')[1] - 1), dateStr.split('/')[0], $scope.checkinHour);
      $scope.isSearchDateValid = (date >= minimumDateToSearch);
    }
    else{
      $scope.isSearchDateValid = false;
    }
  };

  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    return ($scope.isSearchDateValid);
  }

  $scope.resonsForDisabledSubmit = function(){
    if ((!$scope.isSearchDateValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
    }
  }


}]);
