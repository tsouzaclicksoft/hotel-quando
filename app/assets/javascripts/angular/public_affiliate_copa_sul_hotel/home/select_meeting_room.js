var SelectMeetingRoom = (function($) {
  var obj = {}

  obj.init = function(options, locale){
    var affiliate_hotels = JSON.parse(options.replace(/&quot;/g, "\"").replace(/=&gt;/g, ":"))
    var page_locale = locale.replace(/&quot;/g, "\"")
    var opt_options = []
    var selectize_options = []
    var tr_selectize;
    var $tr_selectize;

    $tr_selectize = $('#transamerica-hotels-select-hq').selectize({
      create: false,
      optgroupField: 'class',
      optgroups: [],
      options: [],
      render: {
        optgroup_header: function(item, escape) {
          return '<div class="optgroup-header">' + escape(item.title) + '</div>';
        },
      }
    })
    tr_selectize = $tr_selectize[0].selectize
    var opt_group;
    if(page_locale == 'pt_br' || page_locale == 'pt-BR'){
      opt_group = {
        class: 'TODOS OS HOTEIS',
        title: 'TODOS OS HOTEIS'
      }
    }else if(page_locale == 'es'){
      opt_group = {
        class: 'TODOS LOS HOTELES',
        title: 'TODOS LOS HOTELES'
      }      
    }else{
      opt_group = {
        class: 'ALL HOTELS',
        title: 'ALL HOTELS'
      }
    }
    tr_selectize.addOptionGroup(opt_group.class, opt_group)
    tr_selectize.addOption({class: opt_group.class, value: opt_group.title, text: opt_group.title})
  
    for(i = 0; i <= Object.keys(affiliate_hotels).length; i++){
      var opt_group = {
        class: affiliate_hotels["cidade_" + i],
        title: affiliate_hotels["cidade_" + i]
      }
      tr_selectize.addOptionGroup(opt_group.class, opt_group)
      tr_selectize.addOption({class: affiliate_hotels["cidade_" + i], value: affiliate_hotels["hotel_name_" + i], text: affiliate_hotels["hotel_name_" + i]})
      tr_selectize.addOption({class: affiliate_hotels["cidade_" + i], value: affiliate_hotels["cidade_" + i], text: affiliate_hotels["cidade_" + i]})
    }
  
    $(function(){
      if(page_locale == 'pt_br'){
        tr_selectize.setValue('TODOS OS HOTEIS')
      }else if(page_locale == 'es'){
        tr_selectize.setValue('TODOS LOS HOTELES')
      }else{
        tr_selectize.setValue('ALL HOTELS')
      }
    })
  }

  return obj;
})(jQuery)