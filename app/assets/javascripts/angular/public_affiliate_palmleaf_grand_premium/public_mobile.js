HotelQuandoApp.controller('PublicMobileSearchCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.checkinHour = 0;
  $scope.numberOfPeople = 1;

  $scope.isSearchDateValid = false;

  $scope.selectedTabRoom = 3;

  $scope.selectedPackSize = 03;

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  $scope.changeTab = function(type){
    $scope.selectedTabRoom = type
    if(type == 3){
      $scope.selectedPackSize = 03
    }else{
      $scope.selectedPackSize = 01
    }
  }

  $scope.setSearchDateValidity = function(){
    var parsedDate;

    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);

      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'
                   + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'
                   + parsedDate.getFullYear();
    } else {
      parsedDate = $scope.checkinDate;
    }

    $scope.parsedSelectedCheckinDate = parsedDate;
    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = new Date(dateStr.split('/')[2], (dateStr.split('/')[1] - 1), dateStr.split('/')[0], $scope.checkinHour);
      $scope.isSearchDateValid = (date >= minimumDateToSearch);
    }
    else{
      $scope.isSearchDateValid = false;
    }
  };

 
  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    return ($scope.isSearchDateValid);
  }

  $scope.resonsForDisabledSubmit = function(){
    if ((!$scope.isSearchDateValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
    }
  }


}]);