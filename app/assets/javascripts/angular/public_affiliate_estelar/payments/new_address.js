HotelQuandoApp.controller('PublicAffiliateNewPaymentAddressCtrl', ['$scope', '$window', function($scope, $window) {
  var initialize = function() {
    autocomplete = new $window.google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete')),{ types: ['(cities)']}
    );
    $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
      $scope.$digest();
    });
  }

  $('.js-autocomplete').onload = initialize();  

}]);
