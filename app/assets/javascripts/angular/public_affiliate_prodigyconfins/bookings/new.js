HotelQuandoApp.controller('PublicAffiliateNewBookingTravelersCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.changePhone = function(phone_number){
    if (phone_number != null){
      $("#booking_phone_number").intlTelInput("setNumber", phone_number)
    }
    else{
      $("#booking_phone_number").val('');
    }
  };

  var initialize = function() {
    autocomplete = new $window.google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete')),{ types: ['(cities)']}
    );
    $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
      $scope.$digest();
    });
  }

  $('.js-autocomplete').onload = initialize();

}]);
