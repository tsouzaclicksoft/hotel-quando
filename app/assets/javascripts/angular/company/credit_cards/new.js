HotelQuandoApp.controller('CompanyCreditCardCtrl', ['$scope', function($scope) {
  $scope.stateToReturnIfUserChangeCountryBackToBr = '';
  $scope.selectedCreditCardId = '';


  $scope.creditCardCountry = yojs.get('creditCardCountry');


  $scope.countryChanged = function(){
    if ($scope.creditCardCountry != 'BR') {

      var creditCardStateSelectVal = $('.js-credit-card-billing-state').val();
      if (!(creditCardStateSelectVal == 'ZZ')){
        $scope.stateToReturnIfUserChangeCountryBackToBr = creditCardStateSelectVal;
      }

      $scope.stateVisible = false;

      if ($('.js-credit-card-billing-state option[value="ZZ"]').length == 0){
        // Need to append a new option to the select, so we can set the value to ZZ
        // If we don't do this, the value of the select will be null
        $('.js-credit-card-billing-state').html($('.js-credit-card-billing-state').html() + '<option value="ZZ"></option>', '');
      }

      $('.js-credit-card-billing-state').val('ZZ');

    }
    else{
      $('.js-credit-card-billing-state').html($('.js-credit-card-billing-state').html().replace('<option value="ZZ"></option>', ''));
      $('.js-credit-card-billing-state').val(($scope.stateToReturnIfUserChangeCountryBackToBr || 'SP'));
      $scope.stateVisible = true;
    }
  }

}]);
