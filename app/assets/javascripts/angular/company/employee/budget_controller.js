HotelQuandoApp.controller('CompanyEmployeeBudgetController', ['$scope', function($scope) {
	$scope.budget_for_24_hours_pack = $('.js-employee-budget').val()

  $scope.getParsedEmployeeBudget = function(){
    $scope.employeeBudget = $('.js-employee-budget').maskMoney('unmasked')[0]
  }

}]);
