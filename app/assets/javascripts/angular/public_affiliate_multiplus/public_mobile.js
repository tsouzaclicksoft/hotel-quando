HotelQuandoApp.controller('PublicMobileSearchCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.checkinHour = 0;
  $scope.numberOfPeople = 1;
  $scope.address = '';

  $scope.latitude = '';

  $scope.longitude = '';

  $scope.isSearchDateValid = false;

  $scope.isLatitudeAndLongitudeValid = false;

  $scope.selectedPackSize = 3;

	var tabNumber = yojs.get('tabNumber');

  $scope.selectedTabRoom = parseInt(tabNumber);

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  $scope.setSearchDateValidity = function(){
    var parsedDate;

    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);

      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'
                   + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'
                   + parsedDate.getFullYear();
    } else {
      parsedDate = $scope.checkinDate;
    }

    $scope.parsedSelectedCheckinDate = parsedDate;
    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = new Date(dateStr.split('/')[2], (dateStr.split('/')[1] - 1), dateStr.split('/')[0], $scope.checkinHour);
      $scope.isSearchDateValid = (date >= minimumDateToSearch);
    }
    else{
      $scope.isSearchDateValid = false;
    }
  };

  $scope.setLatitudeAndLongitudeValidity = function(){
    // Check if latitude and longitude are numeric
    var latitudeValid = ((!isNaN(parseFloat($scope.latitude))) && (isFinite($scope.latitude)))
    var longitudeValid = ((!isNaN(parseFloat($scope.longitude))) && (isFinite($scope.longitude)))
    $scope.isLatitudeAndLongitudeValid = (latitudeValid && longitudeValid)
  }

  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    return ($scope.isSearchDateValid && $scope.isLatitudeAndLongitudeValid);
  }

  $scope.resonsForDisabledSubmit = function(){
    if ((!$scope.isSearchDateValid) && (!$scope.isLatitudeAndLongitudeValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li><li>' + yojs.get('error_message_for_address') + '</li></ul>';
    }
    else {
      if(!($scope.isSearchDateValid)){
        return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
      }
      else {
        return '<ul><li>' + yojs.get('error_message_for_address') + '</li></ul>';
      }
    }
  }

  var autocomplete;

  var initialize = function() {
    autocomplete = new $window.google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete'))
    );
    $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
      $scope.setLatitudeAndLongitude();
      $scope.$digest();
    });
  }

  $scope.setLatitudeAndLongitude = function() {
    try{
      var place = autocomplete.getPlace();
      $scope.latitude = place.geometry.location.lat();
      $scope.longitude = place.geometry.location.lng();
      //$scope.cityName = place.address_components[2].short_name;
      //$scope.stateName = place.address_components[3].short_name;
      $scope.address = place.formatted_address
      $scope.setLatitudeAndLongitudeValidity();
      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'locality') {
          var val = place.address_components[i].short_name;
          $scope.cityName = val;
        } else if (addressType == 'country') {
          var val = place.address_components[i].short_name;
          $scope.stateName = val;
        }
      }
    }
    catch(err){
      $scope.isLatitudeAndLongitudeValid = false
    }
  }

  $('.js-autocomplete').onload = initialize();

  // the angularjs dont fill this fields
  $('.js-autocomplete').closest('form').submit(function(){
    $('.cpy-mobile-city-name').val($scope.cityName);
    $('.cpy-mobile-state-name').val($scope.stateName);
    $('.cpy-mobile-latitude-field').val($scope.latitude);
    $('.cpy-mobile-longitude-field').val($scope.longitude);
    $('.cpy-mobile-address').val($scope.address);
  })

}]);