HotelQuandoApp.controller('PublicAffiliateSearchCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.checkinHour = 00;
  $scope.checkoutHour = 03;
  $scope.numberOfPeople = 1;
  $scope.address = '';

  $scope.latitude = '';

  $scope.longitude = '';

  $scope.isSearchDateValid = false;

  $scope.isLatitudeAndLongitudeValid = false;

  $scope.selectedPackSize = 03;


  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  angular.element(document).ready(function () {
    $scope.showNextDayOrSameDayText()
    $scope.changeNumberOfPeople()
    $scope.disableCheckinButtons()
  });

  $scope.changeCheckinHour = function(operation){
    if(operation == "minus"){
      $scope.checkinHour -= 1
      if ($scope.checkoutHour == 0){
        $scope.checkoutHour = 23
      }else{
        $scope.checkoutHour -= 1
      }
    }
    else if(operation == "plus"){
      $scope.checkinHour += 1
      if ($scope.checkoutHour == 23){
        $scope.checkoutHour = 0
      }else{
        $scope.checkoutHour += 1
      }
    }
    $scope.disableCheckinButtons()
    $scope.showNextDayOrSameDayText()
  }

  $scope.changePackSize = function(){
    var checkout = $scope.checkinHour + $scope.selectedPackSize
    if (checkout >= 24){
      checkout -= 24
    }
    $scope.checkoutHour = checkout
    $scope.showNextDayOrSameDayText()
  }

  $scope.disableCheckinButtons = function(){
    if ($scope.checkinHour == 0){
      $('.minus-checkin-hour').prop('disabled', true)
      $('.plus-checkin-hour').prop('disabled', false)
    }else if($scope.checkinHour == 23){
      $('.plus-checkin-hour').prop('disabled', true)
      $('.minus-checkin-hour').prop('disabled', false)
    }else{
      $('.minus-checkin-hour').prop('disabled', false)
      $('.plus-checkin-hour').prop('disabled', false)
    }
  }


  $scope.changeNumberOfPeople = function(operation){
    if(operation == "minus"){
      $scope.numberOfPeople -= 1
      if ($scope.numberOfPeople == 1){
        $('.minus-number-of-people').prop('disabled', true)
      }else{
        $('.plus-number-of-people').prop('disabled', false)
      }
    }
    else if(operation == "plus"){
      $scope.numberOfPeople += 1
      if ( $scope.numberOfPeople == 4){
        $('.plus-number-of-people').prop('disabled', true)
      }else{
        $('.minus-number-of-people').prop('disabled', false)
      }
    }
    else{
      if ($scope.numberOfPeople == 1){
        $('.minus-number-of-people').prop('disabled', true)
      }else if($scope.numberOfPeople == 4){
        $('.plus-number-of-people').prop('disabled', true)
      }else{
        $('.plus-number-of-people').prop('disabled', false)
        $('.minus-number-of-people').prop('disabled', false)
      }
    }
  }

  $scope.showNextDayOrSameDayText = function(){
    if ($scope.selectedPackSize == 3 && $scope.checkinHour >= 21){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 6 && $scope.checkinHour >= 18){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 9 && $scope.checkinHour >= 15){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else if ($scope.selectedPackSize == 12&& $scope.checkinHour >= 12){
      $('.next-day').show()
      $('.same-day').hide()
    }
    else{
      $('.next-day').hide()
      $('.same-day').show()
    }
  }

  $scope.setSearchDateValidity = function(){
    var parsedDate;

    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);

      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'
                   + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'
                   + parsedDate.getFullYear();
    } else {
      parsedDate = $scope.checkinDate;
    }

    $scope.parsedSelectedCheckinDate = parsedDate;
    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = new Date(dateStr.split('/')[2], (dateStr.split('/')[1] - 1), dateStr.split('/')[0], $scope.checkinHour);
      $scope.isSearchDateValid = (date >= minimumDateToSearch);
    }
    else{
      $scope.isSearchDateValid = false;
    }
  };

  $scope.setLatitudeAndLongitudeValidity = function(){
    // Check if latitude and longitude are numeric
    var latitudeValid = ((!isNaN(parseFloat($scope.latitude))) && (isFinite($scope.latitude)))
    var longitudeValid = ((!isNaN(parseFloat($scope.longitude))) && (isFinite($scope.longitude)))
    $scope.isLatitudeAndLongitudeValid = (latitudeValid && longitudeValid)
  }

  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    return ($scope.isSearchDateValid && $scope.isLatitudeAndLongitudeValid);
  }

  $scope.resonsForDisabledSubmit = function(){
    if ((!$scope.isSearchDateValid) && (!$scope.isLatitudeAndLongitudeValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li><li>' + yojs.get('error_message_for_address') + '</li></ul>';
    }
    else {
      if(!($scope.isSearchDateValid)){
        return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
      }
      else {
        return '<ul><li>' + yojs.get('error_message_for_address') + '</li></ul>';
      }
    }
  }

  var autocomplete;

  var initialize = function() {
    autocomplete = new $window.google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete2'))
    );
    $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
      $scope.setLatitudeAndLongitude();
      $scope.$digest();
    });
  }

  $scope.setLatitudeAndLongitude = function() {
    try{
      var place = autocomplete.getPlace();
      $scope.latitude = place.geometry.location.lat();
      $scope.longitude = place.geometry.location.lng();
      //$scope.cityName = place.address_components[2].short_name;
      //$scope.stateName = place.address_components[3].short_name;
      $scope.address = place.formatted_address
      $scope.setLatitudeAndLongitudeValidity();
      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'locality' || addressType == 'administrative_area_level_2') {
          var val = place.address_components[i].short_name;
          $scope.cityName = val;
        } else if (addressType == 'country') {
          var val = place.address_components[i].short_name;
          $scope.stateName = val;
        }
      }
    }
    catch(err){
      $scope.isLatitudeAndLongitudeValid = false
    }
  }

  $('.js-autocomplete2').onload = initialize();
  
  // the angularjs dont fill this fields
  $('.js-autocomplete2').closest('form').submit(function(){
    $('.cpy-city-name').val($scope.cityName);
    $('.cpy-state-name').val($scope.stateName);
    $('.cpy-latitude-field').val($scope.latitude);
    $('.cpy-longitude-field').val($scope.longitude);
    $('.cpy-address').val($scope.address);
  })

}]);