HotelQuandoApp.controller('AdminHotelCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.init = function(countries) {
    $scope.countryList = countries;
  };
  

  angular.element(document).ready(function () {
    $('#hotel_country').change();
    $('#hotel_state').change();
  });

}]);
