HotelQuandoApp.controller('PromotionalCodesFormCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.discountType = yojs.get('getDiscountType');
  $scope.userType = yojs.get('getUserType');
  $scope.usageLimit = yojs.get('getUsageLimit');
  $scope.Discount = $("#promotional_code_discount").val()
  var parsedDate = yojs.get('getExpirationDate').split('-')
  var locale = yojs.get("locale");
  var parsedStartDate = yojs.get('getStartDate').split('-') 
  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);
  
  if (parsedDate != ''){
	  parsedDate = parsedDate[2] +'/'+ parsedDate[1] + '/' + parsedDate[0]
	  $scope.expirationDate = parsedDate
  }
  if (parsedStartDate != ''){
    parsedStartDate = parsedStartDate[2] +'/'+ parsedStartDate[1] + '/' + parsedStartDate[0]
    $scope.startDate = parsedStartDate
  }


  function applyMaskInDiscountField() {
  	if (parseInt($scope.discountType) == 1){
  		$scope.discountTypeEmpty = false
		  $(document).on('mask-it', function(){
	 			$("#promotional_code_discount").maskMoney({suffix:' %', allowNegative: false, allowZero: false, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
	  		$("#promotional_code_discount").maskMoney('mask');
			}).trigger('mask-it');
  	}else if (parseInt($scope.discountType) == 2){
  		$scope.discountTypeEmpty = false
  		$(document).on('mask-it', function(){
	  		$("#promotional_code_discount").maskMoney({prefix: "", allowNegative: false, allowZero: false, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
	  		$("#promotional_code_discount").maskMoney('mask');
	  	}).trigger('mask-it');
  	}else{
  		$scope.discountTypeEmpty = true
  		$scope.Discount = ''
  	}
  	$("#promotional_code_discount").click()
  }

  applyMaskInDiscountField();

  if($scope.userType == 2){
  	$scope.userTypeWithoutBooking = true
  }


  $scope.changeDiscountType = function(){
  	applyMaskInDiscountField()
  }

  $scope.changeUserType = function(){
    if ($scope.userType == 2){
      $scope.userTypeWithoutBooking = true
      $scope.usageLimit = 1
    }else{
      $scope.userTypeWithoutBooking = false
    }

  }

  $scope.changeDiscountIn = function(){
  	if ($scope.discountIn == 2){
  		$scope.discountType = 2
      applyMaskInDiscountField()
  	}
  }

  $scope.changeStartDate = function(){
    if(yojs.get("locale") == "en"){
      parsedStartDate = new Date($scope.startDate);
      parsedStartDate = ('0' + parsedStartDate.getDate()).slice(-2) + '/' + ('0' + (parsedStartDate.getMonth()+1)).slice(-2) + '/' + parsedStartDate.getFullYear();
    }
    else parsedStartDate = $scope.startDate;


    var dateStr = $scope.startDate;

    if(datePattern.test(dateStr)){
      var date = dateStr.split('/');
      var dateToSearch = new Date(date[2], (date[1] - 1), date[0], 0);
      $('.date-picker-expiration').datepicker('setStartDate', dateToSearch.toLocaleDateString(locale));
    }
  }

}]);
