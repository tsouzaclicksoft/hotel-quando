HotelQuandoApp.controller('MemberGetMemberConfigFormCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.discountType = yojs.get('getDiscountType');
  $scope.userType = yojs.get('getUserType');
  $scope.usageLimit = yojs.get('getUsageLimit');
  $scope.Discount = $("#member_get_member_config_discount").val()
  var parsedDate = yojs.get('getExpirationDate').split('-')

  if (parsedDate != ''){
	  parsedDate = parsedDate[2] +'/'+ parsedDate[1] + '/' + parsedDate[0]
	  $scope.expirationDate = parsedDate
  }



  function applyMaskInDiscountField() {
  	if (parseInt($scope.discountType) == 1){
  		$scope.discountTypeEmpty = false
		  $(document).on('mask-it', function(){
	 			$("#member_get_member_config").maskMoney({suffix:' %', allowNegative: false, allowZero: false, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
	  		$("#member_get_member_config").maskMoney('mask');
			}).trigger('mask-it');
  	}else if (parseInt($scope.discountType) == 2){
  		$scope.discountTypeEmpty = false
  		$(document).on('mask-it', function(){
	  		$("#member_get_member_config").maskMoney({prefix: "", allowNegative: false, allowZero: false, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
	  		$("#member_get_member_config").maskMoney('mask');
	  	}).trigger('mask-it');
  	}else{
  		$scope.discountTypeEmpty = true
  		$scope.Discount = ''
  	}
  	$("#member_get_member_config").click()
  }

  applyMaskInDiscountField();

  if($scope.userType == 2){
  	$scope.userTypeWithoutBooking = true
  }


  $scope.changeDiscountType = function(){
  	applyMaskInDiscountField()
  }

  $scope.changeUserType = function(){
  	if ($scope.userType == 2){
  		$scope.userTypeWithoutBooking = true
  		$scope.usageLimit = 1
  	}else{
  		$scope.userTypeWithoutBooking = false
  	}

  }

}]);
