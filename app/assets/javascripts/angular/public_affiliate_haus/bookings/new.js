HotelQuandoApp.controller('PublicAffiliateNewBookingCtrl', ['$scope', '$window', function($scope, $window) {
  

  $scope.init = function(options_multiplus_points_for_people_select, number_of_people_selected) {
  	$scope.numberOfPeopleSelected = number_of_people_selected;
  	$scope.arrayMultiplusAndNumberOfPeople = options_multiplus_points_for_people_select;

	  angular.forEach($scope.arrayMultiplusAndNumberOfPeople, function(array) {
	  	var multiplus_points = array[0]
	  	var number_of_people = array[1]
	  	if ($scope.numberOfPeopleSelected == number_of_people){
	  		$scope.multiplusPoints = multiplus_points
	  	}
	  });
  };

  $scope.changeMultiplusPoints = function(options_multiplus_points_for_people_select){
  	$scope.arrayMultiplusAndNumberOfPeople = options_multiplus_points_for_people_select;
	  angular.forEach($scope.arrayMultiplusAndNumberOfPeople, function(array) {
	  	var multiplus_points = array[0]
	  	var number_of_people = array[1]
	  	if ($scope.numberOfPeopleSelected == number_of_people){
	  		$scope.multiplusPoints = multiplus_points
	  	}
	  });
  };


}]);