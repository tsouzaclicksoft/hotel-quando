HotelQuandoApp.controller('SignUpCtrl', ['$scope', '$window', function($scope, $window) {
  var autocomplete;

  var initialize = function() {
    autocomplete = new $window.google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete')),{ types: ['geocode']}
    );
    $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
      $scope.$digest();
    });
  }

  $('.js-autocomplete').onload = initialize();
}])