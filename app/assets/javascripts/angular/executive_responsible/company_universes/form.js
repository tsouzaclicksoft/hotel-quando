HotelQuandoApp.controller('CompanyUniversesFormCtrl', ['$scope', '$window', function($scope, $window) {
  $scope.isSentEmail = yojs.get('wasSentEmail');
  $scope.isTalkedPhone = yojs.get('wasTalkedPhone');
  $scope.isFaceMeeting = yojs.get('wasFaceMeeting');
  $scope.isRegistered = yojs.get('wasRegistered');
  $scope.hasAnAgency = yojs.get('hasAnAgency');
  $scope.hasContractAgency = yojs.get('hasContractAgency');
  $scope.getMuchCompanyPaysToCancel = yojs.get('getMuchCompanyPaysToCancel');
  $scope.getCompanyCanCancelThisAgreement = yojs.get('getCompanyCanCancelThisAgreement');
  $scope.getCompanyPaysThisAgency = yojs.get('getCompanyPaysThisAgency');

  var parsedMailDate = yojs.get('getSendEmailDate').split('-')
  var parsedTalkedDate = yojs.get('getTalkedDate').split('-')
  var parsedFaceDate = yojs.get('getFaceDate').split('-')
  var parsedRegisterDate = yojs.get('getRegisterDate').split
  var parsedDateExpiryAgencyContract = yojs.get('getDateExpiryAgencyContract').split('-')
  var parsedFollowUpDate = yojs.get('getFollowUpDate').split('-')

  $scope.modelCompanyPays = $scope.getCompanyPaysThisAgency
  $scope.modelCompanyPaysToCancel = $scope.getMuchCompanyPaysToCancel

  if($scope.getCompanyCanCancelThisAgreement == 'false' || $scope.getCompanyCanCancelThisAgreement == ''){
    $scope.isCheckedCompanyCanCancelAgreement = false
  }else{
    $scope.isCheckedCompanyCanCancelAgreement = true
  }

  if($scope.hasContractAgency == 'false' || $scope.hasContractAgency == ''){
    $scope.isCheckedHasContractAgency = false
    $scope.modelHasContractAgency = false
  }else{
    $scope.isCheckedHasContractAgency = true
    $scope.modelHasContractAgency = true
  }


  if($scope.hasAnAgency == 'false' || $scope.hasAnAgency == '' ){
    $scope.disabledCompanyPays = true
    $scope.modelCompanyPays = ''

    $scope.disabledHasContractAgency = true

    $scope.isCheckedHasContractAgency = false


    $scope.isCheckedCompanyCanCancelAgreement = false

    $scope.disabledCompanyCanCancelAgreement = true
    $scope.disableCompanyPaysToCancel = true
    $scope.disableDateExpiryAgencyContract = true
  }


  $scope.changeHasAnAgency = function() {
   if ($scope.modelHasAnAgency ) {
    $scope.disabledCompanyPays = false
    $scope.disabledHasContractAgency = false
    $scope.valueDateExpiryAgencyContract = ''

  } else {
   $scope.disabledCompanyPays = true
   $scope.modelCompanyPays = ''

   $scope.disabledHasContractAgency = true
   $scope.isCheckedHasContractAgency = false
   $scope.modelHasContractAgency = false

   $scope.isCheckedCompanyCanCancelAgreement = false
   $scope.modelCompanyCanCancelAgreement = false

   $scope.modelDateExpiryAgencyContract = ''
   $scope.modelCompanyPaysToCancel = ''

   $scope.disabledCompanyCanCancelAgreement = true
   $scope.disableCompanyPaysToCancel = true
   $scope.disableDateExpiryAgencyContract = true
   $scope.valueDateExpiryAgencyContract = ''
 }
}


$scope.changeHasContractAgency = function(){

  if ($scope.modelHasContractAgency ) {
    $scope.disabledCompanyCanCancelAgreement = false
    $scope.disableDateExpiryAgencyContract = false

  } else {
    $scope.modelCompanyPaysToCancel = ''
    $scope.modelDateExpiryAgencyContract = ''
    $scope.modelCompanyCanCancelAgreement = false
    $scope.isCheckedCompanyCanCancelAgreement=false
    $scope.disableCompanyPaysToCancel = true
    $scope.disabledCompanyCanCancelAgreement = true
    $scope.disableDateExpiryAgencyContract = true

  }

};

$scope.changeCompanyCanCancelAgreement = function(){

  if ($scope.modelCompanyCanCancelAgreement ) {
    $scope.disableCompanyPaysToCancel = false

  } else { 

    $scope.modelCompanyPaysToCancel = ''
    $scope.disableCompanyPaysToCancel = true

  }

};



if (parsedMailDate != ''){
 parsedMailDate = parsedMailDate[2] +'/'+ parsedMailDate[1] + '/' + parsedMailDate[0]
 $scope.mailDateValue = parsedMailDate
}

if (parsedDateExpiryAgencyContract != ''){
  parsedDateExpiryAgencyContract = parsedDateExpiryAgencyContract[2] +'/'+ parsedDateExpiryAgencyContract[1] + '/' + parsedDateExpiryAgencyContract[0]
  $scope.modelDateExpiryAgencyContract = parsedDateExpiryAgencyContract
}

if (parsedFollowUpDate != ''){
  parsedFollowUpDate = parsedFollowUpDate[2] +'/'+ parsedFollowUpDate[1] + '/' + parsedFollowUpDate[0]
  $scope.modelFollowUpDate = parsedFollowUpDate
}

if (parsedTalkedDate != ''){
  parsedTalkedDate = parsedTalkedDate[2] +'/'+ parsedTalkedDate[1] + '/' + parsedTalkedDate[0]
  $scope.talkedDateValue = parsedTalkedDate
}

if (parsedFaceDate != ''){
  parsedFaceDate = parsedFaceDate[2] +'/'+ parsedFaceDate[1] + '/' + parsedFaceDate[0]
  $scope.faceDateValue = parsedFaceDate
}

if (parsedRegisterDate != ''){
  parsedRegisterDate = parsedRegisterDate[2] +'/'+ parsedRegisterDate[1] + '/' + parsedRegisterDate[0]
  $scope.registerDateValue = parsedRegisterDate
}

if($scope.isFaceMeeting == 'false'|| $scope.isFaceMeeting == ''){
  $scope.disableFaceDate = true
  $scope.faceDateValue = ''
}

if($scope.isSentEmail == 'false'|| $scope.isSentEmail == ''){
  $scope.disableMailDate = true
  $scope.mailDateValue = ''
}

if($scope.isTalkedPhone == 'false'|| $scope.isTalkedPhone == ''){
  $scope.disableTalkedDate = true
  $scope.talkedDateValue = ''
}

if($scope.isRegistered == 'false'|| $scope.isRegistered == ''){
  $scope.disableRegisterDate = true
  $scope.registerDateValue = ''
}

if($scope.getCompanyCanCancelThisAgreement == 'false'|| $scope.getCompanyCanCancelThisAgreement == ''){
  $scope.disableRegisterDate = true
  $scope.registerDateValue = ''
}else{

}

$scope.changeFaceDate = function() {
  disableFieldFaceDate()
};

$scope.changeMailDate = function() {
  disableFieldMailDate()
};

$scope.changeTalkedDate = function() {
  disableFieldTalkedDate()
};

$scope.changeRegisterDate = function() {
  disableFieldRegisterDate()
};

function disableFieldMailDate(){
 if ($scope.checkboxMail ) {
  $scope.disableMailDate = false
} else {
  $scope.disableMailDate = true
  $scope.mailDateValue = ''
}
}

function disableFieldTalkedDate(){
  if ($scope.checkboxTalked ) {
    $scope.disableTalkedDate = false
  } else {
    $scope.disableTalkedDate = true
    $scope.talkedDateValue = ''
  }
}

function disableFieldFaceDate(){
  if ($scope.checkboxFace ) {
    $scope.disableFaceDate = false
  } else {
    $scope.disableFaceDate = true
    $scope.faceDateValue = ''
  }
}

function disableFieldRegisterDate(){
  if ($scope.checkboxRegister ) {
    $scope.disableRegisterDate = false
  } else {
    $scope.disableRegisterDate = true
    $scope.registerDateValue = ''
  }
}

}]);
