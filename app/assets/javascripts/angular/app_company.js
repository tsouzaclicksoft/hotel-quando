var HotelQuandoApp = angular.module('HotelQuandoApp', ['ngAnimate']);

HotelQuandoApp.config(['$httpProvider', function($httpProvider) {
  var authToken = $("meta[name=\"csrf-token\"]").attr("content");
  $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = authToken;
}]);

HotelQuandoApp.filter('numberBr', ['$filter', function($filter){
    var standardNumberFilterFn = $filter('number');
    return function(numberToFormat) {
      return standardNumberFilterFn(numberToFormat, 2).toString().replace(',', '#').replace('.', ',').replace('#', '.');
    }
}]);
