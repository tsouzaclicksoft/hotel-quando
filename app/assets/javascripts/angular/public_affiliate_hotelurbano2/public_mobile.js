HotelQuandoApp.factory('publicSearchFactory', ['$http', function ($http){
  var tzArray = {};
    tzArray.getTZ = function(lat,lng,tmstp){
        return $http({
                method : "GET",
                url : "/timezone?location="+lat+","+lng+"&tmstp="+tmstp
              }).then(function mySucces(response) {
                //(dstOffset + rawOffset) and timeZoneId
                return(response.data.msg);
              }, function myError(response) {
                console.log("Couldn't get timezone, please choose the search address again.")
                return('');
              });

    }
    return tzArray;
}]);

HotelQuandoApp.controller('PublicMobileSearchCtrl', ['$scope', '$window', 'publicSearchFactory', function($scope, $window, publicSearchFactory) {

  $scope.checkinHour = 0;
  $scope.checkoutHour = 03;
  $scope.numberOfPeople = 1;
  $scope.numberOfPeopleMeetingRoom = 1;
  $scope.address = '';
  $scope.timezone = {};
  $scope.timezoneSearched = '';

  $scope.latitude = '';

  $scope.longitude = '';

  $scope.isSearchDateValid = false;

  $scope.isLatitudeAndLongitudeValid = false;

  $scope.selectedPackSize = 3;

  $scope.moreHours = false;

  $scope.checkinHourMTR = 00;
  $scope.checkoutHourMTR = 01;
  $scope.selectedPackSizeMTR = 1;
  $scope.numberOfPeopleMeetingRoom = 1;

  $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
  $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
  $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0)
  $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)

  var datePattern = new RegExp(/\d{2}\/\d{2}\/\d{4}/);

  angular.element(document).ready(function () {
    if ($scope.latitude != "")
      $scope.getTimezoneFromGoogle($scope.latitude, $scope.longitude);
  });

  $scope.changeMoreHours = function(operation){
    if(operation == "plus"){
      $scope.moreHours = true;
      $scope.checkinHour = 18;
      $scope.checkoutHour = 7;
      $scope.selectedPackSize = 13
      $scope.setSearchDate();
      $scope.disableCheckinOptions();
    }
    else if(operation == "minus"){
      $scope.moreHours = false;
      $scope.checkinHour = 0;
      $scope.checkoutHour = 3;
      $scope.selectedPackSize = 3;
      $scope.enableCheckinOptions();
      $scope.enableCheckoutOptions;
    }
  }

  $scope.enableCheckinOptions = function(){
    Array.from(Array(24).keys()).forEach(function(entry) {
      $(".js-checkin-hour option[value='"+entry+"']").prop('disabled',false);
    });
  };

  $scope.enableCheckoutOptions = function(){
    Array.from(Array(24).keys()).forEach(function(entry) {
      $(".js-checkout-hour option[value='"+entry+"']").prop('disabled',false);
    });
  };

  $scope.disableCheckinOptions = function(){
    if($scope.moreHours == true || $scope.moreHours == "true"){
      Array.from(Array(18).keys()).forEach(function(entry) {
        $(".js-checkin-hour option[value='"+entry+"']").prop('disabled',true);
      });
    }
  };

  $scope.disableCheckoutOptions = function(){
    if ($scope.moreHours == true || $scope.moreHours == 'true'){
      if ($scope.selectedPackSize > 24){
        var checkout_hour = $scope.checkinHour - 12
      } else {
        var checkout_hour = $scope.checkinHour - 11
      }

      Array.from(Array(24).keys()).forEach(function(entry) {
        if (entry < checkout_hour)
          $(".js-checkout-hour option[value='"+entry+"']").prop('disabled',true);
      });
    }
  }

  $scope.disableCheckinButtons = function(){
    $scope.disableCheckinOptions();
  }

  $scope.disableCheckinButtonsMTR = function(){
    if ($scope.moreHours == true  || $scope.moreHours == 'true'){
      if ($scope.checkinHourMTR == 18){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHourMTR == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
    else {
      if ($scope.checkinHourMTR == 0){
        $('.minus-checkin-hour').prop('disabled', true)
        $('.plus-checkin-hour').prop('disabled', false)
      }else if($scope.checkinHourMTR == 23){
        $('.plus-checkin-hour').prop('disabled', true)
        $('.minus-checkin-hour').prop('disabled', false)
      }else{
        $('.minus-checkin-hour').prop('disabled', false)
        $('.plus-checkin-hour').prop('disabled', false)
      }
    }
  }

  $scope.disableCheckoutButtons = function(){
    $scope.disableCheckoutOptions();
  }

  $scope.disableCheckoutButtonsMTR = function(){
    var difference_hours = ($scope.checkoutHourMTR + 24) - $scope.checkinHourMTR
    if ($scope.checkoutHourMTR == 12){
      if ($scope.checkinHourMTR == 23){
        $('.minus-checkout-hour').prop('disabled', true)
        $('.plus-checkout-hour').prop('disabled', true)
      } else {
        $('.minus-checkout-hour').prop('disabled', false)
        $('.plus-checkout-hour').prop('disabled', true)
      }
    } else if ($scope.checkoutHourMTR == 7){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 13 && $scope.selectedPackSizeMTR < 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else if (difference_hours == 12 && $scope.selectedPackSizeMTR > 24){
      $('.minus-checkout-hour').prop('disabled', true)
      $('.plus-checkout-hour').prop('disabled', false)
    } else {
      $('.minus-checkout-hour').prop('disabled', false)
      $('.plus-checkout-hour').prop('disabled', false)
    }
  }

  $scope.changeCheckinHour = function(){
    if($scope.moreHours == true || $scope.moreHours == 'true'){
      $scope.checkinHour = parseInt($scope.checkinHour);
      $scope.checkoutHour = parseInt($scope.checkoutHour);
      var difference_hours = ($scope.checkoutHour + 24) - $scope.checkinHour
      if (difference_hours < 13)
        $scope.checkoutHour = parseInt($scope.checkinHour) + 13 - 24
      if ($scope.selectedPackSize < 24){
        $scope.selectedPackSize = ($scope.checkoutHour + 24) - $scope.checkinHour
      } else if ($scope.selectedPackSize > 24 && $scope.selectedPackSize < 48){
        $scope.selectedPackSize = ($scope.checkoutHour + 48) - $scope.checkinHour
      } else {
        $scope.selectedPackSize = ($scope.checkoutHour + 72) - $scope.checkinHour
      }
      $scope.enableCheckoutOptions();
      $scope.disableCheckoutOptions();
      $scope.setSearchDateValidity();
    }
  }

  $scope.changeCheckinHourMTR = function(operation){
    if(operation == "minus"){
      $scope.checkinHourMTR -= 1;
      $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0);
      if ($scope.checkoutHourMTR == 0){
        $scope.checkoutHourMTR = 23
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else if ($scope.selectedPackSizeMTR > 12 && $scope.checkoutHourMTR == 7){
        $scope.checkoutHourMTR;
        $scope.selectedPackSizeMTR += 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else {
        $scope.checkoutHourMTR -= 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      }
    }
    else if(operation == "plus"){
      $scope.checkinHourMTR += 1;
      $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0);
      if ($scope.checkoutHourMTR == 23){
        $scope.checkoutHourMTR = 0
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else if ($scope.selectedPackSizeMTR > 12 && $scope.checkoutHourMTR == 12){
        $scope.checkoutHourMTR;
        $scope.selectedPackSizeMTR -= 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      } else {
        $scope.checkoutHourMTR += 1;
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0);
      }
    }
    $scope.disableCheckinButtonsMTR();
    $scope.disableCheckoutButtonsMTR();
    $scope.showNextDayOrSameDayText();
    $scope.setSearchDateValidityMTR();
  }

  $scope.changeCheckoutHour = function(){
    if($scope.moreHours == true || $scope.moreHours == 'true'){
      $scope.checkinHour = parseInt($scope.checkinHour);
      $scope.checkoutHour = parseInt($scope.checkoutHour);
      var pack = $scope.selectedPackSize;
      var checkout = parseInt($scope.checkoutHour);
      if ($scope.selectedPackSize < 24){
        $scope.selectedPackSize = (checkout + 24) - $scope.checkinHour
      } else if ($scope.selectedPackSize > 24 && $scope.selectedPackSize < 48){
        $scope.selectedPackSize = (checkout + 48) - $scope.checkinHour
      } else {
        $scope.selectedPackSize = (checkout + 72) - $scope.checkinHour
      }
    }
  }

  $scope.changeCheckoutHourMTR = function(operation){
    if(operation == "minus"){
      $scope.checkoutHourMTR -= 1;
      $scope.selectedPackSizeMTR -= 1;
    }
    else if(operation == "plus"){
      $scope.checkoutHourMTR += 1;
      $scope.selectedPackSizeMTR += 1;
    }
    $scope.disableCheckoutButtonsMTR();
  }

  $scope.setSearchDate = function(){
    var parsedDate;
    var dateToSearch;
    var locale = yojs.get("locale");
    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDate);

      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'
                   + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'
                   + parsedDate.getFullYear();
    }
    else parsedDate = $scope.checkinDate;

    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = dateStr.split('/');
      dateToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
      dateInt = dateToSearch.getTime() - minimumDateToSearch;
      // Check timezone (requesting our server) only if difference between +/- 24h
      if((typeof $scope.timezone.timezone_id != 'undefined') && (dateInt > -82799000) && (dateInt < 82801000) && ($scope.latitude != '') )
        $scope.getTimezoneFromGoogle($scope.latitude, $scope.longitude);
    }
    if(($scope.moreHours == true || $scope.moreHours == 'true') && dateToSearch != undefined){
      var dataFormat = (locale == "en" ? "mm/dd/yyyy" : "dd/mm/yyyy")
      var tomorrow = dateToSearch.setDate(dateToSearch.getDate() +1);
      var endDate = new Date(tomorrow);
      endDate.setDate(endDate.getDate()+2);
      var formatedTomorrow = dateToSearch.toLocaleDateString(locale);
      $scope.checkoutDate = formatedTomorrow;
      $('.cpy-checkout-date-calendar-input').datepicker('setStartDate', dateToSearch.toLocaleDateString(locale));
      $('.cpy-checkout-date-calendar-input').datepicker('setEndDate', endDate.toLocaleDateString(locale));
      $scope.parsedSelectedCheckoutDate = formatedTomorrow;
      $scope.selectedPackSize = (parseInt($scope.checkoutHour) + 24) - $scope.checkinHour;
    }
    $scope.setSearchDateValidity();
  }

  $scope.setSearchDateMTR = function(){
    var parsedDate;
    var locale = yojs.get("locale");
    if(yojs.get("locale") == "en"){
      parsedDate = new Date($scope.checkinDateMTR);
      parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/' + ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/' + parsedDate.getFullYear();
    }
    else parsedDate = $scope.checkinDateMTR;

    var dateStr = parsedDate;
    var actualDate = new Date();
    var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
    if(datePattern.test(dateStr)){
      date = dateStr.split('/');
      dateToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR);
      dateInt = dateToSearch.getTime() - minimumDateToSearch;
      // Check timezone (requesting our server) only if difference between +/- 24h
      if(typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateInt > -82799000) && (dateInt < 82801000) && ($scope.latitude != '') )
          $scope.getTimezoneFromGoogleMTR($scope.latitude, $scope.longitude);
    }
    if($scope.moreHours == true || $scope.moreHours == 'true'){
      var dataFormat = (locale == "en" ? "mm/dd/yyyy" : "dd/mm/yyyy")
      var tomorrow = dateToSearch.setDate(dateToSearch.getDate() +1);
      var endDate = new Date(tomorrow);
      endDate.setDate(endDate.getDate()+2);
      var formatedTomorrow = dateToSearch.toLocaleDateString(locale);
      $scope.checkoutDateMTR = formatedTomorrow;
      $('.cpy-checkout-date-calendar-input').datepicker('setStartDate', dateToSearch.toLocaleDateString(locale));
      $('.cpy-checkout-date-calendar-input').datepicker('setEndDate', endDate.toLocaleDateString(locale));
      $scope.parsedSelectedCheckoutDateMTR = formatedTomorrow;
      $scope.selectedPackSizeMTR = ($scope.checkoutHourMTR + 24) - $scope.checkinHourMTR;;
    }
    $scope.setSearchDateValidityMTR();
  }

  $scope.setSearchEndDate = function(){
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    if(yojs.get("locale") == "en"){
      parsedCheckinDate = new Date($scope.checkinDate);
      parsedCheckinDate = ('0' + parsedCheckinDate.getDate()).slice(-2) + '/' + ('0' + (parsedCheckinDate.getMonth()+1)).slice(-2) + '/' + parsedCheckinDate.getFullYear();
      parsedCheckoutDate = new Date($scope.checkoutDate);
      parsedCheckoutDate = ('0' + parsedCheckoutDate.getDate()).slice(-2) + '/' + ('0' + (parsedCheckoutDate.getMonth()+1)).slice(-2) + '/' + parsedCheckoutDate.getFullYear();
    }
    else {
      parsedCheckinDate = $scope.checkinDate;
      parsedCheckoutDate = $scope.checkoutDate;
    }

    if(datePattern.test(parsedCheckinDate)){
      date = parsedCheckinDate.split('/');
      checkinToSearch = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
    }
    if(datePattern.test(parsedCheckoutDate)){
      date = parsedCheckoutDate.split('/');
      checkoutToSearch = new Date(date[2], (date[1] - 1), date[0], parseInt($scope.checkoutHour));
    }


    var firstDate = new Date(checkinToSearch);
    var secondDate = new Date(checkoutToSearch);

    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
    if (diffDays == 1){
      $scope.selectedPackSize = (parseInt($scope.checkoutHour) + 24) - $scope.checkinHour
    } else if (diffDays == 2){
      $scope.selectedPackSize = (parseInt($scope.checkoutHour) + 48) - $scope.checkinHour
    } else {
      $scope.selectedPackSize = (parseInt($scope.checkoutHour) + 72) - $scope.checkinHour
    }
  }

  $scope.setSearchDateValidity = function(){
    if (typeof $scope.checkinDate == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.checkinDate);

      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour); // subtract 1 from month because javascript start month in 0 = jan
      } else {
        parsedDate = $scope.checkinDate;
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHour);
      }
      $scope.parsedSelectedCheckinDate = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;

      // Consider timezone when search datetime is between +/- 23h from local time
      if (typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateToInt > -82799000) && (dateToInt < 82801000) && ($scope.latitude != '') )
          minimumDateToSearch += $scope.timezone.timezone_value + (new Date().getTimezoneOffset()*60000);
    }

    if(datePattern.test(parsedDate)){
      if(dateToSearch >= minimumDateToSearch)
        $scope.isSearchDateValid = true
      else $scope.isSearchDateValid = false;
    }
    else $scope.isSearchDateValid = false;
  };

  $scope.setSearchDateValidityMTR = function(){
    if (typeof $scope.checkinDateMTR == 'undefined'){
      var dt = new Date();
      var parsedDate = dt.setDate(dt.getDate()-1);
    }
    else {
      var parsedDate = new Date($scope.checkinDateMTR);
      if(yojs.get("locale") == "en"){
        parsedDate = ('0' + parsedDate.getDate()).slice(-2) + '/'+ ('0' + (parsedDate.getMonth()+1)).slice(-2) + '/'+ parsedDate.getFullYear();
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR); // subtract 1 from month because javascript start month in 0 = jan
      } else {
        parsedDate = $scope.checkinDateMTR;
        var date                = parsedDate.split('/');
        var dateToSearch        = new Date(date[2], (date[1] - 1), date[0], $scope.checkinHourMTR);
      }
      $scope.parsedSelectedCheckinDateMTR = parsedDate;

      var actualDate          = new Date();
      var minimumDateToSearch = actualDate.setHours(actualDate.getHours() + 1);
      var dateToInt           = dateToSearch.getTime() - minimumDateToSearch;

      // Consider timezone when search datetime is between +/- 23h from local time
      if (typeof $scope.timezone != 'undefined')
        if ( ($scope.timezone.timezone_name != '') && (dateToInt > -82799000) && (dateToInt < 82801000) && ($scope.latitude != '') )
          minimumDateToSearch += $scope.timezone.timezone_value + (new Date().getTimezoneOffset()*60000);
    }

    if(datePattern.test(parsedDate)){
      if(dateToSearch >= minimumDateToSearch)
        $scope.isSearchDateValidMTR = true
      else $scope.isSearchDateValidMTR = false;
    }
    else $scope.isSearchDateValidMTR = false;
  };

  $scope.setLatitudeAndLongitudeValidity = function(){
    // Check if latitude and longitude are numeric
    var latitudeValid = ((!isNaN(parseFloat($scope.latitude))) && (isFinite($scope.latitude)))
    var longitudeValid = ((!isNaN(parseFloat($scope.longitude))) && (isFinite($scope.longitude)))
    $scope.isLatitudeAndLongitudeValid = (latitudeValid && longitudeValid)
  }

  $scope.isFormValid = function(){
    $scope.setSearchDateValidity();
    return ($scope.isSearchDateValid && $scope.isLatitudeAndLongitudeValid);
  }

  $scope.isFormValidMTR = function(){
    $scope.setSearchDateValidityMTR();
    return ($scope.isSearchDateValidMTR);
  }

  $scope.resonsForDisabledSubmit = function(){
    if ((!$scope.isSearchDateValid) && (!$scope.isLatitudeAndLongitudeValid)){
      return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li><li>' + yojs.get('error_message_for_address') + '</li></ul>';
    }
    else {
      if(!($scope.isSearchDateValid)){
        return '<ul><li>' + yojs.get('the_selected_date_is_invalid_or_in_the_past') + '</li></ul>';
      }
      else {
        return '<ul><li>' + yojs.get('error_message_for_address') + '</li></ul>';
      }
    }
  }

  var autocomplete;

  var initialize = function() {
    if (document.getElementById('js-autocomplete') != null) {
      autocomplete = new $window.google.maps.places.Autocomplete((document.getElementById('js-autocomplete')));
      $window.google.maps.event.addListener(autocomplete, 'place_changed', function() {
        $scope.setLatitudeAndLongitude();
        $scope.$digest();
      });
    }
  }

  var autocomplete_mr_mobile;
  var initialize_mr_mobile = function(){
    if (document.getElementById('js-autocomplete-2-meeting-room') != null){
      autocomplete_mr_mobile = new $window.google.maps.places.Autocomplete((document.getElementById('js-autocomplete-2-meeting-room')));
      $window.google.maps.event.addListener(autocomplete_mr_mobile, 'place_changed', function() {
        $scope.setLatitudeAndLongitude();
        $scope.$digest();
      });
    }
  }

  $scope.getTimezoneFromGoogle = function(lat, lng) {
    if (lat != "") {
      if (!$scope.checkinDate)
        var tmstp = Math.round(((new Date()).getTime()/1000))
      else {
        var d = $scope.checkinDate.split("/");
        // Get user timezone - local
        var splitDateTimeNow = new Date().toString().split(" ");
        timeZoneFormatted = splitDateTimeNow[splitDateTimeNow.length - 2] + " " + splitDateTimeNow[splitDateTimeNow.length - 1];
        // Format datetime with timezone to send to server
        if (yojs.get("locale") == "en")
              var tmstp = new Date(d[2], ( d[0]-1 ), d[1], ($scope.checkinHour).toString(), "0").getTime() / 1000
        else  var tmstp = new Date(d[2], ( d[1]-1 ), d[0], $scope.checkinHour.toString(), "0").getTime() / 1000;
      }
      publicSearchFactory.getTZ($scope.latitude, $scope.longitude, tmstp)
        .then(function(arrItems){
          $scope.timezone = arrItems;
          $scope.timezoneSearched = arrItems.timezone_id;
      });
    }
  }

  $scope.setLatitudeAndLongitude = function() {
    try{
      var place = (autocomplete.getPlace() == undefined ? autocomplete_mr_mobile.getPlace() : autocomplete.getPlace())
      $scope.latitude = place.geometry.location.lat();
      $scope.longitude = place.geometry.location.lng();
      //$scope.cityName = place.address_components[2].short_name;
      //$scope.stateName = place.address_components[3].short_name;
      $scope.address = place.formatted_address
      $scope.setLatitudeAndLongitudeValidity();

      if ($scope.isLatitudeAndLongitudeValid)
        $scope.getTimezoneFromGoogle(place.geometry.location.lat(), place.geometry.location.lng())
      else $scope.timezone = {};

      // Get each component of the address from the place details
      // and fill the corresponding field on the form.
      for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'locality') {
          var val = place.address_components[i].short_name;
          $scope.cityName = val;
        } else if (addressType == 'country') {
          var val = place.address_components[i].short_name;
          $scope.stateName = val;
        }
      }
    }
    catch(err){
      $scope.isLatitudeAndLongitudeValid = false
    }
  }

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  $scope.set_checkin_checkout_hourMTR = function(){
    var search = getParameterByName("by_search", window.location)
    if (search == 'meetingRoom'){
      $('.search_tab_rooms').removeClass('active_tab')
      var checkin, pack_size, checkout;
      pack_size = ( parseInt(getParameterByName("by_pack_lengthMTR", window.location)) || parseInt(getParameterByName("length_of_pack", window.location)) )
      checkin = ( parseInt(getParameterByName("by_checkin_hourMTR", window.location)) || parseInt(getParameterByName("checkin_hour", window.location)) )
      checkout = checkin + pack_size
      if( (checkin.toString() != 'NaN') && (pack_size.toString() != 'NaN') ){
        $scope.checkinHourDateMTR = new Date().setHours(checkin, 0, 0)
        $scope.checkoutHourDateMTR = new Date().setHours(checkout, 0, 0)
        $scope.checkinHourMTR = checkin
        $scope.selectedPackSizeMTR = pack_size
        $scope.checkoutHourMTR = checkout
        $scope.checkinDateMTR = getParameterByName("by_checkin_dateMTR", window.location)
      }else{
        $scope.checkinHourDateMTR = new Date().setHours($scope.checkinHourMTR, 0, 0)
        $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)
      }
    }
  }

  $scope.set_checkin_checkout_hour = function(){
    var checkin, pack_size, checkout;
    pack_size = parseInt(getParameterByName("by_pack_length", window.location))
    checkin = parseInt(getParameterByName("by_checkin_hour", window.location))
    checkout = checkin + pack_size
    if( (checkin.toString() != 'NaN') && (pack_size.toString() != 'NaN') ){
      $scope.checkinHourDate = new Date().setHours(checkin, 0, 0)
      $scope.checkoutHourDate = new Date().setHours(checkout, 0, 0)
    }else{
      $scope.checkinHourDate = new Date().setHours($scope.checkinHour, 0, 0)
      $scope.checkoutHourDate = new Date().setHours($scope.checkoutHour, 0, 0)
    }
    $('.search_tab_rooms').addClass('active_tab')
    $('.search_tab_meeting_rooms').removeClass('active_tab')
    $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'none')
    $('.tab-content.tab-content-custom.any-time').css('display', 'inherit')
  }

  $scope.changePackSizeMTR = function(){
    var checkout_mtr = $scope.checkinHourMTR + $scope.selectedPackSizeMTR
    if (checkout_mtr >= 24){
      checkout_mtr -= 24
    }
    $scope.checkoutHourMTR = checkout_mtr
    $scope.checkoutHourDateMTR = new Date().setHours($scope.checkoutHourMTR, 0, 0)
    $scope.disableCheckinButtonsMTR();
    $scope.showNextDayOrSameDayText()
  }

  $('.js-autocomplete').onload = initialize();
  $('.js-autocomplete-2-meeting-room').onload = initialize_mr_mobile();

  // the angularjs dont fill this fields
  $('.js-autocomplete').closest('form').submit(function(){
    $('.cpy-mobile-city-name').val($scope.cityName);
    $('.cpy-mobile-state-name').val($scope.stateName);
    $('.cpy-mobile-latitude-field').val($scope.latitude);
    $('.cpy-mobile-longitude-field').val($scope.longitude);
    $('.cpy-mobile-address').val($scope.address);
  })

}]);
