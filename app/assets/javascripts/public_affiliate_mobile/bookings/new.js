yojs.define("hq.public_affiliate.bookings.new", function() {
  $("#booking_phone_number").intlTelInput({
    preferredCountries: ["br","us","es"],
    autoPlaceholder: 'off'
  });

  $("form").submit(function() {
    phoneNumberInput = $("#booking_phone_number");
    phoneNumberInput.val(phoneNumberInput.intlTelInput("getNumber"));
  });

  $("#booking_phone_number").on("countrychange", function(e, countryData) {
    var countryData = $("#booking_phone_number").intlTelInput("getSelectedCountryData");
    if (countryData.iso2 == "br"){
      $("#booking_phone_number").mask('(99) 99999-9999');
    }
    else{
      $("#booking_phone_number").unmask();
    }
  });  
});