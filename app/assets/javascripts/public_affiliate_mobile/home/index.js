yojs.define("hq.public_affiliate.home.index", function() {

  $(document).ready(function(){
    $(".how-it-works-btn").click(function(){
      if($('#toggle').is(":checked")){
        $('#toggle').prop('checked', false);
        $('.arrow-up-or-down').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        $('div.slideOutTab').stop(true, false)
              .animate({
                  left: '-75%'
              }, 400);
      }
      else{
        $('#toggle').prop('checked', true);
        $('.arrow-up-or-down').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        $('div.slideOutTab').stop(true, false)
              .animate({
                  left: 0
              }, 400);
       }    
    });
  });

})