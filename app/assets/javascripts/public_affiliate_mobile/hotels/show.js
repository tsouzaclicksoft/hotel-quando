yojs.define("hq.public_affiliate.hotel.show", function(){
  yojs.call('hq.configMap');
  yojs.call("hq.goToNewBooking");
});

yojs.define("hq.configMap", function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(yojs.get('hotelLatitude'),yojs.get('hotelLongitude')),
    zoom: 16,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map-canvas-mobile"), mapOptions);
  var marker = new google.maps.Marker({
    map: map,
    position: map.getCenter()
  });
  var infowindow = new google.maps.InfoWindow();
  infowindow.setContent('<b>' + yojs.get('hotelName') + '</b>');
  infowindow.open(map,marker);
  google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map, marker);
  });
});

yojs.define("hq.goToNewBooking", function() {
  $(".js-room-type-description").on("click", function(){
    $(this).siblings().last().find(".js-new-booking")[0].click();
  });
});
