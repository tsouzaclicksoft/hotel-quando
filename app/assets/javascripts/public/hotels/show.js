yojs.define("hq.public.hotel.show", function(){
  yojs.call('hq.configMap');
  yojs.call("floatRightBox");

  // For explanation of this code, see home#index javascript
  $('.js-scroll-button').click(function() {
    var target = $(this.hash);
    $('html,body').animate({
      scrollTop: (target.offset().top - 69)
    }, 1000);
    return false;
  });
});

yojs.define("hq.configMap", function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(yojs.get('hotelLatitude'),yojs.get('hotelLongitude')),
    zoom: 16,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
  var marker = new google.maps.Marker({
    map: map,
    position: map.getCenter()
  });
  var infowindow = new google.maps.InfoWindow();
  infowindow.setContent('<b>' + yojs.get('hotelName') + '</b>');
  infowindow.open(map,marker);
  google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map, marker);
  });
});

yojs.define("floatRightBox", function(){
  $(window).scroll(function () {
    if (yojs.call("floatRightBox.scrollTop") > 330) {
      if (!$(".js-float-right-box").hasClass("floating")) {
        $(".js-float-right-box").addClass("floating");
        $(".footer").css("z-index","101");
      }
    } else {
      if ($(".js-float-right-box").hasClass("floating")) {
        $(".js-float-right-box").removeClass("floating");
        $(".footer").css("z-index","0");
      }
    }
  });
});

yojs.define("floatRightBox.scrollTop", function(){
  var ScrollTop = document.body.scrollTop;
  if (ScrollTop == 0)
  {
    if (window.pageYOffset)
      ScrollTop = window.pageYOffset;
    else
      ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
  }
  return ScrollTop;
});
