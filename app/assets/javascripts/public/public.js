yojs.define("hq.public", function(){
  $('.js-chosen-select').chosen();
  yojs.call("components.masks.init");
  yojs.call("components.bootbox.init");
  yojs.call("components.directs3upload.configureDirectS3Upload");
  yojs.call("components.emailSuggestions.init");
  yojs.call("showNewsletterModal");
  yojs.call("sendNewsletterModal");
  yojs.call("showCurrencyModal");
  yojs.call("maskLocaleDate");
  yojs.call("floatHeaderSearchForm");
  yojs.call("configUserDropdownMenu");
  yojs.call("configKnowMoreDropdownMenu");
  yojs.call("configCurrencyMoneyDropdownMenu");
  yojs.call("configMyProfileDropdownMenu");
  yojs.call("configDisabledButtonTooltip");
  yojs.call("putBorderBoxOnFlashMessage");
  yojs.call("configDatePicker");
  yojs.call("components.jqueryPlaceholder.init");
  yojs.call("loadingModal");
  yojs.call("enableSearchBarTabs");
  yojs.call("showAlertAirOffline");

})

yojs.define("showAlertAirOffline", function(){
  $("#search-button-air").click(function(){
     $('#air-modal').modal();
  });
})

yojs.define("sendNewsletterModal", function(){
  $('.form-newsletter-modal').submit(function(){
    $('.form').hide();
    $('.email-sent').show();
  })
})

yojs.define("loadingModal", function(){
  $body = $("body");

  $(document).on({
    ajaxStart: function() { $body.addClass("loading"); },
    ajaxStop: function() { $body.removeClass("loading"); }
  });
})

yojs.define("showNewsletterModal", function(){
  if(yojs.get("firstVisit") == "true"){
    $('#newsletter-modal').modal();
  }
})

yojs.define("showCurrencyModal", function(){
  if(yojs.get("firstVisit") == "true"){
    $('#currency-modal').modal();
  }
})

yojs.define("maskLocaleDate", function(){
  if(yojs.get("locale") == "en"){
    $('.js-mask-date').attr('placeholder','mm/dd/yyyy');
  } else {
    $('.js-mask-date').attr('placeholder','dd/mm/aaaa');
  }
})

yojs.define("floatHeaderSearchForm", function(){
  $(window).scroll(function () {
    if (yojs.call("floatHeaderSearchForm.scrollTop") > 269) {
      if (!$(".js-float-search-form").hasClass("floating")) {
        $(".js-float-search-form").addClass("floating");
      }
    } else {
      if ($(".js-float-search-form").hasClass("floating")) {
        $(".js-float-search-form").removeClass("floating");
      }
    }
  });
});

yojs.define("floatHeaderSearchForm.scrollTop", function(){
  var ScrollTop = document.body.scrollTop;
  if (ScrollTop == 0)
  {
    if (window.pageYOffset)
      ScrollTop = window.pageYOffset;
    else
      ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
  }
  return ScrollTop;
});


yojs.define("configUserDropdownMenu", function(){
  $(".js-menu-wrapper").mouseenter(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-user-dropdown-menu").slideDown('fast');
    }
  });

  $(".js-menu-wrapper").mouseleave(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-user-dropdown-menu").stop(true, true).slideUp('fast');
    }
  })

});

yojs.define("configKnowMoreDropdownMenu", function(){
  $(".js-menu-know-more-wrapper").mouseenter(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-know-more-dropdown-menu").slideDown('fast');
    }
  });

  $(".js-menu-know-more-wrapper").mouseleave(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-know-more-dropdown-menu").stop(true, true).slideUp('fast');
    }
  })

});


yojs.define("configMyProfileDropdownMenu", function(){
  $(".js-menu-my-profile-wrapper").mouseenter(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-my-profile-dropdown-menu").slideDown('fast');
    }
  });

  $(".js-menu-my-profile-wrapper").mouseleave(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-my-profile-dropdown-menu").stop(true, true).slideUp('fast');
    }
  })

});

yojs.define("configCurrencyMoneyDropdownMenu", function(){
  $(".js-menu-currency-current").mouseenter(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-currency-current-dropdown-menu").slideDown('fast');
    }
  });

  $(".js-menu-currency-current").mouseleave(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-currency-current-dropdown-menu").stop(true, true).slideUp('fast');
    }
  });
});

yojs.define("configDisabledButtonTooltip", function(){
  $('.js-tooltip-toggler').tooltip();
  $('.tooltip').tooltip('fixTitle');
});

yojs.define("putBorderBoxOnFlashMessage", function(){
  if($(".border-box").length > 0){
    $(".js-flash-message").addClass("border-box");
  }
});

yojs.define("configDatePicker", function(){
  var locale = yojs.get("locale");
  var dataFormat = (locale == "en" ? "mm/dd/yyyy" : "dd/mm/yyyy")
  var startDate = new Date();
  startDate.setDate(startDate.getDate()-1);

  $( ".js-date-picker-origin-air" ).datepicker({
      orientation: 'auto',
      autoclose: true,
      language: locale,
      format: dataFormat,
      startDate: startDate
  }).on( "change", function() {
      $( ".js-date-picker-destiny-air" ).datepicker( "setStartDate", getDate( this )  );
  });

  $( ".js-date-picker-destiny-air" ).datepicker({
      rientation: 'auto',
      autoclose: true,
      language: locale,
      format: dataFormat
  }).on( "change", function() {
      $( ".js-date-picker-origin-air" ).datepicker( "setEndDate", getDate( this )  );
  });

  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( "dd/mm/yy", element.value );
    } catch( error ) {
      date = null;
    }

    return date;
  }


  $('.js-date-picker').datepicker({
    orientation: 'auto',
    autoclose: true,
    todayHighlight: false,
    language: locale,
    format: dataFormat,
    startDate: startDate.toLocaleDateString(locale)
  }).next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
  $('#datepicker-checkout').datepicker()
  .on('changeDate', function(ev){
      $('#datepicker-checkout').datepicker('hide');
  });

  $('.js-date-picker').datepicker().on('show', function(e){
    $(".datepicker").css("z-index", "999");
  });

  $('.js-search-date-picker').datepicker().on('show', function(e){
    $(".datepicker").css("z-index", "10059");
  });

});

yojs.define("enableSearchBarTabs", function(){
  $('.cpy-search-by').attr('value', 'rooms')
  $('.describe-any-time').removeClass('hide')
  $('.describe-any-time-meeting_room').addClass('hide')
  $('.describe-any-time-air').addClass('hide')

  $('.search_tab_rooms').on('click', function(){
    $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'none')
    $('.tab-content.tab-content-custom.any-time_air').css('display', 'none')
    $('.tab-content.tab-content-custom.any-time').css('display', 'inherit')
    $('.search_tab_rooms').addClass('active_tab')
    $('.search_tab_meeting_rooms').removeClass('active_tab')
    $('.search_tab_airs').removeClass('active_tab')
    $('.cpy-search-by').attr('value', 'rooms')
    $('.search-hotels-wrapper').removeClass('any-time-airs-background')
    $('.search-hotels-wrapper').removeClass('any-time-meeting-rooms-background')
    $('.search-hotels-wrapper').addClass('any-time-rooms-background')

    $('.image-room-category').removeClass('meeting-rooms-background')
    $('.image-room-category').addClass('any-time-rooms-background')
    $('.describe-box-rooms').removeClass('hide')
    $('.describe-box-meeting-room').addClass('hide')
    $('.describe-box-air').addClass('hide')
    $('.describe-any-time').removeClass('hide')
    $('.describe-any-time-meeting_room').addClass('hide')
    $('.describe-any-time-air').addClass('hide')
  });

  $('.search_tab_meeting_rooms').on('click', function(){
    $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'inherit')
    $('.tab-content.tab-content-custom.any-time').css('display', 'none')
    $('.tab-content.tab-content-custom.any-time_air').css('display', 'none')
    $('.search_tab_airs').removeClass('active_tab')
    $('.search_tab_rooms').removeClass('active_tab')
    $('.search_tab_meeting_rooms').addClass('active_tab')
    $('.cpy-search-by').attr('value', 'meetingRoom')
    $('.search-hotels-wrapper').addClass('any-time-meeting-rooms-background')
    $('.search-hotels-wrapper').removeClass('any-time-rooms-background')
    $('.search-hotels-wrapper').removeClass('any-time-airs-background')

    $('.image-room-category').addClass('meeting-rooms-background')
    $('.image-room-category').removeClass('any-time-rooms-background')
    $('.image-room-category').removeClass('any-time-airs-background')

    $('.describe-box-rooms').addClass('hide')
    $('.describe-box-meeting-room').removeClass('hide')
    $('.describe-any-time').addClass('hide')
    $('.describe-any-time-meeting_room').removeClass('hide')
  });

  $('.search_tab_airs').on('click', function(){
    $('.tab-content.tab-content-custom.any-time_air').css('display', 'inherit')
    $('.tab-content.tab-content-custom.any-time').css('display', 'none')
    $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'none')
    $('.search_tab_rooms').removeClass('active_tab')
    $('.search_tab_meeting_rooms').removeClass('active_tab')
    $('.search_tab_airs').addClass('active_tab')
    $('.cpy-search-by').attr('value', 'airs')
    $('.search-hotels-wrapper').addClass('any-time-airs-background')
    $('.search-hotels-wrapper').removeClass('any-time-rooms-background')
    $('.search-hotels-wrapper').removeClass('any-time-meeting-rooms-background')

    $('.image-room-category').addClass('any-time-airs-background')
    $('.image-room-category').removeClass('meeting-rooms-background')
    $('.image-room-category').removeClass('any-time-rooms-background')

    $('.describe-box-rooms').addClass('hide')
    $('.describe-box-meeting-room').addClass('hide')
    $('.describe-any-time').addClass('hide')
    $('.describe-any-time-meeting_room').addClass('hide')
    $('.describe-any-time_air').removeClass('hide')
  });
});
