yojs.define("hq.public.bookings.payments.new", function() {
  $('html, body').animate({
    scrollTop: $(window).scrollTop() + $(".js-top-menu").height()
  });

  $('[data-toggle="billing-information-tooltip"]').tooltip();

  $('.cpy-credit-card-true-radio-button').click(function() {
    $('#ppplus').show()
  });
  $('.cpy-credit-card-false-radio-button').click(function() {
    $('#ppplus').hide()
  });

});
