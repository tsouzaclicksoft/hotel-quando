var Validate_modal = (function($) {

  var obj = {};

  var email_regex = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/ig

  var flashDelay = 2500
    , flashFadeOut = 200
    , html_city_message = ' \
      <div class="js-flash-message"> \
        <div class="alert alert-danger"> \
          <p>Cidade Inválida</p> \
        </div> \
      </div> \
    '
    , html_email_message = ' \
      <div class="js-flash-message"> \
        <div class="alert alert-danger"> \
          <p>E-mail Inválido</p> \
        </div> \
      </div> \
    '

  obj.init = function() {
    var $modal = $('.newsletter-modal')
      , $emailSent = $modal.find('.email-sent')
      , $form = $modal.find('.form')


    $form.find('.button.submit-newsletter').on('click', function() {
      var city = $('#js-autocomplete-newsletter-modal').val()
        , email = $('.email.no-padding #email').val()

      if (!email || !email_regex.test(email)) {
        $emailSent.find('span').css('display', 'none');
        $emailSent.append(html_email_message);
        $emailSent.find('.js-flash-message').delay(flashDelay).fadeOut(flashFadeOut, function() {
          $emailSent.css('display', 'none');
          $form.css('display', 'block');
        });
      }else if (!city) {
        console.log($('.email.no-padding #email').val().match(email_regex))
        $emailSent.find('span').css('display', 'none');
        $emailSent.append(html_city_message);
        $emailSent.find('.js-flash-message').delay(flashDelay).fadeOut(flashFadeOut, function() {
          $emailSent.css('display', 'none');
          $form.css('display', 'block');
        });
      }else{
        $emailSent.find('span').css('display', 'block');
      }
    });
  };

  return obj;

})(jQuery);
