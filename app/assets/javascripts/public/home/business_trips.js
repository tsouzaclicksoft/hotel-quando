yojs.define("hq.public.home.business_trips", function() {

  $(document).ready(function(){
    $(document).on("click", ".hire-now-btn", function () {
      booking_pack_id = $(this).data('id');
      $('#booking_pack_id').val(booking_pack_id);
      $('.js-hire-now-modal').modal();
    });

    //select sao paulo - sp
    $('.js-booking-pack-city').val(5389).change()
    $('.js-hotel-chosen-select').find('option:last').attr('selected','selected').change()
    hotel_selected = $('.js-hotel-chosen-select').find('option:last').text()
    $('.chosen-single > span').text(hotel_selected)
    $('.chosen-single').removeClass('chosen-default')
  });


  function sort_by_name(array){
    array.sort(function (a, b) {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
  }

  $('.js-booking-pack-city').change(function(){
    var optionsHtml = '<option value=""></option>';
    cities_hash = yojs.get("hq.admin.booking_packs.hashCityHotels")[$('.js-booking-pack-city').val()];
    if(cities_hash != ""){
      sort_by_name(cities_hash);
        cities_hash.forEach(function(hotel) {
        optionsHtml += '<option value="' + hotel.id + '">' + hotel.name + '</option>';
      });
    }else{
      optionsHtml = '<option value=""></option>'
    }
    
    $('.js-hotel-chosen-select').html(optionsHtml);
    $('.js-hotel-chosen-select').trigger("chosen:updated");
  })
  $('.js-hotel-chosen-select').chosen();
  if (!(yojs.isDefined('hq.admin.booking_pack.hotelSet'))){
    $('.js-booking-pack-city').change();
  }

  var locale = yojs.get('locale')
  if(locale == "pt-BR"){
    locale = ""
  }else{
    locale = "/en"
  }

  $(".js-hotel-chosen-select").change(function(){
    city_id = $('.js-booking-pack-city').val()
    hotel_id = $('.js-hotel-chosen-select').val()
    $.ajax({
      url: locale+'/get_booking_packs/'+hotel_id,
      method: 'get',
      success: function(result){


      },
      fail: function(){
        alert('Failed');
      },

      error: function() {
          alert('Error occured');
      },
      beforeSend: function (xhr, settings){
        $('.ajax-loader').show();
        $('.alert-info').text("");
        $('table').hide()
        $('.alert-info').hide()
      },
      complete: function(xhr, status) {
        
      }
    })
  })


});

