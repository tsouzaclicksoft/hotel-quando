var Validate_newsletter = (function($) {

  var obj = {}

  obj.init = function(autocomplete) {
    var $newsletter_bar = $('.newsletter-bar')
      , $form = $newsletter_bar.find('.form-newsletter')
      , $button = $form.find('.button.submit-newsletter')
      , $city = $form.find('#js-autocomplete-newsletter')
      , $email = $form.find('#email')

    var city_length = 0;

    $city.on('keydown', function() {
      city_length = 0
      validate()
    })

    autocomplete.addListener('place_changed', function() {
      city_length = $city.length
      validate()
    })

    $email.on('input', function() {
      validate()
    })

    validate()

    function validate () {
      if ( ($email.val().length > 0) && (city_length > 0) ) {
        $button.prop('disabled', false)
      } else {
        $button.prop('disabled', true)
      }
    }
  }
  return obj
})(jQuery)
