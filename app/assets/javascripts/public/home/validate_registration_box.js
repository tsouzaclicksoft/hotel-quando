var Validate_box = (function($) {

  var obj = {}

  obj.init = function(autocomplete) {
    var $newsletter_box = $('.registration_box')
        , $box_form = $newsletter_box.find('.form-box')
        , $box_button = $box_form.find('.airport_filter_btn.register_button')
        , $box_city = $box_form.find('#js-autocomplete-newsletter_box')
        , $box_email = $box_form.find('#email')
        , $box_country = $box_form.find('#country')

    var box_city_length = 0;

    $box_city.on('keydown', function() {
      box_city_length = 0
      validate_box()
    })

    autocomplete.addListener('place_changed', function() {
      box_city_length = $box_city.length
      validate_box()
    })

    $box_email.on('input', function() {
      validate_box()
    })

    validate_box()

    function validate_box () {
      if ( ($box_email.val().length > 0) && (box_city_length > 0) && ($box_country.val().length > 0) ) {
        $box_button.prop('disabled', false)
      } else {
        $box_button.prop('disabled', true)
      }
    }
  }
  return obj
})(jQuery)
