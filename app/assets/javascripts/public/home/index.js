yojs.define("hq.public.home.index", function() {
  if (!window.isMobileBrowser) {
    var s = skrollr.init({forceHeight: false});
  }

  $('.js-scroll-button').click(function() {
    // Getting the element to scroll to
    var target = $(this.hash);
    $('html, body').animate({
      // Subtracting 75px because of the header
      scrollTop: (target.offset().top - 75)
      // 1000 = time of the animation (1 second)
    }, 1000);
    // Returning false to prevent the default behaviour of the <a> tag
    return false;
  });

  $(".js-open-search-modal").click(function() {
    $('html,body').animate({ scrollTop: 0 }, 500);
    $(".js-headline-search").removeClass('shake animated').addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(this).removeClass('shake animated');
    });
  });

  $(".cpy-search-button-color").click(function(){
    console.log($(".cpy-address").val());
    $(".cpy-address").val() == '' ? $(".cpy-address").css('border', '1px solid red') : '';
    $(".cpy-checkin-date-calendar-input").val() == '' ? $(".cpy-checkin-date-calendar-input").css('border', '1px solid red') : '';
  });

  var autocomplete;
  var initialize = function() {
    autocomplete = new google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete-newsletter')),{ types: ['geocode'] }
    );
    google.maps.event.addListener(autocomplete, 'place_changed', function() {});
  }

  var autocomplete_box;
  var initialize_box = function() {
    autocomplete_box = new google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete-newsletter_box')),{ types: ['geocode'] }
    );
    google.maps.event.addListener(autocomplete, 'place_changed', function() {});
  }

  var autocomplete_modal;
  var initialize_modal = function() {
    autocomplete_modal = new google.maps.places.Autocomplete(
      (document.getElementById('js-autocomplete-newsletter-modal')),{ types: ['geocode'] }
    );
    google.maps.event.addListener(autocomplete_modal, 'place_changed', function() {});
  }




  $('.js-autocomplete-newsletter').onload = initialize();
  $('.js-autocomplete-newsletter-modal').onload = initialize_modal();
  $('js-autocomplete-newsletter_box').onload = initialize_box();
  Validate_modal.init();
  Validate_newsletter.init(autocomplete);
  Validate_box.init(autocomplete_box);
  $('.form-newsletter').submit(function(){
    $('.submit-newsletter').val($('.wait_text').val())
    $('.submit-newsletter').removeClass('submit-button')
    $('.submit-newsletter').addClass('wait-submit-button')
    $(this).submit(function() {
        return false;
    });
    return true;
  });

});
