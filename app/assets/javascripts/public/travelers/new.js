yojs.define("hq.public.company_details.cost_centers.travelers.new", function() {
  $('html, body').animate({
    scrollTop: $(window).scrollTop() + $(".js-top-menu").height()
  });

  $("#traveler_phone_number").intlTelInput({
    preferredCountries: ["br","us","es"],
    autoPlaceholder: 'off'
  });

	$("form").submit(function() {
		phoneNumberInput = $("#traveler_phone_number");
  	phoneNumberInput.val(phoneNumberInput.intlTelInput("getNumber"));
	});

  var countryData = $("#traveler_phone_number").intlTelInput("getSelectedCountryData");
  if (countryData.iso2 == "br"){
    $("#traveler_phone_number").mask('(99) 99999-9999');
  }

  $("#traveler_phone_number").on("countrychange", function(e, countryData) {
    var countryData = $("#traveler_phone_number").intlTelInput("getSelectedCountryData");
    if (countryData.iso2 == "br"){
      $("#traveler_phone_number").mask('(99) 99999-9999');
    }
    else{
      $("#traveler_phone_number").unmask();
    }
  });  
});
