yojs.define("hq.admin.promotional_codes.edit", function() {

	var dateFormat
		, language = ''

	if (yojs.get('locale') == 'pt-BR'){
		dateFormat = 'dd/mm/yy'
		language = 'pt-BR'
	}
	else if (yojs.get('locale') == 'en'){
		dateFormat = 'dd/mm/yy'
		language = 'en'
	}else{
		dateFormat = 'dd/mm/yy'
		language = 'es'
	}

		$('.js-date-picker').datepicker({
			autoclose:true,
			language: language,
			dateFormat: dateFormat,
			minDate: new Date()
		}).next().on(ace.click_event, function(){
			$(this).prev().focus();
		});


});
