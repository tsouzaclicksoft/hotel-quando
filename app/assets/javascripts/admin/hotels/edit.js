yojs.define("hq.admin.hotels.edit", function() {
  $('.js-hotel-state').change(function(){
    var optionsHtml = '<option value=""></option>';
    cities_hash = yojs.get("hq.admin.hotels.hashStateCities")[$('.js-hotel-state').val()];

    cities_hash.forEach(function(city) {
      optionsHtml += '<option value="' + city.id + '">' + city.name + '</option>';
    });
    $('.js-chosen-select').html(optionsHtml);
    $('.js-chosen-select').trigger("chosen:updated");
  })
  $('.js-chosen-select').chosen();
  if (!(yojs.isDefined('hq.admin.hotels.citySet'))){
    $('.js-hotel-state').change();
  }

  yojs.call("hq.admin.hotels.edit.configureOmnibeesForm");
});

yojs.define("hq.admin.hotels.edit.configureOmnibeesForm", function() {
  var allHours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];

  function authorizePack(pack, isAuthorized) {
    $('#hotel_omnibees_config_accepts_' + pack + 'h_pack').val(isAuthorized.toString());
  }

  function authorizeHours(pack, hours) {
    $('input[name="hotel[omnibees_config][accepted_hours_for_' + pack + 'h_pack][]"]').prop('checked', false)
    hours.forEach(function(hour) {
      $('input[name="hotel[omnibees_config][accepted_hours_for_' + pack + 'h_pack][]"][value="' + hour + '"]').prop('checked', true);
    });
  }

  $('.js-activate-full-pack-config').click(function() {
    authorizePack(3, true);
    authorizeHours(3, allHours);

    authorizePack(6, true);
    authorizeHours(6, allHours);

    authorizePack(9, true);
    authorizeHours(9, allHours);

    authorizePack(12, true);
    authorizeHours(12, allHours);
  });

  $('.js-activate-restrict-pack-config').click(function() {
    authorizePack(3, true);
    authorizeHours(3, [9, 10, 11, 12, 13, 14, 15]);

    authorizePack(6, true);
    authorizeHours(6, [9, 10, 11, 12] );

    authorizePack(9, true);
    authorizeHours(9, allHours);

    authorizePack(12, true);
    authorizeHours(12, allHours);
  });
});
