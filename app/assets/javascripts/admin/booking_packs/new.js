yojs.define("hq.admin.booking_packs.new", function() {

  function sort_by_name(array){
    array.sort(function (a, b) {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
  }

  $('.js-booking-pack-city').change(function(){
    var optionsHtml = '<option value=""></option>';
    cities_hash = yojs.get("hq.admin.booking_packs.hashCityHotels")[$('.js-booking-pack-city').val()];
    if(cities_hash != ""){
      sort_by_name(cities_hash);
    }
    cities_hash.forEach(function(hotel) {
      optionsHtml += '<option value="' + hotel.id + '">' + hotel.name + '</option>';
    });
    $('.js-hotel-chosen-select').html(optionsHtml);
    $('.js-room-type-chosen-select').html('<option value=""></option>');
    $('.js-hotel-chosen-select').trigger("chosen:updated");
  })
  $('.js-hotel-chosen-select').chosen();
  if (!(yojs.isDefined('hq.admin.booking_pack.hotelSet'))){
    $('.js-booking-pack-city').change();
  }

  $('.js-hotel-chosen-select').change(function(){
    var optionsHtml = '<option value=""></option>';
    hotels_hash = yojs.get("hq.admin.booking_packs.hashHotelRoomTypes")[$('.js-hotel-chosen-select').val()];

    if (hotels_hash != undefined){
      sort_by_name(hotels_hash);
      hotels_hash.forEach(function(room_type) {
        optionsHtml += '<option value="' + room_type.id + '">' + room_type.name_pt_br + '</option>';
      });
      $('.js-room-type-chosen-select').html(optionsHtml);
      $('.js-room-type-chosen-select').trigger("chosen:updated");
    }
    else {
      $('.js-room-type-chosen-select').html('<option value=""></option>');
      $('.js-room-type-chosen-select').trigger("chosen:updated");
    }
  })
  $('.js-room-type-chosen-select').chosen();
  if (!(yojs.isDefined('hq.admin.booking_pack.roomTypeSet'))){
    $('.js-hotel-chosen-select').change();
  }

});