yojs.define("hq.admin", function() {
  yojs.call("components.masks.init");
  yojs.call("components.directs3upload.configureDirectS3Upload");
  yojs.call("components.jqueryPlaceholder.init");
  yojs.call("hq.admin.configurePopover");
  yojs.call("hq.admin.configCurrencyMoneyDropdownMenu");
  if($(".browserIE").length > 0){
    $(".js-btn-scroll-up").remove();
  }
});

yojs.define("hq.admin.configurePopover", function(){
  $('.js-popover').popover({
    trigger: 'hover',
    html: true
  });
});


yojs.define("hq.admin.configCurrencyMoneyDropdownMenu", function(){
  $(".js-menu-currency-current").mouseenter(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-currency-current-dropdown-menu").slideDown('fast');
    }
  });

  $(".js-menu-currency-current").mouseleave(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-currency-current-dropdown-menu").stop(true, true).slideUp('fast');
    }
  });
});
