yojs.define("hq.public", function(){
  $('.js-chosen-select').chosen();
  yojs.call("components.masks.init");
  yojs.call("components.emailSuggestions.init");
  yojs.call("maskLocaleDate");
  yojs.call("showNewsletterModal");
  yojs.call("sendNewsletterModal");
  yojs.call("floatHeaderMenu");
  yojs.call("configDisabledButtonTooltip");
  yojs.call("putBorderBoxOnFlashMessage");
  yojs.call("configDatePicker");
  yojs.call("components.jqueryPlaceholder.init");
  yojs.call("setHeaderMenuMaxHeight");
  yojs.call("showCurrencyModal");
  yojs.call("enableSearchBarTabs");
})

yojs.define("sendNewsletterModal", function(){
  $('.form-newsletter-modal').submit(function(){
    $('.form').hide();
    $('.email-sent').show();
  })
})

yojs.define("showNewsletterModal", function(){
  if(yojs.get("firstVisit") == "true"){
    $('#newsletter-modal').modal();
  }
})

yojs.define("showCurrencyModal", function(){
  if(yojs.get("firstVisit") == "true"){
    $('#currency-modal').modal();
  }
})

yojs.define("maskLocaleDate", function(){
  if(yojs.get("locale") == "en"){
    $('.js-mask-date').attr('placeholder','mm/dd/yyyy');
  } else {
    $('.js-mask-date').attr('placeholder','dd/mm/aaaa');
  }
})

yojs.define("floatHeaderMenu", function(){
  $(window).scroll(function() {
    var top, marginTop;
    if(yojs.call("detectMobile")){
      top = 1;
      marginTop = "0"
    } else {
      top = 48;
      marginTop = "0"
    }

    if (yojs.call("floatHeaderMenu.scrollTop") > top) {
      if (!$(".js-float-menu").hasClass("floating")) {
        $(".js-float-menu").addClass("floating");
        $(".js-top-menu").addClass("taller");
        $(".content-body").css("marginTop", marginTop);
      }
    } else {
      if ($(".js-float-menu").hasClass("floating")) {
        $(".js-top-menu").removeClass("taller");
        $(".js-float-menu").removeClass("floating");
        $(".content-body").css("marginTop", "0");
      }
    }
  });
});

yojs.define("floatHeaderMenu.scrollTop", function(){
  var ScrollTop = document.body.scrollTop;
  if (ScrollTop == 0)
  {
    if (window.pageYOffset)
      ScrollTop = window.pageYOffset;
    else
      ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
  }
  return ScrollTop;
});

yojs.define("configDisabledButtonTooltip", function(){
  $('.js-tooltip-toggler').tooltip();
  $('.tooltip').tooltip('fixTitle');
});

yojs.define("putBorderBoxOnFlashMessage", function(){
  if($(".border-box").length > 0){
    $(".js-flash-message").addClass("border-box");
  }
});

yojs.define("detectMobile", function(){
  if( navigator.userAgent.match(/Android/i)
  || navigator.userAgent.match(/webOS/i)
  || navigator.userAgent.match(/iPhone/i)
  || navigator.userAgent.match(/iPad/i)
  || navigator.userAgent.match(/iPod/i)
  || navigator.userAgent.match(/BlackBerry/i)
  || navigator.userAgent.match(/Windows Phone/i)
  ){
    return true;
  }
  else {
    return false;
  }
});

yojs.define("setHeaderMenuMaxHeight", function(){
  $('.navbar-toggle').on('click', function(){
    var maxHeight = (window.innerHeight - 49).toString() + "px";
    $(".navbar-collapse").css("max-height", maxHeight);
  });
});

yojs.define("configDatePicker", function(){
  var locale = yojs.get("locale");
  //var dataFormat = (locale == "pt-BR" ? "dd/mm/yyyy" : "mm/dd/yyyy");
  var dataFormat = (locale == "en" ? "mm/dd/yyyy" : "dd/mm/yyyy")
  var todayDate = new Date();

  if(yojs.call("detectMobile")){
    $('.js-date-picker').attr('readonly','true');
  }

  $('.js-date-picker').datepicker({
    autoclose: true,
    todayHighlight: false,
    language: locale,
    format: dataFormat,
    startDate: todayDate.toLocaleDateString(locale)
  }).next().on(ace.click_event, function(){
    $(this).prev().focus();
  })

  $('#datepicker-checkout').datepicker()
  .on('changeDate', function(ev){
      $('#datepicker-checkout').datepicker('hide');
  });

  function disable_scroll() {
    $(document).bind('touchmove', function(e){e.preventDefault()});
  }

  function enable_scroll() {
    $(document).unbind('touchmove');
  }

  $('.js-date-picker').datepicker().on('show', function(e){
    $(".datepicker").css("z-index", "999");
    disable_scroll();
  });

  $('.js-date-picker').datepicker().on('hide', function(e){
    enable_scroll();
  });

  $('.js-search-date-picker').datepicker().on('show', function(e){
    $(".datepicker").css("z-index", "10059");
  });
});

yojs.define("enableSearchBarTabs", function(){
  $('.cpy-search-by').attr('value', 'rooms')
  $('.search_tab_rooms_mobile').addClass('active_mobile_tab')
  $('.search_tab_rooms_mobile').on('click', function(){
    $('.tab-content-metting-room-mobile').css('display', 'none')
    $('.tab-content-custom-mobile').css('display', 'inherit')
    $('.cpy-search-by').attr('value', 'rooms')
    $('.search_tab_rooms_mobile').addClass('active_mobile_tab')
    $('.search_tab_meeting_rooms_mobile').removeClass('active_mobile_tab')
  });

  $('.search_tab_meeting_rooms_mobile').on('click', function(){
    $('.tab-content-metting-room-mobile').css('display', 'inherit')
    $('.tab-content-custom-mobile').css('display', 'none')
    $('.cpy-search-by').attr('value', 'meetingRoom')
    $('.search_tab_rooms_mobile').removeClass('active_mobile_tab')
    $('.search_tab_meeting_rooms_mobile').addClass('active_mobile_tab')
  });
});
