yojs.define("hq.public.home.friend_hq", function() {

	var name = yojs.get("traduc_name");
	var email = yojs.get("traduc_email");
	var count = 1;

	$(".add-friend").click(function() {
		count += 1; 
		$( ".form-questions" ).append('<div class="row fields-to-add" style="display:none"><div class="col-md-6"><div class="input-group input-group-lg group-refer"><span class="input-group-addon fa fa-child" id="sizing-addon1"></span><input class="form-control" type="text" placeholder="'+name+'" aria-describedby="sizing-addon1" name="referred_friend_log[friend_name'+count.toString()+']"></div></div><div class="col-md-6"><div class="input-group input-group-lg group-refer"><span class="input-group-addon" id="sizing-addon1">@</span><input class="form-control" type="text" placeholder="'+email+'" aria-describedby="sizing-addon1" name="referred_friend_log[friend_email'+count.toString()+']"></div></div></div>').fadeIn('fast');
		$( ".form-questions" ).find(".fields-to-add:last").slideDown("fast");
	});

	$("#form_friend").submit(function() {
		$("#count").val(count+1);
	});

});
