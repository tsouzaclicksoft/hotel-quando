yojs.define("hq.executive_responsible.company_universes.all_companies", function() {
	var todayDate = new Date();
	var dateFormat
		, language = ''

	if (yojs.get('locale') == 'pt-BR'){
		dateFormat = 'dd/mm/yy'
		language = 'pt-BR'
	}
	else if (yojs.get('locale') == 'en'){
		dateFormat = 'mm/dd/yy'
		language = 'en'
	}else{
		dateFormat = 'dd/mm/yy'
		language = 'es'
	}

  $('.js-date-picker').datepicker({
  	autoclose:true,
  	language: language,
		dateFormat: dateFormat,
  	startDate: todayDate
  }).next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
});
