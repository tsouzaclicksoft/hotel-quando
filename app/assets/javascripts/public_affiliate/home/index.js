yojs.define("hq.public_affiliate.home.index", function() {
  if (!window.isMobileBrowser) {
    var s = skrollr.init({forceHeight: false});
  }

  $('.js-scroll-button').click(function() {
    // Getting the element to scroll to
    var target = $(this.hash);
    $('html, body').animate({
      // Subtracting 75px because of the header
      scrollTop: (target.offset().top - 75)
      // 1000 = time of the animation (1 second)
    }, 1000);
    // Returning false to prevent the default behaviour of the <a> tag
    return false;
  });

  $(".js-open-search-modal").click(function() {
    $('html,body').animate({ scrollTop: 0 }, 500);
    $(".js-headline-search").removeClass('shake animated').addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
      $(this).removeClass('shake animated');
    });
  });
  
  

  $(document).ready(function(){
    $(".how-it-works-btn").click(function(){
      if($('#toggle').is(":checked")){
        $('#toggle').prop('checked', false);
        $('.arrow-up-or-down').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        $('div.slideOutTab').stop(true, false)
              .animate({
                  left: '-75%'
              }, 400);
      }
      else{
        $('#toggle').prop('checked', true);
        $('.arrow-up-or-down').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        $('div.slideOutTab').stop(true, false)
              .animate({
                  left: 0
              }, 400);
       }    
    });
  });


});
