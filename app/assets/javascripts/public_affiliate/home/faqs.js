yojs.define("hq.public_affiliate.home.faq", function() {
  // Because accordion doesn't work on IE9
  if($(".browserIE9").length == 0){
    $.each($('ol:first>li'), function(index){
      $('ol:first>li:nth-child(' + (index + 1) + ') .accordion').attr({"href": ".answer" + (index + 1), "data-toggle": "collapse", "data-parent": "#accordion"})
      $('ol:first>li:nth-child(' + (index + 1) + ') .answer').attr({"class": "panel-collapse collapse answer" + (index + 1)})
    });
  }
});
