yojs.define("hq.public_affiliate", function(){
  $('.js-chosen-select').chosen();
  yojs.call("components.masks.init");
  yojs.call("components.bootbox.init");
  yojs.call("components.emailSuggestions.init");
  yojs.call("maskLocaleDate");
  yojs.call("configUserDropdownMenu");
  yojs.call("configCurrencyMoneyDropdownMenu");
  yojs.call("configDisabledButtonTooltip");
  yojs.call("putBorderBoxOnFlashMessage");
  yojs.call("configDatePicker");
  yojs.call("components.jqueryPlaceholder.init");
  yojs.call("floatHeaderSearchForm");
  yojs.call("enableSearchBarTabs");
})
yojs.define("maskLocaleDate", function(){
  if(yojs.get("locale") == "en"){
    $('.js-mask-date').attr('placeholder','mm/dd/yyyy');
  } else {
    $('.js-mask-date').attr('placeholder','dd/mm/aaaa');
  }
})

yojs.define("floatHeaderSearchForm", function(){
  $(window).scroll(function () {
    if (yojs.call("floatHeaderSearchForm.scrollTop") > 269) {
      if (!$(".js-float-search-form").hasClass("floating")) {
        $(".js-float-search-form").addClass("floating");
      }
    } else {
      if ($(".js-float-search-form").hasClass("floating")) {
        $(".js-float-search-form").removeClass("floating");
      }
    }
  });
});

yojs.define("floatHeaderSearchForm.scrollTop", function(){
  var ScrollTop = document.body.scrollTop;
  if (ScrollTop == 0)
  {
    if (window.pageYOffset)
      ScrollTop = window.pageYOffset;
    else
      ScrollTop = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
  }
  return ScrollTop;
});


yojs.define("configUserDropdownMenu", function(){
  $(".js-menu-wrapper").mouseenter(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-user-dropdown-menu").slideDown('fast');
    }
  });

  $(".js-menu-wrapper").mouseleave(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-user-dropdown-menu").stop(true, true).slideUp('fast');
    }
  })

});

yojs.define("configDisabledButtonTooltip", function(){
  $('.js-tooltip-toggler').tooltip();
  $('.tooltip').tooltip('fixTitle');
});

yojs.define("putBorderBoxOnFlashMessage", function(){
  if($(".border-box").length > 0){
    $(".js-flash-message").addClass("border-box");
  }
});

yojs.define("configDatePicker", function(){
  var locale = yojs.get("locale");
  var dataFormat = (locale != "en" ? "dd/mm/yyyy" : "mm/dd/yyyy")
  var todayDate = new Date();

  $('.js-date-picker').datepicker({
    autoclose: true,
    todayHighlight: false,
    language: locale,
    format: dataFormat,
    startDate: todayDate.toLocaleDateString(locale)
  }).next().on(ace.click_event, function(){
    $(this).prev().focus();
  })

  $('.js-date-picker').datepicker().on('show', function(e){
    $(".datepicker").css("z-index", "1");
  });

  $('.js-search-date-picker').datepicker().on('show', function(e){
    $(".datepicker").css("z-index", "10059");
  });

});

yojs.define("configCurrencyMoneyDropdownMenu", function(){
  $(".js-menu-currency-current").mouseenter(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-currency-current-dropdown-menu").slideDown('fast');
    }
  });

  $(".js-menu-currency-current").mouseleave(function(){
    if($(".modal.fade.in").length == 0){
      $(".js-currency-current-dropdown-menu").stop(true, true).slideUp('fast');
    }
  });
});

yojs.define("enableSearchBarTabs", function(){
  
  $('.search_tab_rooms').on('click', function(){
    $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'none')
    $('.tab-content.tab-content-custom.any-time').css('display', 'inherit')
    $('.search_tab_rooms').addClass('active_tab')
    $('.search_tab_meeting_rooms').removeClass('active_tab')
    $('.cpy-search-by').attr('value', 'rooms')
    $('.search-hotels-wrapper').removeClass('any-time-meeting-rooms-background')
    $('.search-hotels-wrapper').addClass('any-time-rooms-background')

    $('.image-room-category').removeClass('meeting-rooms-background')
    $('.image-room-category').addClass('any-time-rooms-background')
    $('.describe-box-rooms').removeClass('hide')
    $('.describe-box-meeting-room').addClass('hide')
    $('.describe-any-time').removeClass('hide')
    $('.describe-any-time-meeting_room').addClass('hide')
  });

  $('.search_tab_meeting_rooms').on('click', function(){
    $('.tab-content.tab-content-custom.any-time_meeting_room').css('display', 'inherit')
    $('.tab-content.tab-content-custom.any-time').css('display', 'none')
    $('.search_tab_rooms').removeClass('active_tab')
    $('.search_tab_meeting_rooms').addClass('active_tab')
    $('.cpy-search-by').attr('value', 'meetingRoom')
    $('.search-hotels-wrapper').addClass('any-time-meeting-rooms-background')
    $('.search-hotels-wrapper').removeClass('any-time-rooms-background')

    $('.image-room-category').addClass('meeting-rooms-background')
    $('.image-room-category').removeClass('any-time-rooms-background')
    $('.describe-box-rooms').addClass('hide')
    $('.describe-box-meeting-room').removeClass('hide')
    $('.describe-any-time').addClass('hide')
    $('.describe-any-time-meeting_room').removeClass('hide')
  });
});

