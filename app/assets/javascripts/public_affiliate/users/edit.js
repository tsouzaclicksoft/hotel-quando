yojs.define("hq.public_affiliate.users.edit", function() {
  $(".js-change-password-show-button").click(function() {
    $(".js-change-password-fields").fadeIn();
    $(".js-change-password-show-button").hide();
  });

  $('.form-group>span.help-block').addClass('col-sm-offset-3');
  $('.form-group>span.help-block').addClass('col-sm-12');
})

yojs.define("hq.public_affiliate.users.update", function() {
  yojs.call("hq.public_affiliate.users.edit");
})
