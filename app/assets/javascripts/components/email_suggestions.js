yojs.define("components.emailSuggestions.init", function() {
  // Defining the structure of the elements
  var emailSuggestionElements = '<p class="js-email-suggestion-wrapper text-right hidden">'
  try{
    if (yojs.get('locale') == 'pt-BR'){
      emailSuggestionElements += 'Você quis dizer ';
    }
    else{
      emailSuggestionElements += 'Did you mean ';
    }
  }
  catch(err){
    emailSuggestionElements += 'Você quis dizer ';
  }
  emailSuggestionElements += '<a class="js-email-suggestion" href="javascript:void(0);"></a>';
  emailSuggestionElements += '?';
  emailSuggestionElements += '</p>';
  // Inserting after the email input
  $(emailSuggestionElements).insertAfter('.js-email-with-suggestion');
  // Adding the suggestions to the input
  $('.js-email-with-suggestion').on('blur', function() {
    $(this).mailcheck({
      suggested: function(element, suggestion) {
        $('.js-email-suggestion-wrapper').removeClass('hidden');
        $('.js-email-suggestion').html(suggestion.full);
      },
      empty: function(element) {
        $('.js-email-suggestion-wrapper').addClass('hidden');
        $('.js-email-suggestion').html('');
      }
    });
  });

  // Adding the trigger to change the input value to the suggestion
  // and hiding the suggestion wrapper
  $('.js-email-suggestion').on('click', function(){
    $('.js-email-with-suggestion').val($('.js-email-suggestion').html());
    $('.js-email-suggestion-wrapper').addClass('hidden');
  });

})