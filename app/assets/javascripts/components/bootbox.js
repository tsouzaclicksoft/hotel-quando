yojs.define("components.bootbox.init", function() {
  var myCustomConfirmBox = function(title, confirm, cancel, message, callback) {
    var options;
    options = {
      message: message,
      title: title,
      buttons: {
        success: {
          label: confirm,
          className: "btn-success",
          callback: callback
        },
        danger: {
          label: cancel,
          className: "btn-danger"
        }
      }
    };

    return bootbox.dialog(options);
  };

  $.rails.allowAction = function(element) {
    var answer, callback, message, title, confirm, cancel;
    title = element.data("title");
    confirm = element.data("allow");
    cancel = element.data("cancel");
    message = element.data("confirm");
    if (!message) {
      return true;
    }
    answer = false;
    callback = void 0;
    if ($.rails.fire(element, "confirm")) {
      myCustomConfirmBox(title, confirm, cancel, message, function() {
        var oldAllowAction;
        callback = $.rails.fire(element, "confirm:complete", [answer]);
        if (callback) {
          oldAllowAction = $.rails.allowAction;
          $.rails.allowAction = function() {
            return true;
          };
          element.trigger("click");
          return $.rails.allowAction = oldAllowAction;
        }
      });
    }
    return false;
  };
})
