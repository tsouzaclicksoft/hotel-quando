$.extend($.gritter.options, {
  position: 'bottom-right',
  fade_in_speed: 'medium',
  fade_out_speed: 2000,
  time: 2000,
  class_name: 'gritter-info'
});
