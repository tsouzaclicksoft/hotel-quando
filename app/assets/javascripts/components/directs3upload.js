
yojs.define("components.directs3upload.configureDirectS3Upload", function() {
  yojs.set("components.directs3upload.current_uploads", 0);
  yojs.set("components.directs3upload.directUploadLastPicture", "");
  yojs.set("components.directs3upload.directUploadLastContextPicture", "");

  $('.js-s3-uploader').each(function() {
    var currentUploader = $(this);
    currentUploader.S3Uploader({
      allow_multiple_files: false,
      remove_completed_progress_bar: true,
      progress_bar_target: $(this).find('.js_upload_bar_container'),
      before_add: function(content) {
        var currentUploads = yojs.get("components.directs3upload.current_uploads");
        yojs.set("components.directs3upload.current_uploads", currentUploads + 1);
        return true;
      }
    });
  });

  $('.js-s3-uploader').bind('s3_upload_complete', function(e, content) {
    var currentUploads = yojs.get("components.directs3upload.current_uploads");
    var directUploadInupt = $(".js-modal-add-media .js-direct-upload-url");
    if (directUploadInupt.length == 0) {
      directUploadInupt = $(".js-direct-upload-url");
    }
    yojs.set("components.directs3upload.current_uploads", currentUploads - 1);
    $(this).find(".js_file_name").html(content.filename);
    $(this).find(".js_image_preview").attr('src', content.url);
    directUploadInupt.val(content.url);
    directUploadInupt.trigger('input');
  });

  $('.js-s3-uploader').bind('s3_upload_failed', function(e, content) {
    var currentUploads = yojs.get("components.directs3upload.current_uploads");
    yojs.set("components.directs3upload.current_uploads", currentUploads - 1);
    return alert('Sorry, an error ocurred while uploading: ' + content.filename + ". Please try again.");
  });

});

yojs.define("components.directs3upload.confirmFormUploadSubmit", function() {
  var currentUploads = yojs.get("components.directs3upload.current_uploads");
  if (currentUploads > 0) {
    return confirm("Your file upload hasn't finished yed. Do you want do proceed?");
  } else {
    return true;
  }
});
