yojs.define("components.masks.init", function() {
  // $('_campo_').data('mask').getCleanVal(); retorna o valor do campo sem a mascara aplicada, exceto para maskMoney
  // Para maskMoney, o comando para retornar o valor sem a mascara é $('campo').maskMoney('unmasked')[0]
  $('.js-mask-integer-until-100').mask('999');
  $('.js-mask-integer-until-1000').mask('9999');
  $('.js-mask-cep').mask('99999-999');
  $('.js-mask-cpf').mask('999.999.999-99');
  $('.js-mask-cnpj').mask('99.999.999/9999-99');
  $('.js-mask-mobile-phone').mask('(99) 99999-9999');
  $('.js-mask-mobile-phone').attr('placeholder','(99) 99999-9999');
  $('.js-mask-mobile-phone-with-country').mask('+99 (99) 99999-9999');
  $('.js-mask-mobile-phone-with-country-without-length').mask('+99 99999999999999');
  $('.js-mask-fixed-phone').mask('(99) 9999-9999');
  $('.js-mask-cnh').mask('99999999999');
  $('.js-mask-date').mask('99/99/9999');
  $('.js-mask-date').attr('placeholder','dd/mm/aaaa');
  $('.js-mask-date-time').mask('99/99/9999 99:99');
  $('.js-mask-date-time').attr('placeholder','dd/mm/aaaa HH:MM');
  $('.js-mask-time').mask('99:99');
  $('.js-mask-time').attr('placeholder','HH:MM');
  $('.js-mask-plate').mask('SSS-0000');
  $('.js-mask-integer').mask("0#",{maxlength: false})
  $('.js-mask-year').mask('y999', {'translation': {y: {pattern: /[12]/}}});
  $('.js-mask-time').mask('h9:m9', {'translation': {h: {pattern: /[012]/}, m: {pattern: /[012345]/}}});
  $('.js-mask-time').attr('placeholder','hh:mm');
  $(".js-mask-money").maskMoney({prefix: 'R$ ', allowNegative: false, allowZero: true, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
  $(".js-mask-money-dollar").maskMoney({prefix: 'US$ ', allowNegative: false, allowZero: true, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
  $(".js-mask-money-without-prefix").maskMoney({allowNegative: false, allowZero: true, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
  $(".js-mask-money").maskMoney('mask');
  $(".js-mask-float-percent").maskMoney({suffix:' %', allowNegative: false, allowZero: true, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
  $(".js-mask-float-percent").maskMoney('mask');
  $(".js-mask-float-percent-1-decimal-point").maskMoney({suffix:' %', precision: 1, allowNegative: false, allowZero: true, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
  $(".js-mask-float-percent-1-decimal-point").maskMoney('mask');
  $(".js-mask-float-percent-1-decimal-point-no-symbol").maskMoney({precision: 1, allowNegative: false, allowZero: true, defaultZero: true, thousands: '.', decimal: '.' , affixesStay : true});
  $(".js-mask-float-percent-1-decimal-point-no-symbol").maskMoney('mask');
  $(".js-mask-float-km-distance").maskMoney({suffix:' KM', allowNegative: false, allowZero: true, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
  $(".js-mask-float-km-distance").maskMoney('mask');
  $('.js-mask-credit-card-number').mask('9999 9999 9999 9999');
  $('.js-mask-credit-card-security-code').mask('999');
  $(".js-mask-float-without-unit").maskMoney({allowNegative: false, allowZero: true, defaultZero: true, thousands: '.', decimal: ',' , affixesStay : true});
  $(".js-mask-float-without-unit").maskMoney('mask');
});
