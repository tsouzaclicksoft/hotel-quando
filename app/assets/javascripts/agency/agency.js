yojs.define("hq.agency", function() {
  yojs.call("components.masks.init");
  yojs.call("components.bootbox.init");

  $('.js-chosen-select').chosen();

  if($(".browserIE").length > 0){
    $(".js-btn-scroll-up").remove();
  }

  $('body.browserIE input.js-date-picker').removeClass('form-control');
})
