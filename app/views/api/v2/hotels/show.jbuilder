if @search_exact_match.empty?
	@hotels = []
else
	@hotels = Array.new([@search_exact_match.first[:min_offer].hotel])
	@is_omnibees = !@hotels.first.omnibees_code.blank?
end

json.hotels @hotels do |hotel|
	json.hotel do
		json.extract! hotel, :id, :name, :email, :minimum_hours_of_notice, :category, :latitude, :longitude, :cep, :street, :number, :complement, :state, :description_pt_br, :description_en, :description_es, :payment_method_pt_br, :payment_method_en, :payment_method_es
		json.city do
			json.extract! hotel.city, :name
			json.state do
				json.extract! hotel.city.state, :initials, :name
			end
		end
		photos = hotel.photos
		if photos.empty?
			json.photos []
		else
			json.photos photos do |photo|
				json.photo do
					json.extract! photo, :id, :direct_image_url
				end
			end
		end
		json.hotel_amenity do
			json.extract! hotel.hotel_amenity, :breakfast_included, :free_internet, :internet_pt_br, :internet_en, :internet_es, :park_pt_br, :park_en, :park_es, :pool, :rooftop,
				:garden, :terrace, :sauna, :gym, :restaurant, :bar, :reception_24_hour, :luggage_room, :laundry, :room_service,
				:wake_up_service, :transfer_offered_by_the_hotel_pt_br, :transfer_offered_by_the_hotel_en, :transfer_offered_by_the_hotel_es, :gratuity_for_children_pt_br,
				:gratuity_for_children_en, :gratuity_for_children_es

			json.breakfast_included allow_breakfast_in_this_time_interval?(hotel.hotel_amenity, params[:checkin_hour], params[:length_of_pack]).to_s
		end
	end
end

json.room_types @search_exact_match do |offer|
	json.room_type do
		json.extract! offer[:room_type], :id, :name_pt_br, :name_en, :name_es, :description_pt_br, :description_en, :description_es, :initial_capacity, :maximum_capacity, :hotel_id, :single_bed_quantity, :double_bed_quantity
=begin
		if @is_omnibees
			json.description_pt_br 	Hash.from_xml(offer[:min_offer].room_type_xml)["RoomType"]["RoomDescription"]["Description"]
			json.description_en 	Hash.from_xml(offer[:min_offer].room_type_xml)["RoomType"]["RoomDescription"]["Description"]
			json.description_es 	Hash.from_xml(offer[:min_offer].room_type_xml)["RoomType"]["RoomDescription"]["Description"]
		else
			json.description_pt_br 	offer[:min_offer].room_type.description_pt_br
			json.description_en 	offer[:min_offer].room_type.description_en
			json.description_es 	offer[:min_offer].room_type.description_es
		end
=end
		if offer[:min_offer].room_type.single_bed_quantity == 1
			json.bed_configuration "1 #{t(:single_bed)}"
		elsif offer[:min_offer].room_type.single_bed_quantity > 1
			json.bed_configuration "#{offer[:min_offer].room_type.single_bed_quantity} #{t(:single_beds)}"
		end

		if offer[:min_offer].room_type.double_bed_quantity == 1
			json.bed_configuration "1 #{t(:double_bed)}"
		elsif offer[:min_offer].room_type.double_bed_quantity > 1
			json.bed_configuration "#{offer[:min_offer].room_type.double_bed_quantity} #{t(:double_beds)}"
		end

		json.offer do
			json.id offer[:min_offer].id
			json.price calculate_currency(total_price_for_number_of_people(@by_number_of_people, offer[:min_offer]), Hotel::CURRENCY_SYMBOL_REVERSED[offer[:min_offer].hotel.currency_symbol]).round
                  json.price_without_currency total_price_for_number_of_people(@by_number_of_people, offer[:min_offer])
			#json.room_id offer[:min_offer].room_id
			json.checkin_timestamp offer[:min_offer].checkin_timestamp
			json.checkout_timestamp offer[:min_offer].checkout_timestamp
			json.pack_in_hours offer[:min_offer].pack_in_hours
			#json.status offer[:min_offer].status
			json.room_type_initial_capacity offer[:min_offer].room_type_initial_capacity
			json.room_type_maximum_capacity offer[:min_offer].room_type_maximum_capacity
			json.no_show_value calculate_currency(offer[:min_offer].no_show_value, Hotel::CURRENCY_SYMBOL_REVERSED[offer[:min_offer].hotel.currency_symbol]).round
			json.room_type_id offer[:min_offer].room_type_id
			json.hotel_id offer[:min_offer].hotel_id
			#json.log_id offer[:min_offer].log_id
			#json.checkin_timestamp_day_of_week offer[:min_offer].checkin_timestamp_day_of_week
			json.extra_price_per_person calculate_currency(offer[:min_offer].extra_price_per_person, Hotel::CURRENCY_SYMBOL_REVERSED[offer[:min_offer].hotel.currency_symbol]).round
		end
=begin
		photos = hotel.photos
		if photos.empty?
			json.photos []
		else
			json.photos photos do |photo|
				json.photo do
					json.extract! photo, :id, :direct_image_url
				end
			end
		end
=end
		rt_photos = offer[:min_offer].room_type.photos
		if rt_photos.empty?
			json.photos []
		else
			#@rt_photos = Array.new([rt_photos])
			json.photos rt_photos do |photo|
				json.photo do
					json.extract! photo, :id, :title_es, :title_en, :title_pt_br, :direct_image_url, :file_file_name, :file_updated_at, :is_main
				end
			end
		end
	end
end
