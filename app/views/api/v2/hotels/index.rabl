#collection @search
object false
child(@hotel_exact => :hotel_exacts) do
	attributes :id, :name, :category, :minimum_hours_of_notice, :description_pt_br, :description_en, :description_es, :payment_method_pt_br, :payment_method_en, :payment_method_es, :email, :latitude, :longitude, :state, :cep, :street, :number, :complement
	
	node :breakfast_included do |hotel|
		allow_breakfast_in_this_time_interval?(hotel.hotel_amenity, params[:by_checkin_hour], params[:by_pack_length])
	end

	child :city do
		attribute :name
		child :state do
			attributes :initials, :name
		end
	end
	node :photo do |hotel|
		photo = Photo.where(hotel_id: hotel.id).where(is_main: true).where(processing_status: "processed").first
		if photo.blank?
			photo = Photo.where(hotel_id: hotel.id).where(processing_status: "processed")[0]
		end
		unless photo.nil?
			img_url = photo.direct_image_url
		end
		{ :photo => img_url }
	end
#	node(:distance) { @distance.first }
end

child @min_offer_exact => 'offer_exacts' do
	attributes :id, :checkin_timestamp, :checkout_timestamp, :pack_in_hours, :price, :status, :room_type_initial_capacity,
		:room_type_maximum_capacity, :no_show_value, :room_id, :room_type_id, :hotel_id, :log_id, :checkin_timestamp_day_of_week,
		:extra_price_per_person

      node :price do |offer|
        calculate_currency(total_price_for_number_of_people(@by_number_of_people, offer), Hotel::CURRENCY_SYMBOL_REVERSED[offer.hotel.currency_symbol]).round
      end
      node :extra_price_per_person do |offer|
        calculate_currency(offer.extra_price_per_person, Hotel::CURRENCY_SYMBOL_REVERSED[offer.hotel.currency_symbol]).round
      end
      node :no_show_value do |offer|
        calculate_currency(offer.no_show_value, Hotel::CURRENCY_SYMBOL_REVERSED[offer.hotel.currency_symbol]).round
      end
end

node(:distance_exacts) { |d| @distance_exact }



#child @hotel_similar => 'hotel_similars' do
#	attributes :id, :name, :category, :minimum_hours_of_notice, :description_pt_br, :description_en, :description_es, :payment_method_pt_br, :payment_method_en, :payment_method_es, :email, :latitude, :longitude, :state, :cep, :street, :number, :complement
#	child :city do
#		attribute :name
#		child :state do
#			attributes :initials, :name
#		end
#	end
#	node :photo do |hotel|
#		photo = Photo.where(hotel_id: hotel.id).where(is_main: true).where(processing_status: "processed").first
#		if photo.blank?
#			photo = Photo.where(hotel_id: hotel.id).where(processing_status: "processed")[0]
#		end
#		unless photo.nil?
#			img_url = photo.direct_image_url
#		end
#		{ :photo => img_url }
#	end
#end
#
#child @min_offer_similar => 'offer_similars' do
#	attributes :id, :checkin_timestamp, :checkout_timestamp, :pack_in_hours, :price, :status, :room_type_initial_capacity,
#		:room_type_maximum_capacity, :no_show_value, :room_id, :room_type_id, :hotel_id, :log_id, :checkin_timestamp_day_of_week,
#		:extra_price_per_person
#end
#
#node(:distance_similars) { |d| @distance_similar }
#
