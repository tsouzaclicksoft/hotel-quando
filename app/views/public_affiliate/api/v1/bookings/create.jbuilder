json.msg @msg
json.currency current_currency.code
json.currency_sign current_currency.money_sign
json.booking do
	json.extract! @booking, :id, :status, :guest_name, :note

	json.hotel do
		json.extract! @booking.hotel, :id, :category, :name, :state, :city, :cep, :street, :number, :complement, :latitude, :longitude
		if @booking.hotel.omnibees_code.blank?
			json.description_pt_br @booking.hotel.description_pt_br
			json.description_en @booking.hotel.description_en
                  json.description_es @booking.hotel.description_es
		else
			json.hotel_description Hash.from_xml(@booking.omnibees_offer.room_type_xml)["RoomType"]["RoomDescription"]["Description"]
		end
        json.cancel_permitted @booking.hotel.accepts_booking_cancellation?
        json.minimun_hours_to_cancel @booking.hotel.minimum_hours_of_notice
        json.accept_payment_at_hotel !(OnlinePayment.booking_accept?(@booking))

		json.room_type do
			if @booking.is_meeting_offer?
				json.extract! @booking.meeting_room_offer.meeting_room, :id, :name_pt_br, :name_en, :name_es, :description_pt_br, :description_en, :description_es, :meeting_room_maximum_capacity, :booking_name_pt_br, :booking_name_en
			elsif @booking.is_event_offer?
				json.extract! @booking.event_room_offer.event_room, :id, :name_pt_br, :name_en, :name_es, :description_pt_br, :description_en, :description_es, :event_room_maximum_capacity, :booking_name_pt_br, :booking_name_en
			else
				json.extract! @booking.offer.room_type, :id, :name_pt_br, :name_en, :name_es, :description_pt_br, :description_en, :description_es, :initial_capacity, :maximum_capacity, :single_bed_quantity, :double_bed_quantity, :booking_name_en
			end

			if @booking.offer.room_type.photos.empty?
				json.photos ""
			else
				json.photos @booking.offer.room_type.photos do |photo|
					json.extract! photo, :id, :title_en, :title_pt_br, :title_es, :direct_image_url, :file_file_name, :file_updated_at, :is_main
				end
			end
		end
		if @booking.hotel.photos.empty?
			json.photos ""
		else
			json.photos @booking.hotel.photos do |photo|
				json.extract! photo, :id, :title_en, :title_pt_br, :title_es, :direct_image_url, :file_file_name, :file_updated_at, :is_main
			end
		end

	end
end

#json.t(:personal_informations)
json.personal_informations do
    #json.t('bookings.guest_name') @booking.guest_name
    json.guest_name @booking.guest_name
    #json.t(:observations) @booking.note
    json.observations @booking.note
end

#json.t(:booking_informations)
json.booking_informations do
	#json.t(:pack_size) "#{@booking.pack_in_hours}h"
	json.pack_size "#{@booking.pack_in_hours}h"

    if @booking.is_meeting_offer?
        #json.t(:meeting_room_name) @booking.business_room.send("name_#{I18n.locale}")
        json.meeting_room_name @booking.business_room.send("name_#{I18n.locale}")
    elsif @booking.is_event_offer?
    	#json.t(:event_room_name) @booking.business_room.send("name_#{I18n.locale}")
    	json.event_room_name @booking.business_room.send("name_#{I18n.locale}")
    else
    	#json.t(:room_type) @booking.room_type.send("full_name_#{I18n.locale}")
    	json.room_type @booking.room_type.send("full_name_#{I18n.locale}")
    end
end

offer_class = @booking.offer.class.name.constantize
#json.t(:schedule) do
json.schedule do
	#json.t(:checkin) do
	json.check_in do
        if @booking.is_omnibees_or_hoteisnet?
        	#json.t(:date_word) "#{l((@booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute).to_date)}"
        	json.date_word "#{l((@booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute).to_date)}"
        else
        	json.date_word "#{l((@booking.date_interval.first + offer_class::ROOM_CLEANING_TIME - 1.minute).to_date)}"
        end
        if @booking.is_omnibees_or_hoteisnet?
        	#json.t(:hour) "#{(@booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute).strftime("%H:%M")}"
        	json.hour "#{(@booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute).strftime("%H:%M")}"
        else
        	json.hour "#{(@booking.date_interval.first + offer_class::ROOM_CLEANING_TIME - 1.minute).strftime("%H:%M")}"
        end
    end

    #json.t(:checkout) do
    json.check_out do
        if @booking.is_omnibees_or_hoteisnet?
        	json.date_word "#{l((@booking.date_interval.last - Offer::ROOM_CLEANING_TIME + 1.minute).to_date)}"
        else
        	json.date_word "#{l((@booking.date_interval.last - offer_class::ROOM_CLEANING_TIME + 1.minute).to_date)}"
        end
    	if @booking.is_omnibees_or_hoteisnet?
        	json.hour "#{(@booking.date_interval.last - Offer::ROOM_CLEANING_TIME + 1.minute).strftime("%H:%M")}"
        else
        	json.hour "#{(@booking.date_interval.last - offer_class::ROOM_CLEANING_TIME + 1.minute).strftime("%H:%M")}"
    	end
    end

    json.prices do
        json.initial_price calculate_currency(@booking.initial_price, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.offer.hotel.currency_symbol]).round(2)
        if !@booking.is_meeting_offer? && !@booking.is_event_offer?
          json.value_for_extra_people calculate_currency((@booking.offer.extra_price_per_person * (@booking.number_of_people - 1) ), Hotel::CURRENCY_SYMBOL_REVERSED[@booking.offer.hotel.currency_symbol]).round(2)
        end
      json.booking_tax calculate_currency(@booking.booking_tax, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.offer.hotel.currency_symbol]).round(2)
        json.total_value calculate_currency(@booking.total_price, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.offer.hotel.currency_symbol]).round(2)
    end
end

json.status do
    json.booking_creation_date_and_hour l @booking.created_at
    if (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_without_card])
    	json.booking_confirmation_date_and_hour l @booking.updated_at

        json.confirmation Booking::ACCEPTED_STATUS_REVERSED[@booking.status].to_s
    end
    json.checkout_delay @booking.delayed_checkout_flag
end
=begin
	if !@booking.is_meeting_offer? && !@booking.is_event_offer?
    	options_for_number_of_people_select = []
    	@booking.offer.room_type_maximum_capacity.times do | i |
    	options_for_number_of_people_select << {value: (i + 1)}
=end
