json.currency current_currency.code
json.currency_sign current_currency.money_sign
json.offer do
	json.extract! @booking.offer, :id, :checkin_timestamp, :checkout_timestamp, :pack_in_hours, :room_type_id
	json.price calculate_currency(@booking.initial_price, Hotel::CURRENCY_SYMBOL_REVERSED[@offer.hotel.currency_symbol]).round(2)
	json.taxes calculate_currency(@booking.booking_tax_to_affiliates, Hotel::CURRENCY_SYMBOL_REVERSED[@offer.hotel.currency_symbol]).round(2)

	json.number_of_people @booking.number_of_people
	json.choose_number_of_person (@offer.room_type_initial_capacity..@offer.room_type_maximum_capacity).to_a do |i|
    	next if @booking.is_omnibees_or_hoteisnet? and i != @booking.number_of_people
    	json.quantity_of_people i
    	json.price_by_quantity_of_people calculate_currency(total_price_for_number_of_people(i), Hotel::CURRENCY_SYMBOL_REVERSED[@offer.hotel.currency_symbol]).round(2)
    end
	#, :hotel_availabilities_xml, :room_rate_xml, :room_type_xml, :rate_plan_xml
	json.hotel do
		json.extract! @booking.offer.hotel, :id, :name, :category, :latitude, :longitude, :cep, :street, :number, :complement, :state
		json.city @booking.offer.hotel.city.name
		if @booking.offer.hotel.omnibees_code.blank?
			json.description_pt_br @booking.offer.hotel.description_pt_br
			json.description_en @booking.offer.hotel.description_en
                  json.description_es @booking.offer.hotel.description_es
		else
			json.hotel_description Hash.from_xml(@booking.offer.room_type_xml)["RoomType"]["RoomDescription"]["Description"]
		end

		json.room_type do
			json.description_of_the_room @offer.room_type.send('name_' + I18n.locale.to_s)

			if @offer.room_type.single_bed_quantity == 1
				json.simple_bed_configuration "1 #{t(:single_bed)}"
			elsif @offer.room_type.single_bed_quantity > 1
				json.simple_bed_configuration "#{@offer.room_type.single_bed_quantity} #{t(:single_beds)}"
			end

			if @offer.room_type.double_bed_quantity == 1
				json.double_bed_configuration "1 #{t(:double_bed)}"
			elsif @offer.room_type.double_bed_quantity > 1
				json.double_bed_configuration "#{@offer.room_type.double_bed_quantity} #{t(:double_beds)}"
			end
		end
	end
end
