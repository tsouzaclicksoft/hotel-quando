json.total @bookings.count
json.hotels @bookings do |booking|
  json.extract! booking, :id, :status, :guest_name, :note
  json.self_url public_affiliate_api_v1_booking_path(booking)
end
