json.currencies @currencies do |currency|
  json.extract! currency, :name, :money_sign, :code
end
