json.total @rooms.count
json.hotels @rooms do |room|
  json.extract! room, :id, :number
  json.self_url public_affiliate_api_v1_hotel_room_path(room.hotel, room)
  json.hotel_url public_affiliate_api_v1_hotel_path(room.hotel)
end
