json.hotel_group_name @hotel_group.first.name

json.hotels @hotels do |hotel|
    json.extract! hotel, :id, :name, :category, :contact_name, :contact_phone, :cnpj, :minimum_hours_of_notice,
     :description_pt_br, :description_en, :payment_method_pt_br, :payment_method_en, :cep, :street, :number, :complement,
     :iss_in_percentage, :accept_online_payment, :timezone, :hotel_chain_id, :service_tax

    json.city hotel.city.name
    json.state hotel.city.state.name
    json.country_pt_bt hotel.city.state.country.name_pt_br
    json.country_en hotel.city.state.country.name_en
    json.country_es hotel.city.state.country.name_es
end



