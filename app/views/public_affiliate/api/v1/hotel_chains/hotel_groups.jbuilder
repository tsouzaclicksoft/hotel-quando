json.hotel_groups @hotel_groups do |hotel_group|
  json.extract! hotel_group, :id, :name
end
