object :total_hotels
child(@hotels) do
  attributes :id, :name, :category, :street, :country, :latitude, :longitude

  node do |hotel|
    node :accepts_3h_pack do
      hotel.omnibees_config.accepts_3h_pack
    end


    node :accepts_6h_pack do
      hotel.omnibees_config.accepts_6h_pack
    end


    node :accepts_9h_pack do
      hotel.omnibees_config.accepts_9h_pack
    end

    node :minimum_hours_for_cancellation do
      hotel.minimum_hours_of_notice
    end

    node :city do
      hotel.city.name
    end

    node :state do
      hotel.city.state.initials
    end

    node :currency do
      Hotel.const_get('CURRENCY_SYMBOL_REVERSED')[hotel.currency_symbol]
    end

    node :accepts_booking_cancellation do
      hotel.accepts_booking_cancellation?
    end
  end
end
