object @offers
attributes :checkin_timestamp, :checkout_timestamp,
            :pack_in_hours, :room_type_maximum_capacity, :id

node :current_currency do
  @currency_code
end

node :have_breakfast do |offer|
  if allow_breakfast_in_this_time_interval?(offer.hotel.hotel_amenity, offer.checkin_timestamp.hour, offer.pack_in_hours)
    true
  else
    false
  end
end

node :price do |offer|
  calculate_currency(offer.price, Hotel::CURRENCY_SYMBOL_REVERSED[offer.hotel.currency_symbol]).round(2)
end

node :no_show_value do |offer|
  calculate_currency(offer.no_show_value, Hotel::CURRENCY_SYMBOL_REVERSED[offer.hotel.currency_symbol]).round(2)
end

node :extra_price_per_person do |offer|
  calculate_currency(offer.extra_price_per_person, Hotel::CURRENCY_SYMBOL_REVERSED[offer.hotel.currency_symbol]).round(2)
end

child :hotel do
  attributes :name, :category
  node :minimum_hours_for_cancellation do |hotel|
    hotel.minimum_hours_of_notice
  end
  node :accepts_booking_cancellation do |hotel|
    hotel.accepts_booking_cancellation?
  end
  node :currency do |hotel|
    Hotel.const_get('CURRENCY_SYMBOL_REVERSED')[hotel.currency_symbol]
  end
end
