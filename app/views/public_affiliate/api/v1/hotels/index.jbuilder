json.total_hotels @search_exact_match.count
json.currency current_currency.code
json.currency_sign current_currency.money_sign
json.hotels @search_exact_match do |hotel|
  json.extract! hotel[:hotel], :id, :name, :category, :latitude, :longitude, :cep, :street, :number, :complement, :state, :contact_phone
  json.cancel_permitted hotel[:hotel].accepts_booking_cancellation?
  json.minimun_hours_to_cancel hotel[:hotel].minimum_hours_of_notice
  json.accept_payment_at_hotel !(OnlinePayment.hotel_accept?(hotel[:hotel]))
  json.city hotel[:hotel].city.name
  json.min_offer_price calculate_currency(hotel[:min_offer].price, Hotel::CURRENCY_SYMBOL_REVERSED[hotel[:min_offer].hotel.currency_symbol]).round(2)
  #json.hotel_availabilities Hash.from_xml(hotel[:min_offer].hotel_availabilities_xml, :to_json)
  json.distance hotel[:distance]
	if hotel[:hotel].photos.empty?
		json.photos do
			json.title_en "No image to display"
			json.title_pt_br "Não há imagens para exibir"
                  json.title_es "Não hay image"
			json.direct_image_url "https://www.hotelquando.com/paperclip_images/big/processing.png"
		end
	else
		json.photos hotel[:hotel].photos do |photo|
			json.extract! photo, :title_en, :title_pt_br, :title_es, :direct_image_url if photo.is_main?
		end
	end

  json.timezone do
    json.name get_timezone(hotel[:hotel].latitude, hotel[:hotel].longitude)[:timezone_name]
    json.offset_in_hours get_timezone(hotel[:hotel].latitude, hotel[:hotel].longitude)[:offset_in_hours]
    json.offset_in_seconds get_timezone(hotel[:hotel].latitude, hotel[:hotel].longitude)[:offset_in_seconds]
  end
end
