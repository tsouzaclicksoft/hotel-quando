json.total_offers @search_exact_match.count
json.currency current_currency.code
json.currency_sign current_currency.money_sign
json.hotel do
	json.extract! @search_exact_match.first[:min_offer].hotel, :id, :name, :category, :latitude, :longitude, :cep, :street, :number, :complement, :state, :contact_phone
	json.city @search_exact_match.first[:min_offer].hotel.city.name
	json.cancel_permitted @search_exact_match.first[:min_offer].hotel.accepts_booking_cancellation?
	json.minimun_hours_to_cancel @search_exact_match.first[:min_offer].hotel.minimum_hours_of_notice
	json.accept_payment_at_hotel !(OnlinePayment.hotel_accept?(@search_exact_match.first[:min_offer].hotel))

	if @search_exact_match.first[:min_offer].hotel.omnibees_code.blank?
		json.description_pt_br @search_exact_match.first[:min_offer].hotel.description_pt_br
		json.description_en @search_exact_match.first[:min_offer].hotel.description_en
            json.description_es @search_exact_match.first[:min_offer].hotel.description_es
	else
		json.description_pt_br Hash.from_xml(@search_exact_match.first[:min_offer].room_type_xml)["RoomType"]["RoomDescription"]["Description"]
		json.description_en Hash.from_xml(@search_exact_match.first[:min_offer].room_type_xml)["RoomType"]["RoomDescription"]["Description"]
            json.description_es Hash.from_xml(@search_exact_match.first[:min_offer].room_type_xml)["RoomType"]["RoomDescription"]["Description"]
	end

	json.offers @search_exact_match do |offer|
		json.extract! offer[:min_offer], :id
		json.price calculate_currency(offer[:min_offer].price, Hotel::CURRENCY_SYMBOL_REVERSED[offer[:min_offer].hotel.currency_symbol]).round(2)
            booking = Booking.new(number_of_people: params[:number_of_people])
            booking.offer = offer[:min_offer]
            json.taxes calculate_currency(booking.booking_tax_to_affiliates, Hotel::CURRENCY_SYMBOL_REVERSED[offer[:min_offer].hotel.currency_symbol]).round(2)
            if allow_breakfast_in_this_time_interval?(offer[:min_offer].hotel.hotel_amenity, params[:by_checkin_hour], params[:by_pack_length])
              json.have_breakfast true
            else
              json.have_breakfast false
            end
		json.room_type do
			json.extract! offer[:min_offer].room_type, :id, :name_pt_br, :name_en, :name_es, :description_pt_br, :description_en, :description_es, :initial_capacity, :maximum_capacity, :single_bed_quantity, :double_bed_quantity, :booking_name_pt_br, :booking_name_en, :booking_name_es
			if offer[:min_offer].room_type.photos.empty?
				json.photos ""
			else
				json.photos offer[:min_offer].room_type.photos do |photo|
					json.extract! photo, :id, :title_en, :title_pt_br, :title_es, :direct_image_url, :file_file_name, :file_updated_at, :is_main
				end
			end
		end
		#json.rooms_url public_affiliate_api_v1_hotel_rooms_path(offer[:min_offer])
	end
	json.available_offers @room_type_available_offers.each do |room_type, offers|
		json.room_type_id room_type
		json.offers_count offers.count
	end

	if @search_exact_match.first[:min_offer].hotel.photos.empty?
		json.photos ""
	else
		json.photos @search_exact_match.first[:min_offer].hotel.photos do |photo|
			json.extract! photo, :id, :title_en, :title_pt_br, :title_es, :direct_image_url, :file_file_name, :file_updated_at, :is_main
		end
	end

  json.timezone do
    json.name get_timezone( @search_exact_match.first[:min_offer].hotel.latitude, @search_exact_match.first[:min_offer].hotel.longitude)[:timezone_name]
    json.offset_in_hours get_timezone( @search_exact_match.first[:min_offer].hotel.latitude, @search_exact_match.first[:min_offer].hotel.longitude)[:offset_in_hours]
    json.offset_in_seconds get_timezone( @search_exact_match.first[:min_offer].hotel.latitude, @search_exact_match.first[:min_offer].hotel.longitude)[:offset_in_seconds]
  end
end
