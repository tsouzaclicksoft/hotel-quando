object @hotel
attributes :id, :name, :category, :street, :country, :latitude, :longitude

node :accepts_3h_pack do |hotel|
  hotel.omnibees_config.accepts_3h_pack
end


node :accepts_6h_pack do |hotel|
  hotel.omnibees_config.accepts_6h_pack
end


node :accepts_9h_pack do |hotel|
  hotel.omnibees_config.accepts_9h_pack
end

node :minimum_hours_for_cancellation do |hotel|
  hotel.minimum_hours_of_notice
end

node :city do |hotel|
  hotel.city.name
end

node :state do |hotel|
  hotel.city.state.initials
end

node :currency do |hotel|
  Hotel.const_get('CURRENCY_SYMBOL_REVERSED')[hotel.currency_symbol]
end
node :accepts_booking_cancellation do |hotel|
  hotel.accepts_booking_cancellation?
end
