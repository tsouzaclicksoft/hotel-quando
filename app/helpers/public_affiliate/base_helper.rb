# encoding: utf-8

module PublicAffiliate::BaseHelper
  include Shared::BaseHelper
  include PublicAffiliate::HotelStarsHelper
  include PublicAffiliate::HotelHelper
  include PublicAffiliate::UserHelper

  def display_flash_message
    return if flash.empty?
    alerts = flash.inject('') do |content, (key, message)|
      next unless ["success", "notice", "error", "danger", "warning", "info"].include?(key)
      key = "success" if key == "notice"
      key = "danger" if key == "error"

      alert = content_tag :div, :class => "alert alert-#{key}" do
        content_tag(:p, message) unless message.blank?  || key == "timedout"
      end.html_safe
      content << alert.html_safe
    end
    alerts.html_safe if alerts
  end

  def pagination_links_for(resource_list, param_name = 'page', custom_params = {})
    paginate(resource_list, param_name: "#{param_name}", params: custom_params, :theme => 'public_template')
  end

  def pagination_ajax_links_for(resource_list, param_name = 'page', custom_params = {})
    paginate(resource_list, param_name: "#{param_name}", params: custom_params, :theme => 'public_template', remote: true)
  end

  def layout_name
    session[:affiliate].layout_name
  end

  def multiplus_point(price)
    comission = session[:affiliate].comission_value
    points = comission / 0.0456
    points.round(0).to_i
  end
 
  def allow_breakfast_in_this_time_interval?(hotel_amenity, checkin_hour, pack_in_hours)
    # allow breakfast if checkin or checkout is between 6:00 and 10:00 hours
    checkout_hour = checkin_hour.to_i + pack_in_hours.to_i
    if checkout_hour >= 24
      checkout_hour -= 24
    end
    if ((6..10).include? checkin_hour.to_i ) || ((6..10).include? checkout_hour)
      case pack_in_hours.to_i
      when 3
        return true if hotel_amenity.breakfast_included_for_pack_3h
      when 6
        return true if hotel_amenity.breakfast_included_for_pack_6h
      when 9
        return true if hotel_amenity.breakfast_included_for_pack_9h
      when 12
        return true if hotel_amenity.breakfast_included_for_pack_12h
      else
        return false
      end
    else
      return false
    end
  end
    
end
