module PublicAffiliate::UserHelper

  def can_current_public_affiliate_user_afford?(options = {})
    if current_public_affiliate_user.present?
      #return current_public_user.can_afford?(options)
      return true
    else
      return true
    end
  end

  def can_show_payment_button?(booking)
    return true if (booking.checkin_date >= Date.current) && (booking.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) && can_current_public_affiliate_user_afford?(offer: booking.offer, number_of_people: booking.number_of_people)
    return false
  end
end
