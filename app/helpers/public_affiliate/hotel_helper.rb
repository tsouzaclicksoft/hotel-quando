module PublicAffiliate::HotelHelper
	def can_show_uber_benefits?
		 last_search_log.city_name == "São Paulo" ||
		 last_search_log.city_name == "Rio de Janeiro" ||
		 last_search_log.city_name == "Belo Horizonte" ||
		 last_search_log.city_name == "Brasília"
	end

	def convert_checkin_hour_to_am_pm(checkin_date, checkin_hour)
		date = checkin_date.split('/')
		time = Time.new(date[2], date[1], date[0], checkin_hour.to_i)
		if I18n.locale == 'en' || I18n.locale == :en
			return time.strftime("%I %p")
		else
			return time.strftime("%H:%M")
		end
	end

	def calculate_cancelation_date(checkin_date, checkin_hour, hours_of_notice=12)
		if I18n.locale == 'en' || I18n.locale == :en
			(checkin_date + checkin_hour - hours_of_notice.hours).strftime("%I %p")
		else
			l(checkin_date + checkin_hour - hours_of_notice.hours)
		end
	end
end
