module Hotel::EventRoomOffersHelper
  module ArrayHelpers
    def map_status_per_room_type (status, room_type_id)
      self.map{ |offer| offer if offer.status == status && offer.room_type_id == room_type_id }.compact
    end

    def counting_per_day(day)
      self.map do |offer|
        offer.number_of_rooms if offer.checkin_timestamp.day == day + 1
      end.compact.inject(0) do |sum, n|
        sum = sum+n
      end
    end
  end

  Array.send(:include, ArrayHelpers)

  module OfferHelpers
    def room_type_and_room
      "#{self.room_type.full_name_pt_br} | #{self.room.number}".html_safe
    end
  end

  EventRoomOffer.send(:include, OfferHelpers)


  def event_rooms_json
    event_rooms = []
    EventRoom.by_hotel_id(current_hotel_hotel.id).each do | event_room |
      if event_room.present?
        event_rooms << event_room
      end
    end
    event_rooms = event_rooms.map do | event_room |
      { id: event_room.id, name_pt_br: event_room.name_pt_br }
    end
    event_rooms.to_json
  end

  def pricing_policies_with_event_room_id_json
    pricing_policies = {}
    EventRoomPricingPolicy.by_hotel_id(current_hotel_hotel.id).includes(:business_room_prices).each do | policy |
      event_room_ids_array = []
      policy.business_room_prices.each do | business_room_price |
        unless business_room_price.all_prices_are_zero?
          event_room_ids_array << business_room_price.business_room_id
        end
      end
      pricing_policies[policy.id] = { id: policy.id, name: policy.name, eventRoomsId: event_room_ids_array }
    end
    pricing_policies.to_json
  end

  def get_selected_pricing_policy_from_flash_json
    selected_pricing_policy_object = EventRoomPricingPolicy.find(flash[:pricing_policy_id].to_i)
    event_rooms_ids_array = []
    selected_pricing_policy_object.business_room_prices.each do | business_room_price |
      unless business_room_price.all_prices_are_zero?
        event_rooms_ids_array << business_room_price.business_room_id
      end
    end
    selected_pricing_policy = { id: selected_pricing_policy_object.id, name: selected_pricing_policy_object.name, eventRoomsId: event_rooms_ids_array }
    selected_pricing_policy.to_json
  end

end
