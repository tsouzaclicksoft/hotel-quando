module Hotel::BusinessRoomsHelper
	# Returns a dynamic path based on the provided parameters
	def sti_business_path(type, obj = nil, action = nil)
	  send "#{format_sti(action, type, obj)}_path", obj
	end

	def format_sti(action, type, obj)
	  action || obj ? "#{format_action(action)}#{type.underscore}" : "hotel_#{type.underscore.pluralize}"
	end

	def format_action(action)
	  action ? "#{action}_hotel_" : "hotel_"
	end
end