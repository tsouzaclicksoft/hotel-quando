module Hotel::CheckinDatesHelper

  def prepare_checkin_dates_from_params(params_hash)
    checkin_dates = params_hash['initial_date'].to_date.upto(params_hash['final_date'].to_date).to_a
    checkin_dates.delete_if do | date |
      should_delete = true
      params_hash['selected_days_hash'].each do | _ , day_of_week |
        if should_delete
          should_delete = !date.send(day_of_week.to_s + '?')
        end
      end
      should_delete
    end
    checkin_dates.map(&:to_date)
  end

end
