# encoding: utf-8

module Hotel::BaseHelper
  include CodusTemplates::Helpers::AceAdmin
  include Shared::CodusHelper
  include Shared::FormHelper
  include Hotel::CheckinDatesHelper

  def menu_active_for(type, action = nil, type_room = nil)
    active_in_controller = ["hotel/#{type}"].include? params[:controller]
    if type_room && params[:type]
      active_in_controller = type_room.eql? params[:type]
    end
    if action.nil?
      active_in_controller ? "active" : ''
    else
      if action.is_a? Array
        active_in_action = action.map(&:to_s).include? params[:action]
      else
        active_in_action = (params[:action] == action.to_s)
      end
      (active_in_controller && active_in_action) ? "active" : ''
    end
  end

  def authorized?
    current_admin_manager.present?
  end

  def show_panel_buttons_section(custom_options = {})
    options = {
      back_button_name: get_translation_for_button(:back),
      edit_button_name: get_translation_for_button(:edit)
    }.merge(custom_options)

    buttons = content_tag(:div, class: 'wizard-actions') do
      if options[:path_to_return]
        back_link = link_to("<i class='icon-arrow-left'></i> #{options[:back_button_name]}".html_safe, options[:path_to_return].to_sym, {class: 'btn btn-prev'})
      else
        begin
          back_link = link_to("<i class='icon-arrow-left'></i> #{options[:back_button_name]}".html_safe, {action: 'index'}, {class: 'btn btn-prev'})
        rescue
          back_link = link_to("<i class='icon-arrow-left'></i> #{options[:back_button_name]}".html_safe, :back, {class: 'btn btn-prev'})
        end
      end
      begin
        edit_link = link_to("<i class='icon-edit'></i> #{options[:edit_button_name]}".html_safe, {action: 'edit'}, {class: 'btn btn-info'})
      rescue
        edit_link = ''
      end

      begin
        edit_link = link_to("<i class='icon-edit'></i> #{options[:edit_button_name]}".html_safe, {action: 'edit'}, {class: 'btn btn-info'})
      rescue
        edit_link = ''
      end
      if actions = options[:action_to_hide_button]
        if actions.kind_of?(Array)
          actions = options[:action_to_hide_button].map { |x| x.to_s }
          edit_link = '' if actions.include?('edit')
          back_link = '' if actions.include?('back')
        else
          actions.to_s.eql?('edit') ? (edit_link = '') : (back_link = '')
        end
      end
      back_link.html_safe + edit_link.html_safe
    end
    ("<hr/>".html_safe + buttons)
  end
end
