module Hotel::OffersHelper
  module ArrayHelpers
    def map_status_per_room_type (status, room_type_id)
      self.map{ |offer| offer if offer.status == status && offer.room_type_id == room_type_id }.compact
    end

    def counting_per_day(day)
      self.map do |offer|
        offer.number_of_rooms if offer.checkin_timestamp.day == day + 1
      end.compact.inject(0) do |sum, n|
        sum = sum+n
      end
    end
  end

  Array.send(:include, ArrayHelpers)

  module OfferHelpers
    def room_type_and_room
      "#{self.room_type.full_name_pt_br} | #{self.room.number}".html_safe
    end
  end

  Offer.send(:include, OfferHelpers)


  def room_types_with_active_rooms_json
    room_types = []
    RoomType.includes(:active_rooms).by_hotel_id(current_hotel_hotel.id).includes(:active_rooms).each do | room_type |
      if room_type.active_rooms.size > 0
        room_types << room_type
      end
    end
    room_types = room_types.map do | room_type |
      { id: room_type.id, name_pt_br: room_type.full_name_pt_br, active_rooms: room_type.active_rooms.as_json }
    end
    room_types.to_json
  end

  def pricing_policies_with_room_types_id_json
    pricing_policies = {}
    PricingPolicy.by_hotel_id(current_hotel_hotel.id).includes(:room_type_pricing_policies).each do | policy |
      room_types_ids_array = []
      policy.room_type_pricing_policies.each do | room_type_pricing_policy |
        unless room_type_pricing_policy.all_prices_are_zero?
          room_types_ids_array << room_type_pricing_policy.room_type_id
        end
      end
      pricing_policies[policy.id] = { id: policy.id, name: policy.name, roomTypesId: room_types_ids_array }
    end
    pricing_policies.to_json
  end

  def get_selected_pricing_policy_from_flash_json
    selected_pricing_policy_object = PricingPolicy.find(flash[:pricing_policy_id].to_i)
    room_types_ids_array = []
    selected_pricing_policy_object.room_type_pricing_policies.each do | room_type_pricing_policy |
      unless room_type_pricing_policy.all_prices_are_zero?
        room_types_ids_array << room_type_pricing_policy.room_type_id
      end
    end
    selected_pricing_policy = { id: selected_pricing_policy_object.id, name: selected_pricing_policy_object.name, roomTypesId: room_types_ids_array }
    selected_pricing_policy.to_json
  end

end
