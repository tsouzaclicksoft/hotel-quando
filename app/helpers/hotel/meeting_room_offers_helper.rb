module Hotel::MeetingRoomOffersHelper
  module ArrayHelpers
    def map_status_per_room_type (status, room_type_id)
      self.map{ |offer| offer if offer.status == status && offer.room_type_id == room_type_id }.compact
    end

    def counting_per_day(day)
      self.map do |offer|
        offer.number_of_rooms if offer.checkin_timestamp.day == day + 1
      end.compact.inject(0) do |sum, n|
        sum = sum+n
      end
    end
  end

  Array.send(:include, ArrayHelpers)

  module OfferHelpers
    def room_type_and_room
      "#{self.room_type.full_name_pt_br} | #{self.room.number}".html_safe
    end
  end

  MeetingRoomOffer.send(:include, OfferHelpers)


  def meeting_rooms_json
    meeting_rooms = []
    MeetingRoom.by_hotel_id(current_hotel_hotel.id).each do | meeting_room |
      if meeting_room.present?
        meeting_rooms << meeting_room
      end
    end
    meeting_rooms = meeting_rooms.map do | meeting_room |
      { id: meeting_room.id, name_pt_br: meeting_room.name_pt_br }
    end
    meeting_rooms.to_json
  end

  def pricing_policies_with_meeting_room_id_json
    pricing_policies = {}
    MeetingRoomPricingPolicy.by_hotel_id(current_hotel_hotel.id).includes(:business_room_prices).each do | policy |
      meeting_room_ids_array = []
      policy.business_room_prices.each do | business_room_price |
        unless business_room_price.all_prices_are_zero?
          meeting_room_ids_array << business_room_price.business_room_id
        end
      end
      pricing_policies[policy.id] = { id: policy.id, name: policy.name, meetingRoomsId: meeting_room_ids_array }
    end
    pricing_policies.to_json
  end

  def get_selected_pricing_policy_from_flash_json
    selected_pricing_policy_object = MeetingRoomPricingPolicy.find(flash[:pricing_policy_id].to_i)
    meeting_rooms_ids_array = []
    selected_pricing_policy_object.business_room_prices.each do | business_room_price |
      unless business_room_price.all_prices_are_zero?
        meeting_rooms_ids_array << business_room_price.business_room_id
      end
    end
    selected_pricing_policy = { id: selected_pricing_policy_object.id, name: selected_pricing_policy_object.name, meetingRoomsId: meeting_rooms_ids_array }
    selected_pricing_policy.to_json
  end

end
