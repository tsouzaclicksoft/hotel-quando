module Hotel::PricingPoliciesHelper

  def recover_money_from_params(room_type, hours)
    index_of_room_type_pricing_policy = nil
    begin
      if params.try(:[],:pricing_policy).try(:[], :room_type_pricing_policies_attributes)
        self.params[:pricing_policy][:room_type_pricing_policies_attributes].each do |key,value|
          if value[:room_type_id] == room_type.id.to_s
            index_of_room_type_pricing_policy = key
          end
        end
      end
      if index_of_room_type_pricing_policy
        (self.params.try(:[], "pricing_policy")
                   .try(:[], "room_type_pricing_policies_attributes")
                   .try(:[], index_of_room_type_pricing_policy)
                   .try(:[], "pack_price_#{hours}") || 0)*10
      end
    rescue
      return 0
    end
  end

  def recover_money_from_params_for_extra_person( room_type, hours)
    index_of_room_type_pricing_policy = nil
    begin
      if params.try(:[],:pricing_policy).try(:[], :room_type_pricing_policies_attributes)
        self.params[:pricing_policy][:room_type_pricing_policies_attributes].each do |key,value|
          if value[:room_type_id] == room_type.id.to_s
            index_of_room_type_pricing_policy = key
          end
        end
      end
      if index_of_room_type_pricing_policy
        (self.params.try(:[], "pricing_policy")
                   .try(:[], "room_type_pricing_policies_attributes")
                   .try(:[], index_of_room_type_pricing_policy)
                   .try(:[], "extra_price_per_person_for_pack_price_#{hours}") || 0)*10
      end
    rescue
      return 0
    end
  end

end
