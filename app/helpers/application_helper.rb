module ApplicationHelper
  include CodusTemplates::Helpers::AceAdmin

  def show_meta_tags_according_to_page(action, controller=nil)
    if action == 'index_airport' || action == 'airports'
      '/layouts/public/meta_tags_airports'
    elsif action == 'index_city' || action == 'cities'
      '/layouts/public/meta_tags_cities'
    elsif action == 'business_trips'
      '/layouts/public/meta_tags_corporate'
    elsif action == 'be_our_partner'
      '/layouts/public/meta_tags_partners'
    elsif action == 'show' && controller == 'public/hotel'
      '/layouts/public/meta_tags_hotel'
    else
      '/layouts/public/meta_tags'
    end
  end
end
