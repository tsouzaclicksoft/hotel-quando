# encoding: utf-8

module Agency::BaseHelper
  include CodusTemplates::Helpers::AceAdmin
  include Shared::CodusHelper
  include Shared::FormHelper

  def menu_active_for(type, action = nil)
    active_in_controller = ["agency/#{type}"].include? params[:controller]
    if action.nil?
      active_in_controller ? "active" : ''
    else
      if action.is_a? Array
        active_in_action = action.map(&:to_s).include? params[:action]
      else
        active_in_action = (params[:action] == action.to_s)
      end
      (active_in_controller && active_in_action) ? "active" : ''
    end
  end
end
