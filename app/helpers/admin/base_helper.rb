# encoding: utf-8

module Admin::BaseHelper
  include CodusTemplates::Helpers::AceAdmin
  include Shared::CodusHelper
  include Shared::FormHelper

  def menu_active_for(type, action = nil)
    active_in_controller = ["admin/#{type}"].include? params[:controller]
    if action.nil?
      active_in_controller ? "active" : ''
    else
      if action.is_a? Array
        active_in_action = action.map(&:to_s).include? params[:action]
      else
        active_in_action = (params[:action] == action.to_s)
      end
      (active_in_controller && active_in_action) ? "active" : ''
    end
  end

  def hotel_names_for_ids(selected_hotels_ids_arrays)
    @hotels_ids_and_names ||= Hotel.pluck(:id, :name)
    @hotels_ids_and_names.select do |hotel_array|
      selected_hotels_ids_arrays.include?(hotel_array[0]) # 0 is the id
    end.map { |hotel_array| hotel_array[1] }
  end

  def checked_packs(resource, area)
    resource.avaiable_for_packs.nil? ? false : resource.avaiable_for_packs.match(area)
  end

  def checked_days_of_week(resource, area)
    resource.avaiable_for_days_of_the_week.nil? ? false : resource.avaiable_for_days_of_the_week.match(area)
  end

  def show_panel_buttons_section(custom_options = {})
    options = {
      back_button_name: get_translation_for_button(:back),
      edit_button_name: get_translation_for_button(:edit)
    }.merge(custom_options)
    

    buttons = content_tag(:div, class: 'wizard-actions') do
      if options[:path_to_return]
        back_link = link_to("<i class='icon-arrow-left'></i> #{options[:back_button_name]}".html_safe, options[:path_to_return].to_sym, {class: 'btn btn-prev'})
      else
        begin
          back_link = link_to("<i class='icon-arrow-left'></i> #{options[:back_button_name]}".html_safe, {action: 'index'}, {class: 'btn btn-prev'})
        rescue
          back_link = link_to("<i class='icon-arrow-left'></i> #{options[:back_button_name]}".html_safe, :back, {class: 'btn btn-prev'})
        end
      end
      begin
        edit_link = link_to("<i class='icon-edit'></i> #{options[:edit_button_name]}".html_safe, {action: 'edit'}, {class: 'btn btn-info'})
      rescue
        edit_link = ''
      end

      begin
        edit_link = link_to("<i class='icon-edit'></i> #{options[:edit_button_name]}".html_safe, {action: 'edit'}, {class: 'btn btn-info'})
      rescue
        edit_link = ''
      end

      if actions = options[:action_to_hide_button]
        if actions.kind_of?(Array)
          actions = options[:action_to_hide_button].map { |x| x.to_s }
          edit_link = '' if actions.include?('edit')
          back_link = '' if actions.include?('back')
        else
          actions.to_s.eql?('edit') ? (edit_link = '') : (back_link = '')
        end
      end
      back_link.html_safe + edit_link.html_safe
    end
    ("<hr/>".html_safe + buttons)
  end  

  def hotel_locale(hotel)
    User::ACCEPTED_LOCALES.collect{ | locale | locale[1] if locale[0] == hotel.locale}.compact.first
  end

  def is_active?(place)
    place.active ? t('show_for.yes') : t('show_for.no')
  end
end
