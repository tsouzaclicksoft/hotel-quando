module Public::HotelHelper
	def can_show_uber_benefits?
		 last_search_log.city_name == "São Paulo" ||
		 last_search_log.city_name == "Rio de Janeiro" ||
		 last_search_log.city_name == "Belo Horizonte" ||
		 last_search_log.city_name == "Brasília"
	end

	def cancelation_allowed?(checkin_date, checkin_hour, hours_of_notice=12)
		(checkin_date + checkin_hour - hours_of_notice.hours > DateTime.now)
	end

	def calculate_cancelation_date(checkin_date, checkin_hour, hours_of_notice=12)
		if I18n.locale != :pt_br
			(checkin_date + checkin_hour - hours_of_notice.hours).strftime("%I %p")
		else
			l(checkin_date + checkin_hour - hours_of_notice.hours)
		end
	end

	def convert_checkin_hour_to_am_pm(checkin_date, checkin_hour)
		date = checkin_date.split('/')
		time = Time.new(date[2], date[1], date[0], checkin_hour.to_i)
		if I18n.locale != :pt_br
			return time.strftime("%I %p")
		else
			return time.strftime("%H:%M")
		end
	end

	def hotel_includes_breakfast?(hotel, pack_in_hour)
    case pack_in_hour.present?
    when pack_in_hour == 3 then hotel.hotel_amenity.breakfast_included_for_pack_3h
    when pack_in_hour == 6 then hotel.hotel_amenity.breakfast_included_for_pack_6h
    when pack_in_hour == 9 then hotel.hotel_amenity.breakfast_included_for_pack_9h
    when pack_in_hour == 12 then hotel.hotel_amenity.breakfast_included_for_pack_12h
    else false
    end
  end

	def hotel_on_request_box_button
		text = if I18n.locale == :en
							'This hotel is ON REQUEST'
						elsif I18n.locale == :pt_br
							'Este hotel é SOB SOLICITAÇÃO'
						else
							'Este hotel es SOB SOLICITUD'
						end
		content_tag :a, id: 'booking_request_tooltip'  do
			content_tag :div, class: 'on_request_box' do
				content_tag(:p, text) + content_tag(:i, '', class: 'glyphicon glyphicon-question-sign big', style: 'margin: 25px 78%; position: absolute; color: #fff;')
			end
		end
	end

	def hotel_on_request_explanation
		explanation = if I18n.locale == :en
										'You request the reservation and the hotel may or may not accept it. We will inform you on time via email.'
									elsif I18n.locale == :pt_br
										'Você solicita a reserva e o hotel pode aceitar ou não. Nós vamos lhe informar em tempo por e-mail.'
									else
										'Usted solicita la reserva y el hotel puede aceptar o no. Nosotros le informaremos en tiempo por correo electrónico'
									end
		content_tag(:p, explanation, class: 'hide', id: 'booking_request_tooltip_explanation')
	end

	def hotel_on_request_button_text(hotel)
		text = if I18n.locale == :en
						'Request reservation'
					elsif I18n.locale == :pt_br
						'Solicitar reserva'
					else
						'Solicitar reserva'
					end
		hotel.booking_on_request ? text : t(:reserve)
	end

	def show_meta_tags_according_to_page(action, controller=nil)
		if action == 'index_airport' || action == 'airports'
			'/layouts/public/meta_tags_airports'
		elsif action == 'index_city' || action == 'cities'
			'/layouts/public/meta_tags_cities'
		elsif action == 'business_trips'
			'/layouts/public/meta_tags_corporate'
		elsif action == 'be_our_partner'
			'/layouts/public/meta_tags_partners'
		elsif action == 'show' && controller == 'public/hotel'
			'/layouts/public/meta_tags_hotel'
		else
			'/layouts/public/meta_tags'
		end
	end
end
