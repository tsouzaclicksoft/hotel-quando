module Public::Bookings::PaymentsHelper

  def hotel_charges_service_tax?(hotel)
    (hotel.has_attribute?(:service_tax) && !(hotel.service_tax.nil? || hotel.service_tax == 0))
  end
end
