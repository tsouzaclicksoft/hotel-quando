module Public::HotelStarsHelper

  def render_stars_from_hotel(hotel)
    output = ''
    number_of_empty_stars = 5

    hotel.category[0].to_i.times do
      output = "#{output}<i class='glyphicon glyphicon-star'></i>"
      number_of_empty_stars -= 1
    end

    number_of_empty_stars.times do
      output = "#{output}<i class='glyphicon glyphicon-star-empty'></i>"
    end

    output.html_safe
  end

  def render_stars_from_hotel_mobile(hotel)
    output = "#{hotel.category[0].to_i}<i class='glyphicon glyphicon-star'></i>"

    output.html_safe
  end

end
