# encoding: utf-8

module Public::BaseHelper
  include Shared::BaseHelper
  include Shared::MobileDeviceHelper
  include Public::HotelStarsHelper
  include Public::HotelHelper
  include Public::UserHelper

  def display_flash_message
    return if flash.empty?
    alerts = flash.inject('') do |content, (key, message)|
      content = '' if content == nil
      next unless ["success", "notice", "error", "danger", "warning", "info"].include?(key)
      key = "success" if key == "notice"
      key = "danger" if key == "error"

      alert = content_tag :div, :class => "alert alert-#{key}" do
        content_tag(:p, message) unless message.blank?  || key == "timedout"
      end.html_safe
      content << alert.html_safe
    end
    alerts.html_safe if alerts
  end

  def pagination_links_for(resource_list, param_name = 'page', custom_params = {})
    paginate(resource_list, param_name: "#{param_name}", params: custom_params, :theme => 'public_template')
  end

  def pagination_ajax_links_for(resource_list, param_name = 'page', custom_params = {})
    paginate(resource_list, param_name: "#{param_name}", params: custom_params, :theme => 'public_template', remote: true)
  end

  def allow_breakfast_in_this_time_interval?(hotel_amenity, checkin_hour, pack_in_hours)
    # allow breakfast if checkin or checkout is between 6:00 and 10:00 hours
    checkout_hour = checkin_hour.to_i + pack_in_hours.to_i
    if checkout_hour >= 24
      checkout_hour -= 24
    end
    if ((6..10).include? checkin_hour.to_i ) || ((6..10).include? checkout_hour)
      case pack_in_hours.to_i
      when 3
        return true if hotel_amenity.breakfast_included_for_pack_3h
      when 6
        return true if hotel_amenity.breakfast_included_for_pack_6h
      when 9
        return true if hotel_amenity.breakfast_included_for_pack_9h
      when 12
        return true if hotel_amenity.breakfast_included_for_pack_12h
      else
        return false
      end
    else
      return false
    end
  end
  
  def button_class
    if mobile_device?
      return 'green'
    else
      room_category = last_search_log.try(:room_category) || params[:room_category].to_i
      if room_category.eql?(1)
        return 'night-btn'
      elsif room_category.eql?(2)
        return 'day-btn'
      else
        return 'any-time-btn'
      end
    end
  end

  def show_meta_tags_according_to_page(action, controller=nil)
    if action == 'index_airport' || action == 'airports'
      '/layouts/public/meta_tags_airports'
    elsif action == 'index_city' || action == 'cities'
      '/layouts/public/meta_tags_cities'
    elsif action == 'business_trips'
      '/layouts/public/meta_tags_corporate'
    elsif action == 'be_our_partner'
      '/layouts/public/meta_tags_partners'
    elsif action == 'show' && controller == 'public/hotel'
      '/layouts/public/meta_tags_hotel'
    else
      '/layouts/public/meta_tags'
    end
  end

end
