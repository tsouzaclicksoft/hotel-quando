# encoding: utf-8

module Shared::MobileDeviceHelper
  def mobile_device?
    request.user_agent =~ /Mobile|webOS/
  end
end
