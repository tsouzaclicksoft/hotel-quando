module Shared::IconsHelper
  def icon_for(icon_type)
    selected_icon_class = {
      friend_hq: 'fa fa-child',
      hours_discount: "fa fa-money",
      black_friday: "fa fa-bolt",
      summer: "fa fa-thermometer-full",
      search: 'fa fa-search',
      user: 'fa fa-user',
      bookmark: 'fa fa-bookmark',
      question_mark: 'fa fa-question-circle',
      customer_service: 'fa fa-comments',
      mobile_menu_bar: 'fa fa-bars green',
      faq: 'fa fa-list',
      privacy: 'fa fa-lock',
      business_travel: 'fa fa-plane',
      partner: 'fa fa-users',
      sign_out: 'fa fa-sign-out',
      terms_of_use: 'fa fa-file-text',
      sign_in: 'fa fa-sign-in',
      plus: 'fa fa-plus',
      double_arrow_right: 'fa fa-angle-double-right',
      company: 'fa fa-briefcase',
      consultants: 'fa fa-users',
      job_title: 'fa fa-briefcase',
      cost_center: 'fa fa-usd'
    }[icon_type]
    "<i class='#{selected_icon_class}'></i>".html_safe
  end
end
