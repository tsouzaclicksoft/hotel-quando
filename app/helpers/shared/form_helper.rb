# encoding: utf-8

module Shared::FormHelper
  def country_and_state_select_for(options)
    # Doing this because there are some places where the country input is string, there is no real relation between
    # the country table and the resource.
    # DO NOT use this if the resource country is going to be sent to MaxiPago (at the moment, the only place
    # it is necessary to send the country it is in the payment creation), because it uses the iso initials of the country

    # Putting the prompt in the options of the state select to avoid a problem with angular, where it sends
    # "? string: ?" to the server as the state
    options_for_country_select = [[I18n.t(:select_the_country), '']] + Country.all.order("name_#{I18n.locale}").pluck("name_#{I18n.locale}", :iso_initials)
    options_for_state_select = [[I18n.t(:select_the_state), '']] + State.all.order(:initials).pluck(:name, :initials)
    return content_tag(:div, { 'ng-controller' => 'CountryAndStateSelectCtrl', 'ng-init' => "country = '#{options[:resource].country}'; state = '#{options[:resource].state}'; countryChanged()"}) do
      country_select = options[:form_builder].input :country, collection: options_for_country_select, include_blank: false,
                          input_html: { 'ng-model' => 'country', 'ng-change' => 'countryChanged()'}.merge(options[:country].try(:[], :input_html) || {})

      state_select = content_tag(:div, {'ng-show' => 'stateShouldBeVisible'}) do
                       options[:form_builder].input :state, collection: options_for_state_select, include_blank: false,
                       input_html: { 'ng-model' => 'state'}.merge(options[:state].try(:[], :input_html) || {})
                     end

      country_select + state_select

    end
  end

  def gender_select_options
    return [[I18n.t('male'), 'male'], [I18n.t('female'), 'female']]
  end
end


