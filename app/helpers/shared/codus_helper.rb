# encoding: utf-8

module Shared::CodusHelper
  def shared_component(component_name, locals = {})
    render(partial: "/layouts/components/#{component_name}", locals: locals)
  end

  def layout_component(component_name, locals = {})
    render(partial: "/layouts/#{current_layout}/components/#{component_name}", locals: locals)
  end
end