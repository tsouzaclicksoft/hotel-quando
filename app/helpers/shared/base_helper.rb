module Shared::BaseHelper
  include Shared::CodusHelper
  include Shared::FormHelper
  include Shared::IconsHelper
  include Shared::MobileDeviceHelper
end
