# encoding: utf-8

module Shared::CurrencyCurrentHelper

  def current_currency
    @current_currency = Currency.by_code_or_money_sign(cookies[:currency]).order(created_at: :desc).first
    @current_currency = Currency.by_code_or_money_sign("USD").order(created_at: :desc).first if @current_currency.nil?
    @current_currency
  end

  def calculate_currency(booking_value, currency_symbol)
    currency = Currency.by_money_sign(currency_symbol).order(created_at: :desc).first if currency_symbol != current_currency.money_sign

    if currency_symbol == current_currency.money_sign
      booking_value
    elsif currency_symbol != "US$" && current_currency.money_sign == "US$"
      booking_value / currency.value
    else
      hotel_currency_in_dollar = booking_value / currency.value
      hotel_current_currency = hotel_currency_in_dollar * current_currency.value

      hotel_current_currency
    end
  end

  def calculate_currency_tax(hotel_currency_code, value)
    if hotel_currency_code == "US$"
      value
    else
      currency_by_hotel_code = Currency.by_code_or_money_sign(hotel_currency_code).order(created_at: :desc).first
      value * currency_by_hotel_code.value
    end
  end

  def calculate_to_currency_booking(hotel_currency_code, booking_value, booking_currency_code, authorized_at)
    currency = Currency.by_code_or_money_sign(booking_currency_code).in_day(authorized_at).first

    if currency.nil?
      currency = Currency.by_code_or_money_sign(booking_currency_code).where("created_at < ?", authorized_at).order(created_at: :desc).first
      currency_by_hotel = Currency.by_code_or_money_sign(Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code]).where("created_at < ?", authorized_at).order(created_at: :desc).first
    else
      currency = Currency.by_code_or_money_sign(booking_currency_code).in_day(authorized_at).first
      currency_by_hotel = Currency.by_code_or_money_sign(Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code]).in_day(authorized_at).first
    end

    if currency.nil?
      booking_value
    elsif currency.money_sign == "US$"
      booking_value / currency_by_hotel.value
    else
      hotel_currency_in_dollar = booking_value / currency_by_hotel.value

      hotel_currency_in_dollar * currency.value
    end
  end

  def calculate_to_currency_at_authorized_at(hotel_currency_code, booking_value, booking_currency_code, authorized_at)
    currency = Currency.by_code_or_money_sign(booking_currency_code).in_day(authorized_at).first
    currency_by_hotel = Currency.by_code_or_money_sign(Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code]).in_day(authorized_at).first unless currency.nil?

    if currency.nil?
      booking_value
    elsif Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code] == "US$"
      booking_value / currency.value
    elsif booking_currency_code == "US$" && Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code] != "US$"
      currency_by_hotel.value * booking_value
    else
      hotel_currency_in_dollar = booking_value / currency_by_hotel.value

      hotel_currency_in_dollar * currency.value
    end
  end

def simple_current_convert(hotel_currency_code, value)
  currency = Currency.by_code_or_money_sign("R$").order(created_at: :desc).first if Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code] != "R$"

  if Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code] == "US$"
    value / currency.value
  elsif Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code] == "R$"
    value
  else
    currency_by_currency_code = Currency.by_code_or_money_sign(Hotel::CURRENCY_SYMBOL_REVERSED[hotel_currency_code]).order(created_at: :desc).first
    hotel_currency_in_dollar = value / currency.value
    hotel_currency_in_dollar * currency_by_currency_code.value
  end
end

  def its_currency_current?(currency)
    currency.money_sign == cookies[:currency]
  end

  def current_convert_to_epayco(payment_currency_code, hotel_currency_code, value)

    if (payment_currency_code == "USD" && hotel_currency_code == "US$") || (payment_currency_code == "COP" && hotel_currency_code == "COP")
      value
    elsif payment_currency_code == "USD"
      currency_by_hotel_code = Currency.by_code_or_money_sign(hotel_currency_code).order(created_at: :desc).first
      value / currency_by_hotel_code.value
    else
      currency_by_hotel_code = Currency.by_code_or_money_sign(payment_currency_code).order(created_at: :desc).first
      currency_dollar = Currency.by_code_or_money_sign(hotel_currency_code).order(created_at: :desc).first

      convert_to_dollar = value / currency_dollar.value
      convert_to_dollar * currency_by_hotel_code.value
    end
  end

  def convert_value_ws_api_to_paypal(user_country, hotel_currency_code, value)
    if user_country == "BR"
      return value if hotel_currency_code == "R$"

      currency_by_hotel = Currency.by_code_or_money_sign(hotel_currency_code).order(created_at: :desc).first
      currency = Currency.by_code_or_money_sign("R$").order(created_at: :desc).first

      convert_to_dollar = value / currency_by_hotel.value
      convert_to_dollar * currency.value
    else
      return value if hotel_currency_code == "US$"

      currency = Currency.by_code_or_money_sign(hotel_currency_code).order(created_at: :desc).first
      convert_to_dollar = value / currency_by_hotel.value
    end
  end

  #consider only Dollar and Real
  def current_convert_to_paypal(payment_currency_code, hotel_currency_code, value)
    if (payment_currency_code == "USD" && hotel_currency_code == "US$") || (payment_currency_code == "COP" && hotel_currency_code == "COP")
      value
    elsif payment_currency_code == "USD"
      currency_by_hotel_code = Currency.by_code_or_money_sign(hotel_currency_code).order(created_at: :desc).first
      value / currency_by_hotel_code.value
    else
      currency_by_hotel_code = Currency.by_code_or_money_sign(payment_currency_code).order(created_at: :desc).first
      currency_dollar = Currency.by_code_or_money_sign(hotel_currency_code).order(created_at: :desc).first

      convert_to_dollar = value / currency_dollar.value
      convert_to_dollar * currency_by_hotel_code.value
    end
  end
end
