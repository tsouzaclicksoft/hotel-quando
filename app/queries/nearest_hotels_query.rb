class NearestHotelsQuery
  DEFAULT_MAXIMUM_DISTANCE_FROM_ADDRESS_TO_HOTEL_IN_KM = 30

  def initialize(params)
    @latitude = params[:latitude]
    @longitude = params[:longitude]
    @max_distance_in_km = params[:max_distance_in_km] || DEFAULT_MAXIMUM_DISTANCE_FROM_ADDRESS_TO_HOTEL_IN_KM
    @search_scope = Hotel.includes(:photos, :city)
  end

  def only_omnibees!
    @search_scope = @search_scope.omnibees
  end

  def only_hoteisnet!
    @search_scope = @search_scope.omnibees
  end

  def hotel_by_id(id)
    if @hotel_by_id
      @hotel_by_id[id]
    else
      @hotel_by_id = {}
      nearest_hotels.each do |hotel|
        @hotel_by_id[hotel.id] = hotel
      end
      @hotel_by_id[id]
    end
  end

  def hotel_list
    nearest_hotels
  end

  private
  def nearest_hotels
    return @nearest_hotels if @nearest_hotels
    box = Geocoder::Calculations.bounding_box([@latitude, @longitude], @max_distance_in_km)
    @nearest_hotels = @search_scope.within_bounding_box(box).to_a
    @nearest_hotels
  end
end
