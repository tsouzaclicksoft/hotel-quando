class Api::V2::BookingsController < ApplicationController

  #before_filter :authenticate_public_user!
  before_filter :authenticate_request!
  include Shared::CurrencyCurrentHelper
  include ActionView::Helpers::NumberHelper
  include Public::UserHelper
  before_filter :set_currency

  def get_bookings
    bookings = current_public_user.bookings.includes(:offer, :hotel, :meeting_room_offer, :omnibees_offer).order(created_at: :desc).select(Booking.attribute_names - ['omnibees_code', 'omnibees_request_xml', 'omnibees_response_xml'])
    bookings_data = []
    bookings.each do |item|
      new_field = {"checkin_timestamp" => item.offer.checkin_timestamp.to_s}
      can_show_payment_button = {"can_show_payment_button" => can_show_payment_button?(item)}
      new_field_item = JSON::parse(item.to_json).merge(new_field)
      can_show_payment_button_item = new_field_item.merge(can_show_payment_button)
      bookings_data << can_show_payment_button_item
    end

    hotels = Hotel.all.order(id: :asc)
    options_for_status_select = []
    Booking::ACCEPTED_STATUSES.each do | status_name, status_code |
      if status_code == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
        options_for_status_select << {"pt-BR"=> t(status_name, locale: :pt_br), "en"=> t(status_name, locale: :en), "es"=> t(status_name, locale: :es), "status"=> [status_code, Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]]}
      elsif status_code == Booking::ACCEPTED_STATUSES[:confirmed]
        options_for_status_select << {"pt-BR"=> t(status_name, locale: :pt_br), "en"=> t(status_name, locale: :en), "es"=> t(status_name, locale: :es), "status"=> [status_code, Booking::ACCEPTED_STATUSES[:confirmed_without_card]]}
      elsif status_code != Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced] && status_code != Booking::ACCEPTED_STATUSES[:confirmed_without_card]
        options_for_status_select << {"pt-BR"=> t(status_name, locale: :pt_br), "en"=> t(status_name, locale: :en), "es"=> t(status_name, locale: :es), "status"=> status_code}
      end
    end
    status = options_for_status_select
    render :json => { bookings: Kaminari.paginate_array(bookings_data).page(params[:page]).per(10), hotels: hotels, status: status }
  end

  def get_booking
    @booking = current_public_user.bookings.find(params[:id])
    offer = @booking.offer
    room = @booking.room
    hotel = @booking.hotel
    unit_to_currency = Currency.by_code_or_money_sign(@booking.currency_code).first unless @booking.currency_code.nil?
    @booking.currency_code = current_currency.code
    booking_initial_value = number_to_currency(@booking.initial_price, unit: Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol])
    value_for_extra_people = number_to_currency(calculate_currency(@booking.extra_people_price, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]), unit: Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol])
    booking_tax = number_to_currency(@booking.booking_tax, unit: Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol])
    total_value = number_to_currency(@booking.total_price, unit: Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol])
    booking_creation_date_and_hour = @booking.created_at
    if (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_without_card])
      booking_confirmation_date_and_hour = @booking.updated_at
    end
    offer_class = @booking.offer.class.name.constantize
    if @booking.is_omnibees_or_hoteisnet?
      checkin = "#{l((@booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute).to_date)}"
    else
      checkin = "#{l((@booking.date_interval.first + offer_class::ROOM_CLEANING_TIME - 1.minute).to_date)}"
    end
    if @booking.is_omnibees_or_hoteisnet?
      checkin = checkin + ' ' + "#{(@booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute).strftime("%H:%M")}"
    else
      checkin = checkin + ' ' + "#{(@booking.date_interval.first + offer_class::ROOM_CLEANING_TIME - 1.minute).strftime("%H:%M")}"
    end
    if @booking.is_omnibees_or_hoteisnet?
      checkout = "#{l((@booking.date_interval.last - Offer::ROOM_CLEANING_TIME + 1.minute).to_date)}"
    else
      checkout = "#{l((@booking.date_interval.last - offer_class::ROOM_CLEANING_TIME + 1.minute).to_date)}"
    end

    if @booking.is_omnibees_or_hoteisnet?
      checkout = checkout + ' ' + "#{(@booking.date_interval.last - Offer::ROOM_CLEANING_TIME + 1.minute).strftime("%H:%M")}"
    else
      checkout = checkout + ' ' + "#{(@booking.date_interval.last - offer_class::ROOM_CLEANING_TIME + 1.minute).strftime("%H:%M")}"
    end
    confirmation = {"pt-BR"=>t("bookings.#{Booking::ACCEPTED_STATUS_REVERSED[@booking.status]}", locale: :pt_br), "en"=>t("bookings.#{Booking::ACCEPTED_STATUS_REVERSED[@booking.status]}", locale: :en), "es"=>t("bookings.#{Booking::ACCEPTED_STATUS_REVERSED[@booking.status]}", locale: :es)}
    cancelable = false
    if ((@booking.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (@booking.reload.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]) || (@booking.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (@booking.reload.status == Booking::ACCEPTED_STATUSES[:confirmed_without_card])) && (@booking.hotel.accepts_booking_cancellation?)
      if (@booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - @booking.hotel.minimum_hours_of_notice.hours) > (Time.now)
        cancelable = true
      else
        cancelable = false
      end
    else
      cancelable = false
    end
    render :json => { booking: @booking, offer: offer, room: room, hotel: hotel, booking_initial_value: booking_initial_value,
      value_for_extra_people: value_for_extra_people, booking_tax: booking_tax, total_value: total_value,
      booking_creation_date_and_hour: booking_creation_date_and_hour, booking_confirmation_date_and_hour: booking_confirmation_date_and_hour,
      confirmation: confirmation, checkin: checkin, checkout: checkout, cancelable: cancelable,
      room_type: {"es"=>room.room_type.name_es, "en"=>room.room_type.name_en, "pt-BR"=>room.room_type.name_pt_br} }
  end

  def cancel
    @booking = current_public_user.bookings.find(params[:id])

    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking)
    if booking_cancellation_response.success
      render :json => { canceled: true }
    else
      render :json => { canceled: false }
    end
  end

  def show
    @booking = current_public_user.bookings.find params[:id]
    if @booking.nil?
      render :json => {  }.to_json, :status => 500
    else
      can_pay_card = false
      pay_now = @booking.will_be_paid_online? ? @booking.total_price : @booking.booking_tax

    if @booking.guest.country == "BR" && current_currency.code != "BRL"
      can_pay_card = false
    end

    render :json => {
      booking: @booking,
      will_be_paid_online: @booking.will_be_paid_online?,
      pay_now: ('%.2f' % calculate_currency(pay_now, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2)),
      tax: ('%.2f' % calculate_currency(@booking.booking_tax, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2)),
      room_value: ('%.2f' % calculate_currency(@booking.total_price_without_tax, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2)),
      total_price: ('%.2f' % calculate_currency(@booking.total_price, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2)),
      is_epayco: current_public_user.country == 'CO',
      is_maxipago: @booking.guest.country == "BR" && current_currency.code == "BRL",
      currency_code: current_currency.code,
      can_pay_card: can_pay_card
    }.to_json, :status => 200
    end
  end

  def create
    begin
      hotel = Hotel.find(params[:hotel_id])
      offer_type = hotel.omnibees_code.blank? ? "Offer" : "OmnibeesOffer"
      @booking = Booking.new(params.permit(:guest_name, :number_of_people, :note, :guest_email, :have_promotional_code, :consultant_id, :traveler_id, :phone_number))
      @offer = offer_type.constantize.find params[:offer_id]
      @booking.currency_code = current_currency.code
      @booking.hotel = hotel

      if current_public_user.is_a_agency? || current_public_user.is_a_company?
        create_consultant() if params[:new_consultant].eql?("true")
        create_traveler_and_company() if params[:new_traveler_and_company].eql?("true")
        create_traveler() if params[:new_traveler].eql?("true")
      end
      if current_public_user.errors.present?
        render :json => { :msg => current_public_user.errors }.to_json, :status => 400
      elsif @consultant.try(:errors).present? || @company.try(:errors).present? || @traveler.try(:errors).present? || @cost_center.try(:errors).present? || @credit_card.try(:errors).present?
        if @consultant.try(:errors).present?
          render :json => { :msg => @consultant.try(:errors) }, :status => 400
        elsif @company.try(:errors).present?
          render :json => { :msg => @company.try(:errors) }, :status => 400
        elsif @traveler.try(:errors).present?
          render :json => { :msg => @traveler.try(:errors) }, :status => 400
        elsif @cost_center.try(:errors).present?
          render :json => { :msg => @cost_center.try(:errors) }, :status => 400
        elsif @credit_card.try(:errors).present?
          render :json => { :msg => @credit_card.try(:errors) }, :status => 400
        end
      elsif (current_public_user.is_a_agency? || current_public_user.is_a_company?) && @booking.traveler.blank?
        render :json => { :msg => "#{t(:traveler)} #{I18n.t('errors.messages.blank')}" }.to_json, :status => 400
      elsif @booking.traveler.present? && current_currency.code != 'BRL' && @booking.traveler.need_to_complete_address? && !current_public_user.accept_confirmed_and_invoiced
        render :json => { :msg => "Para realizar transação em USD é necessário preencher todos os campos de endereço corretamente" }.to_json, :status => 400
      elsif @booking.traveler.present? && (@booking.traveler.country == "BR" || @booking.traveler.country.blank?) && @booking.traveler.cpf.blank?
        render :json => { :msg => "Para realizar transação em BRL é necessário preencher o CPF" }.to_json, :status => 400
      else
        @booking.user_id = current_public_user.id
        @booking.hotel_comments = @offer.hotel.send("description_#{I18n.locale}")
        @booking.pack_in_hours = @offer.pack_in_hours
        @booking.company_id = current_public_user.company_id
        @booking.status = Booking::ACCEPTED_STATUSES[:waiting_for_payment]
        @booking.created_by_agency = current_public_user.agency_detail.present?
        @booking.checkin_date = @offer.checkin_timestamp
        @booking.currency_code = current_currency.code
        @booking.is_app_mobile = true
        booking_create_response = BookingMaker.call(offer: @offer, booking: @booking)

        if booking_create_response.success
          @booking = Booking.find(booking_create_response.booking.id)
          verify_promotional_code if params[:have_promotional_code] == "true"

          @booking.reload
          if params[:have_promotional_code] == "true"
            @booking.have_promotional_code = true
            @booking.save
          end
          render :json => { :id => booking_create_response.booking.id }.to_json, :status => 200
        else
          render :json => { :msg => booking_create_response.message }.to_json, :status => 400
        end
      end
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      render :json => { :msg => t(:message_booking_creation_error) }.to_json, :status => 400
    end
  end

  def cancel
    @booking = Booking.find(params[:id])

    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking)
    if booking_cancellation_response.success
      render :json => { :msg => booking_cancellation_response.message }.to_json, :status => 200
    else
      render :json => { :msg => booking_cancellation_response.message }.to_json, :status => 400
    end
  end

  private
  def set_currency
    if params[:currency]
      cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
    elsif cookies[:currency].nil?
      cookies[:currency] = "US$"
    end
  end

  def create_consultant
    @consultant = Consultant.new(agency_detail_id: current_public_user.agency_detail.id , name: params[:consultant_name])
    if @consultant.save
      @booking.consultant_id = @consultant.id
      params[:new_consultant] = false
    else
      flash[:error] = "#{t(:consultant)}: #{@consultant.errors.full_messages.to_sentence}"
    end
  end

  def create_traveler_and_company
      create_company()
      create_traveler()
    end

  def create_company
    @company = CompanyDetail.new(name: params[:company_name], cnpj: params[:company_cnpj], financial_contact: params[:company_financial_contact], phone_financial_contact: params[:company_phone_financial_contact], user_id: current_public_user.id)
    if current_public_user.is_a_agency?
      @company.agency_detail_id = current_public_user.agency_detail.id
    end
    if @company.save
      params[:new_traveler_and_company] = false
      params[:new_company] = false
      CompanyUniverse.update_company_universe_equals_cnpj(@company.cnpj, @company.id)
    else
      flash[:error] = "#{t(:company)}: #{@company.errors.full_messages.to_sentence}"
    end
  end

  def create_traveler
    CostCenter.transaction do
      @cost_center = CostCenter.new(name: params[:cost_center_name])
      if @company.present?
        @cost_center.company_detail_id = @company.id
      elsif current_public_user.is_a_company?
        @cost_center.company_detail_id = current_public_user.company_detail.id
      else
        @cost_center.company_detail_id = params[:company_id]
      end

      if @cost_center.save
        @traveler = Traveler.new(cost_center_id: @cost_center.id,
                                name: params[:traveler_name],
                                email: params[:traveler_email],
                                phone_number: params[:traveler_phone],
                                cpf: params[:traveler_cpf],
                                passport: params[:traveler_passport],
                                street: params[:traveler_street],
                                city: params[:traveler_city],
                                state: params[:traveler_state],
                                country: params[:traveler_country],
                                postal_code: params[:traveler_postal_code],
                                company_detail_id: @cost_center.company_detail_id)
        if @traveler.save
          if !current_public_user.accept_confirmed_and_invoiced
            @credit_card = CreditCard.new(card_number: params[:card_number],
                                  cvv: params[:cvv],
                                  expiration_month: params[:expiration_month].to_i,
                                  expiration_year: params[:expiration_year].to_i,
                                  traveler_id: @traveler.id)
            @credit_card.temporary_number = @credit_card.card_number
            if @credit_card.save
              params[:new_traveler] = false
              @booking.traveler_id = @traveler.id
              @booking.guest_name = @booking.traveler.name
              if @booking.phone_number.blank?
                @booking.phone_number = @booking.traveler.phone_number
              end
            else
              flash[:error] = "#{t(:credit_card)}: #{@credit_card.errors.full_messages.to_sentence}"
              raise ActiveRecord::Rollback
            end
          else
            params[:new_traveler] = false
            @booking.traveler_id = @traveler.id
            @booking.guest_name = @booking.traveler.name
            if @booking.phone_number.blank?
              @booking.phone_number = @booking.traveler.phone_number
            end
          end
        else
          flash[:error] = "#{t(:traveler)}: #{@traveler.errors.full_messages.to_sentence}"
          raise ActiveRecord::Rollback
        end
      else
        flash[:error] = "#{t(:cost_center)}: #{@cost_center.errors.full_messages.to_sentence}"
      end
    end
  end


  def verify_promotional_code
    @promotional_code = PromotionalCode.find_by_code params[:promotional_code]
    unless @promotional_code.nil?
      if check_if_code_is_active
        create_promotional_code_log()
      end
    end
  end

  def check_if_code_is_active
    if @promotional_code.status == PromotionalCode::ACCEPTED_STATUSES[:expired]
      return false
    elsif @promotional_code.status != PromotionalCode::ACCEPTED_STATUSES[:active]
      return false
    else
      check_pack_and_weekday
    end
  end

  def check_pack_and_weekday
    @checking_date = params[:checkin_date] || @booking.checkin_date
    @pack_in_hours = params[:pack_in_hours] || @booking.pack_in_hours
    @avaiable_for_packs = @promotional_code.avaiable_for_packs || ""
    @avaiable_for_days_of_the_week = @promotional_code.avaiable_for_days_of_the_week || ""
    if @avaiable_for_packs.include?(@pack_in_hours.to_s) && @avaiable_for_days_of_the_week.include?(@checking_date.to_date.wday.to_s)
      check_if_hotel_is_partner_this_campaign
    else
      return false
    end
  end

  def check_if_hotel_is_partner_this_campaign
    @hotel =@booking.hotel
    hotels_partners_in_this_campaign = @promotional_code.campaign.hotel_groups.pluck(:hotel_id)
    if !hotels_partners_in_this_campaign.include?(@hotel.id)
      return false
    else
      check_if_user_have_booking
    end
  end

  def check_if_user_have_booking
    if @promotional_code.user_type == PromotionalCode::ACCEPTED_USER_TYPE[:users_without_booking]
      bookings = current_public_user.bookings.where(status: [
                                  Booking::ACCEPTED_STATUSES[:confirmed],
                                  Booking::ACCEPTED_STATUSES[:no_show_paid_with_success],
                                  Booking::ACCEPTED_STATUSES[:confirmed_and_captured],
                                  Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
                                  ])
      if bookings.first
        return false
      else
        return true
      end
    else
      return true
    end
  end

  def create_promotional_code_log
    destroy_promotional_code_log_if_necessary()
    @promotional_code_log = PromotionalCodeLog.new
    @promotional_code_log.promotional_code_id = @promotional_code.id
    @promotional_code_log.booking_id = @booking.id
    @promotional_code_log.currency_code = cookies[:currency]
    @promotional_code_log.save!
    if @promotional_code.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:percentage] && @promotional_code.discount.eql?(100)
      @booking.booking_tax = 0
      @booking.save!
    end
    update_promotional_code_status_if_necessary()
  end

  def destroy_promotional_code_log_if_necessary
    if @booking.promotional_code_log
      @booking.promotional_code_log.destroy
    end
  end

  def update_promotional_code_status_if_necessary
    @booking.reload
    promotional_code = @booking.promotional_code_log.promotional_code
    if promotional_code.promotional_code_logs.count >= promotional_code.usage_limit && promotional_code.user_type == PromotionalCode::ACCEPTED_USER_TYPE[:all_users]
      promotional_code.status = PromotionalCode::ACCEPTED_STATUSES[:used]
      promotional_code.save!
    end
  end

end
