class Api::V2::PaymentsController < ApplicationController
  respond_to :json

  before_filter :authenticate_request!
  before_filter :set_resources_and_check_permissions!, only: [ :create_payment_epayco, :create_payment_maxipago]
  before_filter :set_currency
  include PayPal::SDK::Core::Logging
  include Shared::CurrencyCurrentHelper

  class ExecutePaymentPaypalFailed < StandardError; end
  class ExecutePaymentEpaycoFailed < StandardError; end
  class OmnibeesReservationError < StandardError; end

  def create_payment_maxipago
    begin
      use_credit_card_to_pay_booking()
      advantage_club

      @booking.reload
      reserve_on_omnibees() if @booking.is_omnibees_or_hoteisnet?
      if @booking.hotel.booking_on_request
        HotelMailer.send_booking_request_confirmation(@booking).deliver
        UserMailer.send_booking_request_confirmation(@booking).deliver
      else
        HotelMailer.send_booking_confirmation_notice(@booking).deliver
        UserMailer.send_booking_confirmation_notice(@booking).deliver
      end
      render :json => { :msg => t(:success_payment) }
    rescue OmnibeesReservationError => omnibees_error
      Airbrake.notify(omnibees_error)
      AdminMailer.error_reserving_on_omnibees(@booking, nil).deliver
    rescue Exception => exception
      if @payment
        UserMailer.send_payment_failure_notice(@payment).deliver
      end
      logger.error(exception)
      Airbrake.notify(exception, parameters: {user_id: current_public_user.id})
      # Airbrake.notify(exception, parameters: {user_id: @affiliate.id})
      render :json => { :msg => t('payments.errors.transaction_not_authorized'), :error => 404 }
    end
  end

    def use_credit_card_to_pay_booking
    create_payment

    begin
      price_to_pay_now = @booking.will_be_paid_online? ? @booking.total_price : @booking.booking_tax
      @currency = current_currency.code

      authorization_response = MaxiPagoInterface.direct_sale_without_token({
        payment: @payment,
        id: @payment.booking_id,
        # force payment in BRL
        total_price: calculate_to_currency_booking(1, price_to_pay_now, @currency, @booking.created_at).round(2),
        number: params[:card_number],
        expMonth: params[:card_expire_month],
        expYear: params[:card_expire_year],
        cvv: params[:card_cvc]
      })
    rescue MaxiPagoInterface::MalformedMaxipagoResponseError => error
      @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
      @payment.maxipago_response += error.to_s
      @payment.save!
      raise PaymentFailedError.new(t('payments.errors.maxipago_response_error'))
    end

    if authorization_response.success
      if @booking.will_be_paid_online?
        @payment.status = Payment::ACCEPTED_STATUSES[:captured]
        @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
      else
        @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
        @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
      end

      @booking.save!
    else
      @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
    end
    set_payment_maxipago_response_fields(authorization_response)
    @payment.save!
    raise PaymentFailedError.new(t('payments.errors.transaction_not_authorized')) unless authorization_response.success
  end

  def create_payment_epayco
    test_mode = (Rails.env == "development" || Rails.env == "test" || Rails.env == "staging") ? true : false
    Epayco.apiKey = ENV['EPAYCO_PUBLIC_KEY']
    Epayco.privateKey = ENV['EPAYCO_PRIVATE_KEY']
    Epayco.lang = 'ES'
    Epayco.test = test_mode
    create_payment
    advantage_club

    price_to_pay_now = @booking.will_be_paid_online? ? @booking.total_price : @booking.booking_tax
    currency = current_currency.code == 'COP' ? 'COP' : 'USD'

    begin
      credit_info = {
        "card[number]" => params[:card_number],
        "card[exp_year]" => params[:card_expire_year],
        "card[exp_month]" => params[:card_expire_month],
        "card[cvc]" => params[:card_cvc]
      }
      card_token = Epayco::Token.create credit_info
      customer_params = {
        token_card: card_token[:id],
        name: "#{params[:card_first_name]} #{params[:card_last_name]}",
        email: params[:card_email],
        phone: params[:card_billing_phone]
      }
      customer = Epayco::Customers.create customer_params
        charge_params = {
          token_card: card_token[:id],
          customer_id: customer[:data][:customerId],
          doc_type: params[:card_doc_type],
          doc_number: params[:card_doc_number],
          name: params[:card_first_name],
          last_name: params[:card_last_name],
          email: params[:card_email],
          bill: "Booking - #{@booking.id}",
          description: "Pago de reservas de hotel HotelQuando.Com",
          value: current_convert_to_epayco(currency, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol], price_to_pay_now).round(2).to_s,
          tax: "0",
          tax_base: "0",
          currency: currency,
          dues: "1"
        }

        epayco_payment = Epayco::Charge.create charge_params

        if epayco_payment[:success] && epayco_payment[:data][:estado] == "Aceptada"
          logger.info "Payment[#{epayco_payment[:data][:ref_payco]}] execute successfully"
          if @booking.will_be_paid_online?
            @payment.status = Payment::ACCEPTED_STATUSES[:captured]
            @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
          else
            @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
            @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
          end
          @payment.epayco_ref = epayco_payment[:data][:ref_payco].to_s
          @booking.save!
          @payment.save!
          if @booking.hotel.booking_on_request
            HotelMailer.send_booking_request_confirmation(@booking).deliver
            UserMailer.send_booking_request_confirmation(@booking).deliver
          else
            HotelMailer.send_booking_confirmation_notice(@booking).deliver
            UserMailer.send_booking_confirmation_notice(@booking).deliver
          end          
          render :json => { :msg => t(:success_payment) }
        else
          @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
          @payment.save!
          raise ExecutePaymentEpaycoFailed
        end
      rescue ExecutePaymentEpaycoFailed
        logger.error epayco_payment[:data][:respuesta]
        logger.error epayco_payment[:data][:description]
        @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
        @payment.maxipago_response += epayco_payment[:data][:description] unless epayco_payment[:data][:description].nil?
        @payment.maxipago_response += epayco_payment[:data][:respuesta] unless epayco_payment[:data][:respuesta].nil?
        @payment.save!
        render :json => { :msg => epayco_payment[:data][:descripcion], :error => 404 }
      rescue Exception => exception
        logger.error(exception)
        logger.error epayco_payment[:data][:respuesta]
        logger.error epayco_payment[:data][:description]
        @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
        @payment.maxipago_response += epayco_payment[:data][:description] unless epayco_payment[:data][:description].nil?
        @payment.maxipago_response += epayco_payment[:data][:respuesta] unless epayco_payment[:data][:respuesta].nil?
        @payment.save!
        if @payment
          UserMailer.send_payment_failure_notice(@payment).deliver
        end
        Airbrake.notify(exception, parameters: {user_id: current_public_user.id})        
        render :json => { :msg => epayco_payment[:data][:descripcion], :error => 404 }
      end
  end

  def finish
    @booking = Booking.find(params[:booking_id])
    create_payment
    advantage_club

    @paypal_payment = PayPal::SDK::REST::DataTypes::Payment.find(params[:payment_id])

    if(@paypal_payment.execute( :payer_id => @paypal_payment.payer.payer_info.payer_id ))
      @payment = @booking.payments.last

      if @booking.room_type.single_bed_quantity == 1
        bed_configuration = "1 #{t(:single_bed)}"
      elsif @booking.room_type.single_bed_quantity > 1
        bed_configuration = "#{@booking.room_type.single_bed_quantity} #{t(:single_beds)}"
      end

      if @booking.room_type.double_bed_quantity == 1
        bed_configuration = "1 #{t(:double_bed)}"
      elsif @booking.room_type.double_bed_quantity > 1
        bed_configuration = "#{@booking.room_type.double_bed_quantity} #{t(:double_beds)}"
      end

      logger.info "Payment[#{@paypal_payment.id}] execute successfully"
      if @booking.will_be_paid_online?
        @payment.status = Payment::ACCEPTED_STATUSES[:captured]
        @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
      else
        @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
        @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
      end
      @payment.paypal_transaction_id = @paypal_payment.transactions[0].related_resources[0].sale.id
      @booking.save!
      @payment.save!
      
      if @booking.hotel.booking_on_request
        HotelMailer.send_booking_request_confirmation(@booking).deliver
        UserMailer.send_booking_request_confirmation(@booking).deliver
      else
        HotelMailer.send_booking_confirmation_notice(@booking).deliver
        UserMailer.send_booking_confirmation_notice(@booking).deliver
      end

      render :json => {
       :booking => @booking,
       :offer => @booking.offer,
       :payment => @booking.payments.last,
       :hotel => @booking.hotel,
       :room => @booking.room_type,
       :booking_with_100_percent_discount => @booking.booking_with_100_percent_discount,
       :hotel_is_brazilian => @booking.hotel.is_brazilian,
       :total_price =>  ('%.2f' % calculate_currency(@booking.total_price, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2)),
       :booking_tax =>  ('%.2f' % calculate_currency(@booking.booking_tax, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2)),
       :bed_configuration => bed_configuration
      }.to_json, :status => 200
    else
      render :json => { :msg => "" }.to_json, :status => 400
    end
  end

  def finish_epayco
    @booking = Booking.find(params[:booking_id])

    if @booking.room_type.single_bed_quantity == 1
      bed_configuration = "1 #{t(:single_bed)}"
    elsif @booking.room_type.single_bed_quantity > 1
      bed_configuration = "#{@booking.room_type.single_bed_quantity} #{t(:single_beds)}"
    end

    if @booking.room_type.double_bed_quantity == 1
      bed_configuration = "1 #{t(:double_bed)}"
    elsif @booking.room_type.double_bed_quantity > 1
      bed_configuration = "#{@booking.room_type.double_bed_quantity} #{t(:double_beds)}"
    end

    render :json => {
     :booking => @booking,
     :offer => @booking.offer,
     :hotel => @booking.hotel,
     :room => @booking.room_type,
     :booking_with_100_percent_discount => @booking.booking_with_100_percent_discount,
     :hotel_is_brazilian => @booking.hotel.is_brazilian,
     :total_price =>  ('%.2f' % calculate_currency(@booking.total_price, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2)),
     :booking_tax =>  ('%.2f' % calculate_currency(@booking.booking_tax, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2)),
     :bed_configuration => bed_configuration
    }.to_json, :status => 200
  end

  private

  def create_payment
    @payment = Payment.new
    @payment.user_id = current_public_user.id
    @payment.traveler_id = @booking.traveler_id
    @payment.booking_id = @booking.id
    @payment.status = Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response]
    if params[:payment_id].present? || params[:payment].present?
      @payment.paypal_payment_id = params[:payment_id].present? ? params[:payment_id] : params[:payment][:paypal_payment_id]
    end
    @payment.is_app_mobile = true
    @payment.save!
  end

  def set_resources_and_check_permissions!
    begin
      @booking = current_public_user.bookings.find(params[:booking_id])
      @offer = @booking.offer
      if (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
         render :json => { :msg => t("bookings.booking_already_confirmed")}
      end
      if @booking.status == Booking::ACCEPTED_STATUSES[:canceled]
         render :json => { :msg => t("bookings.booking_canceled") }
      end
    rescue
       render :json => { :msg => t(:you_cant_confirm_a_booking_that_not_yours) }
    end
  end

  def advantage_club
    if params[:participant_cpf_tudo_azul].present?
      @booking.tudo_azul_associate!(params[:participant_cpf]) if  CpfValidator::Cpf.valid?(params[:participant_cpf])
    end
    if params[:participant_code_multiplus].present?
      @booking.multiplus_code = params[:multiplus_code]
      @booking.save!
    end
  end

  def set_currency
    if params[:currency]
      cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
    elsif cookies[:currency].nil?
      cookies[:currency] = "US$"
    end
  end

  def reserve_on_omnibees
    begin
      # 1 = Hoteisnet || 2 = Omnibees
      choice = ChooseOmnibeesOrHoteisnet.choose(@booking.pack_in_hours, @booking.offer.checkin_timestamp)
      if choice == 1
        client = Hotelnet::Client.new
      else
        client = Omnibees::Client.new
      end
      reservation_xml = client.make_booking({ booking: @booking })
      @booking.omnibees_request_xml = client.last_request_xml
      @booking.omnibees_response_xml = reservation_xml
      @booking.save!

      if choice == 1
        reservation_status = Hotelnet::DTO::HotelReservationToBookingResult.parse(reservation_xml)
      else
        reservation_status = Omnibees::DTO::HotelReservationToBookingResult.parse(reservation_xml)
      end

      if reservation_status.confirmed?
        @booking.omnibees_code = reservation_status.reservation_id
      end
      @booking.save!
    rescue Exception => ex
      Airbrake.notify(ex)
      raise OmnibeesReservationError
    end
    raise OmnibeesReservationError unless reservation_status.confirmed?
  end

  def set_payment_maxipago_response_fields(authorization_response)
    @payment.maxipago_order_id = authorization_response.order_id
    @payment.maxipago_response += authorization_response.body
    @payment.maxipago_transaction_id = authorization_response.transaction_id
  end
end
