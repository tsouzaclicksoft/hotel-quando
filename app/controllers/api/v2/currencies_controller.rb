class Api::V2::CurrenciesController < Public::BaseController
  respond_to :json

  def index
    if Currency.in_day(DateTime.now).exists?
      @currencies = Currency.in_day(DateTime.now)
    else
      @currencies = []
      Currency.all.order(created_at: :desc).group_by{ | currency | currency.name}.each do |name, currency|
        @currencies << currency.first
      end
    end
  end
end
