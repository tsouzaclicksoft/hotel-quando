class Api::V2::RegistrationController < ApplicationController

  def create_user
    puts data = request.body.read
    json = JSON.parse(data)
    json.delete("card_address")
    json.delete("address")
    json.delete("surname")
    user = User.new(json)
    if user.save
      render json: {success: :ok}, status: 200
    else
      puts user.errors.full_messages
      render json: {errors: user.errors.full_messages}, status: :error
    end
  end

  def recover_password
    user = User.find_by_email(params[:email])
    if user
      user.send_reset_password_instructions
      render json: {success: :ok}, status: 200
    else
      render json: {errors: 'Not found'}, status: :error
    end
  end

end
