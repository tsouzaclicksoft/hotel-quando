# encoding: utf-8
class Api::V1::HotelsController < Public::BaseController
  respond_to :json
  class CheckinDateIsInThePastError < StandardError; end
  class OfferNotAvailable < StandardError; end

  def index
    @hotels = []
    begin
      check_in_datetime = "#{params[:by_checkin_date]} #{params[:by_checkin_hour]}:00 -03:00".to_datetime
      raise CheckinDateIsInThePastError if check_in_datetime < (DateTime.now)
      @searched_address_coordinates = [params[:by_latitude], params[:by_longitude]]
      #@any_available_voucher = EasyTaxiVoucher.available_on(check_in_datetime).any?
      @hotel_search = OffersSearch::Hotels.new({
        number_of_guests: params[:by_number_of_people].to_i,
        latitude: params[:by_latitude].to_f,
        longitude: params[:by_longitude].to_f,
        check_in_datetime: check_in_datetime,
        pack_in_hours: params[:by_pack_length].to_i,
        room_category: params[:room_category].to_i
      })
      @search_exact_match = HotelDistanceSorter.sort(@hotel_search.results.exacts, @searched_address_coordinates)
      #@search_similar_match = HotelDistanceSorter.sort(@hotel_search.results.similars, @searched_address_coordinates)
      
      @hotel_exact = @search_exact_match.collect {|x| x[:hotel]}
      @min_offer_exact = @search_exact_match.collect {|x| x[:min_offer]}
      @distance_exact = @search_exact_match.collect {|x| x[:distance]}
      #hotel_exact_temp.each_with_index.map {|x,i| test << [:hotel => x.hotel, :offer => x.min_offer, :distance => x.distance ]}
      
      #@hotel_similar = @search_similar_match.collect {|x| x[:hotel]}
      #@min_offer_similar = @search_similar_match.collect {|x| x[:min_offer]}
      #@distance_similar = @search_similar_match.collect {|x| x[:distance]}

      begin
        log.exact_match_hotels_ids = @hotels_exact_match.to_a.map(&:id).join("-")
        #log.similar_match_hotels_ids = @hotels_similar_match.to_a.map(&:id).join("-")
        log.save
      rescue
      end

    rescue CheckinDateIsInThePastError
      render :json => { :msg => t(:error_message_selected_checkin_datetime_is_in_the_past) }.to_json, :status => 500
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      render :json => { :msg => 'Date or time seems in the past' }.to_json, :status => 500
    
    end

  end

  def show
    begin
      if params[:checkin_timestamp] #user is comming from booking
        checkin_datetime = params[:checkin_timestamp].to_datetime
        params[:length_of_pack] = params[:pack_in_hours]
        params[:checkin_date] = checkin_datetime.to_date
        params[:checkin_hour] = checkin_datetime.hour
      else
        checkin_datetime = "#{params[:checkin_date]} #{params[:checkin_hour]}:00 -03:00".to_datetime
      end
      raise OfferNotAvailable if checkin_datetime < (DateTime.now + 30.minutes)
      @hotel = Hotel.includes(:city).includes(room_types: :photos).find(params[:id])
      session[:last_hotel_visited_id] = @hotel.id
      pack_in_hours = params[:length_of_pack].to_i
      #@business_room = BusinessRoom.includes(:photos).find_by id: (params[:business_room])

      #if @business_room
      #  @offer_business_room = @business_room.is_a_meeting_room? ? MeetingRoomOffer.find(params[:offer_id]) : EventRoomOffer.find(params[:offer_id])
      #  if !@offer_business_room.available?
      #    render :json => { :msg => t(:error_message_hotel_without_offers_to_show) }.to_json, :status => 500
      #  end
      #else
        @room_type_search = OffersSearch::RoomTypes.new({
          number_of_guests: params[:number_of_people].to_i,
          hotel_id: @hotel.id,
          check_in_datetime: checkin_datetime,
          pack_in_hours: pack_in_hours
        })
        @search_exact_match = @room_type_search.results.exacts.sort_by { |s|
          offer = s[:min_offer]
          offer.save! if offer.is_omnibees?
          offer.reload #we need all the fields to calculate value for the entire number of guests
          s[:min_offer].price
        }

#        @search_similar_match = @room_type_search.results.similars.sort_by { |s|
#          offer = s[:min_offer]
#          offer.save! if offer.is_omnibees?
#          [ offer.checkin_timestamp,
#            offer.get_number_of_common_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)),
#            (offer.checkin_timestamp - checkin_datetime),
#            -(offer.get_number_of_exceeding_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)))
#          ]
#        }
        #@search = @search_exact_match  + @search_similar_match
      #end
    rescue OfferNotAvailable
      render :json => { :msg => t(:error_message_offer_not_available) }.to_json, :status => 500
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      render :json => { :msg => t(:error_message_search_params_error) }.to_json, :status => 500
    end


  end

  def hotel_room_type_show
    begin
      if params[:checkin_timestamp]
        checkin_datetime = params[:checkin_timestamp].to_datetime
      else
        checkin_datetime = (params[:checkin_date].to_date + params[:checkin_hour].to_i.hours)
      end

      raise if checkin_datetime < (DateTime.now + 1.hour)
      @room_type = RoomType.includes(:photos).find(params[:id])
      @hotel = @room_type.hotel
    rescue => ex
      render :json => { :msg => t(:error_message_search_params_error) }.to_json, :status => 500
    end
    
    render :json => { :room_type => @room_type, :hotel => @hotel, :status => :ok }
  end


end
