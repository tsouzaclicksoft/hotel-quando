class CompanyAttendant::CompanyDetailsController < CompanyAttendant::BaseController

	def index
		@company_details = CompanyDetail.getAllCompanyDetails(params)
		@companiesOrdened = CompanyDetail.getComapaniesOrdenedInMonth(@company_details)
	end

	def show
		@company_detail = CompanyDetail.find(params[:id])
	end
end
