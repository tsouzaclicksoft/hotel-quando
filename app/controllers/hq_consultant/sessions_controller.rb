# encoding: utf-8
class HqConsultant::SessionsController < Devise::SessionsController
  layout 'hq_consultant/login'

	def create
    hq_consultant = HqConsultant.find_by_login(params[:hq_consultant_hq_consultant].try(:[], :login))
    if hq_consultant.nil? || hq_consultant.try(:is_active?)
      super # if the affiliate is not found or the affiliate is active, we call the default devise action, else the affiliate is inactive and we display a custom message
    else
      flash['alert'] = "Sua conta foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_hq_consultant_hq_consultant_session_path
    end
  end

  def after_sign_in_path_for(hq_consultant)
    hq_consultant_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    hq_consultant_root_path
  end
end
