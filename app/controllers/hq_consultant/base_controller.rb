# encoding: utf-8
class HqConsultant::BaseController < ActionController::Base
  protect_from_forgery
  inherit_resources
  layout 'hq_consultant'
  before_filter :authenticate_hq_consultant_hq_consultant!, :set_locale_as_pt_br
  helper Shared::CurrencyCurrentHelper

  protected

    def begin_of_association_chain
      current_hq_consultant_hq_consultant
    end

    def getCurrentExecutive
      return current_hq_consultant_hq_consultant
    end

    def set_locale_as_pt_br
      I18n.locale = :pt_br
    end

end
