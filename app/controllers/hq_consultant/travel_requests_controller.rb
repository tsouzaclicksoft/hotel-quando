class HqConsultant::TravelRequestsController < HqConsultant::BaseController
	actions :all, :except => [ :destroy ]

	def edit
		@travel_request = TravelRequest.find(params[:id])
	end

	def end_of_association_chain
		return TravelRequest.all
	end

	def remove_air
		air = Air.find(params[:id])
		air.destroy;

		redirect_to edit_hq_consultant_travel_request_path(air.travel_request_id)
	end

	def add_air
		air = Air.new
		air.travel_request_id = params[:id].to_i
		air.flight_class = Air::ACCEPTED_CLASSES[:economy]
     	air.is_online = false
		air.save

		redirect_to edit_hq_consultant_travel_request_path(params[:id])

	end

	def cancel
		travel = TravelRequest.find(params[:id])

		if travel.request_status == TravelRequest::ACCEPTED_REQUEST_STATUS[:canceled]

			flash[:error] = "Solicitação já se encontra cancelada";

		elsif travel.request_status == TravelRequest::ACCEPTED_REQUEST_STATUS[:completed]

			flash[:error] = "Solicitação não pode mais ser cancelada";
		else
			travel.request_status = TravelRequest::ACCEPTED_REQUEST_STATUS[:canceled]
			travel.date_canceled = Time.now

			flash[:notice] = "Solicitação cancelada com sucesso";
		end

		travel.save;

		redirect_to hq_consultant_travel_requests_path
	end

	def send_email_to_traveler

		travel = TravelRequest.find(params[:id])

		begin
		  TravelRequestMailer.send_email_to_traveler(travel).deliver
		  flash[:notice] = "E-mail enviado com sucesso";
		rescue => ex
		  flash[:error] = "Ocorreu um erro ao tentar enviar email";
		end

		travel.request_status = TravelRequest::ACCEPTED_REQUEST_STATUS[:pending_request]

		travel.save

		redirect_to hq_consultant_travel_requests_path

	end


	def collection
		@travel_requests ||= end_of_association_chain.order('id DESC').includes(:airs).page(params[:page]).per(40)
	end

	def update
		@travel_request = TravelRequest.find(params[:id])

		@travel_request.hq_consultant_id = current_hq_consultant_hq_consultant.id

		@travel_request.update_attributes(permitted_params)

		@travel_request.save
		
		redirect_to hq_consultant_travel_request_path(id: @travel_request.id)
	end

	def permitted_params

		params[:travel_request][:total].gsub!('R$ ', ' ')
		params[:travel_request][:total].gsub!('.','')
		params[:travel_request][:total].gsub!(',','.')

		params[:travel_request][:total_paid_hq].gsub!('R$ ', ' ')
		params[:travel_request][:total_paid_hq].gsub!('.','')
		params[:travel_request][:total_paid_hq].gsub!(',','.')

		params[:travel_request][:airs_attributes].each do |k, v|
			v[:air_taxe_attributes][:fare].gsub!('R$ ', ' ')
			v[:air_taxe_attributes][:fare].gsub!('.','')
			v[:air_taxe_attributes][:fare].gsub!(',','.')

			v[:air_taxe_attributes][:boarding_taxes].gsub!('R$ ', ' ')
			v[:air_taxe_attributes][:boarding_taxes].gsub!('.', '')
			v[:air_taxe_attributes][:boarding_taxes].gsub!(',','.')

			v[:air_taxe_attributes][:taxe_hq].gsub!('R$ ', ' ')
			v[:air_taxe_attributes][:taxe_hq].gsub!('.', '')
			v[:air_taxe_attributes][:taxe_hq].gsub!(',','.')

			v[:air_taxe_attributes][:taxe_change].gsub!('R$ ', ' ')
			v[:air_taxe_attributes][:taxe_change].gsub!('.', '')
			v[:air_taxe_attributes][:taxe_change].gsub!(',','.')

			v[:air_taxe_attributes][:taxe_noshow].gsub!('R$ ', ' ')
			v[:air_taxe_attributes][:taxe_noshow].gsub!('.', '')
			v[:air_taxe_attributes][:taxe_noshow].gsub!(',','.')

			v[:air_taxe_attributes][:taxe_cancellation].gsub!('R$ ', ' ')
			v[:air_taxe_attributes][:taxe_cancellation].gsub!('.', '')
			v[:air_taxe_attributes][:taxe_cancellation].gsub!(',','.')
		end

    	params.fetch(:travel_request).permit(:id, :total, :contact_traveler, :total_paid_hq, :comments_traveler, :comments_consultant, :date_canceled, :reservation_status,

    		[:airs_attributes => [:id, :origin, :destiny, :departure_date, :arrival_date, :flight_duration, :ticket,
    			 :cia_locator, :seat, :airline, :operation_airline, :flight_number, :number_connections, :number_scales,
    			 :flight_class,:arrival_time, :departure_time, :is_included_hand_baggage, :is_included_baggage_dispatch, :number_baggage_dispatch, :due_date,
    			 [:air_taxe_attributes => [:id, :fare, :boarding_taxes, :taxe_hq, :taxe_change, :taxe_noshow, :taxe_cancellation]]
    			 ]]

    	)
  	end

end
