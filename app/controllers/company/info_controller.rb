# encoding: utf-8
class Company::InfoController < Company::BaseController
  actions :none

  def index
  end

  def edit
  end

  def update
    if current_company_company.update_with_password(company_params)
      sign_in(current_company_company, :bypass => true)
      flash[:success] = "Senha modificada com sucesso."
      redirect_to company_root_path
    else
      render "edit"
    end

  end

  private

  def company_params
    params.required(:company).permit(:password, :password_confirmation, :current_password, :responsible_name, :phone_number, :country, :state, :city, :street, :number, :complement, :login, :accept_confirmed_and_invoiced, :accept_confirmed_without_card )
  end

end
