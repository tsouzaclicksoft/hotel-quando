# encoding: utf-8
class Company::SessionsController < Devise::SessionsController
  layout 'company/login'

	def create
    company = Company.find_by_login(params[:company_company].try(:[], :login))
    if company.nil? || company.try(:is_active?)
      super # if the company is not found or the company is active, we call the default devise action, else the company is inactive and we display a custom message
    else
      flash['alert'] = "Sua empresa foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_company_company_session_path
    end
  end

  def after_sign_in_path_for(company)
    company_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    company_root_path
  end
end
