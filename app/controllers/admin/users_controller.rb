class Admin::UsersController < Admin::BaseController
  actions :index, :show, :edit, :update
  before_filter :check_colombian_authorization, :only => [:show, :edit]
  include Admin::BaseHelper

  has_scope :by_name
  has_scope :by_company_name
  has_scope :by_agency_name
  has_scope :by_email
  has_scope :by_cpf
  has_scope :by_passport
  has_scope :by_gender
  has_scope :by_country
  has_scope :by_state
  has_scope :by_city
  has_scope :by_min_birth_date
  has_scope :by_max_birth_date

  def index
    case params[:commit]
    when t(:export_data)
      send_data(UsersReport.new(collection.per(collection.total_count)).export.to_stream.read,
        filename: "users_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
        type:     "application/xlsx")
    else
      @user_count = apply_scopes(User).order('id DESC').count
      index!
    end
  end

  private
  def check_colombian_authorization
    user = User.find(params[:id])
    if current_admin_manager.master? && current_admin_manager.country == "CO" && user.country != 'CO'
      redirect_to admin_users_path
    end
  end

    def build_resource_params
      [params.fetch(:user, {}).permit!]
    end

  protected
    def collection
      if current_admin_manager.country == "BR"
        @users = end_of_association_chain.includes(:agency_detail, :company_detail).page(params[:page]).per(40)
      else
        @users = end_of_association_chain.includes(:agency_detail, :company_detail).where(country: current_admin_manager.country).page(params[:page]).per(40)
      end
    end
end


