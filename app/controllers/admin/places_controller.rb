class Admin::PlacesController < Admin::BaseController
  include Admin::BaseHelper
  before_action :set_place, only: [:show, :edit, :update, :activate, :deactivate, :set_hotels]
  before_action :authorized_master_and_b2b_commercial

  def index
    @places = Place.all.order(:id)
  end

  def show
  end

  def airports
    @airports = Place.where(place_type: Place::PLACE_TYPES[:airport]).order(:id)
  end

  def cities
    @cities = Place.where(place_type: Place::PLACE_TYPES[:city]).order(:id)
  end

  def regions
    @regions = Place.where(place_type: Place::PLACE_TYPES[:region]).order(:id)
  end

  def new
    @place = Place.new
  end

  def create
    @place = Place.new(place_params)
    if @place.valid?
      if params[:place][:place_type].to_i == Place::PLACE_TYPES[:region] || params[:place][:place_type].to_i == Place::PLACE_TYPES[:airport]
        hotels = verify_offers(params[:place][:latitude], params[:place][:longitude])
        hotel = ensure_hotel_distance(hotels, params[:place][:latitude], params[:place][:longitude], params[:place][:distance])
      else
        hotel = verify_offers(params[:place][:latitude], params[:place][:longitude])
      end
      if hotel.present?
        @place.hotel_id = hotel.collect{ |h| h[:hotel].id }.uniq
        @place.hotel_count = @place.hotel_id.count
      end
    end
    create!
  end

  def edit
  end

  def update
    @place.update_attributes!(place_params)
    if params[:place][:place_type].to_i == Place::PLACE_TYPES[:city]
      hotel = verify_offers(params[:place][:latitude], params[:place][:longitude])
    else
      hotels = verify_offers(@place.latitude, @place.longitude)
      hotel = ensure_hotel_distance(hotels, params[:place][:latitude], params[:place][:longitude], params[:place][:distance])
    end
    @place.hotel_id = hotel.collect{ |h| h[:hotel].id }.uniq
    @place.hotel_count = @place.hotel_id.count
    @place.save if @place.valid?
    redirect_to admin_place_path(id: @place.id)
  end



  def activate
    @place.update_attributes!(active: true)
    redirect_to admin_place_path(id: @place.id), notice: I18n.t('controllers.admin.places.activate')
  end

  def deactivate
    @place.update_attributes!(active: false)
    redirect_to admin_place_path(id: @place.id), notice: I18n.t('controllers.admin.places.deactivate')
  end

  private
    def place_params
      unless params[:citiesHash].blank?
        cities_hash = params[:citiesHash].split(', ')
        params[:place][:city_id]  = cities_hash[0]
        params[:place][:country]  = cities_hash[3]
      end
      params.require(:place).permit(:name_pt_br, :name_es, :name_en, :latitude,
                                    :longitude, :city_id, :distance, :place_type,
                                    :country, :street, :number, :complement,
                                    :zipcode, :active,
                                    photos_attributes: [
                                      :id, :is_main, :title_pt_br, :title_en,
                                      :title_es, :direct_image_url
                                      ])
    end

    def set_place
      @place = Place.find(params[:id])
    end

    def ensure_hotel_distance(hotel, latitude, longitude, distance)
      hotels = []
      hotel.each do |hotel|
        hotels << hotel if hotel[:hotel].distance_from([latitude.to_f, longitude.to_f]) <= distance.to_f
      end
      return hotels
    end

    def verify_offers(lat, long)
      offers = []
      Offer::ACCEPTED_LENGTH_OF_PACKS.each do |pack_of_hours|
        offers << OffersSearch::Hotels.new({number_of_guests: 1, latitude: lat,
          longitude: long, check_in_datetime: (DateTime.now.change({ hour: 14, min: 0, sec: 0 }) + 1.day), pack_in_hours: pack_of_hours, room_category: 0}).results.exacts
      end
      offers.flatten
    end
end
