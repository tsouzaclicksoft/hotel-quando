# encoding: utf-8
class Admin::BookingsController < Admin::BaseController
  actions :index, :show, :send_confirmation_notice_email
  before_filter :check_colombian_authorization, :only => [:show, :edit, :cancel]
  include Admin::BaseHelper
  has_scope :by_checkin_date, using: [:initial_date, :final_date]
  has_scope :by_created_at, using: [:initial_date, :final_date]
  has_scope :by_status
  has_scope :by_pack_length, type: :array
  has_scope :by_guest_name
  has_scope :by_hotel_id
  has_scope :by_id
  has_scope :by_actual_month
  has_scope :by_last_month
  has_scope :by_delayed_month
  has_scope :by_checkout_delay
  has_scope :by_refund_error
  has_scope :by_agency_creation
  has_scope :by_multiplus_code
  has_scope :by_meeting_room_offer
  has_scope :by_event_room_offer
  has_scope :by_room_offer
  has_scope :by_user_id
  before_action :set_booking, only: [:hotel_accept_request, :hotel_deny_request]

  def index
    case params[:commit]
    when t("views.admin.bookings.export_data")
      send_data(BookingsReport.new(collection.per(500)).export.to_stream.read,
        filename: "reserva_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
        type:     "application/xlsx")
    else
      @booking_count = apply_scopes(Booking).order('id DESC').count
      super
    end
  end

  def send_confirmation_notice_email
    @booking = Booking.find(params[:id])
    if params[:commit] == t("views.admin.bookings.send_confirmation_notice_mail_to_hotel")
      HotelMailer.send_booking_confirmation_notice(@booking).deliver
      flash[:notice] = t("views.admin.bookings.email_sent_to_hotel", hotel_name: @booking.hotel.name)
    elsif params[:commit] == t("views.admin.bookings.send_confirmation_notice_mail_to_user")
      UserMailer.send_booking_confirmation_notice(@booking).deliver
      flash[:notice] = t("views.admin.bookings.email_sent_to_user", user_name: @booking.user.name)
    end
    redirect_to admin_booking_path(@booking)
  end

  def cancel
    @booking = Booking.find(params[:id])

    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking, admin_cancelation: true)
    if booking_cancellation_response.success
      flash[:notice] = booking_cancellation_response.message
    else
      flash[:error] = booking_cancellation_response.message
    end

    redirect_to admin_bookings_path
  end

  def hotel_accept_request
    if @booking.status == Booking::ACCEPTED_STATUSES[:requested_reservation]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
    elsif @booking.status == Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
    end
    @booking.save! if @booking.valid?
    UserMailer.booking_request_accepted(@booking).deliver
    redirect_to admin_bookings_path
  end

  def hotel_deny_request
    @booking.status = Booking::ACCEPTED_STATUSES[:canceled]
    @booking.save! if @booking.valid?
    UserMailer.booking_request_denied(@booking).deliver
    redirect_to admin_bookings_path
  end

  private
  def check_colombian_authorization
    booking = Booking.find(params[:id])
    if current_admin_manager.master? && current_admin_manager.country == "CO" && booking.user.country != 'CO'
      redirect_to admin_bookings_path
    end
  end

  protected

    def collection
      if current_admin_manager.country == "BR"
        @bookings ||= end_of_association_chain.order('id DESC').page(params[:page]).per(40)
      else
        @bookings ||= end_of_association_chain.joins(:user).where('users.country' => current_admin_manager.country).order('id DESC').page(params[:page]).per(40)
      end
    end

    def set_booking
      @booking = Booking.find_by id: params[:id]
    end

end
