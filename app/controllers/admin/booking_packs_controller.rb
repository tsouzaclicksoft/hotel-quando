class Admin::BookingPacksController < Admin::BaseController
  actions :all, :except => [ :destroy ]

  def index
    index!
  end

  def create
    prepare_params
    create!
  end

  def update
    prepare_params
    update!
  end

  private

  def collection
    @booking_packs = BookingPack.includes(:city, :hotel, :room_type).order(:created_at)
  end

  def build_resource_params
    [params.fetch(:booking_pack, {}).permit!]
  end

  def create_resource(object)

    if params[:booking_pack] && params[:booking_pack][:city_id] == ''
      flash[:city_blank_error] = true
    end
    if params[:booking_pack] && params[:booking_pack][:city_id] == ''
      flash[:hotel_blank_error] = true
    end
    object.save
  end

  def update_resource(object, attributes)

    if params[:booking_pack] && params[:booking_pack][:city_id] == ''
      flash[:city_blank_error] = true
    end

    object.update_attributes(*attributes)
  end

  def prepare_params
    params[:booking_pack][:price].gsub!('R$ ', ' ')
    params[:booking_pack][:price].gsub!('.', '')
    params[:booking_pack][:price].gsub!(',', '.')
  end

end
