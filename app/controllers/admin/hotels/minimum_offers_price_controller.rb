class Admin::Hotels::MinimumOffersPriceController < Admin::BaseController
  actions :show

  def show
    @minimum_price = Offer.where(hotel_id: params[:hotel_id], pack_in_hours: params[:pack_in_hours], status: Offer::ACCEPTED_STATUS[:available]).pluck('MIN(price)').first
    @offers = Offer.where(hotel_id: params[:hotel_id], pack_in_hours: params[:pack_in_hours], price: @minimum_price, status: Offer::ACCEPTED_STATUS[:available]).order(:checkin_timestamp).page(params[:page]).per(40)
  end

  def meeting_show
  	@minimum_price = MeetingRoomOffer.where(hotel_id: params[:hotel_id], pack_in_hours: params[:pack_in_hours], status: MeetingRoomOffer::ACCEPTED_STATUS[:available]).pluck('MIN(price)').first
    @offers = MeetingRoomOffer.where(hotel_id: params[:hotel_id], pack_in_hours: params[:pack_in_hours], price: @minimum_price, status: MeetingRoomOffer::ACCEPTED_STATUS[:available]).order(:checkin_timestamp).page(params[:page]).per(40)
  end

  def event_show
	@minimum_price = EventRoomOffer.where(hotel_id: params[:hotel_id], pack_in_hours: params[:pack_in_hours], status: EventRoomOffer::ACCEPTED_STATUS[:available]).pluck('MIN(price)').first
    @offers = EventRoomOffer.where(hotel_id: params[:hotel_id], pack_in_hours: params[:pack_in_hours], price: @minimum_price, status: EventRoomOffer::ACCEPTED_STATUS[:available]).order(:checkin_timestamp).page(params[:page]).per(40)  	
  end
end
