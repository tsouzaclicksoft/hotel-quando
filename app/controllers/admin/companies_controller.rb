class Admin::CompaniesController < Admin::BaseController
  actions :index, :show, :activate, :deactivate

	def activate
    begin
      company = Company.find(params[:id])
      company.activate!
      company.users.update_all(is_active: true)
      flash['success'] = I18n.t('controllers.admin.companies_controller.activate_success')
    rescue
      flash['error'] = I18n.t('controllers.admin.companies_controller.activate_error')
    end
    redirect_to :back
  end

  def deactivate
    begin
      company = Company.find(params[:id])
      company.deactivate!
      company.users.update_all(is_active: false)
      flash['success'] = I18n.t('controllers.admin.companies_controller.deactivate_success')
    rescue
      flash['error'] = I18n.t('controllers.admin.companies_controller.deactivate_error')
    end
    redirect_to :back
  end

  protected
    def collection
      @companies ||= end_of_association_chain.page(params[:page]).per(40)
    end
end
