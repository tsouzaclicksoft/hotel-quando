class Admin::BookingTaxesController < Admin::BaseController
  actions :index, :edit, :update
  before_action :authorized_master

  def update
    prepare_params
    update! do | success, failure |
      success.html do
        redirect_to admin_booking_taxes_path
      end
    end   
  end
  
  private
  def collection
    @booking_taxes = end_of_association_chain.order('id').includes(:country)
  end

  def prepare_params
    params[:booking_tax][:value].gsub!(' %', '')
    params[:booking_tax][:value].gsub!('US$ ', ' ')
    params[:booking_tax][:value].gsub!(',', '.')
  end

  def build_resource_params
    [params.fetch(:booking_tax, {}).permit!]
  end
end