class Admin::HotelsController < Admin::BaseController
  actions :all, :except => [ :destroy ]
  before_filter :check_colombian_authorization, :only => [:show, :edit, :authenticate_as]
  include Admin::BaseHelper

  def index
    @room_count_per_hotel_id = Room.group(:hotel_id).count
    @meeting_room_count_per_hotel_id = MeetingRoom.group(:hotel_id).count
    @event_room_count_per_hotel_id = EventRoom.group(:hotel_id).count
    #@room_count_per_hotel_id = Offer.select(:room_id).uniq.group(:hotel_id).count
    #@meeting_room_count_per_hotel_id = MeetingRoomOffer.select(:meeting_room_id).uniq.group(:hotel_id).count
    #@event_room_count_per_hotel_id = EventRoomOffer.select(:event_room_id).uniq.group(:hotel_id).count
    @room_count_per_hotel_id.default = 0
    @meeting_room_count_per_hotel_id.default = 0
    @event_room_count_per_hotel_id.default = 0
    index!
  end

  def create
    prepare_params
    create!
  end

  def update
    prepare_params
    update!
  end

  def authenticate_as
    sign_in resource, :bypass => true
    redirect_to hotel_root_path
  end

  private

  def check_colombian_authorization
    hotel = Hotel.find(params[:id])
    if current_admin_manager.master? && current_admin_manager.country == "CO" && hotel.country != 'CO'
      redirect_to admin_hotels_path
    end
  end

  def collection
    if current_admin_manager.country == "BR"
      @hotels = Hotel.includes(:city).order(:name)
    else
      @hotels = Hotel.includes(:city).where(country: current_admin_manager.country).order(:name)
    end
  end

  def build_resource_params
    [params.fetch(:hotel, {}).permit!]
  end

  def create_resource(object)

    if params[:hotel] && params[:hotel][:city_id] == ''
      flash[:city_blank_error] = true
    end
    object.save
  end

  def update_resource(object, attributes)

    if params[:hotel] && params[:hotel][:city_id] == ''
      flash[:city_blank_error] = true
    end
    object.update_attributes(*attributes)
  end

  def prepare_params
    unless params[:citiesHash].blank?
      cities_hash = params[:citiesHash].split(', ')
      params[:hotel][:city_id]  = cities_hash[0]
      params[:hotel][:state]    = cities_hash[2]
      params[:hotel][:country]  = cities_hash[3]
    end
    params[:hotel][:omnibees_config][:accepted_hours_for_3h_pack].keep_if { |i| !i.blank? }
    params[:hotel][:omnibees_config][:accepted_hours_for_6h_pack].keep_if { |i| !i.blank? }
    params[:hotel][:omnibees_config][:accepted_hours_for_9h_pack].keep_if { |i| !i.blank? }
    params[:hotel][:omnibees_config][:accepted_hours_for_12h_pack].keep_if { |i| !i.blank? }
    params[:hotel][:iss_in_percentage].gsub!(' %', '')
    params[:hotel][:iss_in_percentage].gsub!(',', '.')
    params[:hotel][:comission_value].gsub!('R$ ', ' ')
    params[:hotel][:comission_value].gsub!(',', '.')
    #params[:hotel][:fee_on_charge].gsub!('R$ ', ' ')
    #params[:hotel][:fee_on_charge].gsub!(',', '.')
    params[:hotel][:locale]
    params[:hotel][:accept_invoiced_by_agency] = "0" if params[:hotel][:accept_invoiced_payment] == "0"
  end

end
