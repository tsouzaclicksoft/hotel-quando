class Admin::HotelChainsController < Admin::BaseController
  actions :all

  def create
    create! do | success, failure |
      success.html do
        redirect_to admin_hotel_chains_path
      end
    end
  end

  def new
    @hotel_chain = HotelChain.new
  end

  def update
      build_resource_params
      update! do | success, failure |
        success.html do
          redirect_to admin_hotel_chains_path
        end
      end
  end

  protected
    def collection
      @hotel_chains ||= end_of_association_chain.page(params[:page]).per(40)
    end

    def build_resource_params
      [params.fetch(:hotel_chain, {}).permit!]
    end
end
