class Admin::CompanyUniversesController < Admin::BaseController
	has_scope :with_cnpj
	before_action :authorized_master
	
	def create
		prepare_params
		virify_company_already_registered
		create!
	end

	def update
		prepare_params
		virify_company_already_registered
		update!
	end

	def activate
		begin
			CompanyUniverse.find(params[:id]).activate!
			flash['success'] = I18n.t('views.company_universe.region_controller.activate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.region_controller.activate_error')
		end
		redirect_to :back
	end

	def deactivate
		begin
			CompanyUniverse.find(params[:id]).deactivate!
			flash['success'] = I18n.t('views.company_universe.region_controller..deactivate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.region_controller..deactivate_error')
		end
		redirect_to :back
	end

	def build_resource_params
		[params.fetch(:company_universe, {}).permit!]
	end

	def virify_company_already_registered
		@company_detail = CompanyDetail.with_cnpj.where(agency_detail_id: nil).where(cnpj: params[:company_universe][:cnpj]).first
		
		if @company_detail != nil
			params[:company_universe][:company_detail_id] = @company_detail.id
			params[:company_universe][:company_registered_date] = @company_detail.created_at
			params[:company_universe][:company_already_registered] = true
			executive_responsible_id = params[:company_universe][:executive_responsible_id]

			CompanyDetail.update_company_detail_equals_cnpj(executive_responsible_id, @company_detail)

		end

	end

	def prepare_params
		unless params[:citiesHash].blank?
			cities_hash = params[:citiesHash].split(', ')
			params[:company_universe][:city_id]  = cities_hash[0]
		end
		unless params[:companyRegionsHash].blank?
			regions_hash = params[:companyRegionsHash].split(', ')
			params[:company_universe][:company_region_id]  = regions_hash[0]
		end
		unless params[:executiveHash].blank?
			executives_hash = params[:executiveHash].split(', ')
			params[:company_universe][:executive_responsible_id]  = executives_hash[0]
		end
	end

	def collection
      @company_universes ||= CompanyUniverse.where(company_already_registered: false).page(params[:page]).per(40)
    end

end
