class Admin::CodusController < Admin::BaseController
  def objects_list
    @objects = ObjectSpace.each_object.with_object(Hash.new(0)){|obj, h| h[obj.class] +=1 }.to_a.sort{|array| array[1]}
  end
end
