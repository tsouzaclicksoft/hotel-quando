class Admin::CurrenciesController < Admin::BaseController
  actions :all
  before_action :authorized_master_and_hotel_manager

  def index
    if Currency.in_day(DateTime.now).exists?
      @currencies = Currency.in_day(DateTime.now)
    else
      @currencies = []
      Currency.all.order(created_at: :desc).group_by{ | currency | currency.name}.each do |name, currency|
        @currencies << currency.first
      end
    end
  end

  def new
    @currency = Currency.new
  end

  def create
    prepare_params
    create! do | success, failure |
      success.html do
        redirect_to admin_currencies_path
      end
    end
  end

  def update
    if params[:currency]
      prepare_params
      update! do | success, failure |
        success.html do
          redirect_to admin_currencies_path
        end
      end
    else
      prepare_currencies_params
      params[:currencies].each do |currency|
        new_currency = Currency.new(currency[1])
        new_currency.save
      end

      redirect_to admin_currencies_path
    end
  end

  def update_all
    @currencies = Currency.all.order(created_at: :desc).group_by{|currency| currency.name}
  end

  private

  def build_resource_params
    [params.fetch(:currency, {}).permit!]
  end

  def prepare_params
    begin
      params[:currency][:value].gsub!('.','')
      params[:currency][:value].gsub!(',','.')
    rescue
    end
  end

  def prepare_currencies_params
    begin
      params[:currencies].each do| id, currency |
        currency[:value].gsub!('.','')
        currency[:value].gsub!(',','.')
      end
    rescue
    end
  end
end
