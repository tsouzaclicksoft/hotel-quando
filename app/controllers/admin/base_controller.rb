# encoding: utf-8
class Admin::BaseController < ActionController::Base
  protect_from_forgery
  inherit_resources
  layout 'admin'
  before_filter :authenticate_admin_manager!, :show_message_if_there_are_contacts_that_are_not_visualized
  before_action :set_locale
  helper Shared::CurrencyCurrentHelper

  protected

  # def begin_of_association_chain
  #   current_admin_manager
  # end

  def authorized_master
    redirect_to admin_root_path if !current_admin_manager.master?
  end

  def authorized_master_and_hotel_manager
    redirect_to admin_root_path if !current_admin_manager.master? && !current_admin_manager.hotel_manager?
  end

  def authorized_master_and_b2b_commercial
    redirect_to admin_root_path if !current_admin_manager.master? && !current_admin_manager.b2b_commercial?
  end

  def authorized_master_and_b2b_management
    redirect_to admin_root_path if !current_admin_manager.master? && !current_admin_manager.b2b_management?
  end

  def authorized_master_and_attendant
    redirect_to admin_root_path if !current_admin_manager.master? && !current_admin_manager.attendant?
  end

  def authorized_master_and_hotel_manager_and_attendant
    redirect_to admin_root_path if !current_admin_manager.master? && !current_admin_manager.hotel_manager? && !current_admin_manager.attendant?
  end

  def authorized_master_and_b2b_commercial_and_b2b_management
    redirect_to admin_root_path if !current_admin_manager.master? && !current_admin_manager.b2b_commercial? && !current_admin_manager.b2b_management?
  end

  def authorized_master_and_hotel_manager_and_b2b_commercial_and_b2b_management
    redirect_to admin_root_path if !current_admin_manager.master? && !current_admin_manager.hotel_manager? && !current_admin_manager.b2b_commercial? && !current_admin_manager.b2b_management?
  end

  def authorized_master_and_b2b_commercial_and_attendant
    redirect_to admin_root_path if !current_admin_manager.master? && !current_admin_manager.b2b_commercial? && !current_admin_manager.attendant?
  end


  def authenticate
    authenticate_or_request_with_http_basic do |username, password|
      user_and_password = [username, password]
      case user_and_password
      when [ENV["ADMIN_PANEL_USER_LOGIN"], ENV["ADMIN_PANEL_USER_PASSWORD"]]
        session[:admin_type] = "general"
        return true
      when [ENV["ADMIN_PANEL_USER_LOGIN_COP"], ENV["ADMIN_PANEL_USER_PASSWORD_COP"]]
        session[:admin_type] = "cop"
        return true
      end
      #username == ENV["ADMIN_PANEL_USER_LOGIN"] && password == ENV["ADMIN_PANEL_USER_PASSWORD"]
    end
  end

  def show_message_if_there_are_contacts_that_are_not_visualized
    unless session[:message_for_there_contacts_that_are_not_visualized_shown]
      if PossiblePartnerContact.where(admin_visualized: false).count > 0
        flash[:warning] = flash[:warning].to_s + I18n.t('controllers.admin.base_controller.warning')
      end
      session[:message_for_there_contacts_that_are_not_visualized_shown] = true
    end
  end

  def set_locale
    unless action_name == 'timezone'
      if params[:locale] != 'pt_br' || params[:lang] == 'pt-br'
        cookies[:locale] = params[:locale]
      elsif current_admin_manager
        cookies[:locale] = current_admin_manager.locale
      else
        if cookies[:locale].blank?
          cookies[:locale] = set_locale_from_header
        end
      end
    end

    I18n.locale = I18n.available_locales.include?(cookies[:locale].try(:to_sym)) ? cookies[:locale] : I18n.default_locale
  end

  def set_locale_from_header
    request.env['HTTP_ACCEPT_LANGUAGE'].gsub! 'pt-BR', 'pt_BR' unless request.env['HTTP_ACCEPT_LANGUAGE'].blank?
    return http_accept_language.compatible_language_from(I18n.available_locales) || 'en'
  end
end
