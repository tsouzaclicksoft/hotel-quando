class Admin::GoalsExecutiveResponsiblesController < Admin::BaseController
	before_action :authorized_master
	has_scope :by_executive_responsible

	def index
		case params[:commit]
		when t(:export_data)
			send_data(GoalsExecutiveResponsiblesReport.new(collection.per(500)).export.to_stream.read,
				filename: "metas_executivos_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
				type:     "application/xlsx")
		else
			super
		end
	end

	def show
		@goals_executives = GoalsExecutiveResponsible.find(params[:id])
	end

	def collection
		@goals_executives ||= apply_scopes(GoalsExecutiveResponsible).all.page(params[:page]).per(40)
	end


end
