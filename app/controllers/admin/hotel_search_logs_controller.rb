# encoding: utf-8
class Admin::HotelSearchLogsController < Admin::BaseController
  actions :index

  def index
    case params[:commit]
    when t(:export_data)
      send_data(HotelSearchLogsReport.new(collection.per(4000)).export.to_stream.read,
        filename: "hotel_search_logs_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
        type:     "application/xlsx")
    else
      super
    end
  end

  protected
    def collection
      @hotel_search_logs ||= end_of_association_chain.order("id DESC").page(params[:page]).per(50)
    end  
end
