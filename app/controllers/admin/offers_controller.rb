# encoding: utf-8
class Admin::OffersController < Admin::BaseController
  actions :index
  before_action :authorized_master
  
  has_scope :by_month_of_a_year, :using => [:month, :year]
  has_scope :by_status, type: :array
  has_scope :by_hotel_id
  has_scope :by_pack_in_hours, type: :array
  has_scope :by_checkin_range, :using => [:initial_date, :final_date]

  def collection
    @offers = end_of_association_chain.where(status: [
      Offer::ACCEPTED_STATUS[:available],
      Offer::ACCEPTED_STATUS[:suspended],
      Offer::ACCEPTED_STATUS[:reserved]
    ]).includes(:room, :room_type, :hotel).page(params[:page]).per(50)
  end

end
