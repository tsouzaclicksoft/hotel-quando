class Admin::CitiesController < Admin::BaseController
  actions :new, :create
  before_action :authorized_master_and_hotel_manager

  def create
    create! do | success, failure |
      success.html do
        redirect_to admin_states_path
      end
    end   
  end

  private

  def collection
    @states ||= end_of_association_chain.includes(:state).order('name').page(params[:page]).per(40)
  end

  def build_resource_params
    [params.fetch(:city, {}).permit!]
  end
end