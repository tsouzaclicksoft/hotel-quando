# encoding: utf-8
class Admin::InfoController < Admin::BaseController
  actions :none

  def update_password
    if current_admin_manager.update_with_password(manager_params)
      sign_in(current_admin_manager, :bypass => true)
      flash[:success] = I18n.t('controllers.hotel.info_controller.upate_password')
      redirect_to admin_root_path
    else
      render "edit"
    end

  end

  private

  def manager_params
    params.required(:manager).permit(:password, :password_confirmation, :current_password)
  end

end
