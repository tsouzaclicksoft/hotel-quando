class Admin::ExecutiveResponsiblesController < Admin::BaseController
	before_action :authorized_master
	def create
		create! do | success, failure |
			success.html do
				GoalsExecutiveResponsible.new_goals_executive_responsible(@executive_responsible)
				ExecutiveResponsibleMailer.send_login_and_password(@executive_responsible).deliver
				redirect_to admin_executive_responsible_path(@executive_responsible)
			end
		end
	end

	def update
		build_resource_params
		update!
		GoalsExecutiveResponsible.update_goals_executive_responsible(params)
	end

	def activate
		begin
			ExecutiveResponsible.find(params[:id]).activate!
			flash['success'] = I18n.t('views.company_universe.executive_controller.activate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.executive_controller.activate_error')
		end
		redirect_to :back
	end

	def deactivate
		begin
			ExecutiveResponsible.find(params[:id]).deactivate!
			flash['success'] = I18n.t('views.company_universe.executive_controller..deactivate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.executive_controller..deactivate_error')
		end
		redirect_to :back
	end

	def build_resource_params
		[params.fetch(:executive_responsible, {}).permit!]
	end

	def collection
      @executive_responsibles ||= ExecutiveResponsible.all.page(params[:page]).per(40)
    end
end