# encoding: utf-8
class Admin::PaymentsController < Admin::BaseController
  actions :index, :show
  before_filter :check_colombian_authorization, :only => [:show, :edit]
  include Admin::BaseHelper
  has_scope :by_id
  has_scope :by_booking_id
  has_scope :by_user_id
  has_scope :by_credit_card_id
  has_scope :by_status

  protected

    def collection
      if current_admin_manager.country == "BR"
        @payments ||= end_of_association_chain.page(params[:page]).per(40)
      else
        @payments ||= end_of_association_chain.joins(:user).where('users.country' => current_admin_manager.country).page(params[:page]).per(40)
      end
    end

  private
  def check_colombian_authorization
    payment = Payment.find(params[:id])
    if current_admin_manager.master? && current_admin_manager.country == "CO" && payment.user.country != 'CO'
      redirect_to admin_payments_path
    end
  end

end
