# encoding: utf-8
class Agency::BookingsReportController < Agency::BaseController
  QUANTITY_OF_MONTHS = 12
  include BookingReportsConcern
  actions :index

  def index
    if request.post?
      @report = invoice_for(current_agency_agency, params[:report_type], params[:tenth_day_in_report_month].to_time)
      respond_to do | format |
        format.html
        format.xls
      end
    end
  end
end
