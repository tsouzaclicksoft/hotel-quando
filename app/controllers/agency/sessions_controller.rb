# encoding: utf-8
class Agency::SessionsController < Devise::SessionsController
  layout 'agency/login'

  def create
    agency = Agency.find_by_login(params[:agency_agency].try(:[], :login))
    if agency.nil? || agency.try(:is_active?)
      super # if the agency is not found or the agency is active, we call the default devise action, else the agency is inactive and we display a custom message
    else
      flash['alert'] = "Sua agência foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_agency_agency_session_path
    end
  end

  def after_sign_in_path_for(agency)
    agency_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    agency_root_path
  end
end
