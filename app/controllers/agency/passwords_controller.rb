class Agency::PasswordsController < Devise::PasswordsController
  layout 'agency/login'

  def create
    change_login_for_email_in_params
    super
  end

  def after_resetting_password_path_for(agency)
    hotel_root_path
  end

  private
  def change_login_for_email_in_params
    params[resource_name]['email'] = resource_class.find_by_login(params[resource_name]['login']).email
    params[resource_name].delete 'login'
  end
end
