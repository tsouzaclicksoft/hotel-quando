# encoding: utf-8
class Agency::BookingsController < Agency::BaseController
  actions :index, :show
  has_scope :by_id
  has_scope :by_status
  has_scope :by_company_id
  has_scope :by_checkin_date, using: [:initial_date, :final_date]
  has_scope :by_checkout_date, using: [:initial_date, :final_date]

  def cancel
    @booking = current_agency_agency.bookings.where(created_by_agency: true).find(params[:id])

    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking, agency_cancelation: true)
    if booking_cancellation_response.success
      flash[:notice] = booking_cancellation_response.message
    else
      flash[:error] = booking_cancellation_response.message
    end

    redirect_to agency_bookings_path
  end

  protected
    def end_of_association_chain
      association_chain = super
      return association_chain.where(created_by_agency: true)
    end

    def collection
      @bookings ||= end_of_association_chain.order('checkin_date DESC').includes(:hotel).includes(:company).page(params[:page]).per(40)
    end

end
