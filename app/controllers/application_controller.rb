class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  helper PublicAffiliate::BaseHelper
  protect_from_forgery with: :exception
  helper_method :last_search_log
  protect_from_forgery with: :exception
  after_action  :set_access_control_headers
  skip_before_action :verify_authenticity_token

  def set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
  end

  def last_search_log
    begin
      @last_search_log ||= HotelSearchLog.find(session[:current_search_log_id])
    rescue
    end
  end

  def authenticate_request!
    puts current_public_user
    unless user_id_in_token? || current_public_user
      respond_to do |format|
        format.html { redirect_to new_user_session_path }
        format.json { render json: { errors: ['Not Authenticated'] }, status: :unauthorized }
      end
      return
    end
    if current_public_user
      @current_public_user = current_public_user
    else
      @current_public_user = User.find(auth_token[:user_id])
    end
    rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  def ssl_required?
    true
  end

  private
  def http_token
    @http_token ||= if request.headers['Authorization'].present?
      request.headers['Authorization'].split(' ').last
    end
  end

  def auth_token
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def user_id_in_token?
    http_token && auth_token && auth_token[:user_id].to_i
  end

end
