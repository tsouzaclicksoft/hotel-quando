# encoding: utf-8
class Affiliate::InvoicesController < Affiliate::BaseController
  actions :all, :except => [ :destroy, :edit, :update ]

  private
    def build_resource_params
      [params.fetch(:invoice, {}).permit!]
    end
end
