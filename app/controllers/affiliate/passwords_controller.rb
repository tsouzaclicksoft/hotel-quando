# encoding: utf-8
class Affiliate::PasswordsController < Devise::PasswordsController
  layout 'affiliate/login'

  def create
    change_login_for_email_in_params
    super
  end

  def after_resetting_password_path_for(affiliate)
    affiliate_root_path
  end

  private
  def change_login_for_email_in_params
    params[resource_name]['email'] = resource_class.find_by_login(params[resource_name]['login']).email
    params[resource_name].delete 'login'
  end
end
