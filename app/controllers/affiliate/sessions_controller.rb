# encoding: utf-8
class Affiliate::SessionsController < Devise::SessionsController
  layout 'affiliate/login'

	def create
    affiliate = Affiliate.find_by_login(params[:affiliate_affiliate].try(:[], :login))
    if affiliate.nil? || affiliate.try(:is_active?)
      super # if the affiliate is not found or the affiliate is active, we call the default devise action, else the affiliate is inactive and we display a custom message
    else
      flash['alert'] = "Sua afiliação foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_affiliate_affiliate_session_path
    end
  end

  def after_sign_in_path_for(affiliate)
    affiliate_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    affiliate_root_path
  end
end
