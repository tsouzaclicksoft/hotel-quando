# encoding: utf-8
class PublicAffiliate::HotelSearchLogsController < PublicAffiliate::BaseController
  def set_email_for_last_log
    @search_log_user = SearchLogsUser.where(email: params[:subscribe_search][:email]).first_or_initialize
    if @search_log_user.save
      last_search_log.search_logs_user_id = @search_log_user.id
      last_search_log.save
    end
  end
end
