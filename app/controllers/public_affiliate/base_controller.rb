# encoding: utf-8
class PublicAffiliate::BaseController < ActionController::Base
  CONTROLLERS_TO_AVOID_LOG_PATH = ['sessions', 'passwords', 'registrations', 'confirmations']
  protect_from_forgery
  layout 'public_affiliate'
  before_filter :set_affiliate
  before_filter :log_last_public_path_unless_devise_controllers
  before_action :set_locale unless Rails.env.test?
  before_action :set_currency
  after_action  :allow_iframe
  helper_method :last_search_log
  helper_method :total_price_for_number_of_people
  helper Shared::CurrencyCurrentHelper

  def current_public_affiliate_user
    super
    if @current_public_affiliate_user.present?
      @current_public_affiliate_user = User::BudgetDecorator.decorate(@current_public_affiliate_user)
    end
    @current_public_affiliate_user
  end

  def total_price_for_number_of_people(number_of_people, offer)
    mock_booking = Booking.new
    mock_booking.user = current_public_affiliate_user
    set_booking_cached_attributes(mock_booking, offer)
    mock_booking.number_of_people = number_of_people
    mock_booking.hotel = offer.hotel
    mock_booking.offer_id = offer.id
    mock_booking.total_price_without_tax
  end

  protected

  def begin_of_association_chain
    current_public_affiliate_user
  end

  def last_search_log
    begin
      @last_search_log ||= HotelSearchLog.find(session[:current_search_log_id])
    rescue
    end
  end

  private

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  def set_affiliate
    if !params[:token].blank?
        @affiliate = Affiliate.where(token: params[:token], is_active: true).first
      if !@affiliate.blank?
        session[:affiliate] = @affiliate
      else
        render :file => "#{Rails.root}/public/404.html",  :status => 404
      end

    elsif !session[:affiliate].blank?
      @affiliate = session[:affiliate]

    else
      render :file => "#{Rails.root}/public/404.html",  :status => 404

    end

  end

  def set_booking_cached_attributes(booking, offer)
    booking.cached_room_type_initial_capacity = offer.room_type_initial_capacity
    booking.cached_room_type_maximum_capacity = offer.room_type_maximum_capacity
    booking.cached_offer_price = offer.price
    booking.cached_offer_extra_price_per_person = offer.extra_price_per_person
  end

  def set_locale
    if current_public_affiliate_user || params[:locale] != 'en'
      if current_public_affiliate_user
        cookies[:locale] = current_public_affiliate_user.locale
      end

      if params[:locale]
        cookies[:locale] = params[:locale]
      end

      if ( 'en-US' == request.original_url.slice(request.original_url.length-5, request.original_url.length) )
        cookies[:locale] = 'en'
      end
    else
      if ( 'en-US' == request.original_url.slice(request.original_url.length-5, request.original_url.length) )
        cookies[:locale] = 'en'
      else
        cookies[:locale] = set_locale_from_header
      end
    end

    I18n.locale = I18n.available_locales.include?(cookies[:locale].try(:to_sym)) ? cookies[:locale] : I18n.default_locale
  end

  def set_locale_from_header
    request.env['HTTP_ACCEPT_LANGUAGE'].gsub! 'pt-BR', 'pt_BR'
    return http_accept_language.compatible_language_from(I18n.available_locales)
  end

  def log_last_public_path_unless_devise_controllers
    namespace, controller = params[:controller].split("/") # namespace is always 'public'
    session[:last_path] = request.fullpath unless (CONTROLLERS_TO_AVOID_LOG_PATH.include? controller)
  end
  def set_currency_by_locale
    if cookies[:locale] == :pt_br || cookies[:locale] == 'pt_br'
      return Currency.where(country: 'BR').first.money_sign
    else
      return Currency.where(country: 'US').first.money_sign
    end
  end

  def set_currency
    if params[:currency]
      cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
    elsif cookies[:currency].nil?
      cookies[:currency] = set_currency_by_locale
    end
  end
end
