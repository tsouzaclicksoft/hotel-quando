class PublicAffiliate::Api::V1::HotelChainsController < PublicAffiliate::Api::V1::BaseController

  def hotel_groups
    if params[:name].present?
      @hotel_groups = HotelChain.where("name ILIKE ?", "%#{params[:name]}%").select(:id, :name)
    else
      @hotel_groups = HotelChain.all
    end
  end

  def hotels_by_hotel_groups
    @hotel_group = HotelChain.where(id: params[:id])

    if @hotel_group.blank?
      render :json => { :msg => t(:hotel_chain_doesnt_exist), :error => 404 }
    else
      @hotels = Hotel.where(hotel_chain_id: @hotel_group.first.id).all

     #  render :json => {
     #   hotel_group_name: hotel_group.first.name,
     #   hotels: hotels
     # }
    end
  end
end
