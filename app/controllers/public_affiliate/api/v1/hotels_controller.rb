# encoding: utf-8
class PublicAffiliate::Api::V1::HotelsController < PublicAffiliate::Api::V1::BaseController
  include Shared::CurrencyCurrentHelper

  def index
    @hotels = []
    begin
	    check_in_datetime = "#{params[:by_checkin_date]} #{params[:by_checkin_hour]}:00 -03:00".to_datetime
	    #raise CheckinDateIsInThePastError if check_in_datetime < (DateTime.now)
	    if check_in_datetime < (DateTime.now)
	    	render :json => { :msg => t(:error_message_selected_checkin_datetime_is_in_the_past) }
	    else
		    @searched_address_coordinates = [params[:by_latitude], params[:by_longitude]]
		    @hotel_search = OffersSearch::Hotels.new({
		    	number_of_guests: params[:by_number_of_people].to_i,
		        latitude: params[:by_latitude].to_f,
		        longitude: params[:by_longitude].to_f,
		        check_in_datetime: check_in_datetime,
		        pack_in_hours: params[:by_pack_length].to_i,
		        room_category: params[:room_category].to_i
		    	})
	#	    @hotels = HotelDistanceSorter.sort(@hotel_search.results.exacts, @searched_address_coordinates)

				@search_exact_match = HotelDistanceSorter.sort(@hotel_search.results.exacts, @searched_address_coordinates)
				#@search_similar_match = HotelDistanceSorter.sort(@hotel_search.results.similars, @searched_address_coordinates)
				@hotels = @search_exact_match.collect {|x| x[:hotel]}
				@min_offer_exact = @search_exact_match.collect {|x| x[:min_offer]}
				@distance_exact = @search_exact_match.collect {|x| x[:distance]}
				#hotel_exact_temp.each_with_index.map {|x,i| test << [:hotel => x.hotel, :offer => x.min_offer, :distance => x.distance ]}

				#@hotel_similar = @search_similar_match.collect {|x| x[:hotel]}
				#@min_offer_similar = @search_similar_match.collect {|x| x[:min_offer]}
				#@distance_similar = @search_similar_match.collect {|x| x[:distance]}
		    begin
			    log.exact_match_hotels_ids = @hotels_exact_match.to_a.map(&:id).join("-")
			    #log.similar_match_hotels_ids = @hotels_similar_match.to_a.map(&:id).join("-")
			    log.save
				rescue
				end
			end
    #rescue CheckinDateIsInThePastError
      #render :json => { :msg => t(:error_message_selected_checkin_datetime_is_in_the_past) }.to_json, :status => 500
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      #render :json => { :msg => 'Date or time seems in the past' }.to_json, :status => 500
			render :json => { :msg => t(:error_message_search_params_error_call_support_or_try_again) } #, :status => 500
    end
  end

  def show
    begin
      if params[:checkin_timestamp] #user is comming from booking
        checkin_datetime = params[:checkin_timestamp].to_datetime
        params[:length_of_pack] = params[:pack_in_hours]
        params[:checkin_date] = checkin_datetime.to_date
        params[:checkin_hour] = checkin_datetime.hour
      else
		  	params[:length_of_pack] = params[:by_pack_length]
		  	params[:checkin_date] = params[:by_checkin_date]
		  	params[:checkin_hour] = params[:by_checkin_hour]
	  		params[:number_of_people] = params[:by_number_of_people]
        checkin_datetime = "#{params[:checkin_date]} #{params[:checkin_hour]}:00 -03:00".to_datetime
      end

      if checkin_datetime < (DateTime.now + 1.hour)
      	render :json => { :msg => t(:error_message_offer_not_available) }
      else
	      #raise OfferNotAvailable if checkin_datetime < (DateTime.now + 1.hour)

	      @hotel = Hotel.includes(:city).includes(room_types: :photos).find(params[:id])
	      session[:last_hotel_visited_id] = @hotel.id
	      pack_in_hours = params[:length_of_pack].to_i

        if @hotel.is_omnibees_or_hoteisnet?
          choice = ChooseOmnibeesOrHoteisnet.choose_based_on_time_now( pack_in_hours, checkin_datetime )
          if choice == 1
            @room_type_search = DataSources::MinOfferPerRoomType::Hotelnet.new({
    	        number_of_guests: params[:number_of_people].to_i,
    	        hotel_id: @hotel.id,
    	        check_in_datetime: checkin_datetime,
    	        pack_in_hours: pack_in_hours
    	      })
          elsif choice == 2
            @room_type_search = DataSources::MinOfferPerRoomType::Omnibees.new({
              number_of_guests: params[:number_of_people].to_i,
              hotel_id: @hotel.id,
              check_in_datetime: checkin_datetime,
              pack_in_hours: pack_in_hours
            })
          end
        else
          @room_type_search = DataSources::MinOfferPerRoomType::HQ.new({
  	        number_of_guests: params[:number_of_people].to_i,
  	        hotel_id: @hotel.id,
  	        check_in_datetime: checkin_datetime,
  	        pack_in_hours: pack_in_hours
  	      })
        end

        @room_type_available_offers = []
        if @room_type_search.results.inspect.include?('exact_offers_by_room_type')
          @room_type_available_offers = @room_type_search.results.exact_offers_by_room_type
        end
	      @search_exact_match = @room_type_search.results.exacts.sort_by { |s|
	        offer = s[:min_offer]
	        offer.save! if offer.is_omnibees?
	        offer.reload #we need all the fields to calculate value for the entire number of guests
	        s[:min_offer].price
	      }
	    end

#        @search_similar_match = @room_type_search.results.similars.sort_by { |s|
#          offer = s[:min_offer]
#          offer.save! if offer.is_omnibees?
#          [ offer.checkin_timestamp,
#            offer.get_number_of_common_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)),
#            (offer.checkin_timestamp - checkin_datetime),
#            -(offer.get_number_of_exceeding_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)))
#          ]
#        }
=begin
      @search = @search_exact_match  #+ @search_similar_match
      @min_offer = @search.collect {|x| x[:min_offer]}
      @hotel = Hotel.where(id: @min_offer.collect {|h| h[:hotel_id]}.uniq) # collecting uniq hotel_id values
      @room_ids = @min_offer.collect {|r| r[:room_id]}
      @room_type_ids = @min_offer.collect {|r| r[:room_type_id]}
      @rt = RoomType.where('id IN (?)', @room_type_ids)
=end
    #rescue OfferNotAvailable
      #render :json => { :msg => t(:error_message_offer_not_available) } #, :status => 500
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      render :json => { :msg => t(:error_message_search_params_error_call_support_or_try_again) } #, :status => 500
    end


  end
=begin
  def show
    @hotel = Hotel.find(params[:id])

  end
=end


  def offers_per_hotel
    page = params[:page].present? ? params[:page] : 0
    data_search = params[:by_checkin_date]
    @currency_code = current_currency.code
    hotel_name = Hotel.where(name: params[:name].to_s).first if params[:name].present?
    hotel_name = Hotel.find(params[:id]) if params[:id].present?
    hotel_id = params[:id].present? ? params[:id] : hotel_name.id
    pack_in_hours = params[:by_pack_length].present? ? params[:by_pack_length] : 3
    @offers = Offer.where(hotel_id: hotel_id, checkin_timestamp: data_search.to_datetime.at_beginning_of_day...data_search.to_datetime.at_end_of_day, pack_in_hours: params[:by_pack_length])
    .order(:pack_in_hours, :checkin_timestamp).page(page).per(20)
    respond_to do |format|
      format.json{ @offers }
    end
  end

  def offers_per_city
    @currency_code = current_currency.code
    city = City.where(name: params[:city].titleize.to_s)
    hotel_ids = Hotel.where(city: city).pluck(:id)
    data_search = params[:date].present? ? params[:date] : DateTime.now
    page = params[:page].present? ? params[:page] : 0

    @offers = Offer.where(hotel_id: hotel_ids, checkin_timestamp: data_search.to_datetime.at_beginning_of_day...data_search.to_datetime.at_end_of_day)
    .order(:pack_in_hours, :checkin_timestamp).page(page).per(20)
    respond_to do |format|
      format.json { @offers }
    end
  end

  def offers_per_price
    @currency_code = current_currency.code
    hotel_name = Hotel.where(name: params[:hotel_name].to_s).first if params[:hotel_name].present?
    hotel_id = params[:id].present? ? params[:id] : hotel_name.id
    data_search = params[:date].present? ? params[:date] : DateTime.now

    @offers = Offer.where("hotel_id = ? AND price >= ? AND price <= ? AND checkin_timestamp = ?",
    hotel_id,
    params[:min_value],
    params[:max_value],
    data_search.to_datetime.at_beginning_of_day...data_search.to_datetime.at_end_of_day)
    respond_to do |format|
      format.json{ @offers }
    end
  end

  def countries
    if params[:country].present? && params[:state].present?
      state_id = State.where("name = :state OR initials = :state", state: params[:state]).first
      @data = City.where(state_id: state_id)
    elsif params[:country].present?
      country_id = Country.where("name_pt_br LIKE :country OR name_en LIKE :country OR name_es LIKE :country OR iso_initials LIKE :country", country: params[:country]).first.id
      @data = State.where(country_id: country_id)
    else
      @data = Country.all
    end
    respond_to do |format|
      format.json{ @data }
    end
  end

  def hotel_list
    @hotel = Hotel.all
    respond_to do |format|
      format.json { @hotel }
    end
  end

  def hotel_list_by_city
    city_name = get_correct_city_name(params[:city]) if params[:city].to_i <= 0
    city = City.where("name ILIKE :name", name: city_name).first if city_name.present?
    city = City.where("id = :id", id: params[:city]).first if params[:city].to_i > 0
    @hotels = Hotel.where(city_id: city.id)
    respond_to do |format|
      format.json { @hotels }
    end
  rescue => ex
    Rails.logger.error(ex)
    Airbrake.notify(ex)
    render :json => { :msg => t(:error_message_search_params_error_call_support_or_try_again) } #, :status => 500
  end

  helper_method :get_timezone
  def get_timezone(latitude, longitude)
    hotel_timezone = {}
    timezone = Timezone.lookup(latitude, longitude)
    offset_in_hours = (TZInfo::Timezone.get(timezone.name).current_period.offset.utc_total_offset) / 3600.0
    offset_in_seconds = (TZInfo::Timezone.get(timezone.name).current_period.offset.utc_total_offset)
    hotel_timezone[:timezone_name] = timezone.name
    hotel_timezone[:offset_in_hours] = offset_in_hours
    hotel_timezone[:offset_in_seconds] = offset_in_seconds
    hotel_timezone
  end

  private
    def get_correct_city_name(city)
      encoded_url = URI.encode("http://maps.googleapis.com/maps/api/geocode/json?address=#{params[:city].downcase}&language=en&sensor=false")
      response = HTTParty.get(encoded_url)
      response.flatten[1][0].flatten[1][1].flatten[1]
    end

end
