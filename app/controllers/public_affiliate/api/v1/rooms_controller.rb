# encoding: utf-8
class PublicAffiliate::Api::V1::RoomsController < PublicAffiliate::Api::V1::BaseController

  def index
    @rooms = Hotel.find(params[:hotel_id]).rooms
  end

  def show
    hotel = Hotel.find(params[:hotel_id])
    @room = hotel.rooms.find(params[:id])
  end

end
