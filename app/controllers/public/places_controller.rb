class Public::PlacesController < Public::BaseController
  before_action {flash.clear}

  def index_airport
    @countries = Country.all.map(&:name_pt_br) + Country.all.map(&:name_en) + Country.all.map(&:name_es)
    city = City.where(name: params[:filter_by_city]).first
    if city.present?
      @airports = Place.where(place_type: 1).where.not(hotel_count: 0).where(active: true).where(city_id: city.id).order("hotel_count DESC").page(params[:page]).per(15)
      airport_error_message(params[:filter_by_city]) unless @airports.present?
    else
      @airports = Place.where(place_type: 1).where.not(hotel_count: 0).where(active: true).order("hotel_count DESC").page(params[:page]).limit(15)
    end
  end

  def index_city
    @countries = Country.all.map(&:name_pt_br) + Country.all.map(&:name_en) + Country.all.map(&:name_es)
    state = State.where("name LIKE :state", state: params[:filter_by_state]).first
    if state.present?
      @cities = Place.where(place_type: 2).where.not(hotel_count: 0).where(active: true).where(city: City.where(state: state)).order("hotel_count DESC").page(params[:page]).per(15)
      city_error_message(params[:filter_by_state]) unless @cities.present?
    else
      @cities = Place.where(place_type: 2).where.not(hotel_count: 0).where(active: true).order("hotel_count DESC").page(params[:page]).per(15)
    end
  end

  def airports
    @airport = Place.where(id: params[:id]).first
    @main_airports = Place.where(place_type: 1).where(active: true).where.not(hotel_count: 0).order('hotel_count DESC').limit(20)
    distance = params[:distance]
    if distance.present?
      @hotels = Hotel.where(id: @airport.hotel_id) 
      distance_error_message(distance) if @hotels.count == 0
    else
      @hotels = Hotel.where(id: @airport.hotel_id)    
      distance_error_message(@airport.distance) if @hotels.count == 0
    end
    @hotels_ordered_distance = @hotels.map {|hotel| {hotel: hotel, distance: nil}}
    @hotels_ordered_distance = HotelDistanceSorter.sort(@hotels_ordered_distance, [@airport.latitude, @airport.longitude])
    @hotel_array = @hotels_ordered_distance.map{|h| h[:hotel]}
    @hotel_list = Kaminari.paginate_array(@hotel_array).page(params[:page]).per(18)     
  end

  def cities
    city = Place.where(id: params[:id]).first.city
    @main_regions = Place.where(place_type: 3).where(city_id: city).where(active: true).where.not(hotel_count: 0).order('hotel_count DESC').limit(20)
    @place = Place.where(id: params[:id]).first
    @hotels = Hotel.where(id: @place.hotel_id)
    @hotels_ordered_distance = @hotels.map {|hotel| {hotel: hotel, distance: nil}}
    @hotels_ordered_distance = HotelDistanceSorter.sort(@hotels_ordered_distance, [@place.latitude, @place.longitude])
    @hotel_array = @hotels_ordered_distance.map{|h| h[:hotel]}
    @hotel_list = Kaminari.paginate_array(@hotel_array).page(params[:page]).per(18)
    city_error_message(@place.send("name_#{I18n.locale}")) if @hotels.length == 0
  end

  def regions
    @place = Place.where(id: params[:id]).where(active: true).first
    @hotels = Hotel.where(id: @place.hotel_id).page(params[:page]).per(12)
  end

  def json_states
    country_id = Country.where("name_pt_br LIKE :country OR name_en LIKE :country OR name_es LIKE :country", country: params[:country]).first.id
    @states = State.where(country_id: country_id)
    respond_to do |format|
      format.json { @states }
    end
  end

  def json_cities
    state_id = State.where(country_id: Country.where("name_pt_br LIKE :country OR name_en LIKE :country OR name_es LIKE :country", country: params[:country]).first.id)
    cities = City.where(state_id: state_id).map(&:name)
    @cities = OpenStruct.new(name: [])
    cities.each do |city|
      @cities.name << [city]
    end
    respond_to do |format|
      format.json { @cities }
    end
  end

  private
    def airport_error_message(place)
      flash['error'] = I18n.t(:airport_not_found, place: place)
    end

    def city_error_message(place)
      flash['error'] = I18n.t(:city_not_found, city: place)
    end

    def distance_error_message(airport_distance)
      flash['error'] = I18n.t(:hotel_not_found, default_distance: airport_distance)
    end
end
