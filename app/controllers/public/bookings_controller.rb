# encoding: utf-8
class Public::BookingsController < Public::BaseController
  include ActionView::Helpers::NumberHelper
  include Public::UserHelper
  include Shared::CurrencyCurrentHelper

  inherit_resources
  actions :index, :update
  before_filter :authenticate_public_user!
  has_scope :by_status
  has_scope :by_client_id
  has_scope :by_traveler_id
  has_scope :by_pack_length, type: :array
  has_scope :by_checkin_date, using: [:initial_date, :final_date]
  has_scope :by_cost_center_id
  has_scope :by_consultant_id
  helper_method :total_price_for_number_of_people_dropdpwn

  def index
    case params[:commit]
    when t(:export_data)
      if current_public_user.is_a_agency?
        send_data(BookingsAgenciesReport.new(collection.per(collection.total_count)).export.to_stream.read,
          filename: "reserva_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
          type:     "application/xlsx")
      else
        send_data(BookingsCompaniesReport.new(collection.per(collection.total_count)).export.to_stream.read,
          filename: "reserva_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
          type:     "application/xlsx")
      end
    else
      super
    end
  end

  def cancel
    @booking = current_public_user.bookings.find(params[:id])

    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking)
    if booking_cancellation_response.success
      flash[:notice] = booking_cancellation_response.message
    else
      flash[:error] = booking_cancellation_response.message
    end

    redirect_to public_bookings_path
  end


  def new
    @booking = Booking.new(number_of_people: params[:number_of_people])
    @business_room = BusinessRoom.find_by id: (params[:business_room_id])
    if @business_room
      business_type = @business_room.type
=begin # code working meeting room and event room
      offer_class = (business_type + "Offer").constantize
      @offer = offer_class.where("#{business_type.foreign_key} = #{params[:business_room_id]}")
                          .where(checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                             pack_in_hours: params[:pack_in_hours], price: params[:price], \
                             status: offer_class::ACCEPTED_STATUS[:available]).first
=end

      @offer = MeetingRoomOffer.where(meeting_room_id: params[:business_room_id])
                          .where(checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                             pack_in_hours: params[:pack_in_hours], price: params[:price], \
                             status: MeetingRoomOffer::ACCEPTED_STATUS[:available]).first
    else
      @offer = OmnibeesOffer.where(room_type_id: params[:room_type_id], \
                             checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                             pack_in_hours: params[:pack_in_hours], price: params[:price]).first
      @offer ||= Offer.where(room_type_id: params[:room_type_id], \
                             checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                             pack_in_hours: params[:pack_in_hours], price: params[:price], \
                             status: Offer::ACCEPTED_STATUS[:available]).first
    end

    if @offer && (@offer.available?)
      @booking.offer = @offer
      set_current_offer_id @offer.id
      set_current_offer_type @offer.class.name
      set_current_offer_price @offer.price
    else
      flash[:error] = t(:error_message_offer_not_available)
      redirect_to public_hotel_path(params.merge(id: session[:last_hotel_visited_id]))
    end
  end

  def create
    begin
      @booking = Booking.new(params[:booking].permit(:guest_name, :number_of_people, :note, :guest_email, :have_promotional_code, :consultant_id, :traveler_id, :phone_number, :using_balance))
      @offer = get_current_offer_type.constantize.find get_current_offer_id

      if current_public_user.is_a_agency? || current_public_user.is_a_company?
        create_consultant() if params[:new_consultant].eql?("true")
        create_traveler_and_company() if params[:new_traveler_and_company].eql?("true")
        create_traveler() if params[:new_traveler].eql?("true")
      else
        if current_public_user.need_to_complete_address?
          current_public_user.update(country: params[:public_user][:country], state: params[:public_user][:state], city: params[:public_user][:city], street: params[:public_user][:street], postal_code: params[:public_user][:postal_code])
          current_public_user.errors.add(:country, I18n.t('errors.messages.blank')) if current_public_user.country.blank?
          current_public_user.errors.add(:state, I18n.t('errors.messages.blank')) if current_public_user.state.blank?
          current_public_user.errors.add(:city, I18n.t('errors.messages.blank')) if current_public_user.city.blank?
          current_public_user.errors.add(:street, I18n.t('errors.messages.blank')) if current_public_user.street.blank?
          current_public_user.errors.add(:postal_code, I18n.t('errors.messages.blank')) if current_public_user.postal_code.blank?
        end
        if (current_public_user.country == "BR" || current_public_user.country.blank?) && current_public_user.cpf.blank?
          current_public_user.update(cpf: params[:public_user][:cpf])
          current_public_user.errors.add(:cpf, I18n.t('errors.messages.blank')) if current_public_user.cpf.blank?
        end
      end
      if current_public_user.errors.present?
        render :new
      elsif @consultant.try(:errors).present? || @company.try(:errors).present? || @traveler.try(:errors).present? || @cost_center.try(:errors).present? || (@credit_card.try(:errors).present? && !current_public_user.accept_confirmed_and_invoiced)
        render :new
      elsif (current_public_user.is_a_agency? || current_public_user.is_a_company?) && @booking.traveler.blank?
        flash[:error] = "#{t(:traveler)} #{I18n.t('errors.messages.blank')}"
        render :new
      elsif @booking.traveler.present? && current_currency.code != 'BRL' && @booking.traveler.need_to_complete_address? && !current_public_user.accept_confirmed_and_invoiced
        flash[:error] = "Para realizar transação em USD é necessário preencher todos os campos de endereço corretamente"
        redirect_to edit_public_traveler_path(@booking.traveler)
      elsif @booking.traveler.present? && (@booking.traveler.country == "BR") && @booking.traveler.cpf.blank?
        flash[:error] = "Para realizar transação em BRL é necessário preencher o CPF"
        redirect_to edit_public_traveler_path(@booking.traveler)
      else
        @booking.user_id = current_public_user.id
        @booking.hotel_comments = @offer.hotel.send("description_#{I18n.locale}")
        @booking.pack_in_hours = @offer.pack_in_hours
        @booking.company_id = current_public_user.company_id
        @booking.status = Booking::ACCEPTED_STATUSES[:waiting_for_payment]
        @booking.created_by_agency = current_public_user.agency_detail.present?
        @booking.checkin_date = @offer.checkin_timestamp
        @booking.currency_code = current_currency.code
        @booking.using_balance = false if (@booking.currency_code != "BRL" && @booking.user.balance <= 0)
        @booking.is_app_mobile = false

        if current_executive_responsible_executive_responsible.present?
          @booking.executive_responsible_id = current_executive_responsible_executive_responsible.id
        end  

        booking_create_response = BookingMaker.call(offer: @offer, booking: @booking)

        if booking_create_response.success
          current_public_user.update(emkt_flux: 2, emkt_day_of_week: Date.current.wday) if current_public_user.emkt_flux == 1
          redirect_to new_public_booking_payments_path(booking_create_response.booking.id)
        elsif booking_create_response.message
          #SendUserDataRdsWorker.perform_async(current_public_user.id, @offer.hotel.name, @offer.hotel.city.name, "Publico - Reserva - Venda Não Concluída (Erro: #{booking_create_response.message})")
          flash[:error] = booking_create_response.message
          redirect_to public_root_path
        else
          render :new
        end
      end
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      flash[:error] = t(:message_booking_creation_error)
      redirect_to public_root_path
    end
  end

  def show
    @booking = current_public_user.bookings.find(params[:id])
    if request.get?
      if @booking.blank?
        flash[:error] = t(:sorry_but_this_reservation_wasnt_found_among_your_reservations)
        redirect_to public_bookings_path
      end
    elsif request.post?
      if (@booking.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed])
        if @booking.status != Booking::ACCEPTED_STATUSES[:waiting_for_payment]
          params[:booking].delete(:number_of_people)
        else
          params[:booking][:number_of_people] = params[:booking][:number_of_people].to_i + 1
        end
        @booking.merge params[:booking] # look merge method in Booking
        if @booking.save
          flash[:success] = t(:booking_successfully_edited)
        else
          flash[:error] = t(:booking_error_editing)
        end
      end
      redirect_to public_booking_path(@booking.id)
    else
      redirect_to new_public_booking_path
    end
  end

  def edit
    @booking = current_public_user.bookings.find_by_id(params[:id])
    booking_editable = ((@booking.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_without_card]) ) && (@booking.offer.checkin_timestamp > Time.now.strftime("&Y/%m/%d %H:%M +0000").to_datetime)
    unless booking_editable
      flash[:error] = t(:you_cant_edit_this_booking)
      redirect_to public_bookings_path
    end
  end

  def update
    if params[:commit] == t(:save_response)
      @booking = current_public_user.bookings.find_by_id(params[:id])
      if params[:recommendation_scale].present?
        @booking.recommendation_scale = params[:recommendation_scale]
        if @booking.save!
          flash[:notice] = t(:thank_for_the_feedback)
        else
          flash[:error] = 'Houve um problema ao salvar a resposta.'
        end
      end
      redirect_to public_bookings_path
    else
      update! do | success, failure |
        success.html do
          flash[:notice] = t(:booking_edited_successfully)
          redirect_to public_booking_path(params[:id])
        end
      end
    end
  end

  def total_price_for_number_of_people_dropdpwn(number_of_people)
    mock_booking = Booking.new
    mock_booking.user = current_public_user
    set_booking_cached_prices(mock_booking)
    mock_booking.number_of_people = number_of_people
    mock_booking.hotel = @offer.hotel
    mock_booking.offer_id = @offer.id
    mock_booking.total_price_without_tax
  end

  protected
    def collection
      # TODO - FIXME
      # includes :offer doesn't work because rails doesn't work well with
      # big int columns
      @bookings = apply_scopes(Booking).where(user_id: current_public_user.id).includes(:offer, :hotel, :meeting_room_offer, :omnibees_offer, traveler: {cost_center: {company_detail: :agency_detail}} ).order('created_at DESC').page(params[:page]).per(20)
    end

    def begin_of_association_chain
      current_public_user
    end

  private

  def create_consultant
    @consultant = Consultant.new(agency_detail_id: current_public_user.agency_detail.id , name: params[:consultant_name])
    if @consultant.save
      @booking.consultant_id = @consultant.id
      params[:new_consultant] = false
    else
      flash[:error] = "#{t(:consultant)}: #{@consultant.errors.full_messages.to_sentence}"
    end
  end

  def create_traveler_and_company
    create_company()
    create_traveler()
  end

  def create_company
    @company = CompanyDetail.new(name: params[:company_name], cnpj: params[:company_cnpj], financial_contact: params[:company_financial_contact], phone_financial_contact: params[:company_phone_financial_contact], user_id: current_public_user.id)
    @company.street = params[:traveler_street]
    @company.city = params[:traveler_city]
    @company.state = params[:traveler_state]
    @company.country = params[:traveler_country]
    @company.postal_code = params[:traveler_postal_code]
    if current_public_user.is_a_agency?
      @company.agency_detail_id = current_public_user.agency_detail.id
    end
    if @company.save
      params[:new_traveler_and_company] = false
      params[:new_company] = false
      CompanyUniverse.update_company_universe_equals_cnpj(@company.cnpj, @company.id)
    else
      flash[:error] = "#{t(:company)}: #{@company.errors.full_messages.to_sentence}"
    end
  end

  def create_traveler
    CostCenter.transaction do
      @cost_center = CostCenter.new(name: params[:cost_center_name])
      if @company.present?
        @cost_center.company_detail_id = @company.id
      elsif current_public_user.is_a_company?
        @cost_center.company_detail_id = current_public_user.company_detail.id
      else
        @cost_center.company_detail_id = params[:company_id]
      end
      if @cost_center.save
        @traveler = Traveler.new(cost_center_id: @cost_center.id,
                                name: params[:traveler_name],
                                email: params[:traveler_email],
                                phone_number: params[:traveler_phone],
                                cpf: params[:traveler_cpf],
                                passport: params[:traveler_passport],
                                street: params[:traveler_street],
                                city: params[:traveler_city],
                                state: params[:traveler_state],
                                country: params[:traveler_country],
                                postal_code: params[:traveler_postal_code],
                                company_detail_id: @cost_center.company_detail_id)
        if @traveler.save
          if !current_public_user.accept_confirmed_and_invoiced
            @credit_card = CreditCard.new(card_number: params[:card_number],
                                  cvv: params[:cvv],
                                  expiration_month: params[:expiration_month].to_i,
                                  expiration_year: params[:expiration_year].to_i,
                                  traveler_id: @traveler.id)
            @credit_card.temporary_number = @credit_card.card_number
            if @credit_card.save
              params[:new_traveler] = false
              @booking.traveler_id = @traveler.id
              @booking.guest_name = @booking.traveler.name
              if @booking.phone_number.blank?
                @booking.phone_number = @booking.traveler.phone_number
              end
            else
              flash[:error] = "#{t(:credit_card)}: #{@credit_card.errors.full_messages.to_sentence}"
              raise ActiveRecord::Rollback
            end
          else
            params[:new_traveler] = false
            @booking.traveler_id = @traveler.id
            @booking.guest_name = @booking.traveler.name
            if @booking.phone_number.blank?
              @booking.phone_number = @booking.traveler.phone_number
            end
          end
        else
          flash[:error] = "#{t(:traveler)}: #{@traveler.errors.full_messages.to_sentence}"
          raise ActiveRecord::Rollback
        end
      else
        flash[:error] = "#{t(:cost_center)}: #{@cost_center.errors.full_messages.to_sentence}"
      end
    end
  end

  def set_booking_cached_prices(booking)
    booking.cached_room_type_initial_capacity = @offer.room_type_initial_capacity
    booking.cached_room_type_maximum_capacity = @offer.room_type_maximum_capacity
    booking.cached_offer_price = @offer.price
    booking.cached_offer_extra_price_per_person = @offer.extra_price_per_person
  end

  def get_current_offer_id
    session[:current_offer_id] || nil
  end

  def set_current_offer_id id
    session[:current_offer_id] = id
  end


  def get_current_offer_type
    session[:current_offer_type] || nil
  end

  def set_current_offer_type type
    session[:current_offer_type] = type
  end

  def current_offer_price
    session[:current_offer_price] || nil
  end

  def set_current_offer_price price
    session[:current_offer_price] = price
  end

  def build_resource_params
    [params.fetch(:booking, {}).permit("guest_name", "note", "have_promotional_code", "phone_number")]
  end
end
