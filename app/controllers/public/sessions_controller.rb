# encoding: utf-8
class Public::SessionsController < Devise::SessionsController
  layout 'public'
  helper Shared::CurrencyCurrentHelper
  before_filter :set_currency

  def create
    user = User.find_by_email(params[:public_user].try(:[], :email))
    if user.nil? || user.try(:is_active?)
      super # if the user is not found or the user is active, we call the default devise action, else the user is inactive and we display a custom message
    else
      flash['alert'] = "Sua conta foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_public_user_session_path
    end
  end

  def after_sign_in_path_for(user)
    if user.created_by_affiliate
      user.created_by_affiliate = false
      user.affiliate_id = nil
      user.save!
    end
    #SendUserDataRdsWorker.perform_async(user.email, nil, user.city, "Publico - Usuário logou no sistema")

    if session[:last_path] == public_root_path
      public_bookings_path
    else
      session[:last_path] || public_bookings_path
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    new_public_user_session_path
  end

  def set_currency
    if params[:currency]
      cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
    elsif cookies[:currency].nil?
      cookies[:currency] = "US$"
    end
  end
end
