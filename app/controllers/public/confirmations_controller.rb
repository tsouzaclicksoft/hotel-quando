class Public::ConfirmationsController < Devise::ConfirmationsController
  layout 'public'

  def after_confirmation_path_for(resource_name, resource)
    url_to_redirect = cookies.delete(:last_booking_creation_path)
    sign_in resource
    url_to_redirect || public_root_path
  end
end
