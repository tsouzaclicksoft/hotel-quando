# encoding: utf-8
class Public::TravelRequestsController < Public::BaseController
  include ActionView::Helpers::NumberHelper
  include Public::UserHelper
  include Shared::CurrencyCurrentHelper

  inherit_resources
  before_filter :authenticate_public_user!

  def build_resource_params
    [params.fetch(:travel_request, {}).permit!]
  end

  def update
     pay_with_invoiced
  end

  def pay_with_invoiced
    travel = TravelRequest.find(params[:id])
    travel.payment_status = TravelRequest::ACCEPTED_PAYMENT_STATUS[:billed]
    travel.request_status = TravelRequest::ACCEPTED_REQUEST_STATUS[:pending_agency]

    begin
      TravelRequestMailer.send_email_confirmed_and_fatured(travel).deliver
      flash[:notice] = "Confirmado e faturado, foi enviado um e-mail para agência efetuar a reserva.
        Aguarde você sera notificado por e-mail";
    rescue => ex
      flash[:error] = "Ocorreu um erro ao tentar enviar email";
    end

    travel.save

    redirect_to public_travel_requests_path

  end

  def disapprove
    flash[:notice] = "Orçamento Reprovado";
    redirect_to public_travel_requests_path
  end
  
  def cancel
    travel = TravelRequest.find(params[:id])

    if travel.request_status == TravelRequest::ACCEPTED_REQUEST_STATUS[:canceled]

      flash[:error] = "Solicitação já se encontra cancelada";

    elsif travel.request_status == TravelRequest::ACCEPTED_REQUEST_STATUS[:completed]

      flash[:error] = "Solicitação não pode mais ser cancelada";
    else
      travel.request_status = TravelRequest::ACCEPTED_REQUEST_STATUS[:canceled]
      travel.date_canceled = Time.now

      flash[:notice] = "Solicitação cancelada com sucesso";
    end

    travel.save;

    redirect_to public_travel_requests_path
  end



  protected
  def collection
      @travel_requests = apply_scopes(TravelRequest).where(user_id: current_public_user.id).includes(:airs).order('created_at DESC').page(params[:page]).per(20)
    end

    def begin_of_association_chain
      current_public_user
    end





  end
