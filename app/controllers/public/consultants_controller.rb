# encoding: utf-8
class Public::ConsultantsController < Public::BaseController
  inherit_resources
  actions :index, :show, :new, :create, :edit, :update
  before_filter :authenticate_public_user!
  before_filter :authenticate_agency_detail!

  def create
    params[:consultant][:agency_detail_id] = current_public_user.agency_detail.id
    create!
  end

  def show
    @consultant = current_public_user.agency_detail.consultants.find_by_id(params[:id])
    if @consultant.blank?
      redirect_to public_consultants_path
    end
  end

  def edit
    @consultant = current_public_user.agency_detail.consultants.find_by_id(params[:id])
    if @consultant.blank?
      redirect_to public_consultants_path
    end
  end

  protected
    def collection
      @consultants = current_public_user.agency_detail.consultants
    end

  private
    def build_resource_params
      [params.fetch(:consultant, {}).permit!]
    end

    def authenticate_agency_detail!
      if !current_public_user.agency_detail
        redirect_to public_root_path
      end
    end
end
