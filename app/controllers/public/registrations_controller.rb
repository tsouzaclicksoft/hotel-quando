class Public::RegistrationsController < Devise::RegistrationsController
  layout 'public'

  before_filter :configure_permitted_parameters, :set_currency
  before_action :get_city, only: [:create]
  helper Shared::CurrencyCurrentHelper

  def new
    if session[:last_path] && (session[:last_path].split('/').include?('reservas') || session[:last_path].split('/').include?('bookings'))
      redirect_to new_public_user_session_path
    else
      super
    end
  end

  def create
    build_resource(sign_up_params)
    if !params[:user_type].present?
      resource.skip_validation_for_agency_and_company = false
      if I18n.locale == :en && is_birth_date_format_valid?(params[:public_user][:birth_date]) && params[:public_user].try(:[], :birth_date)
        date_parts = params[:public_user][:birth_date].split('/')
        resource.birth_date = date_parts[1] + '/' + date_parts[0] + '/' + date_parts[2]
      end
    else
      resource.skip_validation_for_agency_and_company = true
    end

    if params[:public_user][:city].present?
      resource.city = get_city
    end

    if params[:user_type].present? 
      if params[:user_type].to_i == User::ACCEPTED_TYPES[:agency] || params[:user_type].to_i == User::ACCEPTED_TYPES[:company]
        resource.errors.add(:street, I18n.t('errors.messages.blank')) if params[:agency_company_detail_street].blank?
        resource.errors.add(:state, I18n.t('errors.messages.blank')) if params[:agency_company_detail_country] != 'CO' && params[:agency_company_detail_state].blank?
        resource.errors.add(:country, I18n.t('errors.messages.blank')) if params[:agency_company_detail_country].blank?
        resource.errors.add(:postal_code, I18n.t('errors.messages.blank')) if params[:agency_company_detail_country] != 'CO' && params[:agency_company_detail_postal_code].blank? 
        resource.errors.add(:number, I18n.t('errors.messages.blank')) if params[:agency_company_detail_number].blank?
        resource.errors.add(:city, I18n.t('errors.messages.blank')) if params[:public_user][:city].blank?
      end
    end

    if resource.errors.present?
      flash[:alert] = "#{t(:there_were_the_following_errors_with_the_form)}: "
      flash[:alert] += resource.errors.full_messages.uniq.map(&:downcase).join(', ')
      flash[:alert] += '. ' if flash[:alert]
      flash[:alert] += "#{t(:please_fix_them_and_try_again)}."
      redirect_to :back
    elsif resource.save
      if params[:user_type].present?
        if params[:user_type].to_i == User::ACCEPTED_TYPES[:agency]
          agency_detail = AgencyDetail.new()
          agency_detail.city = resource.city
          agency_detail.user_id = resource.id
          agency_detail.name = params[:agency_company_detail_name]
          agency_detail.street = params[:agency_company_detail_street]
          agency_detail.state = params[:agency_company_detail_state]
          agency_detail.country = params[:agency_company_detail_country]
          agency_detail.postal_code = params[:agency_company_detail_postal_code]
          agency_detail.number = params[:agency_company_detail_number]
          agency_detail.complement = params[:agency_company_detail_complement]
          agency_detail.save!
        else
          company_detail = CompanyDetail.new()
          company_detail.user_id = resource.id
          company_detail.city = resource.city
          company_detail.name = params[:agency_company_detail_name]
          company_detail.street = params[:agency_company_detail_street]
          company_detail.state = params[:agency_company_detail_state]
          company_detail.country = params[:agency_company_detail_country]
          company_detail.postal_code = params[:agency_company_detail_postal_code]
          company_detail.number = params[:agency_company_detail_number]
          company_detail.complement = params[:agency_company_detail_complement]
          company_detail.save!
        end
      end
      yield resource if block_given?
      if resource.active_for_authentication?
        # setting locale to config flash message and after sign up page
        params[:locale] = params[:public_user][:locale]
        I18n.locale = I18n.available_locales.include?(params[:locale].to_sym) ? params[:locale] : I18n.default_locale
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)

        #SendUserDataRdsWorker.perform_async(params[:public_user][:email], nil, params[:public_user][:city], "Publico - Usuário registrou no sistema", params[:public_user][:name], params[:public_user][:country], params[:public_user][:locale])
        #EmktMailer.register_without_booking(resource).deliver

        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      if session[:last_path] && (session[:last_path].split('/').include?('reservas') || session[:last_path].split('/').include?('bookings'))
        flash[:alert] = "#{t(:there_were_the_following_errors_with_the_form)}: "
        flash[:alert] += resource.errors.full_messages.uniq.map(&:downcase).join(', ')
        flash[:alert] += '. ' if flash[:alert]
        flash[:alert] += "#{t(:please_fix_them_and_try_again)}."
        redirect_to new_public_user_session_path(user_input: {name: resource.name.to_s, phone_number: resource.phone_number.to_s, email: resource.email.to_s}, tab: 'signup')
      else
        if params[:user_type].present?
          flash[:alert] = "#{t(:there_were_the_following_errors_with_the_form)}: "
          flash[:alert] += resource.errors.full_messages.uniq.map(&:downcase).join(', ')
          flash[:alert] += '. ' if flash[:alert]
          flash[:alert] += "#{t(:please_fix_them_and_try_again)}."
          redirect_to :back
        else
          respond_with resource
        end
      end
    end
  end

  def after_sign_up_path_for(user)
    if session[:last_path] == public_root_path
      public_bookings_path
    else
      session[:last_path] || public_bookings_path
    end
  end

  def after_inactive_sign_up_path_for(user)
    if session[:last_path] == public_root_path
      public_bookings_path
    else
      session[:last_path] || public_bookings_path
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
    devise_parameter_sanitizer.for(:sign_up) << :locale
    devise_parameter_sanitizer.for(:sign_up) << :phone_number
    devise_parameter_sanitizer.for(:sign_up) << :birth_date
    devise_parameter_sanitizer.for(:sign_up) << :street
    devise_parameter_sanitizer.for(:sign_up) << :number
    devise_parameter_sanitizer.for(:sign_up) << :city
    devise_parameter_sanitizer.for(:sign_up) << :state
    devise_parameter_sanitizer.for(:sign_up) << :country
    devise_parameter_sanitizer.for(:sign_up) << :postal_code
  end

  private

    def is_birth_date_format_valid?(birth_date)
      return true if birth_date =~ /\d{2}\/\d{2}\/\d{4}/
      false
    end

    def set_currency
      if params[:currency]
        cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
      elsif cookies[:currency].nil?
        cookies[:currency] = "US$"
      end
    end

    def get_city
      city = params[:public_user][:city].include?(' - ') ? params[:public_user][:city].split(' - ')[0] : params[:public_user][:city].split(',')[0]
    end
end
