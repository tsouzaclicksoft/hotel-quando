# encoding: utf-8
class Public::CompanyDetailsController < Public::BaseController
  inherit_resources
  actions :index, :show, :new, :create, :edit, :update
  before_filter :authenticate_public_user!
  before_filter :authenticate_agency_detail!

  def create
    params[:company_detail][:agency_detail_id] = current_public_user.agency_detail.id
    create!
  end

  def show
    @company_detail = current_public_user.agency_detail.company_details.find_by_id(params[:id])
    if @company_detail.blank?
      redirect_to public_company_details_path
    else
      @cost_centers = @company_detail.cost_centers
      @job_titles = @company_detail.job_titles
    end
  end

  def edit
    @company_detail = current_public_user.agency_detail.company_details.find_by_id(params[:id])
    if @company_detail.blank?
      redirect_to public_company_details_path
    end
  end

  protected
    def collection
      @company_details = current_public_user.agency_detail.company_details
    end

  private
    def build_resource_params
      [params.fetch(:company_detail, {}).permit!]
    end

    def authenticate_agency_detail!
      if !current_public_user.is_a_agency?
        redirect_to public_root_path
      end
    end
end
