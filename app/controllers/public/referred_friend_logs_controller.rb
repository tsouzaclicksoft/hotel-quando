# encoding: utf-8
class Public::ReferredFriendLogsController < Public::BaseController
  include ActionView::Helpers::NumberHelper

  def send_code
    begin
      @promotional_code = PromotionalCode.by_code(current_public_user.refer_code).last
      if @promotional_code.present?
        create_referred_friend_log
      else
        if @promotional_code = PromotionalCode.create_member_get_member_code(current_public_user)
          create_referred_friend_log
        else
          flash[:error] = "Houve um problema, tente novamente"
          redirect_to public_friend_hq_path
        end
      end
    rescue => ex
      Rails.logger.error(ex)
      flash[:error] = ex
      redirect_to public_friend_hq_path
    end
  end

  private

  def create_referred_friend_log
    count = params[:count].to_i
    if count > 1
      friends_list = []
      send_all_codes = false
      count.times do |i|
        if i == 0 
          @referred_friend_log = ReferredFriendLog.new(user_id: current_public_user.id, promotional_code_id: @promotional_code.id, friend_email: params[:referred_friend_log]["friend_email"], friend_name: params[:referred_friend_log]["friend_name"])
        else
          @referred_friend_log = ReferredFriendLog.new(user_id: current_public_user.id, promotional_code_id: @promotional_code.id, friend_email: params[:referred_friend_log]["friend_email#{i}"], friend_name: params[:referred_friend_log]["friend_name#{i}"])
        end
        if @referred_friend_log.save
          send_all_codes = true
          ReferredFriendMailer.send_refer_code_to_friend(@referred_friend_log).deliver
          flash[:success] = t(:refer_code_sended)
        end
      end
      if send_all_codes
        respond_to do |format|
          format.js { render :text => flash[:success], content_type: 'text/plain' }
          format.html { redirect_to public_friend_hq_path}
        end  
      else
        respond_to do |format|
          format.js { render :text => flash[:error], content_type: 'text/plain' }
          format.html { redirect_to public_friend_hq_path}
        end        
      end
    else
      @referred_friend_log = ReferredFriendLog.new(
        user_id: current_public_user.id,
        promotional_code_id: @promotional_code.id,
        friend_email: params[:referred_friend_log][:friend_email],
        friend_name: params[:referred_friend_log][:friend_name]
      )
      if @referred_friend_log.save
        ReferredFriendMailer.send_refer_code_to_friend(@referred_friend_log).deliver
        flash[:success] = t(:refer_code_sended)
        respond_to do |format|
          format.js { render :text => flash[:success], content_type: 'text/plain' }
          format.html { redirect_to public_friend_hq_path}
        end
      else
        flash[:error] = "#{t(:there_were_the_following_errors_with_the_form)}: "
        flash[:error] += @referred_friend_log.errors.full_messages.uniq.map(&:downcase).join(', ')
        flash[:error] += '. ' if flash[:error]
        flash[:error] += "#{t(:please_fix_them_and_try_again)}."
        respond_to do |format|
          format.js { render :text => flash[:error], content_type: 'text/plain' }
          format.html { redirect_to public_friend_hq_path}
        end
      end
    end
  end

end
