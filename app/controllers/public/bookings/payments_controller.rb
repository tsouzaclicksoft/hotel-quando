# encoding: utf-8
class Public::Bookings::PaymentsController < Public::BaseController
  include ActionView::Helpers::NumberHelper
  include Public::UserHelper
	include PayPal::SDK::Core::Logging
  include Shared::CurrencyCurrentHelper
  require 'ostruct'

  SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT = (ENV['SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT'] == 'true')
  class ExecutePaymentPaypalFailed < StandardError; end
  class PaymentFailedError < StandardError; end
  class OmnibeesReservationError < StandardError; end
  class ParticipantCpfNotFilledError < StandardError; end
  class CpfNotValidError < StandardError; end
  class ExecutePaymentEpaycoFailed < StandardError; end
  class ExecutePaymentPayuFailed < StandardError; end
  class HotelNotAcceptInvoicedPayment < StandardError; end

  force_ssl unless (Rails.env == "development" || Rails.env == "test")

  before_filter :authenticate_public_user!
  before_filter :set_resources_and_check_permissions!, only: [:new, :create, :create_express_checkout_payment]
  helper_method :extra_person_price_text

  def new
    @booking.currency_code = current_currency.code
    @payment = Payment.new
    if user_country != 'CO'
      @is_express_checkout = false
      if current_currency.code != 'BRL' && @booking.traveler.try(:need_to_complete_address?)
        flash[:error] = "Para realizar transação em USD é necessário preencher todos os campos de endereço corretamente"
        redirect_to edit_public_traveler_path(@booking.traveler)
      else
        if Rails.env == "production"
          create_payment_paypal()
        end
      end
    end
  end

  def create
    begin
      response_object = OpenStruct.new
      response_object.success = false
      if @booking.promotional_code_log
        promotional_100_percent = @booking.promotional_code_log.promotional_code.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:percentage] && @booking.promotional_code_log.promotional_code.discount.eql?(100)
      else
        promotional_100_percent = false
      end
      if params[:tudo_azul_participant] == 'true'
        raise ParticipantCpfNotFilledError if params[:participant_cpf].blank?
        raise CpfNotValidError unless CpfValidator::Cpf.valid?(params[:participant_cpf])
        Booking.find(params[:booking_id]).tudo_azul_associate!(params[:participant_cpf])
      end

      if
        if promotional_100_percent
          if @booking.hotel.booking_on_request
            @booking.status = Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
            @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
          else
            @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
            @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
          end
          @booking.save!
        else
          if current_public_user.accept_confirmed_and_invoiced && @booking.traveler_id.present?
            if @booking.hotel.accept_invoiced_payment
              if @booking.hotel.booking_on_request
                @booking.status = Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
                # @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
              else
                @booking.status = @booking.offer.hotel.accept_invoiced_by_agency ? Booking::ACCEPTED_STATUSES[:invoiced_by_agency] : Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
                # @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
              end
              @booking.save!
            else
              raise HotelNotAcceptInvoicedPayment
            end
          else
            if user_country == 'CO'
              execute_payment_payu()
            else
              if Rails.env == "development" || Rails.env == "test" || Rails.env == "staging"
                create_fake_payment()
              elsif execute_payment_paypal()
                response_object.success = true
              end
            end
          end
        end
      end
      if params[:multiplus_participant] == 'true'
        @booking.multiplus_code = params[:multiplus_code]
        @booking.save!
      end
      @booking.reload
      if @booking.promotional_code_log && User.where(refer_code: (@booking.promotional_code_log.promotional_code.code)).exists?
        @booking.user.add_credit_to_balance_user(@booking)
      end
      if @booking.can_use_credit_and_have_founds_balance?
        @booking.balance_value_used = @booking.discount_value_balance
        @booking.user.deduct_credit_when_use_in_booking(@booking)
        @booking.save!
        @booking.user.reload
      end
      reserve_on_omnibees() if @booking.is_omnibees_or_hoteisnet?
      if @booking.hotel.booking_on_request
        HotelMailer.send_booking_request_confirmation(@booking).deliver
        UserMailer.send_booking_request_confirmation(@booking).deliver
      else
        HotelMailer.send_booking_confirmation_notice(@booking).deliver
        UserMailer.send_booking_confirmation_notice(@booking).deliver
      end
      if @booking.created_by_agency?
        AgencyMailer.send_booking_confirmation_notice(@booking).deliver
        #SendUserDataRdsWorker.perform_async(current_public_user.id, @offer.hotel.name, @offer.hotel.city.name, "Publico Agencia - Reserva - Venda Realizada")
      else
        #SendUserDataRdsWorker.perform_async(current_public_user.id, @offer.hotel.name, @offer.hotel.city.name, "Publico - Reserva - Venda Realizada")
      end
      #SendSmsWorker.perform_async(@booking.id, :sms_booking_confirmation)
      respond_to do |format|
        format.json { render :json => response_object }
        format.html { render :action => 'create'}
      end
    rescue HotelNotAcceptInvoicedPayment
      flash[:error] = t(:this_hotel_not_accept_invoiced_payment)
      respond_to do |format|
        format.json { render :json => response_object }
        format.html { redirect_to :back}
      end
    rescue OmnibeesReservationError => omnibees_error
      Airbrake.notify(omnibees_error)
      AdminMailer.error_reserving_on_omnibees(@booking, nil).deliver
      #SendUserDataRdsWorker.perform_async(current_public_user.id, @booking.offer.hotel.name, @booking.offer.hotel.city.name, "Publico - Reserva - Venda Realizada")
    rescue ParticipantCpfNotFilledError
      flash[:error] = t(:error_message_participant_cpf_not_filled_error)
      respond_to do |format|
        format.json { render :json => response_object }
        format.html { redirect_to :back}
      end
    rescue CpfNotValidError
      flash[:error] = t(:error_message_participant_cpf_not_valid_error)
      respond_to do |format|
        format.json { render :json => response_object }
        format.html { redirect_to :back}
      end
    rescue ExecutePaymentPaypalFailed
      message = @paypal_payment.error.present? ? @paypal_payment.error.message : t(:paypal_execute_payment_error)
      flash[:error] = message
      respond_to do |format|
        format.json { render :json => response_object }
        format.html { redirect_to :back}
      end
    rescue ExecutePaymentPayuFailed
      get_message_error_payment_payu
      redirect_to :back
    rescue ExecutePaymentEpaycoFailed
      flash[:error] =  t(:paypal_execute_payment_error)
      redirect_to :back
    rescue Exception => exception
      if @payment
        if @booking.created_by_agency?
          AgencyMailer.send_payment_failure_notice(@payment).deliver
          #SendUserDataRdsWorker.perform_async(current_public_user.id, @booking.offer.hotel.name, @booking.offer.hotel.city.name, "Agencia - Reserva - Venda Não Concluída (Erro: Pagamento não efetuado)")
        else
          UserMailer.send_payment_failure_notice(@payment).deliver
          #SendUserDataRdsWorker.perform_async(current_public_user.id, @booking.offer.hotel.name, @booking.offer.hotel.city.name, "Publico - Reserva - Venda Não Concluída (Erro: Pagamento não efetuado)")
        end
      end
      logger.error(exception)
      Airbrake.notify(exception, parameters: {user_id: current_public_user.id})
      flash[:error] = exception.to_s
      respond_to do |format|
        format.json { render :json => response_object }
        format.html { redirect_to :back}
      end
    end
  end

  def create_express_checkout_payment
    @payment = Payment.new
    @is_express_checkout = true
    create_payment_paypal()
    respond_to do |format|
      format.json { render :json => @payment_paypal.to_json }
      format.html { render :nothing => true }
    end
  end

  def get_message_error_payment_payu
    message = t(:paypal_execute_payment_error)
      if @payu_payment["error"] != nil
        message = t(:paypal_execute_payment_error) + '. ' + t(:payu_reason) + @payu_payment["error"]
      elsif @payu_payment["transactionResponse"] != nil
        if @payu_payment["transactionResponse"]["responseMessage"] != nil
          message = t(:paypal_execute_payment_error) + '. ' +  t(:payu_reason_card) + @payu_payment["transactionResponse"]["state"] + ', ' + @payu_payment["transactionResponse"]["responseMessage"]
        else
          message = t(:paypal_execute_payment_error) + '. ' +  t(:payu_reason_card) + @payu_payment["transactionResponse"]["state"];
        end

      end
      flash[:error] = message
  end

  def payment_success
    @booking = Booking.find(params[:booking_id])
    @offer = @booking.offer
    @payment = @booking.payments.last
  end

  private

  def user_country
    current_public_user.country || current_public_user.try(:agency_detail).country || current_public_user.try(:company_detail).country
  end

    def reserve_on_omnibees
      begin
        # 1 = Hoteisnet || 2 = Omnibees
        choice = ChooseOmnibeesOrHoteisnet.choose(@booking.pack_in_hours, @booking.offer.checkin_timestamp)
        if choice == 1
          client = Hotelnet::Client.new
        else
          client = Omnibees::Client.new
        end
        reservation_xml = client.make_booking({ booking: @booking })
        @booking.omnibees_request_xml = client.last_request_xml
        @booking.omnibees_response_xml = reservation_xml
        @booking.save!

        if choice == 1
          reservation_status = Hotelnet::DTO::HotelReservationToBookingResult.parse(reservation_xml)
        else
          reservation_status = Omnibees::DTO::HotelReservationToBookingResult.parse(reservation_xml)
        end

        if reservation_status.confirmed?
          @booking.omnibees_code = reservation_status.reservation_id
        end
        @booking.save!
      rescue Exception => ex
        Airbrake.notify(ex)
        raise OmnibeesReservationError
      end
      raise OmnibeesReservationError unless reservation_status.confirmed?
    end

    def create_fake_payment
      @payment = Payment.new
      @payment.user_id = current_public_user.id
      @payment.traveler_id = @booking.traveler_id
      @payment.booking_id = @booking.id
      if @booking.will_be_paid_online?
        if @booking.hotel.booking_on_request
          @booking.status = Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
        else
          @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
        end
        @payment.status = Payment::ACCEPTED_STATUSES[:captured]
      else
        if @booking.hotel.booking_on_request
          @booking.status = Booking::ACCEPTED_STATUSES[:requested_reservation]
        else
          @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
        end
        @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
      end
      @booking.save!
      @payment.save!
    end

    def create_payment_paypal
      @currency = current_currency.code
      @guest = @booking.traveler.present? ? @booking.traveler : @booking.user

      city = @guest.city.present? ? @guest.city.split(',').first : ''
      #inflector to remove accents in the city
      city_no_accent = ActiveSupport::Inflector.transliterate(city)

      # to test in dev use your localhost in this variable
      if Rails.env.development?
        return_url = 'http://dev:3000'
      elsif Rails.env.staging?
        return_url = 'http://staging.hotelquando.com.br'
      elsif Rails.env.production?
        return_url = 'https://www.hotelquando.com.br'
      end

      price_to_pay_now = @booking.will_be_paid_online? ? @booking.total_price : @booking.booking_tax

      if (@currency == 'BRL' && @guest.country == "BR") || @is_express_checkout
        @payment_paypal = PayPal::SDK::REST::DataTypes::Payment.new({
          :intent => "sale",
          :payer => {
            :payment_method => "paypal"},
          :transactions => [{
            :amount => {
              :total => calculate_to_currency_booking(@booking.hotel.currency_symbol, price_to_pay_now, @currency, @booking.created_at).round(2),
              :currency => @currency },
            :description => "This is the payment transaction description."
          }],
          :redirect_urls => {
            :return_url => return_url + new_public_booking_payments_path(@booking.id),
            :cancel_url => return_url
          },
          :experience_profile_id => "XP-HCBJ-UC7P-6V6H-3PAC"
        })
      else
        @payment_paypal = PayPal::SDK::REST::DataTypes::Payment.new({
          :intent => "sale",
          :payer => {
            :payment_method => "paypal"},
          :transactions => [{
            :amount => {
              :total => calculate_currency(price_to_pay_now, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2),
              :currency => @currency },
            :description => "This is the payment transaction description.",
            :item_list => {
              :shipping_address => {
                  :recipient_name => @booking.guest_name,
                  :line1 => @guest.street,
                  :city => city_no_accent,
                  :country_code => @guest.country,
                  :postal_code => @guest.postal_code,
                  :state => @guest.state,
                  :phone => @booking.phone_number
              },
              :items =>
                 {
                    :name => "Booking id #{@booking.id}",
                    :description => "Hotel #{@booking.hotel.name}",
                    :quantity => "1",
                    :price => calculate_currency(price_to_pay_now, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol]).round(2),
                    :tax => "0",
                    :sku => "Item1",
                    :currency => @currency
                 }
            }
          }],
          :redirect_urls => {
            :return_url => return_url + new_public_booking_payments_path(@booking.id),
            :cancel_url => return_url
          },
          :experience_profile_id => "XP-HCBJ-UC7P-6V6H-3PAC"
        })
      end
      if @payment_paypal.create
        @booking.update(currency_code: @currency)
        # Redirect the user to given approval url
        @redirect_url = @payment_paypal.links.find{|v| v.rel == "approval_url" }.href
        @payment.paypal_payment_id = @payment_paypal.id
        logger.info "Payment[#{@payment_paypal.id}]"
        logger.info "Redirect: #{@redirect_url}"
      else
        if @guest.country != "BR" || (current_currency != "BRL" && @guest.country.present? )
          if @payment_paypal.error.name == "VALIDATION_ERROR"
            flash[:error] = t('paypal.validation_error')
          else
            flash[:error] = @payment_paypal.error.message
          end
        end
        logger.error @payment_paypal.error.inspect
      end
    end

    def execute_payment_payu

      test_mode = (Rails.env == "development" || Rails.env == "test" || Rails.env == "staging") ? true : false

      create_payment
      price_to_pay_now = @booking.will_be_paid_online? ? @booking.total_price : @booking.booking_tax
      currency = current_currency.code == 'COP' ? 'COP' : 'USD'

      CreditCardValidator::Validator.options[:allowed_card_types] = [:visa, :master_card, :diners_club, :amex]
      card_type = CreditCardValidator::Validator.card_type(params[:card_number]).gsub('_', '');

      begin
        @payu_payment = PayuInterface.payment({
                        number: params[:card_number],
                        securityCode: params[:cvc],
                        expirationDate: params[:year] << '/' << params[:month],
                        referenceCode: "booking_id_#{@booking.id}",
                        description: "Pago de reservas de hotel HotelQuando.Com",
                        value: current_convert_to_epayco(currency, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol], price_to_pay_now).round(2).to_s,
                        currency: currency,
                        session_id: session.id,
                        paymentMethod: card_type.upcase,
                        emailAddress: params[:email],
                        user: current_public_user,
                        fullName: params[:name],
                        dniType: params[:type_doc].upcase,
                        contactPhone: params[:phone].gsub(/\D/, ''),
                        dniNumber: params[:number_doc],
                        test_mode: test_mode,
                        cookie: request.cookies["_hotel_quando_session"].to_s,
                        userAgent: request.user_agent
                      });

        if @payu_payment["transactionResponse"] != nil && @payu_payment["code"] == 'SUCCESS'
          if @payu_payment["transactionResponse"]["state"] == "APPROVED" || @payu_payment["transactionResponse"]["state"] == "PENDING"
            if @payu_payment["transactionResponse"]["state"] == "PENDING"
              flash[:success] = t(:payu_payment_pending)
            end
            logger.info "Payment[#{@payu_payment["transactionResponse"]["orderId"]}] execute successfully"
            if @booking.will_be_paid_online?
              if @booking.hotel.booking_on_request
                @booking.status = Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
                @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
              else
                @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
                @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
              end
              @payment.status = Payment::ACCEPTED_STATUSES[:captured]
            else
              if @booking.hotel.booking_on_request
                @booking.status = Booking::ACCEPTED_STATUSES[:requested_reservation]
                @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
              else
                @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
                @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
              end
              @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
            end
            @payment.maxipago_response = '';
            @payment.payu_order_id = @payu_payment["transactionResponse"]["orderId"].to_s
            @payment.payu_transaction_id = @payu_payment["transactionResponse"]["transactionId"].to_s
            @booking.save!
            @payment.save!
          else
            if @payu_payment["transactionResponse"] != nil
              @payment.maxipago_response += @payu_payment["transactionResponse"]["state"] unless @payu_payment["transactionResponse"]["state"].nil?
              @payment.maxipago_response += @payu_payment["transactionResponse"]["responseMessage"] unless @payu_payment["transactionResponse"]["responseMessage"].nil?
            end
            @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
            @payment.save!

            raise ExecutePaymentPayuFailed
          end
        end
      rescue Exception => exception
        Airbrake.notify(exception)
        logger.error @payu_payment["error"] unless @payu_payment["error"].nil?
        @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
        @payment.maxipago_response += @payu_payment["error"] unless @payu_payment["error"].nil?
        if @payu_payment["transactionResponse"] != nil
           @payment.maxipago_response += @payu_payment["transactionResponse"]["responseMessage"] unless @payu_payment["transactionResponse"]["responseMessage"].nil?
        end
        @payment.save!
        raise ExecutePaymentPayuFailed
      end

    end

    def execute_payment_epayco
       test_mode = (Rails.env == "development" || Rails.env == "test" || Rails.env == "staging") ? true : false
      # settings = { public_key: ENV['EPAYCO_PUBLIC_KEY'], private_key: ENV['EPAYCO_PRIVATE_KEY'], test_mode: test_mode}
      # epayco_client = EPayCo::Client.new(settings)

      Epayco.apiKey = ENV['EPAYCO_PUBLIC_KEY']
      Epayco.privateKey = ENV['EPAYCO_PRIVATE_KEY']
      Epayco.lang = 'ES'
      Epayco.test = test_mode

      create_payment
      price_to_pay_now = @booking.will_be_paid_online? ? @booking.total_price : @booking.booking_tax
      currency = current_currency.code == 'COP' ? 'COP' : 'USD'
      card_token = params[:token]
      begin
        customer_params = {
          token_card: card_token,
          name: params[:name],
          email: params[:email],
          phone: params[:phone],
          default: false
        }
        customer = Epayco::Customers.create customer_params

        charge_params = {
          token_card: card_token,
          customer_id: customer[:data][:customerId],
          doc_type: params[:type_doc],
          doc_number: params[:number_doc],
          name: params[:name].split(" ")[0],
          last_name: params[:name].split(" ")[1],
          email: params[:email],
          # ip: request.remote_ip,
          bill: "Booking - #{@booking.id}",
          description: "Pago de reservas de hotel HotelQuando.Com",
          value: current_convert_to_epayco(currency, Hotel::CURRENCY_SYMBOL_REVERSED[@booking.hotel.currency_symbol], price_to_pay_now).round(2).to_s,
          tax: "0",
          tax_base: "0",
          currency: currency,
          dues: "1"
        }
        epayco_payment = Epayco::Charge.create charge_params
        logger.error epayco_payment[:data][:estado]

        if epayco_payment[:success] && epayco_payment[:data][:estado] == "Aceptada"
          logger.info "Payment[#{epayco_payment[:data][:ref_payco]}] execute successfully"
          if @booking.will_be_paid_online?
            if @booking.hotel.booking_on_request
              @booking.status = Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
              @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
            else
              @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
              @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
            end
            @payment.status = Payment::ACCEPTED_STATUSES[:captured]
          else
            if @booking.hotel.booking_on_request
              @booking.status = Booking::ACCEPTED_STATUSES[:requested_reservation]
              @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
            else
              @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
              @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
            end
            @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
          end
          @payment.epayco_ref = epayco_payment[:data][:ref_payco].to_s
          @booking.save!
          @payment.save!
        else
          @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
          @payment.save!
          raise ExecutePaymentEpaycoFailed
        end
      rescue Exception => exception
        Airbrake.notify(exception)
        logger.error epayco_payment[:data][:respuesta] unless epayco_payment[:data][:respuesta].nil?
        logger.error epayco_payment[:data][:description] unless epayco_payment[:data][:description].nil?
        @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
        @payment.maxipago_response += epayco_payment[:data][:description] unless epayco_payment[:data][:description].nil?
        @payment.maxipago_response += epayco_payment[:data][:respuesta] unless epayco_payment[:data][:respuesta].nil?
        @payment.save!
      end
    end

    def execute_payment_paypal
      create_payment

      begin
        if params[:paymentID] && params[:payerID]
          @paypal_payment = PayPal::SDK::REST::DataTypes::Payment.find(params[:paymentID])
          @payer_id = params[:payerID]
        else
          @paypal_payment = PayPal::SDK::REST::DataTypes::Payment.find(params[:payment][:paypal_payment_id])
          @payer_id = @paypal_payment.payer.payer_info.payer_id
        end
        if @paypal_payment.execute( :payer_id => @payer_id )
          logger.info "Payment[#{@paypal_payment.id}] execute successfully"
          if @booking.will_be_paid_online?
            if @booking.hotel.booking_on_request
              @booking.status = Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
              @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
            else
              @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
              @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
            end
            @payment.status = Payment::ACCEPTED_STATUSES[:captured]
          else
            if @booking.hotel.booking_on_request
              @booking.status = Booking::ACCEPTED_STATUSES[:requested_reservation]
              @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
            else
              @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
              @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
            end
            @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
          end
          @payment.paypal_transaction_id = @paypal_payment.transactions[0].related_resources[0].sale.id
          @booking.save!
          @payment.save!
        else
          @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
          @payment.save!
          raise ExecutePaymentPaypalFailed
        end
      rescue Exception => exception
        Airbrake.notify(exception)
        logger.error @paypal_payment.error.inspect
        @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
        @payment.maxipago_response += @paypal_payment.error.inspect.to_s
        @payment.save!
        raise ExecutePaymentPaypalFailed
      end
    end

    def create_payment
      @payment = Payment.new
      @payment.user_id = current_public_user.id
      @payment.traveler_id = @booking.traveler_id
      @payment.booking_id = @booking.id
      @payment.status = Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response]
      if params[:paymentID].present? || params[:payment].present?
        @payment.paypal_payment_id = params[:paymentID].present? ? params[:paymentID] : params[:payment][:paypal_payment_id]
      end
      @payment.is_app_mobile = false
      @payment.save!
    end

    def set_resources_and_check_permissions!
      begin
        @booking = current_public_user.bookings.find(params[:booking_id])
        @offer = @booking.offer
        if (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
          flash[:error] = t("bookings.booking_already_confirmed")
          redirect_to public_booking_path(params[:booking_id])
        end
        if @booking.status == Booking::ACCEPTED_STATUSES[:canceled]
          flash[:error] = t("bookings.booking_canceled")
          redirect_to public_booking_path(params[:booking_id])
        end
      rescue
        flash[:error] = t(:you_cant_confirm_a_booking_that_not_yours)
        redirect_to public_bookings_path
      end
    end

    def extra_person_price_text booking
      if booking.number_of_people <= booking.offer.room_type_initial_capacity
        return "#{number_to_currency 0}"
      elsif booking.number_of_people <= booking.offer.room_type_maximum_capacity
        return "#{booking.number_of_people - booking.offer.room_type_initial_capacity} x #{number_to_currency booking.extra_price_per_person_without_iss}"
      end
    end
end
