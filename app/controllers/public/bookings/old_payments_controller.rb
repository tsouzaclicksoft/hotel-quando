# encoding: utf-8
class Public::Bookings::PaymentsController < Public::BaseController
  include ActionView::Helpers::NumberHelper
  include Public::UserHelper


  SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT = (ENV['SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT'] == 'true')
  class CannotCreateCreditCardError < StandardError; end
  class PaymentFailedError < StandardError; end
  class ClientTokenError < StandardError; end
  class CreditCardTokenError < StandardError; end
  class OmnibeesReservationError < StandardError; end
  class ParticipantCpfNotFilledError < StandardError; end
  class CpfNotValidError < StandardError; end

  force_ssl unless (Rails.env == "development" || Rails.env == "test")

  before_filter :authenticate_public_user!
  before_filter :set_resources_and_check_permissions!, only: [:new, :create]
  helper_method :extra_person_price_text

  def new
    @credit_card = CreditCard.new
  end

  def create
    # Search for credit card - OK
    # verify authorization
    # create another payment with success or not
    # redirect to final step if success or render new if not
    # success if value is authorized
    begin
      if @booking.promotional_code_log
        promotional_100_percent = @booking.promotional_code_log.promotional_code.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:percentage] && @booking.promotional_code_log.promotional_code.discount.eql?(100)
      else
        promotional_100_percent = false
      end

      if params[:tudo_azul_participant] == 'true'
        raise ParticipantCpfNotFilledError if params[:participant_cpf].blank?
        raise CpfNotValidError unless CpfValidator::Cpf.valid?(params[:participant_cpf])
        Booking.find(params[:booking_id]).tudo_azul_associate!(params[:participant_cpf])
      end

      if SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT
        set_and_verify_credit_card()
        confirm_booking_and_create_fake_payment_for_view()
        send_credit_card_info_to_admin()
      else
        if promotional_100_percent
          @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
          @booking.save!
        else
          create_client_token() if current_public_user.maxipago_token.blank? && @booking.traveler_id.nil?
          set_a_valid_credit_card_to_use() if !current_public_user.accept_confirmed_and_invoiced
          if current_public_user.accept_confirmed_and_invoiced && @booking.traveler_id.present?
            @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
            #@booking.invoice_id = params[:invoice_id]
            @booking.save!
          elsif @booking.hotel.accept_online_payment || @booking.is_omnibees_or_hoteisnet? || @booking.have_promotional_code
            if params[:pay_at_the_hotel] == 'true'
              use_credit_card_to_confirm_booking()
            else
              use_credit_card_to_pay_booking()
            end
          else
            use_credit_card_to_confirm_booking()
          end
        end
      end
      if params[:multiplus_participant] == 'true'
        @booking.multiplus_code = params[:multiplus_code]
        @booking.save!
      end
      @booking.reload
      reserve_on_omnibees() if @booking.is_omnibees_or_hoteisnet?
      HotelMailer.send_booking_confirmation_notice(@booking).deliver
      UserMailer.send_booking_confirmation_notice(@booking).deliver
      if @booking.created_by_agency?
        AgencyMailer.send_booking_confirmation_notice(@booking).deliver 
        #SendUserDataRdsWorker.perform_async(current_public_user.id, @offer.hotel.name, @offer.hotel.city.name, "Publico Agencia - Reserva - Venda Realizada")
      else
        #SendUserDataRdsWorker.perform_async(current_public_user.id, @offer.hotel.name, @offer.hotel.city.name, "Publico - Reserva - Venda Realizada")
      end
      #SendSmsWorker.perform_async(@booking.id, :sms_booking_confirmation)
    rescue OmnibeesReservationError => omnibees_error
      Airbrake.notify(omnibees_error)
      AdminMailer.error_reserving_on_omnibees(@booking, @credit_card).deliver
      #SendUserDataRdsWorker.perform_async(current_public_user.id, @booking.offer.hotel.name, @booking.offer.hotel.city.name, "Publico - Reserva - Venda Realizada")
    rescue ParticipantCpfNotFilledError
      @credit_card ||= CreditCard.new(params[:credit_card].permit!)
      flash.now[:error] = t(:error_message_participant_cpf_not_filled_error)
      render :new
    rescue CpfNotValidError
      @credit_card ||= CreditCard.new(params[:credit_card].permit!)
      flash.now[:error] = t(:error_message_participant_cpf_not_valid_error)
      render :new
    rescue CannotCreateCreditCardError
      @credit_card ||= CreditCard.new(params[:credit_card].permit!)
      flash.now[:error] = t(:error_message_cannot_create_credit_card_error)
      render :new
    rescue Exception => exception
      if @payment
        if @booking.created_by_agency?
          AgencyMailer.send_payment_failure_notice(@payment).deliver
          #SendUserDataRdsWorker.perform_async(current_public_user.id, @booking.offer.hotel.name, @booking.offer.hotel.city.name, "Agencia - Reserva - Venda Não Concluída (Erro: Pagamento não efetuado)")
        else
          UserMailer.send_payment_failure_notice(@payment).deliver
          #SendUserDataRdsWorker.perform_async(current_public_user.id, @booking.offer.hotel.name, @booking.offer.hotel.city.name, "Publico - Reserva - Venda Não Concluída (Erro: Pagamento não efetuado)")
        end
      end
      Airbrake.notify(exception, parameters: {user_id: current_public_user.id})
      @credit_card ||= CreditCard.new(params[:credit_card].permit!)
      flash.now[:error] = exception.to_s
      render :new
    end
  end

  private

    def reserve_on_omnibees
      begin
        # 1 = Hoteisnet || 2 = Omnibees
        choice = ChooseOmnibeesOrHoteisnet.choose(@booking.pack_in_hours, @booking.offer.checkin_timestamp) 
        if choice == 1 
          client = Hotelnet::Client.new
        else          
          client = Omnibees::Client.new
        end
        reservation_xml = client.make_booking({ booking: @booking })
        @booking.omnibees_request_xml = client.last_request_xml
        @booking.omnibees_response_xml = reservation_xml
        @booking.save!

        if choice == 1
          reservation_status = Hotelnet::DTO::HotelReservationToBookingResult.parse(reservation_xml)
        else          
          reservation_status = Omnibees::DTO::HotelReservationToBookingResult.parse(reservation_xml)
        end

        if reservation_status.confirmed?
          @booking.omnibees_code = reservation_status.reservation_id
        end
        @booking.save!
      rescue Exception => ex
        Airbrake.notify(ex)
        raise OmnibeesReservationError
      end
      raise OmnibeesReservationError unless reservation_status.confirmed?
    end

    def confirm_booking_and_create_fake_payment_for_view
      @payment = Payment.new
      @payment.user = current_public_user
      @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
      @payment.booking = @booking
      @booking.save!
    end

    def set_and_verify_credit_card
      @credit_card = CreditCard.new(params[:credit_card].permit!)
      @credit_card.user = current_public_user
      @credit_card.valid?
      @credit_card.errors.messages.delete(:maxipago_token)
      raise CannotCreateCreditCardError.new(t("bookings.cc_invalid")) unless @credit_card.errors.messages.empty?
    end

    def send_credit_card_info_to_admin
      AdminMailer.send_credit_card_informations(@credit_card, @booking).deliver
    end

    # find any existing credit card or create a new one for use
    def set_a_valid_credit_card_to_use
      if params[:credit_card][:id].blank? && @booking.traveler_id.nil?
        @credit_card = CreditCard.new(params[:credit_card].permit!)

        @credit_card = CreditCard.where({ user_id: current_public_user.id,
          last_four_digits: @credit_card.last_four_digits,
          expiration_year: @credit_card.expiration_year,
          expiration_month: @credit_card.expiration_month
        }).first
        if @credit_card.nil?
          @credit_card = CreditCard.new(params[:credit_card].permit!)
          @credit_card.user_id = current_public_user.id
          @credit_card.valid?
          @credit_card.errors.messages.delete(:maxipago_token)
          if @credit_card.errors.messages.empty?
            create_token_for_credit_card()
            raise CannotCreateCreditCardError.new(t("bookings.cc_invalid")) unless @credit_card.save
          else
            raise CannotCreateCreditCardError.new(t("bookings.cc_invalid")) unless @credit_card.save
          end
        else
          @credit_card.temporary_number = params[:credit_card][:temporary_number]
          @credit_card.security_code = params[:credit_card][:security_code]
        end
      else
        if (current_public_user.is_a_company? || current_public_user.is_a_agency?)
          @credit_card = @booking.traveler.credit_cards.find(params[:credit_card][:id])
        else
          @credit_card = current_public_user.credit_cards.find(params[:credit_card][:id])
        end
      end
    end

    # check if user already has a token and generate if necessary
    # check if CC already has a token and generate if necessary
    # try to authorize the payment and create the payment
    def use_credit_card_to_confirm_booking
      create_payment

      begin
        authorization_response = MaxiPagoInterface.authorize_transaction({
          payment: @payment
        })
      rescue MaxiPagoInterface::MalformedMaxipagoResponseError => error
        @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
        @payment.maxipago_response += error.to_s
        @payment.save!
        raise PaymentFailedError.new(t('payments.errors.maxipago_response_error'))
      end


      if authorization_response.is_authorized
        @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
        @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
        @booking.save!
      else
        @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
      end
      set_payment_maxipago_response_fields(authorization_response)
      @payment.save!
      
=begin
      if @payment.status == Payment::ACCEPTED_STATUSES[:authorized]
        begin
          MaxiPagoInterface.void_transaction({
            payment: @payment
          })
        rescue => ex
          Rails.logger.error(ex)
          Airbrake.notify(ex)
        end
      end
=end      
      raise PaymentFailedError.new(t('payments.errors.transaction_not_authorized')) unless authorization_response.is_authorized
    end

    def use_credit_card_to_pay_booking
      create_payment

      begin
        authorization_response = MaxiPagoInterface.direct_sale({
          payment: @payment
        })
      rescue MaxiPagoInterface::MalformedMaxipagoResponseError => error
        @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
        @payment.maxipago_response += error.to_s
        @payment.save!
        raise PaymentFailedError.new(t('payments.errors.maxipago_response_error'))
      end

      if authorization_response.success
        @payment.status = Payment::ACCEPTED_STATUSES[:captured]
        @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
        @booking.save!
      else
        @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
      end
      set_payment_maxipago_response_fields(authorization_response)
      @payment.save!
      raise PaymentFailedError.new(t('payments.errors.transaction_not_authorized')) unless authorization_response.success
    end

    def create_payment
      @payment = Payment.new
      @payment.credit_card_id = @credit_card.id
      @payment.user_id = current_public_user.id
      @payment.traveler_id = @booking.traveler_id
      @payment.booking_id = @booking.id
      @payment.status = Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response]
      @payment.save!
    end

    def set_payment_maxipago_response_fields(authorization_response)
      @payment.maxipago_order_id = authorization_response.order_id
      @payment.maxipago_response += authorization_response.body
      @payment.maxipago_transaction_id = authorization_response.transaction_id
    end

    def create_client_token
      maxipago_token = MaxiPagoInterface.generate_client_token_for({
        user: current_public_user
      })
      unless current_public_user.update_attribute(:maxipago_token, maxipago_token)
        raise ClientTokenError.new(t("bookings.cannot_register_cc_client"))
      end
    end

    def create_token_for_credit_card
      @credit_card.maxipago_token = MaxiPagoInterface.generate_credit_card_token_for({
        credit_card: @credit_card
      })
      unless @credit_card.save
        raise CreditCardTokenError.new(t("bookings.cannot_register_cc_in_gateway"))
      end
    end

    def set_resources_and_check_permissions!
      begin
        @booking = current_public_user.bookings.find(params[:booking_id])
        @offer = @booking.offer
        if can_current_public_user_afford?(offer: @offer, number_of_people: @booking.number_of_people)
          if (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
            flash.now[:error] = t("bookings.booking_already_confirmed")
            redirect_to public_booking_path(params[:booking_id])
          end
          if @booking.status == Booking::ACCEPTED_STATUSES[:canceled]
            flash.now[:error] = t("bookings.booking_canceled")
            redirect_to public_booking_path(params[:booking_id])
          end
        else
          flash.now[:error] = t(:message_booking_is_outside_budget_error)
          redirect_to public_root_path
        end
      rescue
        flash.now[:error] = t(:you_cant_confirm_a_booking_that_not_yours)
        redirect_to public_bookings_path
      end
    end

    def extra_person_price_text booking
      if booking.number_of_people <= booking.offer.room_type_initial_capacity
        return "#{number_to_currency 0}"
      elsif booking.number_of_people <= booking.offer.room_type_maximum_capacity
        return "#{booking.number_of_people - booking.offer.room_type_initial_capacity} x #{number_to_currency booking.extra_price_per_person_without_iss}"
      end
    end
end
