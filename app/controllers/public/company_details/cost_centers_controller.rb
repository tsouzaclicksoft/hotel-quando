# encoding: utf-8
class Public::CompanyDetails::CostCentersController < Public::BaseController
  inherit_resources
  actions :show, :new, :create, :edit, :update, :index
  before_filter :authenticate_public_user!
  before_filter :authenticate_agency_or_company_detail!

  def index
    @cost_centers = current_public_user.company_detail.cost_centers
  end

  def new
    # inserir no caso de ser uma company
    @company_detail = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(params[:company_detail_id]) : current_public_user.company_detail
    if @company_detail.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_company_detail_cost_centers_path
      new!
    end
  end

  def create
    @company_detail = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(params[:company_detail_id]) : current_public_user.company_detail
    if @company_detail.blank? 
      redirect_to public_company_details_path
    else
      @cost_center = CostCenter.new()
      @cost_center.company_detail_id = @company_detail.id
      @cost_center.name = params[:cost_center][:name]
      @url_form = public_company_detail_cost_centers_path
      create! do |success, failure|
        success.html do
          redirect_to public_cost_center_path(@cost_center)
        end
      end
    end
  end

  def edit
    @cost_center = CostCenter.find(params[:id])
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_cost_center_path(@cost_center)
    end
  end

  def update
    @cost_center = CostCenter.find(params[:id])
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_cost_center_path(@cost_center)
      update! do |success, failure|
        success.html do
          redirect_to public_cost_center_path(@cost_center)
        end
      end
    end
  end

  def show
    @cost_center = CostCenter.find(params[:id])
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @travelers = @cost_center.travelers
    end
  end

  private
    def build_resource_params
      [params.fetch(:cost_center, {}).permit!]
    end

    def authenticate_agency_or_company_detail!
      if !current_public_user.is_a_agency? && !current_public_user.is_a_company?
        redirect_to public_root_path
      end
    end
end
