# encoding: utf-8
class Public::CompanyDetails::CostCenters::TravelersController < Public::BaseController
  inherit_resources
  actions :show, :new, :create, :edit, :update
  before_filter :authenticate_public_user!
  before_filter :authenticate_agency_or_company_detail!

  def new
    @cost_center = CostCenter.find(params[:cost_center_id])
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_cost_center_travelers_path
      new!
    end
  end

  def create
    @cost_center = CostCenter.find(params[:cost_center_id])
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @traveler = Traveler.new()
      @traveler.cost_center_id = @cost_center.id
      @traveler.company_detail_id = @cost_center.company_detail_id
      @traveler.name = params[:traveler][:name]
      @traveler.job_title_id = params[:traveler][:job_title_id]
      @traveler.email = params[:traveler][:email]
      @traveler.phone_number = params[:traveler][:phone_number]
      @traveler.cpf = params[:traveler][:cpf]
      @traveler.passport = params[:traveler][:passport]
      @traveler.country = params[:traveler][:country]
      @traveler.state = params[:traveler][:state]
      @traveler.city = params[:traveler][:city]
      @traveler.street = params[:traveler][:street]
      @traveler.number = params[:traveler][:number]
      @traveler.complement = params[:traveler][:complement]
      @traveler.postal_code = params[:traveler][:postal_code]
      @url_form = public_cost_center_travelers_path
      create! do |success, failure|
        success.html do
          redirect_to public_traveler_path(@traveler)
        end
      end
    end
  end

  def edit
    @traveler = Traveler.find(params[:id])
    @cost_center = @traveler.cost_center
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_traveler_path(@traveler)
    end
  end

  def update
    @traveler = Traveler.find(params[:id])
    @cost_center = @traveler.cost_center
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_traveler_path(@traveler)
      update! do |success, failure|
        success.html do
          redirect_to public_traveler_path(@traveler)
        end
      end
    end
  end

  def show
    @traveler = Traveler.find(params[:id])
    @cost_center = @traveler.cost_center
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @credit_cards = @traveler.credit_cards
    end
  end

  def traveler_to_user
    @traveler = Traveler.find(params[:id])
    @user = User.new(name: @traveler.name, email: @traveler.email, city: @traveler.city, country: @traveler.country, traveler_id: @traveler.id)
    if @user.save
      flash[:success] = I18n.t('traveler_turned')
      redirect_to public_traveler_path(@traveler)
    else  
      flash[:error] = @user.errors.full_messages
      redirect_to public_traveler_path(@traveler)
    end
  end
  
  private

    def build_resource_params
      [params.fetch(:traveler, {}).permit!]
    end

    def authenticate_agency_or_company_detail!
      if !current_public_user.is_a_agency? && !current_public_user.is_a_company?
        redirect_to public_root_path
      end
    end


end
