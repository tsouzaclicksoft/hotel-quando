# encoding: utf-8
class Public::CompanyDetails::CostCenters::Travelers::CreditCardsController < Public::BaseController
  inherit_resources
  actions :show, :new, :create, :edit, :update
  before_filter :authenticate_public_user!
  before_filter :authenticate_agency_or_company_detail!

  def new
    @traveler = Traveler.find(params[:traveler_id])
    @cost_center = @traveler.cost_center
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_traveler_credit_cards_path
      new!
    end
  end

  def create
    @traveler = Traveler.find(params[:traveler_id])
    @cost_center = @traveler.cost_center
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @credit_card = CreditCard.new()
      @credit_card.traveler_id = @traveler.id
      @credit_card.card_number = params[:credit_card][:card_number]
      @credit_card.cvv = params[:credit_card][:cvv]
      @credit_card.owner_cpf = params[:credit_card][:owner_cpf]
      @credit_card.owner_passport = params[:credit_card][:owner_passport]
      @credit_card.expiration_month = params[:credit_card][:expiration_month]
      @credit_card.expiration_year = params[:credit_card][:expiration_year]
      @credit_card.temporary_number = params[:credit_card][:card_number]
      @url_form = public_traveler_credit_cards_path
      create! do |success, failure|
        success.html do
          redirect_to public_credit_card_path(@credit_card)
        end
      end
    end
  end

  def edit
    @credit_card = CreditCard.find(params[:id])
    @traveler = @credit_card.traveler
    @cost_center = @traveler.cost_center
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_credit_card_path(@credit_card)
    end
  end

  def update
    @credit_card = CreditCard.find(params[:id])
    @traveler = @credit_card.traveler
    @cost_center = @traveler.cost_center
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @credit_card.temporary_number = params[:credit_card][:card_number]
      @url_form = public_credit_card_path(@credit_card)
      update! do |success, failure|
        success.html do
          redirect_to public_credit_card_path(@credit_card)
        end
      end
    end
  end


  def show
    @credit_card = CreditCard.find(params[:id])
    @traveler = @credit_card.traveler
    @cost_center = @traveler.cost_center
    @company_detail = @cost_center.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @credit_cards = @traveler.credit_cards
    end
  end

  private

    def build_resource_params
      [params.fetch(:credit_card, {}).permit!]
    end

    def authenticate_agency_or_company_detail!
      if !current_public_user.is_a_agency? && !current_public_user.is_a_company?
        redirect_to public_root_path
      end
    end
end
