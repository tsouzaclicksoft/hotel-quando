# encoding: utf-8
class Public::HotelController < Public::BaseController
  respond_to :js
  class CheckinDateIsInThePastError < StandardError; end
  class OfferNotAvailable < StandardError; end

  def index
    if is_meeting_room?
      get_meeting_rooms
    # elsif params[:by_search] == 'airs'
    #   new_travel_request
    else
      log = HotelSearchLog.new
      if params[:hotel_id].present?
        hotel = Hotel.find(params[:hotel_id])
        log.latitude = hotel.latitude
        log.longitude = hotel.longitude
        log.checkin_date = params[:by_checkin_date]
        log.checkin_hour = params[:by_checkin_hour].to_i
        log.number_of_people = params[:by_number_of_people]
        log.length_of_pack = params[:by_pack_length]
        log.city_name = hotel.city.name
        log.state_name = hotel.city.state.initials
        log.address = hotel.address
        log.room_category = params[:room_category]
        log.timezone = params[:by_timezone] || ''
        log.save
        session[:current_search_log_id] = log.id
        redirect_to public_hotel_path(id: hotel.id, name: hotel.name.parameterize, length_of_pack: params[:by_pack_length], checkin_date: params[:by_checkin_date], checkin_hour: params[:by_checkin_hour], number_of_people: params[:by_number_of_people], room_category: params[:room_category], timezone: params[:by_timezone])
      else
        log.latitude = params[:by_latitude]
        log.longitude = params[:by_longitude]
        log.checkin_date = params[:by_checkin_date]
        log.checkin_hour = params[:by_checkin_hour].to_i
        log.number_of_people = params[:by_number_of_people]
        log.length_of_pack = params[:by_pack_length]
        log.city_name = params[:city_name]
        log.state_name = params[:state_name]
        log.address = params[:by_address]
        log.room_category = params[:room_category]
        log.timezone = params[:by_timezone] || ''
        log.save
        session[:current_search_log_id] = log.id
      end
      @hotels = []
      begin
        if params[:by_timezone].blank?
          check_in_datetime = "#{params[:by_checkin_date]} #{params[:by_checkin_hour]}:00 -02:00".to_datetime
          timezone = ''
        else
          check_in_datetime = "#{params[:by_checkin_date]} #{params[:by_checkin_hour]}:00 -02:00".in_time_zone(params[:by_timezone].gsub('%2F', '/')).to_datetime
          timezone = params[:by_timezone]
        end
        raise CheckinDateIsInThePastError if check_in_datetime < (DateTime.now)
        @searched_address_coordinates = [params[:by_latitude], params[:by_longitude]]
        @any_available_voucher = EasyTaxiVoucher.available_on(check_in_datetime).any?
        @hotel_search = OffersSearch::Hotels.new({
          number_of_guests: params[:by_number_of_people].to_i,
          latitude: params[:by_latitude].to_f,
          longitude: params[:by_longitude].to_f,
          check_in_datetime: check_in_datetime,
          pack_in_hours: params[:by_pack_length].to_i,
          room_category: params[:room_category].to_i,
          timezone: timezone,
          max_distance_in_km: params[:max_distance_in_km]
        })
        @search_exact_match = HotelDistanceSorter.sort(@hotel_search.results.exacts, @searched_address_coordinates)
        @search_similar_match = HotelDistanceSorter.sort(@hotel_search.results.similars, @searched_address_coordinates)
        if params[:commit] == t(:sort_by_price)
          @search_exact_match = @hotel_search.results.exacts.sort_by {|hotel| hotel[:min_offer][:price]}
          @search_similar_match = @hotel_search.results.similars.sort_by {|hotel| hotel[:min_offer][:price]}
        end
        begin
          log.exact_match_hotels_ids = @hotel_search.results.exacts.collect{|hotel| hotel[:hotel][:id]}.join("-")
          log.similar_match_hotels_ids = @hotel_search.results.similars.collect{|hotel| hotel[:hotel][:id]}.join("-")
          log.save
        rescue
        end

      rescue CheckinDateIsInThePastError
        flash[:error] = t(:error_message_selected_checkin_datetime_is_in_the_past)
        redirect_to public_root_path
      rescue => ex
        Rails.logger.error(ex)
        Airbrake.notify(ex)
        flash[:error] = t(:message_checkin_date_and_address_error)
        redirect_to public_root_path
      end
    end
  end

  def show
    begin
      @hotel = Hotel.includes(:city).includes(room_types: :photos).find(params[:id])
      @business_room = BusinessRoom.includes(:photos).find_by id: (params[:business_room])
      unless @business_room.nil?
        @offer_business_room = @business_room.is_a_meeting_room? ? MeetingRoomOffer.find(params[:offer_id]) : EventRoomOffer.find(params[:offer_id])
      end
      if search_params
        if params[:checkin_timestamp] #user is comming from booking
          checkin_datetime = params[:checkin_timestamp].to_datetime
          params[:length_of_pack] = params[:pack_in_hours]
          params[:checkin_date] = checkin_datetime.to_date
          params[:checkin_hour] = checkin_datetime.hour
        else
          if params[:timezone].blank?
            date_time = "#{params[:checkin_date]} #{params[:checkin_hour]}:00"
            timestamp = date_time.to_datetime.to_i
            timezone = get_timezone(timestamp, @hotel.latitude, @hotel.longitude)
            checkin_datetime = date_time.in_time_zone(timezone).to_datetime
          else
            checkin_datetime = "#{params[:checkin_date]} #{params[:checkin_hour]}:00".in_time_zone(params[:timezone].gsub('%2F', '/')).to_datetime
          end

        end
        raise OfferNotAvailable if checkin_datetime < (DateTime.now + 1.hour)

        session[:last_hotel_visited_id] = @hotel.id
        pack_in_hours = params[:length_of_pack].to_i

        if @business_room
          @offer_business_room = @business_room.is_a_meeting_room? ? MeetingRoomOffer.find(params[:offer_id]) : EventRoomOffer.find(params[:offer_id])
          if !@offer_business_room.available?
            flash[:warning] = t(:error_message_hotel_without_offers_to_show)
            redirect_to public_root_path
          end
        else
          @room_type_search = OffersSearch::RoomTypes.new({
            number_of_guests: params[:number_of_people].to_i,
            hotel_id: @hotel.id,
            check_in_datetime: checkin_datetime,
            pack_in_hours: pack_in_hours
          })

          @search_exact_match = @room_type_search.results.exacts.sort_by { |s|
            offer = s[:min_offer]
            offer.save! if offer.is_omnibees?
            offer.reload #we need all the fields to calculate value for the entire number of guests
            s[:min_offer].price
          }

          @search_exact_match = @search_exact_match.uniq {|t|t[:min_offer][:room_type_id]}

          @search_similar_match = @room_type_search.results.similars.sort_by { |s|
            offer = s[:min_offer]
            offer.save! if offer.is_omnibees?
            [ offer.checkin_timestamp,
              offer.get_number_of_common_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)),
              (offer.checkin_timestamp - checkin_datetime),
              -(offer.get_number_of_exceeding_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)))
            ]
          }

          @search_similar_match = @search_similar_match.uniq {|t|t[:min_offer][:room_type_id]}
        end
      else
        @search_exact_match = ''
        @search_similar_match = ''
      end
    rescue OfferNotAvailable
      flash[:error] = t(:error_message_offer_not_available)
      redirect_to public_root_path
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      flash[:error] = t(:error_message_search_params_error_test)
      redirect_to public_root_path
    end
  end

  def hotel_room_type_show
    begin
      if params[:checkin_timestamp] #user is comming from booking
        checkin_datetime = params[:checkin_timestamp].to_datetime
        params[:length_of_pack] = params[:pack_in_hours]
        params[:checkin_date] = checkin_datetime.to_date
        params[:checkin_hour] = checkin_datetime.hour
      else
        if params[:timezone].blank?
          checkin_datetime = "#{params[:checkin_date]} #{params[:checkin_hour]}:00".to_datetime
        else
          checkin_datetime = "#{params[:checkin_date]} #{params[:checkin_hour]}:00".in_time_zone(params[:timezone]).to_datetime
        end
      end

      raise if checkin_datetime < (DateTime.now + 1.hour)
      @room_type = RoomType.includes(:photos).find(params[:id])
      @hotel = @room_type.hotel
    rescue => ex
      flash[:error] = t(:error_message_search_params_error)
      redirect_to public_root_path
    end
  end

  private
    def get_timezone(timestamp, lat, lng)
      json = TimezoneSrv.get(lat, lng, timestamp)
      # timezone_value  = (json["dstOffset"]+json["rawOffset"]) * 1000
      # response_hash   = {timezone_value: timezone_value, timezone_id: json["timeZoneId"], timezone_name: json["timeZoneName"]}
      response_hash   = {timezone_id: json["timeZoneId"], timezone_name: json["timeZoneName"]}
      response_hash[:timezone_id]
    end

    def search_params
      params.include?("checkin_date" || "checkin_dateMTR" || "checkin_hour" || "checkin_hourMTR" || "length_of_pack" || "length_of_packMTR" || "number_of_people" || "timezone" || "controller" || "action" || "locale" || "id" || "name")
    end

    def is_meeting_room?
      (params[:by_search] == 'meetingRoom')
    end

    def get_meeting_rooms
      log = HotelSearchLog.new
      log.latitude = params[:by_latitude]
      log.longitude = params[:by_longitude]
      log.checkin_date = params[:by_checkin_dateMTR]
      log.checkin_hour = params[:by_checkin_hourMTR].to_i
      log.number_of_people = params[:by_number_of_people]
      log.length_of_pack = params[:by_pack_lengthMTR]
      log.city_name = params[:city_name]
      log.state_name = params[:state_name]
      log.address = params[:by_address]
      log.room_category = params[:room_category]
      log.timezone = params[:by_timezone] || ''
      log.save

      if params[:by_timezone].blank?
        check_in_datetime = "#{params[:by_checkin_dateMTR]} #{params[:by_checkin_hourMTR]}:00".to_datetime
        timezone = ''
      else
        check_in_datetime = "#{params[:by_checkin_dateMTR]} #{params[:by_checkin_hourMTR]}:00".in_time_zone(params[:by_timezone].gsub('%2F', '/')).to_datetime
        timezone = params[:by_timezone]
      end
      @searched_address_coordinates = [params[:by_latitude], params[:by_longitude]]
      @meeting_room_search = OffersSearch::Hotels.new({
        number_of_guests: params[:by_number_of_people].to_i,
        latitude: params[:by_latitude].to_f,
        longitude: params[:by_longitude].to_f,
        check_in_datetime: check_in_datetime,
        pack_in_hours: params[:by_pack_lengthMTR].to_i,
        room_category: params[:room_category].to_i,
        timezone: timezone,
        max_distance_in_km: params[:max_distance_in_km],
        search_by: params[:by_search],
        room_category: 2
      })
      @search_exact_match = HotelDistanceSorter.sort(@meeting_room_search.results.exacts, @searched_address_coordinates)
      @search_similar_match = HotelDistanceSorter.sort(@meeting_room_search.results.similars, @searched_address_coordinates)
      if params[:commit] == t(:sort_by_price)
        @search_exact_match = @meeting_room_search.results.exacts.sort_by {|hotel| hotel[:min_offer][:price]}
        @search_similar_match = @meeting_room_search.results.similars.sort_by {|hotel| hotel[:min_offer][:price]}
      end
    end

    def new_travel_request

      if public_user_is_logged

        createTravelRequest

        if isRoundTrip

          createIdaAir
          createReturnAir

        elsif
           createIdaAir
        end

        HqConsultant.all.each do |hq|
          TravelRequestMailer.send_request_air_to_agency(@travel, hq.email).deliver
        end

      else
        flash[:error] = "Necessário estar logado para solicitar passagens"
      end

      redirect_to public_root_path

    end

    def public_user_is_logged
      return current_public_user != nil && current_public_user.is_a_company?
    end

    def parsedDatesAir
      @originDateAir = params[:by_origin_date_air] + ' ' + params[:by_origin_hour_air]
      @destinyDateAir = params[:by_destiny_date_air] + ' ' + params[:by_destiny_hour_air]

      @originDateAir = Time.strptime(@originDateAir.to_datetime , '%Y/%d/%m/ %H:%M')
      @destinyDateAir = Time.strptime(@destinyDateAir.to_datetime , '%d/%m/%Y %H:%M')
    end

    def isRoundTrip
      return params[:by_round_trip_air] == "true" || params[:by_round_trip_air] == true
    end

    def updateTaxeAir(id_air, id_air_taxe)
      taxe = AirTaxe.find(id_air_taxe)
      taxe.air_id = id_air
      taxe.save
    end

    def createTravelRequest

      @travel = TravelRequest.new

      @travel.date_request = Time.now
      @travel.request_status = TravelRequest::ACCEPTED_REQUEST_STATUS[:pending_agency]
      @travel.payment_status = TravelRequest::ACCEPTED_PAYMENT_STATUS[:waiting_for_payment_method]
      @travel.user_id = current_public_user.id
      @travel.contact_traveler = params[:by_number_contact]
      @travel.save

    end

    def createIdaAir

      @air_taxe_ida = AirTaxe.new
      @air_taxe_ida.save

      @air_ida = Air.new
      @air_ida.origin = params[:by_address_origin_air]
      @air_ida.destiny = params[:by_address_destiny_air]
      @air_ida.departure_date = DateTime.parse(params[:by_origin_date_air] + " " + params[:by_origin_hour_air])
      @air_ida.flight_class = Air::ACCEPTED_CLASSES[:economy]
      @air_ida.is_online = false
      @air_ida.travel_request_id = @travel.id
      @air_ida.air_taxe_id = @air_taxe_ida.id
      @air_ida.save

      updateTaxeAir(@air_ida.id, @air_taxe_ida.id)
    end

    def createReturnAir

      @air_taxe_return = AirTaxe.new
      @air_taxe_return.save

      @air_return = Air.new
      @air_return.origin = params[:by_address_destiny_air]
      @air_return.destiny = params[:by_address_origin_air]
      @air_return.departure_date = DateTime.parse(params[:by_destiny_date_air] + " " + params[:by_destiny_hour_air])
      @air_return.flight_class = Air::ACCEPTED_CLASSES[:economy]
      @air_return.is_online = false
      @air_return.travel_request_id = @travel.id
      @air_return.air_taxe_id = @air_taxe_return.id
      @air_return.save

      updateTaxeAir(@air_return.id, @air_taxe_return.id)
    end


end
