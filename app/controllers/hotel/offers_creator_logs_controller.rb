# encoding: utf-8
class Hotel::OffersCreatorLogsController < Hotel::BaseController
  actions :index, :show
  before_action :set_type, only: :index

  protected
    def collection
      @offers_logs ||= end_of_association_chain.page(params[:page]).per(20)
    end

 	private
	  def set_type
	    @type = params[:type]
	  end
end
