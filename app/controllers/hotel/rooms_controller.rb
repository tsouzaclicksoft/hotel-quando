# encoding: utf-8
class Hotel::RoomsController < Hotel::BaseController
  actions :index, :create
  has_scope :by_number
  has_scope :by_room_type_id, type: :array

  def create
    numbers_array = []
    if params[:room_registration_method] == 'Dropdown'
      Room.create_rooms_from_quantity(params[:room_quantity], params[:room_type], current_hotel_hotel.id)
    else
      numbers_array = params[:room_numbers].to_s.split(",").map(&:strip)
      numbers_with_space = []
      numbers_array.delete_if do | number |
        if number.match(' ')
          numbers_with_space << number
          true
        end
      end
      failed_numbers = Room.save_rooms_from_numbers_array_and_return_invalids(numbers_array, params[:room_type], current_hotel_hotel.id)
      unless (failed_numbers.size == 0)
       if (failed_numbers.size > 1)
          flash[:notice] = t(:error_message_number_already_included_plural) + " #{failed_numbers.to_s.gsub('[','').gsub(']','')}."
        else
          flash[:notice] = t(:error_message_number_already_included) + " #{failed_numbers.to_s.gsub('[','').gsub(']','')}."
        end
      end
      unless (numbers_with_space.size == 0)
        if (numbers_with_space.size > 1)
          flash[:warning] = t(:error_message_invalid_number_plural) + " #{numbers_with_space.to_s.gsub('[','').gsub(']','')}."
        else
          flash[:warning] = t(:error_message_invalid_number) + " #{numbers_with_space.to_s.gsub('[','').gsub(']','')}."
        end
      end
    end

    flash[:success] = I18n.t('controllers.hotel.room_types_controller.create_success')
    redirect_to hotel_rooms_path
  end

  protected
    def collection
      @rooms ||= end_of_association_chain.includes(:room_type).page(params[:page]).per(40)
    end
end
