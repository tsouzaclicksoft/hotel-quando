# encoding: utf-8
class Hotel::SessionsController < Devise::SessionsController
  layout 'hotel/login'

  def after_sign_in_path_for(user)
    hotel_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    new_hotel_hotel_session_path
  end
end
