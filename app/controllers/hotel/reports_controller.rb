# encoding: utf-8
class Hotel::ReportsController < Hotel::BaseController
  helper_method :bookings_per_day_data, :bookings_per_pack_and_room_type_data, :advance_time_for_booking_per_pack_data
  respond_to :js, :html

  def index
    initial_date = Date.today.beginning_of_month
    final_date = Date.today.end_of_month
    @bookings_data = Booking.joins(:room_type, :offer).where("room_types.hotel_id = ?", current_hotel_hotel.id).by_checkin_date(initial_date, final_date).by_status([Booking::ACCEPTED_STATUSES[:confirmed], Booking::ACCEPTED_STATUSES[:confirmed_and_captured], Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced], Booking::ACCEPTED_STATUSES[:confirmed_without_card]])
    @no_shows_data = Booking.joins(:room_type, :offer).where("room_types.hotel_id = ?", current_hotel_hotel.id).by_checkin_date(initial_date, final_date).by_status(Booking::ACCEPTED_STATUSES[:no_show_paid_with_success])

    @bookings_info = bookings_info
    @no_shows_info = no_shows_info
    @income_per_pack = income_per_pack_data

    @bookings = @bookings_data.eager_load(:offer).page(params[:page]).per(20)
    @no_shows = @no_shows_data.eager_load(:offer).page(params[:page]).per(20)

    @total_earned = @bookings_info[:total_value] + @no_shows_info[:total_value]
    @total_earned_without_iss = @bookings_info[:total_value_without_iss] + @no_shows_info[:total_value_without_iss]
    @total_credit_card_tax = @bookings_info[:credit_card_tax] + @no_shows_info[:credit_card_tax]
    # Removing hotel quando comission from report view, leaving it here, so if for some reason
    # it needs to appear again, it is already here
    # @total_comission = @bookings_info[:comission] + @no_shows_info[:comission]
    @final_total_value = @bookings_info[:final_total_value] + @no_shows_info[:final_total_value]
  end

  private

  def bookings_info
    total_value = bookings_total_price(@bookings_data)
    # total_value_without_iss = (100 - current_hotel_hotel.iss_in_percentage)/100*total_value
    total_value_without_iss = ((total_value * 100) / (current_hotel_hotel.iss_in_percentage + 100))
    comission = current_hotel_hotel.comission_in_percentage/100*total_value_without_iss
    credit_card_tax = credit_card_tax_for(@bookings_data.where(status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured]))
    { :total => @bookings_data.count,
      :total_paid_at_the_hotel => @bookings_data.where(status: [Booking::ACCEPTED_STATUSES[:confirmed], Booking::ACCEPTED_STATUSES[:confirmed_without_card]]).count,
      :total_paid_online => @bookings_data.where(status: [Booking::ACCEPTED_STATUSES[:confirmed_and_captured], Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced] ]).count,
      :total_value => total_value,
      :total_value_without_iss => total_value_without_iss,
      credit_card_tax: credit_card_tax,
      :comission => comission,
      final_total_value: (total_value_without_iss - (credit_card_tax + comission))
    }
  end

  def no_shows_info
    total_value = no_shows_total_price(@no_shows_data)
    # total_value_without_iss = (100 - current_hotel_hotel.iss_in_percentage)/100*total_value
    total_value_without_iss = ((total_value * 100) / (current_hotel_hotel.iss_in_percentage + 100))
    # without_comission = (100 - current_hotel_hotel.comission_in_percentage)/100*total_value_without_iss
    comission = current_hotel_hotel.comission_in_percentage/100*total_value_without_iss
    credit_card_tax = credit_card_tax_for(@no_shows_data)
    { :total => @no_shows_data.count,
      :total_value => total_value,
      :total_value_without_iss => total_value_without_iss,
      credit_card_tax: credit_card_tax_for(@no_shows_data),
      comission: comission,
      final_total_value: (total_value_without_iss - (credit_card_tax + comission))
    }
  end

  def credit_card_tax_for(bookings)
    total_tax = 0.0
    if bookings.try(:first).try(:status) == Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
      bookings.each do | booking |
        total_tax += (booking.no_show_value * (Payment::CREDIT_CARD_OPERATION_TAX_IN_PERCENTAGE/100))
      end
    else
      bookings.each do | booking |
        total_tax += (booking.total_price * (Payment::CREDIT_CARD_OPERATION_TAX_IN_PERCENTAGE/100))
      end
    end
    total_tax
  end

  def bookings_per_day_data
    bookings_per_day_array = []
    date = Time.now
    beginning_of_month = Date.new(date.year, date.month, date.beginning_of_month.day)
    end_of_month = Date.new(date.year, date.month, date.end_of_month.day)

    (beginning_of_month..end_of_month).each do |each_date|
      bookings_number = @bookings_data.where(:checkin_date => each_date).count
      bookings_per_day_array << [ each_date.day, bookings_number ]
    end
    bookings_per_day_array
  end

  def income_per_pack_data
    income_array = []
    Offer::ACCEPTED_LENGTH_OF_PACKS.each do |pack|
      income_array << { :pack => "#{pack}h", :value => (bookings_total_price(@bookings_data.by_pack_length(pack)) + no_shows_total_price(@no_shows_data.by_pack_length(pack))) }
    end
    income_array
  end

  def bookings_per_pack_and_room_type_data
    per_room_type_array = [[],[]]
    current_hotel_hotel.room_types.each do |room_type|
      per_pack_array = []
      bookings_in_all_packs = 0
      Offer::ACCEPTED_LENGTH_OF_PACKS.reverse.each do |pack|
        bookings_number = @bookings_data.by_room_type_id(room_type.id).by_pack_length(pack).count
        bookings_in_all_packs += bookings_number
        per_pack_array << [ bookings_number, "#{pack}h"]
      end
      per_pack_array << [ bookings_in_all_packs, "all packs"]
      per_room_type_array[0] << per_pack_array
      per_room_type_array[1] << room_type.full_name_pt_br
    end
    per_room_type_array
  end

  def advance_time_for_booking_per_pack_data
    per_pack_array = []
    Offer::ACCEPTED_LENGTH_OF_PACKS.reverse.each do |pack|
      total_advance_time = 0
      average_advance_time = 0
      bookings_number = 0
      @bookings_data.by_pack_length(pack).eager_load(:offer).each do |booking|
        checkin_time = (booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute)
        total_advance_time += (checkin_time - booking.created_at) / (24*60*60)
        bookings_number += 1
      end
      if bookings_number > 0
        average_advance_time = total_advance_time/bookings_number
      end
      per_pack_array << [ average_advance_time, "#{pack}h"]
    end
    per_pack_array
  end

  def bookings_total_price bookings
    total_price = 0
    bookings.each do |booking|
        total_price += booking.total_price_without_tax
    end
    total_price
  end

  def no_shows_total_price no_shows
    total_price = 0
    no_shows.each do |no_show|
        total_price += no_show.no_show_value_without_tax
    end
    total_price
  end
end
