# encoding: utf-8
class Hotel::InfoController < Hotel::BaseController
  actions :none

  def update_password
    if current_hotel_hotel.update_with_password(hotel_params)
      sign_in(current_hotel_hotel, :bypass => true)
      flash[:success] = I18n.t('controllers.hotel.info_controller.upate_password')
      redirect_to hotel_root_path
    else
      render "edit"
    end

  end

  private

  def hotel_params
    params.required(:hotel).permit(:password, :password_confirmation, :current_password)
  end

end
