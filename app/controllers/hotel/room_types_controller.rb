# encoding: utf-8
class Hotel::RoomTypesController < Hotel::BaseController
  actions :all, :except => [ :destroy ]
  has_scope :by_hotel_id

  def create
    create! do |success, failure|
      success.html do
        save_rooms_if_room_type_is_valid
        redirect_to hotel_room_type_path(@room_type), :flash => { :success => I18n.t('controllers.hotel.room_types_controller.create_success') }
      end
    end
  end

  def update
    update! do |success, failure|
      success.html do
        redirect_to hotel_room_type_path(@room_type)
      end
    end
  end

  private

  def save_rooms_if_room_type_is_valid
    if @room_type.persisted?
      numbers_array = []

      if params[:room_registration_method] == 'Dropdown'
        params[:room_quantity].to_i.times do | i |
          numbers_array << (i + 1).to_s
        end
      else
        numbers_array = params[:room_numbers].to_s.split(",").map(&:strip)
      end

      if numbers_array.empty?
        return
      end
      numbers_with_space = []
      numbers_array.delete_if do | number |
        if number.match(' ')
          numbers_with_space << number
          true
        end
      end
      failed_numbers = Room.save_rooms_from_numbers_array_and_return_invalids(numbers_array, @room_type.id, current_hotel_hotel.id)
      unless (failed_numbers.size == 0)
        if (failed_numbers.size > 1)
          flash[:notice] = t(:error_message_number_already_included_plural) + " #{failed_numbers.to_s.gsub('[','').gsub(']','')}."
        else
          flash[:notice] = t(:error_message_number_already_included) + " #{failed_numbers.to_s.gsub('[','').gsub(']','')}."
        end
      end
      unless (numbers_with_space.size == 0)
        if (numbers_with_space.size > 1)
          flash[:warning] = t(:error_message_invalid_number_plural) + " #{numbers_with_space.to_s.gsub('[','').gsub(']','')}."
        else
          flash[:warning] = t(:error_message_invalid_number) + " #{numbers_with_space.to_s.gsub('[','').gsub(']','')}."
        end
      end
    end
  end

  def build_resource_params
    [params.fetch(:room_type, {}).permit(:booking_name_pt_br, :booking_name_en, :booking_name_es, :name_pt_br, :name_en, :name_es, :description_en, :description_pt_br, :description_es, :initial_capacity, :maximum_capacity, :single_bed_quantity, :double_bed_quantity, :room_registration_method, :photos_attributes => [:id, :title_en, :title_pt_br, :title_es, :direct_image_url, :_destroy] )]
  end

end
