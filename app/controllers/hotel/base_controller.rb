# encoding: utf-8
class Hotel::BaseController < ActionController::Base
  protect_from_forgery
  inherit_resources
  layout 'hotel'
  before_filter :authenticate_hotel_hotel!, :set_locale
  helper Shared::CurrencyCurrentHelper

  protected

  def authorize
    if !authorized?
      redirect_to action: 'index'
    end
  end

  def authorized?
    current_admin_manager.present?
  end

  def begin_of_association_chain
    current_hotel_hotel
  end

  def set_locale
    unless action_name == 'timezone'
      if params[:locale] != 'pt_br' || params[:lang] == 'pt-br'
        cookies[:locale] = params[:locale]
      elsif current_public_user
        cookies[:locale] = current_public_user.locale
      else
        if cookies[:locale].blank?
          cookies[:locale] = set_locale_from_header
        end
      end
    end

    I18n.locale = I18n.available_locales.include?(cookies[:locale].try(:to_sym)) ? cookies[:locale] : I18n.default_locale
  end

  def set_locale_from_header
    request.env['HTTP_ACCEPT_LANGUAGE'].gsub! 'pt-BR', 'pt_BR' unless request.env['HTTP_ACCEPT_LANGUAGE'].blank?
    return http_accept_language.compatible_language_from(I18n.available_locales) || 'en'
  end
end
