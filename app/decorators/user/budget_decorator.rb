class User::BudgetDecorator < SimpleDelegator

  def can_afford?(options = {})
    return true unless self.is_a_company_employee? # if the user is not a company employee, he is the one who decides if can afford or not
    options[:number_of_people] ||= 1
    options[:room_category] ||= 1
    offer = options[:offer]
    room_category = offer.try(:room_category) || options[:room_category]
    pack_in_hours = offer.try(:pack_in_hours) || options[:pack_in_hours]
    # define which default pricing policy percentage to use
    if room_category == Hotel::ROOM_CATEGORY[:offer]
      percentage_the_user_can_spend_in_decimal = HotelQuando::Constants::DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK[ pack_in_hours.to_s + '_hours'].to_f / 100.0
    else
      percentage_the_user_can_spend_in_decimal = HotelQuando::Constants::BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK[ pack_in_hours.to_s + '_hours'].to_f / 100.0
    end
    if (options[:price_without_tax])
      price = options[:price_without_tax]
    else
      booking = Booking.new
      booking.offer = offer
      booking.number_of_people = options[:number_of_people]
      price = booking.total_price
    end

    #price_is_in_user_monthly_budget = (price <= available_budget_in_current_month(options))
    price_is_in_the_user_budget_for_the_24_hours_pack = (price <= budget_for_24_hours_pack.to_f * percentage_the_user_can_spend_in_decimal)

    #return price_is_in_user_monthly_budget && price_is_in_the_user_budget_for_the_24_hours_pack
    return  price_is_in_the_user_budget_for_the_24_hours_pack
  end

  def available_budget_in_current_month(options = {})
    self.monthly_budget.to_f - total_spent_in_current_month(options)
  end

  def total_spent_in_current_month(options = {})
    unless @total && !options[:reload]
      @total = 0
      self.bookings.where(status: [
          Booking::ACCEPTED_STATUSES[:confirmed],
          Booking::ACCEPTED_STATUSES[:no_show_paid_with_success],
          Booking::ACCEPTED_STATUSES[:no_show_with_payment_error],
          Booking::ACCEPTED_STATUSES[:confirmed_and_captured],
          Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced],
          Booking::ACCEPTED_STATUSES[:no_show_with_payment_pending]
        ]).by_checkin_date(Date.today.beginning_of_month, Date.today.end_of_month).each do | booking |
          @total += booking.total_price
        end
    end
    @total
  end

  def self.decorate(user)
    new(user)
  end

  # Doing this to be able to set the decorated user in active record relations
  def is_a?(target_class)
    super || __getobj__.is_a?(target_class)
  end

  def class
    __getobj__.class
  end
end
