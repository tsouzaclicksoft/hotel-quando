class HqConsultantMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_login_and_password(consultant)
    @consultant = consultant
    headers[:"X-MC-Tags"] = "Primeiro cadastro para agência"
    mail(:to => @consultant.email, :subject => 'HotelQuando.com - Senha de acesso')
  end

end
