class VoucherMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def easy_taxi_linked(voucher_id)
    @voucher = EasyTaxiVoucher.find(voucher_id)
    if @voucher.booking.user.locale == 'en'
      mail(:to => @voucher.booking.user.email , :subject => 'HotelQuando.com - We will pay part of your taxi')
    else
      mail(:to => @voucher.booking.user.email , :subject => 'HotelQuando.com - Vamos pagar parte do seu táxi')
    end
  end

  def uber_linked(voucher_id)
    @voucher = UberVoucher.find(voucher_id)
    if @voucher.booking.user.locale == 'en'
      mail(:to => @voucher.booking.user.email , :subject => "HotelQuando.com - Don't know Uber yet?")
    else
      mail(:to => @voucher.booking.user.email , :subject => 'HotelQuando.com - Ainda não conhece o Uber?')
    end
  end
end
