class ReferredFriendMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_refer_code_to_friend(referred_friend_log)
    @promotional_code = referred_friend_log.promotional_code
    @referred_friend_log = referred_friend_log
  	@user = referred_friend_log.user
  	I18n.with_locale(referred_friend_log.user.locale) do
  		headers[:"X-MC-Tags"] = I18n.t("mailers.referred_friend_mailer.send_refer_code_to_friend.header")
      subject = I18n.t("mailers.referred_friend_mailer.send_refer_code_to_friend.subject", friend_name: referred_friend_log.friend_name)
      mail(:to => @referred_friend_log.friend_email , :subject => subject)
  	end
  end
end