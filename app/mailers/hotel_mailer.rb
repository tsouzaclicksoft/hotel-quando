class HotelMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_thank_you_notice(hotel)
    @user = hotel
    @hotel = hotel
    headers[:"X-MC-Tags"] = "Para Todos Nossos Clientes e Parceiros: Muito Obrigado! Muchas Gracias! Thank you!"
    mail(:to => @hotel.email, :subject => "Para Todos Nossos Clientes e Parceiros: Muito Obrigado!")
  end

  def rooms_or_meeting_rooms_notice_emails
    emails = @booking.is_meeting_offer? ? @hotel.emails_for_notice_meeting_room : @hotel.emails_for_notice
    if @booking.promotional_code_log.present? && @booking.promotional_code_log.promotional_code.notice_email.present?
      emails << @booking.promotional_code_log.promotional_code.notice_email
    end
    return emails
  end

  def send_login_and_password(hotel)
    @hotel = hotel
    I18n.with_locale(@hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.hotel_mailer.send_login_and_password.header")
      mail(:to => @hotel.email, :subject => I18n.t("mailers.hotel_mailer.send_login_and_password.subject"))
    end
  end

  def send_booking_cancellation_notice(booking)
    @booking = booking
    @hotel = booking.hotel
    I18n.with_locale(@hotel.locale) do
      unless @booking.status == Booking::ACCEPTED_STATUSES[:canceled]
        raise I18n.t("mailers.hotel_mailer.send_booking_cancellation_notice.error")
      end
      headers[:"X-MC-Tags"] = I18n.t("mailers.hotel_mailer.send_booking_cancellation_notice.header")
      subject = I18n.t("mailers.hotel_mailer.send_booking_cancellation_notice.subject")
      mail(:to => rooms_or_meeting_rooms_notice_emails , :subject => subject)
    end
  end

  def send_booking_confirmation_notice(booking)
    @booking = booking
    @hotel = booking.hotel
    subject = ''

    I18n.with_locale(@hotel.locale) do
      if @booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured] || @booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
        headers[:"X-MC-Tags"] = I18n.t("mailers.hotel_mailer.send_booking_confirmation_notice.paid_header")
        subject = I18n.t("mailers.hotel_mailer.send_booking_confirmation_notice.paid_subject", id: @booking.id)
      else
        headers[:"X-MC-Tags"] = I18n.t("mailers.hotel_mailer.send_booking_confirmation_notice.confirmed_header")
        subject = I18n.t("mailers.hotel_mailer.send_booking_confirmation_notice.confirmed_subject", id: @booking.id)
      end
    end

    mail(:to => rooms_or_meeting_rooms_notice_emails, :subject => subject)
  end

  def noshow_successfull_charged_notice(booking)
    @hotel = booking.hotel
    @booking = booking
    I18n.with_locale(@hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.hotel_mailer.noshow_successfull_charged_notice.header")
      mail(:to => rooms_or_meeting_rooms_notice_emails, :subject => I18n.t("mailers.hotel_mailer.noshow_successfull_charged_notice.subject"))
    end
  end

  def no_show_with_charge_error_notice(booking)
    @hotel = booking.hotel
    @booking = booking
    I18n.with_locale(@hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.hotel_mailer.no_show_with_charge_error_notice.header")
      mail(:to => rooms_or_meeting_rooms_notice_emails , :subject => I18n.t("mailers.hotel_mailer.no_show_with_charge_error_notice.subject"))
    end
  end

  def no_show_pending_charge_notice(booking)
    @hotel = booking.hotel
    @booking = booking
    I18n.with_locale(@hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.hotel_mailer.no_show_pending_charge_notice.header")
      mail(:to => rooms_or_meeting_rooms_notice_emails, :subject => I18n.t("mailers.hotel_mailer.no_show_pending_charge_notice.subject"))
    end
  end

  def send_booking_request_confirmation(booking)
    @hotel = booking.hotel
    @booking = booking
    I18n.with_locale(@hotel.locale) do
      headers[:"X-MC-Tags"] = 'HotelQuando.com - Solicitação de Reserva confirmada com sucesso'
      mail(:to => rooms_or_meeting_rooms_notice_emails , :subject => "HotelQuando.com - Solicitação de Reserva confirmada com sucesso - ##{@booking.id}")
    end
  end

end
