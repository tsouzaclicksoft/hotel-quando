# encoding: utf-8
class CompanyMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_login_password(company)
    @company = company
    headers[:"X-MC-Tags"] = "Primeiro cadastro para empresas"
    mail(:to => @company.email, :subject => 'HotelQuando.com - Senha de acesso')
  end
end
