class ExecutiveResponsibleMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_login_and_password(executive_responsible)
    @executive_responsible = executive_responsible
    headers[:"X-MC-Tags"] = "Primeiro cadastro para agência"
    mail(:to => @executive_responsible.email, :subject => 'HotelQuando.com - Senha de acesso')
  end

end
