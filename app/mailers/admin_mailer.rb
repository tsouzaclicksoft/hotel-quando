# encoding: utf-8
class AdminMailer < ActionMailer::Base
  layout 'email_admin'
  default from: HotelQuando::Constants::EMAIL_FROM
  MAIL_TO_SEND_HOTEL_PARTNERSHIPS = 'parceiros@hotelquando.com'
  MAIL_TO_SEND_COMPANY_PARTNERSHIPS = 'corporate@hotelquando.com'
  MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS = 'parceiros@hotelquando.com'

  def send_new_possible_partner_contact(possible_partner_contact)
    @possible_partner_contact = possible_partner_contact
    I18n.with_locale(@possible_partner_contact.locale) do
      if possible_partner_contact.type_of_contact == PossiblePartnerContact::TYPES_OF_CONTACT[:hotel_contact]
        headers[:"X-MC-Tags"] = I18n.t('mailers.admin.send_new_possible_partner_contact.hotel_header')
        mail(:to => MAIL_TO_SEND_HOTEL_PARTNERSHIPS , :subject => I18n.t('mailers.admin.send_new_possible_partner_contact.hotel_subject'))
      elsif possible_partner_contact.type_of_contact == PossiblePartnerContact::TYPES_OF_CONTACT[:company_contact]
        headers[:"X-MC-Tags"] = I18n.t('mailers.admin.send_new_possible_partner_contact.company_header')
        mail(:to => MAIL_TO_SEND_COMPANY_PARTNERSHIPS , :subject => I18n.t('mailers.admin.send_new_possible_partner_contact.company_subject'))
      else
        raise I18n.t('mailers.admin.send_new_possible_partner_contact.error')
      end
    end

  end


  def send_no_show_refund_error_notice(booking)
    @booking = booking
    I18n.with_locale(@booking.hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t('mailers.admin.refund_error.header')
      mail(:to => MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS , :subject => I18n.t('mailers.admin.refund_error.subject'))
    end
  end

  def send_credit_card_informations(credit_card, booking)
    @credit_card = credit_card
    @booking = booking
    I18n.with_locale(@booking.hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t('mailers.admin.booking_confirmed.header')
      mail(:to => MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS , :subject => I18n.t('mailers.admin.booking_confirmed.subject'))
    end
  end

  def no_show_with_charge_error_notice(booking)
    @booking = booking
    I18n.with_locale(@booking.hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t('mailers.admin.no_show_error.header')
      mail(:to => MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS , :subject => I18n.t('mailers.admin.no_show_error.subject'))
    end
  end

  def no_show_pending_charge_notice(booking)
    @booking = booking
    I18n.with_locale(@booking.hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t('mailers.admin.no_show_pending_charge.header')
      mail(:to => MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS , :subject => I18n.t('mailers.admin.no_show_pending_charge.subject'))
    end
  end

  def send_booking_cancellation_notice(booking)
    @booking = booking
    I18n.with_locale(@booking.hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t('mailers.admin.booking_cancellation.header')
      mail(:to => MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS , :subject => I18n.t('mailers.admin.booking_cancellation.subject'))
    end
  end

  def error_reserving_on_omnibees(booking, cc)
    @booking = booking
    @credit_card = cc
    recipients = ['contasapagar@hotelquando.com' , 'contato@hotelquando.com', 'giovanni@hotelquando.com', 'beatriz@hotelquando.com' ]
    I18n.with_locale(@booking.hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t('mailers.admin.error_reserving_on_omnibees.header')
      recipients.each do |email|
        mail(:to => email , :subject => I18n.t('mailers.admin.error_reserving_on_omnibees.subject'))
      end
    end
  end

  def error_cancelling_on_omnibees(booking)
    @booking = booking
    recipients = ['contasapagar@hotelquando.com' , 'contato@hotelquando.com', 'giovanni@hotelquando.com', 'beatriz@hotelquando.com' ]
    I18n.with_locale(@booking.hotel.locale) do
      headers[:"X-MC-Tags"] = I18n.t('mailers.admin.error_cancelling_on_omnibees.header')
      recipients.each do |email|
        mail(:to => email, :subject => I18n.t('mailers.admin.error_cancelling_on_omnibees.subject'))
      end
    end
  end

end
