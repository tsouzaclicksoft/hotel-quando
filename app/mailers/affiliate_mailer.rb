class AffiliateMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_login_and_password(affiliate)
    @affiliate = affiliate
    @affiliate_mark = false
    headers[:"X-MC-Tags"] = "Primeiro cadastro para agência"
    mail(:to => @affiliate.email, :subject => 'HotelQuando.com - Senha de acesso')
  end

  def send_booking_creation_notice(booking)
    @user = booking.user
    @affiliate = booking.affiliate
    @booking = booking
    headers[:"X-MC-Tags"] = "Reserva criada"
    mail(:to => @affiliate.email, :subject => "HotelQuando.com - Nova reserva para #{@user.name}")
  end

  def send_booking_expiration_notice(booking)
    @user = booking.user
    @booking = booking
    @affiliate = booking.affiliate
    headers[:"X-MC-Tags"] = "Reserva expirada"
    mail(:to => @affiliate.email , :subject => "HotelQuando.com - A reserva para #{@user.name} foi expirada")
  end


  def send_payment_failure_notice(payment)
    @payment = payment
    @user = payment.user
    @booking = payment.booking
    @affiliate = booking.affiliate
    headers[:"X-MC-Tags"] = "Reserva não foi confirmada com sucesso"
    mail(:to => @affiliate.email , :subject => "HotelQuando.com - A reserva para #{@user.name} não foi confirmada")
  end

  def send_booking_confirmation_notice(booking)
    @user = booking.user
    @booking = booking
    @affiliate = booking.affiliate
    headers[:"X-MC-Tags"] = "Reserva confirmada"
    mail(:to => @affiliate.email , :subject => "HotelQuando.com - A reserva para #{@user.name} confirmada com sucesso - ##{booking.id}")
  end

end
