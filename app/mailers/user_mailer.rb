# encoding: utf-8
class UserMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM
  helper Shared::CurrencyCurrentHelper
  helper Public::HotelHelper

  def send_login_and_password(user)
    @send_log_pass = true
    @user = user
    I18n.with_locale(@user.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.user_mailer.send_login_and_password.header")
      mail(:to => @user.email, :subject => I18n.t("mailers.user_mailer.send_login_and_password.subject"))
    end
  end

  def agency_domain
    if @agency.sign_email.present?
      @agency.sign_email.split("@").last
    else
      @agency.user.email.split("@").last
    end
  end

  def send_registration_notice_to_employee(user)
    @user = user
    if user.locale == 'en'
      headers[:"X-MC-Tags"] = "Your password"
      mail(:to => user.email , :subject => 'HotelQuando.com - Your access credentials')
    else
      headers[:"X-MC-Tags"] = "Sua senha"
      mail(:to => user.email , :subject => 'HotelQuando.com - Seus dados para acesso')
    end
  end

  def send_booking_expiration_notice(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Booking expired"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Booking Expired')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Booking Expired')
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Reserva expirada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Reserva Expirada')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Reserva Expirada')
      end
    else
      headers[:"X-MC-Tags"] = "Reserva expirada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Reserva Expirada')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Reserva Expirada')
      end
    end
  end

  def send_no_show_refund_error_notice(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Refund error"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Refund error')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Refund error')
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Error en el reembolso"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Error en el reembolso')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Error en el reembolso')
      end      
    else
      headers[:"X-MC-Tags"] = "Erro no reembolso"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Erro no reembolso')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Erro no reembolso')
      end
    end
  end


  def send_booking_cancellation_notice(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @agency = @traveler.try(:cost_center).try(:company_detail).try(:agency_detail)
    @white_mark = @booking.created_by_agency?
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Booking canceled"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Booking Canceled')
      elsif @white_mark
        mail(:from => "#{@agency.name} <noanswer@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Booking Canceled')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Booking Canceled')
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Reserva cancelada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Reserva Cancelada')
      elsif @white_mark
        mail(:from => "#{@agency.name} <naoresponda@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Reserva Cancelada')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Reserva Cancelada')
      end
    else
      headers[:"X-MC-Tags"] = "Reserva cancelada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Reserva Cancelada')
      elsif @white_mark
        mail(:from => "#{@agency.name} <naoresponda@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Reserva Cancelada')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Reserva Cancelada')
      end
    end
  end

  # def send_booking_creation_notice(booking)
  #   @user = booking.user
  #   if booking.user.locale == 'en'
  #     headers[:"X-MC-Tags"] = "Booking created"
  #     mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - New Booking')
  #   else
  #     headers[:"X-MC-Tags"] = "Reserva criada"
  #     mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Nova Reserva')
  #   end
  # end

  def send_delayed_checkout_notice(booking)
    @user =  booking.traveler_id.present? ? booking.traveler : booking.user
    @booking = booking
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Delayed checkout notice"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Your booking was not confirmed')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Delayed checkout notice')
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Aviso de retraso en el checkout"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Aviso de retraso en el checkout')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Aviso de retraso en el checkout')
      end      
    else
      headers[:"X-MC-Tags"] = "Aviso de atraso no checkout"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Aviso de atraso no checkout')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Aviso de atraso no checkout')
      end
    end
  end

  def send_payment_failure_notice(payment)
    @user =  payment.user
    @traveler =  payment.traveler if payment.traveler_id.present?
    @payment = payment
    @booking = payment.booking
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end
    if @payment.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Booking not successfully confirmed"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Your booking was not confirmed')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Your booking was not confirmed')
      end
    elsif @booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Su reserva no ha sido confirmada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Su reserva no ha sido confirmada')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Su reserva no ha sido confirmada')
      end      
    else
      headers[:"X-MC-Tags"] = "Reserva não foi confirmada com sucesso"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Sua reserva não foi confirmada')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Sua reserva não foi confirmada')
      end
    end
  end

  def send_payu_payment_failure_notice(payment)
    @user =  payment.user
    @traveler =  payment.traveler if payment.traveler_id.present?
    @payment = payment
    @booking = payment.booking
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end
    if @payment.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Your payment was declined"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Your payment was declined')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Your booking was not confirmed')
      end
    elsif @booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Su pago fue rechazado"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Su pago fue rechazado')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Su reserva no ha sido confirmada')
      end      
    else
      headers[:"X-MC-Tags"] = "Seu pagamento foi recusado"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Seu pagamento foi recusado')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Seu pagamento foi recusado')
      end
    end
  end

  def send_booking_confirmation_notice(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @agency = @traveler.try(:cost_center).try(:company_detail).try(:agency_detail)
    @white_mark = @booking.created_by_agency?
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end
    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Booking confirmed"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Booking confirmed successfully - ##{booking.id}")
      elsif @white_mark
        mail(:from => "#{@agency.name} <noanswer@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Booking confirmed successfully - ##{booking.id}")
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => "HotelQuando.com - Booking confirmed successfully - ##{booking.id}")
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Reserva confirmada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Reserva confirmada con éxito - ##{booking.id}")
      elsif @white_mark
        mail(:from => "#{@agency.name} <naoresponda@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Reserva confirmada con éxito - ##{booking.id}")
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => "HotelQuando.com - Reserva confirmada con éxito - ##{booking.id}")
      end      
    else
      headers[:"X-MC-Tags"] = "Reserva confirmada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Reserva confirmada com sucesso - ##{booking.id}")
      elsif @white_mark
        mail(:from => "#{@agency.name} <naoresponda@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Reserva confirmada com sucesso - ##{booking.id}")
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => "HotelQuando.com - Reserva confirmada com sucesso - ##{booking.id}")
      end
    end
  end

  def no_show_with_charge_successfull_notice(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "No-Show successfully paid"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'No-Show successfully paid')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - No-Show successfully paid')
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "No-Show pagado con éxito"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'No-Show pagado con éxito')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - No-Show pagado con éxito')
      end      
    else
      headers[:"X-MC-Tags"] = "No-Show pago com sucesso"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'No-Show pago com sucesso')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - No-Show pago com sucesso')
      end
    end
  end

  def no_show_for_already_captured_cc_notice(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "No-Show successfully paid"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'No-Show pago com sucesso')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - No-Show successfully paid')
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "No-Show pagado con éxito"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'No-Show pagado con éxito')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - No-Show pagado con éxito')
      end      
    else
      headers[:"X-MC-Tags"] = "No-Show pago com sucesso"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'No-Show pago com sucesso')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - No-Show pago com sucesso')
      end
    end
  end

  def no_show_pending_charge_notice(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @affiliate_mark = @booking.created_by_affiliate?
    @booking = booking

    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "No-show will be charged"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Your booking's no-show will be charged")
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => "HotelQuando.com - Your booking's no-show will be charged")
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "El hotel acusó no-show"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'El hotel acusó no-show')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - El hotel acusó no-show')
      end      
    else
      headers[:"X-MC-Tags"] = "Hotel acusou no-show"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Hotel acusou no-show')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Hotel acusou no-show')
      end
    end
  end

  def no_show_with_charge_error_notice(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Error charging no-show"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Error charging your booking's no-show")
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => "HotelQuando.com - Error charging your booking's no-show")
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Error al cobrar el no-show"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Error al cobrar el no-show')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Error al cobrar el no-show')
      end      
    else
      headers[:"X-MC-Tags"] = "Erro ao cobrar no-show"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => 'Erro ao cobrar no-show')
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Erro ao cobrar no-show')
      end
    end
  end

  def booking_canceled_send_to_epayco(payment)
    @epayco_ref = payment.epayco_ref
    render :layout => 'simple_email'
    response_email = "diegom@hotelquando.com"
    email_from = (Rails.env.development? || Rails.env.staging?) ? "#{payment.booking.user.email}" : "#{response_email}"
    mail(:from => email_from,:to =>"soporte@epayco.co" , :subject => "Reversion de transacción numero #{@epayco_ref}")
  end

  def booking_refund_send_to_payu(payment)
    render :layout => 'simple_email'
    if payment.payu_order_id != nil
      @payu_order_id = payment.payu_order_id
      mail(:to =>"contactenos@hotelquando.com" , :subject => "Reembolso de PayU order_id #{@payu_order_id}")
    else
      @payu_transaction_id = payment.payu_transaction_id
      mail(:to =>"contactenos@hotelquando.com" , :subject => "Reembolso de PayU transaction_id #{@payu_transaction_id}")
    end
  end

  def booking_canceled_send_to_payu(payment)
    @booking_id_payu = payment.booking
    mail(:to =>"contactenos@hotelquando.com" , :subject => "URGENTE - Cancelación de reserva #{@booking_id_payu}")
  end

  def send_booking_request_confirmation(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @agency = @traveler.try(:cost_center).try(:company_detail).try(:agency_detail)
    @white_mark = @booking.created_by_agency?
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end
    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Booking Request confirmed"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <noanswer@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Booking Request confirmed successfully - ##{booking.id}")
      elsif @white_mark
        mail(:from => "#{@agency.name} <noanswer@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Booking Request confirmed successfully - ##{booking.id}")
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => "HotelQuando.com - Booking Request confirmed successfully - ##{booking.id}")
      end
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Solicitud de reserva confirmada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Solicitud de reserva confirmada con éxito - ##{booking.id}")
      elsif @white_mark
        mail(:from => "#{@agency.name} <naoresponda@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Solicitud de reserva confirmada con éxito - ##{booking.id}")
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => "HotelQuando.com - Solicitud de reserva confirmada con éxito - ##{booking.id}")
      end      
    else
      headers[:"X-MC-Tags"] = "Solicitação de reserva confirmada"
      if @affiliate_mark
        mail(:from => "#{@affiliate.name} <naoresponda@#{@affiliate_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Solicitação de reserva confirmada com sucesso - ##{booking.id}")
      elsif @white_mark
        mail(:from => "#{@agency.name} <naoresponda@#{agency_domain}>", :to => emails_to_send_booking_notices(@booking) , :subject => "Solicitação de reserva confirmada com sucesso - ##{booking.id}")
      else
        mail(:to => emails_to_send_booking_notices(@booking) , :subject => "HotelQuando.com - Solicitação de reserva confirmada com sucesso - ##{booking.id}")
      end
    end
  end

  def booking_request_denied(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @agency = @traveler.try(:cost_center).try(:company_detail).try(:agency_detail)
    @white_mark = @booking.created_by_agency?
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Booking request denied"
      mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'Booking request denied')
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Solicitud de reserva rechazada"
      mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Solicitud de reserva rechazada')      
    else
      headers[:"X-MC-Tags"] = "Solicitação de Reserva recusada"
      mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Solicitação de Reserva recusada')
    end
  end

  def booking_request_accepted(booking)
    @user = booking.user
    @traveler = booking.traveler if booking.traveler_id.present?
    @booking = booking
    @agency = @traveler.try(:cost_center).try(:company_detail).try(:agency_detail)
    @white_mark = @booking.created_by_agency?
    @affiliate_mark = @booking.created_by_affiliate?
    if @affiliate_mark
      @affiliate = @booking.affiliate
      @affiliate_domain = @affiliate.email.split("@").last
    end

    if booking.user.locale == 'en'
      headers[:"X-MC-Tags"] = "Booking request accepted"
      mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'Booking request accepted')
    elsif booking.user.locale == 'es'
      headers[:"X-MC-Tags"] = "Solicitud de reserva aceptada"
      mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Solicitud de reserva aceptada')
    else
      headers[:"X-MC-Tags"] = "Solicitação de Reserva aceita"
      mail(:to => emails_to_send_booking_notices(@booking) , :subject => 'HotelQuando.com - Solicitação de Reserva aceita')
    end
  end

  private
    def emails_to_send_booking_notices(booking)
      if booking.user.email == "affiliate_ws_api_system@hotelquando.com"
        return "wsaffiliate20171306@hotelquando.com"
      else
        user = booking.traveler_id.present? ? booking.traveler : booking.user
        if is_a_valid_email? booking.guest_email
          return [ booking.guest_email, user.email ].uniq.compact
        else
          return user.email
        end
      end
    end

    def is_a_valid_email?(email)
      mail_pattern = /.+@.+\..+/
      if email.blank?
        false
      else
        mail_pattern === email # testing the supplied email against the simple email pattern
      end
    end
end
