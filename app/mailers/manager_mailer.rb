class ManagerMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_login_and_password(manager)
    @manager = manager
    headers[:"X-MC-Tags"] = "Primeiro cadastro para manager"
    mail(:to => @manager.email, :subject => 'HotelQuando.com - Senha de acesso')
  end

end
