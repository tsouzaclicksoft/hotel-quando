class TravelRequestMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_request_air_to_agency(travel, hq_consultant_email)
  	@travel = travel
  	@user = @travel.user
  	@company_detail = @user.company_detail
    headers[:"X-MC-Tags"] = "Nova Solicitação de Aéreo"
    mail(:to => hq_consultant_email, :subject => 'Nova SV: #{@travel.id}')
  end

  def send_email_to_traveler(travel)
  	@travel = travel
  	@user = @travel.user
    headers[:"X-MC-Tags"] = "Solicitação de passagens"
    mail(:to => @user.email, :subject => 'Seu orçamento está pronto!')
  end

  def send_email_confirmed_and_fatured(travel)
    @travel = travel
    headers[:"X-MC-Tags"] = "Orçamento aprovado"
    mail(:to => @travel.hq_consultant.email, :subject => 'Orçamento foi aprovado')
  end

end
