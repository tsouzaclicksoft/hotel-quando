class EmktMailer < ActionMailer::Base
  layout 'email_emkt'
  default from: HotelQuando::Constants::EMAIL_FROM

  def emkt(user, emkt)
    @emkt = emkt

    headers[:"X-MC-Tags"] = @emkt.name_pt_br
    mail(:to => user.email , :subject => @emkt.name_pt_br)
  end

  def register_without_booking(user)
    if user.is_a?(Newsletter)
      country = Country.where("countries.name_pt_br = '#{user.country}' OR  countries.name_en= '#{user.country}'  OR countries.name_es = '#{user.country}'").first
    else
      country = Country.where("countries.iso_initials = '#{user.country}' OR countries.iso_initials= '#{user.country}' OR countries.iso_initials = '#{user.country}'").first
    end
    unless country.nil?
      country = country.iso_initials
      default_country = Country.where(iso_initials: 'US').first.iso_initials
      @emkt = Emkt.where(country: country, emkt_type: 2).order(:order_send).first
      @emkt ||= Emkt.where(country: default_country, emkt_type: 2).order(:order_send).first
      user.update(emkt_flux: @emkt.order_send, emkt_day_of_week: Date.current.wday, emkt_flux_type: 2)
      unless @emkt.nil?
        headers[:"X-MC-Tags"] = @emkt.name_pt_br
        mail(:to => user.email , :subject => @emkt.name_pt_br)
      end
    end
  end
end
