class CompanyAttendantMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_login_and_password(attendant)
  	@company_attendant = attendant
    headers[:"X-MC-Tags"] = "Primeiro cadastro para agência"
    mail(:to => @company_attendant.email, :subject => 'HotelQuando.com - Senha de acesso')
  end

end
