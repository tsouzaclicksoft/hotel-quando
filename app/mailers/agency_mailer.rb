class AgencyMailer < ActionMailer::Base
  layout 'email'
  default from: HotelQuando::Constants::EMAIL_FROM

  def send_login_and_password(agency)
    @agency = agency
    @user = @agency.user
    I18n.with_locale(@user.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.agency_mailer.send_login_and_password.header")
      mail(:to => @agency.email, :subject => I18n.t("mailers.agency_mailer.send_login_and_password.subject"))
    end
  end

  def send_booking_creation_notice(booking)
    @client = booking.traveler.cost_center.company_detail
    @agency = @client.agency_detail
    @booking = booking
    @user = @agency.user
    I18n.with_locale(@user.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.agency_mailer.send_booking_creation_notice.header")
      mail(:to => @agency.email_for_notice, :subject => I18n.t("mailers.agency_mailer.send_booking_creation_notice.subject", client: @client.name))
    end
  end

  def send_booking_expiration_notice(booking)
    @client = booking.traveler.cost_center.company_detail
    @agency = @client.agency_detail
    @booking = booking
    @user = @agency.user
    I18n.with_locale(@user.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.agency_mailer.send_booking_expiration_notice.header")
      mail(:to => @agency.email_for_notice , :subject => I18n.t("mailers.agency_mailer.send_booking_expiration_notice.subject", client: @client.name))
    end
  end


  def send_payment_failure_notice(payment)
    @payment = payment
    @client = payment.traveler.cost_center.company_detail
    @agency = @client.agency_detail
    @booking = payment.booking
    @user = @agency.user
    I18n.with_locale(@user.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.agency_mailer.send_payment_failure_notice.header")
      mail(:to => @agency.email_for_notice , :subject => I18n.t("mailers.agency_mailer.send_payment_failure_notice.subject", client: @client.name))
    end
  end

  def send_booking_confirmation_notice(booking)
    @client = booking.traveler.cost_center.company_detail
    @agency = @client.agency_detail
    @booking = booking
    @user = @agency.user
    I18n.with_locale(@user.locale) do
      headers[:"X-MC-Tags"] = I18n.t("mailers.agency_mailer.send_booking_confirmation_notice.header")
      mail(:to => @agency.email_for_notice , :subject => I18n.t("mailers.agency_mailer.send_booking_confirmation_notice.subject", client: @client.name, booking_id: @booking.id))
    end
  end

end
