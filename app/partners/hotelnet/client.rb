class Hotelnet::Client
  class MissingParameterError < StandardError; end;

  def get_offers(params)
    params = normalize_params(params)
    check_params!([:number_of_guests, :check_in_datetime, :pack_in_hours, :hotel_codes], params)
    @last_request_xml = Hotelnet::Client::RequestXMLGenerator::GetOffers.new(params).generate
    call_action(:get_hotel_avail)
  end

  def make_booking(params)
    params = normalize_params(params)
    check_params!([:booking], params)
    @last_request_xml = Hotelnet::Client::RequestXMLGenerator::MakeBooking.new(params).generate
    call_action(:send_hotel_res)
  end

  def cancel_booking(params)
    params = normalize_params(params)
    check_params!([:booking], params)
    @last_request_xml = Hotelnet::Client::RequestXMLGenerator::CancelBooking.new(params).generate
    call_action(:send_hotel_res_cancel)
  end

  def last_request_xml
    @last_request_xml
  end

  private

  def normalize_params(params)
    HashWithIndifferentAccess.new(params)
  end

  def check_params!(required_keys, parameters)
    unless required_keys.all? { |key| parameters.has_key?(key) }
      error_message = "You must provide all this parameteres: #{required_keys.join(',')}"
      raise MissingParameterError.new(error_message)
    end
  end

  def client
    #@client ||= Savon.client(proxy: ENV['OMNIBEES_PROXY_ADDRESS'], wsdl: ENV['OMNIBEES_WSDL_ENDPOINT'], log_level: :debug, log: true, encoding: "UTF-8", open_timeout: 5, read_timeout: 5)
    @client ||= Savon.client(wsdl: ENV['OMNIBEES_WSDL_ENDPOINT'], log_level: :debug, log: true, encoding: "UTF-8", open_timeout: 5, read_timeout: 5)
    @client
  end

  def call_action(action, xml = last_request_xml)
    response = client.call(action, xml: xml)
    xml = response.to_xml
    xml.force_encoding('UTF-8')
    xml.gsub!('&#xD;', '')
    xml
  end

end
