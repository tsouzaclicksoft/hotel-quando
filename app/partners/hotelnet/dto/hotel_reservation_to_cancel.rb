class Hotelnet::DTO::HotelReservationToCancel

  CancelledBookingResult = Struct.new(:is_cancelled?)

  def self.parse(booking_cancelation_response_xml)
    new(booking_cancelation_response_xml).result_object
  end

  def initialize(booking_cancelation_response_xml)
    @booking_cancelation_response = Nokogiri::XML(booking_cancelation_response_xml)
    @booking_cancelation_response.remove_namespaces!
  end

  def result_object
    result = CancelledBookingResult.new
    result[:is_cancelled?] = is_cancelled?
    result
  end

  private

  def is_cancelled?
    @booking_cancelation_response.search('Status').children.try(:text).to_s.squish == 'Cancelled'
  end

end
