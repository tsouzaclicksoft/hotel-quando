class Hotelnet::DTO::HotelReservationToBookingResult

  BookingResult = Struct.new(:confirmed?, :reservation_id, :response_xml)

  def self.parse(hotel_reservation_response_xml)
    new(hotel_reservation_response_xml).result_object
  end

  def initialize(hotel_reservation_response_xml)
    @reservation_response = Nokogiri::XML(hotel_reservation_response_xml)
    @reservation_response.remove_namespaces!
    @hotel_reservation_response_xml = hotel_reservation_response_xml
  end

  def result_object
    result = BookingResult.new
    result[:confirmed?] = !reservation_id.blank?
    result[:response_xml] = @hotel_reservation_response_xml
    result[:reservation_id] = reservation_id
    result
  end

  private

  def reservation_id
    @reservation_response.search('//UniqueID/ID').first.try(:text).to_s.squish
  end

end
