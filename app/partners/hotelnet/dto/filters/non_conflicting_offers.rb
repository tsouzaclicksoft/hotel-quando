# encoding: utf-8

class Hotelnet::DTO::Filters::NonConflictingOffers < Hotelnet::DTO::Filters::Base

  def filter
    @omnibees_offers.select do |omnibees_offer|
      total_rooms(omnibees_offer) > conflict_bookings_count(omnibees_offer)
    end
  end

  private
  # calculate the total available rooms for a hotel
  # on a given day
  def total_rooms(omnibees_offer)
    room_type_node = Nokogiri::XML(omnibees_offer.room_type_xml)
    room_type_node.remove_namespaces!
    omnibees_availabilities = room_type_node.xpath('//NumberOfUnits').text.strip.to_i
    bookings_on_day = Booking.by_room_type_id(omnibees_offer.room_type_id)
                      .by_checkin_date(omnibees_offer.checkin_timestamp.to_date)
                      .count
    omnibees_availabilities - bookings_on_day
    #(omnibees_availabilities + bookings_on_day)/HotelQuando::Constants::LENGTH_OF_PACKS.size
  end

  def conflict_bookings_count(omnibees_offer)
    Booking.conflicting_with_offer(omnibees_offer).count
  end
end
