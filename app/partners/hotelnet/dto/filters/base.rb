# encoding: utf-8

class Hotelnet::DTO::Filters::Base

  def self.filter(omnibees_offers)
    new(omnibees_offers).filter
  end

  def initialize(omnibees_offers)
    @omnibees_offers = omnibees_offers
  end

  def filter
    raise 'Abstract method: implement on child class'
  end

end
