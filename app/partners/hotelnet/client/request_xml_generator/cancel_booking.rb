# encoding: utf-8
class Hotelnet::Client::RequestXMLGenerator::CancelBooking < Hotelnet::Client::RequestXMLGenerator::Base
  include ActionView::Helpers::SanitizeHelper
  def generate
    days = booking.pack_in_hours / 24 + 1
    return %Q{
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
      xmlns:tem="http://tempuri.org/"
      xmlns:pull="http://schemas.datacontract.org/2004/07/Pull.BLL.Models"
      xmlns:pull1="http://schemas.datacontract.org/2004/07/Pull.BLL.Models.OTA">
         <soapenv:Header/>
         <soapenv:Body>
            <tem:SendHotelResCancel>
              <tem:login>
                <pull:Password>#{ENV['HOTEISNET_PASSWORD']}</pull:Password>
                <pull:UserName>#{ENV['HOTEISNET_USERNAME']}</pull:UserName>
              </tem:login>
              <tem:ota_CancelRQ xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <pull1:CancelType xsi:nil="true" />
                <pull1:EchoToken>#{SecureRandom.uuid}</pull1:EchoToken>
                <pull1:PrimaryLangID>#{target_language}</pull1:PrimaryLangID>
                <pull1:Target>#{target_webservice_value}</pull1:Target>
                <pull1:TimeStamp>#{Time.now.strftime("%Y-%m-%d")}T00:00:00Z</pull1:TimeStamp>
                <pull1:UniqueID>
                  <pull1:UniqueID>
                    <pull1:ID>#{booking.omnibees_code}</pull1:ID>
                    <pull1:Reason>Cancelation reason</pull1:Reason>
                    <pull1:Type>Reservation</pull1:Type>
                  </pull1:UniqueID>
                </pull1:UniqueID>
                <pull1:Verification>
                  <pull1:Email>#{booking.guest_email}</pull1:Email>
                  <pull1:HotelRef>
                    <pull1:ChainCode xsi:nil="true" />
                    <pull1:HotelCode>#{hotel.omnibees_code}</pull1:HotelCode>
                  </pull1:HotelRef>
                  <pull1:ReservationTimeSpan>
                    <pull1:End>#{(offer.checkin_timestamp + days.day).strftime('%Y-%m-%d')}T00:00:00</pull1:End>
                    <pull1:Start>#{offer.checkin_timestamp.strftime('%Y-%m-%d')}T00:00:00</pull1:Start>
                  </pull1:ReservationTimeSpan>
                </pull1:Verification>
                <pull1:Version>1.6</pull1:Version>
              </tem:ota_CancelRQ>
            </tem:SendHotelResCancel>
         </soapenv:Body>
      </soapenv:Envelope>
    }
  end

  private
=begin
  def reservation_time_node
    if booking.pack_in_hours == 9 && booking.date_interval.first.strftime("%H").between?('00', '03')
      return %Q{
        <pull1:End>#{offer.checkin_timestamp.strftime('%Y-%m-%d')}T00:00:00</pull1:End>
        <pull1:Start>#{(offer.checkin_timestamp - 1.day).strftime('%Y-%m-%d')}T00:00:00</pull1:Start>
      }
    else
      return %Q{
        <pull1:End>#{(offer.checkin_timestamp + 1.day).strftime('%Y-%m-%d')}T00:00:00</pull1:End>
        <pull1:Start>#{offer.checkin_timestamp.strftime('%Y-%m-%d')}T00:00:00</pull1:Start>
      }
    end
  end
=end
  def offer
    @offer ||= booking.offer
  end

  def booking
    @booking ||= params[:booking]
  end

  def hotel
    @hotel ||= booking.hotel
  end
end
