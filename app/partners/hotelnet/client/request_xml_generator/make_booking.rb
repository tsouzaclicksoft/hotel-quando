# encoding: utf-8
class Hotelnet::Client::RequestXMLGenerator::MakeBooking < Hotelnet::Client::RequestXMLGenerator::Base
  include ActionView::Helpers::SanitizeHelper
  def generate
    return %Q{
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
      xmlns:tem="http://tempuri.org/"
      xmlns:pull="http://schemas.datacontract.org/2004/07/Pull.BLL.Models"
      xmlns:pull1="http://schemas.datacontract.org/2004/07/Pull.BLL.Models.OTA">
         <soapenv:Header/>
         <soapenv:Body>
            <tem:SendHotelRes>
              <tem:login>
                <pull:Password>#{ENV['HOTEISNET_PASSWORD']}</pull:Password>
                <pull:UserName>#{ENV['HOTEISNET_USERNAME']}</pull:UserName>
              </tem:login>
              <tem:ota_HotelResRQ xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <pull1:EchoToken>#{SecureRandom.uuid}</pull1:EchoToken>
                <pull1:HotelReservationsType>
                  <pull1:HotelReservations>
                    <pull1:HotelReservation>
                      <pull1:CreateDateTime>#{DateTime.now.utc.strftime("%Y-%m-%dT%H:%M:%S")}</pull1:CreateDateTime>
                      <pull1:LastModifyDateTime>#{DateTime.now.utc.strftime("%Y-%m-%dT%H:%M:%S")}</pull1:LastModifyDateTime>
                      #{res_global_info_xml}
                      #{res_guests_type_xml}
                      <pull1:ResStatus>
                        <pull1:PMS_ResStatusType xsi:nil="true" />
                        <pull1:TransactionActionType>Book</pull1:TransactionActionType>
                      </pull1:ResStatus>
                      #{rooms_stays_type_xml}
                    </pull1:HotelReservation>
                  </pull1:HotelReservations>
                </pull1:HotelReservationsType>
                <pull1:PrimaryLangID>#{target_language}</pull1:PrimaryLangID>
                <pull1:Target>#{target_webservice_value}</pull1:Target>
                <pull1:TimeStamp>#{Time.now.strftime("%Y-%m-%d")}</pull1:TimeStamp>
                <pull1:Version>1.6</pull1:Version>
              </tem:ota_HotelResRQ>
            </tem:SendHotelRes>
         </soapenv:Body>
      </soapenv:Envelope>
    }
  end

  private

  def res_guests_type_xml
    xml = %Q{
      <pull1:ResGuestsType>
        <pull1:ResGuests>
    }
    is_primary = 'true'
    booking.number_of_people.times do |i|
      xml += %Q{
          <pull1:ResGuest>
            <pull1:Age>0</pull1:Age>
            <pull1:AgeQualifyingCode>Adult</pull1:AgeQualifyingCode>
            <pull1:PrimaryIndicator>#{is_primary}</pull1:PrimaryIndicator>
            <pull1:Profiles>
              <pull1:ProfileInfos>
                <pull1:ProfileInfo>
                  <pull1:Profile>
                    <pull1:Customer>
                      <pull1:Email>#{booking.guest_email}</pull1:Email>
                      <pull1:PersonName>
                        <pull1:GivenName>#{booking.guest_name}</pull1:GivenName>
                      </pull1:PersonName>
                    </pull1:Customer>
                  </pull1:Profile>
                </pull1:ProfileInfo>
              </pull1:ProfileInfos>
            </pull1:Profiles>
            <pull1:ResGuestRPH>#{i + 1}</pull1:ResGuestRPH>
          </pull1:ResGuest>
        }
      is_primary = 'false'

    end
    xml += %Q{
        </pull1:ResGuests>
      </pull1:ResGuestsType>
    }
  end

  def res_global_info_xml
    %Q{
      <pull1:ResGlobalInfo>
        #{res_comments_type_xml}
        <pull1:Guarantee>
          <pull1:GuaranteesAcceptedType>
            <pull1:GuaranteesAccepted>
              <pull1:GuaranteeAccepted>
                <pull1:GuaranteeTypeCode>#{getGuaranteeTypeCode}</pull1:GuaranteeTypeCode>
              </pull1:GuaranteeAccepted>
            </pull1:GuaranteesAccepted>
          </pull1:GuaranteesAcceptedType>
        </pull1:Guarantee>
        #{normalize_xml(res_global_info_total_node.to_s)}
      </pull1:ResGlobalInfo>
    }
  end

  def res_comments_type_xml
    %Q{
        <pull1:CommentsType>
          <pull1:Comments>
            <pull1:Comment>
              <pull1:Description>
                Reserva do HotelQuando
                Valor total da Reserva: R$ #{booking.total_price_without_tax.to_money.to_f.to_s}
                Data e hora de Check-In: #{booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M')}
                Pacote (período de permanência em horas): #{booking.pack_in_hours}hrs
                Data e hora de Check-Out: #{booking.offer.checkout_timestamp.strftime('%d/%m/%Y %H:%M')}
                Tipo do pagamento: #{(booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured] || booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) ? 'Faturado' : 'No destino'}
                Comentário do cliente:
                #{sanitize(booking.note)}
              </pull1:Description>
            </pull1:Comment>
          </pull1:Comments>
        </pull1:CommentsType>
      }
  end

  def rooms_stays_type_xml
    xml_block = %Q{
      <pull1:RoomStaysType>
        <MoreIndicator>false</MoreIndicator>
        <pull1:RoomStays>
          <pull1:RoomStay>

            <pull1:BasicPropertyInfo>
              <pull1:HotelRef>
                <pull1:HotelCode>#{hotel.omnibees_code}</pull1:HotelCode>
              </pull1:HotelRef>
            </pull1:BasicPropertyInfo>

            <pull1:GuestCountsType>
              <pull1:GuestCounts>
                <pull1:GuestCount>
                  <pull1:AgeQualifyCode>Adult</pull1:AgeQualifyCode>
                  <pull1:Count>#{booking.number_of_people}</pull1:Count>
                  <pull1:ResGuestRPH>
                  }
                  booking.number_of_people.times do |i|
                    xml_block += %Q{
                      <ns5:int xmlns:ns5="http://schemas.microsoft.com/2003/10/Serialization/Arrays">#{i+1}</ns5:int>
                    }
                  end
                  xml_block += %Q{
                  </pull1:ResGuestRPH>
                </pull1:GuestCount>
              </pull1:GuestCounts>
            </pull1:GuestCountsType>

            <pull1:RatePlans>
              #{normalize_xml(offer.rate_plan_xml)}
            </pull1:RatePlans>

            <pull1:RoomRates>
              #{normalize_xml(offer.room_rate_xml)}
            </pull1:RoomRates>

            <pull1:RoomTypes>
              #{normalize_xml(offer.room_type_xml)}
            </pull1:RoomTypes>

            #{normalize_xml(room_stay_total_node.to_s)}
          </pull1:RoomStay>
        </pull1:RoomStays>
      </pull1:RoomStaysType>
    }
  end

  private
=begin
  def room_rates_node
    node = room_rate_node 
    if booking.pack_in_hours == 9 && booking.date_interval.first.strftime("%H").between?('00', '03')
      date = booking.date_interval.first - 1.day
      room_rate_node.xpath('//EffectiveDate').each do |rr|
        room_rates_type_rates_rate_effective_date_node = rr
        room_rates_type_rates_rate_effective_date_node.content = date.strftime("%Y-%m-%dT00:00:00")  
      end
      expire_date_node = room_rate_node.search('./ExpireDate').first
      expire_date_node.content = booking.date_interval.first.strftime("%Y-%m-%dT00:00:00")
    end
    node.xpath('//RatesType').xpath('//AmountBeforeTax').each do |bt|
      before_tax_node = bt
      before_tax_node.content = booking.total_price_without_tax.to_money.to_f
    end
    node 
  end
=end
  def room_stay_total_node
    node = room_rate_node.search('./Total').dup

    total_before_tax_node = node.search('./AmountBeforeTax').first
    total_before_tax_node.content = total_before_tax_node.to_s.to_money.to_f.to_s
    total_after_tax_node = node.search('./AmountAfterTax').first
    total_after_tax_node.content = total_after_tax_node.to_s.to_money.to_f.to_s

    type_node = node.search('./ChargeType').first
    type_node.content='PerRoomPerNight'
    node
  end

  def res_global_info_total_node
    node = room_rate_node.search('./Total').dup
    type_node = node.search('./ChargeType').first
    type_node.content='PerStay'

    total_before_tax_node = node.search('./AmountBeforeTax').first
    total_before_tax_node.content = total_before_tax_node.to_s.to_money.to_f.to_s
    total_after_tax_node = node.search('./AmountAfterTax').first
    total_after_tax_node.content = total_after_tax_node.to_s.to_money.to_f.to_s
    node
  end

  def normalize_xml(xml)
    node = Nokogiri::XML(xml)
    node.xpath("//*[@nil]").remove
    node.root.to_s.gsub("<", "<pull1:").gsub("pull1:/", "/pull1:")
  end

  def room_rate_node
     @room_rate_node ||= Nokogiri::XML(offer.room_rate_xml).root
  end

  def offer
    @offer ||= booking.offer
  end

  def booking
    @booking ||= params[:booking]
  end

  def hotel
    @hotel ||= booking.hotel
  end

  def getGuaranteeTypeCode
    rate_plan_xml = Nokogiri::XML(offer.rate_plan_xml)
    type_code = nil
    rate_plan_xml.xpath("//GuaranteeTypeCode").each do |type|
      if type.text == 'DirectBill'
        return 'DirectBill'
      elsif type.text == 'Voucher' || type.text == 'PrePay'
        type_code = type.text
      end
    end
    return type_code || rate_plan_xml.xpath("//GuaranteeTypeCode").first.text.strip
  end
end
