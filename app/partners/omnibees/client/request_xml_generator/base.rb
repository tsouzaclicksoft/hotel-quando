class Omnibees::Client::RequestXMLGenerator::Base
  attr_accessor :params

  def initialize(_params)
    @params = _params
  end

  def normalize_params
    params[:check_in_datetime] = params[:check_in_datetime].utc
  end

  def complete_params
    params[:check_out_datetime] = params[:check_in_datetime] + 1.day
  end

  def target_webservice_value
    Rails.env.production? ? 'Production' : 'Test'
  end

  def target_language
    if @params[:language] == :en
      return 'en'
    elsif @params[:language] == :es
      return 'es'
    else
      return 'pt'
    end
  end
end
