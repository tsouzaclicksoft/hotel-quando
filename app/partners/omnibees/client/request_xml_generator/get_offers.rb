# encoding: utf-8

class Omnibees::Client::RequestXMLGenerator::GetOffers < Omnibees::Client::RequestXMLGenerator::Base

  def initialize(_params)
    super
    normalize_params
    complete_params
  end

  def normalize_params
    params[:check_in_datetime] = params[:check_in_datetime].strftime("%Y-%m-%d %H:%M:%S +0000").to_datetime
  end

  def complete_params
    params[:check_out_datetime] ||= params[:check_in_datetime] + 1.day
  end

  def generate
    return %Q{
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
      xmlns:tem="http://tempuri.org/"
      xmlns:pull="http://schemas.datacontract.org/2004/07/Pull.BLL.Models"
      xmlns:pull1="http://schemas.datacontract.org/2004/07/Pull.BLL.Models.OTA">
         <soapenv:Header/>
         <soapenv:Body>
            <tem:GetHotelAvail>
                <tem:login>
                  <pull:Password>#{ENV['OMNIBEES_PASSWORD']}</pull:Password>
                  <pull:UserName>#{ENV['OMNIBEES_USERNAME']}</pull:UserName>
                </tem:login>
                <tem:ota_HotelAvailRQ xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <pull1:BestOnly>false</pull1:BestOnly>
                  <pull1:EchoToken>#{SecureRandom.uuid}</pull1:EchoToken>
                  <pull1:HotelSearchCriteria>
                    <pull1:Criterion>
                      <pull1:HotelRefs>
                        #{params[:hotel_codes].map { |hotel_code| %Q{
                          <pull1:HotelRef>
                            <pull1:ChainCode xsi:nil="true" />
                            <pull1:HotelCode>#{hotel_code}</pull1:HotelCode>
                          </pull1:HotelRef>
                        }}.join}
                      </pull1:HotelRefs>
                      <pull1:Radius>
                        <pull1:DistanceMax>0</pull1:DistanceMax>
                        <pull1:UnitOfMesureCode>Kilometers</pull1:UnitOfMesureCode>
                      </pull1:Radius>
                      <pull1:RoomStayCandidatesType>
                        <pull1:RoomStayCandidates>
                          <pull1:RoomStayCandidate>
                            <pull1:GuestCountsType>
                              <pull1:GuestCounts>
                                <pull1:GuestCount>
                                  <pull1:Age xsi:nil="true" />
                                  <pull1:AgeQualifyCode>Adult</pull1:AgeQualifyCode>
                                  <pull1:Count>#{params[:number_of_guests]}</pull1:Count>
                                </pull1:GuestCount>
                              </pull1:GuestCounts>
                            </pull1:GuestCountsType>
                            <pull1:Quantity>1</pull1:Quantity>
                            <pull1:RPH>1</pull1:RPH>
                            <pull1:RoomID xsi:nil="true" />
                          </pull1:RoomStayCandidate>
                        </pull1:RoomStayCandidates>
                      </pull1:RoomStayCandidatesType>
                      <pull1:StayDateRange>
                        <pull1:End>#{(params[:check_out_datetime]).strftime("%Y-%m-%d")}</pull1:End>
                        <pull1:Start>#{params[:check_in_datetime].strftime("%Y-%m-%d")}</pull1:Start>
                      </pull1:StayDateRange>
                    </pull1:Criterion>
                  </pull1:HotelSearchCriteria>
                  <pull1:MaxResponses>50</pull1:MaxResponses>
                  <pull1:PageNumber>1</pull1:PageNumber>
                  <pull1:PrimaryLangID>#{target_language}</pull1:PrimaryLangID>
                  <pull1:Target>#{target_webservice_value}</pull1:Target>
                  <pull1:TimeStamp>#{Time.now.strftime("%Y-%m-%d")}</pull1:TimeStamp>
                  <pull1:Version>1.6</pull1:Version>
               </tem:ota_HotelAvailRQ>
            </tem:GetHotelAvail>
         </soapenv:Body>
      </soapenv:Envelope>
    }
  end

  def target_value
    Rails.env.production? ? 'Production' : 'Test'
  end
end
