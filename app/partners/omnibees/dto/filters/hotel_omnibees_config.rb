# encoding: utf-8

class Omnibees::DTO::Filters::HotelOmnibeesConfig < Omnibees::DTO::Filters::Base

  def filter
    @omnibees_offers.select do |omnibees_offer|
      config = omnibees_offer.hotel.omnibees_config
      pack = omnibees_offer.pack_in_hours.to_i
      hour = omnibees_offer.checkin_timestamp.strftime("%H").to_i
      config.accepts_pack?(pack) and config.accepted_hours_for(pack).include?(hour)
    end
  end

end
