# encoding: utf-8

class Omnibees::DTO::HotelAvailabilitiesToOffers
  def self.parse(xml)
    new(xml)
  end

  def initialize(xml)
    @xml = Nokogiri::XML(xml)
    @xml.remove_namespaces!
    @room_type_data_by_omnb_code = {}
  end

  def to_offers(params)
    @params = params
    # for each roomstay node we can get multiple offers
    # according to the number of room types nodes and free rooms available for each room type
    begin
      persist_room_types!
      offers = generate_all_possible_offers
      filter_offers(offers)
    rescue Exception => ex
      Airbrake.notify(ex)
      []
    end
  end

  private

  def persist_room_types!
    @xml.xpath("//RoomStay").each do |hotel_stay|
      hotel_omnibees_code = hotel_stay.search("./BasicPropertyInfo/HotelRef/HotelCode").text.strip
      hotel = Hotel.find_by(omnibees_code: hotel_omnibees_code)
      hotel_stay.search("./RoomTypes/RoomType").each do |room_type_node|
        parse_room_type_node!(room_type_node, hotel)
      end
    end
  end

  def parse_room_type_node!(room_type_node, hotel)
    omnibees_code = room_type_node.search("./RoomID").text.strip
    language = if (room_type_node.search("./RoomDescription/Language").text.strip == 'en')
      'es'
    elsif (room_type_node.search("./RoomDescription/Language").text.strip == 'es')
      'es'
    else
      'pt_br'
    end
    room_type = RoomType.where(omnibees_code: omnibees_code).first_or_initialize
    if room_type.new_record?
      room_type.hotel = hotel
      room_type.send("name_#{language}=", room_type_node.search("./RoomName").text.strip) if room_type.send("name_#{language}").blank?
      room_type.send("description_#{language}=", room_type_node.search("./RoomDescription/Description").text.strip) if room_type.send("description_#{language}").blank?
      if language == 'en' and room_type.description_pt_br.blank?
        room_type.name_pt_br = room_type.name_en
        room_type.description_pt_br = room_type.description_en
      end
      if language == 'es' and room_type.description_pt_br.blank?
        room_type.name_pt_br = room_type.name_es
        room_type.description_pt_br = room_type.description_es
      end
      occupancy = room_type_node.search("./Occupancies/Occupancy")
      room_type.initial_capacity = occupancy.search("./MinOccupancy").sum { |node| node.content.to_i }
      room_type.maximum_capacity = occupancy.search("./MaxOccupancy").sum { |node| node.content.to_i }
      room_type.double_bed_quantity = 1
      room_type.single_bed_quantity = 0
    end
    @room_type_data_by_omnb_code[room_type.omnibees_code] = { model: room_type, xml: room_type_node.to_xml }
    room_type.save!
  end

  def generate_all_possible_offers
    offers = []
    # we select the cheapest room rate for each room type and create a offer for them
    @xml.xpath("//RoomRate").group_by do |room_rate_node|
      room_rate_node.search("RoomID").text.strip
    end.each_pair do |room_type_omnibees_code, room_rates_nodes|
      offer = OmnibeesOffer.new
      room_type = @room_type_data_by_omnb_code[room_type_omnibees_code][:model]
      offer.room_type_id = room_type.id
      offer.hotel_id = room_type.hotel_id
      offer.checkin_timestamp = @params[:check_in_datetime]
      offer.pack_in_hours = @params[:pack_in_hours]
      offer.number_of_guests = @params[:number_of_guests]

      min_room_rate_node = room_rates_nodes.min_by { |node| node.search("./Total/AmountAfterTax").text.strip.to_f }
      # hack if to work with homologation
      if @params[:choosen_room_rate_id].present?
        min_room_rate_node = room_rates_nodes.find { |node| node.xpath("./RatePlanID").text.strip.to_s == @params[:choosen_room_rate_id].to_s }
        if min_room_rate_node.nil?
          min_room_rate_node = room_rates_nodes.min_by { |node| node.search("./Total/AmountAfterTax").text.strip.to_f }
        end
      end

      pack_price = price_for_pack(min_room_rate_node.search("./Total/AmountAfterTax").text.strip.to_f)
      #price_with_iss = (pack_price.to_money*(1.0+iss_for_hotel_id(room_type.hotel_id)/100.00)).to_money.to_f

      #removed aditional iss calculated using hotel iss tax, AmountAfterTax is already added by iss tax
      price_with_iss = ( pack_price.to_money*1.0 ).to_money.to_f 
      offer.price = price_with_iss
      offer.hotel_availabilities_xml = @xml.to_s

      offer.room_type_xml = @room_type_data_by_omnb_code[room_type_omnibees_code][:xml]
      offer.room_rate_xml = min_room_rate_node.to_xml
      selected_rate_plan_id = min_room_rate_node.search("./RatePlanID").text.strip
      offer.rate_plan_xml = @xml.xpath("//RatePlanType").find { |node| node.search("./RatePlanID").text.strip ==  selected_rate_plan_id}.to_xml
      offers << offer
    end
    offers
  end

  def filter_offers(offers)
    selected_offers = offers
    filter_criterias.each do |filter_criteria|
      selected_offers = filter_criteria.filter(selected_offers)
    end
    selected_offers
  end

  def filter_criterias
    [Omnibees::DTO::Filters::HotelOmnibeesConfig, Omnibees::DTO::Filters::NonConflictingOffers]
  end

  def price_for_pack(base_price_for_3h)
    # x ------- NOVO PRECO
    # 0.38 ---- PRECO (0.38 é o de 3hrs)
    percentage_for_pack = {
      3 => 0.38,
      6 => 0.43,
      9 => 0.60,
      12 => 0.65
    }[@params[:pack_in_hours].to_i]
    if percentage_for_pack.nil?
      raise "Non mapped price for hour conversion for Omnibees Offer: #{@params[:pack_in_hours].to_s}"
    end
    new_price = base_price_for_3h*percentage_for_pack/0.38
    new_price.to_money.to_f
  end

  def iss_for_hotel_id(hotel_id)
    @iss_and_hotel_ids ||= Hotel.omnibees.pluck(:iss_in_percentage, :id)
    @iss_and_hotel_ids.find { |iss_hotel_id| iss_hotel_id[1] == hotel_id}.first
  end
end
