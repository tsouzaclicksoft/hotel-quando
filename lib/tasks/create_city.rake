require 'aws-sdk'
def with_spinner
  chars = %w[| / - \\]
  not_done = true
  spinner = Thread.new do
    while not_done do
      print chars.rotate!.first
      sleep 0.1
      print "\b"
    end
  end
  yield.tap do
    not_done = false
    spinner.join
  end
end

# ENV['AWS_REGION'], ENV['AWS_ACCESS_KEY_ID'] and ENV['AWS_SECRET_ACCESS_KEY']

def upload_city_images
  ENV['AWS_REGION'] = 'sa-east-1'
  file_name = ['Aracaju.png', 'Barueri.png', 'Bauru.png', 'Belém.png',
                'Belo Horizonte.png', 'Brasília.png', 'Campinas.png',
                'Campo Grande.png', 'Campos do Jordão.png', 'Cuiabá.png',
                'Curitiba.png', 'Florianópolis.png', 'Fortaleza.png',
                'Foz do Iguaçu.png', 'Goiânia.png', 'Guarulhos.png',
                'João Pessoa.png', 'Joinville.png', 'Jundiaí.png', 'Londrina.png',
                'Macaé.png', 'Maceió.png', 'Manaus.png', 'Natal.png', 'Porto Alegre.png',
                'Recife.png', 'Ribeirão Preto.png', 'Rio de Janeiro.png', 'Salvador.png',
                'Santos.png', 'São Bernardo do Campo.png', 'São José dos Campos.png',
                'São Luís.png', 'São Paulo.png', 'Sorocaba.png', 'Vitória.png']

  #s3 = Aws::S3::Resource.new(region: ENV['AWS_REGION'])
  file_name.each do |file|
    #obj = s3.bucket('hotelquando-development').object(file)
    #print "Uploading file #{file}\n"
    #obj.upload_file("app/assets/images/cities/#{file}")
    create_place(file.gsub('.png', ''), nil)#, obj.public_url)
  end
end

def create_place(name, image_url)
  city = City.where(name: name).first
  hotel_ids = Hotel.where(city_id: city.id).map(&:id)
  hotel_count = hotel_ids.count
  place = Place.new({name_pt_br: name, name_es: name, name_en: name, place_type: 2, active: true,
                     hotel_count: hotel_count, hotel_id: hotel_ids, city: city, country: 'BR'})
  place.save(validate: false)
  #photo = Photo.new({title_pt_br: name, title_en: name, title_es: name, place_id: place.id, direct_image_url: image_url})
  #photo.save
  #place.photos << photo
  #place.save
end

namespace :places do
  task create_cities: :environment do

    with_spinner do
      upload_city_images
    end
    puts "Done!"
  end
end
