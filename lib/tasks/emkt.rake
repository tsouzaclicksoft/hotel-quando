namespace :emkts do
  task with_booking: :environment do
    EmktWithBookingWorker.perform_async
  end

  task without_booking: :environment do
    EmktWithoutBookingWorker.perform_async
  end
end
