namespace :hotels do
  task :update_offers_cache_count => :environment do
    Hotel.all.pluck(:id).each do |hotel_id|
      active_offers_count = Offer.where(hotel_id: hotel_id, status: Offer::ACCEPTED_STATUS[:available]).count
    	active_meeting_offers_count = MeetingRoomOffer.where(hotel_id: hotel_id, status: MeetingRoomOffer::ACCEPTED_STATUS[:available]).count
    	active_event_offers_count = EventRoomOffer.where(hotel_id: hotel_id, status: EventRoomOffer::ACCEPTED_STATUS[:available]).count
      Hotel.where(id: hotel_id).update_all(active_offers_count: active_offers_count, active_meeting_offers_count: active_meeting_offers_count, active_event_offers_count: active_event_offers_count )
    end
  end
end
