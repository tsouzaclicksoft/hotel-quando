namespace :places do
  task create: :environment do
    airports = {"Aeroporto de Campo Grande" => {"ICAO" => "SBCG", "Latitude" => "-20.469528", "Longitude" => "-54.673686", "Telefone" => "(67) 3763.2444", "Endereço" => "Aeroporto de Campo Grande (CGR) - Campo Grande - MS, Brasil", "Município" => "Campo Grande", "Estado" => "Mato Grosso do Sul"},
                "Aeroporto de Congonhas" => {"ICAO" => "SBSP", "Latitude" => "-23.626111", "Longitude" => "-46.656389", "Telefone" => "(11) 5090.9000", "Endereço" => "Aeroporto de Congonhas (CGH) - Avenida Washington Luís, Campo Belo - São Paulo, 04626-001, Brasil", "Município" => "São Paulo", "Estado" => "São Paulo"},
                "Aeroporto de Guarulhos" => {"ICAO" => "SBGR", "Latitude" => "-23.431785", "Longitude" => "-46.469353", "Telefone" => "(11) 6445.2945", "Endereço" => "Aeroporto Internacional São Paulo-cumbica (GRU) - Guarulhos - São Paulo, Brasil", "Município" => "Guarulhos", "Estado" => "São Paulo"},
                "Aeroporto de Navegante" => {"ICAO" => "SBNF", "Latitude" => "-26.878881", "Longitude" => "-48.650728", "Telefone" => "(47) 3342.1132", "Endereço" => "Aeroporto de Navegantes (NVT) - Navegantes - SC, Brasil", "Município" => "Navegantes", "Estado" => "Santa Catarina"},
                "Aeroporto Galeão" => {"ICAO" => "SBGL", "Latitude" => "-22.8088889", "Longitude" => "-43.2436111", "Telefone" => "(21) 3398.5050", "Endereço" => "Aeroporto Internacional Antônio Carlos Jobim (GIG) - Galeão, Rio de Janeiro - RJ, Brasil", "Município" => "Rio de Janeiro", "Estado" => "Rio de Janeiro"},
                "Aeroporto Internacional de Natal" => {"Longitude" => "-35.3761111", "Endereço" => "Greater Natal International Airport - São Gonçalo do Amarante - RN, Brasil", "Município" => "São Gonçalo do Amarante", "Estado" => "Rio Grande do Norte"},
                "Aeroporto Afonso Pena" => {"ICAO" => "SBCT", "Latitude" => "-25.5322", "Longitude" => "-49.176544", "Telefone" => "(41) 3381.1100", "Endereço" => "Aeroporto Afonso Pena (CWB) - Aeroporto, São José dos Pinhais - PR, Brasil", "Município" => "São José dos Pinhais", "Estado" => "Paraná"},
                "Aeroporto de Campinas - Viracopos" => {"ICAO" => "SBKP", "Latitude" => "-23.009892", "Longitude" => "-47.141674", "Telefone" => "(19) 3725.5000", "Endereço" => "Aeroporto de Viracopos (CPQ) - Campinas - São Paulo, Brasil", "Município" => "Campinas", "Estado" => "São Paulo"},
                "Aeroporto Castro Pinto" => {"ICAO" => "SBJP", "Latitude" => "-7.1473403", "Longitude" => "-34.9501457", "Telefone" => "(83) 3232.1200", "Endereço" => "Aeroporto Pres. Castro Pinto (JPA) - Jardim Aeroporto, João Pessoa - PB, Brasil", "Município" => "João Pessoa", "Estado" => "Paraíba"},
                "Aeroporto Internacional Cataratas Foz do Iguaçu" => {"ICAO" => "SBFI", "Latitude" => "-25.59469", "Longitude" => "-54.488494", "Telefone" => "(45) 3521.4200", "Endereço" => "Aeroporto internacional cataratas (IGU) - Iguassu Falls - PR, Brasil", "Município" => "Foz do Iguaçu", "Estado" => "Paraná"},
                "Aeroporto de Confins" => {"ICAO" => "SBCF", "Latitude" => "-19.634129", "Longitude" => "-43.968397", "Telefone" => "(31) 3689.2700", "Endereço" => "Aeroporto Internacional Tancredo Neves (CNF) - Belo Horizonte - MG, Brasil", "Município" => "Confins", "Estado" => "Minas Gerais"},
                "Aeroporto Internacional Eduardo Gomes" => {"ICAO" => "SBEG", "Latitude" => "-3.031327", "Longitude" => "-60.046093", "Telefone" => "(92) 3652.1212", "Endereço" => "Aeroporto Internacional Eduardo Gomes (MAO) - Tarumã, Manaus - AM, Brasil", "Município" => "Manaus", "Estado" => "Amazonas"},
                "Aeroporto Eurico de Aguiar Salles" => {"ICAO" => "SBVT", "Latitude" => "-20.2594559", "Longitude" => "-40.2890769", "Telefone" => "(27) 3235.6300", "Endereço" => "Aeroporto de Vitória (VIX) - Vitória - ES, Brasil", "Município" => "Vitória", "Estado" => "Espírito Santo (estado)"},
                "Aeroporto Internacional dos Guararapes" => {"ICAO" => "SBRF", "Latitude" => "-8.126389", "Longitude" => "-34.922778", "Telefone" => "(81) 3464.4188", "Endereço" => "Aeroporto (REC) - Ibura, Recife - PE, Brasil", "Município" => "Recife", "Estado" => "Pernambuco"},
                "Aeroporto Internacional Hercílio Luz" => {"ICAO" => "SBFL", "Latitude" => "-27.665689", "Longitude" => "-48.551584", "Telefone" => "(48) 3236.0879", "Endereço" => "Aeroporto Internacional Hercílio Luz (FLN) - Av. Diomício Freitas, 3393 - Carianos, Florianópolis - SC, 88047-900, Brasil", "Município" => "Florianópolis", "Estado" => "Santa Catarina"},
                "Aeroporto Internacional Juscelino Kubitschek" => {"ICAO" => "SBBR", "Latitude" => "-15.8700393", "Longitude" => "-47.9217625", "Telefone" => "(61) 3365.1224", "Endereço" => "Aeroporto Internacional Juscelino Kubitscheck (BSB) - Lago Sul, Brasília - DF, 71608-900, Brasil", "Município" => "Brasília", "Estado" => "Distrito Federal (Brasil)"},
                "Aeroporto Internacional Dep. Luís Eduardo Magalhães" => {"ICAO" => "SBSV", "Latitude" => "-12.9151251", "Longitude" => "-38.3333678", "Telefone" => "(71) 3204.1010", "Endereço" => "Aeroporto Dep. Luiz Eduardo Magalhães (Antigo Dois de Julho) (SSA) - Salvador - BA, Brasil", "Município" => "Salvador", "Estado" => "Bahia"},
                "Aeroporto Internacional Marechal Rondon" => {"ICAO" => "SBCY", "Latitude" => "-15.653684", "Longitude" => "-56.116543", "Telefone" => "(65) 3614.2500", "Endereço" => "Aeroporto Mal. Rondon (CGB) - Várzea Grande - MT, Brasil", "Município" => "Várzea Grande", "Estado" => "Mato Grosso"},
                "Aeroporto Internacional Pinto Martins" => {"ICAO" => "SBFZ", "Latitude" => "-3.775833", "Longitude" => "-38.532222", "Telefone" => "(85) 3477.1200", "Endereço" => "Aeroporto Pinto Martins (FOR) - Av. Sen. Carlos Jereissati, 3000 - Serrinha, Fortaleza - CE, 60741-900, Brasil", "Município" => "Fortaleza", "Estado" => "Ceará"},
                "Aeroporto Internacional Salgado Filho" => {"ICAO" => "SBPA", "Latitude" => "-29.993889", "Longitude" => "-51.171111", "Telefone" => "(51) 3358.2000", "Endereço" => "Aeroporto Internacional Salgado Filho (POA) - Av. Severo Dulius, 90010 - São João, Porto Alegre - RS, 90200-310, Brasil", "Município" => "Porto Alegre", "Estado" => "Rio Grande do Sul"},
                "Aeroporto Santa Genoveva" => {"ICAO" => "SBGO", "Latitude" => "-16.631598", "Longitude" => "-49.221991", "Telefone" => "(62) 3265.1500", "Endereço" => "Aeroporto (GYN) - Goiânia - GO, Brasil", "Município" => "Goiânia", "Estado" => "Goiás"},
                "Aeroporto de Santa Maria" => {"ICAO" => "SBSM", "Latitude" => "-29.7106", "Longitude" => "-53.6875", "Endereço" => "Aeroporto de Santa Maria (RIA) - Camobi, Santa Maria - RS, Brasil", "Município" => "Santa Maria", "Estado" => "Rio Grande do Sul"},
                "Aeroporto Santos Dumont" => {"ICAO" => "SBRJ", "Latitude" => "-22.9061967", "Longitude" => "-43.165634", "Telefone" => "(21) 3814.7070", "Endereço" => "Aeroporto Santos Dumont (SDU) - Centro, Rio de Janeiro - RJ, Brasil", "Município" => "Rio de Janeiro", "Estado" => "Rio de Janeiro"},
                "Aeroporto Internacional de Val de Cans" => {"ICAO" => "SBBE", "Latitude" => "-1.3847222", "Longitude" => "-48.4788889", "Telefone" => "(91) 3210.6039", "Endereço" => "Aeroporto (BEL) - Val-de-Cães, Belém - PA, Brasil", "Município" => "Belém", "Estado" => "Pará"},
                "Aeroporto Internacional Zumbi dos Palmares" => {"ICAO" => "SBMO", "Latitude" => "-9.512521", "Longitude" => "-35.800446", "Telefone" => "(82) 3221.3000", "Endereço" => "Aeroporto (MCZ) - Maceió - AL, Brasil", "Município" => "Maceió", "Estado" => "Alagoas"},
                "Atlanta International Airport" => {"Latitude" => "33.6291291", "Longitude" => "-84.415657", "Endereço" => "6000 N Terminal Pkwy, Atlanta, GA 30320, EUA", "Município" => "Atlanta"},
                "Baltimore Washington International" => {"Latitude" => "39.1660638", "Longitude" => "-76.6911698", "Endereço" => "Baltimore, MD 21240, EUA", "Município" => "Baltimore"},
                "Boston Logan International" => {"Latitude" => "42.3656171", "Longitude" => "-71.0117542", "Endereço" => "1 Harborside Dr, Boston, MA 02128, EUA", "Município" => "Boston"},
                "Charlotte Douglas International" => {"Latitude" => "35.214407", "Longitude" => "-80.9495086", "Endereço" => "5501 R C Josh Birmingham Pkwy, Charlotte, NC 28208, EUA", "Município" => "Charlotte"},
                "O'Hare International Airport" => {"Latitude" => "41.9741665", "Longitude" => "-87.9095154", "Endereço" => "10000 W O'Hare Ave, Chicago, IL 60666, EUA", "Município" => "Chicago"},
                "Dallas - Fort Worth International" => {"Latitude" => "32.9070051", "Longitude" => "-97.041675", "Endereço" => "DFW International Airport (DFW), Grapevine, TX 75261, EUA", "Município" => "Grapevine"},
                "Denver International Airport" => {"Latitude" => "39.8561004", "Longitude" => "-104.6759316", "Endereço" => "8500 Peña Blvd, Denver, CO 80249, EUA", "Município" => "Denver"},
                "Detroit Metropolitan Airport" => {"Latitude" => "42.2084487", "Longitude" => "-83.3600788", "Endereço" => "Detroit Metro Airport, Detroit, MI 48242, EUA", "Município" => "Detroit"},
                "Houston Intercontinental Airport" => {"Latitude" => "29.9902245", "Longitude" => "-95.3389767", "Endereço" => "2800 N Terminal Rd, Houston, TX 77032, EUA", "Município" => "Houston"},
                "JFK Airport New York" => {"Latitude" => "40.6388155", "Longitude" => "-73.7785347", "Endereço" => "Queens, NY 11430, EUA", "Município" => "New York"},
                "Las Vegas International Airport" => {"Latitude" => "36.0840041", "Longitude" => "-115.1559329", "Endereço" => "5757 Wayne Newton Blvd, Las Vegas, NV 89119, EUA", "Município" => "Las Vegas"},
                "Miami International Airport" => {"Latitude" => "25.7958698", "Longitude" => "-80.2892397", "Endereço" => "2100 NW 42nd Ave, Miami, FL 33126, EUA", "Município" => "Miami"},
                "Midway International Airport" => {"Latitude" => "41.7867799", "Longitude" => "-87.7543824", "Endereço" => "5700 S Cicero Ave, Chicago, IL 60638, EUA", "Município" => "Chicago"},
                "Minneapolis - St Paul International" => {"Latitude" => "44.8834038", "Longitude" => "-93.212794", "Endereço" => "Fort Snelling Unorganized Territory, MN 55111, Estados Unidos", "Município" => "Minneapolis"},
                "Orlando International Airport" => {"Latitude" => "28.4322965", "Longitude" => "-81.308673", "Endereço" => "Orlando, FL 32827, Estados Unidos", "Município" => "Orlando"},
                "Philadelphia International" => {"Latitude" => "39.8744", "Longitude" => "-75.2446169", "Endereço" => "8000 Essington Ave, Philadelphia, PA 19153, EUA", "Município" => "Philadelphia"},
                "Phoenix Sky Harbor International" => {"Latitude" => "33.4372731", "Longitude" => "-112.0099821", "Endereço" => "3400 E Sky Harbor Blvd, Phoenix, AZ 85034, EUA", "Município" => "Phoenix"},
                "Salt Lake City International" => {"Latitude" => "40.7899444", "Longitude" => "-111.9812646", "Endereço" => "776 N Terminal Dr, Salt Lake City, UT 84122, EUA", "Município" => "Salt Lake City"},
                "San Diego International Airport" => {"Latitude" => "32.7338051", "Longitude" => "-117.1954978", "Endereço" => "3225 N Harbor Dr, San Diego, CA 92101, EUA", "Município" => "San Diego"},
                "San Francisco International" => {"Latitude" => "37.6213171", "Longitude" => "-122.3811494", "Endereço" => "San Francisco, CA 94128, EUA", "Município" => "San Francisco"},
                "Seattle - Tacoma International" => {"Latitude" => "47.4502535", "Longitude" => "-122.3110105", "Endereço" => "17801 International Blvd, Seattle, WA 98158, EUA", "Município" => "Seattle"},
                "Washington National Airport" => {"Latitude" => "47.4502535", "Longitude" => "-122.3110105", "Endereço" => "Arlington, VA 22202, EUA", "Município" => "Washington"}
              }
    airports.each do |key, value|
      airport_place = Place.new
      airport_place.name_pt_br = key
      airport_place.name_en = key
      airport_place.name_es = key
      airport_place.place_type = 1
      airport_place.distance = 5
      airport_place.street = value["Endereço"]
      airport_place.latitude = value["Latitude"]
      airport_place.longitude = value["Longitude"]
      airport_place.active = true
      if City.where(name: value["Município"]).present?
        city = City.where(name: value["Município"]).first
        airport_place.city_id = city.id
        airport_place.country = city.state.country.iso_initials
        airport_place.hotel_id = Hotel.where(city_id: city.id).map(&:id)
        airport_place.hotel_count = airport_place.hotel_id.count
        airport_place.save if airport_place.valid?
      end
    end
  end
end
