def has_booking?(user)
  (Booking.where(user_id: user.id).exists?)
end

namespace :users_emkt_fields do
  task update: :environment do
    users = User.where(past_emkt_date: nil)
    users.each_slice(100) do |users|
      users.each do |user|
        user.past_emkt_date = (DateTime.now - 7.days).to_date.strftime('%Y-%m-%d')
        if user.emkt_flux.nil?
           has_booking?(user) ? user.emkt_flux = 1 : user.emkt_flux = 2
         elsif user.emkt_flux_type.nil?
           has_booking?(user) ? user.emkt_flux_type = 1 : user.emkt_flux_type = 2
        end
        user.save if user.valid?
      end
    end
  end
end
