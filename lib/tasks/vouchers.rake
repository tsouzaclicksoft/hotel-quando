namespace :vouchers do
  task send_pending: :environment do
    PendingVouchersSender.new.run
    PendingUberVouchersSender.new.run
  end
end
