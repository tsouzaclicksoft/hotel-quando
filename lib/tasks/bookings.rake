namespace :bookings do

  task expire_bookings: :environment do
    bookings_to_expire = Booking.where("status=#{Booking::ACCEPTED_STATUSES[:waiting_for_payment]} AND created_at BETWEEN (?) AND (?)", (DateTime.now - 2.days), (DateTime.now - 20.minutes))
    expired_bookings_count = 0

    bookings_to_expire.each do | booking |
      if booking.expire
      	if booking.promotional_code_log
      		booking.promotional_code_log.promotional_code.active_when_booking_expire(booking.promotional_code_log)
      	end
        expired_bookings_count += 1
      end
    end
  end

  task send_sms: :environment do
    bookings_confirmed = Booking.by_status([Booking::ACCEPTED_STATUSES[:confirmed],
                      Booking::ACCEPTED_STATUSES[:confirmed_and_captured],
                      Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
                    ]).where("date_interval @> ?::timestamp OR date_interval @> ?::timestamp", 1.hour.ago.strftime('%Y-%m-%d %H:%M +0000'), 1.hour.since.strftime('%Y-%m-%d %H:%M +0000')).eager_load(:offer, :omnibees_offer)

    bookings_to_send_sms_checkin = bookings_confirmed.where("(offers.checkin_timestamp BETWEEN ? AND ?) OR (omnibees_offers.checkin_timestamp BETWEEN ? AND ?)",
                                    (Time.current + 1.minute).strftime('%Y-%m-%d %H:%M +0000'),(Time.current + 1.hour).strftime('%Y-%m-%d %H:%M +0000'),
                                    (Time.current + 1.minute).strftime('%Y-%m-%d %H:%M +0000'),(Time.current + 1.hour).strftime('%Y-%m-%d %H:%M +0000')
                                  )
    bookings_to_send_sms_checkout = bookings_confirmed.where("offers.checkout_timestamp BETWEEN ? AND ? OR omnibees_offers.checkout_timestamp BETWEEN ? AND ?",
                                    (Time.current + 1.minute).strftime('%Y-%m-%d %H:%M +0000'), (Time.current + 1.hour).strftime('%Y-%m-%d %H:%M +0000'),
                                    (Time.current + 1.minute).strftime('%Y-%m-%d %H:%M +0000'), (Time.current + 1.hour).strftime('%Y-%m-%d %H:%M +0000')
                                  )

    bookings_to_send_sms_checkin.each do |booking|
      SendSmsWorker.perform_async(booking.id, :sms_checkin_information)
    end
    bookings_to_send_sms_checkout.each do |booking|
      SendSmsWorker.perform_async(booking.id, :sms_checkout_information)
    end
  end

end
