require 'platform-api'

namespace :heroku do
  desc 'restarts all heroku dynos'
  task :restart_dyno do
    token = ENV.fetch('HEROKU_API_TOKEN_FOR_RESTART_DYNO')
    heroku = PlatformAPI.connect_oauth(token)
    heroku.dyno.restart_all("hotelquando")
  end
end
