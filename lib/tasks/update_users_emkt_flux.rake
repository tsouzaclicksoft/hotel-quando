namespace :users do
  task update_users_emkt_flux: :environment do
    User.all.each_slice(100) do |users|
      users.each do |user|
        user_booking = user.bookings.where("status = ? OR status = ? OR status = ? OR status = ?", 2, 4, 6, 8).count
        if user_booking == 0
          user.emkt_flux_type = User::EMKT_FLUX_TYPES[:without_bookings]
        else
          user.emkt_flux_type = User::EMKT_FLUX_TYPES[:with_bookings]
        end
        user.save if user.valid?
      end
    end
  end
end