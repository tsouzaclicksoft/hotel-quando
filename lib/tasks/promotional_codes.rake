namespace :promotional_codes do

  task expire_promotional_codes: :environment do
    promotional_codes_to_expire = PromotionalCode.where("expiration_date < ?", DateTime.now.to_date).where(status: [PromotionalCode::ACCEPTED_STATUSES[:active],[PromotionalCode::ACCEPTED_STATUSES[:suspended]], [PromotionalCode::ACCEPTED_STATUSES[:used]]])
    expired_promotional_codes_count = 0
    promotional_codes_to_expire.each do | promotional_code |
      if promotional_code.expire
        expired_promotional_codes_count += 1
      end
    end
		puts "Done! #{expired_promotional_codes_count}"
  end

	task start_promotional_codes: :environment do
		promotional_code_to_start = PromotionalCode.where("start_date <= ?", DateTime.now.to_date).where(status: PromotionalCode::ACCEPTED_STATUSES[:suspended])
		start_promotional_code_count = 0  	
		promotional_code_to_start.each do | promotional_code |
			if promotional_code.start
				start_promotional_code_count += 1
			end
		end
	end

end
