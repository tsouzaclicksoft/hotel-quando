namespace :tudo_azul do
  task send_pending: :environment do
    AzulPendingBookingsSender.new.run
  end
end
