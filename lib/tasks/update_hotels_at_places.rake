def ensure_hotel_distance(hotel, latitude, longitude, distance)
  hotels = []
  hotel.each do |hotel|
    hotels << hotel if hotel[:hotel].distance_from([latitude.to_f, longitude.to_f]) <= distance.to_f
  end
  return hotels
end

def verify_offers(lat, long)
  offers = []
  [3, 6, 9, 12].each do |pack_of_hours|
    offers << OffersSearch::Hotels.new({number_of_guests: 1, latitude: lat,
      longitude: long, check_in_datetime: (DateTime.now.change({ hour: 14, min: 0, sec: 0 }) + 1.day), pack_in_hours: pack_of_hours, room_category: 0}).results.exacts
  end
  offers.flatten
end


namespace :update_hotels_at_places do
  task update: :environment do
    places = Place.all
    places.each do |place|
      hotels = verify_offers(place.latitude, place.longitude)
      unless hotels.empty?
        if place.place_type == '2'
          place.hotel_id = hotels.collect{ |h| h[:hotel].id }
        else
          hotel = ensure_hotel_distance(hotels, place.latitude, place.longitude, place.distance)
          place.hotel_id = hotel.collect{ |h| h[:hotel].id }
        end
        place.hotel_count = place.hotel_id.count
      end
      place.save if place.valid?
    end
  end
end
