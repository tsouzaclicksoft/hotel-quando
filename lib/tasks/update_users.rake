namespace :update_users do
  task past_emkt_date: :environment do
    User.all.each_slice(200) do |users|
      users.each do |user|
        next if user.past_emkt_date.nil?
        if user.past_emkt_date.wday == 1
          user.past_emkt_date = ((DateTime.now + 3.days) - 7.days).to_date.strftime('%Y-%m-%d')
          user.save(validate: false)
        elsif user.past_emkt_date.wday == 2
          user.past_emkt_date = ((DateTime.now + 4.days) - 7.days).to_date.strftime('%Y-%m-%d')
          user.save(validate: false)
        elsif user.past_emkt_date.wday == 3
          user.past_emkt_date = ((DateTime.now + 5.days) - 7.days).to_date.strftime('%Y-%m-%d')
          user.save(validate: false)
        elsif user.past_emkt_date.wday == 5
          user.past_emkt_date = ((DateTime.now + 7.days) - 7.days).to_date.strftime('%Y-%m-%d')
          user.save(validate: false)
        end
      end
    end
  end
end
