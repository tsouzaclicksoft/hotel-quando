def with_spinner
  chars = %w[| / - \\]
  not_done = true
  spinner = Thread.new do
    while not_done do
      print chars.rotate!.first
      sleep 0.1
      print "\b"
    end
  end
  yield.tap do
    not_done = false
    spinner.join
  end
end

def convert_date_to_wday
  User.all.each_slice(100) do |users|
    users.each do |user|
      next if user.emkt_day_of_week.nil?
      day = user.emkt_day_of_week.to_date.wday
      user.emkt_day_of_week = day.to_i
      user.save
    end
  end
  Newsletter.all.each_slice(100) do |news|
    news.each do |n|
      next if n.emkt_day_of_week.nil?
      day = n.emkt_day_of_week.to_date.wday
      n.emkt_day_of_week = day.to_i
      n.save
    end
  end
end

namespace :emkt do
  task change_emkt_date: :environment do

    with_spinner do
      convert_date_to_wday
    end
    puts "Done!"
  end
end