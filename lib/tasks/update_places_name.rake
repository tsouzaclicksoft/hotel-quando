namespace :places do
  task update_name: :environment do
    Place.all.each do |place|
      place.name_en = place.photos.first.title_en
      place.name_es = place.photos.first.title_es
      place.save
    end
  end
end
