desc "Create booking taxes for meeting room's pack of hours"
namespace :create_booking_taxes do
  task meeting_room: :environment do
    countries = Country.all.where(id: BookingTax.all.map(&:country_id))
    currency = Currency.first.money_sign
    countries.each do |country|
      1.upto(8) do |pack_of_hours|
        booking_tax = BookingTax.new({name: "fee_consumer_for_pack_#{pack_of_hours}h",
                                      country_id: country.id, value: 0, currency_symbol: currency})
        booking_tax.save if booking_tax.valid?
      end
    end
  end
end
