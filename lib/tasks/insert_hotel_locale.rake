namespace :hotel do
  task insert_hotel_locale: :environment do
    Hotel.all.each_slice(100) do |hotels|
      hotels.each do |hotel|
        if hotel.country == ('MX' || 'PY' || 'CO' || 'ES')
          hotel.locale = User::ACCEPTED_LOCALES[2][0]
        elsif hotel.country == ('US' || 'CA')
          hotel.locale = User::ACCEPTED_LOCALES[1][0]
        elsif hotel.country == ('BR')
          hotel.locale = User::ACCEPTED_LOCALES[0][0]
        end
        hotel.save if hotel.valid?
      end
    end
  end
end
