namespace :places do
  task update: :environment do
    Place.all.each do |place|
      unless place.country == ('BR' || 'ES' || 'US')
        place.country = Country.where(id: place.country.to_i).first.iso_initials
      end
      place.street = place.address
      place.active = true
      place.save if place.valid?

      city = City.where(id: place.city_id).first
      city.place_id = [place.id]
      city.save! if city.valid?
    end
  end
end
