namespace :offers do
  task :expire_olds => :environment do
    Offer.where(%Q{
    (status IN (?)) AND id IN
      (SELECT offer_id FROM bookings WHERE
        (checkin_timestamp BETWEEN (?) AND (?)) AND
        offer_id IS NOT NULL
      )
    },
      [ Offer::ACCEPTED_STATUS[:available],
        Offer::ACCEPTED_STATUS[:suspended],
        Offer::ACCEPTED_STATUS[:ghost]
      ],
      10.day.ago.strftime("%Y-%m-%d 00:00:00"),
      3.hours.ago.strftime("%Y-%m-%d %H:%M:%S")
    ).update_all(status: Offer::ACCEPTED_STATUS[:expired])

    Offer.where('(status IN (?)) AND (checkin_timestamp BETWEEN (?) AND (?))',
      [ Offer::ACCEPTED_STATUS[:available],
        Offer::ACCEPTED_STATUS[:suspended],
        Offer::ACCEPTED_STATUS[:ghost]],
      (10.day.ago).strftime("%Y-%m-%d 00:00:00"),
      3.hours.ago.strftime("%Y-%m-%d %H:%M:%S"))
    .update_all(status: Offer::ACCEPTED_STATUS[:waiting_removal])

    MeetingRoomOffer.where(%Q{
    (status IN (?)) AND id IN
      (SELECT meeting_room_offer_id FROM bookings WHERE
        (checkin_timestamp BETWEEN (?) AND (?)) AND
        meeting_room_offer_id IS NOT NULL
      )
    },
      [ MeetingRoomOffer::ACCEPTED_STATUS[:available],
        MeetingRoomOffer::ACCEPTED_STATUS[:suspended],
        MeetingRoomOffer::ACCEPTED_STATUS[:ghost]
      ],
      10.day.ago.strftime("%Y-%m-%d 00:00:00"),
      3.hours.ago.strftime("%Y-%m-%d %H:%M:%S")
    ).update_all(status: MeetingRoomOffer::ACCEPTED_STATUS[:expired])

    MeetingRoomOffer.where('(status IN (?)) AND (checkin_timestamp BETWEEN (?) AND (?))',
      [ MeetingRoomOffer::ACCEPTED_STATUS[:available],
        MeetingRoomOffer::ACCEPTED_STATUS[:suspended],
        MeetingRoomOffer::ACCEPTED_STATUS[:ghost]],
      (10.day.ago).strftime("%Y-%m-%d 00:00:00"),
      3.hours.ago.strftime("%Y-%m-%d %H:%M:%S"))
    .update_all(status: MeetingRoomOffer::ACCEPTED_STATUS[:waiting_removal])

    EventRoomOffer.where(%Q{
    (status IN (?)) AND id IN
      (SELECT event_room_offer_id FROM bookings WHERE
        (checkin_timestamp BETWEEN (?) AND (?)) AND
        event_room_offer_id IS NOT NULL
      )
    },
      [ EventRoomOffer::ACCEPTED_STATUS[:available],
        EventRoomOffer::ACCEPTED_STATUS[:suspended],
        EventRoomOffer::ACCEPTED_STATUS[:ghost]
      ],
      10.day.ago.strftime("%Y-%m-%d 00:00:00"),
      3.hours.ago.strftime("%Y-%m-%d %H:%M:%S")
    ).update_all(status: EventRoomOffer::ACCEPTED_STATUS[:expired])

    EventRoomOffer.where('(status IN (?)) AND (checkin_timestamp BETWEEN (?) AND (?))',
      [ EventRoomOffer::ACCEPTED_STATUS[:available],
        EventRoomOffer::ACCEPTED_STATUS[:suspended],
        EventRoomOffer::ACCEPTED_STATUS[:ghost]],
      (10.day.ago).strftime("%Y-%m-%d 00:00:00"),
      3.hours.ago.strftime("%Y-%m-%d %H:%M:%S"))
    .update_all(status: EventRoomOffer::ACCEPTED_STATUS[:waiting_removal])
  end


  task :delete_olds => :environment do
    Offer.where('status = ? AND checkin_timestamp < ?', Offer::ACCEPTED_STATUS[:waiting_removal], 1.months.ago).delete_all
    MeetingRoomOffer.where('status = ? AND checkin_timestamp < ?', MeetingRoomOffer::ACCEPTED_STATUS[:waiting_removal], 1.months.ago).delete_all
    EventRoomOffer.where('status = ? AND checkin_timestamp < ?', EventRoomOffer::ACCEPTED_STATUS[:waiting_removal], 1.months.ago).delete_all
  end

  task :update_offer_prices => :environment do
    Hotel.all.each do |hotel|
      days_pricing_policies_array = ['Hoje','Amanhã','Depois de Amanhã','D+3','D+4','D+5']
      days_pricing_policies_array.each do |day|
        pricing_policy = hotel.pricing_policies.where(name: day).first
        next if pricing_policy.nil?
        offers = case day
          when "Hoje" then hotel.offers.where('status = ? AND (checkin_timestamp BETWEEN (?) AND (?))', Offer::ACCEPTED_STATUS[:available], DateTime.now.in_time_zone("America/Sao_Paulo").strftime("%Y-%m-%d %H:%M:%S"), DateTime.now.in_time_zone("America/Sao_Paulo").strftime("%Y-%m-%d 23:59:59"))
          when "Amanhã" then hotel.offers.where('status = ? AND (checkin_timestamp BETWEEN (?) AND (?))', Offer::ACCEPTED_STATUS[:available], (DateTime.now.in_time_zone("America/Sao_Paulo") + 1.day).strftime("%Y-%m-%d 00:00:00"), (DateTime.now.in_time_zone("America/Sao_Paulo") + 1.day).strftime("%Y-%m-%d 23:59:59"))
          when "Depois de Amanhã" then hotel.offers.where('status = ? AND (checkin_timestamp BETWEEN (?) AND (?))', Offer::ACCEPTED_STATUS[:available], (DateTime.now.in_time_zone("America/Sao_Paulo") + 2.days).strftime("%Y-%m-%d 00:00:00"), (DateTime.now.in_time_zone("America/Sao_Paulo") + 2.day).strftime("%Y-%m-%d 23:59:59"))
          when "D+3" then hotel.offers.where('status = ? AND (checkin_timestamp BETWEEN (?) AND (?))', Offer::ACCEPTED_STATUS[:available], (DateTime.now.in_time_zone("America/Sao_Paulo") + 3.day).strftime("%Y-%m-%d 00:00:00"), (DateTime.now.in_time_zone("America/Sao_Paulo") + 3.day).strftime("%Y-%m-%d 23:59:59"))
          when "D+4" then hotel.offers.where('status = ? AND (checkin_timestamp BETWEEN (?) AND (?))', Offer::ACCEPTED_STATUS[:available], (DateTime.now.in_time_zone("America/Sao_Paulo") + 4.day).strftime("%Y-%m-%d 00:00:00"), (DateTime.now.in_time_zone("America/Sao_Paulo") + 4.day).strftime("%Y-%m-%d 23:59:59"))
          when "D+5" then hotel.offers.where('status = ? AND (checkin_timestamp BETWEEN (?) AND (?))', Offer::ACCEPTED_STATUS[:available], (DateTime.now.in_time_zone("America/Sao_Paulo") + 5.day).strftime("%Y-%m-%d 00:00:00"), (DateTime.now.in_time_zone("America/Sao_Paulo") + 5.day).strftime("%Y-%m-%d 23:59:59"))
        end
        pricing_policy.room_type_pricing_policies.each do |rt_pricing_policy|
          Offer::ACCEPTED_LENGTH_OF_PACKS.each do |pack_size|
            update_offers = offers.where(room_type_id: rt_pricing_policy.room_type_id, pack_in_hours: pack_size)
            
            pack_price = rt_pricing_policy.send("pack_price_#{pack_size}h")
            extra_person = rt_pricing_policy.send("extra_price_per_person_for_pack_price_#{pack_size}h")
            no_show_value = pack_price*((rt_pricing_policy.send("penalty_for_#{pack_size}h_pack_in_percentage"))/100)

            if update_offers.first.present? && pack_price > 0
              offers_need_to_update = update_offers.first.price != pack_price || update_offers.first.extra_price_per_person != extra_person || update_offers.first.no_show_value != no_show_value
              if offers_need_to_update
                update_offers.update_all(price: pack_price, extra_price_per_person: extra_person, no_show_value: no_show_value)
              end
            end
          end
        end
      end
    end
  end
end
