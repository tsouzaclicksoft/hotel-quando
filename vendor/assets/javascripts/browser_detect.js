
// Checks the browser and adds classes to the body to reflect it.

$(document).ready(function(){

    var userAgent = navigator.userAgent.toLowerCase();
    $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
    // Is this a version of IE?
    if($.browser.msie){
        if (!$.browser.version) {
          $.browser.version = ""
        }
        $('body').addClass('browserIE');
        // Add the version number
        var msie = userAgent.indexOf ( "msie " )

        if ( msie > 0 ){
          var version = parseInt(userAgent.substring (msie+5, userAgent.indexOf (".", msie )));
          $('body').addClass('browserIE' + version);
        }
        $.browser.mozilla = false;
    }

    // Is this a version of Chrome?
    if($.browser.chrome){

        $('body').addClass('browserChrome');

        //Add the version number
        userAgent = userAgent.substring(userAgent.indexOf('chrome/') +7);
        userAgent = userAgent.substring(0,1);
        $('body').addClass('browserChrome' + userAgent);

        // If it is chrome then jQuery thinks it's safari so we have to tell it it isn't
        $.browser.safari = false;
    }

    // Is this a version of Safari?
    if($.browser.safari){
        $('body').addClass('browserSafari');

        // Add the version number
        userAgent = userAgent.substring(userAgent.indexOf('version/') +8);
        userAgent = userAgent.substring(0,1);
        $('body').addClass('browserSafari' + userAgent);
    }

    // Is this a version of Mozilla?
    if($.browser.mozilla){

        //Is it Firefox?
        if(navigator.userAgent.toLowerCase().indexOf('firefox') != -1){
            $('body').addClass('browserFirefox');

            // Add the version number
            userAgent = userAgent.substring(userAgent.indexOf('firefox/') +8);
            userAgent = userAgent.substring(0,1);
            $('body').addClass('browserFirefox' + userAgent);
        }
        // If not then it must be another Mozilla
        else{
            $('body').addClass('browserMozilla');
        }
    }

    // Is this a version of Opera?
    if($.browser.opera){
        $('body').addClass('browserOpera');
    }


});
