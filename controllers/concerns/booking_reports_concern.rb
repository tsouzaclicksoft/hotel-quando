module BookingReportsConcern
  extend ActiveSupport::Concern

  included do
    def invoice_for(subject, invoice_type, tenth_day_of_invoice_month)
      invoice = {}
      invoice[:bookings_collection] = bookings_collections_for(subject, invoice_type, tenth_day_of_invoice_month)
      build_bookings_info_for(subject, invoice_type, invoice)
      return invoice
    end

    protected
      # Using april as an example (month #4), we will have this two possible invoices
      # Invoice of 10th day (generated after 10/04 00:00) will have:
      # bookings paid at the hotel: 10/03 -> 09/04 23:59
      # no-shows: 01/03 00:00 -> 09/03 23:59
      # bookings paid online: 01/03 00:00 -> 09/03 23:59
      # Invoice of end of month (generated after 01/05 00:00) will have:
      # no-shows: 10/03 00:00 -> 31/03 23:59
      # bookings paid online: 10/03 00:00 -> 31/03 23:59
      # Need to subtract one second from some dates, otherwise, for example,
      # if there is a booking for 10/04 00:00, it will appear on tenth day invoice and end of month invoice
      # meaning that it will be paid 2 times to the hotel

      def bookings_collections_for(subject, invoice_type, tenth_day_of_invoice_month)
        invoice_bookings = {}
        subject_bookings = subject.bookings.eager_load(:offer)
        set_report_dates(tenth_day_of_invoice_month)
        set_bookings_paid_at_the_hotel_collection(subject_bookings, invoice_type, invoice_bookings)
        set_no_shows_and_bookings_paid_online_collection(subject_bookings, invoice_type, invoice_bookings)
        return invoice_bookings # hash with the bookings list for the invoice
      end


      def set_report_dates(tenth_day_of_invoice_month)
        @tenth_day_of_invoice_month = tenth_day_of_invoice_month
        @beginning_of_prev_month = @tenth_day_of_invoice_month.to_datetime.prev_month.beginning_of_month.strftime('%Y-%m-%d %H:%M +0000')
        @end_of_prev_month = @tenth_day_of_invoice_month.to_datetime.prev_month.end_of_month.strftime('%Y-%m-%d %H:%M +0000')
        @tenth_day_of_prev_month = (@beginning_of_prev_month.to_datetime + 9.days)
      end

      def set_no_shows_and_bookings_paid_online_collection(mixed_bookings_collection, invoice_type, invoice_bookings_hash)
        if invoice_type == 'tenth_day'
          date_range = [ @beginning_of_prev_month, (@tenth_day_of_prev_month - 1.second) ]
        else
          date_range = [ @tenth_day_of_prev_month, @end_of_prev_month ]
        end

        mixed_bookings_collection_scoped_by_date = mixed_bookings_collection.where(
                                                      "(offers.checkout_timestamp + interval '1 day') BETWEEN ? AND ?",
                                                      date_range.first, date_range.last
                                                    )

        invoice_bookings_hash[:no_shows] = mixed_bookings_collection_scoped_by_date.by_status(Booking::ACCEPTED_STATUSES[:no_show_paid_with_success])
        invoice_bookings_hash[:bookings_paid_online] = mixed_bookings_collection_scoped_by_date.by_status([Booking::ACCEPTED_STATUSES[:confirmed_and_captured],Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]])
      end

      def set_bookings_paid_at_the_hotel_collection(mixed_bookings_collection, invoice_type, invoice_bookings_hash)
        if invoice_type == "tenth_day"
          invoice_bookings_hash[:bookings_paid_at_the_hotel] = mixed_bookings_collection.where("(offers.checkout_timestamp + interval '1 day') BETWEEN ? AND ?",
                                                                                        @tenth_day_of_prev_month, (@tenth_day_of_invoice_month - 1.second))
                                                                               .by_status([Booking::ACCEPTED_STATUSES[:confirmed], Booking::ACCEPTED_STATUSES[:confirmed_without_card]])
        end
      end

      def build_bookings_info_for(subject, invoice_type, invoice)
        hotel_invoice = subject.is_a? Hotel
        invoice[:info] = {}
        invoice[:info][:bookings_paid_online] = bookings_info_from_collection(invoice[:bookings_collection][:bookings_paid_online], hotel_invoice)
        invoice[:info][:bookings_paid_at_the_hotel] = bookings_info_from_collection(invoice[:bookings_collection][:bookings_paid_at_the_hotel], hotel_invoice)
        invoice[:info][:no_shows] = bookings_info_from_collection(invoice[:bookings_collection][:no_shows], hotel_invoice)
        invoice[:info][:totals] = invoice_totals(invoice)
        invoice[:info][:totals].merge! agency_invoice_totals(subject, invoice) if subject.is_a? Agency
      end

      def bookings_info_from_collection(bookings_collection, hotel_invoice = true)
        if bookings_collection.try(:first)

          if hotel_invoice # if the invoice is for a hotel, there is no need to calculate the comission and value without iss in a per booking basis
            @hotel ||= bookings_collection.first.hotel
            total_value, total_booking_tax = bookings_collection_totals(bookings_collection)
            total_value_without_iss = (1 - (@hotel.iss_in_percentage / 100)) * total_value
            comission = @hotel.comission_value
          else # the hotels may have different comission and iss percentages
            total_value, total_booking_tax, total_value_without_iss, comission = bookings_collection_totals(bookings_collection, true)
          end

          if bookings_collection.first.status == Booking::ACCEPTED_STATUSES[:confirmed]
            credit_card_operation_tax = 0
            total_value_to_be_transferred_to_the_hotel = 0
          else
            credit_card_operation_tax = ((Payment::CREDIT_CARD_OPERATION_TAX_IN_PERCENTAGE.to_f)/100) * total_value
            total_value_to_be_transferred_to_the_hotel = total_value - (credit_card_operation_tax + comission)
          end

          default_value = nil

        else
          default_value = 0
        end

        { total: default_value || bookings_collection.count,
          total_value: default_value || total_value,
          total_value_without_iss: default_value || total_value_without_iss,
          booking_tax: default_value || total_booking_tax,
          credit_card_operation_tax: default_value || credit_card_operation_tax,
          comission: default_value || comission,
          total_value_to_be_transferred_to_the_hotel: default_value || total_value_to_be_transferred_to_the_hotel
        }
      end

      def bookings_collection_totals(bookings_collection, calculate_all_possible_booking_totals = false)
        if bookings_collection.first.status == Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
          booking_attribute_with_paid_price = :no_show_value_without_tax
        else
          booking_attribute_with_paid_price = :total_price_without_tax
        end
        if calculate_all_possible_booking_totals
          calculate_all_possible_bookings_totals(bookings_collection, booking_attribute_with_paid_price)
        else
          calculate_bookings_total_price_and_tax(bookings_collection, booking_attribute_with_paid_price)
        end
      end

      def calculate_bookings_total_price_and_tax(bookings_collection, booking_attribute_with_paid_price)
        total_price = 0
        total_tax = 0
        bookings_collection.each do | booking |
          total_price += booking.send(booking_attribute_with_paid_price)
          total_tax += booking.booking_tax
        end
        [ total_price, total_tax ]
      end

      def calculate_all_possible_bookings_totals(bookings_collection, booking_attribute_with_paid_price)
        total_price = 0
        total_tax = 0
        total_value_without_iss = 0
        comission = 0
        bookings_collection.eager_load(:hotel)
        bookings_collection.each do | booking |
          total_price += booking.send(booking_attribute_with_paid_price)
          total_tax += booking.booking_tax
          booking_value_without_iss = (1 - (booking.hotel.iss_in_percentage / 100)) * booking.send(booking_attribute_with_paid_price)
          total_value_without_iss += booking_value_without_iss
          comission += booking.hotel.comission_value
        end
        [ total_price, total_tax, total_value_without_iss, comission ]
      end


      def invoice_totals(invoice)
        totals = {}
        totals[:value] = invoice[:info][:bookings_paid_at_the_hotel][:total_value].to_f + invoice[:info][:bookings_paid_online][:total_value].to_f + invoice[:info][:no_shows][:total_value].to_f
        totals[:hotelquando_comission] = invoice[:info][:bookings_paid_at_the_hotel][:comission].to_f + invoice[:info][:bookings_paid_online][:comission].to_f + invoice[:info][:no_shows][:comission].to_f
        totals[:booking_tax] = invoice[:info][:bookings_paid_at_the_hotel][:booking_tax].to_f + invoice[:info][:bookings_paid_online][:booking_tax].to_f + invoice[:info][:no_shows][:booking_tax].to_f
        totals[:value_without_iss] = invoice[:info][:bookings_paid_at_the_hotel][:total_value_without_iss].to_f + invoice[:info][:no_shows][:total_value_without_iss] + invoice[:info][:bookings_paid_online][:total_value_without_iss]
        totals[:to_pay_to_the_hotel] = invoice[:info][:no_shows][:total_value_to_be_transferred_to_the_hotel] + invoice[:info][:bookings_paid_online][:total_value_to_be_transferred_to_the_hotel]
        totals[:credit_card_tax] = invoice[:info][:bookings_paid_online][:credit_card_operation_tax] + invoice[:info][:no_shows][:credit_card_operation_tax]
        totals[:balance] = ( invoice[:info][:bookings_paid_at_the_hotel].try(:[], :comission).to_f + invoice[:info][:bookings_paid_at_the_hotel].try(:[], :booking_tax).to_f ) - totals[:to_pay_to_the_hotel]
        return totals
      end

      def agency_invoice_totals(agency, invoice_hash)
        agency_invoice_totals = {}
        agency_invoice_totals[:agency_comission] = (agency.comission_in_percentage.to_f / 100) * invoice_hash[:info][:totals][:hotelquando_comission]
        agency_invoice_totals[:number_of_bookings_created_by_agency_clients] = invoice_hash[:bookings_collection][:no_shows].try(:where, created_by_agency: false).try(:length).to_i + invoice_hash[:bookings_collection][:bookings_paid_online].try(:where, created_by_agency: false).try(:length).to_i + invoice_hash[:bookings_collection][:bookings_paid_at_the_hotel].try(:where, created_by_agency: false).try(:length).to_i
        agency_invoice_totals[:number_of_bookings_created_by_agency] = invoice_hash[:bookings_collection][:no_shows].try(:where, created_by_agency: true).try(:length).to_i + invoice_hash[:bookings_collection][:bookings_paid_online].try(:where, created_by_agency: true).try(:length).to_i + invoice_hash[:bookings_collection][:bookings_paid_at_the_hotel].try(:where, created_by_agency: true).try(:length).to_i
        agency_invoice_totals[:number_of_bookings] = agency_invoice_totals[:number_of_bookings_created_by_agency_clients] + agency_invoice_totals[:number_of_bookings_created_by_agency]
        return agency_invoice_totals
      end

  end # of included

end
