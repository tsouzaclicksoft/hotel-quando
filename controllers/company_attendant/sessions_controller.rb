# encoding: utf-8
class CompanyAttendant::SessionsController < Devise::SessionsController
  layout 'company_attendant/login'

	def create
    attendant = CompanyAttendant.find_by_login(params[:company_attendant_company_attendant].try(:[], :login))
    if attendant.nil? || attendant.try(:is_active?)
      super # if the affiliate is not found or the affiliate is active, we call the default devise action, else the affiliate is inactive and we display a custom message
    else
      flash['alert'] = "Sua conta foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_company_attendant_company_attendant_session_path
    end
  end

  def after_sign_in_path_for(attendant)
    company_attendant_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    company_attendant_root_path
  end
end
