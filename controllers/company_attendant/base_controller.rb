# encoding: utf-8
class CompanyAttendant::BaseController < ActionController::Base
  protect_from_forgery
  inherit_resources
  layout 'company_attendant'
  before_filter :authenticate_company_attendant_company_attendant!, :set_locale_as_pt_br

  protected

    def begin_of_association_chain
      current_company_attendant_company_attendant
    end

    def getCurrentExecutive
      return current_company_attendant_company_attendant
    end

    def set_locale_as_pt_br
      I18n.locale = :pt_br
    end

end
