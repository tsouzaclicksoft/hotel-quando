class Api::V2::PayuNotificationController < ApplicationController

  @@user = 'payu@payu'
  @@password = '@1d33f98f6e4d9730391c@'

  def notification
    
    if authenticated
        if params[:reference_sale].present?
          bookingid = params[:reference_sale].gsub(/\D/, '')
          @booking = Booking.find(bookingid);
          @payment = @booking.payments.last;

          if @booking.status != Booking::ACCEPTED_STATUSES[:confirmed] && @payment.status != Payment::ACCEPTED_STATUSES[:authorized]
            if params[:response_message_pol] == 'APPROVED'
              logger.info "Payment transaction-id: #{params[:transaction_id]}  execute successfully"
              if @booking.will_be_paid_online?
                if @booking.hotel.booking_on_request
                  @booking.status = Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
                  @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
                else
                  @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
                  @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
                end
                @payment.status = Payment::ACCEPTED_STATUSES[:captured]
              else
                if @booking.hotel.booking_on_request
                  @booking.status = Booking::ACCEPTED_STATUSES[:requested_reservation]
                  @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
                else
                  @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
                  @booking.user.update(emkt_flux_type: User::EMKT_FLUX_TYPES[:with_bookings])
                end
                @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
              end
              @payment.maxipago_response = ''
              @payment.payu_transaction_id = params[:transaction_id].to_s
              @booking.save!
              @payment.save!
            else
              @payment.maxipago_response += params[:response_message_pol]
              @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
              @payment.save!

              if @booking.status == Booking::ACCEPTED_STATUSES[:confirmed] && @payment.status == Payment::ACCEPTED_STATUSES[:authorized]
                UserMailer.booking_canceled_send_to_payu(@payment).deliver
                UserMailer.send_payu_payment_failure_notice(@payment).deliver
              end
            end
          end
        end

        render :json => '', status: 200
    else
      render :json => '', :status => 401
    end

  end

  def authenticated
    user = request.query_parameters[:user]
    password = request.query_parameters[:password]

    if user == @@user && password == @@password
      return true
    end

    return false

  end

end
