class Api::V2::DevicesController < Public::BaseController
  respond_to :json

  def create
  	device_id = params[:device]
  	if device_id.nil?
  		render :json => { :msg => 'Missing Device ID' }.to_json, :status => 500
  	else
  		device = Device.where(device_id: device_id).first
  		if device.nil?
  			Device.create(device_id: device_id)
	  		render :json => { :msg => 'Device Registered Successfully' }.to_json
	  	else
	  		render :json => { :msg => 'Device Already Registered' }.to_json
  		end
  	end
  end

end
