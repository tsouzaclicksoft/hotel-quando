# encoding: utf-8
class Company::BookingsController < Company::BaseController
  has_scope :by_checkin_date, using: [:initial_date, :final_date]
  has_scope :by_pack_length, type: :array
  has_scope :by_hotel_id
  has_scope :by_user_id

  def cancel
    @booking = current_company_company.bookings.find(params[:id])
    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking, agency_cancelation: true)

    if booking_cancellation_response.success
      flash[:notice] = booking_cancellation_response.message
    else
      flash[:error] = booking_cancellation_response.message
    end

    redirect_to company_booking_path(@booking)
  end

  private
    def collection
      @bookings ||= end_of_association_chain.includes(:user).includes(:hotel).includes(:offer).page(params[:page]).per(40)
    end
end
