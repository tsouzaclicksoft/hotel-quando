# encoding: utf-8
class Company::CreditCardsController < Company::BaseController

  def create
    @credit_card = CreditCardCreationSrv.call(credit_card_owner: current_company_company, credit_card_attributes: build_resource_params)
    if @credit_card.persisted?
      flash[:success] = 'Cartão de crédito cadastrado com sucesso.'
      redirect_to company_credit_card_path(@credit_card)
    else
      flash.now[:error] = 'O cartão de crédito não pode ser cadastrado. Por favor, revise o formulário e tente novamente.'
      render :new
    end
  end

  private
    def build_resource_params
      params.fetch(:credit_card, {}).permit(:flag_name,
        :billing_name,
        :billing_address1,
        :billing_city,
        :billing_state,
        :billing_zipcode,
        :billing_country,
        :billing_phone,
        :last_four_digits,
        :expiration_year,
        :expiration_month,
        :owner_cpf,
        :owner_passport,
        :temporary_number )
    end
end
