# encoding: utf-8
class Company::DepartmentsController < Company::BaseController

  private

    def build_resource_params
      [params.fetch(:department, {}).permit(:name)]
    end
end
