# encoding: utf-8
class Company::EmployeesController < Company::BaseController
  defaults :resource_class => User

  def create
    create! do | success, failure |
      success.html do
        UserMailer.send_registration_notice_to_employee(@employee).deliver
        redirect_to company_employee_path(@employee)
      end
    end
  end

  def authenticate_as
    employee = current_company_company.employees.find(params[:id])
    sign_in(:public_user, employee)
    flash['success'] = "Você está acessando o sistema como #{employee.name}."
    redirect_to public_root_path
  rescue ActiveRecord::RecordNotFound
    flash['error'] = 'Você não tem permissões para accessar esta conta!'
    redirect_to company_employees_path
  rescue
    flash['error'] = 'Houve um erro ao acessar a conta, por favor tente novamente.'
    redirect_to company_employees_path
  end

  private
    def collection
      @employees ||= end_of_association_chain.order(:name)
    end

    def build_resource_params
      [params.fetch(:user, {}).permit(:name, :budget_for_24_hours_pack, :cpf, :passport, :department_id, :email, :birth_date, :country, :state, :city, :street, :number, :complement)]
    end
end
