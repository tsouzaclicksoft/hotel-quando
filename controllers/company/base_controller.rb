# encoding: utf-8
class Company::BaseController < ActionController::Base
  protect_from_forgery
  inherit_resources
  layout 'company'
  before_filter :authenticate_company_company!, :set_locale_as_pt_br

  protected

    def begin_of_association_chain
      current_company_company
    end

    def set_locale_as_pt_br
      I18n.locale = :pt_br
    end
end
