# encoding: utf-8
class Public::NewslettersController < Public::BaseController

	def create
		country = params[:city].split(',').last.gsub(/^\s/, "")
    @newsletter = Newsletter.new(email: params[:email], city: params[:city].split(/[-,]/).first, country: (params[:country] || country), emkt_flux: 2, emkt_day_of_week: Date.current.wday, emkt_flux_type: 2)
		if @newsletter.save
			flash['notice'] = "Obrigado por se registrar!"
		else 
			error_message(params[:city], params[:email], country)
      #EmktMailer.register_without_booking(@newsletter).deliver
    end
		#SendUserDataRdsWorker.perform_async(@newsletter.email, nil, @newsletter.city, "Newsletter", nil, country)
		redirect_to public_root_path
	end

	private
		def error_message(city, email, country)
			if email.empty? && city.empty?
				flash['error'] = "E-mail/Cidade Inválidos."
			elsif city.empty?
				flash['error'] = "Cidade Inválida."
			elsif email.empty?
				flash['error'] = "E-mail Inválido."
      elsif country.empty?
        flash['error'] = "País Inválido."
			end
		end
end
