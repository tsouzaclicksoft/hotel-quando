class Public::PasswordsController < Devise::PasswordsController
  layout 'public'
  helper Shared::CurrencyCurrentHelper
  before_filter :set_currency

  def after_resetting_password_path_for(user)
    public_bookings_path
  end

  def set_currency
    if params[:currency]
      cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
    elsif cookies[:currency].nil?
      cookies[:currency] = "US$"
    end
  end
end
