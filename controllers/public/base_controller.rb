# encoding: utf-8
class Public::BaseController < ActionController::Base
  CONTROLLERS_TO_AVOID_LOG_PATH = ['sessions', 'passwords', 'registrations', 'confirmations']
  protect_from_forgery
  layout 'public'
  # before_filter :authenticate_public_user!
  before_filter :log_last_public_path_unless_devise_controllers
  before_filter :redirect_with_locale, except: [:set_locale, :set_currency, :set_currency_by_locale]
  # before_filter :set_locale, except: [:redirect_with_locale] unless Rails.env.test?
  before_filter :set_first_visit
  before_filter :set_currency, except: [:redirect_with_locale]
  helper_method :last_search_log
  helper_method :total_price_for_number_of_people
  helper Shared::CurrencyCurrentHelper
  skip_before_filter :verify_authenticity_token
  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers

  # For all responses in this controller, return the CORS access control headers.
  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.

  def cors_preflight_check
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
    headers['Access-Control-Max-Age'] = '1728000'
  end
  
  def current_public_user
    super
    if @current_public_user.present?
      @current_public_user = User::BudgetDecorator.decorate(@current_public_user)
    end
    @current_public_user
  end

  def set_first_visit
    if cookies[:first_visit].nil?
      @first_visit = true
      cookies.permanent[:first_visit] = 1
    else
      @first_visit = false
    end
  end

  def total_price_for_number_of_people(number_of_people, offer)
    mock_booking = Booking.new
    mock_booking.user = current_public_user
    set_booking_cached_attributes(mock_booking, offer)
    mock_booking.number_of_people = number_of_people
    mock_booking.hotel = offer.hotel
    mock_booking.offer_id = offer.id
    mock_booking.total_price_without_tax
  end

  protected

  def begin_of_association_chain
    current_public_user
  end

  def last_search_log
    begin
      @last_search_log ||= HotelSearchLog.find(session[:current_search_log_id])
    rescue
    end
  end

  private

  def set_booking_cached_attributes(booking, offer)
    booking.cached_room_type_initial_capacity = offer.room_type_initial_capacity
    booking.cached_room_type_maximum_capacity = offer.room_type_maximum_capacity
    booking.cached_offer_price = offer.price
    booking.cached_offer_extra_price_per_person = offer.extra_price_per_person
  end

  # def set_locale
  #   unless action_name == 'timezone'
  #     if params[:locale] == 'pt_br' || params[:lang] == 'pt-br'
  #       cookies[:locale] = params[:locale]
  #     elsif params[:locale] == 'en' || params[:locale] == 'es'
  #       cookies[:locale] = params[:locale]
  #     elsif current_public_user
  #       cookies[:locale] = current_public_user.locale
  #     else
  #       if cookies[:locale].blank?
  #         cookies[:locale] = set_locale_from_header
  #       end
  #     end
  #   end

  #   I18n.locale = I18n.available_locales.include?(cookies[:locale].try(:to_sym)) ? cookies[:locale] : I18n.default_locale
  # end

  def set_locale_and_currency_by_params
    cookies[:currency_selected] = false
    case params[:locale]
    when "es"
      I18n.locale = :es
      cookies[:locale] = I18n.locale
      cookies[:currency] = Currency.where(country: 'CO').first.money_sign if cookies[:currency].nil? || cookies[:currency_selected] != 'true'
    when "pt_br"
      I18n.locale = :pt_br
      cookies[:locale] = I18n.locale
      cookies[:currency] = Currency.where(country: 'BR').first.money_sign if cookies[:currency].nil? || cookies[:currency_selected] != 'true'
    else
      I18n.locale = :en
      cookies[:locale] = I18n.locale
      cookies[:currency] = Currency.where(country: 'US').first.money_sign if cookies[:currency].nil? || cookies[:currency_selected] != 'true'
    end
    params[:currency] = nil if params[:currency].present?
  end

  def set_locale_from_header
    request.env['HTTP_ACCEPT_LANGUAGE'].gsub! 'pt-BR', 'pt_BR' unless request.env['HTTP_ACCEPT_LANGUAGE'].blank?
    return http_accept_language.compatible_language_from(I18n.available_locales) || 'en'
  end

  def log_last_public_path_unless_devise_controllers
    namespace, controller = params[:controller].split("/") # namespace is always 'public'
    unless request.fullpath.include?('/timezone')
      session[:last_path] = request.fullpath unless (CONTROLLERS_TO_AVOID_LOG_PATH.include? controller)
    end
  end

  def set_currency_by_locale
    if cookies[:locale] == :pt_br
      return Currency.where(country: 'BR').first.money_sign
    else
      return Currency.where(country: 'US').first.money_sign
    end
  end

  def set_currency
    if params[:currency]
      cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
      cookies[:currency_selected] = true
    elsif cookies[:currency].nil?
      cookies[:currency] = set_currency_by_locale
    end
  end

  def redirect_with_locale
    url = request.url
    if params[:lang].present?
      set_locale_and_currency_by_params
      params.delete :lang
    elsif url.include?('/es')
      I18n.locale = :es
      cookies[:locale] = I18n.locale
      cookies[:currency] = Currency.where(country: 'CO').first.money_sign  if cookies[:currency].nil? || cookies[:currency_selected] != 'true'
    elsif url.include?('/en')
      I18n.locale = :en
      cookies[:locale] = I18n.locale
      cookies[:currency] = Currency.where(country: 'US').first.money_sign  if cookies[:currency].nil? || cookies[:currency_selected] != 'true' 
    elsif params[:lang].present? || params[:locale] == 'pt_br'
      I18n.locale = :pt_br
      cookies[:locale] = I18n.locale
      cookies[:currency] = Currency.where(country: 'BR').first.money_sign  if cookies[:currency].nil? || cookies[:currency_selected] != 'true'
    else
      if cookies[:locale].nil?
        I18n.locale = :en
        cookies[:locale] = I18n.locale
      end
      I18n.locale = cookies[:locale]
      cookies[:currency] = Currency.where(country: 'US').first.money_sign  if cookies[:currency].nil? || cookies[:currency_selected] != 'true'
    end
  end
end
