# encoding: utf-8
class Public::HomeController < Public::BaseController
  #before_action {flash.clear}
  before_filter :authenticate_public_user!, only: [:friend_hq]
  before_filter :check_if_user_is_a_consumer_and_have_bookings!, only: [:friend_hq]

  def index
    @main_airports = Place.where(place_type: 1).where.not(hotel_count: 0).where(active: true).order('hotel_count DESC').limit(15)
    ids = @main_airports.map(&:id)
    @airports = Place.where(place_type: 1).where.not(hotel_count: 0).where(active: true).where.not(id: ids).order('hotel_count DESC').limit(6)
    @main_cities = Place.where(place_type: 2).where.not(hotel_count: 0).where(active: true).order('hotel_count DESC').limit(15)
    ids = @main_cities.map(&:id)
    @cities = Place.where(place_type: 2).where.not(hotel_count: 0).where(active: true).where.not(id: ids).order('hotel_count DESC').limit(6)
  end

  def timezone
    location = params[:location].split(',')
    lat   = location[0]
    lng   = location[1]
    tmstp = params[:tmstp]

    json = TimezoneSrv.get(lat, lng, tmstp)
    # converting timeoffset including DST into timestamp by miliseconds
    timezone_value  = (json["dstOffset"]+json["rawOffset"]) * 1000
    #response_hash   = {destiny_minimum_time: destiny_minimum_time, timezone_value: timezone_value, timezone_id: json["timeZoneId"], timezone_name: json["timeZoneName"]}
    response_hash   = {timezone_value: timezone_value, timezone_id: json["timeZoneId"], timezone_name: json["timeZoneName"]}

    if response.code == "200"
      render :json => { :msg => response_hash }.to_json
    else
      render :json => { :msg => response.body }.to_json
    end

  end

  def friend_hq
    render "public/home/friend_hq/#{I18n.locale}"
  end
  
  def indicate_and_earn
    @user = User.find(params[:id])
    @promotional_code = PromotionalCode.where(code: @user.refer_code).first
    if @promotional_code.present?
      render "public/home/indicate_and_earn/#{I18n.locale}"
    else
      redirect_to public_root_path
    end
  end

  def about_us
    render "public/home/about_us/#{I18n.locale}"
  end

  def how_it_works
    render "public/home/how_it_works/#{I18n.locale}"
  end
  
  def hours_discount
    if Rails.env == "development" || Rails.env == "staging"
      @sao_paulo = Hotel.last(9)
      @rio_de_janeiro = Hotel.last(5)
      @curitiba = Hotel.last(1)
      @florianopolis = Hotel.last(3)
      @goiania = Hotel.last(3)
      @brasilia = Hotel.last(4)
      @salvador = Hotel.last(2)
      @belo_horizonte = Hotel.last(7)
      @campinas = Hotel.last(6)
      @porto_alegre = Hotel.last(2)
    else
      @sao_paulo = Hotel.where(id: [1102,268,338,980,978])
      @rio_de_janeiro = Hotel.where(id: [161,805,156,659])
      @curitiba = Hotel.where(id: [908,654,1248,1220,569])
      @florianopolis = Hotel.where(id: [909])
      @goiania = Hotel.where(id: [910,640,16,969])
      @brasilia = Hotel.where(id: [754,906])
      @salvador = Hotel.where(id: [1059,1125,924,1104,1072,1068,1035])
      @belo_horizonte = Hotel.where(id: [122,903,651,1106,172,373])
      @campinas = Hotel.where(id: [542,999,159])
      @porto_alegre = Hotel.where(id: [851,1103,108,913,912])
    end
    render "public/home/hours_discount/#{I18n.locale}"
  end

  def black_friday
    if Rails.env == "development" || Rails.env == "staging"
      @sao_paulo = Hotel.last(9)
      @rio_de_janeiro = Hotel.last(5)
      @curitiba = Hotel.last(1)
      @florianopolis = Hotel.last(3)
      @goiania = Hotel.last(3)
      @brasilia = Hotel.last(4)
      @salvador = Hotel.last(2)
      @belo_horizonte = Hotel.last(7)
      @campinas = Hotel.last(6)
      @porto_alegre = Hotel.last(2)
    else
      @sao_paulo = Hotel.where(id: [1102,268,338,980,978])
      @rio_de_janeiro = Hotel.where(id: [161,805,156,659])
      @curitiba = Hotel.where(id: [908,654,1248,1220,569])
      @florianopolis = Hotel.where(id: [909])
      @goiania = Hotel.where(id: [910,640,16,969])
      @brasilia = Hotel.where(id: [754,906])
      @salvador = Hotel.where(id: [1059,1125,924,1104,1072,1068,1035])
      @belo_horizonte = Hotel.where(id: [122,903,651,1106,172,373])
      @campinas = Hotel.where(id: [542,999,159])
      @porto_alegre = Hotel.where(id: [851,1103,108,913,912])
    end
    render "public/home/black_friday/#{I18n.locale}"
  end

 def summer
  if Rails.env == "development" || Rails.env == "staging"
    @sao_paulo = Hotel.where(id: [262,726,728,727,938,933,317,935,663,295,314,934,338,664,980,342])
    @rio_de_janeiro = Hotel.where(id: [161,805,156,659])
    @florianopolis = Hotel.where(id: [909])
    @goiania = Hotel.where(id: [910,640,16,969])
    @brasilia = Hotel.where(id: [754,906])
    @salvador = Hotel.where(id: [1059,1125,924,1104,1072,1068,1035])
    @belo_horizonte = Hotel.where(id: [121,903,650,370,363])
    @campinas = Hotel.where(id: [542,999,159])
    @porto_alegre = Hotel.where(id: [851,1103,108,913,912])
  else
    @sao_paulo = Hotel.where(id: [262,726,728,727,938,933,317,935,663,295,314,934,338,664,980,342])
    @rio_de_janeiro = Hotel.where(id: [161,805,156,659])
    @curitiba = Hotel.where(id: [908,654,1248,1220,569])
    @florianopolis = Hotel.where(id: [909])
    @goiania = Hotel.where(id: [910,640,16,969])
    @brasilia = Hotel.where(id: [754,906])
    @salvador = Hotel.where(id: [1059,1125,924,1104,1072,1068,1035])
    @belo_horizonte = Hotel.where(id: [121,903,650,370,363])
    @campinas = Hotel.where(id: [542,999,159])
    @porto_alegre = Hotel.where(id: [851,1103,108,913,912])
  end
  render "public/home/summer/#{I18n.locale}"
end


  def twelve_hours_discount
    render "public/home/twelve_hours_discount/#{I18n.locale}"
  end

  def app_advice
    render "public/home/app_advice/#{I18n.locale}"
  end

  def terms_of_use
    render "public/home/terms_of_use/#{I18n.locale}"
  end

  def faq
    render "public/home/faq/#{I18n.locale}"
  end

  def get_booking_packs
    @booking_packs = BookingPack.where(hotel: params[:by_hotel_id])
  end

  def booking_pack_contracting_company
    begin
      booking_pack_contracting_company = BookingPackContractingCompany.new
      booking_pack = BookingPack.find(params[:booking_pack_id])
      booking_pack_contracting_company.booking_pack_id = params[:booking_pack_id]
      booking_pack_contracting_company.name = params[:name]
      booking_pack_contracting_company.cnpj = params[:cnpj]
      booking_pack_contracting_company.responsible_name = params[:responsible_name]
      booking_pack_contracting_company.phone_number = params[:phone_number]
      booking_pack_contracting_company.role = params[:role]
      booking_pack_contracting_company.email = params[:email]
      booking_pack_contracting_company.cached_room_type_id = booking_pack.room_type_id
      booking_pack_contracting_company.cached_booking_pack_bank_of_hours = booking_pack.bank_of_hours
      booking_pack_contracting_company.cached_booking_pack_number_of_pack_12h = booking_pack.number_of_pack_12h
      booking_pack_contracting_company.cached_booking_pack_price = booking_pack.price
      if booking_pack_contracting_company.save
        flash[:success] = t(:contracting_company_success_message)
      else
        flash[:error] = t(:we_could_not_send_your_data_please_try_again)
      end
    rescue
      flash[:error] = t(:contracting_company_error_message)
    end
    redirect_to :back
  end

  def business_trips
    if request.post?
      begin
        contact = PossiblePartnerContact.new
        contact.name = params[:company_name]
        contact.contact_name = params[:contact_name]
        contact.contact_role = params[:contact_role]
        contact.phone = params[:phone]
        contact.email_for_contact = params[:email_for_contact]
        contact.message = params[:message]
        contact.type_of_contact = PossiblePartnerContact::TYPES_OF_CONTACT[:company_contact]
        contact.locale = I18n.locale
        contact.save!
        flash.now[:success] = t(:message_sent_successfully)
        params.delete(:company_name)
        params.delete(:contact_name)
        params.delete(:contact_role)
        params.delete(:phone)
        params.delete(:email_for_contact)
        params.delete(:message)
      rescue
        flash.now[:error] = t(:we_could_not_send_your_message_please_try_again)
      end
    end
    render "public/home/business_trips/#{I18n.locale}"
  end

  def be_our_partner
    if request.post?
      begin
        contact = PossiblePartnerContact.new
        contact.name = params[:hotel_name]
        contact.contact_name = params[:contact_name]
        contact.contact_role = params[:contact_role]
        contact.phone = params[:phone]
        contact.email_for_contact = params[:email_for_contact]
        contact.message = params[:message]
        contact.message = params[:message]
        contact.country = params[:country]
        contact.city = params[:city]
        contact.id_skype = params[:id_skype]
        # contact.terms_of_use = params[:terms_of_use]
        contact.type_of_contact = PossiblePartnerContact::TYPES_OF_CONTACT[:hotel_contact]
        contact.locale = I18n.locale
        contact.save!
        flash.now[:success] = t(:message_sent_successfully)
        params.delete(:hotel_name)
        params.delete(:contact_name)
        params.delete(:contact_role)
        params.delete(:phone)
        params.delete(:email_for_contact)
        params.delete(:country)
        params.delete(:city)
        params.delete(:id_skype)
        # params.delete(:terms_of_use)
      rescue
        flash.now[:error] = t(:we_could_not_send_your_message_please_try_again)
      end
    end
    render "public/home/be_our_partner/#{I18n.locale}"
  end

  def privacy_policy
    render "public/home/privacy_policy/#{I18n.locale}"
  end

  def customer_service
    if current_public_user.blank? || current_public_user.is_a_consumer?
      render "public/home/customer_service/#{I18n.locale}"
    else
      redirect_to public_customer_service_corp_path
    end
  end
  def customer_service_corp
    if current_public_user.present? && !current_public_user.is_a_consumer?
      render "public/home/customer_service_corp/#{I18n.locale}"
    else
      redirect_to public_customer_service_path
    end
  end
  def tudo_azul
    render "public/home/tudo_azul/#{I18n.locale}"
  end

  def multiplus
    render "public/home/multiplus/#{I18n.locale}"
  end

  private

  def check_if_user_is_a_consumer_and_have_bookings!
    unless current_public_user.is_a_consumer? && current_public_user.have_any_confirmed_booking?
      redirect_to public_root_path
    end
  end

  # def uber
  #   render "public/home/uber/#{I18n.locale}"
  # end

  # def brahma
  #   render "public/home/brahma/#{I18n.locale}"
  # end
end
