# encoding: utf-8
class Public::CompanyDetails::JobTitlesController < Public::BaseController
  inherit_resources
  actions :show, :new, :create, :edit, :update, :index
  before_filter :authenticate_public_user!
  before_filter :authenticate_agency_or_company_detail!

  def index
    @job_titles = current_public_user.company_detail.job_titles
  end

  def new
    # inserir no caso de ser uma company
    @company_detail = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(params[:company_detail_id]) : current_public_user.company_detail
    if @company_detail.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_company_detail_job_titles_path
      new!
    end
  end
  def create
    @company_detail = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(params[:company_detail_id]) : current_public_user.company_detail
    prepare_params
    if @company_detail.blank? 
      redirect_to public_company_details_path
    else
      @job_title = JobTitle.new()
      @job_title.company_detail_id = @company_detail.id
      @job_title.name = params[:job_title][:name]
      @job_title.budget_value = params[:job_title][:budget_value]
      @job_title.money_sign = params[:job_title][:money_sign]
      @url_form = public_company_detail_job_titles_path
      create! do |success, failure|
        success.html do
          redirect_to public_job_title_path(@job_title)
        end
      end
    end
  end

  def edit
    @job_title = JobTitle.find(params[:id])
    @company_detail = @job_title.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_job_title_path(@job_title)
    end
  end

  def update
    @job_title = JobTitle.find(params[:id])
    @company_detail = @job_title.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    prepare_params
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @url_form = public_job_title_path(@job_title)
      update! do |success, failure|
        success.html do
          redirect_to public_job_title_path(@job_title)
        end
      end
    end
  end

  def show
    prepare_params
    @job_title = JobTitle.find(params[:id])
    @company_detail = @job_title.company_detail
    company_belongs_to_current_agency = current_public_user.is_a_agency? ? current_public_user.agency_detail.company_details.find_by_id(@company_detail) : current_public_user.company_detail
    if company_belongs_to_current_agency.blank?
      redirect_to public_company_details_path
    else
      @travelers = @job_title.travelers
    end
  end

  private
    def build_resource_params
      [params.fetch(:job_title, {}).permit!]
    end

    def authenticate_agency_or_company_detail!
      if !current_public_user.is_a_agency? && !current_public_user.is_a_company?
        redirect_to public_root_path
      end
    end

  def prepare_params
    begin
      params[:job_title][:budget_value].gsub!('.','')
      params[:job_title][:budget_value].gsub!(',','.')
      params[:job_title][:name] = params[:job_title][:name].titleize
    rescue
    end
  end

end
