# encoding: utf-8
class Agency::BaseController < ActionController::Base
  protect_from_forgery
  inherit_resources
  layout 'agency'
  before_filter :authenticate_agency_agency!, :set_locale_as_pt_br

  protected

  def begin_of_association_chain
    current_agency_agency
  end

  def set_locale_as_pt_br
    # Doing this because the agency can go to public pages, change the locale to en and come back to the agency pages
    # and if he do that, some texts will appear in english and others in portuguese
    I18n.locale = :pt_br
  end
end
