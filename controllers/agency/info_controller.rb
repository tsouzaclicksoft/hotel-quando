# encoding: utf-8
class Agency::InfoController < Agency::BaseController
  actions :none

  def index
  end

  def edit
  end

  def update_password
    if current_agency_agency.update_with_password(agency_params)
      sign_in(current_agency_agency, :bypass => true)
      flash[:success] = "Senha modificada com sucesso."
      redirect_to agency_root_path
    else
      render "edit"
    end

  end

  private

  def agency_params
    params.required(:agency).permit(:password, :password_confirmation, :current_password)
  end

end
