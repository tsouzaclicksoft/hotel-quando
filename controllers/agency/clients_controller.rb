class Agency::ClientsController < Agency::BaseController

  defaults :resource_class => Company

  def create
    prepare_params
    build_resource
    create! do | success, failure |
      success.html do
        CompanyMailer.send_login_password(@client).deliver
        redirect_to agency_client_path(@client.id)
      end
    end
  end

  def update
    prepare_params
    update!
  end


  def authenticate_as_client
    company = current_agency_agency.clients.find(params[:id])
    sign_in(:company_company, company)
    flash['success'] = "Você está acessando o sistema como #{company.name}."
    redirect_to company_root_path
  rescue ActiveRecord::RecordNotFound
    flash['error'] = 'Você não tem permissões para accessar esta conta!'
    redirect_to agency_clients_path
  rescue
    flash['error'] = 'Houve um erro ao acessar a conta, por favor tente novamente.'
    redirect_to agency_clients_path
  end

  def authenticate_as_client_employee
    company = current_agency_agency.clients.find(params[:id])
    company_employee = company.employees.find(params[:employee_id])
    sign_in(:public_user, company_employee)
    flash['success'] = "Você está acessando o sistema como #{company.name}."
    redirect_to public_root_path
  rescue ActiveRecord::RecordNotFound
    flash['error'] = 'Você não tem permissões para accessar esta conta!'
    redirect_to agency_clients_path
  rescue
    flash['error'] = 'Houve um erro ao acessar a conta, por favor tente novamente.'
    redirect_to agency_clients_path
  end

  protected
    def prepare_params
      params[:client][:markup_value].gsub!('R$ ', ' ')
      params[:client][:markup_value].gsub!(',', '.')
    end

    def build_resource_params
      [params.fetch(:client, {}).permit(:name, :responsible_name, :email, :cnpj, :country, :state, :city, :street, :number, :complement, :phone_number, :login, :accept_confirmed_and_invoiced, :accept_confirmed_without_card, :markup, :markup_value )]
    end

    def collection
      @clients ||= end_of_association_chain.page(params[:page]).per(40)
    end
end
