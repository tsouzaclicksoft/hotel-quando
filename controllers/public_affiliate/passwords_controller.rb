class PublicAffiliate::PasswordsController < Devise::PasswordsController
  layout 'public_affiliate'
  helper Shared::CurrencyCurrentHelper

  def after_resetting_password_path_for(user)
    public_affiliate_bookings_path
  end

end
