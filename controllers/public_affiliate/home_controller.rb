# encoding: utf-8
class PublicAffiliate::HomeController < PublicAffiliate::BaseController

  def index
    if @affiliate.hotel_chain.present?
      @affiliate_hotels = hash_hotels(@affiliate.hotel_chain.hotels) if @affiliate.hotel_chain.hotels.present?
      render :meeting_rooms if has_only_meeting_rooms?
    else
      @main_airports = Place.where(place_type: 1).where.not(hotel_count: 0).where(active: true).order('hotel_count DESC').limit(15)
      ids = @main_airports.map(&:id)
      @airports = Place.where(place_type: 1).where.not(hotel_count: 0).where(active: true).where.not(id: ids).order('hotel_count DESC').limit(6)
      @main_cities = Place.where(place_type: 2).where.not(hotel_count: 0).where(active: true).order('hotel_count DESC').limit(15)
      ids = @main_cities.map(&:id)
      @cities = Place.where(place_type: 2).where.not(hotel_count: 0).where(active: true).where.not(id: ids).order('hotel_count DESC').limit(6)
    end
  end

  def timezone
    location = params[:location].split(',')
    lat   = location[0]
    lng   = location[1]
    tmstp = params[:tmstp]

    json = TimezoneSrv.get(lat, lng, tmstp)
    # converting timeoffset including DST into timestamp by miliseconds
    timezone_value  = (json["dstOffset"]+json["rawOffset"]) * 1000
    #response_hash   = {destiny_minimum_time: destiny_minimum_time, timezone_value: timezone_value, timezone_id: json["timeZoneId"], timezone_name: json["timeZoneName"]}
    response_hash   = {timezone_value: timezone_value, timezone_id: json["timeZoneId"], timezone_name: json["timeZoneName"]}

    if response.code == "200"
      render :json => { :msg => response_hash }.to_json
    else
      render :json => { :msg => response.body }.to_json
    end

  end

  def about_us
  	render "public_affiliate/home/about_us/#{I18n.locale}"
  end

  def how_it_works
  	render "public_affiliate/home/how_it_works/#{I18n.locale}"
  end

  def terms_of_use
    render "public_affiliate/home/terms_of_use/#{I18n.locale}"
  end

  def faq
    render "public_affiliate/home/faq/#{I18n.locale}"
  end

  def business_trips
    if request.post?
      begin
        contact = PossiblePartnerContact.new
        contact.name = params[:company_name]
        contact.contact_name = params[:contact_name]
        contact.contact_role = params[:contact_role]
        contact.phone = params[:phone]
        contact.email_for_contact = params[:email_for_contact]
        contact.message = params[:message]
        contact.type_of_contact = PossiblePartnerContact::TYPES_OF_CONTACT[:company_contact]
        contact.locale = I18n.locale
        contact.save!
        flash.now[:success] = t(:message_sent_successfully)
        params.delete(:company_name)
        params.delete(:contact_name)
        params.delete(:contact_role)
        params.delete(:phone)
        params.delete(:email_for_contact)
        params.delete(:message)
      rescue
        flash.now[:error] = t(:we_could_not_send_your_message_please_try_again)
      end
    end
    render "public_affiliate/home/business_trips/#{I18n.locale}"
  end

  def be_our_partner
    if request.post?
      begin
        contact = PossiblePartnerContact.new
        contact.name = params[:hotel_name]
        contact.contact_name = params[:contact_name]
        contact.contact_role = params[:contact_role]
        contact.phone = params[:phone]
        contact.email_for_contact = params[:email_for_contact]
        contact.message = params[:message]
        contact.type_of_contact = PossiblePartnerContact::TYPES_OF_CONTACT[:hotel_contact]
        contact.locale = I18n.locale
        contact.save!
        flash.now[:success] = t(:message_sent_successfully)
        params.delete(:hotel_name)
        params.delete(:contact_name)
        params.delete(:contact_role)
        params.delete(:phone)
        params.delete(:email_for_contact)
        params.delete(:message)
      rescue
        flash.now[:error] = t(:we_could_not_send_your_message_please_try_again)
      end
    end
    redirect_to public_affiliate_root_path
  end

  def privacy_policy
    render "public_affiliate/home/privacy_policy/#{I18n.locale}"
  end

  def customer_service
    render "public_affiliate/home/customer_service/#{I18n.locale}"
  end

  def tudo_azul
    render "public_affiliate/home/tudo_azul/#{I18n.locale}"
  end

  def multiplus
    render "public_affiliate/home/multiplus/#{I18n.locale}"
  end

  # def uber
  #   render "public_affiliate/home/uber/#{I18n.locale}"
  # end

  # def brahma
  #   render "public_affiliate/home/brahma/#{I18n.locale}"
  # end

  def meeting_rooms
    if @affiliate.hotel_chain.present?
      @affiliate_hotels = {}
      @affiliate.hotel_chain.hotels.each_with_index do |hotel, i|
        if hotel_has_meeting_room_offers?(hotel.id)
          @affiliate_hotels["cidade_#{i}"] = hotel.city.name + '-' + hotel.state
          @affiliate_hotels["hotel_name_#{i}"] = hotel.name
        else
          @affiliate_hotels["cidade_#{i}"] = ''
          @affiliate_hotels["hotel_name_#{i}"] = ''
        end
      end
    end
  end

  private
    def hotel_has_offers?(hotel_id)
      @hotel_search = OffersSearch::HotelChain.new({
        number_of_guests: 1,
        hotel_ids: hotel_id,
        check_in_datetime: DateTime.tomorrow + 2.hours,
        room_type: 1,
        pack_in_hours: 3
        })
      !(@hotel_search.results.exacts.empty?)
    end

    def hotel_has_meeting_room_offers?(hotel_id)
      @hotel_search = OffersSearch::HotelChain.new({
         number_of_guests: 1,
         hotel_ids: hotel_id,
         check_in_datetime: DateTime.tomorrow + 2.hours,
         room_category: 2,
         is_affiliate: true,
         pack_in_hours: 3
       })
      !(@hotel_search.results.exacts.empty?)
    end

    def has_only_meeting_rooms?
      (@affiliate.only_meeting_room)
    end

    def hash_hotels(hotels)
      hotels_hsh = {}
      hotels.each_with_index do |hotel, i|
        hotels_hsh["cidade_#{i}"] = hotel.city.name + '-' + hotel.state
        hotels_hsh["hotel_name_#{i}"] = hotel.name
      end
      hotels_hsh
    end
    
    def meeting_room_hotels(hotels)
      hotels_hsh = {}
      hotels.each_with_index do |hotel, i|
        if hotel_has_meeting_room_offers?(hotel.id)
          hotels_hsh["cidade_#{i}"] = hotel.city.name + '-' + hotel.state
          hotels_hsh["hotel_name_#{i}"] = hotel.name
        else
          hotels_hsh["cidade_#{i}"] = ''
          hotels_hsh["hotel_name_#{i}"] = ''
        end
      end
      hotels_hsh
    end

    def rooms_hotels(hotels)
      hotels_hsh = {}
      hotels.each_with_index do |hotel, i|
        if hotel.is_omnibees?
          hotels_hsh["cidade_#{i}"] = hotel.city.name + '-' + hotel.state
          hotels_hsh["hotel_name_#{i}"] = hotel.name
        elsif hotel_has_offers?(hotel.id)
          hotels_hsh["cidade_#{i}"] = hotel.city.name + '-' + hotel.state
          hotels_hsh["hotel_name_#{i}"] = hotel.name
        else
          hotels_hsh["cidade_#{i}"] = ''
          hotels_hsh["hotel_name_#{i}"] = ''
        end
      end
      hotels_hsh
    end
end
