# encoding: utf-8
class PublicAffiliate::BookingsController < PublicAffiliate::BaseController
  include ActionView::Helpers::NumberHelper
  include PublicAffiliate::UserHelper
  include Shared::CurrencyCurrentHelper

  inherit_resources
  actions :index, :update
  before_filter :authenticate_public_affiliate_user!
  has_scope :by_status
  helper_method :total_price_for_number_of_people_dropdpwn

  def cancel
    @booking = current_public_affiliate_user.bookings.find(params[:id])

    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking)
    if booking_cancellation_response.success
      flash[:notice] = booking_cancellation_response.message
    else
      flash[:error] = booking_cancellation_response.message
    end

    redirect_to public_affiliate_bookings_path
  end


  def new
    @booking = Booking.new(number_of_people: params[:number_of_people])
    @business_room = BusinessRoom.find_by id: (params[:business_room_id])
    if @business_room
      business_type = @business_room.type
=begin # code working meeting room and event room
      offer_class = (business_type + "Offer").constantize
      @offer = offer_class.where("#{business_type.foreign_key} = #{params[:business_room_id]}")
                          .where(checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                             pack_in_hours: params[:pack_in_hours], price: params[:price], \
                             status: offer_class::ACCEPTED_STATUS[:available]).first
=end

      @offer = MeetingRoomOffer.where(meeting_room_id: params[:business_room_id])
                          .where(checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                             pack_in_hours: params[:pack_in_hours], price: params[:price], \
                             status: MeetingRoomOffer::ACCEPTED_STATUS[:available]).first
    else
      @offer = OmnibeesOffer.where(room_type_id: params[:room_type_id], \
                             checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                             pack_in_hours: params[:pack_in_hours], price: params[:price]).first
      @offer ||= Offer.where(room_type_id: params[:room_type_id], \
                             checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                             pack_in_hours: params[:pack_in_hours], price: params[:price], \
                             status: Offer::ACCEPTED_STATUS[:available]).first
    end

    if @offer && (@offer.available?)
      @booking.offer = @offer
      set_current_offer_id @offer.id
      set_current_offer_type @offer.class.name
      set_current_offer_price @offer.price
    else
      flash[:error] = t(:error_message_offer_not_available)
      redirect_to public_affiliate_hotel_path(params.merge(id: session[:last_hotel_visited_id]))
    end
  end

  def create
    begin
      @booking = Booking.new(params[:booking].permit(:guest_name, :number_of_people, :note, :guest_email, :have_promotional_code, :consultant_id, :traveler_id, :phone_number))
      @offer = get_current_offer_type.constantize.find get_current_offer_id

      if current_public_affiliate_user.need_to_complete_address?
        current_public_affiliate_user.update(country: params[:public_affiliate_user][:country], state: params[:public_affiliate_user][:state], city: params[:public_affiliate_user][:city], street: params[:public_affiliate_user][:street], postal_code: params[:public_affiliate_user][:postal_code])
        current_public_affiliate_user.errors.add(:country, I18n.t('errors.messages.blank')) if current_public_affiliate_user.country.blank?
        current_public_affiliate_user.errors.add(:state, I18n.t('errors.messages.blank')) if current_public_affiliate_user.state.blank?
        current_public_affiliate_user.errors.add(:city, I18n.t('errors.messages.blank')) if current_public_affiliate_user.city.blank?
        current_public_affiliate_user.errors.add(:street, I18n.t('errors.messages.blank')) if current_public_affiliate_user.street.blank?
        current_public_affiliate_user.errors.add(:postal_code, I18n.t('errors.messages.blank')) if current_public_affiliate_user.postal_code.blank?
      end

      if (current_public_affiliate_user.country == "BR" || current_public_affiliate_user.country.blank?) && current_public_affiliate_user.cpf.blank? && @booking.traveler.blank?
        current_public_affiliate_user.update(cpf: params[:public_affiliate_user][:cpf])
        current_public_affiliate_user.errors.add(:cpf, I18n.t('errors.messages.blank')) if current_public_affiliate_user.cpf.blank?
      end

      if current_public_affiliate_user.errors.present?
        render :new
      else
        @booking.user_id = current_public_affiliate_user.id
        @booking.hotel_comments = @offer.hotel.send("description_#{I18n.locale}")
        @booking.pack_in_hours = @offer.pack_in_hours
        @booking.status = Booking::ACCEPTED_STATUSES[:waiting_for_payment]
        @booking.created_by_agency = false
        @booking.checkin_date = @offer.checkin_timestamp
        @booking.affiliate_id = session[:affiliate].id
        @booking.created_by_affiliate = true
        @booking.booking_tax = 0
        @booking.currency_code = current_currency.code
        @booking.is_app_mobile = false

        booking_create_response = BookingMaker.call(offer: @offer, booking: @booking)

        if booking_create_response.success
          redirect_to new_public_affiliate_booking_payments_path(booking_create_response.booking.id)
        elsif booking_create_response.message
          #SendUserDataRdsWorker.perform_async(current_public_affiliate_user.id, @offer.hotel.name, @offer.hotel.city.name, "Afiliado - Reserva - Venda Não Concluída (Erro: #{booking_create_response.message})")
          flash[:error] = booking_create_response.message
          redirect_to public_affiliate_root_path
        else
          render :new
        end
      end
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      flash[:error] = t(:message_booking_creation_error)
      redirect_to public_affiliate_root_path
    end
  end

  def show
    @booking = current_public_affiliate_user.bookings.find(params[:id])
    if request.get?
      if @booking.blank?
        flash[:error] = t(:sorry_but_this_reservation_wasnt_found_among_your_reservations)
        redirect_to public_affiliate_bookings_path
      end
    elsif request.post?
      if (@booking.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed])
        if @booking.status != Booking::ACCEPTED_STATUSES[:waiting_for_payment]
          params[:booking].delete(:number_of_people)
        else
          params[:booking][:number_of_people] = params[:booking][:number_of_people].to_i + 1
        end
        @booking.merge params[:booking] # look merge method in Booking
        if @booking.save
          flash[:success] = t(:booking_successfully_edited)
        else
          flash[:error] = t(:booking_error_editing)
        end
      end
      redirect_to public_affiliate_booking_path(@booking.id)
    else
      redirect_to new_public_affiliate_booking_path
    end
  end

  def edit
    @booking = current_public_affiliate_user.bookings.find_by_id(params[:id])
    booking_editable = ((@booking.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_without_card]) ) && (@booking.offer.checkin_timestamp > Time.now.strftime("&Y/%m/%d %H:%M +0000").to_datetime)
    unless booking_editable
      flash[:error] = t(:you_cant_edit_this_booking)
      redirect_to public_affiliate_bookings_path
    end
  end

  def update
    # @booking = current_public_affiliate_user.bookings.find_by_id(params[:id])
    # @offer = @booking.offer
    update! do | success, failure |
      success.html do
        flash[:notice] = t(:booking_edited_successfully)
        redirect_to public_affiliate_booking_path(params[:id])
      end
    end

  end

  def total_price_for_number_of_people_dropdpwn(number_of_people)
    mock_booking = Booking.new
    mock_booking.user = current_public_affiliate_user
    set_booking_cached_prices(mock_booking)
    mock_booking.number_of_people = number_of_people
    mock_booking.hotel = @offer.hotel
    mock_booking.offer_id = @offer.id
    mock_booking.total_price_without_tax
  end

  protected
    def collection
      # TODO - FIXME
      # includes :offer doesn't work because rails doesn't work well with
      # big int columns
      @bookings = apply_scopes(Booking).where(user_id: current_public_affiliate_user.id).includes(:hotel).order('created_at DESC')
    end

    def begin_of_association_chain
      current_public_affiliate_user
    end

  private

  def set_booking_cached_prices(booking)
    booking.cached_room_type_initial_capacity = @offer.room_type_initial_capacity
    booking.cached_room_type_maximum_capacity = @offer.room_type_maximum_capacity
    booking.cached_offer_price = @offer.price
    booking.cached_offer_extra_price_per_person = @offer.extra_price_per_person
  end

  def get_current_offer_id
    session[:current_offer_id] || nil
  end

  def set_current_offer_id id
    session[:current_offer_id] = id
  end


  def get_current_offer_type
    session[:current_offer_type] || nil
  end

  def set_current_offer_type type
    session[:current_offer_type] = type
  end

  def current_offer_price
    session[:current_offer_price] || nil
  end

  def set_current_offer_price price
    session[:current_offer_price] = price
  end

  def build_resource_params
    [params.fetch(:booking, {}).permit("guest_name", "note", "affiliate_id", "created_by_affiliate", "have_promotional_code", "phone_number")]
  end
end
