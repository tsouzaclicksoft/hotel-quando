# encoding: utf-8
class PublicAffiliate::HotelController < PublicAffiliate::BaseController
  respond_to :js
  class CheckinDateIsInThePastError < StandardError; end
  class OfferNotAvailable < StandardError; end

  def index
    if params[:hotel_id].present?
      hotel = Hotel.find(params[:hotel_id])
      redirect_to public_affiliate_hotel_path(id: hotel.id, name: hotel.name.parameterize, length_of_pack: params[:by_pack_length], checkin_date: params[:by_checkin_date], checkin_hour: params[:by_checkin_hour], number_of_people: params[:by_number_of_people])
    else
      if @affiliate.hotel_chain.present?
        begin
          checkin_datetime = "#{params[:by_checkin_date]} #{params[:by_checkin_hour]}:00 -03:00".to_datetime
          if params[:"#{@affiliate.layout_name.downcase}"].present? && Hotel.where(name: params[:"#{@affiliate.layout_name.downcase}"][:hotel_name]).present?
            affiliate_result = Hotel.where(name: params[:"#{@affiliate.layout_name.downcase}"][:hotel_name]).first
            pack_in_hours = params[:by_pack_length].to_i
            @affiliate_hotel_search = OffersSearch::Hotels.new({
                             number_of_guests: params[:by_number_of_people].to_i,
                             latitude: affiliate_result.latitude,
                             longitude: affiliate_result.longitude,
                             check_in_datetime: checkin_datetime,
                             room_category: params[:room_category],
                             pack_in_hours: params[:by_pack_length].to_i
                            })
            @hotel_search = @affiliate_hotel_search.results.exacts.reject{|hotel| !(@affiliate.hotel_chain.hotels.include?(hotel[:hotel])) }
            @search_exact_match =  @hotel_search.sort_by { |s|
                offer = s[:min_offer]
                offer.save! if offer.is_omnibees?
                offer.reload
                s[:min_offer].price
              }
            @search_similar_match = @affiliate_hotel_search.results.similars.sort_by { |s|
              offer = s[:min_offer]
              offer.save! if offer.is_omnibees?
              [ offer.checkin_timestamp,
                offer.get_number_of_common_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)),
                (offer.checkin_timestamp - checkin_datetime),
                -(offer.get_number_of_exceeding_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)))
              ]
            }
          elsif params[:"#{@affiliate.layout_name.downcase}"].present? && City.where(name: params[:"#{@affiliate.layout_name.downcase}"][:hotel_name].gsub(/-[A-Z]{2}/, '')).present?
            affiliate_result = City.where(name: params[:"#{@affiliate.layout_name.downcase}"][:hotel_name].gsub(/-[A-Z]{2}/, '')).first
            latitude = get_coordinates(affiliate_result.name)["lat"]
            longitude = get_coordinates(affiliate_result.name)["lng"]
            pack_in_hours = params[:by_pack_length].to_i
            @affiliate_hotel_search = OffersSearch::Hotels.new({
              number_of_guests: params[:by_number_of_people].to_i,
              latitude: latitude,
              longitude: longitude,
              check_in_datetime: checkin_datetime,
              check_in_date: checkin_date,
              room_category: params[:room_category],
              pack_in_hours: params[:by_pack_length].to_i
            })
            @hotel_search = @affiliate_hotel_search.results.exacts.reject{|hotel| !(@affiliate.hotel_chain.hotels.include?(hotel[:hotel])) }
            if @hotel_search
              @search_exact_match = @hotel_search.sort_by { |s|
                  offer = s[:min_offer]
                  offer.save! if offer.is_omnibees?
                  offer.reload
                  s[:min_offer].price
                }

              @search_similar_match = @affiliate_hotel_search.results.similars.sort_by { |s|
                offer = s[:min_offer]
                offer.save! if offer.is_omnibees?
                [ offer.checkin_timestamp,
                  offer.get_number_of_common_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)),
                  (offer.checkin_timestamp - checkin_datetime),
                  -(offer.get_number_of_exceeding_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)))
                ]
              }
            else
              @search_exact_match = []
              @search_similar_match = []
            end
          else
            @hotel_search = OffersSearch::HotelChain.new({
               number_of_guests: params[:by_number_of_people].to_i,
               hotel_ids: @affiliate.hotel_chain.hotels.map(&:id),
               check_in_datetime: checkin_datetime,
               room_category: params[:room_category],
               pack_in_hours: params[:by_pack_length].to_i
             })
            @search_exact_match = @hotel_search.results.exacts
            @search_similar_match = @hotel_search.results.similars
          end
        rescue CheckinDateIsInThePastError
          flash[:error] = t(:error_message_selected_checkin_datetime_is_in_the_past)
          redirect_to public_affiliate_root_path
        rescue => ex
          Rails.logger.error(ex)
          Airbrake.notify(ex)
          flash[:error] = t(:message_checkin_date_and_address_error)
          redirect_to public_affiliate_root_path
        end
      else
        if @affiliate.hotel_id.present? || params[:hotel_id].present?
          hotel_id = @affiliate.hotel || params[:hotel_id]
          hotel = Hotel.find(hotel_id)
          save_hotel_search_log
          session[:current_search_log_id] = @log.id
          redirect_to public_affiliate_hotel_path(id: hotel.id, name: hotel.name.parameterize, length_of_pack: params[:by_pack_length], checkin_date: params[:by_checkin_date], checkin_hour: params[:by_checkin_hour], number_of_people: params[:by_number_of_people])
        else
          save_hotel_search_log
          session[:current_search_log_id] = @log.id
        end

        @hotels = []
        begin
          if params[:by_timezone].blank?
            check_in_datetime = "#{params[:by_checkin_date]} #{params[:by_checkin_hour]}:00".to_datetime
            timezone = ''
          else
            check_in_datetime = "#{params[:by_checkin_date]} #{params[:by_checkin_hour]}:00".in_time_zone(params[:by_timezone]).to_datetime
            timezone = params[:by_timezone]
          end
          raise CheckinDateIsInThePastError if check_in_datetime < (DateTime.now)
          @searched_address_coordinates = [params[:by_latitude], params[:by_longitude]]
          @any_available_voucher = EasyTaxiVoucher.available_on(check_in_datetime).any?
          @hotel_search = OffersSearch::Hotels.new({
            number_of_guests: params[:by_number_of_people].to_i,
            latitude: params[:by_latitude].to_f,
            longitude: params[:by_longitude].to_f,
            check_in_datetime: check_in_datetime,
            pack_in_hours: params[:by_pack_length].to_i,
            room_category: params[:room_category].to_i,
            is_affiliate: true,
            timezone: timezone,
            max_distance_in_km: params[:max_distance_in_km]
          })
          @search_exact_match = HotelDistanceSorter.sort(@hotel_search.results.exacts, @searched_address_coordinates)
          @search_similar_match = HotelDistanceSorter.sort(@hotel_search.results.similars, @searched_address_coordinates)
          if params[:commit] == t(:sort_by_price)
            @search_exact_match = @hotel_search.results.exacts.sort_by {|hotel| hotel[:min_offer][:price]}
            @search_similar_match = @hotel_search.results.similars.sort_by {|hotel| hotel[:min_offer][:price]}
          end
        rescue CheckinDateIsInThePastError
          flash[:error] = t(:error_message_selected_checkin_datetime_is_in_the_past)
          redirect_to public_affiliate_root_path
        rescue => ex
          Rails.logger.error(ex)
          Airbrake.notify(ex)
          flash[:error] = t(:message_checkin_date_and_address_error)
          redirect_to public_affiliate_root_path
        end
      end
    end
  end

  def show
    begin
      @hotel = Hotel.includes(:city).includes(room_types: :photos).find(params[:id])
      @business_room = BusinessRoom.includes(:photos).find_by id: (params[:business_room])
      
      if params[:checkin_timestamp] #user is comming from booking
        checkin_datetime = params[:checkin_timestamp].to_datetime
        params[:length_of_pack] = params[:pack_in_hours]
        params[:checkin_date] = checkin_datetime.to_date
        params[:checkin_hour] = checkin_datetime.hour
      else
        if params[:timezone].blank?
          date_time = "#{params[:checkin_date]} #{params[:checkin_hour]}:00"
          timestamp = date_time.to_datetime.to_i
          timezone = get_timezone(timestamp, @hotel.latitude, @hotel.longitude)
          checkin_datetime = date_time.in_time_zone(timezone).to_datetime
        else
          checkin_datetime = "#{params[:checkin_date]} #{params[:checkin_hour]}:00".in_time_zone(params[:timezone].gsub('%2F', '/')).to_datetime
        end

      end
      raise OfferNotAvailable if checkin_datetime < (DateTime.now + 1.hour)
      
      session[:last_hotel_visited_id] = @hotel.id
      pack_in_hours = params[:length_of_pack].to_i

      @business_room = BusinessRoom.includes(:photos).find_by id: (params[:business_room])
      if @business_room
        @offer_business_room = @business_room.is_a_meeting_room? ? MeetingRoomOffer.find(params[:offer_id]) : EventRoomOffer.find(params[:offer_id])
        if !@offer_business_room.available?
          flash[:warning] = t(:error_message_hotel_without_offers_to_show)
          redirect_to public_affiliate_root_path
        end
      else
        @room_type_search = OffersSearch::RoomTypes.new({
          number_of_guests: params[:number_of_people].to_i,
          hotel_id: @hotel.id,
          check_in_datetime: checkin_datetime,
          pack_in_hours: pack_in_hours
        })

        @search_exact_match = @room_type_search.results.exacts.sort_by { |s|
          offer = s[:min_offer]
          offer.save! if offer.is_omnibees?
          offer.reload #we need all the fields to calculate value for the entire number of guests
          s[:min_offer].price
        }

        @search_similar_match = @room_type_search.results.similars.sort_by { |s|
          offer = s[:min_offer]
          offer.save! if offer.is_omnibees?
          [ offer.checkin_timestamp,
            offer.get_number_of_common_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)),
            (offer.checkin_timestamp - checkin_datetime),
            -(offer.get_number_of_exceeding_hours_to_checkin_and_checkout(checkin_datetime, (checkin_datetime + pack_in_hours.hours)))
          ]
        }
      end
    rescue OfferNotAvailable
      flash[:error] = t(:error_message_offer_not_available)
      redirect_to public_affiliate_root_path
    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      flash[:error] = t(:error_message_search_params_error)
      redirect_to public_affiliate_root_path
    end
  end

  def hotel_room_type_show
    begin
      if params[:checkin_timestamp] #user is comming from booking
        checkin_datetime = params[:checkin_timestamp].to_datetime
        params[:length_of_pack] = params[:pack_in_hours]
        params[:checkin_date] = checkin_datetime.to_date
        params[:checkin_hour] = checkin_datetime.hour
      else
        if params[:timezone].blank?
          checkin_datetime = "#{params[:checkin_date]} #{params[:checkin_hour]}:00".to_datetime
        else
          checkin_datetime = "#{params[:checkin_date]} #{params[:checkin_hour]}:00".in_time_zone(params[:timezone]).to_datetime
        end
      end

      raise if checkin_datetime < (DateTime.now + 1.hour)
      @room_type = RoomType.includes(:photos).find(params[:id])
      @hotel = @room_type.hotel
    rescue => ex
      flash[:error] = t(:error_message_search_params_error)
      redirect_to public_affiliate_root_path
    end
  end

  def meeting_room_hotel_chain
    if @affiliate.hotel_chain.present?
      @affiliate_hotels = hash_hotels(@affiliate.hotel_chain.hotels) if @affiliate.hotel_chain.hotels.present?
    end
    checkin_datetime = "#{params[:by_checkin_date]} #{params[:by_checkin_hour]}:00 -03:00".to_datetime
    if params[:"#{@affiliate.layout_name.downcase}"].present? && Hotel.where(name: params[:"#{@affiliate.layout_name.downcase}"][:hotel_name]).present?
      affiliate_result = Hotel.where(name: params[:"#{@affiliate.layout_name.downcase}"][:hotel_name]).first
      @affiliate_hotel_search = OffersSearch::HotelChain.new({
                       number_of_guests: params[:by_number_of_people].to_i,
                       check_in_datetime: checkin_datetime,
                       room_category: params[:room_category],
                       pack_in_hours: params[:by_pack_length].to_i,
                       is_affiliate: true,
                       hotel_ids: affiliate_result.id
                      })
      @search_exact_match = @affiliate_hotel_search.results.exacts
      @search_similar_match = @affiliate_hotel_search.results.similars
    elsif params[:"#{@affiliate.layout_name.downcase}"].present? && City.where(name: params[:"#{@affiliate.layout_name.downcase}"][:hotel_name].gsub(/-[A-Z]{2}/, '')).present?
      affiliate_result = City.where(name: params[:"#{@affiliate.layout_name.downcase}"][:hotel_name].gsub(/-[A-Z]{2}/, '')).first
      city_hotels = @affiliate.hotel_chain.hotels.where(city_id: affiliate_result.id)
      hotels = city_hotels.map(&:id)
      @affiliate_hotel_search = OffersSearch::HotelChain.new({
        number_of_guests: params[:by_number_of_people].to_i,
        check_in_datetime: checkin_datetime,
        room_category: params[:room_category],
        pack_in_hours: params[:by_pack_length].to_i,
        is_affiliate: true,
        hotel_ids: hotels
      })
      @search_exact_match = @affiliate_hotel_search.results.exacts
      @search_similar_match = @affiliate_hotel_search.results.similars
    else
      @hotel_search = OffersSearch::HotelChain.new({
         number_of_guests: params[:by_number_of_people].to_i,
         hotel_ids: @affiliate.hotel_chain.hotels.map(&:id),
         check_in_datetime: checkin_datetime,
         room_category: params[:room_category],
         is_affiliate: true,
         pack_in_hours: params[:by_pack_length].to_i
       })
      @search_exact_match = @hotel_search.results.exacts
      @search_similar_match = @hotel_search.results.similars
    end

  rescue CheckinDateIsInThePastError
    flash[:error] = t(:error_message_selected_checkin_datetime_is_in_the_past)
    redirect_to public_affiliate_root_path
  rescue => ex
    Rails.logger.error(ex)
    Airbrake.notify(ex)
    flash[:error] = t(:message_checkin_date_and_address_error)
    redirect_to public_affiliate_root_path
  end

  private
    def save_hotel_search_log
      @log = HotelSearchLog.new
      if @affiliate.hotel_id.present? || params[:hotel_id].present?
        hotel = @affiliate.hotel || Hotel.find(params[:hotel_id])
        @log.latitude = hotel.latitude
        @log.longitude = hotel.longitude
        @log.city_name = hotel.city.name
        @log.state_name = hotel.city.state.initials
        @log.address = hotel.address
      else
        @log.latitude = params[:by_latitude]
        @log.longitude = params[:by_longitude]
        @log.city_name = params[:city_name]
        @log.state_name = params[:state_name]
        @log.address = params[:by_address]
      end
      @log.checkin_date = params[:by_checkin_dateMTR] || params[:by_checkin_date]
      @log.checkin_hour = params[:by_checkin_hourMTR].to_i || params[:by_checkin_hour].to_i
      @log.number_of_people = params[:by_number_of_people]
      @log.length_of_pack = params[:by_pack_lengthMTR] || params[:by_pack_length]
      @log.room_category = params[:room_category]
      @log.timezone = params[:by_timezone] || ''
      @log.save
    end

    def get_timezone(timestamp, lat, lng)
      json = TimezoneSrv.get(lat, lng, timestamp)
      timezone_value  = (json["dstOffset"]+json["rawOffset"]) * 1000
      response_hash   = {timezone_value: timezone_value, timezone_id: json["timeZoneId"], timezone_name: json["timeZoneName"]}
      response_hash[:timezone_id]
    end

    def get_coordinates(place)
      encoded_url = URI.encode("http://maps.googleapis.com/maps/api/geocode/json?address=#{place}&language=en&sensor=false")
      response = HTTParty.get(encoded_url)
      response.flatten[1][0].flatten[5]["location"]
    end

    def has_only_meeting_rooms?
      (@affiliate.only_meeting_room)
    end

    def hash_hotels(hotels)
      hotels_hsh = {}
      hotels.each_with_index do |hotel, i|
        hotels_hsh["cidade_#{i}"] = hotel.city.name + '-' + hotel.state
        hotels_hsh["hotel_name_#{i}"] = hotel.name
      end
      hotels_hsh
    end
end
