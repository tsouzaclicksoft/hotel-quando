class PublicAffiliate::UsersController < PublicAffiliate::BaseController
  inherit_resources
  before_filter :authenticate_public_affiliate_user!
  actions :edit

  def update
    resource()

    if I18n.locale == :en && params[:birth_date]
      date_parts = params[:birth_date].split('/')
      params[:birth_date] = date_parts[1] + '/' + date_parts[0] + '/' + date_parts[2]
    end

    successfully_updated = if needs_password?(@user, params)
      @user.update_with_password(permitted_params)
    else
      # remove the virtual current_password attribute update_without_password
      # doesn't know how to ignore it
      params_to_update = permitted_params
      params_to_update.delete(:current_password)
      params_to_update.delete(:password)
      params_to_update.delete(:password_confirmation)
      @user.update_without_password(params_to_update)
    end

    if successfully_updated
      # Sign in the user bypassing validation in case his password changed
      flash[:success] = t(:users)[:account_successfully_changed]
      sign_in @user, :bypass => true
      if params[:user][:budget_for_24_hours_pack].present?
        redirect_to :back
      else
        redirect_to public_affiliate_bookings_path
      end
    else
      if @user.errors.full_messages.is_a? String
        flash.now[:alert] = [@user.errors.full_messages]
      else
        flash.now[:alert] = @user.errors.full_messages
      end
      render 'edit'
    end


  def update_address
    @booking = Booking.find(params[:booking_id])
    current_public_affiliate_user.update(country: params[:public_affiliate_user][:country], state: params[:public_affiliate_user][:state], city: params[:public_affiliate_user][:city], street: params[:public_affiliate_user][:street], postal_code: params[:public_affiliate_user][:postal_code])
    current_public_affiliate_user.errors.add(:country, I18n.t('errors.messages.blank')) if current_public_affiliate_user.country.blank?
    current_public_affiliate_user.errors.add(:state, I18n.t('errors.messages.blank')) if current_public_affiliate_user.state.blank?
    current_public_affiliate_user.errors.add(:city, I18n.t('errors.messages.blank')) if current_public_affiliate_user.city.blank?
    current_public_affiliate_user.errors.add(:street, I18n.t('errors.messages.blank')) if current_public_affiliate_user.street.blank?
    current_public_affiliate_user.errors.add(:postal_code, I18n.t('errors.messages.blank')) if current_public_affiliate_user.postal_code.blank?
    if current_public_affiliate_user.errors.any?
      flash[:error] = t(:complete_all_fields)
      redirect_to new_public_affiliate_booking_payments_path(@booking.id)
    else
      redirect_to new_public_affiliate_booking_payments_path(@booking.id)
    end
  end

  end

  def resource
    @user = User.find(current_public_affiliate_user.id)
  end

  def permitted_params
    params.fetch(:user, {}).permit([:name, :cpf, :passport, :birth_date, :street, :state, :city, :country, :postal_code, :gender, :number, :complement, :phone_number, :locale, :current_password, :password_confirmation, :password, :budget_for_24_hours_pack, :affiliate_id])
  end

  # check if we need password to update user data
  # ie if password or email was changed
  # extend this as needed
  def needs_password?(user, params)
    params[:user][:current_password].present?
  end

end
