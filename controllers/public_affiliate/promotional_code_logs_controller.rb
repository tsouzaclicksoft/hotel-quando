# encoding: utf-8
class PublicAffiliate::PromotionalCodeLogsController < PublicAffiliate::BaseController
	include ActionView::Helpers::NumberHelper

  def check_promotional_code
    begin
    	@booking = Booking.find(params[:booking_id])
    	@hotel = @booking.hotel
    	@promotional_code = PromotionalCode.find_by_code params[:promotional_code]
    	if @promotional_code.nil?
    		flash['error'] = t(:invalid_code)
    	else
    		if check_if_code_is_active
    			discount_with_symbol = @promotional_code.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:percentage] ?
    			"#{@promotional_code.discount} %" : "#{number_to_currency(@promotional_code.discount, unit: @promotional_code.currency_code)}"
    			flash['success'] = t(:using_promotional_code_success, code: @promotional_code.code, discount: discount_with_symbol)
    			create_promotional_code_log()
    		end
    	end
  		redirect_to new_public_affiliate_booking_payments_path(@booking.id)
    rescue Exception => ex
      Airbrake.notify(ex)
    end
  end

  def check_promotional_code_without_creation
    begin
      @hotel = Hotel.find(params[:hotel_id])
      @promotional_code = PromotionalCode.find_by_code params[:promotional_code]
      if @promotional_code.nil?
        flash['error'] = t(:invalid_code)
      else
        if check_if_code_is_active
          flash['success'] = t(:validated_code_message)
        end
      end
      redirect_to :back
    rescue Exception => ex
      Airbrake.notify(ex)
    end
  end

  private
    def create_promotional_code_log
      destroy_promotional_code_log_if_necessary()
      @promotional_code_log = PromotionalCodeLog.new
      @promotional_code_log.promotional_code_id = @promotional_code.id
      @promotional_code_log.booking_id = @booking.id
      @promotional_code_log.currency_code = cookies[:currency]
      @promotional_code_log.save!
      if @promotional_code.discount_type == PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:percentage] && @promotional_code.discount.eql?(100)
        @booking.booking_tax = 0
        @booking.save!
      end
      update_promotional_code_status_if_necessary()
    end

    def destroy_promotional_code_log_if_necessary
    	if @booking.promotional_code_log
    		@booking.promotional_code_log.destroy
    	end
    end

    def update_promotional_code_status_if_necessary
    	@booking.reload
      promotional_code = @booking.promotional_code_log.promotional_code
      if promotional_code.promotional_code_logs.count >= promotional_code.usage_limit && promotional_code.user_type == PromotionalCode::ACCEPTED_USER_TYPE[:all_users]
	      promotional_code.status = PromotionalCode::ACCEPTED_STATUSES[:used]
	      promotional_code.save!
	    end
    end

	  def check_if_code_is_active
	  	if @promotional_code.status == PromotionalCode::ACCEPTED_STATUSES[:expired]
	  		flash['error'] = t(:expired_code)
	  		return false
	  	elsif @promotional_code.status != PromotionalCode::ACCEPTED_STATUSES[:active]
  			flash['error'] = t(:invalid_code)
  			return false
  		else
  			check_if_hotel_is_partner_this_campaign
  		end
	  end

	  def check_if_hotel_is_partner_this_campaign
	  	hotels_partners_in_this_campaign = @promotional_code.campaign.hotel_groups.pluck(:hotel_id)
  		if !hotels_partners_in_this_campaign.include?(@hotel.id)
  			flash['error'] = t(:this_hotel_does_not_participate_in_this_promotion)
  			return false
  		else
  			check_if_user_have_booking
  		end
	  end

	  def check_if_user_have_booking
	  	if @promotional_code.user_type == PromotionalCode::ACCEPTED_USER_TYPE[:users_without_booking]
        bookings = current_public_affiliate_user.bookings.where(status: [
                                    Booking::ACCEPTED_STATUSES[:confirmed],
                                    Booking::ACCEPTED_STATUSES[:no_show_paid_with_success],
                                    Booking::ACCEPTED_STATUSES[:confirmed_and_captured],
                                    Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
                                    ])
	  		if bookings.first
	  			flash['error'] = t(:this_promotion_is_only_to_users_without_booking)
	  			return false
	  		else
	  			return true
	  		end
	  	else
	  		return true
	  	end
	  end
end



