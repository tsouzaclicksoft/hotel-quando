class PublicAffiliate::RegistrationsController < Devise::RegistrationsController
  helper Shared::CurrencyCurrentHelper
  layout 'public_affiliate'

  before_filter :configure_permitted_parameters

  def new
    if session[:last_path] && (session[:last_path].split('/').include?('reservas') || session[:last_path].split('/').include?('bookings'))
      redirect_to new_public_affiliate_user_session_path
    else
      super
    end
  end

  def create
    build_resource(sign_up_params)
    if I18n.locale == :en && is_birth_date_format_valid?(params[:public_affiliate_user][:birth_date]) && params[:public_affiliate_user].try(:[], :birth_date)
      date_parts = params[:public_affiliate_user][:birth_date].split('/')
      resource.birth_date = date_parts[1] + '/' + date_parts[0] + '/' + date_parts[2]
    end
    resource.created_by_affiliate = true
    resource.affiliate_id = session[:affiliate].id

    if resource.save
      yield resource if block_given?
      if resource.active_for_authentication?
        # setting locale to config flash message and after sign up page
        params[:locale] = params[:public_affiliate_user][:locale]
        I18n.locale = I18n.available_locales.include?(params[:locale].to_sym) ? params[:locale] : I18n.default_locale
        sign_up(resource_name, resource)

        #SendUserDataRdsWorker.perform_async(params[:public_affiliate_user][:email], nil, params[:public_affiliate_user][:city], "Afiliado - Usuário registrou no sistema")

        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      if session[:last_path] && (session[:last_path].split('/').include?('reservas') || session[:last_path].split('/').include?('bookings'))
        flash[:alert] = "#{t(:there_were_the_following_errors_with_the_form)}: "
        flash[:alert] += resource.errors.full_messages.uniq.map(&:downcase).join(', ')
        flash[:alert] += '. ' if flash[:alert]
        flash[:alert] += "#{t(:please_fix_them_and_try_again)}."
        redirect_to new_public_affiliate_user_session_path(user_input: {name: resource.name.to_s, phone_number: resource.phone_number.to_s, email: resource.email.to_s}, tab: 'signup')
      else
        respond_with resource
      end
    end
  end


  def after_sign_up_path_for(user)
    if session[:last_path] == public_affiliate_root_path
      public_affiliate_bookings_path
    else
      session[:last_path] || public_affiliate_bookings_path
    end
  end

  def after_inactive_sign_up_path_for(user)
    if session[:last_path] == public_affiliate_root_path
      public_affiliate_bookings_path
    else
      session[:last_path] || public_affiliate_bookings_path
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
    devise_parameter_sanitizer.for(:sign_up) << :locale
    devise_parameter_sanitizer.for(:sign_up) << :phone_number
    devise_parameter_sanitizer.for(:sign_up) << :birth_date
    devise_parameter_sanitizer.for(:sign_up) << :street
    devise_parameter_sanitizer.for(:sign_up) << :city
    devise_parameter_sanitizer.for(:sign_up) << :state
    devise_parameter_sanitizer.for(:sign_up) << :country
    devise_parameter_sanitizer.for(:sign_up) << :postal_code
  end

  private

    def is_birth_date_format_valid?(birth_date)
      return true if birth_date =~ /\d{2}\/\d{2}\/\d{4}/
      false
    end
end
