# encoding: utf-8
class PublicAffiliate::Api::V1::PaymentsController < PublicAffiliate::Api::V1::BaseController
  respond_to :json

  include PayPal::SDK::Core::Logging
  include Shared::CurrencyCurrentHelper

  before_filter :set_resources_and_check_permissions!, only: [:create_payment_paypal, :create_payment_billed, :create_payment_maxipago]

  class ExecutePaymentPaypalFailed < StandardError; end
  class ExecutePaymentEpaycoFailed < StandardError; end
  class PaymentFailedError < StandardError; end
  class OmnibeesReservationError < StandardError; end

  def create_payment_maxipago
    begin
      use_credit_card_to_pay_booking()

      @booking.reload
      reserve_on_omnibees() if @booking.is_omnibees_or_hoteisnet?
      HotelMailer.send_booking_confirmation_notice(@booking).deliver
      UserMailer.send_booking_confirmation_notice(@booking).deliver
      AffiliateMailer.send_booking_confirmation_notice(@booking).deliver
      #SendUserDataRdsWorker.perform_async(@affiliate.id, @offer.hotel.name, @offer.hotel.city.name, "Afiliado - Reserva - Venda Realizada")
       render :json => { :msg => t(:success_payment) }
    rescue OmnibeesReservationError => omnibees_error
      #SendUserDataRdsWorker.perform_async(@affiliate.id, @booking.offer.hotel.name, @booking.offer.hotel.city.name, "Afiliado - Reserva - Venda Realizada")
      Airbrake.notify(omnibees_error)
      AdminMailer.error_reserving_on_omnibees(@booking, @credit_card).deliver
    rescue Exception => exception
      if @payment
        #SendUserDataRdsWorker.perform_async(@affiliate.id, @booking.offer.hotel.name, @booking.offer.hotel.city.name, "Afiliado - Reserva - Venda Não Concluída (Erro: Pagamento não efetuado)")
      end
      # Airbrake.notify(exception, parameters: {user_id: @affiliate.id})
      render :json => { :msg => exception.to_s, :error => 404 }
    end
  end

  def create_payment_billed
    create_payment
    begin
      @payment.status = Payment::ACCEPTED_STATUSES[:authorized]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]

      #SendUserDataRdsWorker.perform_async(@booking.user_id, @offer.hotel.name, @offer.hotel.city.name, "Afiliado WS - Reserva - Venda Realizada")
      HotelMailer.send_booking_confirmation_notice(@booking).deliver
      UserMailer.send_booking_confirmation_notice(@booking).deliver

      @payment.save!
      @booking.save!

      render :json => { :msg => t(:success_payment) }
    rescue Exception => exception
      #SendUserDataRdsWorker.perform_async(@booking.user_id, @offer.hotel.name, @offer.hotel.city.name, "Afiliado WS - Reserva - Venda Não Concluída (Erro: Pagamento não efetuado)")
      Airbrake.notify(exception)
      @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
      @payment.maxipago_response += "Bill payment error"
      @payment.save!

      render :json => { :msg => @payment_paypal.error.details, :error => 404 }
    end
  end

  private

  def create_payment
    @payment = Payment.new
    @payment.affiliate_id = @affiliate.id
    @payment.traveler_id = @booking.traveler_id
    @payment.booking_id = @booking.id
    @payment.status = Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response]
    if params[:paymentID].present? || params[:payment].present?
      @payment.paypal_payment_id = params[:paymentID].present? ? params[:paymentID] : params[:payment][:paypal_payment_id]
    end
    @payment.save!
  end

  def use_credit_card_to_pay_booking
    create_payment

    begin
      price_to_pay_now = @booking.total_price
      @currency = current_currency.code

      authorization_response = MaxiPagoInterface.direct_sale_without_token({
        payment: @payment,
        id: @payment.booking_id,
        # force payment in BRL
        total_price: calculate_to_currency_booking(1, price_to_pay_now, @currency, @booking.created_at).round(2),
        number: params[:number],
        expMonth: params[:expMonth],
        expYear: params[:expYear],
        cvv: params[:cvv]
      })
    rescue MaxiPagoInterface::MalformedMaxipagoResponseError => error
      @payment.status = Payment::ACCEPTED_STATUSES[:maxipago_response_error]
      @payment.maxipago_response += error.to_s
      @payment.save!
      raise PaymentFailedError.new(t('payments.errors.maxipago_response_error'))
    end

    if authorization_response.success
      @payment.status = Payment::ACCEPTED_STATUSES[:captured]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
      @booking.save!
    else
      @payment.status = Payment::ACCEPTED_STATUSES[:not_authorized]
    end
    set_payment_maxipago_response_fields(authorization_response)
    @payment.save!
    raise PaymentFailedError.new(t('payments.errors.transaction_not_authorized')) unless authorization_response.success
  end

  def set_payment_maxipago_response_fields(authorization_response)
    @payment.maxipago_order_id = authorization_response.order_id
    @payment.maxipago_response += authorization_response.body
    @payment.maxipago_transaction_id = authorization_response.transaction_id
  end

  def set_resources_and_check_permissions!
    begin
      @booking = @affiliate.bookings.find(params[:booking_id])
      @offer = @booking.offer
      if (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
         render :json => { :msg => t("bookings.booking_already_confirmed")}
      end
      if @booking.status == Booking::ACCEPTED_STATUSES[:canceled]
         render :json => { :msg => t("bookings.booking_canceled") }
      end
    rescue
       render :json => { :msg => t(:you_cant_confirm_a_booking_that_not_yours) }
    end
  end

  def reserve_on_omnibees
    begin
      # 1 = Hoteisnet || 2 = Omnibees
      choice = ChooseOmnibeesOrHoteisnet.choose(@booking.pack_in_hours, @booking.offer.checkin_timestamp)
      if choice == 1
        client = Hotelnet::Client.new
      else
        client = Omnibees::Client.new
      end

      reservation_xml = client.make_booking({ booking: @booking })
      @booking.omnibees_request_xml = client.last_request_xml
      @booking.omnibees_response_xml = reservation_xml
      @booking.save!

      if choice == 1
        reservation_status = Hotelnet::DTO::HotelReservationToBookingResult.parse(reservation_xml)
      else
        reservation_status = Omnibees::DTO::HotelReservationToBookingResult.parse(reservation_xml)
      end

      if reservation_status.confirmed?
        @booking.omnibees_code = reservation_status.reservation_id
      end
      @booking.save!
    rescue Exception => ex
      Airbrake.notify(ex)
      raise OmnibeesReservationError
    end
    raise OmnibeesReservationError unless reservation_status.confirmed?
  end

  def credit_card_params
    params.require(:credit_card).permit(:type, :number, :expire_month, :expire_year, :first_name, :last_name)
    params.require(:billing_address).permit(:line1, :city, :state, :postal_code, :country_code)
  end

  def customer_info
     params.require(:customer_info).permit(:token_card, :name, :sur_name, :email, :phone)
  end
end
