class PublicAffiliate::Api::V1::LocationsController < PublicAffiliate::Api::V1::BaseController

  def lat_lng
    return if params[:address].blank?

    json = LocationSrv.get(params[:address])

    if json["results"].blank?
      render :json => { :location => [] }.to_json, :status => 400
    else
      render :json => { :location => json["results"][0]["geometry"]["location"] }.to_json
    end
  end
end
