# encoding: utf-8
class PublicAffiliate::Api::V1::BookingsController < PublicAffiliate::Api::V1::BaseController
  inherit_resources

  include Shared::CurrencyCurrentHelper

  actions :index, :update
  #before_filter :authenticate_public_affiliate_api_V1_affiliate!, :only => [:index, :cancel, :show]
  has_scope :by_status
  helper_method :total_price_for_number_of_people
  before_action :validate_offer_availability, only: [:create]

=begin
  def index
    @booking = User.where(email: "affiliate_ws_api_system@hotelquando.com").first.bookings.where(affiliate_id: session[:affiliate].id)
  end
=end

  def cancel
    #@booking = current_public_affiliate_user.bookings.find(params[:id])
    @booking = @affiliate.bookings.find(params[:id])

    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking)
    if booking_cancellation_response.success
      @msg = booking_cancellation_response.message
      render :json => { :msg => @msg, :success => 200 }
    else
      @msg = booking_cancellation_response.message
      render :json => { :msg => @msg, :error => 404 }
    end
    #redirect_to public_affiliate_bookings_path
  end


  def new
    begin
      @booking = Booking.new(number_of_people: params[:number_of_people])
      @business_room = BusinessRoom.find_by id: (params[:business_room_id])
      @search_params = params
      if @business_room
        business_type = @business_room.type
        @offer = MeetingRoomOffer.where(meeting_room_id: params[:business_room_id])
                            .where(checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                               pack_in_hours: params[:pack_in_hours], price: params[:price], \
                               status: MeetingRoomOffer::ACCEPTED_STATUS[:available]).first
      else
        @offer = OmnibeesOffer.where(room_type_id: params[:room_type_id], \
                               checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                               pack_in_hours: params[:pack_in_hours], price: params[:price]).first
        @offer ||= Offer.where(room_type_id: params[:room_type_id], \
                               checkin_timestamp: params[:checkin_timestamp].to_datetime.strftime("%Y-%m-%d %H:%M:%S +0000"), \
                               pack_in_hours: params[:pack_in_hours], price: params[:price], \
                               status: Offer::ACCEPTED_STATUS[:available]).first
    end
      if @offer && (@offer.available?)
        @booking.offer = @offer
      else
        render :json => { :msg => t(:error_message_offer_not_available), :error => 404 }
      end

    rescue => ex
      Rails.logger.error(ex)
      Airbrake.notify(ex)
      render :json => { :msg => t(:error_message_booking_new_params_error), :error => 404 }
    end

  end

  def create
    if params[:phone_number].blank? || params[:guest_name].blank? || params[:number_of_people].blank? || params[:offer_id].blank?
      render :json => { :msg => t(:message_booking_params_error), :error => 404 }
    else
      booking_params = {:phone_number => params[:phone_number], :guest_name => params[:guest_name], :number_of_people => params[:number_of_people], :note => params[:number_of_people] ,:guest_email => params[:guest_email], :currency_code => params[:currency_code]}
      begin
        @booking = Booking.new(booking_params)
        @offer = if OmnibeesOffer.where(id: params[:offer_id].to_i).present?
                    OmnibeesOffer.find params[:offer_id].to_i
                  else
                    Offer.find params[:offer_id].to_i
                  end
        @booking.user_id = User.where(email: "affiliate_ws_api_system@hotelquando.com").first.id
        @booking.hotel_comments = @offer.hotel.send("description_#{I18n.locale}")
        @booking.pack_in_hours = @offer.pack_in_hours
        @booking.status = Booking::ACCEPTED_STATUSES[:waiting_for_payment]
        @booking.created_by_agency = false
        @booking.checkin_date = @offer.checkin_timestamp
        @booking.affiliate_id = @affiliate.id
        @booking.created_by_affiliate = true
        @booking.booking_tax = 0
        @booking.currency_code = current_currency.code
        booking_create_response = BookingMaker.call(offer: @offer, booking: @booking)

        if booking_create_response.success
          # SendUserDataRdsWorker.perform_async(@booking.user_id, @offer.hotel.name, @offer.hotel.city.name, "Afiliado WS - Reserva - Venda Realizada")
          @booking = booking_create_response.booking
          @msg     = "success"
        elsif booking_create_response.message
          # SendUserDataRdsWorker.perform_async(@booking.user_id, @offer.hotel.name, @offer.hotel.city.name, "Afiliado WS - Reserva - Venda Não Concluída (Erro: #{booking_create_response.message})")
          render :json => { :msg => booking_create_response.message, :error => 404 }
        else
          render :json => { :msg => t(:error_message_offer_not_available), :error => 404 }

        end
      rescue => ex
        Rails.logger.error(ex)
        Airbrake.notify(ex)
        # SendUserDataRdsWorker.perform_async(current_public_user.id, @offer.hotel.name, @offer.hotel.city.name, "Publico - Reserva - Venda Não Concluída (Erro: #{t(:message_booking_creation_error)})")
        render :json => { :msg => t(:message_booking_creation_error), :error => 404 }
      end
    end
  end

  def show
    @booking = Booking.find(params[:id])
    if request.get?
      if @booking.blank?
        render :json => { :msg => t(:sorry_but_this_reservation_wasnt_found_among_your_reservations), :error => 404 }
      end
    elsif request.post?
      if (@booking.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed])
        if @booking.status != Booking::ACCEPTED_STATUSES[:waiting_for_payment]
          params[:booking].delete(:number_of_people)
        else
          params[:booking][:number_of_people] = params[:booking][:number_of_people].to_i + 1
        end
        @booking.merge params[:booking] # look merge method in Booking
        if @booking.save
          render :json => { :msg => t(:booking_successfully_edited), :error => 404 }
        else
          render :json => { :msg => t(:booking_error_editing), :error => 404 }
        end
      end
    else
      render :json => { :msg => t(:booking_error_editing), :error => 404 }
    end
  end

=begin
  def edit
    @booking = current_public_affiliate_user.bookings.find_by_id(params[:id])
    booking_editable = ((@booking.status == Booking::ACCEPTED_STATUSES[:waiting_for_payment]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]) || (@booking.status == Booking::ACCEPTED_STATUSES[:confirmed_without_card]) ) && (@booking.offer.checkin_timestamp > Time.now.strftime("&Y/%m/%d %H:%M +0000").to_datetime)
    unless booking_editable
      flash[:error] = t(:you_cant_edit_this_booking)
      redirect_to public_affiliate_bookings_path
    end
  end
=end

=begin
  def update
    # @booking = current_public_affiliate_user.bookings.find_by_id(params[:id])
    # @offer = @booking.offer
    update! do | success, failure |
      success.html do
        flash[:notice] = t(:booking_edited_successfully)
        redirect_to public_affiliate_booking_path(params[:id])
      end
    end
  end
=end

  def total_price_for_number_of_people(number_of_people)
    booking = Booking.new
    set_booking_cached_prices(booking)
    booking.number_of_people = number_of_people
    booking.hotel = @offer.hotel
    booking.offer_id = @offer.id
    booking.total_price_without_tax
  end

  helper_method :get_timezone
  def get_timezone(latitude, longitude)
    hotel_timezone = {}
    timezone = Timezone.lookup(latitude, longitude)
    offset_in_hours = (TZInfo::Timezone.get(timezone.name).current_period.offset.utc_total_offset) / 3600.0
    offset_in_seconds = (TZInfo::Timezone.get(timezone.name).current_period.offset.utc_total_offset)
    hotel_timezone[:timezone_name] = timezone.name
    hotel_timezone[:offset_in_hours] = offset_in_hours
    hotel_timezone[:offset_in_seconds] = offset_in_seconds
    hotel_timezone
  end

  protected
    def collection
      # TODO - FIXME
      # includes :offer doesn't work because rails doesn't work well with
      # big int columns
      @bookings = apply_scopes(Booking).where(user_id: current_public_affiliate_user.id).includes(:hotel).order('created_at DESC')
    end

    def begin_of_association_chain
      current_public_affiliate_user
    end

  private

  def set_booking_cached_prices(booking)
    booking.cached_room_type_initial_capacity = @offer.room_type_initial_capacity
    booking.cached_room_type_maximum_capacity = @offer.room_type_maximum_capacity
    booking.cached_offer_price = @offer.price
    booking.cached_offer_extra_price_per_person = @offer.extra_price_per_person
  end

  def get_current_offer_id
    session[:current_offer_id] || nil
  end

  def set_current_offer_id id
    session[:current_offer_id] = id
  end


  def get_current_offer_type
    session[:current_offer_type] || nil
  end

  def set_current_offer_type type
    session[:current_offer_type] = type
  end

  def current_offer_price
    session[:current_offer_price] || nil
  end

  def set_current_offer_price price
    session[:current_offer_price] = price
  end

  def build_resource_params
    [params.fetch(:booking, {}).permit("guest_name", "note", "affiliate_id", "created_by_affiliate")]
  end

  def validate_offer_availability
    @offer = if OmnibeesOffer.where(id: params[:offer_id].to_i).present?
                OmnibeesOffer.find params[:offer_id].to_i
              else
                Offer.find params[:offer_id].to_i
              end
    if @offer.is_omnibees?
      render :json => { :msg => t(:error_message_offer_not_available), :error => 404 } unless @offer.available?
    else
      render :json => { :msg => t(:error_message_offer_not_available), :error => 404 } unless @offer.status == 1
    end
  end
end
