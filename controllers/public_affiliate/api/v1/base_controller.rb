# encoding: utf-8
class PublicAffiliate::Api::V1::BaseController < ActionController::Base
  before_filter :set_affiliate, :set_currency
  helper Shared::CurrencyCurrentHelper
  helper PublicAffiliate::BaseHelper

  rescue_from ActiveRecord::RecordNotFound, with: :render_record_not_found
  rescue_from ArgumentError, with: :render_invalid_request

  private
    def render_record_not_found
      render json: "", status: :not_found
    end

    def render_invalid_request(error)
      render json: error.message, status: :bad_request
    end

	def set_affiliate
		if !params[:token].blank?
		  @affiliate = Affiliate.where(webservice_token: params[:token], is_active: true).first
		  if @affiliate.blank?
		  	render :json => { :msg => t(:error_message_no_selected_affiliate) }.to_json
		  end
		else
		  render :json => { :msg => t(:error_message_no_selected_affiliate) }.to_json
		end
	end

  def set_currency
    if params[:currency]
      cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
    elsif cookies[:currency].nil?
      cookies[:currency] = "US$"
    end
  end

end
