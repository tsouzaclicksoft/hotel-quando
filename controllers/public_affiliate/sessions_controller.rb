# encoding: utf-8
class PublicAffiliate::SessionsController < Devise::SessionsController
  helper Shared::CurrencyCurrentHelper
  layout 'public_affiliate'
  after_action  :allow_iframe
  before_filter :set_currency
  before_filter :set_affiliate

  def create
    user = User.find_by_email(params[:public_affiliate_user].try(:[], :email))
    if user.nil? || user.try(:is_active?)
      super # if the user is not found or the user is active, we call the default devise action, else the user is inactive and we display a custom message
    else
      flash['alert'] = "Sua conta foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_public_affiliate_user_session_path
    end
  end

  def after_sign_in_path_for(user)
    #SendUserDataRdsWorker.perform_async(user.email, nil, user.city, "Afiliado - Usuário logou no sistema")
    if session[:last_path] == public_affiliate_root_path
      public_affiliate_bookings_path
    else
      session[:last_path] || public_affiliate_bookings_path
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    new_public_affiliate_user_session_path
  end

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  def set_currency
    if params[:currency]
      cookies[:currency] = params[:currency] == "US%24" || params[:currency] == "US%2524" ? "US$" : params[:currency]
    elsif cookies[:currency].nil?
      cookies[:currency] = "US$"
    end
  end

  def set_affiliate
    if !params[:token].blank?
        @affiliate = Affiliate.where(token: params[:token], is_active: true).first
      if !@affiliate.blank?
        session[:affiliate] = @affiliate
      else
        render :file => "#{Rails.root}/public/404.html",  :status => 404
      end

    elsif !session[:affiliate].blank?
      @affiliate = session[:affiliate]

    else
      render :file => "#{Rails.root}/public/404.html",  :status => 404

    end

  end
end
