# encoding: utf-8
class Affiliate::RegistrationsController < Devise::RegistrationsController  
  layout 'affiliate/login'

  def new
    if session[:last_path] && (session[:last_path].split('/').include?('reservas') || session[:last_path].split('/').include?('bookings'))
      redirect_to new_affiliate_affiliate_session_path
    else
      super
    end
  end

  def create
    build_resource(affiliate_params)
    if resource.save
      AffiliateMailer.send_login_and_password(resource).deliver
      yield resource if block_given?
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_flashing_format?
        sign_up(resource_name, resource)
        respond_with resource, location: after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_flashing_format?
        expire_data_after_sign_in!
        respond_with resource, location: after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      if session[:last_path] && (session[:last_path].split('/').include?('reservas') || session[:last_path].split('/').include?('bookings'))
        flash[:alert] = "#{t(:there_were_the_following_errors_with_the_form)}: "
        flash[:alert] += resource.errors.full_messages.uniq.map(&:downcase).join(', ')
        flash[:alert] += '. ' if flash[:alert]
        flash[:alert] += "#{t(:please_fix_them_and_try_again)}."
        redirect_to new_affiliate_affiliate_session_path(affiliate_input: {
			name: resource.name.to_s, email: resource.email.to_s, login: resource.login.to_s, cnpj: resource.cnpj.to_s, 
			responsible_name: resource.responsible_name.to_s, phone_number: resource.phone_number.to_s, country: resource.country.to_s,
			state: resource.state.to_s, city: resource.city.to_s, street: resource.street.to_s, number: resource.number.to_s,
			complement: resource.complement.to_s}, tab: 'signup')

      else
        respond_with resource
      end
    end
  end


  def after_sign_up_path_for(affiliate)
    if session[:last_path] == affiliate_bookings_path
      affiliate_bookings_path
    else
      session[:last_path] || affiliate_root_path
    end
  end

  def after_inactive_sign_up_path_for(affiliate)
    if session[:last_path] == affiliate_root_path
      affiliate_root_path
    else
      session[:last_path] || affiliate_root_path
    end
  end

  protected

  def affiliate_params
    params.require(:affiliate).permit(:name, :email, :login, :cnpj, :responsible_name, :country, :state, :city, :street, :number, :complement, :phone_number, :password, :password_confirmation)
  end


  private


end