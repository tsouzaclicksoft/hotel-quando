# encoding: utf-8
class Affiliate::BaseController < ActionController::Base
  protect_from_forgery
  inherit_resources
  layout 'affiliate'
  before_filter :authenticate_affiliate_affiliate!, :set_locale_as_pt_br
  helper Shared::CurrencyCurrentHelper

  protected

    def begin_of_association_chain
      current_affiliate_affiliate
    end

    def set_locale_as_pt_br
      I18n.locale = :pt_br
    end
end
