# encoding: utf-8
class Affiliate::InfoController < Affiliate::BaseController
  actions :none

  def index
  end

  def edit
  end

  def update
    if current_affiliate_affiliate.update_with_password(affiliate_params)
      sign_in(current_affiliate_affiliate, :bypass => true)
      flash[:success] = "Senha modificada com sucesso."
      redirect_to affiliate_root_path
    else
      render "edit"
    end

  end

  private

  def affiliate_params      
    params.required(:affiliate).permit(:name, :is_active, :token, :comission_value, :observation, :password, :password_confirmation, :current_password, :responsible_name, :phone_number, :country, :state, :city, :street, :number, :complement, :login, :sign_email, :api_iframe_on, :api_webservice_on)
  end

end
