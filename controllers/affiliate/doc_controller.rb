# encoding: utf-8
class Affiliate::DocController < Affiliate::BaseController
  actions :none

  def index
  end

  def iframe
  end

  def webservice
  end

  def webservice_version
    render "affiliate/doc/webservice_version/#{params[:version]}"
  end

  private


end
