class Admin::CampaignsController < Admin::BaseController
  actions :all
  before_action :authorized_master_and_hotel_manager_and_b2b_commercial_and_b2b_management

  def create
    create! do | success, failure |
      success.html do
        create_or_remove_hotel_group
        redirect_to admin_campaign_path(@campaign)
      end
    end
  end

  def update
    update! do | success, failure |
      success.html do
        create_or_remove_hotel_group
        redirect_to admin_campaign_path(@campaign)
      end
    end
  end

  private
    def build_resource_params
      [params.fetch(:campaign, {}).permit!]
    end

    def create_or_remove_hotel_group
      hotel_groups_ids = @campaign.hotel_groups.pluck(:hotel_id)
      begin
        params[:hotel_groups].each do |key, value|
          if value.eql?("1")
            unless hotel_groups_ids.include?(key.to_i)
              hotel_group_create = HotelGroup.create(campaign_id: @campaign.id, hotel_id: key.to_i)
              hotel_group_create.save!
            end
          else
            if hotel_groups_ids.include?(key.to_i)
              hotel_group_remove = @campaign.hotel_groups.find_by_hotel_id key
              hotel_group_remove.destroy
            end
          end
        end
      rescue
        flash['error'] = I18n.t('controllers.admin.campaigns_controller.hotel_group_error')
      end
    end

  protected
    def collection
      @campaigns ||= end_of_association_chain.order('created_at DESC').page(params[:page]).per(40)
    end
end
