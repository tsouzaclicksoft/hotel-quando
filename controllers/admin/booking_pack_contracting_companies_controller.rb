class Admin::BookingPackContractingCompaniesController < Admin::BaseController
  actions :index, :show

  def index
    index!
  end

  private

  def collection
    @booking_pack_contracting_companies = BookingPackContractingCompany.includes(:booking_pack)
  end

end
