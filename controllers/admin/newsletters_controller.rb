class Admin::NewslettersController < Admin::BaseController
  before_action :authorized_master_and_b2b_commercial_and_attendant
  
  actions :index
  has_scope :by_email
  has_scope :by_city
  has_scope :by_id

  def index
    case params[:commit]
    when t(:export_data)
      send_data(NewslettersReport.new(collection.per(collection.total_count)).export.to_stream.read,
        filename: "newsletter_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
        type:     "application/xlsx")
    else
      super
    end
  end

  def import
  end

  def import_csv
    Newsletter.import(params[:file])
    redirect_to admin_newsletters_path, notice: "Planilha importada com sucesso"
    rescue Exception => exception
      flash[:error] = "Arquivo inválido"
      render :import
  end

  protected

    def collection
      @newsletters ||= end_of_association_chain.order('id DESC').page(params[:page]).per(40)
    end

end
