class Admin::CompanyRemunerationsController < Admin::BaseController
	before_action :authorized_master
	def create
		prepare_params
		create!
	end

	def update
		prepare_params
		update!
	end

	def build_resource_params
		[params.fetch(:company_remuneration, {}).permit!]
	end

	def prepare_params
		begin
			params[:company_remuneration][:remuneration_by_new_company].gsub!('R$ ', ' ')
			params[:company_remuneration][:remuneration_by_new_company].gsub!(',','.')

			params[:company_remuneration][:remuneration_by_reservation].gsub!('R$ ', ' ')
			params[:company_remuneration][:remuneration_by_reservation].gsub!(',','.')

		rescue
		end
	end

end