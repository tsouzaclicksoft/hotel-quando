class Admin::StatesController < Admin::BaseController
  actions :all, :except => [ :destroy ]
  before_action :authorized_master_and_hotel_manager

  def create
    create! do | success, failure |
      success.html do
        redirect_to admin_states_path
      end
    end   
  end

  private

  def collection
    @states ||= end_of_association_chain.includes(:country).order('name').page(params[:page]).per(40)
  end

  def build_resource_params
    [params.fetch(:state, {}).permit!]
  end
end