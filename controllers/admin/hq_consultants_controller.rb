class Admin::HqConsultantsController < Admin::BaseController
	before_action :authorized_master_and_b2b_commercial

	def create
		create! do | success, failure |
			success.html do
				HqConsultantMailer.send_login_and_password(@hq_consultant).deliver
				redirect_to admin_hq_consultant_path(@hq_consultant)
			end
		end
	end

	def update
		build_resource_params
		update!
	end

	def build_resource_params
		[params.fetch(:hq_consultant, {}).permit!]
	end

	def collection
      @hq_consultants ||= HqConsultant.all.page(params[:page]).per(40)
    end

end