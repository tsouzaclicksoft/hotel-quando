class Admin::AffiliatesController < Admin::BaseController
  actions :all, :except => [ :destroy ]
  before_filter :check_colombian_authorization, :only => [:show, :edit, :activate, :deactivate, :generate_token, :generate_webservice_token]
  include Admin::BaseHelper

  def create
    prepare_params
    create! do | success, failure |
      success.html do
        AffiliateMailer.send_login_and_password(@affiliate).deliver
        redirect_to admin_affiliate_path(@affiliate)
      end
    end
  end

  def update
    prepare_params
    update!
  end

  def activate
    begin
      Affiliate.find(params[:id]).activate!
      flash['success'] = I18n.t('controllers.admin.affiliates_controller.activate_success')
    rescue
      flash['error'] = I18n.t('controllers.admin.affiliates_controller.activate_error')
    end
    redirect_to :back
  end

  def deactivate
    begin
      Affiliate.find(params[:id]).deactivate!
      flash['success'] = I18n.t('controllers.admin.affiliates_controller.activate_success')
    rescue
      flash['error'] = I18n.t('controllers.admin.affiliates_controller.deactivate_error')
    end
    redirect_to :back
  end

  def generate_token
    begin
      affiliate = Affiliate.find(params[:id])
      affiliate.token = loop do
        random_token = SecureRandom.urlsafe_base64(nil, false)
        break random_token unless Affiliate.exists?(token: random_token)
      end
      affiliate.save
      flash['success'] = I18n.t('controllers.admin.affiliates_controller.generate_token_success')
    rescue
      flash['error'] = I18n.t('controllers.admin.affiliates_controller.generate_token_error')
    end
    redirect_to :back
  end

  def generate_webservice_token
    begin
      affiliate = Affiliate.find(params[:id])
      affiliate.webservice_token = loop do
        random_token = SecureRandom.urlsafe_base64(nil, false)
        break random_token unless Affiliate.exists?(token: random_token)
      end
      affiliate.save
      flash['success'] = I18n.t('controllers.admin.affiliates_controller.generate_token_success')
    rescue
      flash['error'] = I18n.t('controllers.admin.affiliates_controller.generate_token_error')
    end
    redirect_to :back
  end

  private

  def check_colombian_authorization
    affiliate = Affiliate.find(params[:id])
    if current_admin_manager.master? && current_admin_manager.country == "CO" && affiliate.country != 'CO'
      redirect_to admin_affiliates_path
    end
  end

  def collection
    if current_admin_manager.country == "BR"
      @affiliates = Affiliate.order(:name)
    else 
      @affiliates = Affiliate.where(country: current_admin_manager.country).order(:name)
    end
  end

  def build_resource_params
    [params.fetch(:affiliate, {}).permit!]
  end

  def prepare_params
    begin
      params[:affiliate][:comission_value].gsub!('R$ ', ' ')
      params[:affiliate][:comission_value].gsub!(',','.')
      if params[:affiliate][:custom_layout] == "true"
        params[:affiliate][:layout_name] = "default"
      else
        params[:affiliate][:custom_layout] = false
        params[:affiliate][:layout_name] = nil
      end
    rescue
    end
  end

end
