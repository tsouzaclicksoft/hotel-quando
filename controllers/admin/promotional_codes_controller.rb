class Admin::PromotionalCodesController < Admin::BaseController
  actions :all
  before_action :authorized_master_and_hotel_manager_and_b2b_commercial_and_b2b_management
  
  has_scope :by_id
  has_scope :by_code
  has_scope :by_campaign_id
  has_scope :by_status

  include Shared::CurrencyCurrentHelper

  def index
    case params[:commit]
    when t(:export_data)
      send_data(PromotionalCodesReport.new(collection.per(collection.total_count)).export.to_stream.read,
        filename: "codigos_promocionais_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
        type:     "application/xlsx")
    else
      super
    end
  end

  def update
    if params[:promotional_code][:status] == PromotionalCode::ACCEPTED_STATUSES[:used]
      params[:promotional_code][:status] = PromotionalCode::ACCEPTED_STATUSES[:used]
    
    elsif params[:promotional_code][:start_date].present? && params[:promotional_code][:expiration_date].present?
      if ((Date.parse(params[:promotional_code][:expiration_date]) >= Date.today) && (Date.parse(params[:promotional_code][:start_date]) <= Date.today))
        params[:promotional_code][:status] = PromotionalCode::ACCEPTED_STATUSES[:active]
      else
        params[:promotional_code][:status] = PromotionalCode::ACCEPTED_STATUSES[:expired]
      end
    
    elsif params[:promotional_code][:start_date].blank? && Date.parse(params[:promotional_code][:expiration_date]) >= Date.today
      params[:promotional_code][:status] = PromotionalCode::ACCEPTED_STATUSES[:active]  
   
    else
      params[:promotional_code][:status] = PromotionalCode::ACCEPTED_STATUSES[:expired]
    end
    
    prepare_params
    update!
  end

  def create
    prepare_params
    amount_of_codes = params[:amount_of_codes].to_i
    change_status_if_necessary
    if amount_of_codes == 0 || amount_of_codes == 1
      create!
    else
      begin
        amount_of_codes.times do |i|
          begin
            @promotional_code = PromotionalCode.new
            @promotional_code.campaign_id = params[:promotional_code][:campaign_id]
            @promotional_code.code = generate_code(params[:promotional_code][:code])
            @promotional_code.user_type = params[:promotional_code][:user_type]
            @promotional_code.discount_in = params[:promotional_code][:discount_in]
            @promotional_code.discount_type = params[:promotional_code][:discount_type]
            @promotional_code.discount = params[:promotional_code][:discount]
            @promotional_code.usage_limit = params[:promotional_code][:usage_limit]
            @promotional_code.start_date = params[:promotional_code][:start_date]
            @promotional_code.expiration_date = params[:promotional_code][:expiration_date]
            @promotional_code.currency_code = params[:promotional_code][:currency_code]
            @promotional_code.status = params[:promotional_code][:status] if params[:promotional_code][:status].present?
            @promotional_code.save!
          rescue ActiveRecord::RecordNotUnique
            retry
          end
        end
        redirect_to admin_promotional_codes_path
      rescue
        flash['error'] = I18n.t('controllers.admin.promotional_codes_controller.create_error')
        render 'new'
      end
    end
  end

  private
    def generate_code(prefix)
      prefix + SecureRandom.hex(4)
    end

    def build_resource_params
      [params.fetch(:promotional_code, {}).permit!]
    end
    def prepare_params
      currencies = Currency.all.pluck(:money_sign).uniq
      if params[:promotional_code][:discount].present?
        params[:promotional_code][:discount].gsub!(' %', '')
        currencies.each do | currency |
          params[:promotional_code][:discount].gsub!("#{currency} ", '')
        end
        params[:promotional_code][:discount].gsub!('.','')
        params[:promotional_code][:discount].gsub!(',', '.')
      end
      params[:promotional_code][:avaiable_for_packs] = params[:promotional_code][:avaiable_for_packs].to_s.gsub!(/[\[\]\"]/, "")
      params[:promotional_code][:avaiable_for_days_of_the_week] = params[:promotional_code][:avaiable_for_days_of_the_week].to_s.gsub!(/[\[\]\"]/, "")
    end
  protected
    def collection
      @promotional_codes ||= end_of_association_chain.order('created_at DESC').includes(:campaign).page(params[:page]).per(40)
    end
    def change_status_if_necessary
      if params[:promotional_code][:start_date].present? && params[:promotional_code][:expiration_date].present?
        if (Date.parse(params[:promotional_code][:start_date]) > Date.today)
          params[:promotional_code][:status] = PromotionalCode::ACCEPTED_STATUSES[:suspended]
        end
      end
    end
end
