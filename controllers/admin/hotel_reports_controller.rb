# encoding: utf-8
class Admin::HotelReportsController < Admin::BaseController
  actions :index
  include BookingReportsConcern

  def index
    @hotels = Hotel.all
    @quantity_of_months = 12
    if params[:date].present? && params[:hotel_id].present?
      @hotel = Hotel.find(params[:hotel_id])
      @invoice = invoice_for(@hotel, params[:invoice_type], params[:date].to_time)
    end
  end

end
