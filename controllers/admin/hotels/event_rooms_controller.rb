class Admin::Hotels::EventRoomsController < Admin::BaseController
  actions :show, :edit, :update

  belongs_to :hotel

  def update
    update! do |success, failure|
      success.html do
        redirect_to admin_hotel_event_room_path(id: @event_room, hotel_id: @event_room.hotel_id)
      end
    end
  end

  private

  def build_resource_params
    [params.fetch(:event_room, {}).permit!]
  end

end
