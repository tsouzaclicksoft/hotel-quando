class Admin::Hotels::MeetingRoomsController < Admin::BaseController
  actions :show, :edit, :update

  belongs_to :hotel

  def update
    update! do |success, failure|
      success.html do
        redirect_to admin_hotel_meeting_room_path(id: @meeting_room, hotel_id: @meeting_room.hotel_id)
      end
    end
  end

  private

  def build_resource_params
    [params.fetch(:meeting_room, {}).permit!]
  end

end
