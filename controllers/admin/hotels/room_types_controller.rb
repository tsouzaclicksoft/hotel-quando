class Admin::Hotels::RoomTypesController < Admin::BaseController
  actions :show, :edit, :update

  belongs_to :hotel

  def update
    update! do |success, failure|
      success.html do
        redirect_to admin_hotel_room_type_path(id: @room_type, hotel_id: @room_type.hotel_id)
      end
    end
  end

  private

  def build_resource_params
    [params.fetch(:room_type, {}).permit!]
  end

end
