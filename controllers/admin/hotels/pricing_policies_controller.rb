class Admin::Hotels::PricingPoliciesController < Admin::BaseController
  actions :show, :edit, :update

  belongs_to :hotel

  def update
    update! do |success, failure|
      success.html do
        redirect_to admin_hotel_pricing_policy_path(id: @pricing_policy, hotel_id: @pricing_policy.hotel_id)
      end
    end
  end

  def meeting_show
 		@pricing_policy = BusinessRoomPricingPolicy.find(params[:id])
  end

  def event_show
		@pricing_policy = BusinessRoomPricingPolicy.find(params[:id])
  end

  private

  def build_resource_params
    [params.fetch(:pricing_policy, {}).permit!]
  end
end
