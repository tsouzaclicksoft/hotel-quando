class Admin::CompanyDetailsController < Admin::BaseController
	before_action :authorized_master_and_b2b_commercial_and_b2b_management
	
	def index
		@company_details = CompanyDetail.getAllCompanyDetails(params)
		@companiesOrdened = CompanyDetail.getComapaniesOrdenedInMonth(@company_details)
	end


	def update
		prepare_params

		@company = CompanyDetail.find(params[:id])
		if @company.blank?
			redirect_to admin_company_details_path
		else
			@company.executive_responsible_id = params[:company_detail][:executive_responsible_id]
			@company.save
			redirect_to admin_company_detail_path(@company)
		end
	end

	def prepare_params
		unless params[:executiveHash].blank?
			executives_hash = params[:executiveHash].split(', ')
			params[:company_detail][:executive_responsible_id]  = executives_hash[0]
		end
	end

	def build_resource_params
		[params.fetch(:company_universe, {}).permit!]
	end
	
end
