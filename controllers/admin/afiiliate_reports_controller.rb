# encoding: utf-8
class Admin::AffiliateReportsController < Admin::BaseController
  include BookingReportsConcern
  actions :index

  def index
    @quantity_of_months = 12

    if params[:date].present? && params[:affiliate_id].present?
      @affiliate = Affiliate.find(params[:affiliate_id])
      @invoice = invoice_for(@affiliate, params[:invoice_type], params[:date].to_time)
    end
  end

end
