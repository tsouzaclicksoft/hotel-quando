class Admin::PossiblePartnerContactsController < Admin::BaseController
  actions :index, :show
  before_action :authorized_master_and_hotel_manager_and_attendant

  has_scope :by_locale
  has_scope :by_type, type: :array
  has_scope :by_admin_visualized, type: :array

  def show
    show!
    unless @possible_partner_contact.admin_visualized
      @possible_partner_contact.update(admin_visualized: true)
    end
  end

  def mark_all_messages_as_read
    PossiblePartnerContact.where(admin_visualized: false).update_all(admin_visualized: true, updated_at: DateTime.now)
    redirect_to admin_possible_partner_contacts_path
  end

  protected
    def collection
      @possible_partner_contacts ||= end_of_association_chain.order('created_at DESC').page(params[:page]).per(40)
    end
end
