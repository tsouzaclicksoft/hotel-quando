class Admin::MemberGetMemberConfigController < Admin::BaseController
  actions :all, :only => [ :show, :edit, :update ]
  before_filter :check_colombian_authorization
  before_action :authorized_master
  include Admin::BaseHelper
  def update
    prepare_params
    update!
  end
  
  def prepare_params
    begin
      params[:member_get_member_config][:avaiable_for_packs] = params[:member_get_member_config][:avaiable_for_packs].to_s.gsub!(/[\[\]\"]/, "")
      params[:member_get_member_config][:avaiable_for_days_of_the_week] = params[:member_get_member_config][:avaiable_for_days_of_the_week].to_s.gsub!(/[\[\]\"]/, "")
      params[:member_get_member_config][:discount_to_referred].gsub!('R$ ', ' ')
      params[:member_get_member_config][:discount_to_referred].gsub!(',','.')
      params[:member_get_member_config][:discount_to_referred_first_bookings].gsub!('R$ ', ' ')
      params[:member_get_member_config][:discount_to_referred_first_bookings].gsub!(',','.')
      
      params[:member_get_member_config][:indicators_credit_first_bookings].gsub!('R$ ', ' ')
      params[:member_get_member_config][:indicators_credit_first_bookings].gsub!(',','.')
      params[:member_get_member_config][:indicators_credit].gsub!('R$ ', ' ')
      params[:member_get_member_config][:indicators_credit].gsub!(',','.')
    rescue
    end
  end
  
  private
    def build_resource_params
      [params.fetch(:member_get_member_config, {}).permit!]
    end

  def check_colombian_authorization
    if current_admin_manager.master? && current_admin_manager.country == "CO"
      redirect_to admin_root_path
    end
  end
end
