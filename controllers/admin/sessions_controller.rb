# encoding: utf-8
class Admin::SessionsController < Devise::SessionsController
  layout 'admin/login'

	def create
    manager = Manager.find_by_login(params[:admin_manager].try(:[], :login))
    if manager.nil? || manager.try(:is_active?)
      super # if the affiliate is not found or the affiliate is active, we call the default devise action, else the affiliate is inactive and we display a custom message
    else
      flash['alert'] = "Sua conta foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_admin_manager_session_path
    end
  end

  def after_sign_in_path_for(admin)
    admin_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    admin_root_path
  end
end
