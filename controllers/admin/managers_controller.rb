class Admin::ManagersController < Admin::BaseController
	before_action :authorized_master
	actions :all, :except => [ :destroy ]


	def create
		create! do | success, failure |
			success.html do
				ManagerMailer.send_login_and_password(@manager).deliver
				redirect_to admin_manager_path(@manager)
			end
		end
	end

	def activate
		begin
			Manager.find(params[:id]).activate!
			flash['success'] = I18n.t('views.admin.manager_controller.activate_success')
		rescue
			flash['error'] = I18n.t('views.admin.manager_controller.activate_error')
		end
		redirect_to :back
	end

	def deactivate
		begin
			Manager.find(params[:id]).deactivate!
			flash['success'] = I18n.t('views.admin.manager_controller..deactivate_success')
		rescue
			flash['error'] = I18n.t('views.admin.manager_controller..deactivate_error')
		end
		redirect_to :back
	end
	private

	def build_resource_params
		[params.fetch(:manager, {}).permit!]
	end

	def collection
    @managers ||= Manager.all.page(params[:page]).per(40)
  end

end