class Admin::AgenciesController < Admin::BaseController
  actions :all, :except => [ :destroy ]

  def create
    prepare_params
    create! do | success, failure |
      success.html do
        AgencyMailer.send_login_and_password(@agency).deliver
        redirect_to admin_agency_path(@agency)
      end
    end
  end

  def update
    prepare_params
    update!
  end

  def activate
    begin
      Agency.find(params[:id]).activate!
      flash['success'] = I18n.t('controllers.admin.agencies_controller.activate_success')
    rescue
      flash['error'] = I18n.t('controllers.admin.agencies_controller.activate_error')
    end
    redirect_to :back
  end

  def deactivate
    begin
      Agency.find(params[:id]).deactivate!
      flash['success'] = I18n.t('controllers.admin.agencies_controller.deactivate_success')
    rescue
      flash['error'] = I18n.t('controllers.admin.agencies_controller.deactivate_error')
    end
    redirect_to :back
  end

  private

  def build_resource_params
    [params.fetch(:agency, {}).permit!]
  end

  def prepare_params
    begin
      params[:agency][:comission_value].gsub!('R$ ', ' ')
      params[:agency][:comission_value].gsub!(',','.')
    rescue
    end
  end

end
