class Admin::CompanyAttendantsController < Admin::BaseController
	before_action :authorized_master
	def create
		create! do | success, failure |
			success.html do
				CompanyAttendantMailer.send_login_and_password(@company_attendant).deliver
				redirect_to admin_company_attendant_path(@company_attendant)
			end
		end
	end

	def update
		build_resource_params
		update!
	end

	def activate
		begin
			CompanyAttendant.find(params[:id]).activate!
			flash['success'] = I18n.t('views.company_universe.executive_controller.activate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.executive_controller.activate_error')
		end
		redirect_to :back
	end

	def deactivate
		begin
			CompanyAttendant.find(params[:id]).deactivate!
			flash['success'] = I18n.t('views.company_universe.executive_controller..deactivate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.executive_controller..deactivate_error')
		end
		redirect_to :back
	end

	def build_resource_params
		[params.fetch(:company_attendant, {}).permit!]
	end

end
