# encoding: utf-8
class Admin::AgencyReportsController < Admin::BaseController
  include BookingReportsConcern
  actions :index

  def index
    @quantity_of_months = 12

    if params[:date].present? && params[:agency_id].present?
      @agency = Agency.find(params[:agency_id])
      @invoice = invoice_for(@agency, params[:invoice_type], params[:date].to_time)
    end
  end

end
