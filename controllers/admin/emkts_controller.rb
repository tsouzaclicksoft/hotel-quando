class Admin::EmktsController < Admin::BaseController
  actions :all
  before_action :authorized_master
  before_action :set_emkt, only: [:show, :edit, :update, :destroy]


  def create
    @emkt = Emkt.new(emkt_params)

    if @emkt.save
      if @emkt.emkt_type == 1
        redirect_to with_bookings_admin_emkts_path
      else
        redirect_to without_bookings_admin_emkts_path
      end
    else
      render :new
    end
  end

  def show
  end

  def with_bookings
    @emkts = Emkt.where(emkt_type: 1).order(:id)
  end

  def without_bookings
    @emkts = Emkt.where(emkt_type: 2).order(:id)
  end

  def new
    @emkt ||= Emkt.new
  end

  def update
    if @emkt.update_attributes(emkt_params)
      if @emkt.emkt_type == 1
        redirect_to with_bookings_admin_emkts_path
      else
        redirect_to without_bookings_admin_emkts_path
      end
    else
      render :edit
    end
  end

  def destroy
    emkt_type = @emkt.emkt_type
    @emkt.destroy
    if emkt_type == 1
      redirect_to with_bookings_admin_emkts_path
    else
      redirect_to without_bookings_admin_emkts_path
    end
  end

 private

  def set_emkt
    @emkt = Emkt.find(params[:id])
  end

  def emkt_params
    params.require(:emkt).permit(:activate, :header, :link_header, :box_1, :link_box_1, :box_2, :link_box_2, :box_3_1, :link_box_3_1, :box_3_2, :link_box_3_2, :box_3_3, :link_box_3_3, :box_3_4, :link_box_3_4, :box_3_5, :link_box_3_5, :box_3_6, :link_box_3_6, :footer, :link_footer, :name_pt_br, :emkt_type, :country, :city, :hour_send, :order_send, :day_of_week_send)
  end
end


