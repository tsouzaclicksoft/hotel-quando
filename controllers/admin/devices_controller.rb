class Admin::DevicesController < Admin::BaseController
  actions :all, :except => [ :destroy ]
  before_filter :authorized_to_master, :except => [ :destroy ]

  def index
    @devices = Device.all.order("created_at DESC")
  end

  def destroy
    device = Device.find(params[:id])
    device.destroy
  end

end
