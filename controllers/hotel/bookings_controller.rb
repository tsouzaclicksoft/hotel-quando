# encoding: utf-8
class Hotel::BookingsController < Hotel::BaseController
  class CannotChargeCreditCard < StandardError; end;
  class CannotRefundUser < StandardError; end;

  BOOKING_STATUSES_TO_SHOW_TO_HOTEL = [Booking::ACCEPTED_STATUSES[:confirmed], Booking::ACCEPTED_STATUSES[:confirmed_and_captured], Booking::ACCEPTED_STATUSES[:canceled], Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced], Booking::ACCEPTED_STATUSES[:confirmed_without_card], Booking::ACCEPTED_STATUSES[:requested_reservation], Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]]
  respond_to :js, :json
  actions :index, :show
  has_scope :by_checkin_date, using: [:initial_date, :final_date]
  has_scope :by_status
  has_scope :by_room_type_id, type: :array
  has_scope :by_room_numbers
  has_scope :by_pack_length, type: :array
  has_scope :by_guest_name
  has_scope :by_id
  before_action :set_booking, only: [:hotel_accept_request, :hotel_deny_request]

  #def index
   # index! do
   #   @bookings = @bookings.where(status: BOOKING_STATUSES_TO_SHOW_TO_HOTEL).order('date_interval DESC')
   # end
  #end

  def rooms
    index! do
      @bookings = @bookings.where(status: BOOKING_STATUSES_TO_SHOW_TO_HOTEL, meeting_room_offer_id: nil, event_room_offer_id: nil).order('date_interval DESC')
    end
  end

  def meeting_rooms
    index! do
      @bookings = @bookings.where(status: BOOKING_STATUSES_TO_SHOW_TO_HOTEL).where.not(meeting_room_offer_id: nil).order('date_interval DESC')
    end
  end

  def event_rooms
    index! do
      @bookings = @bookings.where(status: BOOKING_STATUSES_TO_SHOW_TO_HOTEL).where.not(event_room_offer_id: nil).order('date_interval DESC')
    end
  end

  def show
    show! do
      if @booking.is_meeting_offer? || @booking.is_event_offer?
        params[:type] = @booking.business_room.type
      else
        params[:type] = @booking.room.class.name
      end
      params[:type] += 'Booking'
    end
  end

  def set_as_no_show
    @booking = current_hotel_hotel.bookings.find(params[:id])
    if @booking.can_be_set_as_no_show?
      begin
        if @booking.confirmed_by_maxipago? || @booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
          if @booking.payments.where(status: Payment::ACCEPTED_STATUSES[:captured]).count == 0
            unless @booking.status == Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
              charge_credit_card_for_no_show()
            end
            UserMailer.no_show_with_charge_successfull_notice(@booking).deliver
          else
            refund_difference_from_booking_total_to_no_show if @booking.no_show_refund_value > 0
            UserMailer.no_show_for_already_captured_cc_notice(@booking).deliver
          end
          @booking.set_as_no_show(payment: :success)
          HotelMailer.noshow_successfull_charged_notice(@booking).deliver
        else
          @booking.set_as_no_show(payment: :pending)
          AdminMailer.no_show_pending_charge_notice(@booking).deliver
          HotelMailer.no_show_pending_charge_notice(@booking).deliver
          UserMailer.no_show_pending_charge_notice(@booking).deliver
        end

      rescue CannotRefundUser
        @booking.set_as_no_show({refund: :error, payment: :success})
        HotelMailer.noshow_successfull_charged_notice(@booking).deliver
        AdminMailer.send_no_show_refund_error_notice(@booking).deliver
        UserMailer.send_no_show_refund_error_notice(@booking).deliver
      rescue Exception => exception
        @booking.set_as_no_show(payment: :error)
        AdminMailer.no_show_with_charge_error_notice(@booking).deliver
        HotelMailer.no_show_with_charge_error_notice(@booking).deliver
        UserMailer.no_show_with_charge_error_notice(@booking).deliver
      end
      @booking.save!
    end
  end

  def accuse_delayed_checkout
    begin
      @booking = current_hotel_hotel.bookings.find(params[:id])
      @booking.accuse_delayed_checkout
      UserMailer.send_delayed_checkout_notice(@booking).deliver
      @completed_request = true
    rescue Exception => exception
      @completed_request = false
    end
  end

  def hotel_accept_request
    if @booking.status == Booking::ACCEPTED_STATUSES[:requested_reservation]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
    elsif @booking.status == Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
    end
    @booking.save! if @booking.valid?
    UserMailer.booking_request_accepted(@booking).deliver
    redirect_to rooms_hotel_bookings_path
  end

  def hotel_deny_request
    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking)
    if booking_cancellation_response.success
      flash[:notice] = booking_cancellation_response.message
      UserMailer.booking_request_denied(@booking).deliver
      redirect_to rooms_hotel_bookings_path
    else
      flash[:error] = booking_cancellation_response.message
    end
  end

  protected
    def refund_difference_from_booking_total_to_no_show
      payment = @booking.payments.last
      response = MaxiPagoInterface.refund(payment: payment, value_to_refund: @booking.no_show_refund_value)
      refund = Refund.new
      refund.value = @booking.no_show_refund_value
      refund.payment = payment
      refund.traveler_id = payment.traveler_id
      refund.user_id = payment.user_id
      refund.maxipago_response = response.body
      refund.maxipago_order_id = payment.maxipago_order_id

      if response.success
        refund.status = Refund::ACCEPTED_STATUSES[:success]
        refund.save!
        payment.update!(status: Payment::ACCEPTED_STATUSES[:refunded])
      else
        refund.status = Refund::ACCEPTED_STATUSES[:error]
        refund.save!
        payment.update!(status: Payment::ACCEPTED_STATUSES[:error_refunding])
        raise CannotRefundUser.new
      end
    end

    def charge_credit_card_for_no_show
      authorized_payment = @booking.payments.where(status: [Payment::ACCEPTED_STATUSES[:authorized], Payment::ACCEPTED_STATUSES[:captured]]).last
      payment = Payment.new
      payment.credit_card_id = authorized_payment.credit_card.id
      payment.user_id = authorized_payment.user_id
      payment.traveler_id = authorized_payment.traveler_id
      payment.booking_id = @booking.id
      payment.status = Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response]
      payment.save!
      response = MaxiPagoInterface.direct_sale(payment: payment, no_show: true)
      payment.maxipago_response = response.body
      payment.maxipago_order_id = response.order_id
      if response.success
        payment.status = Payment::ACCEPTED_STATUSES[:captured]
      end
      payment.save!
      raise CannotChargeCreditCard.new unless response.success
    end

    def collection
      @bookings ||= end_of_association_chain.page(params[:page]).per(40)
    end

    def set_booking
      @booking = Booking.find_by id: params[:id]
    end
end
