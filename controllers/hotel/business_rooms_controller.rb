# encoding: utf-8
class Hotel::BusinessRoomsController < Hotel::BaseController

  actions :all, :except => [ :destroy ]
  has_scope :by_hotel_id
  before_action :set_type
  before_action :set_business, only: [:show, :edit, :update]
  include Hotel::BusinessRoomsHelper

  def index
    @business_rooms = business_class.by_hotel_id(current_hotel_hotel)
  end

  def new
    @business_room = business_class.new()
  end

  def create
    @business_room = business_class.new(resource_params)
    @business_room.hotel_id = current_hotel_hotel.id
    if @business_room.save
      registered = @type.eql?("MeetingRoom") ? t(:registered_female) : t(:registered)
      redirect_to sti_business_path(@type,@business_room), :flash => { :success => " #{t(@type.underscore)} #{I18n.t('controllers.hotel.business_rooms_controller.registered_with_success', registered: registered)}" }
    else
      render action: 'new'
    end
  end

  def show
  end

  def edit
  end

  def update
    if @business_room.update(resource_params)
      redirect_to sti_business_path(@type,@business_room)
    else
      render action: 'edit'
    end
  end

  private

  def set_business
    @business_room = business_class.find(params[:id])
  end

  def set_type
    @type = params[:type]
  end

  def business_class
    @type.constantize
  end

  def resource_params
    params.require(@type.underscore.to_sym).permit(:name_pt_br, :name_en, :name_es, :description_en, :description_pt_br, :description_es, :maximum_capacity, :photos_attributes => [:id, :title_en, :title_pt_br, :title_es, :direct_image_url, :_destroy] )
  end

end

