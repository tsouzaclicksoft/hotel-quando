# encoding: utf-8
class Hotel::OffersController < Hotel::BaseController
  has_scope :by_month_of_a_year, :using => [:month, :year]
  has_scope :by_status, type: :array
  has_scope :by_room_type_id, type: :array
  has_scope :by_checkin_range, :using => [:initial_date, :final_date]

  def index
    params[:by_month_of_a_year] ||= {month: Time.now.month, year: Time.now.year}
    @room_types = []
    current_hotel_hotel.room_types.each do |room_type|
      reserved_offers_per_day = apply_scopes(Offer)
                                .reserved
                                .by_hotel_id(current_hotel_hotel.id)
                                .by_room_type_id(room_type.id)
                                .offers_count_per_day
      reserved_offers_per_day.default = 0
      @room_types << {
        name: room_type.full_name_pt_br,
        id: room_type.id,
        reserved_offers_per_day: reserved_offers_per_day,
        room_with_avaliable_offers_count_per_day: room_type.rooms_with_active_offer_count_per_day(params[:by_month_of_a_year][:month], params[:by_month_of_a_year][:year])
      }
    end
  end

  def show_all
    @offers = apply_scopes(Offer).by_hotel_id(current_hotel_hotel.id).order("checkin_timestamp ASC").page(params[:page]).per(20)
    @offers_log = RoomOffersCreatorLog.where(hotel_id: current_hotel_hotel.id)
  end

  def create
    form_complete = return_if_form_is_complete_and_set_notices_for_creation
    if form_complete
      log = save_form_log(:creation)
      log.result = 'Processing'
      log.save
      CreateOffersWorker.perform_async(log.id, current_hotel_hotel.id, log.form_params)
      flash[:success] = I18n.t('controllers.hotel.offers_controller.create_success')
      redirect_to hotel_offers_path
    else
      redirect_to new_hotel_offer_path, params
    end
  end

  def remove
    if params[:remove_confirmation]
      log = RoomOffersCreatorLog.find(params[:log_id])
      log.result = 'Processing'
      log.save
      ProcessOffersRemovalWorker.perform_async(params[:log_id], current_hotel_hotel.id, log.form_params, params[:number_of_offers_that_will_be_removed], params[:number_of_offers_that_cant_be_removed])
      flash[:success] = I18n.t('controllers.hotel.offers_controller.remove_success')
      redirect_to hotel_offers_path
    else
      if request.post?
        form_complete = return_if_form_is_complete_and_set_notices_for_removal
        if form_complete
          log = save_form_log(:removal)
          redirect_to action: "remove_intermediate_step", log_id: log.id
        else
          redirect_to remove_hotel_offers_path, params
        end
      end
    end
  end

  def remove_intermediate_step
  end

  def remove_all_intermediate_step
    if current_admin_manager.present?
      redirect_to hotel_root_path if !current_admin_manager.master? && !current_admin_manager.hotel_manager?
    else
      redirect_to hotel_root_path
    end
  end


  def remove_all_offers
    if current_admin_manager.present? && (current_admin_manager.master? || current_admin_manager.hotel_manager?)
      if params[:remove_confirmation]
        log = RoomOffersCreatorLog.find(params[:log_id])
        log.result = 'Processing'
        log.save
        ProcessAllOffersRemovalWorker.perform_async(params[:log_id], current_hotel_hotel.id, params[:number_of_offers_that_will_be_removed], params[:number_of_offers_that_cant_be_removed])
        flash[:success] = I18n.t('controllers.hotel.offers_controller.remove_success')
        redirect_to hotel_offers_path
      else
        if request.post?
          log = save_form_log(:removal_all)
          redirect_to action: "remove_all_intermediate_step", log_id: log.id
        end
      end
    else
      redirect_to hotel_root_path
    end
  end

  def count_per_room
    render json: apply_scopes(Offer.group(:room_id))
                 .by_status([Offer::ACCEPTED_STATUS[:available], Offer::ACCEPTED_STATUS[:suspended]])
                 .by_hotel_id(current_hotel_hotel.id)
                 .count
  end

  private

  def save_form_log(type)
    log = RoomOffersCreatorLog.new
    log.hotel = current_hotel_hotel
    form_params = Marshal.load(Marshal.dump(params))
    form_params.delete(:utf8)
    form_params.delete(:authenticity_token)
    form_params.delete(:action)
    form_params.delete(:controller)
    log.form_params = form_params
    log.type_of_log = type
    log.save
    log.reload
  end

  def return_if_form_is_complete_and_set_notices
    form_complete = true
    if params[:initial_date].empty?
      form_complete = false
      flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.initial_date_empty_error')
    end

    if params[:final_date].empty?
      form_complete = false
      flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.final_date_empty_error')
    end
    begin
      if params[:initial_date].present? && params[:initial_date].to_date < Date.today
        form_complete = false
        flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.initial_date_before_today_error')
      end

      if params[:final_date].present? && params[:final_date].to_date < Date.today
        form_complete = false
        flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.final_date_before_today_error')
      end

      if params[:initial_date].present? && params[:final_date].present? && params[:initial_date].to_datetime > params[:final_date].to_datetime
        form_complete = false
        flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.initial_date_after_final_date_error')
      end
    rescue
      form_complete = false
      flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.form_invalid_fields_error')
    end

    unless params[:selected_days_hash]
      form_complete = false
      flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.no_day_of_week_selected')
    end

    if !(params[:room_types_ids])
      form_complete = false
      flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.no_room_type_selected')
    # else
    #   unless params[:rooms_ids]
    #     form_complete = false
    #     flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.no_room_selected')
    #   end
    end

    if !(params[:selected_pack_lengths])
      form_complete = false
      flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.no_pack_length_selected')
    end
    form_complete
  end

  def return_if_form_is_complete_and_set_notices_for_removal
    form_complete = return_if_form_is_complete_and_set_notices

    unless form_complete
      flash[:error].capitalize!
      flash[:error].gsub!(/[,]\s$/, '. ')
      flash[:error] = I18n.t('controllers.hotel.offers_controller.return_if_form_is_complete_and_set_notices_for_removal_error', error: flash[:error])
    end

    form_complete
  end

  def return_if_form_is_complete_and_set_notices_for_creation
    form_complete = return_if_form_is_complete_and_set_notices

    unless params[:pack]
      form_complete = false
      flash[:error] = flash[:error].to_s + I18n.t('controllers.hotel.offers_controller.return_if_form_is_complete_and_set_notices_for_creation_error')
    end

    unless form_complete
      flash[:error].capitalize!
      flash[:error].gsub!(/[,]\s$/, '. ')
      flash[:error].prepend(I18n.t('controllers.hotel.offers_controller.form_uncomplete_error'))
      flash[:error] += I18n.t('controllers.hotel.offers_controller.please_complete_form')
    end

    form_complete
  end

end
