# encoding: utf-8
class Hotel::BusinessRoomPricingPoliciesController < Hotel::BaseController
  before_action :set_type
  before_action :set_pricing_policy, only: [:show, :edit, :update, :destroy]
  before_filter :authorize, only: [:edit, :update, :destroy, :new, :create]
  include Hotel::BusinessRoomPricingPoliciesHelper

  def index
    @pricing_policies = pricing_policy_class.by_hotel_id(current_hotel_hotel)
  end

  def new
    @pricing_policy = pricing_policy_class.new
    create_business_room_prices_if_necessary()
  end

  def create
    @pricing_policy = pricing_policy_class.new(resource_params)
    @pricing_policy.hotel_id = current_hotel_hotel.id
    if @pricing_policy.save
      redirect_to sti_business_path(@type,@pricing_policy)
    else
      render action: 'new'
    end
  end

  def show
  end

  def edit
    create_business_room_prices_if_necessary
  end

  def update
    if @pricing_policy.update(resource_params)
      redirect_to sti_business_path(@type,@pricing_policy)
    else
      render action: 'edit'
    end
  end

  def destroy
    @pricing_policy.destroy
    redirect_to sti_business_path(@type)
  end

  private
  def create_business_room_prices_if_necessary
    if @type_room.eql?("MeetingRoom")
      current_hotel_hotel.meeting_rooms.each do | business_room |
        existing_business_room_price = @pricing_policy.business_room_prices.find_by_business_room_id(business_room.id)
        if existing_business_room_price.nil?
          @pricing_policy.business_room_prices.build(business_room: business_room)
        end
      end
    else
      current_hotel_hotel.event_rooms.each do | business_room |
        existing_business_room_price = @pricing_policy.business_room_prices.find_by_business_room_id(business_room.id)
        if existing_business_room_price.nil?
          @pricing_policy.business_room_prices.build(business_room: business_room)
        end
      end
    end
  end

  def set_pricing_policy
    @pricing_policy = pricing_policy_class.find(params[:id])
  end

  def set_type
    @type = params[:type]
    if @type.eql?("MeetingRoomPricingPolicy")
      @type_room = "MeetingRoom"
      @length_of_packs = MeetingRoomOffer::ACCEPTED_LENGTH_OF_PACKS
    else
      @type_room = "EventRoom"
      @length_of_packs = EventRoomOffer::ACCEPTED_LENGTH_OF_PACKS
    end

  end

  def pricing_policy_class
    @type.constantize
  end

  def resource_params
    params.require(@type.underscore)
      .permit(:name,
              :percentage_profile_name,
              :percentage_for_1h,
              :percentage_for_2h,
              :percentage_for_3h,
              :percentage_for_4h,
              :percentage_for_5h,
              :percentage_for_6h,
              :percentage_for_7h,
              :percentage_for_8h,
              :percentage_for_24h,
              :business_room_prices_attributes => [
                :id,
                :business_room_id,
                :pack_price_1h,
                :pack_price_2h,
                :pack_price_3h,
                :pack_price_4h,
                :pack_price_5h,
                :pack_price_6h,
                :pack_price_7h,
                :pack_price_8h,
                :pack_price_24h]
              )
  end

end
