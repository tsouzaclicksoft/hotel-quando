# encoding: utf-8
class Hotel::PricingPoliciesController < Hotel::BaseController
  before_filter :authorize, only: [:edit, :update, :destroy, :new, :create]

  def new
    @pricing_policy = PricingPolicy.new
    create_room_type_pricing_policies_if_necessary()
  end

  def edit
    @pricing_policy = PricingPolicy.find(params[:id])
    create_room_type_pricing_policies_if_necessary
  end

  private
  def create_room_type_pricing_policies_if_necessary
    current_hotel_hotel.room_types.each do | room_type |
      existing_room_type_pricing_policy = @pricing_policy.room_type_pricing_policies.find_by_room_type_id(room_type.id)
      if existing_room_type_pricing_policy.nil?
        @pricing_policy.room_type_pricing_policies.build(room_type: room_type)
      end
    end
  end

  def build_resource_params
    [params.fetch(:pricing_policy, {})
      .permit(:name,
              :percentage_profile_name,
              :percentage_for_3h,
              :percentage_for_6h,
              :percentage_for_9h,
              :percentage_for_12h,
              :percentage_for_24h,
              :room_type_pricing_policies_attributes => [
                :id,
                :room_type_id,
                :pack_price_3h,
                :pack_price_6h,
                :pack_price_9h,
                :pack_price_12h,
                :pack_price_24h,
                :extra_price_per_person_for_pack_price_3h,
                :extra_price_per_person_for_pack_price_6h,
                :extra_price_per_person_for_pack_price_9h,
                :extra_price_per_person_for_pack_price_12h,
                :extra_price_per_person_for_pack_price_24h]
              )]
  end

end
