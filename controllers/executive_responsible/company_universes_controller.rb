class ExecutiveResponsible::CompanyUniversesController < ExecutiveResponsible::BaseController
	
	has_scope :with_cnpj
	has_scope :by_name
	has_scope :by_city
	has_scope :by_region
	has_scope :by_was_sent_mail
	has_scope :by_we_talked_phone
	has_scope :by_we_already_face_to_face_meeting
	has_scope :by_cnpj
	has_scope :by_social_name
	has_scope :by_talked_phone_date, using: [:initial_date, :final_date]
	has_scope :by_already_face_to_face_meeting_date, using: [:initial_date, :final_date]
	has_scope :by_company_registered_date, using: [:initial_date, :final_date]
	has_scope :by_follow_up_date, using: [:initial_date, :final_date]

  def all_companies
  	if current_executive_responsible_executive_responsible.country == "CO"
  		@companies = apply_scopes(CompanyUniverse).where(executive_responsible: current_executive_responsible_executive_responsible, country: "CO").order(:talked_phone_date).page(params[:page]).per(40)
  	else
  		@companies = apply_scopes(CompanyUniverse).where(executive_responsible: current_executive_responsible_executive_responsible).order(:talked_phone_date).page(params[:page]).per(40)
  	end
  end

	def create
		prepare_params
		virify_company_already_registered
		create!
	end

	def index
		if current_executive_responsible_executive_responsible.country == "CO"
			@companies = apply_scopes(CompanyUniverse).where(executive_responsible: current_executive_responsible_executive_responsible, company_already_registered: false, country: "CO").page(params[:page]).per(40)
		else
			@companies = apply_scopes(CompanyUniverse).where(executive_responsible: current_executive_responsible_executive_responsible, company_already_registered: false).page(params[:page]).per(40)
		end
	end

	def update
		prepare_params
		virify_company_already_registered
		update!
	end

	def activate
		begin
			CompanyUniverse.find(params[:id]).activate!
			flash['success'] = I18n.t('views.company_universe.region_controller.activate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.region_controller.activate_error')
		end
		redirect_to :back
	end

	def deactivate
		begin
			CompanyUniverse.find(params[:id]).deactivate!
			flash['success'] = I18n.t('views.company_universe.region_controller..deactivate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.region_controller..deactivate_error')
		end
		redirect_to :back
	end

	def build_resource_params
		[params.fetch(:company_universe, {}).permit!]
	end

	def virify_company_already_registered
		@company_detail = CompanyDetail.with_cnpj.where(agency_detail_id: nil).where(cnpj: params[:company_universe][:cnpj]).first
		
		if @company_detail != nil
			params[:company_universe][:company_detail_id] = @company_detail.id
			params[:company_universe][:company_registered_date] = @company_detail.created_at
			params[:company_universe][:company_already_registered] = true
		end

	end

	def prepare_params
		unless params[:citiesHash].blank?
			cities_hash = params[:citiesHash].split(', ')
			params[:company_universe][:city_id]  = cities_hash[0]
		end
		unless params[:companyRegionsHash].blank?
			regions_hash = params[:companyRegionsHash].split(', ')
			params[:company_universe][:company_region_id]  = regions_hash[0]
		end
	end

	protected
    def end_of_association_chain
      return CompanyUniverse.where(executive_responsible: current_executive_responsible_executive_responsible)
    end

    def collection
      @company_universes ||= end_of_association_chain.order('checkin_date DESC').includes(:city).includes(:company_region).page(params[:page]).per(40)
    end
    
end
