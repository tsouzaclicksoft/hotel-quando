# encoding: utf-8
class ExecutiveResponsible::SessionsController < Devise::SessionsController
  layout 'executive_responsible/login'

	def create
    executive = ExecutiveResponsible.find_by_login(params[:executive_responsible_executive_responsible].try(:[], :login))
    if executive.nil? || executive.try(:is_active?)
      super # if the affiliate is not found or the affiliate is active, we call the default devise action, else the affiliate is inactive and we display a custom message
    else
      flash['alert'] = "Sua conta foi desativada pelo administrador, você não pode se logar.<br>Caso tenha alguma dúvida, entre em contato com o HotelQuando.".html_safe
      redirect_to new_executive_responsible_executive_responsible_session_path
    end
  end

  def after_sign_in_path_for(executive)
    executive_responsible_root_path
  end

  def after_sign_out_path_for(resource_or_scope)
    executive_responsible_root_path
  end
end
