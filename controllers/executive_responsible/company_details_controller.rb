class ExecutiveResponsible::CompanyDetailsController < ExecutiveResponsible::BaseController

	has_scope :by_name
  	has_scope :by_cnpj
  	has_scope :by_executive_service
  	has_scope :by_created_at, using: [:initial_date, :final_date]
  	has_scope :by_executive_responsible

	def index
		if current_executive_responsible_executive_responsible.country.iso_initials == "CO"
			@company_details = apply_scopes(CompanyDetail).where(agency_detail_id: nil, country: "CO").includes(:user).page(params[:page]).per(40)
			@companiesOrdened = CompanyDetail.getComapaniesOrdenedInMonth(@company_details)
		else
			@company_details = apply_scopes(CompanyDetail).where(agency_detail_id: nil).includes(:user).page(params[:page]).per(40)
			@companiesOrdened = CompanyDetail.getComapaniesOrdenedInMonth(@company_details)
		end
	end

	def update
		@company = CompanyDetail.find(params[:id])

		@company.already_received_registration_form = params[:company_detail][:already_received_registration_form] == '1' ? true : false
		@company.already_received_signed_contract = params[:company_detail][:already_received_signed_contract] == '1' ? true : false
		@company.already_aligned_campaigns = params[:company_detail][:already_aligned_campaigns] == '1' ? true : false
		@company.executive_service = params[:company_detail][:executive_service]
		@company.save

		@user = User.find(@company.user_id)
		if @user.blank?
			redirect_to executive_responsible_company_details_path
		else
			if params[:user] != nil
				@user.accept_confirmed_and_invoiced = params[:user][:accept_confirmed_and_invoiced] == '1' ? true : false
				@user.save
			end
			redirect_to executive_responsible_company_detail_path(@company)
		end
	end

	def edit
		@company_detail = CompanyDetail.find(params[:id])
		if @company_detail.blank?
			redirect_to executive_respoonsible_company_details_path
		end
	end

	def show
		@company_detail = CompanyDetail.find(params[:id])
	end

	def authenticate_as
		@company = CompanyDetail.find(params[:id])
		@user = User.find(@company.user_id)
		sign_in @user, :bypass => true
		redirect_to public_root_path
	end

end
