# encoding: utf-8
class ExecutiveResponsible::BookingsController < ExecutiveResponsible::BaseController
  actions :index, :show, :cancel
  has_scope :by_checkin_date, using: [:initial_date, :final_date]
  has_scope :by_status
  has_scope :by_pack_length, type: :array
  has_scope :by_guest_name
  has_scope :by_hotel_id
  has_scope :by_id
  has_scope :by_actual_month
  has_scope :by_last_month
  has_scope :by_delayed_month
  has_scope :by_checkout_delay
  has_scope :by_refund_error
  has_scope :by_agency_creation
  has_scope :by_multiplus_code
  has_scope :by_meeting_room_offer
  has_scope :by_event_room_offer
  has_scope :by_room_offer
  has_scope :by_user_id
  before_action :set_booking, only: [:hotel_accept_request, :hotel_deny_request, :show]

  def index
    case params[:commit]
    when t(:export_data)
      send_data(BookingsReport.new(collection.per(500)).export.to_stream.read,
        filename: "reserva_#{Time.now.strftime('%m%d%Y%k%M%S')}.xlsx",
        type:     "application/xlsx")
    else
      super
    end
  end


  def hotel_accept_request
    if @booking.status == Booking::ACCEPTED_STATUSES[:requested_reservation]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
    elsif @booking.status == Booking::ACCEPTED_STATUSES[:requested_and_paid_reservation]
      @booking.status = Booking::ACCEPTED_STATUSES[:confirmed_and_invoiced]
    end
    @booking.save! if @booking.valid?
    UserMailer.booking_request_accepted(@booking).deliver
    redirect_to admin_bookings_path
  end

  def hotel_deny_request
    @booking.status = Booking::ACCEPTED_STATUSES[:canceled]
    @booking.save! if @booking.valid?
    UserMailer.booking_request_denied(@booking).deliver
    redirect_to admin_bookings_path
  end

  def cancel
    @booking = Booking.find(params[:id])

    booking_cancellation_response = BookingCancellationSrv.call(booking: @booking, admin_cancelation: true)
    if booking_cancellation_response.success
      flash[:notice] = booking_cancellation_response.message
    else
      flash[:error] = booking_cancellation_response.message
    end

    redirect_to executive_responsible_bookings_path
  end

  private

  protected

    def collection
      @bookings ||= apply_scopes(Booking).includes(:hotel,:user,:offer,:omnibees_offer).where(user_id: (User.where(email: "affiliate_ws_api_system@hotelquando.com").last.try(:id))).order('id DESC').page(params[:page]).per(40)
    end

    def set_booking
      @booking = Booking.find_by id: params[:id]
    end

end
