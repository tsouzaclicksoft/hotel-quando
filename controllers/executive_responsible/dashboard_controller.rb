class ExecutiveResponsible::DashboardController < ExecutiveResponsible::BaseController

	def index
		@executive = current_executive_responsible_executive_responsible

		GoalsExecutiveResponsible.verify_goals_in_progress(@executive)
		
		@goal = GoalsExecutiveResponsible.getGoalExecutive(@executive.id)
		
		if @goal != nil

			GoalsExecutiveResponsible.verify_goals_completed(@goal)

			@count_dones_daily = GoalsExecutiveResponsible.get_dones_goals_daily(@executive)
			@count_dones_meeting_weeks = GoalsExecutiveResponsible.get_dones_goal_meeting_weeks(@executive)
			@count_dones_company_weeks = GoalsExecutiveResponsible.get_dones_goal_company_weeks(@executive)
			
			@count_daily = calc_goal_daily(@goal)
			@count_meeting_weeks = calc_goal_meeting_weeks(@goal)
			@count_new_companies_weeks = calc_goal_new_companies_weeks(@goal)
		else
			GoalsExecutiveResponsible.new_goals_executive_responsible(@executive)
			redirect_to executive_responsible_root_path
		end
	end

	def calc_goal_daily(goal)

		@days_month = GoalsExecutiveResponsible.count_days_to_finish_the_month

		total = (@goal.amount_contacts.to_f - @goal.amount_contacts_done) / @days_month

		decimal = total-total.to_i

		if decimal > 0
			return total.ceil
		else
			return total.to_i
		end
	end

	def calc_goal_meeting_weeks(goal)
		@weeks_month = GoalsExecutiveResponsible.count_weeks_month

		total = (@goal.amount_meeting.to_f - @goal.amount_meeting_done) / @weeks_month

		decimal = total-total.to_i

		if decimal > 0
			return total.ceil
		else
			return total.to_i
		end
	end

	def calc_goal_new_companies_weeks(goal)
		@weeks_month = GoalsExecutiveResponsible.count_weeks_month

		total = (@goal.amount_new_companies.to_f - @goal.amount_new_companies_done) / @weeks_month

		decimal = total-total.to_i

		if decimal > 0
			return total.ceil
		else
			return total.to_i
		end
	end

end
