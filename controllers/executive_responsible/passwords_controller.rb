# encoding: utf-8
class ExecutiveResponsible::PasswordsController < Devise::PasswordsController
  layout 'executive_responsible/login'

  def create
    change_login_for_email_in_params
    super
  end

  def after_resetting_password_path_for(executive)
    executive_responsible_root_path
  end

  private
  def change_login_for_email_in_params
    params[resource_name]['email'] = resource_class.find_by_login(params[resource_name]['login']).email
    params[resource_name].delete 'login'
  end
end
