# encoding: utf-8
class ExecutiveResponsible::BaseController < ActionController::Base
  protect_from_forgery
  inherit_resources
  layout 'executive_responsible'
  before_filter :authenticate_executive_responsible_executive_responsible!, :set_locale, :set_locale_from_header

  protected

    def begin_of_association_chain
      current_executive_responsible_executive_responsible
    end

    def getCurrentExecutive
      return current_executive_responsible_executive_responsible
    end

    def set_locale
      unless action_name == 'timezone'
        if params[:locale] == 'pt_br' || params[:lang] == 'pt-br'
          cookies[:locale] = params[:locale]
        elsif params[:locale] == 'en' || params[:locale] == 'es'
          cookies[:locale] = params[:locale]
        else
          if cookies[:locale].blank?
            cookies[:locale] = set_locale_from_header
          end
        end
      end

      I18n.locale = I18n.available_locales.include?(cookies[:locale].try(:to_sym)) ? cookies[:locale] : I18n.default_locale
    end
    def set_locale_from_header
      request.env['HTTP_ACCEPT_LANGUAGE'].gsub! 'pt-BR', 'pt_BR' unless request.env['HTTP_ACCEPT_LANGUAGE'].blank?
      return http_accept_language.compatible_language_from(I18n.available_locales) || 'en'
    end
end
