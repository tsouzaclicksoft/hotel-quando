class ExecutiveResponsible::CompanyRegionsController < ExecutiveResponsible::BaseController

	def create
		prepare_params
		create!
	end

	def update
		prepare_params
		update!
	end

	def activate
		begin
			CompanyRegion.find(params[:id]).activate!
			flash['success'] = I18n.t('views.company_universe.region_controller.activate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.region_controller.activate_error')
		end
		redirect_to :back
	end

	def deactivate
		begin
			CompanyRegion.find(params[:id]).deactivate!
			flash['success'] = I18n.t('views.company_universe.region_controller..deactivate_success')
		rescue
			flash['error'] = I18n.t('views.company_universe.region_controller..deactivate_error')
		end
		redirect_to :back
	end

	def build_resource_params
		[params.fetch(:company_region, {}).permit!]
	end


	def prepare_params
		unless params[:citiesHash].blank?
			cities_hash = params[:citiesHash].split(', ')
			params[:company_region][:city_id]  = cities_hash[0]
		end
	end

	protected
	def end_of_association_chain
		return CompanyRegion.all
	end

	def collection
		@company_regions ||= end_of_association_chain.includes(:city).page(params[:page]).per(40)
	end

end
