# This file is used by Rack-based servers to start the application.
require ::File.expand_path('../config/environment',  __FILE__)
if ENV['RACK_ENV'].to_sym == :production
  use Rack::CanonicalHost, 'www.hotelquando.com.br', if: ["www.hotelquando.com.br", "hotelquando.com.br"]
end
run Rails.application
