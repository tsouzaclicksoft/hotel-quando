params = {
  :number_of_guests => offer.number_of_guests,
  :check_in_datetime => offer.checkin_timestamp,
  :pack_in_hours => offer.pack_in_hours,
  :hotel_codes => [offer.hotel.omnibees_code]
}

c = Omnibees::Client.new
res = c.send(:call_action, :get_hotel_avail, xml)


xml_availabilities = ::Omnibees::Client.new.get_offers(params)
offers = ::Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_availabilities).to_offers(params)
