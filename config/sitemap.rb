# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://staging.hotelquando.com/"
SitemapGenerator::Sitemap.adapter = SitemapGenerator::S3Adapter.new(fog_provider: 'AWS',
                                         aws_access_key_id: ENV['AWS_ACCESS_KEY_ID'],
                                         aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
                                         fog_directory: ENV['S3_BUCKET'],
                                         fog_region: ENV['AWS_REGION'])
 SitemapGenerator::Sitemap.sitemaps_host = "https://s3-#{ENV['AWS_REGION']}.amazonaws.com/#{ENV['S3_BUCKET']}/"
 SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'
 SitemapGenerator::Sitemap.public_path = 'tmp/'

SitemapGenerator::Sitemap.create do
  # Add home path
  add public_root_pt_br_path
  add public_root_en_path
  add public_root_es_path

  # Add newsletter path
  add public_newsletters_path
  add new_public_newsletter_path

  # Add basic search path
  add public_hotels_pt_br_path
  add public_hotels_en_path
  add public_hotels_es_path

  # Add search with each hotel
  Hotel.all.each do |hotel|
    add public_hotel_pt_br_path(id: hotel.id, name: hotel.name), :lastmod => hotel.updated_at
    add public_hotel_en_path(id: hotel.id, name: hotel.name), :lastmod => hotel.updated_at
    add public_hotel_es_path(id: hotel.id, name: hotel.name), :lastmod => hotel.updated_at
  end

  # Add room_type in search
  RoomType.all.each do |room_type|
    add public_hotel_room_type_show_pt_br_path(hotel_id: room_type.hotel_id, hotel_name: room_type.hotel.name, id: room_type.id), :lastmod => room_type.updated_at
    add public_hotel_room_type_show_en_path(hotel_id: room_type.hotel_id, hotel_name: room_type.hotel.name, id: room_type.id), :lastmod => room_type.updated_at
    add public_hotel_room_type_show_es_path(hotel_id: room_type.hotel_id, hotel_name: room_type.hotel.name, id: room_type.id), :lastmod => room_type.updated_at
  end

  # Add public bookings path
  add public_bookings_pt_br_path
  add public_bookings_en_path
  add public_bookings_es_path

  # Add new public bookings path
  add new_public_booking_pt_br_path
  add new_public_booking_en_path
  add new_public_booking_es_path

  # Add new public company path
  add new_public_company_detail_pt_br_path
  add new_public_company_detail_en_path
  add new_public_company_detail_es_path

  # Add public consultants path
  add public_consultants_pt_br_path
  add public_consultants_en_path
  add public_consultants_es_path

  # Add new consultant path
  add new_public_consultant_pt_br_path
  add new_public_consultant_en_path
  add new_public_consultant_es_path

  # Add new public user path
  add new_public_user_session_pt_br_path
  add new_public_user_session_en_path
  add new_public_user_session_es_path

  # Add public user path
  add public_user_session_pt_br_path
  add public_user_session_en_path
  add public_user_session_es_path

  # Add public user password path
  add public_user_password_pt_br_path
  add public_user_password_en_path
  add public_user_password_es_path

  # Add new password path
  add new_public_user_password_pt_br_path
  add new_public_user_password_en_path
  add new_public_user_password_es_path

  # Add edit public password
  add edit_public_user_password_pt_br_path
  add edit_public_user_password_en_path
  add edit_public_user_password_es_path

  # Add cancel public registration
  add add cancel_public_user_registration_pt_br_path
  add add cancel_public_user_registration_en_path
  add add cancel_public_user_registration_es_path

  # Add new public registration
  add new_public_user_registration_pt_br_path
  add new_public_user_registration_en_path
  add new_public_user_registration_es_path

  # Add edit public registration
  add edit_public_user_registration_pt_br_path
  add edit_public_user_registration_en_path
  add edit_public_user_registration_es_path

  # Add public user profile
  add public_user_profile_pt_br_path
  add public_user_profile_en_path
  add public_user_profile_es_path

  # Add public user
  add public_user_pt_br_path
  add public_user_en_path
  add public_user_es_path

  # Add public about
  add public_about_us_pt_br_path
  add public_about_us_en_path
  add public_about_us_es_path

  # Add public how it works
  add public_how_it_works_pt_br_path
  add public_how_it_works_en_path
  add public_how_it_works_es_path

  # Add public terms of use
  add public_terms_of_use_pt_br_path
  add public_terms_of_use_en_path
  add public_terms_of_use_es_path

  # Add public faq
  add public_faq_pt_br_path
  add public_faq_en_path
  add public_faq_es_path

  # Add public booking pack contracting company
  add public_booking_pack_contracting_company_pt_br_path
  add public_booking_pack_contracting_company_en_path
  add public_booking_pack_contracting_company_es_path

  # Add public business trips
  add public_get_business_trips_pt_br_path
  add public_get_business_trips_en_path
  add public_get_business_trips_es_path

  # Add public be our partner
  add public_get_be_our_partner_pt_br_path
  add public_get_be_our_partner_en_path
  add public_get_be_our_partner_es_path

  # Add public privacy policy
  add public_privacy_policy_pt_br_path
  add public_privacy_policy_en_path
  add public_privacy_policy_es_path

  # Add public customer service
  add public_customer_service_pt_br_path
  add public_customer_service_en_path
  add public_customer_service_es_path

  # Add public tudo azul
  add public_tudo_azul_pt_br_path
  add public_tudo_azul_en_path
  add public_tudo_azul_es_path

  # Add public multiplus
  add public_multiplus_pt_br_path
  add public_multiplus_en_path
  add public_multiplus_es_path

  # Add public airports
  add public_airports_pt_br_path
  add public_airports_en_path
  add public_airports_es_path

  Place.all.each do |place|
    add public_airport_pt_br_path(id: place.id)
    add public_airport_en_path(id: place.id)
    add public_airport_es_path(id: place.id)

    add public_airport_hotel_pt_br_path(id: place.id, hotel_id: place.hotel_id)
    add public_airport_hotel_en_path(id: place.id, hotel_id: place.hotel_id)
    add public_airport_hotel_es_path(id: place.id, hotel_id: place.hotel_id)
  end

  # Add public affiliate root
  add public_affiliate_root_pt_br_path
  add public_affiliate_root_en_path
  add public_affiliate_root_es_path

  # Add public affiliate hotels
  add public_affiliate_hotels_pt_br_path
  add public_affiliate_hotels_en_path
  add public_affiliate_hotels_es_path

  # Add public affiliate user registration
  add public_affiliate_user_registration_pt_br_path
  add public_affiliate_user_registration_en_path
  add public_affiliate_user_registration_es_path

  # Add public affiliate about us
  add public_affiliate_about_us_pt_br_path
  add public_affiliate_about_us_en_path
  add public_affiliate_about_us_es_path

  # Add public affiliate how it workds
  add public_affiliate_how_it_works_pt_br_path
  add public_affiliate_how_it_works_en_path
  add public_affiliate_how_it_works_es_path

  # Add public affiliate terms of use
  add public_affiliate_terms_of_use_pt_br_path
  add public_affiliate_terms_of_use_en_path
  add public_affiliate_terms_of_use_es_path

  # Add public affiliate faq
  add public_affiliate_faq_pt_br_path
  add public_affiliate_faq_en_path
  add public_affiliate_faq_es_path

  # Add public affiliate business trips
  add public_affiliate_get_business_trips_pt_br_path
  add public_affiliate_get_business_trips_en_path
  add public_affiliate_get_business_trips_es_path

  # Add public affiliate be our partner
  add public_affiliate_get_be_our_partner_pt_br_path
  add public_affiliate_get_be_our_partner_en_path
  add public_affiliate_get_be_our_partner_es_path

  # Add public affiliate privacy policy
  add public_affiliate_privacy_policy_pt_br_path
  add public_affiliate_privacy_policy_en_path
  add public_affiliate_privacy_policy_es_path

  # Add public affiliate custumer service
  add public_affiliate_customer_service_pt_br_path
  add public_affiliate_customer_service_en_path
  add public_affiliate_customer_service_es_path

  # Add public affiliate tudo azul
  add public_affiliate_tudo_azul_pt_br_path
  add public_affiliate_tudo_azul_en_path
  add public_affiliate_tudo_azul_es_path

  # Add public affiliate multiplus
  add public_affiliate_multiplus_pt_br_path
  add public_affiliate_multiplus_en_path
  add public_affiliate_multiplus_es_path
end
