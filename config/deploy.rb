﻿# config valid only for current version of Capistrano
lock '3.5.0'

set :repo_url, 'git@bitbucket.org:hotelquando2015/hotelquando.git'
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

set :user, 'deployer'
set :application, 'hotel-quando'
set :rails_env, 'production'
set :branch, 'master'

set :default_env => {
	'ADMIN_PANEL_USER_LOGIN' => 'manager',
	'ADMIN_PANEL_USER_PASSWORD' => 'mcmaximo',
	'AWS_ACCESS_KEY_ID' => 'AKIAJO3XJ5VM33TB72OA',
	'AWS_SECRET_ACCESS_KEY' => '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',
	'SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT' => 'true',
	'MP_APIKEY' => 'vxqiq3ihjrqzqyb7ngyx06ql ',
	'MP_ID' => '4740',
	'GOOGLE_MAPS_API_KEY' => 'AIzaSyCAjw6VyR_HEeKsHQyEPikFh1ErDMQ60Bs',
	'OMNIBEES_PROXY_ADDRESS' => 'https://proxy:adcf7f7a578e-45f5-b524-a388f18e50d1@proxy-174-129-240-180.proximo.io',
	'OMNIBEES_WSDL_ENDPOINT' => 'https://pull.omnibees.com?wsdl',
	'OMNIBEES_PASSWORD' => '4qKGp7pHJRCRXj8K',
	'OMNIBEES_USERNAME' => 'hotelquando',
	'HOTEISNET_PASSWORD' => 'tqwgXO5M4EcMYrR',
	'HOTEISNET_USERNAME' => 'rhbquando',
	'AIRBRAKE_API_KEY' => '830028d2be9968ecb41411c080e72f01',
	'EGOI_API_USER' => 'nicholas.hortegas@hotelquando.com',
	'EGOI_API_KEY' => '3006',
	'MANDRIL_API_KEY' => 'PN-SLKrDxL6p56Z-wNgkHA',
	'MANDRIL_API_USER' => 'max@hotelquando.com',
	'APP_DATABASE_USERNAME' => 'postgres',
	'APP_DATABASE_PASSWORD' => 'postgres',
	'APP_DATABASE_PROD_DB_NAME' => 'hotelquando5',
	'APP_DATABASE_PROD_USER' => 'postgres',
	'APP_DATABASE_PROD_PWD' => 'pfecW5JQ2jyEF4Y2',
	'APP_DATABASE_PROD_HOST' => '172.31.0.28',
	'APP_DATABASE_PROD_PORT' => '5972',
	'SECRET_KEY_BASE' => '99b63cb40c3464c02e4b8db04f905679eba7ceaa77a99778abef04104d3fda856ac690aad7aad5dea6bd764c207dc6c600d507207d21ccc95d7139f2cb88d2e',
	'RACK_ENV' => 'production',
	'RAILS_ENV' => 'production'
}


server '54.207.12.218', user: "#{fetch(:user)}", roles: %w{app db web}, primary: true
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :pty, true

set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml', 'config/puma.rb')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')

set :config_example_suffix, '.example'
set :config_files, %w{config/database.yml config/secrets.yml}
set :puma_conf, "#{shared_path}/config/puma.rb"

namespace :deploy do
	desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc "No ActiveRecord override"
  task :create do
  	desc "No creation database permitted"
  end
  task :migrate do
  	ENV['RAILS_ENV']
  	ENV['APP_DATABASE_PROD_HOST']
  end

  #before 'check:linked_files', 'config:push'
  #before 'check:linked_files', 'puma:config'
  #before 'check:linked_files', 'puma:nginx_config'
  #before 'deploy:migrate', 'deploy:db:create'
  #after 'puma:smart_restart', 'nginx:restart'
end
