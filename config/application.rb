# encoding: utf-8
require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module HotelQuando

  ACTIVE_AFFILIATES_LAYOUT_NAMES = ["default", "copa_sul_hotel", "hotelurbano2", "vert", "nobile", "transamerica", "bourbon", "baseconcepthotel", "hotelurbano", "linxconfins", "linxriodejaneiro", "nobile", "prodigyberrini", "prodigyconfins", "prodigyhotel", "prodigyrecife", "atlantica", "estelar", "total_hotel", "aerosleep", "bourbonalphaville", "bourbonbarra", "bourbonbatel", "bourboncascavel", "bourboncuritiba", "bourbonjoinville", "bourbonpontagrossa", "bourbonsaopaulo", "bourbonvitoria", "saojorge", "druds", "palmleaf_grand_premium", "palmleaf_slim"].freeze


  class Application < Rails::Application
    config.middleware.insert_before ActionDispatch::Static, Rack::Deflater
    config.middleware.insert_before ActionDispatch::Static, Rack::Cors do
      allow do
        origins '*'
        resource '/assets/*',
        headers: :any,
        methods: [:get, :options]
      end
    end

    # adding cookie store back to rails
    config.api_only = false

    config.action_dispatch.default_headers = {
        'X-Frame-Options' => 'ALLOWALL'
    }

    config.time_zone = 'Brasilia'
    config.log_level = :error
    config.i18n.enforce_available_locales = true
    config.i18n.available_locales = [:pt_br, :en, :es]
    config.i18n.locale = :pt_br
    config.i18n.default_locale = :pt_br

    config.i18n.load_path = [
      Dir[File.join(Rails.root, 'app', 'locales', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'app', 'locales', 'admin', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'app', 'locales', 'hotel', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'app', 'locales', 'public', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'app', 'locales', 'routes','public', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'controllers', 'admin', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'controllers', 'hotel_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'controllers', 'admin_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'controllers', 'hotel', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'mailers', 'admin', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'mailers', 'agency_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'mailers', 'hotel_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'mailers', 'admin_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'mailers', 'referred_friend_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'admin', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'agency_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'admin_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'hotel_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'user_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'referred_friend_mailer', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'hotel', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'nobile', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'transamerica', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'vert', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'atlantica', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'estelar', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'bourbon', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'hotelurbano2', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'copa_sul_hotel', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'total_hotel', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'aerosleep', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'default', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'saojorge', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'affiliates', 'druds', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'company_universe', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'executive_responsible', '*.{rb,yml}')],
      Dir[File.join(Rails.root, 'config', 'locales', 'views', 'travel_request', '*.{rb,yml}')]
    ].flatten

    config.encoding = "utf-8"

    config.filter_parameters += [:password, :password_confirmation, :temporary_number, :security_code]

    #assets
    config.assets.paths << "#{Rails.root}/app/assets/fonts"
    config.assets.paths << "#{Rails.root}/vendor/assets/fonts"
    config.assets.precompile += %w( *.svg *.eot *.woff *.ttf )
    config.assets.precompile += %w[ *.png *.jpeg *.jpg *.gif ]
    config.assets.precompile += [ 'init_admin.js', 'init_hotel.js', 'init_public.js', 'init_agency.js', 'init_company.js','init_login.js','init_executive_responsible.js' ]
    config.assets.precompile += [ 'js_routes/init_admin.js', 'js_routes/init_agency.js', 'js_routes/init_company.js', 'js_routes/init_hotel.js', 'js_routes/init_executive_responsible.js']
    config.assets.precompile += [ 'ace_admin/init1.css', 'ace_admin/init1.css', 'init_admin.css', 'init_hotel.css', 'init_public.css', 'init_login.css', 'init_agency.css', 'init_company.css' ]

    config.assets.precompile +=  ACTIVE_AFFILIATES_LAYOUT_NAMES.map do |affiliate_layout_name|
      ["init_public_affiliate_#{affiliate_layout_name}.js", "init_public_affiliate_#{affiliate_layout_name}.css"]
    end.flatten

    config.generators do |g|
      g.view_specs false
      g.helper_specs false
      g.stylesheets = false
      g.javascripts = false
      g.helper = false
      g.template_engine :slim
      g.test_framework  :rspec, :fixture_replacement => :factory_girl
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
    end

    config.action_controller.include_all_helpers = false
    config.middleware.insert_before 0, "Rack::Cors" do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :options]
      end
    end

  end
end
