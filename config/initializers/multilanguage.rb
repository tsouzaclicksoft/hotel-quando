# encoding: utf-8
module Kernel
  def execute_multilanguage_block_for(attribute_name)
    accepted_languages.each do |lang|
      [:"#{attribute_name}_#{lang}"].each do |language_attribute|
        yield(language_attribute, lang)
      end
    end
  end

  def accepted_languages
    [:pt_br, :en, :es]
  end

  alias :multil :execute_multilanguage_block_for
end