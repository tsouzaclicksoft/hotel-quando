if ENV['AIRBRAKE_API_KEY'].present?
  Airbrake.configure do |config|
    config.api_key = ENV['AIRBRAKE_API_KEY']
  end
elsif !Rails.env.production?
  module Airbrake
    def self.notify(ex)
      raise ex
    end
  end
end
