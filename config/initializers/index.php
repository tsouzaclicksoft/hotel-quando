<!DOCTYPE html>
<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once 'database.php';
?>

<html>
<head>
  <title>Rosita</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css">



  <!-- inicio yasmim -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <link rel="stylesheet" href="jquery-ui.min.css">

  <?php require_once 'favicon.php';?>

</head>
<body>
  <div class="container">

    <form id="order-form" action="form.php" method="post">
      <div class="user_form">
        <div class="logo_top">
        </div>

        <p class="t_center title_1">Encomenda de Fim de Ano</p>
        <p class="t_center title_2">SEUS DADOS</p>

        <div>
         <label for="name">Nome </label>
         <input type="text" name="name">

       </div>
       <div class="cpf">
         <label for="name">CPF </label>
         <input type="text" name="cpf" required placeholder=" Somente números" pattern="\d*" maxlength='11'>
       </div>

       <div>
        <label for="email">Email</label>
        <input type="email" name="email" value="" required>
      </div>
      <div>
        <label for="telefone">Telefone</label>
        <input type="tel" id="telefone-fixo" name="telefone" class="telefone">
      </div>
      <div>
        <label for="celular">Celular</label>
        <input type="tel" id="celular" name="celular" class="celular">
      </div>
      <div>
        <label for="date_retrieve">Dia da retirada</label>
        <input type="text" id="date_retrieve" name="date_retrieve" required>
      </div>
      <div>
        <label for="time_retrieve">Hora da retirada</label>
        <input type="text" id="time_retrieve" data-format="HH:mm" data-template="HH : mm" name="time_retrieve">
        <!-- <input type="text" id="time_retrieve" name="time_retrieve" required> -->
      </div>

      <div>
        <label for="total_people">Ceia para quantas pessoas</label>
        <input type="number" min="0" name="total_people" required>
      </div>

      <div>

        <label for="forma_pagamento">Formas de Pagamento:</label>
        <select id="forma_pagamento" type="select" name="forma_pagamento" required>
          <option value="">Selecione</option>
          <option value="Pagseguro">Pagseguro</option>
          <!-- <option value="Na loja - Cartão - 50%">Na loja - Cartão - 50%</option>
          <option value="Na loja - Cartão - 100%">Na loja - Cartão - 100%</option>
          <option value="Na loja - Dinheiro - 50%">Na loja - Dinheiro - 50%</option>
          <option value="Na loja - Dinheiro - 100%">Na loja - Dinheiro - 100%</option> -->
        </select>
        <br><br>
        <!--p align="justify"><span>*<span>A sua reserva será confirmada mediante pagamento de 50% do valor total da proposta.</p-->

      </div>
      <!--div id="payment-informations" style="display:none">
        <p>
          Dados Para Depósito Bancário:<br>
          Rasão Social: Rosita Perto De Voce Eireli Me<br>
          Banco: Santander 033<br>
          Agencia: 3937<br>
          CC.: 13002315-3<br>
          CNPJ: 23.956.315/0001-02
        </p>
      </div-->
      <div id="mensagem-pagamento" style="display:none">
        <p>
          *Pedido só será confirmado apóis o pagamento de 50%;
        </p>
      </div>

    </div>

    <p class="t_center f_color title_3">
      Cardápio
    </p>

    <div class="itens_form">
      <?php

$cont = 0;

if ($conn) {
    $last_title = '';
    //echo "<br>ok";
    $stmt = $conn->prepare("SELECT pr.titulo as pr_titulo, pr.produto_id as pr_produto_id , c.categoria_id as c_id, c.titulo as c_titulo, pr.valor as pr_valor,pr.tipo_distribuicao as pr_tipo_distribuicao
          FROM produto pr JOIN categoria c ON c.categoria_id = pr.categoria_id ORDER BY c.ordem , pr.ordem ;");
    $stmt->execute();

    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

    $product_title = '';
    $categoria_title = '';
    $float_class = 'int';
    $step = '';
    $sem_acompanhamento = 'sem_acompanhamento';

    while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {

        if (strpos($result['pr_tipo_distribuicao'], '0,1') !== false) {
            $step = "step='0.1'";
            $float_class = '';
        } else if (strpos($result['pr_tipo_distribuicao'], '0,25') !== false) {
            $step = "step='0.25'";
            $float_class = '';
        } else if (strpos($result['pr_tipo_distribuicao'], '0,5') !== false) {
            $step = "step='0.05'";
            $float_class = '';
        } else {
            $step = "step='1'";
            $float_class = 'int';
        }

        if (strpos($result['pr_titulo'], 'Peru de Natal') !== false) {
            $sem_acompanhamento = 'sem_acompanhamento';
            $step = "step='1'";
            $float_class = 'int';
        } else {
            $sem_acompanhamento = '';
        }

        if ($result['c_titulo'] != $categoria_title) {

            if ($cont > 0) {

                echo "</div>";
            }
        }

        if ($result['c_titulo'] != $categoria_title) {
            $categoria_title = $result['c_titulo'];

            $cont++;

            if ($categoria_title == "Terrines e Patês (aprox. 500g)" || $categoria_title == "Quiches (12) fatias") {
                echo " <p class=\"t_center f_color categoria_title subtitle\">  {$result['c_titulo']} </p> ";
            } else {
                echo " <p class=\"t_center f_color categoria_title \">  {$result['c_titulo']} </p> ";
            }
            echo "<div class= \"itens_categoria itens_categoria_{$result['c_id']}\">";
            echo "<div class= \"categoria_description\">";
            echo "<span class=\"span1 f_color_grey f_bold\">PREÇO UNIT</span> <span class=\"span2 f_color_grey f_bold\">QUANT</span> <span class=\"span3 f_color_grey f_bold\">SUB-TOTAL</span> <span class=\"span4 f_color_grey f_bold\">DESCRIÇÂO</span>";
            echo "</div>";
        }

        echo "<div class=\"itens\">";
        echo "<span class=\"f_color_grey f_bold\">R$</span> <span class=\"f_color_grey f_bold item{$result['pr_produto_id']} item_valor\">" . number_format($result['pr_valor'], 2, '.', '') . "</span>";
        //echo "<select name= \"{$result['pr_produto_id']}\" >" . $option . "</select>";
        if ($categoria_title == "Pratos principais <span>(vem com um acompanhamento a escolher)</span>") {
            echo "<input type='number' id='{$result['pr_produto_id']}' min='0' {$step}  class=\"qtd {$float_class} prato_principal {$sem_acompanhamento}  \" placeholder=\"0\"  name= \"itens[{$result['pr_produto_id']}]\">";
        } else if ($categoria_title == "Acompanhamentos do prato principal") {
            echo "<input type='number' id='{$result['pr_produto_id']}' min='0' {$step}  class=\"qtd {$float_class} acompanhamento \" placeholder=\"0\"  name= \"itens[{$result['pr_produto_id']}]\">";
        } else {
            echo "<input type='number' id='{$result['pr_produto_id']}' min='0' {$step} class=\"qtd {$float_class} \" placeholder=\"0\"  name= \"itens[{$result['pr_produto_id']}]\">";
        }

        echo "<input type='hidden' name=\"itens[item{$result['pr_produto_id']}]\" value=\"{$result['pr_titulo']}\">";

        echo "<span class=\"f_bold subtotal_{$result['pr_produto_id']} subtotal f_color\">R$ 0.00</span>";

        echo "<label class=\"f_color_darker_grey\" for=\"{$result['pr_produto_id']}\"> {$result['pr_titulo']}";
        echo "</label>";
        echo "<span id='div-botao-produto-{$result['pr_produto_id']}' class='col-lg-1 flutuacao-direita'>

      <button id='produto-{$result['pr_produto_id']}' type='button' class='btn btn-default' aria-label='Left Align'>
        <span class='glyphicon glyphicon-info-sign' aria-hidden='true'></span>
      </button>
     </span>";

        echo
            "<div style='display:none;' class='produto-{$result['pr_produto_id']} col-lg-1 flutuacao-direita'>

        <div class='form-group'>

          <div class='input-group'>

            <input type='text' name='info_adicionais[{$result['pr_titulo']}]' class='form-control information' id='item-obs-{$result['pr_produto_id']}' placeholder='Observação'>

            <input type='hidden' class='item-obs-{$result['pr_produto_id']}' name='item-observations[{$result['pr_produto_id']}]'>

          </div>
        </div>

      </div>";


        if ($result['pr_titulo'] == 'Tábua de mini sanduíches recheados (média)' || $result['pr_titulo'] == 'Tábua de mini sanduíches recheados (grande)') {

            echo
                "<div style='display:none;' class=\"hidden-{$result['pr_produto_id']}\">
      <span id='opcoes'>
      <br>
      <span><strong>Opções de recheios:</strong>
      <br>Brie com geléia de damasco, parma com pasta de tomate seco,
      peito de peru defumado com pasta de ricota e ervas, lombo canadense com pasta
      de provolone, salaminho com pasta de gorgonzola, presunto com pasta de azeitona
      <br><br><strong>Opções de pães:</strong>
      <br>Baguete simples, baguete gergelim, multigrão (podem escolher os dois tipos)</span>
      <br><br>
      <span class='rediT'>*Favor descrever as opções no botão de informações ao lado.</span>
     </div>";

        }

        echo "</div>";
    }

    echo "</div>";

    // and now we're done; close it
    $conn = null;
} else {
    echo "<br>error";
}
?>
<br>

<div class="itens_categoria ">
  <p id="desconto_acompanhamento">
    Desconto no acompanhamento: R$ 0
  </p>
  <p id="valor_total">
    Total
  </p>
  <br><br>
  <input id="field_total" type="hidden" name="field_total">
  <input id="field_desconto" type="hidden" name="desconto_acompanhamento">
  <div class="itens itens_recomentacoes">
    <p class="f_color_grey f_bold item88 ">Retiradas: Av. das Américas, 500 - bloco 21, loja 126</p>
  </div>
  <div class="itens itens_recomentacoes">
    <p class="f_color_grey f_bold item88 ">Encomendas de Natal: 24/12 de 12h às 15h</p>
  </div>
  <div class="itens itens_recomentacoes">
    <p class="f_color_grey f_bold item88 ">Encomendas de Ano Novo: 31/12 de 12h às 15h</p>
  </div>
  </div>
<br>
<div class="padded">
  <label>Observações gerais:</label>
  <br>
  <textarea rows="4" cols="122" id="observation" name="observation"></textarea>
</div>


<div class="bts">
  <input class ="form_bt" type="submit" value="ENVIAR">
</div>

</div>


</form>

</div>
  <footer>

    <script type="text/javascript"  src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

    <script type="text/javascript"  src="script.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js">
    </script>

    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script type="text/javascript"  src="jquery.maskedinput.min.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/combodate.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        document.getElementById("order-form").reset();
        calcSubTotal();
      })
     var opt_padrao = '<option value=""></option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option>';

     var opt_exceptions = '<option value=""></option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option>';

     var opt_exceptions_min = '<option value="00">00</option>';


     var opt_exceptions_min_padrao = '<option value=""></option><option value="0">00</option><option value="1">01</option><option value="2">02</option><option value="3">03</option><option value="4">04</option><option value="5">05</option><option value="6">06</option><option value="7">07</option><option value="8">08</option><option value="9">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option>';


      if ($(".itens_categoria .acompanhamento").length > 0){
        console.log('teste');
        $('.acompanhamento').closest('.itens_categoria').append('<p class="note">&nbsp;&nbsp;(O valor dos acompanhamentos vinculados aos pratos será descontado no final desta página.)</p>');
      }

      $(document).ready(function(){
        $( ".itens_categoria" ).each(function( index ) {
          if($(this).hasClass('itens_categoria_5') || $(this).hasClass('itens_categoria_7') || $(this).hasClass('itens_categoria_8')) {
            console.log("aasdfasdf");
            $(this).append('<span class="span1 f_color_grey f_bold pirex">* Já vem no pirex</span>');
          }
        });


        $("button").click(function(){


          //console.log($(this).attr("id"));

          id_produto = $(this).attr("id");

          $("."+id_produto).slideDown("300000");

          $("#div-botao-"+id_produto).fadeOut();

           console.log(id_produto);
           console.log("Quem some "+"#div-botao-"+id_produto);

         });
        });

        $(document).ready(function(){
          $("#tirar").click(function(){
          $("#div-input").fadeOut("300000");//tirar
          $(".btn-default").fadeIn("300000");//coloca
        });

      });


      $('#order-form').submit(function() {

          var itemQtdSelect = $('input.qtd').filter(function() {
              return this.value !== "" && this.value !== "0";
          });
          if (itemQtdSelect.length == 0 || itemQtdSelect.length == undefined) {
            alert('Por favor selecione a quantidade de ao menos um item.');
            return false;
          }


          var celular  = $("#celular").val();
          var telefone = $("#telefone-fixo").val();

          if(celular == '' && telefone == '' ){
              alert('Por favor preencha ao menos um dos telefones para contato.');
              telefone.focus();
              return false;

          }

          if(celular == '(__) ____-_____' && telefone == '(__) ____-_____' ){
              alert('Por favor preencha ao menos um dos telefones para contato.');
              telefone.focus();
              return false;

          }

          if(celular == '(__) ____-_____'){
              alert('Por favor verifique o número de celular para contato.');
              celular.focus();
              return false;

          }

          if(telefone == '(__) ____-_____'){
              alert('Por favor verifique o número de telefone para contato.');
              telefone.focus();
              return false;

          }

          $('.information').each(function(){
              var id = $(this).attr('id');
              $('.'+id).val($(this).val());
          })

      });

      $('#forma_pagamento').change(function(){

        var formaPg = $(this).val();
        console.log(formaPg);
        if(formaPg != 'Pagseguro'){
          $('#mensagem-pagamento').slideDown();
          $(".cpf").slideUp();
          $("input[name=cpf]").prop("required", false);
        }else{
          $('#mensagem-pagamento').slideUp();
          $(".cpf").slideDown();
          $("input[name=cpf]").prop("required", true);
        }


      });

      $(document).ready(function(){

       $("#date_retrieve").keypress(function (evt) {
            evt.preventDefault();
        });

        $("#time_retrieve").keypress(function (evt) {
            evt.preventDefault();
        });


        $('#date_retrieve').change(function(){

          // 24/12: de 12h às 15h
          // 31/12: de 12h às 15h
          // Nos outros dias de 12h às 18h

            dateArray = $(this).val();

            // if( dateArray.includes('31/12/')){
            //   console.log('possui');
            // }

            var mes = parseInt(dateArray[1]);
            var dia = parseInt(dateArray[2]);

            // console.log("dia" + dia);
            // console.log("mes" + mes);

            if(dateArray == '24/12/2018'){
                console.log('natal');
               $("#time_retrieve").val('');
               $(".hour")
              .find('option')
              .remove()
              .end()
              .append(opt_exceptions);

            }else if( dateArray == '31/12/2018'){
              console.log('virada');
              $("#time_retrieve").val('');
              $(".hour")
              .find('option')
              .remove()
              .end()
              .append(opt_exceptions);


            }else{
              console.log('exception');
              $("#time_retrieve").val('');
              $(".hour")
              .find('option')
              .remove()
              .end()
              .append(opt_padrao);


            }

            if(dateArray == '25/12/2018'){
              alert('Não realizaremos pedidos no dia 25.');
              $(this).val('');
              return false;
            }

        });
    });
    var c = 0;

    $('input[type=number]').on('focus',function() {
      pdID = $(this).attr('id');
      $('.hidden-'+pdID).fadeIn();
    });

    // $('input[type=number]').blur(function() {
    //   pdID = $(this).attr('id');
    //   $('.'+pdID).fadeOut();
    // })


    $( function(){

      $("#date_retrieve").datepicker({
        dateFormat: 'dd/mm/yy',
        defaultDate: '01-12-2018',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
      });
      $('#date_retrieve').datepicker('option', {minDate: new Date("2018/12/01"), maxDate: new Date("2018/12/31")});


      $('#time_retrieve').combodate({
          minuteStep: 1
      });

      $(".hour")
        .find('option')
        .remove()
        .end()
        .append(opt_padrao);


         $('.hour').change(function(){
            var hora = $(this).val();
            dateArray = $("#date_retrieve").val();


           if( hora == '15' && ((dateArray=='24/12/2018') || (dateArray=='31/12/2018')) ){

                 $(".minute")
                 .find('option')
                 .remove()
                 .end()
                 .append(opt_exceptions_min);
             }else{
                 $(".minute")
                 .find('option')
                 .remove()
                 .end()
                 .append(opt_exceptions_min_padrao);
             }

          });


           var hora = $('.hour').val();
           dateArray = $("#date_retrieve").val();

           if( hora == '15' && ((dateArray=='24/12/2018') || (dateArray=='31/12/2018')) ){

               $(".minute")
               .find('option')
               .remove()
               .end()
               .append(opt_exceptions_min);
            }else{
               $(".minute")
               .find('option')
               .remove()
               .end()
               .append(opt_exceptions_min_padrao);
            }

            $('.hour').attr('required','required');
            $('.minute').attr('required','required');

    });




  </script>

  <footer>
</body>
</html>
