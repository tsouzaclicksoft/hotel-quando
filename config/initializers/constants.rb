# encoding: utf-8
module HotelQuando
  module Constants
    EMAIL_FROM      = "\"HotelQuando\" <naoresponda@hotelquando.net>".freeze
    DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK = {
      '3_hours' => 38,
      '6_hours' => 43,
      '9_hours' => 60,
      '12_hours' => 65,
      '24_hours' => 100
    }
    # hotel need to see reduced price of booking if booking status is 'confirmed and invoiced'
    DEFAULT_REDUCE_PRICE_PERCENTAGE_FOR_EACH_PACK = {
      '3_hours' => 12.5,
      '6_hours' => 11,
      '9_hours' => 10.5,
      '12_hours' => 10
    }

    BUSINESS_ROOM_DEFAULT_PRICING_POLICY_PERCENTAGES_FOR_EACH_PACK = {
      '1_hours' => 12.5,
      '2_hours' => 25.0,
      '3_hours' => 37.5,
      '4_hours' => 50.0,
      '5_hours' => 62.5,
      '6_hours' => 75.0,
      '7_hours' => 87.5,
      '8_hours' => 100,
      '24_hours' => 100
    }
    LENGTH_OF_PACKS = [ 3, 6, 9, 12 ]
    LENGTH_OF_PACKS_OF_HOTELNET = [ 3, 6, 9, 12, 13, 14, 15, 16, 17, 18, 36, 37, 38, 39, 40, 41, 42, 60, 61, 62, 63, 64, 65, 66 ]
    MEETING_ROOM_LENGTH_OF_PACKS = [ 1, 2, 3, 4, 5, 6 ,7 ,8 ]
    EVENT_ROOM_LENGTH_OF_PACKS = [ 2, 4, 8 ]
    ALL_PACKS = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 12 ]
  end
end
