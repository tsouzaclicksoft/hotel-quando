# encoding: utf-8
class DevelopmentMailInterceptor
  def self.delivering_email(message)
    message.subject = "[ #{Rails.env} - #{message.to} - #{message.bcc}] #{message.subject}"
    message.to = "testmail@codus.com.br"
    message.bcc = "testmail@codus.com.br"
  end
end

ActionMailer::Base.register_interceptor(DevelopmentMailInterceptor) if (Rails.env.development? || ENV['INTERCEPTAR_EMAILS'] == 'true')