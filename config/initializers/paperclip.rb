paperclip_config = {
  development: {
    storage: 'filesystem',
    url:  "/uploads/#{Rails.env}/:class/:attachment/:id/:style.:extension",
    default_url: "/paperclip_images/:style/processing.png",
    path: ":rails_root/public/uploads/#{Rails.env}/:class/:attachment/:id/:style.:extension",
      :styles => {
      :big => ["800x800>", :jpg],
      :medium => ["600x600>", :jpg],
      :small => ["350x350>", :jpg],
      :thumb => ["50x50>", :jpg]
    }
  },
  test: {
    storage: 'filesystem',
    url:  "/uploads/#{Rails.env}/:class/:attachment/:id/:style.:extension",
    default_url: "/paperclip_images/:style/processing.png",
    path: ":rails_root/public/uploads/#{Rails.env}/:class/:attachment/:id/:style.:extension",
      :styles => {
      :big => ["800x800>", :jpg],
      :medium => ["600x600>", :jpg],
      :small => ["350x350>", :jpg],
      :thumb => ["50x50>", :jpg]
    }
  },
  production: {
    storage: :s3,
    s3_credentials:       Rails.configuration.aws,
    s3_protocol:          'https',
    url:                  ':s3_domain_url',
    path:                 "uploads/#{Rails.env}/:class/:attachment/:id/:style.:extension",
    bucket: 'hotelquando-prod',
    default_url: "/paperclip_images/:style/processing.png",
      :styles => {
      :big => ["800x800>", :jpg],
      :medium => ["600x600>", :jpg],
      :small => ["350x350>", :jpg]
    }
  },
  staging: {
    storage: :s3,
    s3_credentials:       Rails.configuration.aws,
    s3_protocol:          'https',
    url:                  ':s3_domain_url',
    path:                 "uploads/#{Rails.env}/:class/:attachment/:id/:style.:extension",
    bucket: 'hotelquando-staging',
    default_url: "/paperclip_images/:style/processing.png",
      :styles => {
      :big => ["800x800>", :jpg],
      :medium => ["600x600>", :jpg],
      :small => ["350x350>", :jpg],
      :thumb => ["50x50>", :jpg]
    }
  }
}

Paperclip::Attachment.default_options.merge!(paperclip_config[Rails.env.to_s.to_sym])

Paperclip.options[:log] = false
