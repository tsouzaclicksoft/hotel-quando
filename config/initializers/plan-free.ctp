<?php foreach ($plans as $key => $plan) : ?>
  <div id="plan-id-<?= $plan->id; ?>" class="col-md-3 plan-container">
    <div class="plan-contain">
      <div class="plan-top <?= $plan->id == $user->plan_id ? 'plan-top-active' : ''; ?>">
        <?= $plan->title; ?>
      </div>
      <div class="plan">
        R$ <?= number_format($plan->price, 2, ',', ' '); ?>
      </div>
      <div class="plan-contanin-center">
        <?php if($plan->id == 1) : ?>
          <div class="plan-1"><div class="glyphicon glyphicon-ok tique"></div>5 questões por semana</div>
          <div class="plan-2"><div class="glyphicon glyphicon-ok tique"></div>Acesso por tempo ilimitado</div>
          <div class="plan-1"><div class="glyphicon glyphicon-remove remove"></div>Acesso aos comentários</div>
          <div class="plan-2"><div class="glyphicon glyphicon-remove remove"></div>Acesso aos simulados</div>
        <?php else : ?>
          <div class="plan-1"><div class="glyphicon glyphicon-ok tique"></div>Questões ilimitadas</div>
          <div class="plan-2"><div class="glyphicon glyphicon-ok tique"></div>Acesso por tempo ilimitado</div>
          <div class="plan-1"><div class="glyphicon glyphicon-ok tique"></div>Acesso aos simulados</div>
          <div class="plan-2"><div class="glyphicon glyphicon-ok tique"></div>Acesso aos comentários</div>
        <?php endif; ?>
      </div>
      <div class="plan-footer">
        <div class="plan-footer-item selection <?= $plan->id == $user->plan_id ? 'plan-button-active' : ''; ?>" id="plan-select-<?= $plan->id; ?>" onclick="selectNewPlan('<?= $plan->id; ?>')">
          <?= $plan->id == $user->plan_id ? 'Selecionado' : 'Selecionar'; ?>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
