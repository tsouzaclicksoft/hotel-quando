# encoding: utf-8
require 'sidekiq/web'

Rails.application.routes.draw do

  localized do
    namespace :admin, path_names: { new: 'novo', create: 'novo', edit: 'editar', update: 'editar', remove: 'remover', deactivate: 'desativar'} do

      root :to => 'hotels#index'

      get 'mudar-senha' => 'info#edit', as: :change_password

      patch 'mudar-senha' => 'info#update_password'

      devise_for :managers,
           controllers: { sessions:           'admin/sessions',
                          passwords:          'admin/passwords'
                        },
           path: '/', path_names:{
                                  sign_in: 'entrar',
                                  sign_out: 'sair'
                                 }

      resources :users, path: 'usuarios', only: [:index, :show, :edit, :update]

      resources :emkts, path: 'emkt' do
        collection do
          get :with_bookings
          get :without_bookings
        end
      end

      resources :offers, path: 'ofertas', only: [:index]

      resources :hotel_search_logs, path: 'logs-de-busca', only: [:index]

      get '/usuarios/detalhes_da_reserva/:id', to: 'users#user_booking_show', as: 'user_booking_show'

      resources :payments, path: 'pagamentos', only: [:index, :show]

      resources :bookings, path: 'reservas', only: [:index, :show] do
        member do
          put :cancel
          put :hotel_accept_request
          put :hotel_deny_request
          post :send_confirmation_notice_email
        end
      end

      get 'relatorios' => 'hotel_reports#index', as: :hotel_reports

      get 'relatorios-agencias' => 'agency_reports#index', as: :agency_reports

      resources :campaigns, path: 'campanhas'

      resources :promotional_codes, path: 'codigos-promocionais'

      resources :newsletters, path: 'newsletters', only: [:index] do
        collection do
          get   :import
          post :import_csv
        end
      end

      resources :states, path: 'cidades-e-estados',  except: [:destroy, :show]

      resources :cities, path: 'cidades',  only: [:new, :create]

      resources :booking_packs, path: 'banco-de-horas',  except: [:destroy]

      resources :booking_pack_contracting_companies, path: 'empresas-contratantes', only: [:index, :show]

      resources :booking_taxes, path: 'taxas', only: [:index, :edit, :update]

      resources :company_details, path: 'empresas', only: [:index, :show, :edit, :update] do
      end

      resources :possible_partner_contacts, path: 'contatos-de-possiveis-parceiros', only: [:index, :show] do
        collection do
          put :mark_all_messages_as_read
        end
      end

      resources :managers, path: 'managers', except: [:destroy] do
        member do
          post :deactivate
          post :activate
        end
      end

      resources :agencies, path: 'agencias', except: [:destroy] do
        member do
          post :deactivate
          post :activate
        end
      end

      resources :affiliates, path: 'afiliados', except: [:destroy] do
        member do
          post :deactivate
          post :activate
          post :generate_token
          post :generate_webservice_token
        end
      end

      resources :member_get_member_config, path: 'member_get_member_config', only: [:show, :edit, :update] 
  

      resources :devices, path: 'dispositivos', only: [:index, :destroy]

    resources :hotel_chains, path: 'redes-hoteleiras'

    resources :hotels, path: 'hoteis', :except => [:destroy] do
      resources :room_types, path: 'tipos-de-quartos', controller: 'hotels/room_types', except: [:destroy, :index, :new, :create]
      resources :meeting_rooms, path: 'salas-de-reuniao', controller: 'hotels/meeting_rooms', except: [:destroy, :index, :new, :create]
      #resources :event_rooms, path: 'saloes-de-evento', controller: 'hotels/event_rooms', except: [:destroy, :index, :new, :create]

        member do
          get :authenticate_as
        end

        scope '/quartos' do
          #get '/tabelas-de-precos/:id', to: 'hotels/pricing_policies#show', as: :pricing_policy
          resources :pricing_policies, path: 'tabelas-de-precos', controller: 'hotels/pricing_policies', except: [:destroy, :index, :new, :create]
          get '/menor-preco-por-pacote', to: 'hotels/minimum_offers_price#show', as: :minimum_offers_price_show
        end

        scope '/salas-de-reuniao' do
          get '/tabelas-de-precos/:id', to: 'hotels/pricing_policies#meeting_show', as: :meeting_room_pricing_policy
          get '/menor-preco-por-pacote', to: 'hotels/minimum_offers_price#meeting_show', as: :minimum_meeting_offers_price_show
        end

        #scope '/saloes-de-evento' do
        #  get '/tabelas-de-precos/:id', to: 'hotels/pricing_policies#event_show', as: :event_room_pricing_policy
        #  get '/menor-preco-por-pacote', to: 'hotels/minimum_offers_price#event_show', as: :minimum_event_offers_price_show
        #end

      end

      get 'codus/objects' => 'codus#objects_list'

      resources :currencies, path: 'moedas' do
        collection do
          get :update_all
        end
      end

      resources :places, except: [:index], path: 'lugares' do
        collection do
          post :activate
          post :deactivate
          get :airports
          get :cities
          get :regions
        end
      end


      resources :executive_responsibles, path: 'universo-de-empresas/executivos' do
        member do
          post :activate
          post :deactivate
        end
      end

      resources :company_attendants, path: 'universo-de-empresas/atendimento-empresas' do
        member do
          post :activate
          post :deactivate
        end
      end

      resources :company_regions, path: 'universo-de-empresas/regioes' do
        member do
          post :activate
          post :deactivate
        end
      end

      resources :company_universes, path: 'universo-de-empresas/empresas' do
        member do
          post :activate
          post :deactivate
        end
      end

      resources :company_remunerations, except: [:create, :new] , path: 'universo-de-empresas/remuneracao' do
      end

      resources :goals_executive_responsibles,  only: [:index, :show], except: [:create, :new, :edit], path: 'universo-de-empresas/metas-executivos' do
      end

      resources :hq_consultants, path: 'consultores' do
        member do
          post :activate
          post :deactivate
        end
      end

    end # of admin namespace
  end

  namespace :agency, path: 'agencias', path_names: { new: 'novo', create: 'novo', edit: 'editar', update: 'editar' } do
    root to: 'info#index'

    devise_for :agencies, path: '/', path_names:{
                                                  sign_in: 'entrar',
                                                  sign_out: 'sair'
                                                }

    get 'mudar-senha' => 'info#edit', as: :change_password

    patch 'mudar-senha' => 'info#update_password'

    get 'relatorio-de-reservas' => 'bookings_report#index', as: :bookings_report
    post 'relatorio-de-reservas' => 'bookings_report#index', as: :generate_bookings_report

    resources :clients, except: [:destroy ], path: 'clientes' do
      member do
        post :authenticate_as_client_employee, path: 'funcionario/:employee_id/acessar-como'
        post :authenticate_as_client, path: 'acessar-como'
      end
    end

    resources :bookings, only: [ :index, :show ], path: 'reservas' do
      member do
        put :cancel
      end
    end
  end # of agency namespace

  namespace :executive_responsible, path: 'executivos', path_names: { new: 'novo', create: 'novo', edit: 'editar', update: 'editar' } do
    root to: 'dashboard#index'

    devise_for :executive_responsibles,
                  controllers: {
                              sessions:           'executive_responsible/sessions',
                              passwords:          'executive_responsible/passwords'
                            },
                  path: '/', path_names:{
                                        sign_in: 'entrar',
                                        sign_up: 'registrar',
                                        sign_out: 'sair'
                                       }

    get 'mudar-senha' => 'info#edit', as: :change_password

    patch 'mudar-senha' => 'info#update_password'

    resources :company_details, path: '/empresas' do
      member do
        get :authenticate_as
      end
    end

    resources :company_regions, path: 'universo-de-empresas/regioes' do
      collection do
        post :activate
        post :deactivate
      end
    end

    resources :company_universes, path: 'universo-de-empresas/empresas' do
      collection do
        get  :all_companies, path: 'todas-empresas'
        post :activate
        post :deactivate
      end
    end


    resources :bookings, path: 'reservas', only: [:index, :show] do
      member do
        put :cancel
        put :hotel_accept_request
        put :hotel_deny_request
      end
    end


  end # of executive_responsible namespace


  namespace :hq_consultant, path: 'consultor', path_names: { new: 'novo', create: 'novo', edit: 'editar', update: 'editar' }  do
    root to: 'travel_requests#index'

    devise_for :hq_consultants,
                  controllers: {
                              sessions:           'hq_consultant/sessions',
                              passwords:          'hq_consultant/passwords'
                            },
                  path: '/', path_names:{
                                        sign_in: 'entrar',
                                        sign_up: 'registrar',
                                        sign_out: 'sair'
                                       }

    get 'mudar-senha' => 'info#edit', as: :change_password

    patch 'mudar-senha' => 'info#update_password'

    resources :travel_requests, except: [ :destroy ], path: 'solicitacoes' do
      member do
        put :cancel
        post :send_email_to_traveler
        delete :remove_air
        post :add_air
      end
    end

  end # of hq_consultant namespace

  namespace :company_attendant, path: 'atendimento', path_names: { new: 'novo', create: 'novo', edit: 'editar', update: 'editar' } do
    root to: 'company_details#index'

    devise_for :company_attendants,
                  controllers: {
                              sessions:           'company_attendant/sessions',
                              passwords:          'company_attendant/passwords'
                            },
                  path: '/', path_names:{
                                        sign_in: 'entrar',
                                        sign_up: 'registrar',
                                        sign_out: 'sair'
                                       }

    get 'mudar-senha' => 'info#edit', as: :change_password

    patch 'mudar-senha' => 'info#update_password'

    resources :company_details, path: '/empresas' do
    end

  end # of company_attendant namespace

  namespace :company, path: 'empresas', path_names:  { new: 'novo', create: 'novo', edit: 'editar', update: 'editar', remove: 'remover'} do
    root to: 'info#index'

    devise_for :companies, path: '/', path_names:{
                                        sign_in: 'entrar',
                                        sign_out: 'sair'
                                       }

    get 'atualizar-informacoes' => 'info#edit', as: :update_info

    patch 'atualizar-informacoes' => 'info#update'

    resources :bookings, only: [ :index, :show ], path: 'reservas' do
      member do
        put :cancel
      end
    end
    resources :departments, except: [ :destroy ], path: 'departamentos'

    resources :employees, path: 'funcionarios' do
      member do

        post :authenticate_as, path: 'acessar-como'
      end
    end

    resources :credit_cards, except: [ :edit, :update, :destroy ], path: 'cartoes-de-credito'
    resources :invoices, except: [ :edit, :update, :destroy ], path: 'dados-para-nf'


  end # of company namespace

  namespace :affiliate, path: 'afiliados', path_names:  { new: 'novo', create: 'novo', edit: 'editar', update: 'editar', remove: 'remover'} do
    root to: 'info#index'

    devise_for :affiliates,
                  controllers: {
                              sessions:           'affiliate/sessions',
                              registrations:      'affiliate/registrations',
                              passwords:          'affiliate/passwords'
                            },
                  path: '/', path_names:{
                                        sign_in: 'entrar',
                                        sign_up: 'registrar',
                                        sign_out: 'sair'
                                       }

    get 'atualizar-informacoes' => 'info#edit', as: :update_info

    patch 'atualizar-informacoes' => 'info#update'

    resources :doc, only: [ :index ], path: 'doc' do
      collection do
        get 'iframe'
        get 'webservice'
        get '/webservice/:version', to: "doc#webservice_version", as: :get_webservice
      end
    end

    resources :bookings, only: [ :index, :show ], path: 'reservas' do
      member do
        put :cancel
      end
    end

    #resources :credit_cards, except: [ :edit, :update, :destroy ], path: 'cartoes-de-credito'
    #resources :invoices, except: [ :edit, :update, :destroy ], path: 'dados-para-nf'


  end # of affiliate namespace

  localized do

    namespace :hotel, path: 'fornecedores', path_names: { new: 'novo', create: 'novo', edit: 'editar', update: 'editar', remove: 'remover'} do
      root to: 'info#index'

      get 'mudar-senha' => 'info#edit', as: :change_password

      patch 'mudar-senha' => 'info#update_password'

      get 'relatorios' => 'reports#index', as: :reports

      scope '/quartos' do
        resources :room_types, path: 'tipos_de_quartos', :except => [:destroy]
        resources :rooms, path: 'quartos', :except => [:update, :destroy, :show, :edit, :new]
        resources :pricing_policies, path: 'tabelas_de_precos'
        resources :room_offers_creator_logs, path: 'relatorios_de_ofertas', controller: 'offers_creator_logs', type: 'RoomOffersCreatorLog', only: [:index]
        resources :offers, path: 'ofertas', only: [:create, :new, :index] do
          collection do
            get :count_per_room
            get :room_types_with_active_rooms
            get :pricing_policies_with_room_types_id
            get :remove
            post :remove
            get :show_all, path: 'listar'
            get :remove_intermediate_step, path: 'remover_confirmacao'
            post :remove_intermediate_step, path: 'remover_confirmacao'
            get :remove_all_offers
            post :remove_all_offers
            get :remove_all_intermediate_step, path: 'remover_todas_confirmacao'
            post :remove_all_intermediate_step, path: 'remover_todas_confirmacao'
          end

        end
      end

      scope '/salas-de-reuniao' do
        resources :meeting_rooms, path: 'salas', :except => [:destroy], controller: 'business_rooms', type: 'MeetingRoom'
        resources :meeting_room_pricing_policies, path: 'tabelas_de_precos', controller: 'business_room_pricing_policies', type: 'MeetingRoomPricingPolicy'
        resources :meeting_room_offers_creator_logs, path: 'relatorios_de_ofertas', controller: 'offers_creator_logs', type: 'MeetingRoomOffersCreatorLog', only: [:index]
        resources :meeting_room_offers, path: 'ofertas', only: [:create, :new, :index] do

          collection do
            get :count_per_room
            get :pricing_policies_with_meeting_room_id
            get :remove
            post :remove
            get :show_all, path: 'listar'
            get :remove_intermediate_step, path: 'remover_confirmacao'
            post :remove_intermediate_step, path: 'remover_confirmacao'
          end

        end
      end

=begin
    scope '/saloes-de-evento' do
      resources :event_rooms, path: 'saloes', :except => [:destroy], controller: 'business_rooms', type: 'EventRoom'
      resources :event_room_pricing_policies, path: 'tabelas_de_precos', controller: 'business_room_pricing_policies', type: 'EventRoomPricingPolicy'
      resources :event_room_offers_creator_logs, path: 'relatorios_de_ofertas', controller: 'offers_creator_logs', type: 'EventRoomOffersCreatorLog', only: [:index]
      resources :event_room_offers, path: 'ofertas', only: [:create, :new, :index] do

        collection do
          get :count_per_room
          get :pricing_policies_with_event_room_id
          get :remove
          post :remove
          get :show_all, path: 'listar'
          get :remove_intermediate_step, path: 'remover_confirmacao'
          post :remove_intermediate_step, path: 'remover_confirmacao'
        end

      end
    end
=end

      devise_for :hotels,
                 controllers: { sessions:           'hotel/sessions',
                                passwords:          'hotel/passwords'
                              },
                 path: '/', path_names:{
                                        sign_in: 'entrar',
                                        sign_out: 'sair'
                                       }


      resources :bookings, path: 'reservas', only: [:index, :show] do
        collection do
          get :rooms, path: 'quartos'
          get :meeting_rooms, path: 'salas-de-reuniao'
          #get :event_rooms, path: 'saloes-de-evento'
        end
        member do
          put :set_as_no_show
          put :accuse_delayed_checkout
          put :hotel_accept_request
          put :hotel_deny_request
        end
      end
    end # of hotel namespace
  end

  namespace :public, path:'/' do
    resources :hotel_search_logs, only: [] do
      collection do
        post :set_email_for_last_log
      end
    end

    resources :promotional_code_logs, only: [] do
      collection do
        post :check_promotional_code
        post :check_promotional_code_without_creation
      end
    end

    resources :referred_friend_logs, only: [] do
      collection do
        post :send_code
      end
    end 

    resources :newsletters, only: [:new, :create]
    get '/timezone', to: 'home#timezone', as: 'home'

    localized do
      root to: 'home#index'

      get '/search', to: 'hotel#index', as: 'hotels'

      get '/search/hotel_details/:id/(:name)', to: 'hotel#show', as: 'hotel'

      get '/search/hotel_details/:hotel_id-:hotel_name/room_type_details/:id', to: 'hotel#hotel_room_type_show', as: 'hotel_room_type_show'

      get '/search/map_show/:id', to: 'hotel#hotel_map_show', as: 'hotel_map_show'

      resources :bookings, only: [:index, :show, :new, :create, :edit, :update] do
        resource :payments, only: [:new, :create], controller: 'bookings/payments' do
          member do
            post :create_express_checkout_payment
            get :payment_success
          end
        end
        member do
          put :cancel
        end
      end

      resources :company_details, only: [:index, :show, :new, :create, :edit, :update] do
        resources :cost_centers, only: [:index, :show, :new, :create, :edit, :update], controller: 'company_details/cost_centers', shallow: true do
          resources :travelers, only: [:show, :new, :create, :edit, :update], controller: 'company_details/cost_centers/travelers', shallow: true do
            member do
              post :traveler_to_user
            end
            resources :credit_cards, except: [ :destroy ], controller: 'company_details/cost_centers/travelers/credit_cards', shallow: true
          end
        end

        resources :job_titles, only: [:index, :show, :new, :create, :edit, :update], controller: 'company_details/job_titles', shallow: true

      end

      resources :consultants, only: [:index, :show, :new, :create, :edit, :update]

      get '/bookings/:id/payment_result', to: 'payments#result', as: 'payment_result'


      devise_for :users,
               controllers: { sessions:           'public/sessions',
                              passwords:          'public/passwords',
                              registrations:      'public/registrations'
                            }, path: '/'

      get  '/profile', to: 'users#edit', as: 'user_profile'
      patch  '/profile', to: 'users#update', as: 'user'

      patch  '/profile_address/:booking_id', to: 'users#update_address', as: 'user_update_address'

      #get '/friend_hq', to: 'home#friend_hq', as: 'friend_hq'

      #get '/indicate_and_earn/:id', to: 'home#indicate_and_earn', as: '/indicate_and_earn'

      #get '/hours_discount', to:'home#hours_discount', as: 'hours_discount'

      # get '/black_friday', to:'home#black_friday', as: 'black_friday'

      #get '/summer', to:'home#summer', as: 'summer'

      #get '/twelve_hours_discount', to:'home#twelve_hours_discount', as: 'twelve_hours_discount'

      get '/about_us', to: 'home#about_us', as: 'about_us'

      get '/how_it_works', to: 'home#how_it_works', as: 'how_it_works'

      get '/terms_of_use', to: 'home#terms_of_use', as: 'terms_of_use'

      get '/faq', to: 'home#faq', as: 'faq'

      get '/get_booking_packs/:by_hotel_id', to: 'home#get_booking_packs', as: 'get_booking_packs'

      post '/booking_pack_contracting_company', to: 'home#booking_pack_contracting_company', as: 'booking_pack_contracting_company'

      get '/business_trips', to: 'home#business_trips', as: 'get_business_trips'

      post '/business_trips', to: 'home#business_trips', as: 'post_business_trips'

      get '/be_our_partner', to: 'home#be_our_partner', as: 'get_be_our_partner'

      post '/be_our_partner', to: 'home#be_our_partner', as: 'post_be_our_partner'

      get '/privacy_policy', to: 'home#privacy_policy', as: 'privacy_policy'

      get '/customer_service', to: 'home#customer_service', as: 'customer_service'
      
      get '/customer_service_corp', to: 'home#customer_service_corp', as: 'customer_service_corp'
      
      # get '/tudo-azul', to: 'home#tudo_azul', as: 'tudo_azul'

      # get '/multiplus', to: 'home#multiplus', as: 'multiplus'

      # get '/uber', to: 'home#uber', as: 'uber'

      # get '/brahma', to: 'home#brahma', as: 'brahma'

      get '/airport', to: 'places#index_airport', as: 'airports'
      get '/city', to: 'places#index_city', as: 'cities'
      get '/place/airports/:id/:place_name', to: 'places#airports', as: 'airport'
      get '/place/cities/:id/:place_name', to: 'places#cities', as: 'city'
      get '/place/regions/:id/:place_name', to: 'places#regions', as: 'region'

      get '/cities', to: 'places#json_cities', format: 'json'
      get '/states', to: 'places#json_states', format: 'json'

      # resources :travel_requests, only: [:index, :show, :new, :create, :edit, :update], path: 'passagens-aereas' do
      #   member do
      #     post :disapprove
      #     post :fills_passenger
      #     post :pay_with_invoiced
      #     put :cancel
      #   end
      # end

    end

  end # of public namespace

  namespace :public_affiliate, path:'affiliate' do
    get '/timezone', to: 'home#timezone', as: 'home'

    resources :hotel_search_logs, only: [] do
      collection do
        post :set_email_for_last_log
      end
    end

    resources :promotional_code_logs, only: [] do
      collection do
        post :check_promotional_code
        post :check_promotional_code_without_creation
      end
    end

    namespace :api, defaults: {format: "json"} do
      namespace :v1 do
=begin
        devise_for :affiliates,
         controllers: { sessions:           'public_affiliate/api/v1/sessions'
                      }, path: '/'
=end
        resources :cities,  only: [:index]
        resources :hotels,  only: [:index, :show] do
          resources :rooms, only: [:index, :show]
        end
        ### booking action index was removed for security purposes
        resources :bookings, only: [:create, :destroy, :show, :new] do
          member do
            put :cancel
          end
          resource :payments, only: [] do
            member do
              # post :create_payment_epyaco
              post :create_payment_paypal
              post :create_payment_maxipago
              post :create_payment_billed
              # post :token
            end
          end
        end
        resources :currencies,  only: [:index]
        get '/hotel_offers', to: 'hotels#offers_per_hotel'
        get '/city_offers', to: 'hotels#offers_per_city'
        get '/offers', to: 'hotels#offers_per_price'
        get '/countries', to: 'hotels#countries'
        get '/hotel_list', to: 'hotels#hotel_list'
        get '/city_hotel/:city', to: 'hotels#hotel_list_by_city'

         get '/hotel_groups/:id', to: 'hotel_chains#hotels_by_hotel_groups'
         get '/hotel_groups', to: 'hotel_chains#hotel_groups'

         get '/lat_lng', to: 'locations#lat_lng'
      end
    end


    localized do
      root to: 'home#index'

      get '/search', to: 'hotel#index', as: 'hotels'

      get '/search/hotel_details/:id/(:name)', to: 'hotel#show', as: 'hotel'

      get '/search/hotel_details/:hotel_id-:hotel_name/room_type_details/:id', to: 'hotel#hotel_room_type_show', as: 'hotel_room_type_show'

      get '/search/map_show/:id', to: 'hotel#hotel_map_show', as: 'hotel_map_show'

      resources :bookings, only: [:index, :show, :new, :create, :edit, :update] do
        resource :payments, only: [:new, :create], controller: 'bookings/payments' do
          member do
            post :create_express_checkout_payment
            get :payment_success
          end
        end
        member do
          put :cancel
        end
      end

      get '/bookings/:id/payment_result', to: 'payments#result', as: 'payment_result'


      devise_for :users,
               controllers: { sessions:           'public_affiliate/sessions',
                              passwords:          'public_affiliate/passwords',
                              registrations:      'public_affiliate/registrations'
                            }, path: '/'

      get  '/profile', to: 'users#edit', as: 'user_profile'
      patch  '/profile', to: 'users#update', as: 'user'

      patch  '/profile_address/:booking_id', to: 'users#update_address', as: 'user_update_address'

      get '/about_us', to: 'home#about_us', as: 'about_us'

      get '/how_it_works', to: 'home#how_it_works', as: 'how_it_works'

      get '/terms_of_use', to: 'home#terms_of_use', as: 'terms_of_use'

      get '/faq', to: 'home#faq', as: 'faq'

      get '/business_trips', to: 'home#business_trips', as: 'get_business_trips'

      post '/business_trips', to: 'home#business_trips', as: 'post_business_trips'

      get '/be_our_partner', to: 'home#be_our_partner', as: 'get_be_our_partner'

      post '/be_our_partner', to: 'home#be_our_partner', as: 'post_be_our_partner'

      get '/privacy_policy', to: 'home#privacy_policy', as: 'privacy_policy'

      get '/customer_service', to: 'home#customer_service', as: 'customer_service'

      get '/customer_service_corp', to: 'home#customer_service_corp', as: 'customer_service_corp'

      # get '/tudo-azul', to: 'home#tudo_azul', as: 'tudo_azul'

      # get '/multiplus', to: 'home#multiplus', as: 'multiplus'

      get '/meeting_room', to: 'home#meeting_rooms'

      get '/meeting_room/search', to: 'hotel#meeting_room_hotel_chain'

      # get '/uber', to: 'home#uber', as: 'uber'

      # get '/brahma', to: 'home#brahma', as: 'brahma'
    end
  end # of public affiliate namespace

  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      post '/register_device/:device', to: 'devices#create', as: 'register_device'
      resources :hotels, only: [:index, :show] do
        collection {
          #get '/search', to: 'hotel#index'
          #get '/search/hotel_details/:id', to: 'hotel#show'
          get '/room_type_details/:id', to: 'hotels#hotel_room_type_show', as: 'hotel_room_type_show'
          get '/search/map_show/:id', to: 'hotel#hotel_map_show', as: 'hotel_map_show'
        }
      end
    end

    namespace :v2 do
      post '/register_device/:device', to: 'devices#create', as: 'register_device'
      resources :hotels, only: [:index, :show] do
        collection {
          #get '/search', to: 'hotel#index'
          #get '/search/hotel_details/:id', to: 'hotel#show'
          get '/room_type_details/:id', to: 'hotels#hotel_room_type_show', as: 'hotel_room_type_show'
          get '/search/map_show/:id', to: 'hotel#hotel_map_show', as: 'hotel_map_show'
        }

      end

      resources :bookings, only: [:create, :show] do
        member do
          put :cancel
        end
        resource :payments, only: [] do
          member do
            post :create_payment_epayco
            get :finish
            get :finish_epayco
            post :create_payment_maxipago
          end
        end
      end

      resources :currencies, only: [:index]
      post 'auth_user' => 'authentication#authenticate_user'
      post 'create_user' => 'registration#create_user'
      post 'recover_password' => 'registration#recover_password'
      get 'get_bookings' => 'bookings#get_bookings'
      get 'get_booking' => 'bookings#get_booking'
      get 'cancel' => 'bookings#cancel'
      post 'payu-send-notification' => 'payu_notification#notification'
    end
  end


  mount Sidekiq::Web, at: '/filasidekiq'
  get '/sitemap.xml.gz', to: redirect("http://#{ENV['S3_BUCKET']}.s3.amazonaws.com/sitemaps/sitemap.xml.gz")

end
