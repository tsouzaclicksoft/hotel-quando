--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: btree_gist; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS btree_gist WITH SCHEMA public;


--
-- Name: EXTENSION btree_gist; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION btree_gist IS 'support for indexing common datatypes in GiST';


--
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA public;


--
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_stat_statements IS 'track execution statistics of all SQL statements executed';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agencies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE agencies (
    id integer NOT NULL,
    name character varying(255),
    is_active boolean DEFAULT true,
    comission_in_percentage numeric(5,2) DEFAULT 0,
    observations text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    cnpj character varying(255),
    phone_number character varying(255),
    sign_email character varying(255),
    login character varying(255)
);


--
-- Name: agencies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE agencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: agencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE agencies_id_seq OWNED BY agencies.id;


--
-- Name: bookings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bookings (
    id integer NOT NULL,
    status integer,
    guest_name character varying(255),
    note character varying(255),
    hotel_comments text,
    user_id integer,
    offer_id numeric(20,0),
    room_id integer,
    date_interval tsrange,
    room_type_id integer,
    pack_in_hours integer,
    number_of_people numeric(2,0),
    checkin_date date,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    is_active boolean DEFAULT true,
    hotel_id integer,
    cached_room_type_initial_capacity integer,
    cached_room_type_maximum_capacity integer,
    cached_offer_extra_price_per_person numeric(6,2),
    cached_offer_price numeric(6,2),
    cached_offer_no_show_value numeric(6,2),
    delayed_checkout_flag boolean DEFAULT false,
    refund_error boolean DEFAULT false,
    created_by_agency boolean DEFAULT false,
    guest_email character varying(255),
    booking_tax numeric(8,2) DEFAULT 0,
    canceled_by_agency boolean,
    company_id integer,
    omnibees_code character varying(255),
    omnibees_offer_id numeric(20,0),
    omnibees_request_xml text,
    omnibees_response_xml text,
    tudo_azul_notified_at timestamp without time zone,
    uses_tudo_azul boolean DEFAULT false
);


--
-- Name: bookings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bookings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bookings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bookings_id_seq OWNED BY bookings.id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cities (
    id integer NOT NULL,
    name character varying(255),
    state_id integer
);


--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cities_id_seq OWNED BY cities.id;


--
-- Name: companies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE companies (
    id integer NOT NULL,
    name character varying(255),
    phone_number character varying(255),
    cnpj character varying(255),
    maxipago_token character varying(255),
    responsible_name character varying(255),
    email character varying(255),
    country character varying(255),
    state character varying(255),
    city character varying(255),
    street character varying(255),
    number character varying(255),
    complement character varying(255),
    agency_id integer,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    is_active boolean DEFAULT true,
    login character varying(255)
);


--
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: companies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE companies_id_seq OWNED BY companies.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE countries (
    id integer NOT NULL,
    iso_initials character varying(255),
    name_pt_br character varying(255),
    name_en character varying(255),
    name_es character varying(255)
);


--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE countries_id_seq OWNED BY countries.id;


--
-- Name: credit_cards; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE credit_cards (
    id integer NOT NULL,
    flag_name character varying(20),
    owner_cpf character varying(255),
    owner_passport character varying(255),
    last_four_digits integer,
    expiration_year integer,
    expiration_month smallint,
    maxipago_token character varying(255),
    user_id integer,
    billing_name character varying(255),
    billing_address1 character varying(255),
    billing_city character varying(255),
    billing_state character varying(255),
    billing_zipcode character varying(255),
    billing_country character varying(255),
    billing_phone character varying(255),
    company_id integer
);


--
-- Name: credit_cards_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE credit_cards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: credit_cards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE credit_cards_id_seq OWNED BY credit_cards.id;


--
-- Name: departments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE departments (
    id integer NOT NULL,
    name character varying(255),
    company_id integer
);


--
-- Name: departments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE departments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: departments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE departments_id_seq OWNED BY departments.id;


--
-- Name: easy_taxi_vouchers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE easy_taxi_vouchers (
    id integer NOT NULL,
    code character varying(255),
    linked_at timestamp without time zone,
    expiration_date date,
    booking_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: easy_taxi_vouchers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE easy_taxi_vouchers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: easy_taxi_vouchers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE easy_taxi_vouchers_id_seq OWNED BY easy_taxi_vouchers.id;


--
-- Name: hotel_amenities; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE hotel_amenities (
    id integer NOT NULL,
    hotel_id integer,
    breakfast_included boolean DEFAULT false,
    free_internet boolean DEFAULT false,
    internet_pt_br text,
    internet_en text,
    park_pt_br text,
    park_en text,
    pool boolean DEFAULT false,
    rooftop boolean DEFAULT false,
    garden boolean DEFAULT false,
    terrace boolean DEFAULT false,
    sauna boolean DEFAULT false,
    gym boolean DEFAULT false,
    restaurant boolean DEFAULT false,
    bar boolean DEFAULT false,
    reception_24_hour boolean DEFAULT false,
    luggage_room boolean DEFAULT false,
    laundry boolean DEFAULT false,
    room_service boolean DEFAULT false,
    wake_up_service boolean DEFAULT false,
    transfer_offered_by_the_hotel_pt_br text,
    transfer_offered_by_the_hotel_en text,
    gratuity_for_children_pt_br text,
    gratuity_for_children_en text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: hotel_amenities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hotel_amenities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hotel_amenities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE hotel_amenities_id_seq OWNED BY hotel_amenities.id;


--
-- Name: hotel_search_logs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE hotel_search_logs (
    id integer NOT NULL,
    checkin_date character varying(255),
    checkin_hour numeric(2,0),
    number_of_people numeric(2,0),
    length_of_pack numeric(2,0),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    latitude double precision,
    longitude double precision,
    search_logs_user_id integer,
    address character varying(255),
    city_name character varying(255),
    state_name character varying(255),
    exact_match_hotels_ids text,
    similar_match_hotels_ids text
);


--
-- Name: hotel_search_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hotel_search_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hotel_search_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE hotel_search_logs_id_seq OWNED BY hotel_search_logs.id;


--
-- Name: hotels; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE hotels (
    id integer NOT NULL,
    name character varying(255),
    category character varying(255),
    contact_name character varying(255),
    contact_phone character varying(255),
    cnpj character varying(255),
    minimum_hours_of_notice integer,
    description_pt_br text,
    description_en text,
    payment_method_pt_br text,
    payment_method_en text,
    state character varying(255),
    city_id integer,
    cep character varying(255),
    street character varying(255),
    number character varying(255),
    complement character varying(255),
    login character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    latitude double precision,
    longitude double precision,
    comission_in_percentage numeric(5,2) DEFAULT 0,
    iss_in_percentage numeric(4,1) DEFAULT 0,
    accept_online_payment boolean DEFAULT false,
    notice_emails text,
    active_offers_count bigint,
    omnibees_code character varying(255),
    omnibees_config text
);


--
-- Name: hotels_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hotels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hotels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE hotels_id_seq OWNED BY hotels.id;


--
-- Name: offers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE offers (
    id numeric(20,0) NOT NULL,
    checkin_timestamp timestamp without time zone DEFAULT '2014-02-06 17:24:13.646684'::timestamp without time zone,
    checkout_timestamp timestamp without time zone DEFAULT '2014-02-06 17:24:13.646684'::timestamp without time zone,
    pack_in_hours numeric(2,0),
    price numeric(6,2),
    status numeric(2,0),
    room_type_initial_capacity numeric(2,0),
    room_type_maximum_capacity numeric(2,0),
    no_show_value numeric(6,2),
    room_id numeric(5,0),
    room_type_id numeric(5,0),
    hotel_id numeric(5,0),
    log_id integer,
    checkin_timestamp_day_of_week numeric(1,0),
    extra_price_per_person numeric(6,2) DEFAULT 0
);


--
-- Name: offers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE offers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: offers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE offers_id_seq OWNED BY offers.id;


--
-- Name: offers_logs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE offers_logs (
    id integer NOT NULL,
    form_params text,
    result bytea,
    hotel_id integer,
    type_of_log character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    logged_room_types_and_prices_used text,
    reprocessed boolean DEFAULT false
);


--
-- Name: offers_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE offers_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: offers_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE offers_logs_id_seq OWNED BY offers_logs.id;


--
-- Name: omnibees_offers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE omnibees_offers (
    id numeric(20,0) NOT NULL,
    checkin_timestamp timestamp without time zone,
    checkout_timestamp timestamp without time zone,
    pack_in_hours numeric(2,0),
    price numeric(6,2),
    room_type_id numeric(5,0),
    hotel_id numeric(5,0),
    number_of_guests numeric(2,0),
    hotel_availabilities_xml text,
    room_rate_xml text,
    room_type_xml text,
    rate_plan_xml text
);


--
-- Name: omnibees_offers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE omnibees_offers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: omnibees_offers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE omnibees_offers_id_seq OWNED BY omnibees_offers.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE payments (
    id integer NOT NULL,
    status integer,
    user_id integer,
    booking_id integer,
    credit_card_id integer,
    canceled_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    maxipago_response text DEFAULT ''::text,
    maxipago_auth_code character varying(255),
    maxipago_order_id character varying(255),
    authorized_at timestamp without time zone,
    maxipago_transaction_id character varying(255)
);


--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payments_id_seq OWNED BY payments.id;


--
-- Name: photos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE photos (
    id integer NOT NULL,
    title_en character varying(255),
    title_pt_br character varying(255),
    direct_image_url character varying(255),
    processing_status character varying(255) DEFAULT 'waiting_for_direct_url_processing'::character varying,
    hotel_id integer,
    room_type_id integer,
    file_file_name character varying(255),
    file_content_type character varying(255),
    file_file_size integer,
    file_updated_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    is_main boolean DEFAULT false
);


--
-- Name: photos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE photos_id_seq OWNED BY photos.id;


--
-- Name: possible_partner_contacts; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE possible_partner_contacts (
    id integer NOT NULL,
    name character varying(255),
    phone character varying(255),
    email_for_contact character varying(255),
    locale character varying(255),
    message text,
    admin_visualized boolean DEFAULT false,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    contact_name character varying(255),
    contact_role character varying(255),
    type_of_contact numeric(2,0)
);


--
-- Name: possible_partner_contacts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE possible_partner_contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: possible_partner_contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE possible_partner_contacts_id_seq OWNED BY possible_partner_contacts.id;


--
-- Name: pricing_policies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pricing_policies (
    id integer NOT NULL,
    name character varying(255),
    hotel_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    percentage_for_3h numeric(6,2) DEFAULT 40,
    percentage_for_6h numeric(6,2) DEFAULT 45,
    percentage_for_9h numeric(6,2) DEFAULT 65,
    percentage_for_12h numeric(6,2) DEFAULT 70,
    percentage_for_24h numeric(6,2) DEFAULT 100,
    percentage_for_36h numeric(6,2) DEFAULT 170,
    percentage_for_48h numeric(6,2) DEFAULT 200,
    percentage_profile_name character varying(20) DEFAULT 'normal'::character varying
);


--
-- Name: pricing_policies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pricing_policies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pricing_policies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pricing_policies_id_seq OWNED BY pricing_policies.id;


--
-- Name: refunds; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE refunds (
    id integer NOT NULL,
    status integer,
    user_id integer,
    payment_id integer,
    maxipago_response text,
    maxipago_auth_code character varying(255),
    maxipago_order_id character varying(255),
    maxipago_transaction_id character varying(255),
    authorized_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    value numeric(8,2)
);


--
-- Name: refunds_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE refunds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: refunds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE refunds_id_seq OWNED BY refunds.id;


--
-- Name: room_type_pricing_policies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE room_type_pricing_policies (
    id integer NOT NULL,
    pack_price_3h numeric(6,2) DEFAULT 0,
    pack_price_6h numeric(6,2) DEFAULT 0,
    pack_price_9h numeric(6,2) DEFAULT 0,
    pack_price_12h numeric(6,2) DEFAULT 0,
    pack_price_24h numeric(6,2) DEFAULT 0,
    pack_price_36h numeric(6,2) DEFAULT 0,
    pack_price_48h numeric(6,2) DEFAULT 0,
    pricing_policy_id integer,
    room_type_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    extra_price_per_person_for_pack_price_3h numeric(6,2) DEFAULT 0,
    extra_price_per_person_for_pack_price_6h numeric(6,2) DEFAULT 0,
    extra_price_per_person_for_pack_price_9h numeric(6,2) DEFAULT 0,
    extra_price_per_person_for_pack_price_12h numeric(6,2) DEFAULT 0,
    extra_price_per_person_for_pack_price_24h numeric(6,2) DEFAULT 0,
    extra_price_per_person_for_pack_price_36h numeric(6,2) DEFAULT 0,
    extra_price_per_person_for_pack_price_48h numeric(6,2) DEFAULT 0,
    penalty_for_3h_pack_in_percentage numeric(5,2) DEFAULT 100,
    penalty_for_6h_pack_in_percentage numeric(5,2) DEFAULT 100,
    penalty_for_9h_pack_in_percentage numeric(5,2) DEFAULT 100,
    penalty_for_12h_pack_in_percentage numeric(5,2) DEFAULT 100,
    penalty_for_24h_pack_in_percentage numeric(5,2) DEFAULT 100,
    penalty_for_36h_pack_in_percentage numeric(5,2) DEFAULT 66.7,
    penalty_for_48h_pack_in_percentage numeric(5,2) DEFAULT 50
);


--
-- Name: room_type_pricing_policies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE room_type_pricing_policies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: room_type_pricing_policies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE room_type_pricing_policies_id_seq OWNED BY room_type_pricing_policies.id;


--
-- Name: room_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE room_types (
    id integer NOT NULL,
    name_pt_br character varying(255),
    name_en character varying(255),
    description_pt_br text,
    description_en text,
    initial_capacity integer,
    maximum_capacity integer,
    hotel_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    single_bed_quantity numeric(2,0),
    double_bed_quantity numeric(2,0),
    omnibees_code character varying(255)
);


--
-- Name: room_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE room_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: room_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE room_types_id_seq OWNED BY room_types.id;


--
-- Name: rooms; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE rooms (
    id integer NOT NULL,
    number character varying(255),
    room_type_id integer,
    hotel_id integer,
    is_active boolean DEFAULT true,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: rooms_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE rooms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: rooms_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE rooms_id_seq OWNED BY rooms.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: search_logs_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE search_logs_users (
    id integer NOT NULL,
    email character varying(255)
);


--
-- Name: search_logs_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE search_logs_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: search_logs_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE search_logs_users_id_seq OWNED BY search_logs_users.id;


--
-- Name: states; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE states (
    id integer NOT NULL,
    initials character varying(255),
    name character varying(255)
);


--
-- Name: states_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE states_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: states_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE states_id_seq OWNED BY states.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255),
    cpf character varying(255),
    passport character varying(255),
    gender character varying(255),
    country character varying(255),
    state character varying(255),
    city character varying(255),
    street character varying(255),
    number integer,
    complement character varying(255),
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    confirmation_token character varying(255),
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    phone_number character varying(255),
    locale character varying(255),
    maxipago_token character varying(255),
    company_id integer,
    department_id integer,
    monthly_budget_centavos integer,
    monthly_budget_currency character varying(255) DEFAULT 'BRL'::character varying NOT NULL,
    birth_date date,
    budget_for_24_hours_pack_centavos integer DEFAULT 0 NOT NULL,
    budget_for_24_hours_pack_currency character varying(255) DEFAULT 'BRL'::character varying NOT NULL,
    is_active boolean DEFAULT true
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY agencies ALTER COLUMN id SET DEFAULT nextval('agencies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bookings ALTER COLUMN id SET DEFAULT nextval('bookings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cities ALTER COLUMN id SET DEFAULT nextval('cities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY companies ALTER COLUMN id SET DEFAULT nextval('companies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY countries ALTER COLUMN id SET DEFAULT nextval('countries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY credit_cards ALTER COLUMN id SET DEFAULT nextval('credit_cards_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY departments ALTER COLUMN id SET DEFAULT nextval('departments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY easy_taxi_vouchers ALTER COLUMN id SET DEFAULT nextval('easy_taxi_vouchers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY hotel_amenities ALTER COLUMN id SET DEFAULT nextval('hotel_amenities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY hotel_search_logs ALTER COLUMN id SET DEFAULT nextval('hotel_search_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY hotels ALTER COLUMN id SET DEFAULT nextval('hotels_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY offers ALTER COLUMN id SET DEFAULT nextval('offers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY offers_logs ALTER COLUMN id SET DEFAULT nextval('offers_logs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY omnibees_offers ALTER COLUMN id SET DEFAULT nextval('omnibees_offers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payments ALTER COLUMN id SET DEFAULT nextval('payments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY photos ALTER COLUMN id SET DEFAULT nextval('photos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY possible_partner_contacts ALTER COLUMN id SET DEFAULT nextval('possible_partner_contacts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pricing_policies ALTER COLUMN id SET DEFAULT nextval('pricing_policies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY refunds ALTER COLUMN id SET DEFAULT nextval('refunds_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY room_type_pricing_policies ALTER COLUMN id SET DEFAULT nextval('room_type_pricing_policies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY room_types ALTER COLUMN id SET DEFAULT nextval('room_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY rooms ALTER COLUMN id SET DEFAULT nextval('rooms_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY search_logs_users ALTER COLUMN id SET DEFAULT nextval('search_logs_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY states ALTER COLUMN id SET DEFAULT nextval('states_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: agencies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY agencies
    ADD CONSTRAINT agencies_pkey PRIMARY KEY (id);


--
-- Name: bookings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT bookings_pkey PRIMARY KEY (id);


--
-- Name: cities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: companies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- Name: countries_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: credit_cards_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY credit_cards
    ADD CONSTRAINT credit_cards_pkey PRIMARY KEY (id);


--
-- Name: departments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY departments
    ADD CONSTRAINT departments_pkey PRIMARY KEY (id);


--
-- Name: easy_taxi_vouchers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY easy_taxi_vouchers
    ADD CONSTRAINT easy_taxi_vouchers_pkey PRIMARY KEY (id);


--
-- Name: hotel_amenities_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY hotel_amenities
    ADD CONSTRAINT hotel_amenities_pkey PRIMARY KEY (id);


--
-- Name: hotel_search_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY hotel_search_logs
    ADD CONSTRAINT hotel_search_logs_pkey PRIMARY KEY (id);


--
-- Name: hotels_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY hotels
    ADD CONSTRAINT hotels_pkey PRIMARY KEY (id);


--
-- Name: offers_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY offers_logs
    ADD CONSTRAINT offers_logs_pkey PRIMARY KEY (id);


--
-- Name: offers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY offers
    ADD CONSTRAINT offers_pkey PRIMARY KEY (id);


--
-- Name: omnibees_offers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY omnibees_offers
    ADD CONSTRAINT omnibees_offers_pkey PRIMARY KEY (id);


--
-- Name: overlapping_times; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bookings
    ADD CONSTRAINT overlapping_times EXCLUDE USING gist (room_id WITH =, date_interval WITH &&) WHERE ((is_active = true));


--
-- Name: payments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: photos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY photos
    ADD CONSTRAINT photos_pkey PRIMARY KEY (id);


--
-- Name: possible_partner_contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY possible_partner_contacts
    ADD CONSTRAINT possible_partner_contacts_pkey PRIMARY KEY (id);


--
-- Name: pricing_policies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pricing_policies
    ADD CONSTRAINT pricing_policies_pkey PRIMARY KEY (id);


--
-- Name: refunds_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY refunds
    ADD CONSTRAINT refunds_pkey PRIMARY KEY (id);


--
-- Name: room_type_pricing_policies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY room_type_pricing_policies
    ADD CONSTRAINT room_type_pricing_policies_pkey PRIMARY KEY (id);


--
-- Name: room_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY room_types
    ADD CONSTRAINT room_types_pkey PRIMARY KEY (id);


--
-- Name: rooms_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY rooms
    ADD CONSTRAINT rooms_pkey PRIMARY KEY (id);


--
-- Name: search_logs_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY search_logs_users
    ADD CONSTRAINT search_logs_users_pkey PRIMARY KEY (id);


--
-- Name: states_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY states
    ADD CONSTRAINT states_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_agencies_on_login; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_agencies_on_login ON agencies USING btree (login);


--
-- Name: index_agencies_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_agencies_on_reset_password_token ON agencies USING btree (reset_password_token);


--
-- Name: index_bookings_on_date_interval; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_date_interval ON bookings USING btree (date_interval);


--
-- Name: index_bookings_on_hotel_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_hotel_id ON bookings USING btree (hotel_id);


--
-- Name: index_bookings_on_offer_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_offer_id ON bookings USING btree (offer_id);


--
-- Name: index_bookings_on_omnibees_offer_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_omnibees_offer_id ON bookings USING btree (omnibees_offer_id);


--
-- Name: index_bookings_on_pack_in_hours; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_pack_in_hours ON bookings USING btree (pack_in_hours);


--
-- Name: index_bookings_on_room_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_room_id ON bookings USING btree (room_id);


--
-- Name: index_bookings_on_room_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_room_type_id ON bookings USING btree (room_type_id);


--
-- Name: index_bookings_on_room_type_id_and_is_active_and_date_interval; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_room_type_id_and_is_active_and_date_interval ON bookings USING btree (room_type_id, is_active, date_interval);


--
-- Name: index_bookings_on_status; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_status ON bookings USING btree (status);


--
-- Name: index_bookings_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_user_id ON bookings USING btree (user_id);


--
-- Name: index_bookings_on_uses_tudo_azul; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_uses_tudo_azul ON bookings USING btree (uses_tudo_azul);


--
-- Name: index_bookings_on_uses_tudo_azul_and_tudo_azul_notified_at; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_bookings_on_uses_tudo_azul_and_tudo_azul_notified_at ON bookings USING btree (uses_tudo_azul, tudo_azul_notified_at);


--
-- Name: index_cities_on_state_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_cities_on_state_id ON cities USING btree (state_id);


--
-- Name: index_companies_on_login; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_companies_on_login ON companies USING btree (login);


--
-- Name: index_companies_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_companies_on_reset_password_token ON companies USING btree (reset_password_token);


--
-- Name: index_credit_cards_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_credit_cards_on_user_id ON credit_cards USING btree (user_id);


--
-- Name: index_easy_taxi_vouchers_on_booking_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_easy_taxi_vouchers_on_booking_id ON easy_taxi_vouchers USING btree (booking_id);


--
-- Name: index_easy_taxi_vouchers_on_code; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_easy_taxi_vouchers_on_code ON easy_taxi_vouchers USING btree (code);


--
-- Name: index_easy_taxi_vouchers_on_expiration_date; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_easy_taxi_vouchers_on_expiration_date ON easy_taxi_vouchers USING btree (expiration_date);


--
-- Name: index_easy_taxi_vouchers_on_expiration_date_and_booking_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_easy_taxi_vouchers_on_expiration_date_and_booking_id ON easy_taxi_vouchers USING btree (expiration_date, booking_id);


--
-- Name: index_hotel_search_logs_on_city_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_hotel_search_logs_on_city_name ON hotel_search_logs USING btree (city_name);


--
-- Name: index_hotel_search_logs_on_search_logs_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_hotel_search_logs_on_search_logs_user_id ON hotel_search_logs USING btree (search_logs_user_id);


--
-- Name: index_hotel_search_logs_on_state_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_hotel_search_logs_on_state_name ON hotel_search_logs USING btree (state_name);


--
-- Name: index_hotels_on_city_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_hotels_on_city_id ON hotels USING btree (city_id);


--
-- Name: index_hotels_on_latitude; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_hotels_on_latitude ON hotels USING btree (latitude);


--
-- Name: index_hotels_on_latitude_and_longitude; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_hotels_on_latitude_and_longitude ON hotels USING btree (latitude, longitude);


--
-- Name: index_hotels_on_login; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_hotels_on_login ON hotels USING btree (login);


--
-- Name: index_hotels_on_longitude; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_hotels_on_longitude ON hotels USING btree (longitude);


--
-- Name: index_hotels_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_hotels_on_reset_password_token ON hotels USING btree (reset_password_token);


--
-- Name: index_hotels_on_state; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_hotels_on_state ON hotels USING btree (state);


--
-- Name: index_offers_logs_on_hotel_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_logs_on_hotel_id ON offers_logs USING btree (hotel_id);


--
-- Name: index_offers_on_checkin_timestamp; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_checkin_timestamp ON offers USING btree (checkin_timestamp);


--
-- Name: index_offers_on_checkin_timestamp_day_of_week; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_checkin_timestamp_day_of_week ON offers USING btree (checkin_timestamp_day_of_week);


--
-- Name: index_offers_on_checkout_timestamp; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_checkout_timestamp ON offers USING btree (checkout_timestamp);


--
-- Name: index_offers_on_hotel_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_hotel_id ON offers USING btree (hotel_id);


--
-- Name: index_offers_on_hotel_id_and_status_and_pack_in_hours; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_hotel_id_and_status_and_pack_in_hours ON offers USING btree (hotel_id, status, pack_in_hours);


--
-- Name: index_offers_on_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_id ON offers USING btree (id);


--
-- Name: index_offers_on_log_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_log_id ON offers USING btree (log_id);


--
-- Name: index_offers_on_pack_in_hours_and_checkin_timestamp; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_pack_in_hours_and_checkin_timestamp ON offers USING btree (pack_in_hours, checkin_timestamp);


--
-- Name: index_offers_on_room_and_hotel_and_pack_length_and_checkin; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_offers_on_room_and_hotel_and_pack_length_and_checkin ON offers USING btree (room_id, pack_in_hours, checkin_timestamp);


--
-- Name: index_offers_on_room_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_room_id ON offers USING btree (room_id);


--
-- Name: index_offers_on_room_type_and_pack_length_and_checkin; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_room_type_and_pack_length_and_checkin ON offers USING btree (room_type_id, pack_in_hours, checkin_timestamp);


--
-- Name: index_offers_on_room_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_room_type_id ON offers USING btree (room_type_id);


--
-- Name: index_offers_on_room_type_id_and_price; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_room_type_id_and_price ON offers USING btree (room_type_id, price);


--
-- Name: index_offers_on_status; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_offers_on_status ON offers USING btree (status);


--
-- Name: index_omnibees_offers_on_checkin_timestamp; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_omnibees_offers_on_checkin_timestamp ON omnibees_offers USING btree (checkin_timestamp);


--
-- Name: index_omnibees_offers_on_checkout_timestamp; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_omnibees_offers_on_checkout_timestamp ON omnibees_offers USING btree (checkout_timestamp);


--
-- Name: index_omnibees_offers_on_hotel_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_omnibees_offers_on_hotel_id ON omnibees_offers USING btree (hotel_id);


--
-- Name: index_omnibees_offers_on_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_omnibees_offers_on_id ON omnibees_offers USING btree (id);


--
-- Name: index_omnibees_offers_on_room_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_omnibees_offers_on_room_type_id ON omnibees_offers USING btree (room_type_id);


--
-- Name: index_payments_on_booking_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_payments_on_booking_id ON payments USING btree (booking_id);


--
-- Name: index_payments_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_payments_on_user_id ON payments USING btree (user_id);


--
-- Name: index_pricing_policies_on_hotel_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_pricing_policies_on_hotel_id ON pricing_policies USING btree (hotel_id);


--
-- Name: index_room_type_pricing_policies_on_pricing_policy_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_room_type_pricing_policies_on_pricing_policy_id ON room_type_pricing_policies USING btree (pricing_policy_id);


--
-- Name: index_room_type_pricing_policies_on_room_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_room_type_pricing_policies_on_room_type_id ON room_type_pricing_policies USING btree (room_type_id);


--
-- Name: index_room_types_on_hotel_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_room_types_on_hotel_id ON room_types USING btree (hotel_id);


--
-- Name: index_rooms_on_hotel_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_rooms_on_hotel_id ON rooms USING btree (hotel_id);


--
-- Name: index_rooms_on_is_active; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_rooms_on_is_active ON rooms USING btree (is_active);


--
-- Name: index_rooms_on_room_type_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_rooms_on_room_type_id ON rooms USING btree (room_type_id);


--
-- Name: index_search_logs_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_search_logs_users_on_email ON search_logs_users USING btree (email);


--
-- Name: index_states_on_initials; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_states_on_initials ON states USING btree (initials);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON users USING btree (confirmation_token);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: ignore_duplicate_inserts_on_offers; Type: RULE; Schema: public; Owner: -
--

CREATE RULE ignore_duplicate_inserts_on_offers AS
    ON INSERT TO offers
   WHERE (EXISTS ( SELECT 1
           FROM offers
          WHERE (((offers.room_id = new.room_id) AND (offers.checkin_timestamp = new.checkin_timestamp)) AND (offers.pack_in_hours = new.pack_in_hours)))) DO INSTEAD NOTHING;


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20131216153634');

INSERT INTO schema_migrations (version) VALUES ('20131217133511');

INSERT INTO schema_migrations (version) VALUES ('20131217165447');

INSERT INTO schema_migrations (version) VALUES ('20131219011130');

INSERT INTO schema_migrations (version) VALUES ('20131220163738');

INSERT INTO schema_migrations (version) VALUES ('20131220163757');

INSERT INTO schema_migrations (version) VALUES ('20131220163829');

INSERT INTO schema_migrations (version) VALUES ('20131223192105');

INSERT INTO schema_migrations (version) VALUES ('20140106120257');

INSERT INTO schema_migrations (version) VALUES ('20140106120530');

INSERT INTO schema_migrations (version) VALUES ('20140109120447');

INSERT INTO schema_migrations (version) VALUES ('20140110121955');

INSERT INTO schema_migrations (version) VALUES ('20140207135709');

INSERT INTO schema_migrations (version) VALUES ('20140207184855');

INSERT INTO schema_migrations (version) VALUES ('20140211141840');

INSERT INTO schema_migrations (version) VALUES ('20140212140404');

INSERT INTO schema_migrations (version) VALUES ('20140212141250');

INSERT INTO schema_migrations (version) VALUES ('20140212141704');

INSERT INTO schema_migrations (version) VALUES ('20140212161924');

INSERT INTO schema_migrations (version) VALUES ('20140212161949');

INSERT INTO schema_migrations (version) VALUES ('20140212184450');

INSERT INTO schema_migrations (version) VALUES ('20140217191035');

INSERT INTO schema_migrations (version) VALUES ('20140218134152');

INSERT INTO schema_migrations (version) VALUES ('20140218165309');

INSERT INTO schema_migrations (version) VALUES ('20140218185227');

INSERT INTO schema_migrations (version) VALUES ('20140219182407');

INSERT INTO schema_migrations (version) VALUES ('20140221160405');

INSERT INTO schema_migrations (version) VALUES ('20140224195243');

INSERT INTO schema_migrations (version) VALUES ('20140226212944');

INSERT INTO schema_migrations (version) VALUES ('20140227151341');

INSERT INTO schema_migrations (version) VALUES ('20140228125904');

INSERT INTO schema_migrations (version) VALUES ('20140306202058');

INSERT INTO schema_migrations (version) VALUES ('20140310173454');

INSERT INTO schema_migrations (version) VALUES ('20140310204731');

INSERT INTO schema_migrations (version) VALUES ('20140311151354');

INSERT INTO schema_migrations (version) VALUES ('20140311152823');

INSERT INTO schema_migrations (version) VALUES ('20140311154854');

INSERT INTO schema_migrations (version) VALUES ('20140311190125');

INSERT INTO schema_migrations (version) VALUES ('20140313132251');

INSERT INTO schema_migrations (version) VALUES ('20140313161421');

INSERT INTO schema_migrations (version) VALUES ('20140321183747');

INSERT INTO schema_migrations (version) VALUES ('20140325200439');

INSERT INTO schema_migrations (version) VALUES ('20140326005356');

INSERT INTO schema_migrations (version) VALUES ('20140326005409');

INSERT INTO schema_migrations (version) VALUES ('20140326005424');

INSERT INTO schema_migrations (version) VALUES ('20140326131626');

INSERT INTO schema_migrations (version) VALUES ('20140326131946');

INSERT INTO schema_migrations (version) VALUES ('20140326172751');

INSERT INTO schema_migrations (version) VALUES ('20140326190401');

INSERT INTO schema_migrations (version) VALUES ('20140327131722');

INSERT INTO schema_migrations (version) VALUES ('20140327144353');

INSERT INTO schema_migrations (version) VALUES ('20140327174729');

INSERT INTO schema_migrations (version) VALUES ('20140328135848');

INSERT INTO schema_migrations (version) VALUES ('20140328141750');

INSERT INTO schema_migrations (version) VALUES ('20140328145357');

INSERT INTO schema_migrations (version) VALUES ('20140404184021');

INSERT INTO schema_migrations (version) VALUES ('20140406230153');

INSERT INTO schema_migrations (version) VALUES ('20140407171734');

INSERT INTO schema_migrations (version) VALUES ('20140409182912');

INSERT INTO schema_migrations (version) VALUES ('20140409222827');

INSERT INTO schema_migrations (version) VALUES ('20140410174232');

INSERT INTO schema_migrations (version) VALUES ('20140415181052');

INSERT INTO schema_migrations (version) VALUES ('20140417184054');

INSERT INTO schema_migrations (version) VALUES ('20140422163608');

INSERT INTO schema_migrations (version) VALUES ('20140423133911');

INSERT INTO schema_migrations (version) VALUES ('20140423175714');

INSERT INTO schema_migrations (version) VALUES ('20140519191733');

INSERT INTO schema_migrations (version) VALUES ('20140521135210');

INSERT INTO schema_migrations (version) VALUES ('20140523173059');

INSERT INTO schema_migrations (version) VALUES ('20140523180357');

INSERT INTO schema_migrations (version) VALUES ('20140526130745');

INSERT INTO schema_migrations (version) VALUES ('20140610190530');

INSERT INTO schema_migrations (version) VALUES ('20140610191634');

INSERT INTO schema_migrations (version) VALUES ('20140611165743');

INSERT INTO schema_migrations (version) VALUES ('20140613170237');

INSERT INTO schema_migrations (version) VALUES ('20140613191645');

INSERT INTO schema_migrations (version) VALUES ('20140616180454');

INSERT INTO schema_migrations (version) VALUES ('20140617144020');

INSERT INTO schema_migrations (version) VALUES ('20140618142813');

INSERT INTO schema_migrations (version) VALUES ('20140807172346');

INSERT INTO schema_migrations (version) VALUES ('20140811173129');

INSERT INTO schema_migrations (version) VALUES ('20140811173710');

INSERT INTO schema_migrations (version) VALUES ('20140811174241');

INSERT INTO schema_migrations (version) VALUES ('20140811194915');

INSERT INTO schema_migrations (version) VALUES ('20140812165752');

INSERT INTO schema_migrations (version) VALUES ('20140813135734');

INSERT INTO schema_migrations (version) VALUES ('20140814125836');

INSERT INTO schema_migrations (version) VALUES ('20140818182934');

INSERT INTO schema_migrations (version) VALUES ('20140901191052');

INSERT INTO schema_migrations (version) VALUES ('20150310145359');

INSERT INTO schema_migrations (version) VALUES ('20150401172728');

INSERT INTO schema_migrations (version) VALUES ('20150430144814');

INSERT INTO schema_migrations (version) VALUES ('20150528201153');

INSERT INTO schema_migrations (version) VALUES ('20150603021346');

INSERT INTO schema_migrations (version) VALUES ('20150603022014');

INSERT INTO schema_migrations (version) VALUES ('20150608214013');

INSERT INTO schema_migrations (version) VALUES ('20150610054402');

INSERT INTO schema_migrations (version) VALUES ('20150610062311');

INSERT INTO schema_migrations (version) VALUES ('20150610205308');

INSERT INTO schema_migrations (version) VALUES ('20150611135753');

INSERT INTO schema_migrations (version) VALUES ('20150611210815');

INSERT INTO schema_migrations (version) VALUES ('20150623191307');

INSERT INTO schema_migrations (version) VALUES ('20150625214921');

INSERT INTO schema_migrations (version) VALUES ('20150626070309');

INSERT INTO schema_migrations (version) VALUES ('20150702014242');

INSERT INTO schema_migrations (version) VALUES ('20150723153816');

INSERT INTO schema_migrations (version) VALUES ('20150724182904');

INSERT INTO schema_migrations (version) VALUES ('20150812170557');

INSERT INTO schema_migrations (version) VALUES ('20150812174444');

