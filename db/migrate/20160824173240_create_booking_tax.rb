class CreateBookingTax < ActiveRecord::Migration
  def change
    create_table :booking_taxes do |t|
    	t.string  :name
    	t.decimal :value, precision: 8, scale: 2
    end
  end
end
