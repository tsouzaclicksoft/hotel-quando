class AddCanceledByAdminToBookings < ActiveRecord::Migration
  def change
  	add_column :bookings, :canceled_by_admin, :boolean, default: false
  end
end
