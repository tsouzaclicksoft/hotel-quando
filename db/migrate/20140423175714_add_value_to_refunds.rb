class AddValueToRefunds < ActiveRecord::Migration
  def change
    change_table :refunds do |t|
      t.decimal :value, precision: 8, scale: 2
    end
  end
end
