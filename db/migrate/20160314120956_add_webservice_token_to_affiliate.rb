class AddWebserviceTokenToAffiliate < ActiveRecord::Migration
  def change
    add_column :affiliates, :webservice_token, :string
    add_index  :affiliates, :webservice_token, :unique => true
  end
end
