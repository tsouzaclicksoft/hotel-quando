class AddIsActiveToCompanies < ActiveRecord::Migration
  def change
    change_table :companies do | t |
	  	t.boolean :is_active, default: true
    end
  end
end
