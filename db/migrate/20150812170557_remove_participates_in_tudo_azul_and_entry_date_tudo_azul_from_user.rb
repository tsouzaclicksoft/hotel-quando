class RemoveParticipatesInTudoAzulAndEntryDateTudoAzulFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :participates_in_tudo_azul
    remove_column :users, :entry_date_tudo_azul
  end
end
