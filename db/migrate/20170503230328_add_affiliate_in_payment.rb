class AddAffiliateInPayment < ActiveRecord::Migration
  def change
    add_column :payments, :affiliate_id, :integer
    add_index :payments, :affiliate_id
  end
end
