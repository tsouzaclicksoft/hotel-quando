class AddAgencyDetailIdToPhotos < ActiveRecord::Migration
  def change
  	add_reference :photos, :agency_detail
  end
end
