class AddTabRoomAccepts < ActiveRecord::Migration
  def change
  	add_column :affiliates, :tab_room_accepts, :text, array:true, default: []
  end
end
