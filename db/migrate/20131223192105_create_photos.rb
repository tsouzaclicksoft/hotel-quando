# encoding: utf-8
class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :title_en
      t.string :title_pt_br
      t.string :direct_image_url
      t.string :processing_status, :default => 'waiting_for_direct_url_processing'
      t.references :hotel
      t.references :room_type
      t.attachment :file
      t.timestamps
    end
  end
end
