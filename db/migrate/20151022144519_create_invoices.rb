class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
			t.string  :company_name
      t.string  :cnpj
      t.string  :street
      t.string  :number
      t.string  :complement
      t.string  :district
      t.string  :city
      t.string  :state
      t.string  :country
      t.string  :cep
      t.integer :company_id
    end
    add_index :invoices, :company_id
  end
end
