class AddCompanyDetailToTravelers < ActiveRecord::Migration
  def change
  	add_column :travelers, :company_detail_id, :integer
    add_index :travelers, :company_detail_id

    Traveler.all.each do |traveler|
    	traveler.update(company_detail_id: traveler.cost_center.company_detail.id)
    end
  end
end
