class AddCompanyIdToBookingsCreditCardsAndUsers < ActiveRecord::Migration
  def change
    change_table :bookings do | t |
      t.integer :company_id
    end

    change_table :credit_cards do | t |
      t.integer :company_id
    end

    change_table :users do | t |
      t.integer :company_id
      t.integer :department_id
      t.boolean :company_user_for_agency
    end
  end
end
