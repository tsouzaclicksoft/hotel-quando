class AddConsultantIdToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :consultant_id, :integer

    add_index :bookings, :consultant_id
  end
end
