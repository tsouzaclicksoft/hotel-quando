class AddCreditCardIdToPayments < ActiveRecord::Migration
  def change
    change_table :payments do | t |
      t.integer   :credit_card_id
    end
  end
end
