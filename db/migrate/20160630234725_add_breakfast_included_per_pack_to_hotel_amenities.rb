class AddBreakfastIncludedPerPackToHotelAmenities < ActiveRecord::Migration
  def up
    change_table :hotel_amenities do |t|
      t.boolean :breakfast_included_for_pack_3h, default: false
      t.boolean :breakfast_included_for_pack_6h, default: false
      t.boolean :breakfast_included_for_pack_9h, default: false
      t.boolean :breakfast_included_for_pack_12h, default: false
    end
  end

  def down
    change_table :hotel_amenities do |t|
      t.remove :breakfast_included_for_pack_3h
      t.remove :breakfast_included_for_pack_6h
      t.remove :breakfast_included_for_pack_9h
      t.remove :breakfast_included_for_pack_12h
    end
  end
end
