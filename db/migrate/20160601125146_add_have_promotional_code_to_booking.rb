class AddHavePromotionalCodeToBooking < ActiveRecord::Migration
  def change
  	add_column :bookings, :have_promotional_code, :boolean, default: false
  end
end
