class CreateHotelChain < ActiveRecord::Migration
  def change
    create_table :hotel_chains do |t|
      t.string :name
      t.timestamps
    end

    add_column :hotels, :hotel_chain_id, :integer
    add_index :hotels, :hotel_chain_id
  end
end
