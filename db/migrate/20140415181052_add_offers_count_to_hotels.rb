class AddOffersCountToHotels < ActiveRecord::Migration
  def self.up
    add_column :hotels, :active_offers_count, :bigint

    Hotel.reset_column_information
    Hotel.all.pluck(:id).each do |hotel_id|
      active_offers_count = Offer.where(hotel_id: hotel_id, status: Offer::ACCEPTED_STATUS[:available]).count
      Hotel.where(id: hotel_id).update_all(active_offers_count: active_offers_count)
    end
  end

  def self.down
    remove_column :hotels, :active_offers_count
  end
end
