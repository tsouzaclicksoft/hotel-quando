class CopyPromotionalCodeDiscountToDiscountCached < ActiveRecord::Migration
  def up
  	promotional_code_log_changed = 0
  	promotional_code_log_cannot_changed = 0

  	PromotionalCodeLog.all.each do |promotional_code_log|
  		promotional_code_log.discount_cached = promotional_code_log.promotional_code.discount
  		promotional_code_log.discount_type_cached = promotional_code_log.promotional_code.discount_type
  		if promotional_code_log.save
  			promotional_code_log_changed += 1
  		else
  			promotional_code_log_cannot_changed += 1
		  end
  	end
  	puts "promotional_code_log that changed: " + promotional_code_log_changed.to_s
  	puts "promotional_code_log that cannot changed: " + promotional_code_log_cannot_changed.to_s
  end

  def down
  	PromotionalCodeLog.all.update_all(discount_cached: nil, discount_type_cached: nil)
  end
end
