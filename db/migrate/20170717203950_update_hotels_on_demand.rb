class UpdateHotelsOnDemand < ActiveRecord::Migration
  def change
    add_column(:hotels, :booking_on_request, :boolean, default: false)
  end
end
