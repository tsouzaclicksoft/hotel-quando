# encoding: utf-8
class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string  :number
      t.integer :room_type_id
      t.integer :hotel_id
      t.boolean :is_active, default: true
      t.timestamps
    end

    add_index :rooms, :hotel_id
    add_index :rooms, :room_type_id
    add_index :rooms, :is_active
  end
end
