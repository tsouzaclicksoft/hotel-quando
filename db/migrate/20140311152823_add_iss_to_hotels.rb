class AddIssToHotels < ActiveRecord::Migration
  def change
    change_table :hotels do |t|
      t.decimal :iss, precision: 4, scale: 1, default: 0
    end
  end
end
