class CreateAgencyDetails < ActiveRecord::Migration
  def change
    create_table :agency_details do |t|
    	t.references 	:user
    	t.string 			:name
    	t.string 			:cnpj
    	t.string 			:financial_contact
    	t.string			:phone_financial_contact
    	t.string			:sign_email
    	t.string			:sign_phone

      t.timestamps
    end
  end
end
