# encoding: utf-8
class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.string  :name
      t.string  :category

      # t.string  :email

      t.string  :contact_name
      t.string  :contact_phone
      t.string  :cnpj
      t.integer :minimum_hours_of_notice
      t.decimal  :penalty_for_36_hours_in_percentage, precision: 5, scale: 2, :null => false
      t.decimal  :penalty_for_48_hours_in_percentage, precision: 5, scale: 2, :null => false
      t.text    :description_pt_br
      t.text    :description_en
      t.text    :payment_method_pt_br
      t.text    :payment_method_en

      t.string  :state
      t.integer :city_id
      t.string  :cep
      t.string  :street
      t.string  :number
      t.string  :complement
      t.string  :latitude
      t.string  :longitude

      t.string  :login

      t.timestamps
    end

    add_index :hotels, :city_id
    add_index :hotels, :state
  end
end
