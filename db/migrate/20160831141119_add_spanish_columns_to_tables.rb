class AddSpanishColumnsToTables < ActiveRecord::Migration
  def change
    add_column :hotel_amenities, :internet_es, :text
    add_column :hotel_amenities, :park_es, :text
    add_column :hotel_amenities, :transfer_offered_by_the_hotel_es, :text
    add_column :hotel_amenities, :gratuity_for_children_es, :text
    add_column :hotels, :description_es, :text
    add_column :hotels, :payment_method_es, :text
    add_column :photos, :title_es, :string
    add_column :room_types, :name_es, :string
    add_column :room_types, :description_es, :text
    add_column :room_types, :booking_name_es, :string
  end

end
