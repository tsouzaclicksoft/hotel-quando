class AddPaypalTransactionIdToPayments < ActiveRecord::Migration
  def change
  	add_column :payments, :paypal_transaction_id, :string
  	add_column :payments, :paypal_refund_id, :string
  end
end
