class RemoveDelayedCheckoutValueFromOffers < ActiveRecord::Migration
  def change
    change_table :offers do |t|
      t.remove :delayed_checkout_value
    end
  end
end
