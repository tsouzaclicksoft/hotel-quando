class AirTaxes < ActiveRecord::Migration
	def change
		create_table :air_taxes do |t|

			t.references :travel_request, index: true, foreign_key: true

			#taxes
			t.decimal :fare, precision: 6, scale: 2, :default => 0.0
			t.decimal :boarding_taxes, precision: 6, scale: 2, :default => 0.0
			t.decimal :taxe_hq, precision: 6, scale: 2, :default => 0.0

			#politicas de cancelamento e alteracao
			t.decimal :taxe_change, precision: 6, scale: 2, :default => 0.0
			t.decimal :taxe_noshow, precision: 6, scale: 2, :default => 0.0
			t.decimal :taxe_cancellation, precision: 6, scale: 2, :default => 0.0

			t.timestamps
		end
	end
end
