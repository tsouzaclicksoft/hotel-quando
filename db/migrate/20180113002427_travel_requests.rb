class TravelRequests < ActiveRecord::Migration
	def change
		create_table :travel_requests do |t|

			t.references :hq_consultant, index: true, foreign_key: true
			t.references :user, index: true, foreign_key: true
			t.references :air_taxe, index: true, foreign_key: true

			t.datetime :date_request
			t.datetime :date_conclusion
			t.datetime :date_canceled
			t.integer :request_status
			t.integer :payment_status

			t.text :comments_traveler
			t.text :comments_consultant

			t.decimal :total, precision: 6, scale: 2, :default => 0.0
			t.decimal :total_paid_hq, precision: 6, scale: 2, :default => 0.0

			t.timestamps
		end
	end
end
