class CreateAgencies < ActiveRecord::Migration
  def change
    create_table :agencies do | t |
      t.string  :name
      t.boolean :is_active, default: true
      t.decimal :comission_in_percentage, precision: 5, scale: 2, default: 0
      t.text    :observations
      t.timestamps
    end
  end
end
