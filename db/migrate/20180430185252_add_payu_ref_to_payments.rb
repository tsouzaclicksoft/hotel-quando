class AddPayuRefToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :payu_order_id, :string
    add_column :payments, :payu_transaction_id, :string
  end
end
