class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string    :flag_name, limit: 20
      t.string    :owner_name
      t.string    :owner_cpf
      t.string    :owner_passport
      t.integer   :last_four_digits, limit: 4
      t.integer   :expiration_year, limit: 4
      t.integer   :expiration_month, limit: 2
      t.string    :maxipago_token
      t.integer   :user_id
    end

    add_index :credit_cards, :user_id
  end
end
