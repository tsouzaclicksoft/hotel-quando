class AddInInfoNewsletters < ActiveRecord::Migration
  def change
    add_column :newsletters, :emkt_flux, :integer
    add_column :newsletters, :past_emkt_date, :date
    add_column :newsletters, :emkt_flux_type, :integer
  end
end
