class CreateReferredFriendLogs < ActiveRecord::Migration
  def change
    create_table :referred_friend_logs do |t|
    	t.references		:user
    	t.references		:promotional_code
    	t.string  			:friend_name
    	t.string				:friend_email
      t.timestamps

      t.timestamps
    end
  end
end
