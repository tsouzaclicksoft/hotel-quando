class CreateMemberGetMemberConfig < ActiveRecord::Migration
  def change
    create_table :member_get_member_configs do |t|
    	t.references 			:campaign
    	t.integer 				:discount_type_to_referred
    	t.integer 				:discount_type_to_referred_first_bookings
      t.decimal         :discount_to_referred, precision: 6, scale: 2
      t.decimal         :discount_to_referred_first_bookings, precision: 6, scale: 2
      t.decimal					:indicators_credit, precision: 6, scale: 2
      t.decimal					:indicators_credit_first_bookings, precision: 6, scale: 2
      t.integer         :expiration_date_in_months
      t.integer         :number_of_first_bookings
      t.string					:currency_code_discount
      t.string          :currency_code_credit
      t.string          :avaiable_for_packs
      t.string					:avaiable_for_days_of_the_week
      t.boolean         :is_active
      t.timestamps
    end
  end
end


