class AddAcceptInvoicedPaymentToHotels < ActiveRecord::Migration
  def change
  	add_column :hotels, :accept_invoiced_payment, :boolean, default: false
  	Hotel.update_all('accept_invoiced_payment = accept_online_payment')
  end
end
