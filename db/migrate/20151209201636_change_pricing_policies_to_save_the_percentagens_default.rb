class ChangePricingPoliciesToSaveThePercentagensDefault < ActiveRecord::Migration
	def up
    change_column(:pricing_policies, :percentage_for_6h, :decimal, precision: 6, scale: 2, default: 43)
    change_column(:pricing_policies, :percentage_for_9h, :decimal, precision: 6, scale: 2, default: 63)
    change_column(:pricing_policies, :percentage_for_12h, :decimal, precision: 6, scale: 2, default: 73)
  end

  def down
  	change_column(:pricing_policies, :percentage_for_6h, :decimal, precision: 6, scale: 2, default: 45)
    change_column(:pricing_policies, :percentage_for_9h, :decimal, precision: 6, scale: 2, default: 65)
    change_column(:pricing_policies, :percentage_for_12h, :decimal, precision: 6, scale: 2, default: 70)
  end
end
