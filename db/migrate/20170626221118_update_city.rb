class UpdateCity < ActiveRecord::Migration
  def change
    add_column :cities, :place_id, :integer, array: true, default: []
  end
end
