class AddCpfAndPassportToTravelers < ActiveRecord::Migration
  def change
  	add_column :travelers, :cpf, :string
  	add_column :travelers, :passport, :string
  end
end
