class AddEpaycoRefToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :epayco_ref, :string
  end
end
