class AddCacheNoShowValueToBookings < ActiveRecord::Migration
  def change
    change_table :bookings do |t|
      t.decimal :cached_offer_no_show_value, precision: 6, scale: 2
    end
  end
end
