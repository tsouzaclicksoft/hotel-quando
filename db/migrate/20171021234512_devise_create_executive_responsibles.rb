class DeviseCreateExecutiveResponsibles < ActiveRecord::Migration
  def change
    create_table :executive_responsibles do |t|
      t.string :name
      t.string :login
      t.references :company_universe, index: true, foreign_key: true
      t.references :goals_executive_responsibles, index: true, foreign_key: true
      
      t.integer :amount_goal_contacts
      t.integer :amount_goal_meeting
      t.integer :amount_goal_new_companies
      t.integer :amount_goal_companies_actives
      t.integer :amount_goal_reservation
      t.boolean :is_active, :default => true

      ## Database authenticatable
      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

       ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0, :null => false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      t.timestamps
    end

    add_index :executive_responsibles, :login, :unique => true
    add_index :executive_responsibles, :reset_password_token, :unique => true
  end
end
