class CreateBookingPack < ActiveRecord::Migration
  def change
    create_table :booking_packs do |t|
    	t.integer  	:city_id
    	t.integer   :hotel_id
    	t.integer 	:room_type_id
    	t.integer   :bank_of_hours
    	t.integer   :number_of_pack_12h
    	t.boolean   :breakfast_included
    	t.decimal 	:price, precision: 8, scale: 2, default: 0
        t.timestamps
    end

    add_index :booking_packs, :city_id
    add_index :booking_packs, :hotel_id
    add_index :booking_packs, :room_type_id
  end
end
