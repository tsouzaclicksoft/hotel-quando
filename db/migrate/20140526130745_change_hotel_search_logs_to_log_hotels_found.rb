class ChangeHotelSearchLogsToLogHotelsFound < ActiveRecord::Migration
  def change
    change_table :hotel_search_logs do |t|
      t.text :exact_match_hotels_ids, limit: 4000
      t.text :similar_match_hotels_ids, limit: 4000
    end
  end
end
