class CreateJobTitles < ActiveRecord::Migration
  def change
    create_table :job_titles do |t|
			t.references :company_detail
			t.string :name
			t.string :money_sign
			t.decimal :budget_value, precision: 10, scale: 2, default: 0
			t.timestamps
    end
  end
end
