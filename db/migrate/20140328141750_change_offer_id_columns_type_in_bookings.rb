class ChangeOfferIdColumnsTypeInBookings < ActiveRecord::Migration
  def up
    execute 'ALTER TABLE bookings ALTER COLUMN offer_id TYPE numeric(20);'
  end

  def down
  end
end
