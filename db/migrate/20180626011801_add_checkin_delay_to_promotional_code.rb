class AddCheckinDelayToPromotionalCode < ActiveRecord::Migration
  def change
    add_column :promotional_codes, :checkin_delay, :integer, :default => 3

    PromotionalCode.update_all(checkin_delay: 3)
  end
end
