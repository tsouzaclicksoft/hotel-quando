class AddCanceledByAgencyFlagToBookings < ActiveRecord::Migration
  def change
    change_table :bookings do | t |
      t.boolean :canceled_by_agency
    end
  end
end
