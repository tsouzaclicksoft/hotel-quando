class UpdatePlaces < ActiveRecord::Migration
  def change
    add_column :places, :active, :boolean
    add_column :places, :street, :string
    add_column :places, :number, :integer
    add_column :places, :complement, :string
    add_column :places, :zipcode, :string
    add_column :places, :created_at, :datetime
    add_column :places, :updated_at, :datetime
    rename_column :places, :country_id, :country
    change_column :places, :country, :string
    change_column :places, :latitude, :string
    change_column :places, :longitude, :string
  end
end
