class AddCheckinTimestampDayOfWeekToOffers < ActiveRecord::Migration
  def change
    change_table :offers do |t|
      t.column :checkin_timestamp_day_of_week, 'numeric(1)'
    end

    add_index :offers, :checkin_timestamp_day_of_week
  end
end
