class PopulateReferCodeToUsers < ActiveRecord::Migration
  def up
  	users_generated = 0
  	users_cannot_generated = 0

  	User.all.each do |u|
  		if u.generate_refer_code
  			users_generated += 1
  		else
  			users_cannot_generated += 1
  		end
  	end
  	puts "users that generate refer code: " + users_generated.to_s
  	puts "users that cannot generate refer code: " + users_cannot_generated.to_s
  end

  def down
  	User.all.update_all(refer_code: nil)
  end
end
