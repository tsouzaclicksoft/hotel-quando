class AddMultiplusCodeToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :multiplus_code, :string
  end
end
