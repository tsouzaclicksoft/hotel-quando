class ChangeLimitNumberOfPeopleToHotelSearchLogs < ActiveRecord::Migration
  def up
  	change_column :hotel_search_logs, :number_of_people, 'numeric(3)'
  end
  def down
  	HotelSearchLog.where(number_of_people: 100).update_all(number_of_people: 20)
  	change_column :hotel_search_logs, :number_of_people, 'numeric(2)'
  end
end
