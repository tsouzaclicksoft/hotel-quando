class RemoveIndexUniqToCompanyUniverse < ActiveRecord::Migration
  def up
  	remove_index :company_universes, :cnpj
  end

  def down
  	add_index :company_universes, :cnpj, :unique => true
  end
end
