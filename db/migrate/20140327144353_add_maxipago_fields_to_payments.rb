class AddMaxipagoFieldsToPayments < ActiveRecord::Migration
  def change
    change_table :payments do |t|
      t.string :maxipago_auth_code
      t.string :maxipago_order_id
    end
  end
end
