class RenameColumnsPercentage < ActiveRecord::Migration
  def change
  	rename_column :companies, :markup_percentage, :markup_value
  	rename_column :affiliates, :comission_in_percentage, :comission_value
  	rename_column :agencies, :comission_in_percentage, :comission_value
  	rename_column :hotels, :comission_in_percentage, :comission_value
  end
end
