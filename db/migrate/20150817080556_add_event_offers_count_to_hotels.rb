class AddEventOffersCountToHotels < ActiveRecord::Migration
	def self.up
    add_column :hotels, :active_event_offers_count, :bigint

    Hotel.reset_column_information
    Hotel.all.pluck(:id).each do |hotel_id|
      active_event_offers_count = EventRoomOffer.where(hotel_id: hotel_id, status: EventRoomOffer::ACCEPTED_STATUS[:available]).count
      Hotel.where(id: hotel_id).update_all(active_event_offers_count: active_event_offers_count)
    end
  end

  def self.down
    remove_column :hotels, :active_event_offers_count
  end
end
