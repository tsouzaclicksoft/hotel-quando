class AddCurrencyCodeToBookings < ActiveRecord::Migration
  def change
  	add_column :bookings, :currency_code, :string
  end
end
