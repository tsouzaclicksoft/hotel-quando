class ChangeHotelsComissionToComissionInPercentage < ActiveRecord::Migration
  def change
    change_table :hotels do |t|
      t.rename :comission, :comission_in_percentage
    end
  end
end
