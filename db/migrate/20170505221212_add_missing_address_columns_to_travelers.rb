class AddMissingAddressColumnsToTravelers < ActiveRecord::Migration
  def change
    add_column :travelers, :number, :integer
    add_column :travelers, :complement, :string
  end
end
