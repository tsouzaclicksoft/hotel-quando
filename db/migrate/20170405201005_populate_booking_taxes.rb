class PopulateBookingTaxes < ActiveRecord::Migration
  def up
    iso_initials = Currency.pluck(:country).uniq
    countries = Country.where(iso_initials: iso_initials)
    countries.each do |country|
      next if BookingTax.where(country_id: country.id).any?
      BookingTax.create(name: 'fee_consumer_for_pack_3h', value: 0, country_id: country.id, currency_symbol: "US$")
      BookingTax.create(name: 'fee_consumer_for_pack_6h', value: 0, country_id: country.id, currency_symbol: "US$")
      BookingTax.create(name: 'fee_consumer_for_pack_9h', value: 0, country_id: country.id, currency_symbol: "US$")
      BookingTax.create(name: 'fee_consumer_for_pack_12h', value: 0, country_id: country.id, currency_symbol: "US$")
      BookingTax.create(name: 'financial_costs_percentage_to_public', value: 4, country_id: country.id, currency_symbol: "US$")
      BookingTax.create(name: 'financial_costs_percentage_to_affiliate', value: 4, country_id: country.id, currency_symbol: "US$")
    end
  end

  def down
    brazil_country = Country.where(iso_initials: "BR").first
    BookingTax.where.not(country_id: brazil_country.id).delete_all
  end
end
