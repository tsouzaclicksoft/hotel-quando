class AddFileInEmkt < ActiveRecord::Migration
  def change
    add_attachment :emkts, :header
    add_attachment :emkts, :box_1
    add_attachment :emkts, :box_2
    add_attachment :emkts, :box_3_1
    add_attachment :emkts, :box_3_2
    add_attachment :emkts, :box_3_3
    add_attachment :emkts, :box_3_4
    add_attachment :emkts, :box_3_5
    add_attachment :emkts, :box_3_6
    add_attachment :emkts, :footer

    remove_column :photos, :emkt_id, :integer
  end
end
