class UpdateHotelChain < ActiveRecord::Migration
  def change
    add_column :hotel_chains, :affiliate_id, :integer
  end
end
