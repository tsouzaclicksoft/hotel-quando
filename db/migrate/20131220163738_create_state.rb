# encoding: utf-8
class CreateState < ActiveRecord::Migration
  def change
    create_table :states do |t|
      t.string :initials
      t.string :name
    end

    add_index :states, :initials
  end
end
