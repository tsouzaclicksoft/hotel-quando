class AddOmnibeesConfigToHotels < ActiveRecord::Migration
  def change
    add_column :hotels, :omnibees_config, :text
  end
end
