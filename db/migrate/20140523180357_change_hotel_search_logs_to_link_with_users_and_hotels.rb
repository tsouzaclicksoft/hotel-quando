class ChangeHotelSearchLogsToLinkWithUsersAndHotels < ActiveRecord::Migration
  def change
    change_table :hotel_search_logs do |t|
      t.integer :search_logs_user_id
      t.string :address
      t.string :city_name
      t.string :state_name
    end

    add_index :hotel_search_logs, :search_logs_user_id
    add_index :hotel_search_logs, :city_name
    add_index :hotel_search_logs, :state_name
  end
end
