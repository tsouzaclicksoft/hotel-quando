class AddBookingTaxToBookings < ActiveRecord::Migration
  def change
    change_table(:bookings) do | t |
      t.decimal :booking_tax, precision: 8, scale: 2, default: 0
    end
  end
end
