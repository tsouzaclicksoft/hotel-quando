class AddExecutiveServiceToCompanyDetails < ActiveRecord::Migration
	def change
		add_column :company_details, :executive_service, :string
		add_column :company_details, :follow_up_date, :date
		add_column :company_details, :already_received_registration_form, :boolean, default: false
		add_column :company_details, :already_received_signed_contract, :boolean, default: false
		add_column :company_details, :already_aligned_campaigns, :boolean, default: false
		add_column :company_details, :received_registration_form_date, :date
		add_column :company_details, :received_signed_contract_date, :date
		add_column :company_details, :aligned_campaigns_date, :date
	end
end
