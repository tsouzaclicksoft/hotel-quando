class AddCountryIdToBookingTaxes < ActiveRecord::Migration
  def change
  	add_column :booking_taxes, :country_id, :integer
  	add_column :booking_taxes, :currency_symbol, :string

  	add_index :booking_taxes, :country_id
  end
end
