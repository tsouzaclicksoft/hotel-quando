class AddApiColumnsToAffiliate < ActiveRecord::Migration
  def self.up
    add_column :affiliates, :api_iframe_on, :boolean
    add_column :affiliates, :api_webservice_on, :boolean
    add_column :affiliates, :api_webservice_test_token, :string

    execute 'UPDATE affiliates set api_iframe_on=true where token IS NOT NULL;'
  end

  def self.down
    remove_column :affiliates, :api_iframe_on
    remove_column :affiliates, :api_webservice_on
    remove_column :affiliates, :api_webservice_test_token
  end

end
