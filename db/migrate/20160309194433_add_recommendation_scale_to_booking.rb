class AddRecommendationScaleToBooking < ActiveRecord::Migration
  def change
  	add_column :bookings, :recommendation_scale, :integer
  end
end
