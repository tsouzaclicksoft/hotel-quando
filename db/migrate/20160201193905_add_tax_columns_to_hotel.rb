class AddTaxColumnsToHotel < ActiveRecord::Migration
  def change
    add_column :hotels, :fee_on_charge, :decimal, default: 0, null: false, precision: 6, scale: 2
    add_column :hotels, :financial_costs, :decimal, default: 0, null: false, precision: 5, scale: 2
  end
end
