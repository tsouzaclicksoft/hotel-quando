class AddNoticeEmailToPromotionalCode < ActiveRecord::Migration
  def change
    add_column :promotional_codes, :notice_email, :string
  end
end
