class CreateCostCenters < ActiveRecord::Migration
  def change
    create_table :cost_centers do |t|
      t.references  :company_detail
      t.string      :name

      t.timestamps
    end
  end
end
