class AddResponsibleNameToUsers < ActiveRecord::Migration
  def change
    change_table(:users) do | t |
      t.string :responsible_name
    end
  end
end
