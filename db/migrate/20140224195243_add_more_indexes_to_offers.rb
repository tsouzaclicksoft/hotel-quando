class AddMoreIndexesToOffers < ActiveRecord::Migration
  def up
    add_index :offers, [:room_type_id, :pack_in_hours, :checkin_timestamp], name: 'index_offers_on_room_type_and_pack_length_and_checkin'
    add_index :offers, :room_type_id
    add_index :offers, [:room_type_id, :price]
    add_index :offers, [:pack_in_hours, :checkin_timestamp]
  end

  def down
    remove_index :offers, name: 'index_offers_on_room_type_and_pack_length_and_checkin'
    remove_index :offers, :room_type_id
    remove_index :offers, [:room_type_id, :price]
    remove_index :offers, [:pack_in_hours, :checkin_timestamp]
  end
end
