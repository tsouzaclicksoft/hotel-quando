class ChangeDatesAir < ActiveRecord::Migration
  def change
  	change_column :airs, :departure_date, :date
  	change_column :airs, :arrival_date, :date
  end
end
