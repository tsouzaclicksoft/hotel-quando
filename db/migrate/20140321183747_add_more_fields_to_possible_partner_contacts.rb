class AddMoreFieldsToPossiblePartnerContacts < ActiveRecord::Migration
  def up
    change_table :possible_partner_contacts do |t|
      t.string  :contact_name
      t.string  :contact_role
    end
  end

  def down
    change_table :possible_partner_contacts do |t|
      t.remove  :contact_name
      t.remove  :contact_role
    end
  end
end
