class AddDiscountInToPromotionalCode < ActiveRecord::Migration
  def change
  	add_column :promotional_codes, :discount_in, :integer

  	PromotionalCode.update_all(discount_in: PromotionalCode::ACCEPTED_DISCOUNT_IN[:offer_price])
  end
end
