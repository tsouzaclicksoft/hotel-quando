class AddBirthDateAndRemoveAgeFromUsers < ActiveRecord::Migration
  def up
    change_table :users do | t |
      t.remove :age
      t.date   :birth_date
    end
  end

  def down
    change_table :users do | t |
      t.integer :age
      t.remove  :birth_date
    end
  end
end
