class AddMoreFieldsToHotelSearchLogs < ActiveRecord::Migration
  def change
    change_table :hotel_search_logs do | t |
      t.float :latitude
      t.float :longitude
    end
  end
end
