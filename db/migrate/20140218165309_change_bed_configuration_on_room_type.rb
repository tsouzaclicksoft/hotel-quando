class ChangeBedConfigurationOnRoomType < ActiveRecord::Migration
  def change
    change_table :room_types do |t|
      t.rename :bed_configuration_pt_br, :extra_bed_configuration_pt_br
      t.rename :bed_configuration_en, :extra_bed_configuration_en
    end

  end
end
