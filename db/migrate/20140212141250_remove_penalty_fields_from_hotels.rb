class RemovePenaltyFieldsFromHotels < ActiveRecord::Migration
  def change
    change_table :hotels do |t|
      t.remove :penalty_for_36_hours_in_percentage
      t.remove :penalty_for_48_hours_in_percentage
    end
  end
end
