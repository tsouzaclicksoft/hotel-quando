class AddOmnibeesOfferToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :omnibees_offer_id, 'numeric(20)'
    add_index  :bookings, :omnibees_offer_id
  end
end
