class ChangeHotelsIssToIssInPercentage < ActiveRecord::Migration
  def change
    change_table :hotels do |t|
      t.rename :iss, :iss_in_percentage
    end
  end
end
