class AddAddressToTravelers < ActiveRecord::Migration
  def change
  	add_column :travelers, :street, :string
  	add_column :travelers, :city, :string
  	add_column :travelers, :state, :string
  	add_column :travelers, :country, :string
  	add_column :travelers, :postal_code, :string
  end
end
