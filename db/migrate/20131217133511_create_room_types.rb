# encoding: utf-8
class CreateRoomTypes < ActiveRecord::Migration
  def change
    create_table :room_types do |t|
      t.string  :name_pt_br
      t.string  :name_en
      t.text    :description_pt_br
      t.text    :description_en
      t.text    :bed_configuration_pt_br
      t.text    :bed_configuration_en
      t.integer :initial_capacity
      t.integer :maximum_capacity
      t.decimal  :extra_rate_in_percentage, precision: 5, scale: 2, :null => false
      t.integer :hotel_id

      t.timestamps
    end

    add_index :room_types, :hotel_id
  end
end
