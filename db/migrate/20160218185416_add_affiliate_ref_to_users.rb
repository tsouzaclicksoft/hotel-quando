class AddAffiliateRefToUsers < ActiveRecord::Migration
  def change
    change_table(:users) do | t |
      t.boolean :created_by_affiliate, default: false
      t.integer :affiliate_id
    end
    add_index :users, :affiliate_id
  end
end
