class ChangeNewsletterEmktDateField < ActiveRecord::Migration
  def up
    change_column :newsletters, :past_emkt_date, :string
    rename_column :newsletters, :past_emkt_date, :emkt_day_of_week
  end
  def down
    change_column :newsletters, :emkt_day_of_week, :string
    rename_column :newsletters, :emkt_day_of_week, :past_emkt_date
  end
end