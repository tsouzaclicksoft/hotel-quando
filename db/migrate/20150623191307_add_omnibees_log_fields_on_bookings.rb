class AddOmnibeesLogFieldsOnBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :omnibees_request_xml, :text
    add_column :bookings, :omnibees_response_xml, :text
  end
end
