class PopulateBookingTax < ActiveRecord::Migration
  def up
    BookingTax.create(name: 'fee_consumer_for_pack_3h', value: 19.90)
    BookingTax.create(name: 'fee_consumer_for_pack_6h', value: 19.90)
    BookingTax.create(name: 'fee_consumer_for_pack_9h', value: 14.90)
    BookingTax.create(name: 'fee_consumer_for_pack_12h', value: 14.90)
    BookingTax.create(name: 'financial_costs_percentage_to_public', value: 4)
    BookingTax.create(name: 'financial_costs_percentage_to_affiliate', value: 4)
  end

  def down
    BookingTax.delete_all
  end
end
