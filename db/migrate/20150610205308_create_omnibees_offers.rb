class CreateOmnibeesOffers < ActiveRecord::Migration
  def up
    create_table :omnibees_offers do |t|
      t.column     :checkin_timestamp, 'timestamp'
      t.column     :checkout_timestamp, 'timestamp'
      t.column     :pack_in_hours, 'numeric(2)'
      t.decimal    :price, precision: 6, scale: 2
      t.column     :room_type_id, 'numeric(5)'
      t.column     :hotel_id, 'numeric(5)'
      t.column     :number_of_guests, 'numeric(2)'
      t.text       :hotel_availabilities_xml
      t.text       :room_rate_xml
      t.text       :room_type_xml
      t.text       :rate_plan_xml
    end
    execute 'ALTER TABLE omnibees_offers ALTER COLUMN id TYPE numeric(20);'

    add_index :omnibees_offers, :id
    add_index :omnibees_offers, :hotel_id
    add_index :omnibees_offers, :room_type_id
    add_index :omnibees_offers, :checkin_timestamp
    add_index :omnibees_offers, :checkout_timestamp
  end

  def down
    execute 'DROP TABLE omnibees_offers;'
  end
end
