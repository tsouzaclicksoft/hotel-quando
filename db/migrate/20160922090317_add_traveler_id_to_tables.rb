class AddTravelerIdToTables < ActiveRecord::Migration
  def change
    add_column :bookings, :traveler_id, :integer
    add_column :payments, :traveler_id, :integer
    add_column :refunds, :traveler_id, :integer

    add_index :bookings, :traveler_id
    add_index :payments, :traveler_id
    add_index :refunds, :traveler_id
  end
end
