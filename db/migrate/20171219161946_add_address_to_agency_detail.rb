class AddAddressToAgencyDetail < ActiveRecord::Migration
  def change
  	add_column :agency_details, :street, :string
  	add_column :agency_details, :city, :string
  	add_column :agency_details, :state, :string
  	add_column :agency_details, :country, :string
  	add_column :agency_details, :postal_code, :string
    add_column :agency_details, :number, :integer
    add_column :agency_details, :complement, :string
  end
end
