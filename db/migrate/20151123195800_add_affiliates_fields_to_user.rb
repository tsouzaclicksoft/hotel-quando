class AddAffiliatesFieldsToUser < ActiveRecord::Migration
  def change
    change_table(:bookings) do | t |
      t.boolean :created_by_affiliate, default: false
      t.integer :affiliate_id
    end
    add_index "bookings", ["affiliate_id"], name: "index_bookings_on_affiliate_id", using: :btree
  end
end