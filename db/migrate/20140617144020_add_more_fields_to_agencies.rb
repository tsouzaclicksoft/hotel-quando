class AddMoreFieldsToAgencies < ActiveRecord::Migration
  def change
    change_table(:agencies) do | t |
      t.string :cnpj
      t.string :phone_number
    end
  end
end
