class AddDiscountCachedToPromotionalCodeLogs < ActiveRecord::Migration
  def change
  	add_column :promotional_code_logs, :discount_cached, :decimal, precision: 6, scale: 2, default: 0
  	add_column :promotional_code_logs, :discount_type_cached, :integer
  end
end
