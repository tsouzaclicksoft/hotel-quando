class AddComissionToHotels < ActiveRecord::Migration
  def change
    change_table :hotels do | t |
      t.decimal :comission, precision: 5, scale: 2, default: 0
    end
  end
end
