class CreateEventRoomOffers < ActiveRecord::Migration
	def up
    create_table :event_room_offers do |t|
      t.column     :checkin_timestamp, 'timestamp', default: 'NOW()', extra: ''
      t.column     :checkout_timestamp, 'timestamp', default: 'NOW()', extra: ''
      t.column     :pack_in_hours, 'numeric(2)'
      t.decimal    :price, precision: 6, scale: 2
      t.column     :status, 'numeric(2)'
      t.column     :event_room_maximum_capacity, 'numeric(2)'
      t.decimal    :no_show_value, precision: 6, scale: 2
      t.column     :event_room_id, 'numeric(5)'
      t.column     :hotel_id, 'numeric(5)'
      t.integer    :log_id
      t.column     :checkin_timestamp_day_of_week, 'numeric(1)'
    end
    execute 'ALTER TABLE event_room_offers ALTER COLUMN id TYPE numeric(20);'
    # next is the rule to not thrown an exeption when inserting a composet index that already exists
    execute 'CREATE OR REPLACE RULE ignore_duplicate_inserts_on_offers AS ON INSERT TO event_room_offers
   WHERE (EXISTS ( SELECT 1 FROM event_room_offers WHERE event_room_offers.event_room_id = new.event_room_id and event_room_offers.checkin_timestamp = new.checkin_timestamp and event_room_offers.pack_in_hours = new.pack_in_hours))
   DO INSTEAD NOTHING;'

    add_index :event_room_offers, :id
    add_index :event_room_offers, :event_room_id
    add_index :event_room_offers, :hotel_id
    add_index :event_room_offers, :checkin_timestamp
    add_index :event_room_offers, :checkout_timestamp
    add_index :event_room_offers, :status
    add_index :event_room_offers, [:event_room_id, :pack_in_hours, :checkin_timestamp], name: 'index_e_r_offers_on_room_and_hotel_and_pack_length_and_checkin', unique: true
    add_index :event_room_offers, :log_id
    add_index :event_room_offers, :checkin_timestamp_day_of_week
    add_index :event_room_offers, [:pack_in_hours, :checkin_timestamp], name: 'index_e_r_offers_on_pack_in_hours_and_checkin'
    add_index :event_room_offers, [:hotel_id, :status, :pack_in_hours], name: 'index_e_r_offers_on_hotel_id_and_status_and_pack_in_hours'
  end

  def down
    execute 'DROP TABLE event_room_offers;'
  end
end
