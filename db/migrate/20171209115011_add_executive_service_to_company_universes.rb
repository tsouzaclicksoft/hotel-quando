class AddExecutiveServiceToCompanyUniverses < ActiveRecord::Migration
	def change
		add_column :company_universes, :executive_service, :string
		add_column :company_universes, :follow_up_date, :date
	end
end
