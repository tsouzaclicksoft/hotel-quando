class CreateCompanyRegions < ActiveRecord::Migration
  def change
    create_table :company_regions do |t|
      t.string :name
      t.references :city, index: true
      t.boolean :is_active, :default => true

      t.timestamps
    end
  end
end
