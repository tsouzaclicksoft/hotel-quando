class AddAvailableForPacksAndWeekDaysToPromotionalCodes < ActiveRecord::Migration
  def change
  	add_column :promotional_codes, :avaiable_for_packs, :string
  	add_column :promotional_codes, :avaiable_for_days_of_the_week, :string
  end
end
