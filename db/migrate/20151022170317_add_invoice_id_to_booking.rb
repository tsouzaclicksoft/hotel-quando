class AddInvoiceIdToBooking < ActiveRecord::Migration
  def change
  	add_column :bookings, :invoice_id, :integer
  end
end
