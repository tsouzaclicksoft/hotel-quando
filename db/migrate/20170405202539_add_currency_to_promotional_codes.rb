class AddCurrencyToPromotionalCodes < ActiveRecord::Migration
  def change
    add_column :promotional_codes, :currency_code, :string
  end
end
