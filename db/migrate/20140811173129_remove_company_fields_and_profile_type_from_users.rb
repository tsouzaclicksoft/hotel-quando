class RemoveCompanyFieldsAndProfileTypeFromUsers < ActiveRecord::Migration
  def change
    remove_index :users, :profile_type

    change_table(:users) do | t |
      t.remove :profile_type
      t.remove :cnpj
      t.remove :agency_id
      t.remove :responsible_name
    end

  end
end
