class RenameOffersLogsToOffersCreatorLog < ActiveRecord::Migration
  def up
  	rename_table :offers_logs, :offers_creator_logs
  end

  def down
  	rename_table :offers_creator_logs, :offers_logs
  end
end
