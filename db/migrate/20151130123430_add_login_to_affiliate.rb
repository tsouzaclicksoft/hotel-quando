class AddLoginToAffiliate < ActiveRecord::Migration
  def change
    add_column :affiliates, :login, :string
  end
end
