class AddFieldToAffiliate < ActiveRecord::Migration
  def change
    add_column :affiliates, :only_meeting_room, :boolean, default: :false
  end
end
