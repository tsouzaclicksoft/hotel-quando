class AddColumnsToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :cancellation_request_xml, :text
    add_column :bookings, :cancellation_response_xml, :text
  end
end
