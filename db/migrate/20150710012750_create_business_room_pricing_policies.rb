class CreateBusinessRoomPricingPolicies < ActiveRecord::Migration
  def change
    create_table :business_room_pricing_policies do |t|
      t.string  :name
      t.integer :hotel_id
      t.string  :type
      t.decimal :percentage_for_1h, precision: 6, scale: 2, default: 12.5
      t.decimal :percentage_for_2h, precision: 6, scale: 2, default: 25
      t.decimal :percentage_for_3h, precision: 6, scale: 2, default: 37.5
      t.decimal :percentage_for_4h, precision: 6, scale: 2, default: 50
      t.decimal :percentage_for_5h, precision: 6, scale: 2, default: 62.5
      t.decimal :percentage_for_6h, precision: 6, scale: 2, default: 75
      t.decimal :percentage_for_7h, precision: 6, scale: 2, default: 87.5
      t.decimal :percentage_for_8h, precision: 6, scale: 2, default: 100
      t.string  :percentage_profile_name, limit: 20, default: 'normal'
      t.timestamps
    end

    add_index :business_room_pricing_policies, :hotel_id
  end
end
