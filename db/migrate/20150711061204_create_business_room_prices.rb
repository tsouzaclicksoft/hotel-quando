class CreateBusinessRoomPrices < ActiveRecord::Migration
  def change
    create_table :business_room_prices do |t|
      t.decimal :pack_price_1h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_2h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_3h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_4h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_5h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_6h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_7h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_8h, precision: 6, scale: 2, default: 0
      t.decimal :penalty_for_1h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_2h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_3h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_4h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_5h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_6h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_7h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_8h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.integer :business_room_pricing_policy_id
      t.integer :business_room_id
      t.timestamps
    end

    add_index :business_room_prices, :business_room_pricing_policy_id
    add_index :business_room_prices, :business_room_id
  end
end
