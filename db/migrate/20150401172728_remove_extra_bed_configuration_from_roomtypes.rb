class RemoveExtraBedConfigurationFromRoomtypes < ActiveRecord::Migration
  def change
    remove_column :room_types, :extra_bed_configuration_pt_br
    remove_column :room_types, :extra_bed_configuration_en
  end
end
