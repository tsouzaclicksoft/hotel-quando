class AddSignEmailToAffiliate < ActiveRecord::Migration
  def change
    add_column :affiliates, :sign_email, :string
  end
end
