class AddNumberAndCvvToCreditCards < ActiveRecord::Migration
  def change
  	add_column :credit_cards, :card_number, :string
  	add_column :credit_cards, :cvv, :integer
  end
end
