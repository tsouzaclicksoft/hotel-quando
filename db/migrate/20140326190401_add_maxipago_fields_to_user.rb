class AddMaxipagoFieldsToUser < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :maxipago_token
    end
  end
end
