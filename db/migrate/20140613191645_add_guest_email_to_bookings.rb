class AddGuestEmailToBookings < ActiveRecord::Migration
  def change
    change_table(:bookings) do | t |
      t.string :guest_email
    end
  end
end
