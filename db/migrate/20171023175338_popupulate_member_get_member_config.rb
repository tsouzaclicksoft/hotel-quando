class PopupulateMemberGetMemberConfig < ActiveRecord::Migration
  def up
  	campaign = Campaign.last.present? ? Campaign.last : Campaign.create(name: "Campaign")
  	MemberGetMemberConfig.create(
		    campaign_id: campaign.id,
		    discount_type_to_referred: PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:money_value],
		    discount_type_to_referred_first_bookings: PromotionalCode::ACCEPTED_DISCOUNT_TYPE[:money_value],
		    discount_to_referred: 20,
		    discount_to_referred_first_bookings: 50,
		    indicators_credit: 20,
		    indicators_credit_first_bookings: 50,
		    expiration_date_in_months: 6,
		    number_of_first_bookings: 10,
		    currency_code_discount: "R$",
		    currency_code_credit: "R$",
		    is_active: false,
		    avaiable_for_packs: "3,6,9,12",
		    avaiable_for_days_of_the_week: "0,1,2,3,4,5,6"
  		)
  end

  def down
  	MemberGetMemberConfig.last.destroy
  end
end
