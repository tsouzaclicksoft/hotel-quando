class AddMultilanguageNameToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :name_en, :string
    add_column :places, :name_es, :string
    rename_column :places, :name, :name_pt_br
  end
end
