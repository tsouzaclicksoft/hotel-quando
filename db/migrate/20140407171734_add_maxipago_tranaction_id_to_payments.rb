class AddMaxipagoTranactionIdToPayments < ActiveRecord::Migration
  def change
    change_table :payments do |t|
      t.string   :maxipago_transaction_id
    end
  end
end
