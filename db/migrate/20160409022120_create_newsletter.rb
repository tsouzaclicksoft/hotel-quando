class CreateNewsletter < ActiveRecord::Migration
  def change
    create_table :newsletters do |t|
    	t.string :email, :null => false, :default => ""
    	t.string :city
    	t.timestamps
    end

    add_index :newsletters, :email
  end
end
