class AddPenaltyAndExtraPricePerPersonFieldsToRoomTypePricingPolicies < ActiveRecord::Migration
  def change
    change_table :room_type_pricing_policies do |t|
      t.decimal :extra_price_per_person_for_pack_price_3h, precision: 6, scale: 2, default: 0
      t.decimal :extra_price_per_person_for_pack_price_6h, precision: 6, scale: 2, default: 0
      t.decimal :extra_price_per_person_for_pack_price_9h, precision: 6, scale: 2, default: 0
      t.decimal :extra_price_per_person_for_pack_price_12h, precision: 6, scale: 2, default: 0
      t.decimal :extra_price_per_person_for_pack_price_24h, precision: 6, scale: 2, default: 0
      t.decimal :extra_price_per_person_for_pack_price_36h, precision: 6, scale: 2, default: 0
      t.decimal :extra_price_per_person_for_pack_price_48h, precision: 6, scale: 2, default: 0
      t.decimal :penalty_for_3h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_6h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_9h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_12h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_24h_pack_in_percentage, precision: 5, scale: 2, default: 100
      t.decimal :penalty_for_36h_pack_in_percentage, precision: 5, scale: 2, default: 66.7
      t.decimal :penalty_for_48h_pack_in_percentage, precision: 5, scale: 2, default: 50
    end
  end
end
