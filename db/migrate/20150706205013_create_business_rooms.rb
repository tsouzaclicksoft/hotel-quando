class CreateBusinessRooms < ActiveRecord::Migration
  def change
    create_table :business_rooms do |t|
    	t.string  :name_pt_br
      t.string  :name_en
      t.text    :description_pt_br
      t.text    :description_en
      t.integer :maximum_capacity
      t.string	:type
      t.integer :hotel_id
      t.timestamps
    end

    add_index :business_rooms, :hotel_id
  end
end
