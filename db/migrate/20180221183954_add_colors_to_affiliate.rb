class AddColorsToAffiliate < ActiveRecord::Migration
  def change
  	add_column :affiliates, :header_text_color, :string
  	add_column :affiliates, :header_color, :string
  	add_column :affiliates, :search_box_color, :string
  	add_column :affiliates, :search_box_text_color, :string
  	add_column :affiliates, :search_box_button_color, :string
  	add_column :affiliates, :search_box_button_text_color, :string
    add_column :affiliates, :footer_color, :string
    add_column :affiliates, :footer_text_color, :string
    add_column :affiliates, :custom_layout, :boolean, default: false
  end
end
