class AddMaxipagoTokenToTravelers < ActiveRecord::Migration
  def change
    change_table :travelers do |t|
      t.string :maxipago_token
    end
  end
end
