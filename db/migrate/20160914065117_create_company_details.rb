class CreateCompanyDetails < ActiveRecord::Migration
  def change
    create_table :company_details do |t|
    	t.references 	:user
    	t.references 	:agency_detail
    	t.string 			:name
    	t.string 			:cnpj
    	t.string 			:financial_contact
    	t.string			:phone_financial_contact

      t.timestamps
    end
  end
end
