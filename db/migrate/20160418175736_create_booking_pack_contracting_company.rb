class CreateBookingPackContractingCompany < ActiveRecord::Migration
  def change
    create_table :booking_pack_contracting_companies do |t|
			t.integer 	:booking_pack_id
			t.string  	:name
			t.string  	:cnpj
			t.string  	:responsible_name
			t.string  	:phone_number
			t.string  	:role
			t.string  	:email
			t.integer   :cached_room_type_id
			t.integer   :cached_booking_pack_bank_of_hours
			t.integer   :cached_booking_pack_number_of_pack_12h
			t.decimal 	:cached_booking_pack_price, precision: 8, scale: 2, default: 0
			t.timestamp
    end
    add_index :booking_pack_contracting_companies, :booking_pack_id
  end
end
