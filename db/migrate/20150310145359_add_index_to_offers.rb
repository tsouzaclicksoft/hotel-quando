class AddIndexToOffers < ActiveRecord::Migration
  def change
    add_index :offers, [:hotel_id, :status, :pack_in_hours]
  end
end
