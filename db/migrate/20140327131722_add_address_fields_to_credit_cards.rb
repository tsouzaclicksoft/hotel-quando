class AddAddressFieldsToCreditCards < ActiveRecord::Migration
  def change
    change_table :credit_cards do |t|
      t.string :billing_name
      t.string :billing_address1
      t.string :billing_city
      t.string :billing_state
      t.string :billing_zipcode
      t.string :billing_country
      t.string :billing_phone
    end
  end
end
