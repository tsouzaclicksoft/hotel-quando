class AddIndexesOnLatitudeAndLongitudeToHotels < ActiveRecord::Migration
  def change
    add_index :hotels, :latitude
    add_index :hotels, :longitude
    add_index :hotels, [:latitude, :longitude]
  end
end
