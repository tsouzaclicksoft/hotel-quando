class AddCreatedByAgencyFlagToBookings < ActiveRecord::Migration
  def change
    change_table(:bookings) do | t |
      t.boolean :created_by_agency, default: false
    end
  end
end
