class AddTimezoneToHotels < ActiveRecord::Migration
  def change
    add_column :hotels, :timezone, :string
  end
end
