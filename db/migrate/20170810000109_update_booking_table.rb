class UpdateBookingTable < ActiveRecord::Migration
  def change
    add_column :bookings, :expired_at, :datetime
  end
end
