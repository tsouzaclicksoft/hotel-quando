class AddUsingBalanceToBookings < ActiveRecord::Migration
  def change
  	add_column :bookings, :using_balance, :boolean, default: :false
  	add_column :bookings, :balance_value_used, :decimal, precision: 6, scale: 2, default: 0
  end
end
