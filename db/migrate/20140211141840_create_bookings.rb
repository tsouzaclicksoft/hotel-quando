class CreateBookings < ActiveRecord::Migration
  def up
    execute "CREATE EXTENSION IF NOT EXISTS btree_gist;"
    create_table :bookings do |t|
      t.integer :status
      t.string  :guest_name
      t.string  :note
      t.text    :hotel_comments
      t.integer :user_id
      t.integer :offer_id
      t.integer :room_id
      t.column  :date_interval, 'tsrange'
    end
    add_index :bookings, :status
    add_index :bookings, :user_id
    add_index :bookings, :offer_id
    add_index :bookings, :room_id
    add_index :bookings, :date_interval

    execute %Q{
      ALTER TABLE bookings ADD CONSTRAINT overlapping_times EXCLUDE USING gist (room_id WITH =, date_interval WITH &&)
    }

    # execute %Q{
    #   CREATE OR REPLACE RULE notify_duplicate_inserts_on_bookings
    #   AS ON INSERT TO bookings
    #   WHERE (EXISTS
    #     (SELECT id FROM bookings WHERE
    #       (bookings.room_id = new.room_id
    #         AND ((bookings.checkin_timestamp, bookings.checkout_timestamp) OVERLAPS (new.checkin_timestamp::TIMESTAMP - '59 MINUTE'::INTERVAL, new.checkout_timestamp::TIMESTAMP + '59 MINUTE'::INTERVAL))
    #         AND bookings.status = '2')
    #       )
    #     )
    #   DO INSTEAD RAISE NOTICE 'Cannot insert booking';
    # }

    # execute %Q{
    #   ALTER TABLE bookings ADD CONSTRAINT overlapping_times EXCLUDE USING GIST (
    #     box(
    #         point( extract(epoch FROM checkin_timestamp), extract(epoch FROM checkin_timestamp) ),
    #         point( extract(epoch FROM checkout_timestamp), extract(epoch FROM checkout_timestamp) )
    #     ) WITH &&
    #   )
    # }
  end

  def down
    execute "ALTER TABLE bookings DROP CONSTRAINT overlapping_times"
    # execute 'DROP RULE notify_duplicate_inserts_on_bookings ON bookings;'
    execute 'DROP TABLE bookings;'
  end
end

# TRUNCATE TABLE offers;
# BEGIN;
#   INSERT INTO bookings
#   ("status",
#    "guest_name",
#    "note",
#    "hotel_comments",
#    "user_id",
#    "offer_id",
#    "room_id",
#    "date_interval")
#   VALUES
#   ( '2',
#     'guest_name',
#     'note',
#     'hotel_comments',
#     '1',
#     '1',
#     '1',
#     '[2014-02-14 03:00:01, 2014-02-15 03:00:00]');

#   INSERT INTO offers ("checkin_timestamp",
#     "checkout_timestamp",
#     "pack_in_hours",
#     "price",
#     "extra_price_per_person_in_percentage",
#     "status",
#     "room_type_initial_capacity",
#     "room_type_maximum_capacity",
#     "no_show_value",
#     "delayed_checkout_value",
#     "room_id",
#     "room_type_id",
#     "hotel_id",
#     "log_id")
#     VALUES
#       ('2014-02-11 04:00:00',
#       '2014-02-15 03:00:00',
#       '3',
#       '50',
#       '10',
#       '98',
#       '1',
#       '1',
#       '10',
#       '20',
#       '1',
#       '1',
#       '1',
#       '1');
# COMMIT;

