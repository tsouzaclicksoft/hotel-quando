class RemoveCreditCardsFieldsFromPayments < ActiveRecord::Migration
  def up
    change_table :payments do | t |
      t.remove    :credit_card_type
      t.remove    :active
      t.remove    :credit_card_owner
      t.remove    :masked_credit_card_number
      t.remove    :credit_card_expiration_date
      t.remove    :credit_card_security_code
      t.remove    :token
      t.remove    :token_validity_date
      t.remove    :register_date
      t.remove    :cancel_date
      t.remove    :gateway_response
    end
  end

  def down
  end
end
