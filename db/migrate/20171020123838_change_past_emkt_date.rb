class ChangePastEmktDate < ActiveRecord::Migration
  def change
    change_column :users, :past_emkt_date, :string
    rename_column :users, :past_emkt_date, :emkt_day_of_week
  end
end
