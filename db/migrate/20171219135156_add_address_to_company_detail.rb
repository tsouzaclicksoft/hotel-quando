class AddAddressToCompanyDetail < ActiveRecord::Migration
  def change
  	add_column :company_details, :street, :string
  	add_column :company_details, :city, :string
  	add_column :company_details, :state, :string
  	add_column :company_details, :country, :string
  	add_column :company_details, :postal_code, :string
    add_column :company_details, :number, :integer
    add_column :company_details, :complement, :string
  end
end
