class ChangeBookingsConstraint < ActiveRecord::Migration
  def up
    execute "ALTER TABLE bookings DROP CONSTRAINT overlapping_times"
    change_table :bookings do |t|
      t.boolean :is_active, default: true
    end

    execute %Q{
      ALTER TABLE bookings ADD CONSTRAINT overlapping_times EXCLUDE USING gist (room_id WITH =, date_interval WITH &&) where(is_active = true)
    }

  end

  def down
    execute "ALTER TABLE bookings DROP CONSTRAINT overlapping_times"

    execute %Q{
      ALTER TABLE bookings ADD CONSTRAINT overlapping_times EXCLUDE USING gist (room_id WITH =, date_interval WITH &&)
    }

    change_table :bookings do |t|
      t.remove :is_active
    end

  end
end
