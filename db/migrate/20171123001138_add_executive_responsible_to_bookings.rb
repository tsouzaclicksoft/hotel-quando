class AddExecutiveResponsibleToBookings < ActiveRecord::Migration
	def change
		add_reference :bookings, :executive_responsible, index: true, foreign_key: true
	end
end
