class AddHotelIdToBookings < ActiveRecord::Migration
  def change
    change_table :bookings do |t|
      t.integer :hotel_id
    end

    add_index :bookings, :hotel_id
  end
end
