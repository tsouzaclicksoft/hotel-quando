class CreateCurrency < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :name
      t.string :country
      t.string :money_sign
      t.decimal :value, precision: 6, scale: 2, default: 0
    end
  end
end
