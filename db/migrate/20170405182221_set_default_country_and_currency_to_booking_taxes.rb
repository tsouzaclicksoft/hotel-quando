class SetDefaultCountryAndCurrencyToBookingTaxes < ActiveRecord::Migration
  def change
  	brazil_country_id = Country.where(iso_initials: "BR").first
  	BookingTax.all.update_all(country_id: brazil_country_id.id, currency_symbol: "US$")
  end
end
