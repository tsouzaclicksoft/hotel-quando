class ChangeTypeFieldOfPossiblePartnerContacts < ActiveRecord::Migration
  def up
    change_table :possible_partner_contacts do |t|
      t.rename  :type, :type_of_contact
    end
  end

  def down
    change_table :possible_partner_contacts do |t|
      t.rename  :type_of_contact, :type
    end
  end
end
