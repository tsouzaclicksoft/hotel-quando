class SetDefaultInOldPromotionalCode < ActiveRecord::Migration
  def up
    PromotionalCode.where(currency_code: nil, discount_type: 2).update_all(currency_code: "R$")
  end
end
