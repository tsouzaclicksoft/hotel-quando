class AddAffiliateIdToPhotos < ActiveRecord::Migration
  def change
  	add_reference :photos, :affiliate
  end
end
