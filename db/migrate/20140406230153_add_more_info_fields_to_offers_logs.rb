class AddMoreInfoFieldsToOffersLogs < ActiveRecord::Migration
  def change
    change_table :offers_logs do |t|
      t.text    	:logged_room_types_and_prices_used, limit: 2.megabytes
      t.boolean   :reprocessed, default: false
    end
  end
end
