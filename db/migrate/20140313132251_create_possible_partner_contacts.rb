class CreatePossiblePartnerContacts < ActiveRecord::Migration
  def change
    create_table :possible_partner_contacts do |t|
      t.string  :hotel_name
      t.string  :phone
      t.string  :email_for_contact
      t.string  :locale
      t.text    :message
      t.boolean :admin_visualized, default: false
      t.timestamps
    end
  end
end
