class RemoveReferenceToTravelRequest < ActiveRecord::Migration
  def change
  	remove_reference :travel_requests, :air_taxe, index: true, foreign_key: true
  	remove_reference :air_taxes, :travel_request, index: true, foreign_key: true
  	add_reference :airs, :air_taxe, index: true, foreign_key: true
  	add_reference :air_taxes, :air, index: true, foreign_key: true
  end
end
