class AddIsAppMobileToBookingsAndPayments < ActiveRecord::Migration
  def change
  	add_column :bookings,  :is_app_mobile, :boolean, default: false
  	add_column :payments,  :is_app_mobile, :boolean, default: false
  end
end
