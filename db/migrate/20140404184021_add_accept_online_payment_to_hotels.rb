class AddAcceptOnlinePaymentToHotels < ActiveRecord::Migration
  def change
    change_table :hotels do |t|
      t.boolean :accept_online_payment, default: false
    end
  end
end
