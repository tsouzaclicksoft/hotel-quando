class AddCountryToExecutiveResponsibles < ActiveRecord::Migration
  def change
  	add_reference :executive_responsibles, :country
  end
end
