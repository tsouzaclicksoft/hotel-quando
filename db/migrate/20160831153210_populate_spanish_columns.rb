class PopulateSpanishColumns < ActiveRecord::Migration
  def up
    Hotel.update_all('description_es = description_pt_br, payment_method_es = payment_method_pt_br')
    HotelAmenity.update_all('internet_es = internet_pt_br, park_es = park_pt_br, transfer_offered_by_the_hotel_es = transfer_offered_by_the_hotel_pt_br, gratuity_for_children_es = gratuity_for_children_pt_br')
    RoomType.update_all('name_es = name_pt_br, description_es = description_pt_br, booking_name_es = booking_name_pt_br')
    Photo.update_all('title_es = title_pt_br')
  end

  def down
    HotelAmenity.update_all(internet_es: nil, park_es: nil, transfer_offered_by_the_hotel_es: nil, gratuity_for_children_es: nil)
    Hotel.update_all(description_es: nil, payment_method_es: nil)
    RoomType.update_all(name_es: nil, description_es: nil, booking_name_es: nil)
    Photo.update_all(title_es: nil)
  end
end