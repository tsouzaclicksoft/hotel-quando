class AddRoomCategoryToSearchLog < ActiveRecord::Migration
  def self.up
  	add_column :hotel_search_logs, :room_category, :integer
  end
  def self.down	
  	remove_column :hotel_search_logs, :room_category
	end
end
