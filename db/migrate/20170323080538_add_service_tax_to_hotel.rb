class AddServiceTaxToHotel < ActiveRecord::Migration
  def change
    change_table :hotels do |t|
      t.decimal :service_tax, precision: 4, scale: 1, default: 0
    end
  end
end
