class CreateHotelSearchLog < ActiveRecord::Migration
  def change
    create_table :hotel_search_logs do |t|
      t.integer :city_id
      t.string  :address
      t.string  :checkin_date
      t.column  :checkin_hour, 'numeric(2)'
      t.column  :number_of_people, 'numeric(2)'
      t.column  :length_of_pack, 'numeric(2)'
      t.timestamps
    end
  end
end
