class AddingMoreColumnsToBookings < ActiveRecord::Migration
  def up
    change_table :bookings do |t|
      t.integer :room_type_id
      t.integer :pack_in_hours
      t.column  :number_of_people, 'numeric(2)'
      t.date    :checkin_date
      t.timestamps
    end

    add_index :bookings, :room_type_id
    add_index :bookings, :pack_in_hours
  end

  def down
    change_table :bookings do |t|
      t.remove :room_type_id
      t.remove :pack_in_hours
      t.remove :checkin_date
      t.remove :number_of_people
      t.remove :created_at
      t.remove :updated_at
    end

  end

end
