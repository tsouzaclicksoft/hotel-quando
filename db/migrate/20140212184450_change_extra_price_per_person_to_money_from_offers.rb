class ChangeExtraPricePerPersonToMoneyFromOffers < ActiveRecord::Migration
  def change
    change_table :offers do |t|
      t.remove  :extra_price_per_person_in_percentage
      t.decimal :extra_price_per_person, precision: 6, scale: 2, default: 0
    end
  end
end
