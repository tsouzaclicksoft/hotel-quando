# encoding: utf-8
class ChangeLatitudeAndLongitudeFieldsToFloatFromHotels < ActiveRecord::Migration
 def up
    change_table :hotels do | t |
      t.float :latitude_f
      t.float :longitude_f
    end

    puts('Iniciando conversão de latitude e longitude dos hotéis de string para float')
    puts('---------------------------------------------------------------------------')

    all_conversions_ok = true

    Hotel.all.each do | hotel |
      latitude = hotel.latitude.gsub(/\s/,'')
      longitude = hotel.longitude.gsub(/\s/,'')

      puts("\n\nHotel com id #{hotel.id}")

      if latitude.match(/[^0-9\-\.]+/)
        puts("Latitude não foi convertida com sucesso, valor encontrado: #{latitude}.")
        all_conversions_ok = false
      else
        hotel.latitude_f = latitude
        hotel.save!(validate: false)
        puts("Latitude convertida com sucesso.")
      end
      if longitude.match(/[^0-9\-\.]+/)
        puts("Longitude não foi convertida com sucesso, valor encontrado: #{longitude}.")
        all_conversions_ok = false
      else
        hotel.longitude_f = longitude
        hotel.save!(validate: false)
        puts("Longitude convertida com sucesso.")
      end
    end

    unless all_conversions_ok
      puts("\n\nHá conversões que não puderam ser feitas, ao continuar, os dados não convertidos serão perdidos.")
      puts("Deseja realmente continuar? (s/n)")
      resp = STDIN.gets.chomp
      unless resp == 's'
        raise 'Migração cancelada'
      end
    end

    change_table :hotels do | t |
      t.remove :latitude
      t.remove :longitude
      t.rename :latitude_f, :latitude
      t.rename :longitude_f, :longitude
    end
  end

  def down
    change_column :hotels, :latitude, :string
    change_column :hotels, :longitude, :string
  end
end
