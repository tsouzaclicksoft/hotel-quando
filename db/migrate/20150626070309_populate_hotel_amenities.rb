class PopulateHotelAmenities < ActiveRecord::Migration
  def change
  	Hotel.all.each do |h|
  		if h.hotel_amenity.nil?
  			HotelAmenity.create(hotel_id: h.id)
  		end
  	end
  end
end
