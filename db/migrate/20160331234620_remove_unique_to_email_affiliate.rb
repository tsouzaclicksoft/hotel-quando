class RemoveUniqueToEmailAffiliate < ActiveRecord::Migration
  def up
   	execute "DROP INDEX index_affiliates_on_email"
  	add_index :affiliates, :email, :unique => false
  end

  def down
   	execute "DROP INDEX index_affiliates_on_email"
  	add_index :affiliates, :email, :unique => true
  end
end
