class AddBedQuantitiesToRoomTypes < ActiveRecord::Migration
  def up
    change_table :room_types do |t|
      t.column :single_bed_quantity, 'numeric(2)', :default => 0
      t.column :double_bed_quantity, 'numeric(2)', :default => 0
    end
  end

  def down
    change_table :room_types do |t|
      t.remove :single_bed_quantity
      t.remove :double_bed_quantity
    end
  end
end
