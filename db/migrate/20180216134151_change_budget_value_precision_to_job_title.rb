class ChangeBudgetValuePrecisionToJobTitle < ActiveRecord::Migration
  def up
  	change_column(:job_titles, :budget_value, :decimal, precision: 20, scale: 2, default: 0)
  end

  def down
  	change_column(:job_titles, :budget_value, :decimal, precision: 10, scale: 2, default: 0)
  end
end
