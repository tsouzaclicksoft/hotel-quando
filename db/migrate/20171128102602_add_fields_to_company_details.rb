class AddFieldsToCompanyDetails < ActiveRecord::Migration
	def change
		add_column :company_details, :has_an_agency, :boolean, default: false
		add_column :company_details, :how_your_company_pays_this_agency, :text
		add_column :company_details, :company_has_contract_with_that_agency, :boolean, default: false
		add_column :company_details, :company_can_cancel_this_agreement, :boolean, default: false
		add_column :company_details, :how_much_the_company_pays_to_cancel_this_agreement, :decimal, precision: 10, scale: 2
		add_column :company_details, :date_of_expiry_of_the_agency_contract, :date
	end
end
