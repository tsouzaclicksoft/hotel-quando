class AddPhoneNumberToUsers < ActiveRecord::Migration
  def up
    change_table :users do | t |
      t.string :phone_number
    end
  end

  def down
    change_table :users do | t |
      t.remove :phone_number
    end
  end
end
