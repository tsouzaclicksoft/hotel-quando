class ChangeDefaultValueAndAddBudgetToUsers < ActiveRecord::Migration
  def up
    change_column :users, :company_user_for_agency, :boolean, default: false, null: false

    add_money :users, :monthly_budget, amount: { null: true, default: nil }
    add_money :users, :budget_per_booking, amount: { null: true, default: nil }

  end

  def down
    change_column :users, :company_user_for_agency, :boolean, default: nil, null: true

    remove_money :users, :monthly_budget
    remove_money :users, :budget_per_booking
  end
end
