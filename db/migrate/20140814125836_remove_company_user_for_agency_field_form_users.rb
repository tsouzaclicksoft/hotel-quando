class RemoveCompanyUserForAgencyFieldFormUsers < ActiveRecord::Migration
  def up
    change_table :users do | t |
      t.remove :company_user_for_agency
    end
  end

  def down
    change_table :users do | t |
      t.boolean :company_user_for_agency, default: false, null: false
    end
  end
end
