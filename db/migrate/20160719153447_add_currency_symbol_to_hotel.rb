class AddCurrencySymbolToHotel < ActiveRecord::Migration
  def change
    add_column :hotels, :currency_symbol, :integer
    add_index :hotels, :currency_symbol

    Hotel.update_all currency_symbol: 1
  end
end
