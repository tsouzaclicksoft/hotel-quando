class CreateSearchLogsUsers < ActiveRecord::Migration
  def change
    create_table :search_logs_users do |t|
      t.string :email
    end

    add_index :search_logs_users, :email
  end
end
