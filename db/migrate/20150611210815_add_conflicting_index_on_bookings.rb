class AddConflictingIndexOnBookings < ActiveRecord::Migration
  def change
    add_index :bookings, [:room_type_id, :is_active, :date_interval]
  end
end
