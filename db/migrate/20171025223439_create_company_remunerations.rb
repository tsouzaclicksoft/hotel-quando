class CreateCompanyRemunerations < ActiveRecord::Migration
  def change
    create_table :company_remunerations do |t|

      t.decimal :remuneration_by_new_company, precision: 10, scale: 2
      t.decimal :remuneration_by_reservation, precision: 10, scale: 2

      t.timestamps
    end
  end
end
