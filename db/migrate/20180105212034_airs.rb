class Airs < ActiveRecord::Migration
	def change
		create_table :airs do |t|

			t.references :travel_request, index: true, foreign_key: true
			t.references :traveler, index: true, foreign_key: true

			t.string :airline
			t.string :operation_airline
			t.string :flight_number
			t.string :origin
			t.string :destiny
			t.datetime :departure_date
			t.datetime :arrival_date
			t.string :flight_duration
			t.integer :number_connections
			t.integer :number_scales
			t.integer :flight_class
			t.datetime :due_date
			t.string :ticket
			t.string :cia_locator
			t.string :seat
			t.string :currency
			t.boolean :is_online
			t.boolean :is_included_hand_baggage
			t.boolean :is_included_baggage_dispatch
			t.integer :number_baggage_dispatch
			t.boolean :is_approved
			t.text :justification_of_disapproval

			t.timestamps
		end
	end
end
