class AddCachePriceFieldsToBookings < ActiveRecord::Migration
  def change
    change_table :bookings do |t|
      t.integer :cached_room_type_initial_capacity
      t.integer :cached_room_type_maximum_capacity
      t.decimal :cached_offer_extra_price_per_person, precision: 6, scale: 2
      t.decimal :cached_offer_price, precision: 6, scale: 2
    end
  end
end
