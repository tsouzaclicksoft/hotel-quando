class PopulateHotelWithoutCountry < ActiveRecord::Migration
  def up
    puts("Quantidade de Hotéis com country nil: #{Hotel.where(country: nil).count}")
    @quantidade_de_hoteis_atualizados = 0

    Hotel.where(country: nil).each_with_index do  |hotel, index|
      hotel.country = hotel.city.state.country.iso_initials
      hotel.save

      @quantidade_de_hoteis_atualizados = index
    end

    puts("Quantidade de Hotéis atualizados: #{@quantidade_de_hoteis_atualizados + 1}")
    puts("Quantidade de Hotéis com country nil: #{Hotel.where(country: nil).count}")
  end
end
