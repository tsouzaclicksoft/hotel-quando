require 'nokogiri'
class UpdateCountriesAndStatesTables < ActiveRecord::Migration
  def up
	country_file = File.read(Rails.root.join("public", "country.xml"))
	state_file = File.read(Rails.root.join("public", "state.xml"))

	country_xml = Nokogiri::Slop(country_file)
	state_xml = Nokogiri::Slop(state_file)

	@countries = country_xml.database.country.collect { |x| [x.children[1].content, x.children[3].content, x.children[5].content] }
	@countries_and_states = state_xml.state.state_province.collect { |x| @z=x.children[5].content; [x.children[1].content, x.children[3].content, @z].concat(@countries.select{|e| e[0] == @z}.first[1..-1]) }

	# removing Brazil states from array
	@countries_and_states.select{|e| e[3] == "Brazil"}.each{|d|  @countries_and_states.delete(d)}

	# creating query to save in Sates table
	@query = []
	@countries_not_found = []
	@countries_and_states.each do |c|
		country = Country.where(name_en: c[3]).first
		if country.blank?
			@countries_not_found.concat([c])
		else
			@query.concat([initials: c[1], name: c[0], country_id: country.id])
		end
	end

	@countries_not_found2 = []
	@countries_not_found.each do |c|
		country = Country.where("name_en like '%#{c[3][0..9]}'").first
		if country.blank?
			@countries_not_found2.concat([c])
		else
			@query.concat([initials: c[1], name: c[0], country_id: country.id])
		end
	end

	@countries_not_found3 = []
	@countries_not_found2.each do |c|
		country = Country.where("name_en like '%#{c[3][0..3]}%'").first
		if country.blank?
			@countries_not_found3.concat([c])
		else
			@query.concat([initials: c[1], name: c[0], country_id: country.id])
		end
	end

	p "Countries found: " + @query.count.to_s
	p "Countries not found 1st try: " + @countries_not_found.count.to_s
	p "Countries not found 2nd try: " + @countries_not_found2.count.to_s
	p "Countries not found 3rd try: " + @countries_not_found3.count.to_s
	p "Total Countries from xml file: " + @countries_and_states.count.to_s

	p "Try create not found countries"
	@ctrs = @countries_not_found3.collect{|c| [c[3],c[4]]}.uniq
	@ctrs.each do |c|
		Country.create(iso_initials: c[1], name_pt_br: c[0], name_en: c[0], name_es: c[0])
	end

	@countries_not_found4 = []
	@countries_not_found3.each do |c|
		country = Country.where(name_en: c[3]).first
		if country.blank?
			@countries_not_found4.concat([c])
		else
			@query.concat([initials: c[1], name: c[0], country_id: country.id])
		end
	end

	p "Countries not found 4th try: " + @countries_not_found4.count.to_s
	p "Inserts that will be done in State table: #{@query.count}"

	State.create(@query)

  end

  def down
  	# nothing to do
  end
end
