class ChangeNumberToString < ActiveRecord::Migration
  def up
  	change_column :company_details, :number, :string
  	change_column :agency_details, :number, :string
  end

  def down
  	change_column :company_details, :number, :integer
  	change_column :agency_details, :number, :integer
  end

end
