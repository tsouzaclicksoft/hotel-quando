class AddLanguageToBusinessRooms < ActiveRecord::Migration
  def change
    add_column :business_rooms, :name_es, :text
    add_column :business_rooms, :description_es, :text
  end
end
