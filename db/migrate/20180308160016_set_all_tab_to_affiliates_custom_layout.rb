class SetAllTabToAffiliatesCustomLayout < ActiveRecord::Migration
  def up
  	count = Affiliate.where(custom_layout: true).update_all(tab_room_accepts: ["0", "1", "2"])
  	puts "#{count} Afiliados alterados"
  end

  def down
  	count = Affiliate.where(custom_layout: true).update_all(tab_room_accepts: [])
  	puts "#{count} Afiliados alterados"
  end
end
