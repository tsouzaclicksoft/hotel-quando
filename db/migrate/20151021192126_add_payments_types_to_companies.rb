class AddPaymentsTypesToCompanies < ActiveRecord::Migration
  def change
  	add_column :companies, :accept_confirmed_and_invoiced, :boolean, default: true
    add_column :companies, :accept_confirmed_without_card, :boolean, default: false
  end
end
