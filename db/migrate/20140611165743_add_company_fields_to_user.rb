class AddCompanyFieldsToUser < ActiveRecord::Migration
  def change

    change_table(:users) do | t |
      t.column  :profile_type, 'numeric(1)', default: 0
      t.string  :cnpj
      t.integer :agency_id
    end

    add_index :users, :profile_type
  end
end
