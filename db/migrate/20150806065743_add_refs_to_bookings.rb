class AddRefsToBookings < ActiveRecord::Migration
  def up
  	change_table :bookings do |t|
  		t.integer :meeting_room_offer_id
  		t.integer :event_room_offer_id
      t.integer :business_room_id
  	end
  	add_index :bookings, :meeting_room_offer_id
  	add_index :bookings, :event_room_offer_id
    add_index :bookings, :business_room_id

    execute %Q{
      ALTER TABLE bookings ADD CONSTRAINT business_room_overlapping_times EXCLUDE USING gist (business_room_id WITH =, date_interval WITH &&) where(is_active = true)
    }
    execute 'ALTER TABLE bookings ALTER COLUMN meeting_room_offer_id TYPE numeric(20);'
    execute 'ALTER TABLE bookings ALTER COLUMN event_room_offer_id TYPE numeric(20);'
  end

  def down
    execute "ALTER TABLE bookings DROP CONSTRAINT business_room_overlapping_times"

    change_table :bookings do |t|
      t.remove :meeting_room_offer_id
  		t.remove :event_room_offer_id
      t.remove :business_room_id
    end
  end
end
