class AddNoticeEmailsToHotels < ActiveRecord::Migration
  def change
    change_table :hotels do |t|
      t.text :notice_emails
    end
  end
end
