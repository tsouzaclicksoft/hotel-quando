class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.string :address
      t.float :latitude
      t.float :longitude
      t.integer :distance
      t.integer :place_type
      t.integer :hotel_id, array: true
      t.integer :hotel_count
      t.integer :city_id
      t.integer :country_id
    end
  end
end
