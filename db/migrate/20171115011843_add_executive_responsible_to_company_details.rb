class AddExecutiveResponsibleToCompanyDetails < ActiveRecord::Migration
  def change
    add_reference :company_details, :executive_responsible, index: true, foreign_key: true
  end
end
