class UpdatePrecisionToPromotionalCodeLogs < ActiveRecord::Migration
  def up
  	change_column(:promotional_code_logs, :discount_cached, :decimal, precision: 10, scale: 2)
  end

  def down
  	change_column(:promotional_code_logs, :discount_cached, :decimal, precision: 6, scale: 2)
  end
end
