class AddParticipantTudoAzulAndEntryDateTudoAzulToUsers < ActiveRecord::Migration
  def change
    change_table :users do | t |
      t.boolean :participates_in_tudo_azul, default: false
      t.datetime :entry_date_tudo_azul
    end
  end
end
