class AddAcceptConfirmedAndInvoicedToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :accept_confirmed_and_invoiced, :boolean, default: false
  end
end
