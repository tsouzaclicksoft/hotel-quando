class RemoveFieldsFromCreditCards < ActiveRecord::Migration
  def up
    change_table :credit_cards do |t|
      t.remove :owner_name
    end
  end

  def down
    change_table :credit_cards do |t|
      t.string :owner_name
    end
  end
end
