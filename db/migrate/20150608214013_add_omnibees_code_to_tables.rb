class AddOmnibeesCodeToTables < ActiveRecord::Migration
  def change
    add_column :hotels, :omnibees_code, :string, index: true
    add_column :room_types, :omnibees_code, :string, index: true
    add_column :bookings, :omnibees_code, :string, index: true
  end
end
