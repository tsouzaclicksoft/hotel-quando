class CreateEasyTaxiVouchers < ActiveRecord::Migration
  def change
    create_table :easy_taxi_vouchers do |t|
      t.string :code, unique: true
      t.datetime :linked_at
      t.date :expiration_date
      t.integer :booking_id
      t.timestamps
    end

    add_index :easy_taxi_vouchers, :code
    add_index :easy_taxi_vouchers, :expiration_date
    add_index :easy_taxi_vouchers, :booking_id
    add_index :easy_taxi_vouchers, [:expiration_date, :booking_id]
  end
end
