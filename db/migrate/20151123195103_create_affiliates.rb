class CreateAffiliates < ActiveRecord::Migration
  def change
    create_table :affiliates do |t|

      t.string   :name
      t.boolean  :is_active, default: true
      t.string   :token
      t.decimal  :comission_in_percentage, precision: 5, scale: 2, default: 0
      t.text     :observations      

      t.string 	 :email,              :null => false, :default => ""
      t.string 	 :encrypted_password, :null => false, :default => ""

      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      t.datetime :remember_created_at

      t.integer  :sign_in_count, :default => 0, :null => false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      t.string   :phone_number
      t.string   :cnpj
      t.string   :responsible_name
      t.string   :country
      t.string   :state
      t.string   :city
      t.string   :street
      t.string   :number
      t.string   :complement
      
      t.timestamps

    end

    add_index :affiliates, :email,                :unique => true
    add_index :affiliates, :token,                :unique => true
    add_index :affiliates, :reset_password_token, :unique => true

  end
end
