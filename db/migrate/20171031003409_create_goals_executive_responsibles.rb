class CreateGoalsExecutiveResponsibles < ActiveRecord::Migration
	def change
		create_table :goals_executive_responsibles do |t|

			t.references :executive_responsible, index: true, foreign_key: true

			t.integer :amount_contacts
			t.integer :amount_meeting
			t.integer :amount_new_companies
			t.integer :amount_companies_actives
			t.integer :amount_reservation

			t.integer :amount_contacts_done, :default => 0
			t.integer :amount_meeting_done, :default => 0
			t.integer :amount_new_companies_done, :default => 0
			t.integer :amount_companies_actives_done, :default => 0
			t.integer :amount_reservation_done, :default => 0

			t.boolean :completed_contacts, :default => false
			t.boolean :completed_meeting, :default => false
			t.boolean :completed_new_companies, :default => false
			t.boolean :completed_companies_actives, :default => false
			t.boolean :completed_reservation, :default => false

			t.boolean :goal_accomplished, :default => false
			t.boolean :goal_expired, :default => false
      		t.date :goal_accomplished_date

      		t.datetime :goal_date_expired

			t.timestamps
		end
	end
end
