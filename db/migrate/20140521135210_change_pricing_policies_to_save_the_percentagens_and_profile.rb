class ChangePricingPoliciesToSaveThePercentagensAndProfile < ActiveRecord::Migration
  def change
    change_table :pricing_policies do |t|
      t.decimal :percentage_for_3h, precision: 6, scale: 2, default: 40
      t.decimal :percentage_for_6h, precision: 6, scale: 2, default: 45
      t.decimal :percentage_for_9h, precision: 6, scale: 2, default: 65
      t.decimal :percentage_for_12h, precision: 6, scale: 2, default: 70
      t.decimal :percentage_for_24h, precision: 6, scale: 2, default: 100
      t.decimal :percentage_for_36h, precision: 6, scale: 2, default: 170
      t.decimal :percentage_for_48h, precision: 6, scale: 2, default: 200
      t.string  :percentage_profile_name, limit: 20, default: 'normal'
    end
  end
end
