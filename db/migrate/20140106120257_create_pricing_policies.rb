# encoding: utf-8
class CreatePricingPolicies < ActiveRecord::Migration
  def change
    create_table :pricing_policies do |t|
      t.string  :name
      t.integer :hotel_id
      t.timestamps
    end

    add_index :pricing_policies, :hotel_id
  end
end
