class AddIndexesToPayments < ActiveRecord::Migration
  def change
    add_index :payments, :user_id
    add_index :payments, :booking_id
  end
end
