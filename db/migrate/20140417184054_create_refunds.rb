class CreateRefunds < ActiveRecord::Migration
  def change
    create_table :refunds do | t |
      t.integer   :status
      t.integer   :user_id
      t.integer   :payment_id
      t.text      :maxipago_response
      t.string    :maxipago_auth_code
      t.string    :maxipago_order_id
      t.string    :maxipago_transaction_id
      t.timestamp :authorized_at
      t.timestamps
    end
  end
end
