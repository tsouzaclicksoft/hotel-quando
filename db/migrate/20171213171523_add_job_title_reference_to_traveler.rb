class AddJobTitleReferenceToTraveler < ActiveRecord::Migration
  def change
  	add_reference :travelers, :job_title
  end
end
