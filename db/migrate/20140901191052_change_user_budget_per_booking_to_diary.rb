class ChangeUserBudgetPerBookingToDiary < ActiveRecord::Migration
  def up
    add_money :users, :budget_for_24_hours_pack
    remove_money :users, :budget_per_booking
  end

  def down
    add_money :users, :budget_per_booking
    remove_money :users, :budget_for_24_hours_pack
  end
end
