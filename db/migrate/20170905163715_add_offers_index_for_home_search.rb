class AddOffersIndexForHomeSearch < ActiveRecord::Migration
  def change
    add_index :offers, [:hotel_id, :pack_in_hours, :checkin_timestamp, :status], name: 'index_hotel_id_pack_in_hours_checkin_timestamp_status'
    add_index :offers, [:room_type_initial_capacity, :room_type_maximum_capacity], name: 'index_offers_for_room_capacity'
  end
end
