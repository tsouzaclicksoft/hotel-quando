class AddMarkupPercentageToCompanies < ActiveRecord::Migration
  def change
  	add_column :companies, :markup_percentage, :decimal, precision: 5, scale: 2, default: 0
  end
end
