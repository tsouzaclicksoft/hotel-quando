class AddDeviseToManagers < ActiveRecord::Migration
  def change
    create_table(:managers) do |t|
      t.string :name
      t.string :locale
      t.string :login
      t.boolean :is_active, :default => true
      ## Database authenticatable
      t.string :email,              :null => false, :default => ""
      t.string :encrypted_password, :null => false, :default => ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, :default => 0, :null => false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      t.timestamps
    end

    add_index :managers, :login,                :unique => true
    add_index :managers, :reset_password_token, :unique => true
  end
end
