class ChangeNumericMaxLimitToBusinessOffer < ActiveRecord::Migration
  def up
  	change_column :meeting_room_offers, :meeting_room_maximum_capacity, 'numeric(4)'
  	change_column :event_room_offers, :event_room_maximum_capacity, 'numeric(4)'
  end
  def down
  	change_column :meeting_room_offers, :meeting_room_maximum_capacity, 'numeric(3)'
  	change_column :event_room_offers, :event_room_maximum_capacity, 'numeric(3)'
  end
end
