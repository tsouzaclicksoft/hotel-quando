class AddTermsOfUseToPossiblePartnerContact < ActiveRecord::Migration
  def change
    add_column :possible_partner_contacts, :terms_of_use, :boolean, default: false
  end
end
