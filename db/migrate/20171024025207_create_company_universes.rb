class CreateCompanyUniverses < ActiveRecord::Migration
  def change
    create_table :company_universes do |t|
      t.string :name
      t.string :social_name
      t.string :cnpj
      t.string :address
      t.integer :qtd_employees
      t.string :setor
      t.integer :qtda_reservations_per_month
      
      t.boolean :is_active, :default => true
      t.boolean :has_travel_demand,:default => false
      t.boolean :was_sent_mail,:default => false
      t.boolean :we_talked_phone,:default => false
      t.boolean :we_already_face_to_face_meeting,:default => false
      t.boolean :company_already_registered,:default => false

      t.date :sent_mail_date
      t.date :talked_phone_date
      t.date :already_face_to_face_meeting_date
      t.datetime :company_registered_date
      
      t.string :name_first_contact
      t.string :name_second_contact
      t.string :name_other_contact

      t.string :email_first_contact
      t.string :email_second_contact
      t.string :email_other_contact

      t.string :number_first_contact
      t.string :number_second_contact
      t.string :number_other_contact

      t.references :city, index: true, foreign_key: true
      t.references :company_region, index: true, foreign_key: true
      t.references :executive_responsible, index: true, foreign_key: true

      t.references :company_detail, index: true, foreign_key: true

      t.string :destiny_first
      t.string :destiny_second
      t.string :destiny_other

      t.timestamps
    end

    add_index :company_universes, :cnpj, :unique => true
    
  end
end
