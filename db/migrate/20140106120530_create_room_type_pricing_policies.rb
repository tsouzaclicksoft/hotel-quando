# encoding: utf-8
class CreateRoomTypePricingPolicies < ActiveRecord::Migration
  def change
    create_table :room_type_pricing_policies do |t|
      t.decimal :pack_price_3h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_6h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_9h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_12h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_24h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_36h, precision: 6, scale: 2, default: 0
      t.decimal :pack_price_48h, precision: 6, scale: 2, default: 0
      t.integer :pricing_policy_id
      t.integer :room_type_id
      t.timestamps
    end

    add_index :room_type_pricing_policies, :pricing_policy_id
    add_index :room_type_pricing_policies, :room_type_id
  end
end
