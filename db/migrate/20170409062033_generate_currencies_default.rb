class GenerateCurrenciesDefault < ActiveRecord::Migration
  def up
    if Currency.count == 0
      Currency.new(name: "Dóllar", country: "US", money_sign: "US$", value: 1.0, code: "USD")
      Currency.new(name: "Real", country: "BR", money_sign: "R$", value: 3.05, code: "BRL")
      Currency.new(name: "Colombiano", country: "CO", money_sign: "COP", value: 2000.00, code: "COP")
    end
  end
end
