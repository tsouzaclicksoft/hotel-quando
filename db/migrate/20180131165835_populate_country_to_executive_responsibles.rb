class PopulateCountryToExecutiveResponsibles < ActiveRecord::Migration
  def up
    puts("Quantidade de Executivos com country nil: #{ExecutiveResponsible.where(country: nil).count}")
    @quantidade_de_executivos_atualizados = 0

    ExecutiveResponsible.where(country: nil).each_with_index do  |executive, index|
      executive.country_id = Country.where(name_pt_br: 'Brasil').first.id
      executive.save

      @quantidade_de_executivos_atualizados = index
    end

    puts("Quantidade de Executivos atualizados: #{@quantidade_de_executivos_atualizados + 1}")
    puts("Quantidade de Executivos com country nil: #{ExecutiveResponsible.where(country: nil).count}")
  end

  def down
  	ExecutiveResponsible.where.not(country: nil).update_all(country_id: nil)
  end

end

