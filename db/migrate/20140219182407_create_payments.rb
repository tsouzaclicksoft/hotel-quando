class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer   :credit_card_type
      t.string    :credit_card_owner
      t.string    :masked_credit_card_number
      t.datetime  :credit_card_expiration_date
      t.string    :credit_card_security_code
      t.integer   :status
      t.integer   :active
      t.integer   :gateway_response
      t.datetime  :register_date
      t.datetime  :cancel_date
      t.string    :token
      t.datetime  :token_validity_date
      t.integer   :user_id
      t.integer   :booking_id
    end
  end
end
