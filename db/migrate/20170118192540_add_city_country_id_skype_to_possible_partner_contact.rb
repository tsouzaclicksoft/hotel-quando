class AddCityCountryIdSkypeToPossiblePartnerContact < ActiveRecord::Migration
  def change
    add_column :possible_partner_contacts, :id_skype, :string
    add_column :possible_partner_contacts, :city, :string
    add_column :possible_partner_contacts, :country, :string
  end
end
