class AddTimezoneToHotelSearchLog < ActiveRecord::Migration
  def change
    add_column :hotel_search_logs, :timezone, :string
  end
end
