class AddDelayedCheckoutFlagToBookings < ActiveRecord::Migration
  def change
    change_table :bookings do |t|
      t.boolean :delayed_checkout_flag, default: false
    end
  end
end
