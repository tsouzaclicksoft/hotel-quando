class AddBookingNameToRoomType < ActiveRecord::Migration
  def change
  	add_column :room_types, :booking_name_pt_br, :string
  	add_column :room_types, :booking_name_en, :string
  end
end
