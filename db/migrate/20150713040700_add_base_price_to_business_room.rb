class AddBasePriceToBusinessRoom < ActiveRecord::Migration
  def change
  	add_column :business_room_pricing_policies, :percentage_for_24h, :decimal, precision: 6, scale: 2, default: 100
  	add_column :business_room_prices, :pack_price_24h, :decimal, precision: 6, scale: 2, default: 0
  	add_column :business_room_prices, :penalty_for_24h_pack_in_percentage, :decimal , precision: 5, scale: 2, default: 100
  end
end
