class AddDateFieldsToPayments < ActiveRecord::Migration
  def change
    change_table :payments do |t|
      t.datetime  :canceled_at
      t.timestamps
      t.text :maxipago_response, default: ''
    end
  end
end
