class CreateConsultants < ActiveRecord::Migration
  def change
    create_table :consultants do |t|
    	t.references 	:agency_detail
    	t.string 			:name
    	
      t.timestamps
    end
  end
end
