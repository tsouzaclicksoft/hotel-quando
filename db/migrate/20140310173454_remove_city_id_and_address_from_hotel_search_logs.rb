class RemoveCityIdAndAddressFromHotelSearchLogs < ActiveRecord::Migration
  def up
  	change_table :hotel_search_logs do | t |
  		t.remove :city_id
  		t.remove :address
    end
  end

  def down
  	change_table :hotel_search_logs do | t |
  	  t.integer :city_id
  	  t.string :address
    end
  end
end
