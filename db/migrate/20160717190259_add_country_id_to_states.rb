class AddCountryIdToStates < ActiveRecord::Migration
  def up
  	change_table :states do |t|
      t.integer :country_id
    end
    add_index :states, :country_id

    brazil_id = Country.where(iso_initials: "BR").first.id
    State.all.each do |state|
    	state.country_id = brazil_id
    	state.save
    end
  end

  def down
    remove_index :states, :country_id  	
  	
  	change_table :states do |t|
      t.remove :country_id
    end
  end
end
