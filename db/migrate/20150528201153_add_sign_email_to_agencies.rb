class AddSignEmailToAgencies < ActiveRecord::Migration
  def change
    change_table :agencies do | t |
      t.string :sign_email
    end
	end
end
