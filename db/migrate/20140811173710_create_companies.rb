class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string  :name
      t.string  :phone_number
      t.string  :cnpj
      t.string  :maxipago_token
      t.string  :responsible_name
      t.string  :email
      t.string  :country
      t.string  :state
      t.string  :city
      t.string  :street
      t.string  :number
      t.string  :complement
      t.integer :agency_id
    end
  end
end
