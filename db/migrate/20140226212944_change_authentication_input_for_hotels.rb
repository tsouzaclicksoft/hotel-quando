class ChangeAuthenticationInputForHotels < ActiveRecord::Migration
  def up
    remove_index :hotels, :email
    add_index :hotels, :login, :unique => true
    Hotel.all.each do |hotel|
      hotel.login = hotel.email
      hotel.save
    end
  end
  def down
    remove_index :hotels, :login
    add_index :hotels, :email, :unique => true
  end
end