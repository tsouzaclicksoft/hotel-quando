class AddFieldsToTravelRequest < ActiveRecord::Migration
  def change
  		add_column :travel_requests, :first_name, :string
		add_column :travel_requests, :last_name, :string
		add_column :travel_requests, :cpf, :string
		add_column :travel_requests, :passaporte, :string
		add_column :travel_requests, :telephone, :string
		add_column :travel_requests, :email, :string
  end
end
