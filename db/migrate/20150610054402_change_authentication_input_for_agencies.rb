class ChangeAuthenticationInputForAgencies < ActiveRecord::Migration
  def up
  	change_table :agencies do | t |
  		t.string :login
  	end
    remove_index :agencies, :email
  	add_index :agencies, :login, :unique => true
    Agency.all.each do |agency|
      agency.login = agency.email
      agency.save
    end
  end
  def down
    change_table :agencies do | t |
  		t.remove :login
  	end
    add_index :agencies, :email, :unique => true
  end
end
