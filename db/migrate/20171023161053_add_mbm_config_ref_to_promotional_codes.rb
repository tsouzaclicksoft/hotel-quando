class AddMbmConfigRefToPromotionalCodes < ActiveRecord::Migration
  def change
  	add_column :promotional_codes, :member_get_member_config_id, :integer
  	add_index :promotional_codes, :member_get_member_config_id
  end
end
