class AddHotelIdToAffiliate < ActiveRecord::Migration
  def change
    add_column :affiliates, :hotel_id, :integer
    add_index :affiliates, :hotel_id
  end
end
