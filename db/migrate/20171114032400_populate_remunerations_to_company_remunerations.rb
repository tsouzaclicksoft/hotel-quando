class PopulateRemunerationsToCompanyRemunerations < ActiveRecord::Migration
	def up

		if not CompanyRemuneration.first.present?
			CompanyRemuneration.create(
				remuneration_by_reservation: 0.0,
				remuneration_by_new_company: 0.0
				)
		end
	end

	def down
		CompanyRemuneration.delete_all
	end
end