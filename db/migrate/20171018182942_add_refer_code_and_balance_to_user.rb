class AddReferCodeAndBalanceToUser < ActiveRecord::Migration
  def change
  	add_column :users, :refer_code, :string, uniq: :true
  	add_column :users, :balance, :decimal, precision: 6, scale: 2, default: 0
  end
end
