class AddAuthorizedAtToPayments < ActiveRecord::Migration
  def change
    change_table :payments do |t|
      t.datetime :authorized_at
    end
  end
end
