class AddFieldsToAirs < ActiveRecord::Migration
  def change
  	add_column :airs, :departure_time, :string
  	add_column :airs, :arrival_time, :string
  	add_column :travel_requests, :contact_traveler, :string
  	
  end
end