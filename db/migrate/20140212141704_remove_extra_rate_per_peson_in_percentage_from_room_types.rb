class RemoveExtraRatePerPesonInPercentageFromRoomTypes < ActiveRecord::Migration
  def change
    change_table :room_types do |t|
      t.remove :extra_rate_in_percentage
    end
  end
end
