# encoding: utf-8
class CreateOffersCreationLog < ActiveRecord::Migration
  def change
    create_table :offers_logs do |t|
      t.text      :form_params
      t.binary    :result, limit: 10.megabytes
      t.integer   :hotel_id
      t.string    :type_of_log
      t.timestamps
    end
    add_index :offers_logs, :hotel_id
  end

end
