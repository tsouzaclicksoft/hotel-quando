class AddCountryAndCityInEmkt < ActiveRecord::Migration
  def change
    add_column :emkts, :country, :string
    add_column :emkts, :city, :string
  end
end
