class AddSystemUserEntry < ActiveRecord::Migration
  def up
	User.create(name: "Generic User for Affiliate WS API", cpf: "000.000.000-00", email: "affiliate_ws_api_system@hotelquando.com", password: "10203040", birth_date: "01/01/2016")
  end
  def down
  	u = User.where(email: "affiliate_ws_api_system@hotelquando.com").first
  	u.destroy
  end
end
