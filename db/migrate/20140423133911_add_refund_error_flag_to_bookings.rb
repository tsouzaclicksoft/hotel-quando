class AddRefundErrorFlagToBookings < ActiveRecord::Migration
  def change
    change_table :bookings do |t|
      t.boolean :refund_error, default: false
    end
  end
end
