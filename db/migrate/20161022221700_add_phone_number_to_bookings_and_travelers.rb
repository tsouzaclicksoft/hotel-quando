class AddPhoneNumberToBookingsAndTravelers < ActiveRecord::Migration
  def change
  	add_column :bookings, :phone_number, :string
  	add_column :travelers, :phone_number, :string
  end
end
