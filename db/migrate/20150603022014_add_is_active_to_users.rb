class AddIsActiveToUsers < ActiveRecord::Migration
  def change
    change_table :users do | t |
	  	t.boolean :is_active, default: true
    end
  end
end
