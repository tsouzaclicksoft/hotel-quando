class StartDateToPromotionalCodes < ActiveRecord::Migration
  def change
  	add_column :promotional_codes, :start_date, :date
  end
end
