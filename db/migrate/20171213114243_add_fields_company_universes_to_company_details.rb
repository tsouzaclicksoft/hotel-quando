class AddFieldsCompanyUniversesToCompanyDetails < ActiveRecord::Migration
	def change
		add_column :company_details, :qtd_employees, :integer, :default => 0
		add_column :company_details, :destiny_first, :string
		add_column :company_details, :destiny_second, :string
		add_column :company_details, :destiny_other, :string
	end
end
