class AddInUserEmktType < ActiveRecord::Migration
  def change
    add_column :users, :emkt_flux, :integer
    add_column :users, :past_emkt_date, :date
    add_column :users, :emkt_flux_type, :integer
  end
end
