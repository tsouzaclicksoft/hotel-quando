class RemoveCurrencyMoneyIdFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :currency_money_id, :integer
  end
end
