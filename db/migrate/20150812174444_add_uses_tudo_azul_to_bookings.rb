class AddUsesTudoAzulToBookings < ActiveRecord::Migration
  def change
    add_column :bookings, :uses_tudo_azul, :boolean, default: false
    add_index :bookings, [:uses_tudo_azul, :tudo_azul_notified_at]
    add_index :bookings, :uses_tudo_azul
  end
end
