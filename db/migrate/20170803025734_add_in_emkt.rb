class AddInEmkt < ActiveRecord::Migration
  def change
    add_column :emkts, :day_of_week_send, :integer
    add_column :emkts, :activate, :integer

    add_column :emkts, :link_header, :string
    add_column :emkts, :link_box_1, :string
    add_column :emkts, :link_box_2, :string
    add_column :emkts, :link_box_3_1, :string
    add_column :emkts, :link_box_3_2, :string
    add_column :emkts, :link_box_3_3, :string
    add_column :emkts, :link_box_3_4, :string
    add_column :emkts, :link_box_3_5, :string
    add_column :emkts, :link_box_3_6, :string
    add_column :emkts, :link_footer, :string
  end
end
