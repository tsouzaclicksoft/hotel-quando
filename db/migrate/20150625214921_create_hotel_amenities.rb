class CreateHotelAmenities < ActiveRecord::Migration
  def change
    create_table :hotel_amenities do |t|
    	t.references :hotel
    	t.boolean :breakfast_included, default: false
    	t.boolean :free_internet, default: false
    	t.text :internet_pt_br
    	t.text :internet_en
    	t.text :park_pt_br
    	t.text :park_en
    	t.boolean :pool, default: false
    	t.boolean :rooftop, default: false
    	t.boolean :garden, default: false
    	t.boolean :terrace, default: false
    	t.boolean :sauna, default: false
    	t.boolean :gym, default: false
    	t.boolean :restaurant, default: false
    	t.boolean :bar, default: false
    	t.boolean :reception_24_hour, default: false
    	t.boolean :luggage_room, default: false
    	t.boolean :laundry, default: false
    	t.boolean :room_service, default: false
    	t.boolean :wake_up_service, default: false
    	t.text :transfer_offered_by_the_hotel_pt_br
    	t.text :transfer_offered_by_the_hotel_en
    	t.text :gratuity_for_children_pt_br
    	t.text :gratuity_for_children_en
    	t.timestamps
    end
  end
end
