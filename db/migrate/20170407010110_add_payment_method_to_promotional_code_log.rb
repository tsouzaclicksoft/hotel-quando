class AddPaymentMethodToPromotionalCodeLog < ActiveRecord::Migration
  def change
    add_column :promotional_code_logs, :currency_code, :string
  end
end
