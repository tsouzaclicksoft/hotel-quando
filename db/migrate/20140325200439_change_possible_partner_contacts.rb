class ChangePossiblePartnerContacts < ActiveRecord::Migration
  def up
    change_table :possible_partner_contacts do |t|
      t.rename  :hotel_name, :name
      t.column  :type, 'numeric(2)'
    end
  end

  def down
    change_table :possible_partner_contacts do |t|
      t.rename  :name, :hotel_name
      t.remove  :type
    end
  end
end
