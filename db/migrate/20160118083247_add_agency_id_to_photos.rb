class AddAgencyIdToPhotos < ActiveRecord::Migration
  def change
  	add_reference :photos, :agency
  end
end
