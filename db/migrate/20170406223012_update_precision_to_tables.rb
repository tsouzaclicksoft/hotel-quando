class UpdatePrecisionToTables < ActiveRecord::Migration
  def up
  	change_column(:affiliates, :comission_value, :decimal, precision: 10, scale: 2, default: 0.0)

  	change_column(:bookings, :cached_offer_extra_price_per_person, :decimal, precision: 10, scale: 2)
  	change_column(:bookings, :cached_offer_price, :decimal, precision: 10, scale: 2)
  	change_column(:bookings, :cached_offer_no_show_value, :decimal, precision: 10, scale: 2)
  	change_column(:bookings, :booking_tax, :decimal, precision: 10, scale: 2, default: 0.0)

  	change_column(:business_room_prices, :pack_price_1h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:business_room_prices, :pack_price_2h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:business_room_prices, :pack_price_3h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:business_room_prices, :pack_price_4h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:business_room_prices, :pack_price_5h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:business_room_prices, :pack_price_6h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:business_room_prices, :pack_price_7h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:business_room_prices, :pack_price_8h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:business_room_prices, :pack_price_24h, :decimal, precision: 10, scale: 2, default: 0.0)

  	change_column(:currencies, :value, :decimal, precision: 10, scale: 2, default: 0.0)

  	change_column(:event_room_offers, :price, :decimal, precision: 10, scale: 2)
  	change_column(:event_room_offers, :no_show_value, :decimal, precision: 10, scale: 2)

  	change_column(:hotels, :comission_value, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:hotels, :fee_on_charge, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:hotels, :financial_costs, :decimal, precision: 10, scale: 2, default: 0.0)

  	change_column(:meeting_room_offers, :price, :decimal, precision: 10, scale: 2)
  	change_column(:meeting_room_offers, :no_show_value, :decimal, precision: 10, scale: 2)

  	change_column(:offers, :price, :decimal, precision: 10, scale: 2)
  	change_column(:offers, :no_show_value, :decimal, precision: 10, scale: 2)
  	change_column(:offers, :extra_price_per_person, :decimal, precision: 10, scale: 2, default: 0.0)

  	change_column(:omnibees_offers, :price, :decimal, precision: 10, scale: 2)

  	change_column(:promotional_codes, :discount, :decimal, precision: 10, scale: 2)

  	change_column(:refunds, :value, :decimal, precision: 10, scale: 2)

  	change_column(:room_type_pricing_policies, :pack_price_3h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :pack_price_6h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :pack_price_9h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :pack_price_12h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :pack_price_24h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :pack_price_36h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :pack_price_48h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_3h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_6h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_9h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_12h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_24h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_36h, :decimal, precision: 10, scale: 2, default: 0.0)
  	change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_48h, :decimal, precision: 10, scale: 2, default: 0.0)
  end
 
  def down
    change_column(:affiliates, :comission_value, :decimal, precision: 5, scale: 2, default: 0.0)

    change_column(:bookings, :cached_offer_extra_price_per_person, :decimal, precision: 6, scale: 2)
    change_column(:bookings, :cached_offer_price, :decimal, precision: 6, scale: 2)
    change_column(:bookings, :cached_offer_no_show_value, :decimal, precision: 6, scale: 2)
    change_column(:bookings, :booking_tax, :decimal, precision: 8, scale: 2, default: 0.0)

    change_column(:business_room_prices, :pack_price_1h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:business_room_prices, :pack_price_2h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:business_room_prices, :pack_price_3h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:business_room_prices, :pack_price_4h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:business_room_prices, :pack_price_5h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:business_room_prices, :pack_price_6h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:business_room_prices, :pack_price_7h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:business_room_prices, :pack_price_8h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:business_room_prices, :pack_price_24h, :decimal, precision: 6, scale: 2, default: 0.0)

    change_column(:currencies, :value, :decimal, precision: 6, scale: 2, default: 0.0)

    change_column(:event_room_offers, :price, :decimal, precision: 6, scale: 2)
    change_column(:event_room_offers, :no_show_value, :decimal, precision: 6, scale: 2)

    change_column(:hotels, :comission_value, :decimal, precision: 5, scale: 2, default: 0.0)
    change_column(:hotels, :fee_on_charge, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:hotels, :financial_costs, :decimal, precision: 5, scale: 2, default: 0.0)

    change_column(:meeting_room_offers, :price, :decimal, precision: 6, scale: 2)
    change_column(:meeting_room_offers, :no_show_value, :decimal, precision: 6, scale: 2)

    change_column(:offers, :price, :decimal, precision: 6, scale: 2)
    change_column(:offers, :no_show_value, :decimal, precision: 6, scale: 2)
    change_column(:offers, :extra_price_per_person, :decimal, precision: 6, scale: 2, default: 0.0)

    change_column(:omnibees_offers, :price, :decimal, precision: 6, scale: 2)

    change_column(:promotional_codes, :discount, :decimal, precision: 6, scale: 2)

    change_column(:refunds, :value, :decimal, precision: 8, scale: 2)

    change_column(:room_type_pricing_policies, :pack_price_3h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :pack_price_6h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :pack_price_9h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :pack_price_12h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :pack_price_24h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :pack_price_36h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :pack_price_48h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_3h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_6h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_9h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_12h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_24h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_36h, :decimal, precision: 6, scale: 2, default: 0.0)
    change_column(:room_type_pricing_policies, :extra_price_per_person_for_pack_price_48h, :decimal, precision: 6, scale: 2, default: 0.0)
  end
end
