class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
    	t.string 		      :name
      t.timestamps
    end

    create_table  :promotional_codes do |t|
      t.string          :code, unique: true
      t.integer         :status, default: 1
      t.integer         :user_type
      t.integer         :campaign_id
      t.integer         :discount_type
      t.decimal         :discount, precision: 6, scale: 2
      t.integer         :usage_limit
      t.date            :expiration_date
      t.timestamps
    end
    add_index           :promotional_codes, :campaign_id
    add_index           :promotional_codes, :status
    add_index           :promotional_codes, :user_type

    create_table :promotional_code_logs do |t|
      t.integer         :booking_id
      t.integer         :promotional_code_id
      t.timestamps
    end
    add_index           :promotional_code_logs, :booking_id
    add_index           :promotional_code_logs, :promotional_code_id


    create_table :hotel_groups do |t|
      t.integer         :hotel_id
      t.integer         :campaign_id
      t.timestamps
    end
    add_index           :hotel_groups, :hotel_id
    add_index           :hotel_groups, :campaign_id

  end
end
