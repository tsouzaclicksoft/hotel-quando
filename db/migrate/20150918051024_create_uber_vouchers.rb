class CreateUberVouchers < ActiveRecord::Migration
  def change
    create_table :uber_vouchers do |t|
			t.string :code, unique: true
      t.datetime :linked_at
      t.date :expiration_date
      t.integer :booking_id
      t.timestamps
    end

    add_index :uber_vouchers, :code
    add_index :uber_vouchers, :expiration_date
    add_index :uber_vouchers, :booking_id
    add_index :uber_vouchers, [:expiration_date, :booking_id]
  end
end
