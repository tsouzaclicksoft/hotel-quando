# encoding: utf-8
class CreateOffers < ActiveRecord::Migration
  def up
    create_table :offers do |t|
      t.column     :checkin_timestamp, 'timestamp', default: 'NOW()', extra: ''
      t.column     :checkout_timestamp, 'timestamp', default: 'NOW()', extra: ''
      t.column     :pack_in_hours, 'numeric(2)'
      t.decimal    :price, precision: 6, scale: 2
      t.decimal    :extra_price_per_person_in_percentage, precision: 5, scale: 2
      t.column     :status, 'numeric(2)'
      t.column     :room_type_initial_capacity, 'numeric(2)'
      t.column     :room_type_maximum_capacity, 'numeric(2)'
      t.decimal    :no_show_value, precision: 6, scale: 2
      t.decimal    :delayed_checkout_value, precision: 6, scale: 2
      t.column     :room_id, 'numeric(5)'
      t.column     :room_type_id, 'numeric(5)'
      t.column     :hotel_id, 'numeric(5)'
      t.integer    :log_id
    end
    execute 'ALTER TABLE offers ALTER COLUMN id TYPE numeric(20);'
    # next is the rule to not thrown an exeption when inserting a composet index that already exists
    execute 'CREATE OR REPLACE RULE ignore_duplicate_inserts_on_offers AS ON INSERT TO offers
   WHERE (EXISTS ( SELECT 1 FROM offers WHERE offers.room_id = new.room_id and offers.checkin_timestamp = new.checkin_timestamp and offers.pack_in_hours = new.pack_in_hours))
   DO INSTEAD NOTHING;'

    add_index :offers, :id
    add_index :offers, :room_id
    add_index :offers, :hotel_id
    add_index :offers, :checkin_timestamp
    add_index :offers, :checkout_timestamp
    add_index :offers, :status
    add_index :offers, [:room_id, :pack_in_hours, :checkin_timestamp], name: 'index_offers_on_room_and_hotel_and_pack_length_and_checkin', unique: true
    add_index :offers, :log_id
  end

  def down
    execute 'DROP TABLE offers;'
  end
end
