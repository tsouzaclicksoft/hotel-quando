class AddAcceptInvoicedByAgencyToHotel < ActiveRecord::Migration
  def change
  	add_column :hotels, :accept_invoiced_by_agency, :boolean, default: false
  end
end
