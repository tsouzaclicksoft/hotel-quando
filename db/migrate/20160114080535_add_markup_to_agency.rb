class AddMarkupToAgency < ActiveRecord::Migration
  def change
  	add_column :companies, :markup, :boolean, default: false
  end
end
