class AddHotelLocale < ActiveRecord::Migration
  def change
    add_column :hotels, :locale, :string
  end
end
