class AddTravelerIdToCreditCard < ActiveRecord::Migration
  def change
    add_column :credit_cards, :traveler_id, :integer
    add_index :credit_cards, :traveler_id
  end
end
