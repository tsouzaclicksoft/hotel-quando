class ChangeAuthenticationInputForCompanies < ActiveRecord::Migration
  def up
  	change_table :companies do | t |
  		t.string :login
  	end
    remove_index :companies, :email
  	add_index :companies, :login, :unique => true
    Company.all.each do |company|
      company.login = company.email
      company.save
    end
  end
  def down
    change_table :companies do | t |
  		t.remove :login
  	end
    add_index :companies, :email, :unique => true
  end
end
