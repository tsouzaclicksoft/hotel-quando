class UpdatePlacesTable < ActiveRecord::Migration
  def change
    add_index(:places, :city_id)
    add_index(:places, :country)
    add_index(:places, :hotel_id)
  end
end
