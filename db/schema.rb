# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180910130913) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "btree_gist"

  create_table "affiliates", force: true do |t|
    t.string   "name"
    t.boolean  "is_active",                                             default: true
    t.string   "token"
    t.decimal  "comission_value",              precision: 10, scale: 2, default: 0.0
    t.text     "observations"
    t.string   "email",                                                 default: "",    null: false
    t.string   "encrypted_password",                                    default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                         default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "phone_number"
    t.string   "cnpj"
    t.string   "responsible_name"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "login"
    t.string   "sign_email"
    t.boolean  "api_iframe_on"
    t.boolean  "api_webservice_on"
    t.string   "api_webservice_test_token"
    t.string   "webservice_token"
    t.string   "layout_name"
    t.integer  "hotel_id"
    t.integer  "hotel_chain_id"
    t.boolean  "only_meeting_room",                                     default: false
    t.string   "header_text_color"
    t.string   "header_color"
    t.string   "search_box_color"
    t.string   "search_box_text_color"
    t.string   "search_box_button_color"
    t.string   "search_box_button_text_color"
    t.string   "footer_color"
    t.string   "footer_text_color"
    t.boolean  "custom_layout",                                         default: false
    t.text     "tab_room_accepts",                                      default: [],                 array: true
  end

  add_index "affiliates", ["email"], name: "index_affiliates_on_email", using: :btree
  add_index "affiliates", ["hotel_id"], name: "index_affiliates_on_hotel_id", using: :btree
  add_index "affiliates", ["reset_password_token"], name: "index_affiliates_on_reset_password_token", unique: true, using: :btree
  add_index "affiliates", ["token"], name: "index_affiliates_on_token", unique: true, using: :btree
  add_index "affiliates", ["webservice_token"], name: "index_affiliates_on_webservice_token", unique: true, using: :btree

  create_table "agencies", force: true do |t|
    t.string   "name"
    t.boolean  "is_active",                                      default: true
    t.decimal  "comission_value",        precision: 5, scale: 2, default: 0.0
    t.text     "observations"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                                          default: "",   null: false
    t.string   "encrypted_password",                             default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                  default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "cnpj"
    t.string   "phone_number"
    t.string   "sign_email"
    t.string   "login"
  end

  add_index "agencies", ["login"], name: "index_agencies_on_login", unique: true, using: :btree
  add_index "agencies", ["reset_password_token"], name: "index_agencies_on_reset_password_token", unique: true, using: :btree

  create_table "agency_details", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "cnpj"
    t.string   "financial_contact"
    t.string   "phone_financial_contact"
    t.string   "sign_email"
    t.string   "sign_phone"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "postal_code"
    t.string   "number"
    t.string   "complement"
  end

  create_table "air_taxes", force: true do |t|
    t.decimal  "fare",              precision: 6, scale: 2, default: 0.0
    t.decimal  "boarding_taxes",    precision: 6, scale: 2, default: 0.0
    t.decimal  "taxe_hq",           precision: 6, scale: 2, default: 0.0
    t.decimal  "taxe_change",       precision: 6, scale: 2, default: 0.0
    t.decimal  "taxe_noshow",       precision: 6, scale: 2, default: 0.0
    t.decimal  "taxe_cancellation", precision: 6, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "air_id"
  end

  add_index "air_taxes", ["air_id"], name: "index_air_taxes_on_air_id", using: :btree

  create_table "airs", force: true do |t|
    t.integer  "travel_request_id"
    t.integer  "traveler_id"
    t.string   "airline"
    t.string   "operation_airline"
    t.string   "flight_number"
    t.string   "origin"
    t.string   "destiny"
    t.date     "departure_date"
    t.date     "arrival_date"
    t.string   "flight_duration"
    t.integer  "number_connections"
    t.integer  "number_scales"
    t.integer  "flight_class"
    t.datetime "due_date"
    t.string   "ticket"
    t.string   "cia_locator"
    t.string   "seat"
    t.string   "currency"
    t.boolean  "is_online"
    t.boolean  "is_included_hand_baggage"
    t.boolean  "is_included_baggage_dispatch"
    t.integer  "number_baggage_dispatch"
    t.boolean  "is_approved"
    t.text     "justification_of_disapproval"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "departure_time"
    t.string   "arrival_time"
    t.integer  "air_taxe_id"
  end

  add_index "airs", ["air_taxe_id"], name: "index_airs_on_air_taxe_id", using: :btree
  add_index "airs", ["travel_request_id"], name: "index_airs_on_travel_request_id", using: :btree
  add_index "airs", ["traveler_id"], name: "index_airs_on_traveler_id", using: :btree

  create_table "audits", force: true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.json     "audited_changes"
    t.integer  "version",         default: 0
    t.string   "comment"
    t.string   "remote_address"
    t.string   "request_uuid"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], name: "associated_index", using: :btree
  add_index "audits", ["auditable_id", "auditable_type"], name: "auditable_index", using: :btree
  add_index "audits", ["created_at"], name: "index_audits_on_created_at", using: :btree
  add_index "audits", ["request_uuid"], name: "index_audits_on_request_uuid", using: :btree
  add_index "audits", ["user_id", "user_type"], name: "user_index", using: :btree

  create_table "booking_pack_contracting_companies", force: true do |t|
    t.integer "booking_pack_id"
    t.string  "name"
    t.string  "cnpj"
    t.string  "responsible_name"
    t.string  "phone_number"
    t.string  "role"
    t.string  "email"
    t.integer "cached_room_type_id"
    t.integer "cached_booking_pack_bank_of_hours"
    t.integer "cached_booking_pack_number_of_pack_12h"
    t.decimal "cached_booking_pack_price",              precision: 8, scale: 2, default: 0.0
  end

  add_index "booking_pack_contracting_companies", ["booking_pack_id"], name: "index_booking_pack_contracting_companies_on_booking_pack_id", using: :btree

  create_table "booking_packs", force: true do |t|
    t.integer  "city_id"
    t.integer  "hotel_id"
    t.integer  "room_type_id"
    t.integer  "bank_of_hours"
    t.integer  "number_of_pack_12h"
    t.boolean  "breakfast_included"
    t.decimal  "price",              precision: 8, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "booking_packs", ["city_id"], name: "index_booking_packs_on_city_id", using: :btree
  add_index "booking_packs", ["hotel_id"], name: "index_booking_packs_on_hotel_id", using: :btree
  add_index "booking_packs", ["room_type_id"], name: "index_booking_packs_on_room_type_id", using: :btree

  create_table "booking_taxes", force: true do |t|
    t.string  "name"
    t.decimal "value",           precision: 8, scale: 2
    t.integer "country_id"
    t.string  "currency_symbol"
  end

  add_index "booking_taxes", ["country_id"], name: "index_booking_taxes_on_country_id", using: :btree

  create_table "bookings", force: true do |t|
    t.integer  "status"
    t.string   "guest_name"
    t.string   "note"
    t.text     "hotel_comments"
    t.integer  "user_id"
    t.decimal  "offer_id",                            precision: 20, scale: 0
    t.integer  "room_id"
    t.tsrange  "date_interval"
    t.integer  "room_type_id"
    t.integer  "pack_in_hours"
    t.decimal  "number_of_people",                    precision: 2,  scale: 0
    t.date     "checkin_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active",                                                    default: true
    t.integer  "hotel_id"
    t.integer  "cached_room_type_initial_capacity"
    t.integer  "cached_room_type_maximum_capacity"
    t.decimal  "cached_offer_extra_price_per_person", precision: 10, scale: 2
    t.decimal  "cached_offer_price",                  precision: 10, scale: 2
    t.decimal  "cached_offer_no_show_value",          precision: 10, scale: 2
    t.boolean  "delayed_checkout_flag",                                        default: false
    t.boolean  "refund_error",                                                 default: false
    t.boolean  "created_by_agency",                                            default: false
    t.string   "guest_email"
    t.decimal  "booking_tax",                         precision: 10, scale: 2, default: 0.0
    t.boolean  "canceled_by_agency"
    t.integer  "company_id"
    t.string   "omnibees_code"
    t.decimal  "omnibees_offer_id",                   precision: 20, scale: 0
    t.text     "omnibees_request_xml"
    t.text     "omnibees_response_xml"
    t.datetime "tudo_azul_notified_at"
    t.decimal  "meeting_room_offer_id",               precision: 20, scale: 0
    t.decimal  "event_room_offer_id",                 precision: 20, scale: 0
    t.integer  "business_room_id"
    t.string   "multiplus_code"
    t.text     "cancellation_request_xml"
    t.text     "cancellation_response_xml"
    t.boolean  "uses_tudo_azul",                                               default: false
    t.integer  "invoice_id"
    t.boolean  "created_by_affiliate",                                         default: false
    t.integer  "affiliate_id"
    t.integer  "recommendation_scale"
    t.integer  "multiplus_points"
    t.boolean  "have_promotional_code",                                        default: false
    t.integer  "traveler_id"
    t.integer  "consultant_id"
    t.boolean  "canceled_by_admin",                                            default: false
    t.string   "phone_number"
    t.string   "currency_code"
    t.datetime "expired_at"
    t.boolean  "using_balance",                                                default: false
    t.decimal  "balance_value_used",                  precision: 6,  scale: 2, default: 0.0
    t.boolean  "is_app_mobile",                                                default: false
    t.integer  "executive_responsible_id"
  end

  add_index "bookings", ["affiliate_id"], name: "index_bookings_on_affiliate_id", using: :btree
  add_index "bookings", ["business_room_id", "date_interval"], name: "business_room_overlapping_times", where: "(is_active = true)", using: :gist
  add_index "bookings", ["business_room_id"], name: "index_bookings_on_business_room_id", using: :btree
  add_index "bookings", ["consultant_id"], name: "index_bookings_on_consultant_id", using: :btree
  add_index "bookings", ["date_interval"], name: "index_bookings_on_date_interval", using: :btree
  add_index "bookings", ["event_room_offer_id"], name: "index_bookings_on_event_room_offer_id", using: :btree
  add_index "bookings", ["executive_responsible_id"], name: "index_bookings_on_executive_responsible_id", using: :btree
  add_index "bookings", ["hotel_id"], name: "index_bookings_on_hotel_id", using: :btree
  add_index "bookings", ["meeting_room_offer_id"], name: "index_bookings_on_meeting_room_offer_id", using: :btree
  add_index "bookings", ["offer_id"], name: "index_bookings_on_offer_id", using: :btree
  add_index "bookings", ["omnibees_offer_id"], name: "index_bookings_on_omnibees_offer_id", using: :btree
  add_index "bookings", ["pack_in_hours"], name: "index_bookings_on_pack_in_hours", using: :btree
  add_index "bookings", ["room_id", "date_interval"], name: "overlapping_times", where: "(is_active = true)", using: :gist
  add_index "bookings", ["room_id"], name: "index_bookings_on_room_id", using: :btree
  add_index "bookings", ["room_type_id", "is_active", "date_interval"], name: "index_bookings_on_room_type_id_and_is_active_and_date_interval", using: :btree
  add_index "bookings", ["room_type_id"], name: "index_bookings_on_room_type_id", using: :btree
  add_index "bookings", ["status"], name: "index_bookings_on_status", using: :btree
  add_index "bookings", ["traveler_id"], name: "index_bookings_on_traveler_id", using: :btree
  add_index "bookings", ["user_id"], name: "index_bookings_on_user_id", using: :btree
  add_index "bookings", ["uses_tudo_azul", "tudo_azul_notified_at"], name: "index_bookings_on_uses_tudo_azul_and_tudo_azul_notified_at", using: :btree
  add_index "bookings", ["uses_tudo_azul"], name: "index_bookings_on_uses_tudo_azul", using: :btree

  create_table "business_room_prices", force: true do |t|
    t.decimal  "pack_price_1h",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_2h",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_3h",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_4h",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_5h",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_6h",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_7h",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_8h",                      precision: 10, scale: 2, default: 0.0
    t.decimal  "penalty_for_1h_pack_in_percentage",  precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_2h_pack_in_percentage",  precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_3h_pack_in_percentage",  precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_4h_pack_in_percentage",  precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_5h_pack_in_percentage",  precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_6h_pack_in_percentage",  precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_7h_pack_in_percentage",  precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_8h_pack_in_percentage",  precision: 5,  scale: 2, default: 100.0
    t.integer  "business_room_pricing_policy_id"
    t.integer  "business_room_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "pack_price_24h",                     precision: 10, scale: 2, default: 0.0
    t.decimal  "penalty_for_24h_pack_in_percentage", precision: 5,  scale: 2, default: 100.0
  end

  add_index "business_room_prices", ["business_room_id"], name: "index_business_room_prices_on_business_room_id", using: :btree
  add_index "business_room_prices", ["business_room_pricing_policy_id"], name: "index_business_room_prices_on_business_room_pricing_policy_id", using: :btree

  create_table "business_room_pricing_policies", force: true do |t|
    t.string   "name"
    t.integer  "hotel_id"
    t.string   "type"
    t.decimal  "percentage_for_1h",                  precision: 6, scale: 2, default: 12.5
    t.decimal  "percentage_for_2h",                  precision: 6, scale: 2, default: 25.0
    t.decimal  "percentage_for_3h",                  precision: 6, scale: 2, default: 37.5
    t.decimal  "percentage_for_4h",                  precision: 6, scale: 2, default: 50.0
    t.decimal  "percentage_for_5h",                  precision: 6, scale: 2, default: 62.5
    t.decimal  "percentage_for_6h",                  precision: 6, scale: 2, default: 75.0
    t.decimal  "percentage_for_7h",                  precision: 6, scale: 2, default: 87.5
    t.decimal  "percentage_for_8h",                  precision: 6, scale: 2, default: 100.0
    t.string   "percentage_profile_name", limit: 20,                         default: "normal"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "percentage_for_24h",                 precision: 6, scale: 2, default: 100.0
  end

  add_index "business_room_pricing_policies", ["hotel_id"], name: "index_business_room_pricing_policies_on_hotel_id", using: :btree

  create_table "business_rooms", force: true do |t|
    t.string   "name_pt_br"
    t.string   "name_en"
    t.text     "description_pt_br"
    t.text     "description_en"
    t.integer  "maximum_capacity"
    t.string   "type"
    t.integer  "hotel_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "name_es"
    t.text     "description_es"
  end

  add_index "business_rooms", ["hotel_id"], name: "index_business_rooms_on_hotel_id", using: :btree

  create_table "campaigns", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: true do |t|
    t.string  "name"
    t.integer "state_id"
    t.integer "place_id", default: [], array: true
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "companies", force: true do |t|
    t.string   "name"
    t.string   "phone_number"
    t.string   "cnpj"
    t.string   "maxipago_token"
    t.string   "responsible_name"
    t.string   "email"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.integer  "agency_id"
    t.string   "encrypted_password",                                    default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                         default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "is_active",                                             default: true
    t.string   "login"
    t.boolean  "accept_confirmed_and_invoiced",                         default: true
    t.boolean  "accept_confirmed_without_card",                         default: false
    t.boolean  "markup",                                                default: false
    t.decimal  "markup_value",                  precision: 5, scale: 2, default: 0.0
  end

  add_index "companies", ["login"], name: "index_companies_on_login", unique: true, using: :btree
  add_index "companies", ["reset_password_token"], name: "index_companies_on_reset_password_token", unique: true, using: :btree

  create_table "company_attendants", force: true do |t|
    t.string   "name"
    t.string   "login"
    t.boolean  "is_active",              default: true
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "company_attendants", ["login"], name: "index_company_attendants_on_login", unique: true, using: :btree
  add_index "company_attendants", ["reset_password_token"], name: "index_company_attendants_on_reset_password_token", unique: true, using: :btree

  create_table "company_details", force: true do |t|
    t.integer  "user_id"
    t.integer  "agency_detail_id"
    t.string   "name"
    t.string   "cnpj"
    t.string   "financial_contact"
    t.string   "phone_financial_contact"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "executive_responsible_id"
    t.boolean  "has_an_agency",                                                               default: false
    t.text     "how_your_company_pays_this_agency"
    t.boolean  "company_has_contract_with_that_agency",                                       default: false
    t.boolean  "company_can_cancel_this_agreement",                                           default: false
    t.decimal  "how_much_the_company_pays_to_cancel_this_agreement", precision: 10, scale: 2
    t.date     "date_of_expiry_of_the_agency_contract"
    t.string   "executive_service"
    t.date     "follow_up_date"
    t.boolean  "already_received_registration_form",                                          default: false
    t.boolean  "already_received_signed_contract",                                            default: false
    t.boolean  "already_aligned_campaigns",                                                   default: false
    t.date     "received_registration_form_date"
    t.date     "received_signed_contract_date"
    t.date     "aligned_campaigns_date"
    t.integer  "qtd_employees",                                                               default: 0
    t.string   "destiny_first"
    t.string   "destiny_second"
    t.string   "destiny_other"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "postal_code"
    t.string   "number"
    t.string   "complement"
  end

  add_index "company_details", ["executive_responsible_id"], name: "index_company_details_on_executive_responsible_id", using: :btree

  create_table "company_regions", force: true do |t|
    t.string   "name"
    t.integer  "city_id"
    t.boolean  "is_active",  default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "company_regions", ["city_id"], name: "index_company_regions_on_city_id", using: :btree

  create_table "company_remunerations", force: true do |t|
    t.decimal  "remuneration_by_new_company", precision: 10, scale: 2
    t.decimal  "remuneration_by_reservation", precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "company_universes", force: true do |t|
    t.string   "name"
    t.string   "social_name"
    t.string   "cnpj"
    t.string   "address"
    t.integer  "qtd_employees"
    t.string   "setor"
    t.integer  "qtda_reservations_per_month"
    t.boolean  "is_active",                                                                   default: true
    t.boolean  "has_travel_demand",                                                           default: false
    t.boolean  "was_sent_mail",                                                               default: false
    t.boolean  "we_talked_phone",                                                             default: false
    t.boolean  "we_already_face_to_face_meeting",                                             default: false
    t.boolean  "company_already_registered",                                                  default: false
    t.date     "sent_mail_date"
    t.date     "talked_phone_date"
    t.date     "already_face_to_face_meeting_date"
    t.datetime "company_registered_date"
    t.string   "name_first_contact"
    t.string   "name_second_contact"
    t.string   "name_other_contact"
    t.string   "email_first_contact"
    t.string   "email_second_contact"
    t.string   "email_other_contact"
    t.string   "number_first_contact"
    t.string   "number_second_contact"
    t.string   "number_other_contact"
    t.integer  "city_id"
    t.integer  "company_region_id"
    t.integer  "executive_responsible_id"
    t.integer  "company_detail_id"
    t.string   "destiny_first"
    t.string   "destiny_second"
    t.string   "destiny_other"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "note"
    t.boolean  "has_an_agency",                                                               default: false
    t.text     "how_your_company_pays_this_agency"
    t.boolean  "company_has_contract_with_that_agency",                                       default: false
    t.boolean  "company_can_cancel_this_agreement",                                           default: false
    t.decimal  "how_much_the_company_pays_to_cancel_this_agreement", precision: 10, scale: 2
    t.date     "date_of_expiry_of_the_agency_contract"
    t.string   "executive_service"
    t.date     "follow_up_date"
  end

  add_index "company_universes", ["city_id"], name: "index_company_universes_on_city_id", using: :btree
  add_index "company_universes", ["company_detail_id"], name: "index_company_universes_on_company_detail_id", using: :btree
  add_index "company_universes", ["company_region_id"], name: "index_company_universes_on_company_region_id", using: :btree
  add_index "company_universes", ["executive_responsible_id"], name: "index_company_universes_on_executive_responsible_id", using: :btree

  create_table "consultants", force: true do |t|
    t.integer  "agency_detail_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cost_centers", force: true do |t|
    t.integer  "company_detail_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "countries", force: true do |t|
    t.string "iso_initials"
    t.string "name_pt_br"
    t.string "name_en"
    t.string "name_es"
  end

  create_table "credit_cards", force: true do |t|
    t.string  "flag_name",        limit: 20
    t.string  "owner_cpf"
    t.string  "owner_passport"
    t.integer "last_four_digits"
    t.integer "expiration_year"
    t.integer "expiration_month", limit: 2
    t.string  "maxipago_token"
    t.integer "user_id"
    t.string  "billing_name"
    t.string  "billing_address1"
    t.string  "billing_city"
    t.string  "billing_state"
    t.string  "billing_zipcode"
    t.string  "billing_country"
    t.string  "billing_phone"
    t.integer "company_id"
    t.integer "traveler_id"
    t.string  "card_number"
    t.integer "cvv"
  end

  add_index "credit_cards", ["traveler_id"], name: "index_credit_cards_on_traveler_id", using: :btree
  add_index "credit_cards", ["user_id"], name: "index_credit_cards_on_user_id", using: :btree

  create_table "currencies", force: true do |t|
    t.string   "name"
    t.string   "country"
    t.string   "money_sign"
    t.decimal  "value",      precision: 10, scale: 2, default: 0.0
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "departments", force: true do |t|
    t.string  "name"
    t.integer "company_id"
  end

  create_table "devices", force: true do |t|
    t.string   "device_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "devices", ["device_id"], name: "index_devices_on_device_id", unique: true, using: :btree

  create_table "easy_taxi_vouchers", force: true do |t|
    t.string   "code"
    t.datetime "linked_at"
    t.date     "expiration_date"
    t.integer  "booking_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "easy_taxi_vouchers", ["booking_id"], name: "index_easy_taxi_vouchers_on_booking_id", using: :btree
  add_index "easy_taxi_vouchers", ["code"], name: "index_easy_taxi_vouchers_on_code", using: :btree
  add_index "easy_taxi_vouchers", ["expiration_date", "booking_id"], name: "index_easy_taxi_vouchers_on_expiration_date_and_booking_id", using: :btree
  add_index "easy_taxi_vouchers", ["expiration_date"], name: "index_easy_taxi_vouchers_on_expiration_date", using: :btree

  create_table "emkts", force: true do |t|
    t.integer  "emkt_type"
    t.string   "name_pt_br"
    t.string   "name_es"
    t.string   "country"
    t.string   "city"
    t.string   "header_file_name"
    t.string   "header_content_type"
    t.integer  "header_file_size"
    t.datetime "header_updated_at"
    t.string   "box_1_file_name"
    t.string   "box_1_content_type"
    t.integer  "box_1_file_size"
    t.datetime "box_1_updated_at"
    t.string   "box_2_file_name"
    t.string   "box_2_content_type"
    t.integer  "box_2_file_size"
    t.datetime "box_2_updated_at"
    t.string   "box_3_1_file_name"
    t.string   "box_3_1_content_type"
    t.integer  "box_3_1_file_size"
    t.datetime "box_3_1_updated_at"
    t.string   "box_3_2_file_name"
    t.string   "box_3_2_content_type"
    t.integer  "box_3_2_file_size"
    t.datetime "box_3_2_updated_at"
    t.string   "box_3_3_file_name"
    t.string   "box_3_3_content_type"
    t.integer  "box_3_3_file_size"
    t.datetime "box_3_3_updated_at"
    t.string   "box_3_4_file_name"
    t.string   "box_3_4_content_type"
    t.integer  "box_3_4_file_size"
    t.datetime "box_3_4_updated_at"
    t.string   "box_3_5_file_name"
    t.string   "box_3_5_content_type"
    t.integer  "box_3_5_file_size"
    t.datetime "box_3_5_updated_at"
    t.string   "box_3_6_file_name"
    t.string   "box_3_6_content_type"
    t.integer  "box_3_6_file_size"
    t.datetime "box_3_6_updated_at"
    t.string   "footer_file_name"
    t.string   "footer_content_type"
    t.integer  "footer_file_size"
    t.datetime "footer_updated_at"
    t.integer  "day_of_week_send"
    t.integer  "activate"
    t.string   "link_header"
    t.string   "link_box_1"
    t.string   "link_box_2"
    t.string   "link_box_3_1"
    t.string   "link_box_3_2"
    t.string   "link_box_3_3"
    t.string   "link_box_3_4"
    t.string   "link_box_3_5"
    t.string   "link_box_3_6"
    t.string   "link_footer"
    t.integer  "hour_send"
    t.integer  "order_send"
  end

  create_table "event_room_offers", force: true do |t|
    t.datetime "checkin_timestamp",                                      default: '2018-03-21 21:30:29'
    t.datetime "checkout_timestamp",                                     default: '2018-03-21 21:30:29'
    t.decimal  "pack_in_hours",                 precision: 2,  scale: 0
    t.decimal  "price",                         precision: 10, scale: 2
    t.decimal  "status",                        precision: 2,  scale: 0
    t.decimal  "event_room_maximum_capacity",   precision: 4,  scale: 0
    t.decimal  "no_show_value",                 precision: 10, scale: 2
    t.decimal  "event_room_id",                 precision: 5,  scale: 0
    t.decimal  "hotel_id",                      precision: 5,  scale: 0
    t.integer  "log_id"
    t.decimal  "checkin_timestamp_day_of_week", precision: 1,  scale: 0
  end

  add_index "event_room_offers", ["checkin_timestamp"], name: "index_event_room_offers_on_checkin_timestamp", using: :btree
  add_index "event_room_offers", ["checkin_timestamp_day_of_week"], name: "index_event_room_offers_on_checkin_timestamp_day_of_week", using: :btree
  add_index "event_room_offers", ["checkout_timestamp"], name: "index_event_room_offers_on_checkout_timestamp", using: :btree
  add_index "event_room_offers", ["event_room_id", "pack_in_hours", "checkin_timestamp"], name: "index_e_r_offers_on_room_and_hotel_and_pack_length_and_checkin", unique: true, using: :btree
  add_index "event_room_offers", ["event_room_id"], name: "index_event_room_offers_on_event_room_id", using: :btree
  add_index "event_room_offers", ["hotel_id", "status", "pack_in_hours"], name: "index_e_r_offers_on_hotel_id_and_status_and_pack_in_hours", using: :btree
  add_index "event_room_offers", ["hotel_id"], name: "index_event_room_offers_on_hotel_id", using: :btree
  add_index "event_room_offers", ["id"], name: "index_event_room_offers_on_id", using: :btree
  add_index "event_room_offers", ["log_id"], name: "index_event_room_offers_on_log_id", using: :btree
  add_index "event_room_offers", ["pack_in_hours", "checkin_timestamp"], name: "index_e_r_offers_on_pack_in_hours_and_checkin", using: :btree
  add_index "event_room_offers", ["status"], name: "index_event_room_offers_on_status", using: :btree

  create_table "executive_responsibles", force: true do |t|
    t.string   "name"
    t.string   "login"
    t.integer  "company_universe_id"
    t.integer  "goals_executive_responsibles_id"
    t.integer  "amount_goal_contacts"
    t.integer  "amount_goal_meeting"
    t.integer  "amount_goal_new_companies"
    t.integer  "amount_goal_companies_actives"
    t.integer  "amount_goal_reservation"
    t.boolean  "is_active",                       default: true
    t.string   "email",                           default: "",   null: false
    t.string   "encrypted_password",              default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                   default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "country_id"
  end

  add_index "executive_responsibles", ["company_universe_id"], name: "index_executive_responsibles_on_company_universe_id", using: :btree
  add_index "executive_responsibles", ["goals_executive_responsibles_id"], name: "index_executive_responsibles_on_goals_executive_responsibles_id", using: :btree
  add_index "executive_responsibles", ["login"], name: "index_executive_responsibles_on_login", unique: true, using: :btree
  add_index "executive_responsibles", ["reset_password_token"], name: "index_executive_responsibles_on_reset_password_token", unique: true, using: :btree

  create_table "goals_executive_responsibles", force: true do |t|
    t.integer  "executive_responsible_id"
    t.integer  "amount_contacts"
    t.integer  "amount_meeting"
    t.integer  "amount_new_companies"
    t.integer  "amount_companies_actives"
    t.integer  "amount_reservation"
    t.integer  "amount_contacts_done",          default: 0
    t.integer  "amount_meeting_done",           default: 0
    t.integer  "amount_new_companies_done",     default: 0
    t.integer  "amount_companies_actives_done", default: 0
    t.integer  "amount_reservation_done",       default: 0
    t.boolean  "completed_contacts",            default: false
    t.boolean  "completed_meeting",             default: false
    t.boolean  "completed_new_companies",       default: false
    t.boolean  "completed_companies_actives",   default: false
    t.boolean  "completed_reservation",         default: false
    t.boolean  "goal_accomplished",             default: false
    t.boolean  "goal_expired",                  default: false
    t.date     "goal_accomplished_date"
    t.datetime "goal_date_expired"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "goals_executive_responsibles", ["executive_responsible_id"], name: "index_goals_executive_responsibles_on_executive_responsible_id", using: :btree

  create_table "hotel_amenities", force: true do |t|
    t.integer  "hotel_id"
    t.boolean  "breakfast_included",                  default: false
    t.boolean  "free_internet",                       default: false
    t.text     "internet_pt_br"
    t.text     "internet_en"
    t.text     "park_pt_br"
    t.text     "park_en"
    t.boolean  "pool",                                default: false
    t.boolean  "rooftop",                             default: false
    t.boolean  "garden",                              default: false
    t.boolean  "terrace",                             default: false
    t.boolean  "sauna",                               default: false
    t.boolean  "gym",                                 default: false
    t.boolean  "restaurant",                          default: false
    t.boolean  "bar",                                 default: false
    t.boolean  "reception_24_hour",                   default: false
    t.boolean  "luggage_room",                        default: false
    t.boolean  "laundry",                             default: false
    t.boolean  "room_service",                        default: false
    t.boolean  "wake_up_service",                     default: false
    t.text     "transfer_offered_by_the_hotel_pt_br"
    t.text     "transfer_offered_by_the_hotel_en"
    t.text     "gratuity_for_children_pt_br"
    t.text     "gratuity_for_children_en"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "breakfast_included_for_pack_3h",      default: false
    t.boolean  "breakfast_included_for_pack_6h",      default: false
    t.boolean  "breakfast_included_for_pack_9h",      default: false
    t.boolean  "breakfast_included_for_pack_12h",     default: false
    t.text     "internet_es"
    t.text     "park_es"
    t.text     "transfer_offered_by_the_hotel_es"
    t.text     "gratuity_for_children_es"
  end

  create_table "hotel_chains", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "affiliate_id"
  end

  create_table "hotel_groups", force: true do |t|
    t.integer  "hotel_id"
    t.integer  "campaign_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hotel_groups", ["campaign_id"], name: "index_hotel_groups_on_campaign_id", using: :btree
  add_index "hotel_groups", ["hotel_id"], name: "index_hotel_groups_on_hotel_id", using: :btree

  create_table "hotel_search_logs", force: true do |t|
    t.string   "checkin_date"
    t.decimal  "checkin_hour",             precision: 2, scale: 0
    t.decimal  "number_of_people",         precision: 3, scale: 0
    t.decimal  "length_of_pack",           precision: 2, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "search_logs_user_id"
    t.string   "address"
    t.string   "city_name"
    t.string   "state_name"
    t.text     "exact_match_hotels_ids"
    t.text     "similar_match_hotels_ids"
    t.integer  "room_category"
    t.string   "timezone"
  end

  add_index "hotel_search_logs", ["city_name"], name: "index_hotel_search_logs_on_city_name", using: :btree
  add_index "hotel_search_logs", ["search_logs_user_id"], name: "index_hotel_search_logs_on_search_logs_user_id", using: :btree
  add_index "hotel_search_logs", ["state_name"], name: "index_hotel_search_logs_on_state_name", using: :btree

  create_table "hotels", force: true do |t|
    t.string   "name"
    t.string   "category"
    t.string   "contact_name"
    t.string   "contact_phone"
    t.string   "cnpj"
    t.integer  "minimum_hours_of_notice"
    t.text     "description_pt_br"
    t.text     "description_en"
    t.text     "payment_method_pt_br"
    t.text     "payment_method_en"
    t.string   "state"
    t.integer  "city_id"
    t.string   "cep"
    t.string   "street"
    t.string   "number"
    t.string   "complement"
    t.string   "login"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                                                          default: "",    null: false
    t.string   "encrypted_password",                                             default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                                  default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.float    "latitude"
    t.float    "longitude"
    t.decimal  "comission_value",                       precision: 10, scale: 2, default: 0.0
    t.decimal  "iss_in_percentage",                     precision: 4,  scale: 1, default: 0.0
    t.boolean  "accept_online_payment",                                          default: false
    t.text     "notice_emails"
    t.integer  "active_offers_count",         limit: 8
    t.string   "omnibees_code"
    t.text     "omnibees_config"
    t.integer  "active_meeting_offers_count", limit: 8
    t.integer  "active_event_offers_count",   limit: 8
    t.decimal  "fee_on_charge",                         precision: 10, scale: 2, default: 0.0,   null: false
    t.decimal  "financial_costs",                       precision: 10, scale: 2, default: 0.0,   null: false
    t.integer  "currency_symbol"
    t.text     "description_es"
    t.text     "payment_method_es"
    t.string   "country"
    t.string   "timezone"
    t.decimal  "service_tax",                           precision: 4,  scale: 1, default: 0.0
    t.string   "locale"
    t.integer  "hotel_chain_id"
    t.boolean  "booking_on_request",                                             default: false
    t.text     "notice_emails_meeting_room"
    t.boolean  "accept_invoiced_payment",                                        default: false
    t.boolean  "accept_invoiced_by_agency",                                      default: false
  end

  add_index "hotels", ["city_id"], name: "index_hotels_on_city_id", using: :btree
  add_index "hotels", ["currency_symbol"], name: "index_hotels_on_currency_symbol", using: :btree
  add_index "hotels", ["hotel_chain_id"], name: "index_hotels_on_hotel_chain_id", using: :btree
  add_index "hotels", ["latitude", "longitude"], name: "index_hotels_on_latitude_and_longitude", using: :btree
  add_index "hotels", ["latitude"], name: "index_hotels_on_latitude", using: :btree
  add_index "hotels", ["login"], name: "index_hotels_on_login", unique: true, using: :btree
  add_index "hotels", ["longitude"], name: "index_hotels_on_longitude", using: :btree
  add_index "hotels", ["reset_password_token"], name: "index_hotels_on_reset_password_token", unique: true, using: :btree
  add_index "hotels", ["state"], name: "index_hotels_on_state", using: :btree

  create_table "hq_consultants", force: true do |t|
    t.string   "name"
    t.string   "login"
    t.boolean  "is_active",              default: true
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "hq_consultants", ["login"], name: "index_hq_consultants_on_login", unique: true, using: :btree
  add_index "hq_consultants", ["reset_password_token"], name: "index_hq_consultants_on_reset_password_token", unique: true, using: :btree

  create_table "invoices", force: true do |t|
    t.string  "company_name"
    t.string  "cnpj"
    t.string  "street"
    t.string  "number"
    t.string  "complement"
    t.string  "district"
    t.string  "city"
    t.string  "state"
    t.string  "country"
    t.string  "cep"
    t.integer "company_id"
  end

  add_index "invoices", ["company_id"], name: "index_invoices_on_company_id", using: :btree

  create_table "job_titles", force: true do |t|
    t.integer  "company_detail_id"
    t.string   "name"
    t.string   "money_sign"
    t.decimal  "budget_value",      precision: 20, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "managers", force: true do |t|
    t.string   "name"
    t.string   "locale"
    t.string   "login"
    t.boolean  "is_active",              default: true
    t.string   "email",                  default: "",   null: false
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role"
    t.string   "country"
  end

  add_index "managers", ["login"], name: "index_managers_on_login", unique: true, using: :btree
  add_index "managers", ["reset_password_token"], name: "index_managers_on_reset_password_token", unique: true, using: :btree

  create_table "meeting_room_offers", force: true do |t|
    t.datetime "checkin_timestamp",                                      default: '2018-03-21 21:30:28'
    t.datetime "checkout_timestamp",                                     default: '2018-03-21 21:30:28'
    t.decimal  "pack_in_hours",                 precision: 2,  scale: 0
    t.decimal  "price",                         precision: 10, scale: 2
    t.decimal  "status",                        precision: 2,  scale: 0
    t.decimal  "meeting_room_maximum_capacity", precision: 4,  scale: 0
    t.decimal  "no_show_value",                 precision: 10, scale: 2
    t.decimal  "meeting_room_id",               precision: 5,  scale: 0
    t.decimal  "hotel_id",                      precision: 5,  scale: 0
    t.integer  "log_id"
    t.decimal  "checkin_timestamp_day_of_week", precision: 1,  scale: 0
  end

  add_index "meeting_room_offers", ["checkin_timestamp"], name: "index_meeting_room_offers_on_checkin_timestamp", using: :btree
  add_index "meeting_room_offers", ["checkin_timestamp_day_of_week"], name: "index_meeting_room_offers_on_checkin_timestamp_day_of_week", using: :btree
  add_index "meeting_room_offers", ["checkout_timestamp"], name: "index_meeting_room_offers_on_checkout_timestamp", using: :btree
  add_index "meeting_room_offers", ["hotel_id", "status", "pack_in_hours"], name: "index_m_r_offers_on_hotel_id_and_status_and_pack_in_hours", using: :btree
  add_index "meeting_room_offers", ["hotel_id"], name: "index_meeting_room_offers_on_hotel_id", using: :btree
  add_index "meeting_room_offers", ["id"], name: "index_meeting_room_offers_on_id", using: :btree
  add_index "meeting_room_offers", ["log_id"], name: "index_meeting_room_offers_on_log_id", using: :btree
  add_index "meeting_room_offers", ["meeting_room_id", "pack_in_hours", "checkin_timestamp"], name: "index_m_r_offers_on_room_and_hotel_and_pack_length_and_checkin", unique: true, using: :btree
  add_index "meeting_room_offers", ["meeting_room_id"], name: "index_meeting_room_offers_on_meeting_room_id", using: :btree
  add_index "meeting_room_offers", ["pack_in_hours", "checkin_timestamp"], name: "index_m_r_offers_on_pack_in_hours_and_checkin", using: :btree
  add_index "meeting_room_offers", ["status"], name: "index_meeting_room_offers_on_status", using: :btree

  create_table "member_get_member_configs", force: true do |t|
    t.integer  "campaign_id"
    t.integer  "discount_type_to_referred"
    t.integer  "discount_type_to_referred_first_bookings"
    t.decimal  "discount_to_referred",                     precision: 6, scale: 2
    t.decimal  "discount_to_referred_first_bookings",      precision: 6, scale: 2
    t.decimal  "indicators_credit",                        precision: 6, scale: 2
    t.decimal  "indicators_credit_first_bookings",         precision: 6, scale: 2
    t.integer  "expiration_date_in_months"
    t.integer  "number_of_first_bookings"
    t.string   "currency_code_discount"
    t.string   "currency_code_credit"
    t.string   "avaiable_for_packs"
    t.string   "avaiable_for_days_of_the_week"
    t.boolean  "is_active"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "newsletters", force: true do |t|
    t.string   "email",            default: "", null: false
    t.string   "city"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "country"
    t.integer  "emkt_flux"
    t.string   "emkt_day_of_week"
    t.integer  "emkt_flux_type"
  end

  add_index "newsletters", ["email"], name: "index_newsletters_on_email", using: :btree

  create_table "offers", force: true do |t|
    t.datetime "checkin_timestamp",                                      default: '2018-03-21 21:30:07'
    t.datetime "checkout_timestamp",                                     default: '2018-03-21 21:30:07'
    t.decimal  "pack_in_hours",                 precision: 2,  scale: 0
    t.decimal  "price",                         precision: 10, scale: 2
    t.decimal  "status",                        precision: 2,  scale: 0
    t.decimal  "room_type_initial_capacity",    precision: 2,  scale: 0
    t.decimal  "room_type_maximum_capacity",    precision: 2,  scale: 0
    t.decimal  "no_show_value",                 precision: 10, scale: 2
    t.decimal  "room_id",                       precision: 5,  scale: 0
    t.decimal  "room_type_id",                  precision: 5,  scale: 0
    t.decimal  "hotel_id",                      precision: 5,  scale: 0
    t.integer  "log_id"
    t.decimal  "checkin_timestamp_day_of_week", precision: 1,  scale: 0
    t.decimal  "extra_price_per_person",        precision: 10, scale: 2, default: 0.0
  end

  add_index "offers", ["checkin_timestamp"], name: "index_offers_on_checkin_timestamp", using: :btree
  add_index "offers", ["checkin_timestamp_day_of_week"], name: "index_offers_on_checkin_timestamp_day_of_week", using: :btree
  add_index "offers", ["checkout_timestamp"], name: "index_offers_on_checkout_timestamp", using: :btree
  add_index "offers", ["hotel_id", "pack_in_hours", "checkin_timestamp", "status"], name: "index_hotel_id_pack_in_hours_checkin_timestamp_status", using: :btree
  add_index "offers", ["hotel_id", "status", "pack_in_hours"], name: "index_offers_on_hotel_id_and_status_and_pack_in_hours", using: :btree
  add_index "offers", ["hotel_id"], name: "index_offers_on_hotel_id", using: :btree
  add_index "offers", ["id"], name: "index_offers_on_id", using: :btree
  add_index "offers", ["log_id"], name: "index_offers_on_log_id", using: :btree
  add_index "offers", ["pack_in_hours", "checkin_timestamp"], name: "index_offers_on_pack_in_hours_and_checkin_timestamp", using: :btree
  add_index "offers", ["room_id", "pack_in_hours", "checkin_timestamp"], name: "index_offers_on_room_and_hotel_and_pack_length_and_checkin", unique: true, using: :btree
  add_index "offers", ["room_id"], name: "index_offers_on_room_id", using: :btree
  add_index "offers", ["room_type_id", "pack_in_hours", "checkin_timestamp"], name: "index_offers_on_room_type_and_pack_length_and_checkin", using: :btree
  add_index "offers", ["room_type_id", "price"], name: "index_offers_on_room_type_id_and_price", using: :btree
  add_index "offers", ["room_type_id"], name: "index_offers_on_room_type_id", using: :btree
  add_index "offers", ["room_type_initial_capacity", "room_type_maximum_capacity"], name: "index_offers_for_room_capacity", using: :btree
  add_index "offers", ["status"], name: "index_offers_on_status", using: :btree

  create_table "offers_creator_logs", force: true do |t|
    t.text     "form_params"
    t.binary   "result"
    t.integer  "hotel_id"
    t.string   "type_of_log"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "logged_room_types_and_prices_used"
    t.boolean  "reprocessed",                       default: false
    t.string   "type"
  end

  add_index "offers_creator_logs", ["hotel_id"], name: "index_offers_creator_logs_on_hotel_id", using: :btree

  create_table "omnibees_offers", force: true do |t|
    t.datetime "checkin_timestamp"
    t.datetime "checkout_timestamp"
    t.decimal  "pack_in_hours",            precision: 2,  scale: 0
    t.decimal  "price",                    precision: 10, scale: 2
    t.decimal  "room_type_id",             precision: 5,  scale: 0
    t.decimal  "hotel_id",                 precision: 5,  scale: 0
    t.decimal  "number_of_guests",         precision: 2,  scale: 0
    t.text     "hotel_availabilities_xml"
    t.text     "room_rate_xml"
    t.text     "room_type_xml"
    t.text     "rate_plan_xml"
  end

  add_index "omnibees_offers", ["checkin_timestamp"], name: "index_omnibees_offers_on_checkin_timestamp", using: :btree
  add_index "omnibees_offers", ["checkout_timestamp"], name: "index_omnibees_offers_on_checkout_timestamp", using: :btree
  add_index "omnibees_offers", ["hotel_id"], name: "index_omnibees_offers_on_hotel_id", using: :btree
  add_index "omnibees_offers", ["id"], name: "index_omnibees_offers_on_id", using: :btree
  add_index "omnibees_offers", ["room_type_id"], name: "index_omnibees_offers_on_room_type_id", using: :btree

  create_table "payments", force: true do |t|
    t.integer  "status"
    t.integer  "user_id"
    t.integer  "booking_id"
    t.integer  "credit_card_id"
    t.datetime "canceled_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "maxipago_response",       default: ""
    t.string   "maxipago_auth_code"
    t.string   "maxipago_order_id"
    t.datetime "authorized_at"
    t.string   "maxipago_transaction_id"
    t.integer  "traveler_id"
    t.string   "paypal_payment_id"
    t.string   "paypal_transaction_id"
    t.string   "paypal_refund_id"
    t.string   "epayco_ref"
    t.integer  "affiliate_id"
    t.boolean  "is_app_mobile",           default: false
    t.string   "payu_order_id"
    t.string   "payu_transaction_id"
  end

  add_index "payments", ["affiliate_id"], name: "index_payments_on_affiliate_id", using: :btree
  add_index "payments", ["booking_id"], name: "index_payments_on_booking_id", using: :btree
  add_index "payments", ["traveler_id"], name: "index_payments_on_traveler_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "photos", force: true do |t|
    t.string   "title_en"
    t.string   "title_pt_br"
    t.string   "direct_image_url"
    t.string   "processing_status", default: "waiting_for_direct_url_processing"
    t.integer  "hotel_id"
    t.integer  "room_type_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_main",           default: false
    t.integer  "business_room_id"
    t.integer  "agency_id"
    t.string   "title_es"
    t.integer  "agency_detail_id"
    t.integer  "place_id"
    t.integer  "affiliate_id"
  end

  create_table "places", force: true do |t|
    t.string   "name_pt_br"
    t.string   "address"
    t.string   "latitude"
    t.string   "longitude"
    t.integer  "distance"
    t.integer  "place_type"
    t.integer  "hotel_id",    array: true
    t.integer  "hotel_count"
    t.integer  "city_id"
    t.string   "country"
    t.boolean  "active"
    t.string   "street"
    t.integer  "number"
    t.string   "complement"
    t.string   "zipcode"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name_en"
    t.string   "name_es"
  end

  add_index "places", ["city_id"], name: "index_places_on_city_id", using: :btree
  add_index "places", ["country"], name: "index_places_on_country", using: :btree
  add_index "places", ["hotel_id"], name: "index_places_on_hotel_id", using: :btree

  create_table "possible_partner_contacts", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email_for_contact"
    t.string   "locale"
    t.text     "message"
    t.boolean  "admin_visualized",                          default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contact_name"
    t.string   "contact_role"
    t.decimal  "type_of_contact",   precision: 2, scale: 0
    t.string   "id_skype"
    t.string   "city"
    t.string   "country"
    t.boolean  "terms_of_use",                              default: false
  end

  create_table "pricing_policies", force: true do |t|
    t.string   "name"
    t.integer  "hotel_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "percentage_for_3h",                  precision: 6, scale: 2, default: 38.0
    t.decimal  "percentage_for_6h",                  precision: 6, scale: 2, default: 43.0
    t.decimal  "percentage_for_9h",                  precision: 6, scale: 2, default: 60.0
    t.decimal  "percentage_for_12h",                 precision: 6, scale: 2, default: 65.0
    t.decimal  "percentage_for_24h",                 precision: 6, scale: 2, default: 100.0
    t.decimal  "percentage_for_36h",                 precision: 6, scale: 2, default: 170.0
    t.decimal  "percentage_for_48h",                 precision: 6, scale: 2, default: 200.0
    t.string   "percentage_profile_name", limit: 20,                         default: "normal"
  end

  add_index "pricing_policies", ["hotel_id"], name: "index_pricing_policies_on_hotel_id", using: :btree

  create_table "promotional_code_logs", force: true do |t|
    t.integer  "booking_id"
    t.integer  "promotional_code_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "currency_code"
    t.decimal  "discount_cached",      precision: 10, scale: 2, default: 0.0
    t.integer  "discount_type_cached"
  end

  add_index "promotional_code_logs", ["booking_id"], name: "index_promotional_code_logs_on_booking_id", using: :btree
  add_index "promotional_code_logs", ["promotional_code_id"], name: "index_promotional_code_logs_on_promotional_code_id", using: :btree

  create_table "promotional_codes", force: true do |t|
    t.string   "code"
    t.integer  "status",                                                 default: 1
    t.integer  "user_type"
    t.integer  "campaign_id"
    t.integer  "discount_type"
    t.decimal  "discount",                      precision: 10, scale: 2
    t.integer  "usage_limit"
    t.date     "expiration_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avaiable_for_packs"
    t.string   "avaiable_for_days_of_the_week"
    t.string   "currency_code"
    t.integer  "member_get_member_config_id"
    t.date     "start_date"
    t.integer  "discount_in"
    t.integer  "checkin_delay",                                          default: 3
    t.string   "notice_email"
  end

  add_index "promotional_codes", ["campaign_id"], name: "index_promotional_codes_on_campaign_id", using: :btree
  add_index "promotional_codes", ["member_get_member_config_id"], name: "index_promotional_codes_on_member_get_member_config_id", using: :btree
  add_index "promotional_codes", ["status"], name: "index_promotional_codes_on_status", using: :btree
  add_index "promotional_codes", ["user_type"], name: "index_promotional_codes_on_user_type", using: :btree

  create_table "referred_friend_logs", force: true do |t|
    t.integer  "user_id"
    t.integer  "promotional_code_id"
    t.string   "friend_name"
    t.string   "friend_email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "refunds", force: true do |t|
    t.integer  "status"
    t.integer  "user_id"
    t.integer  "payment_id"
    t.text     "maxipago_response"
    t.string   "maxipago_auth_code"
    t.string   "maxipago_order_id"
    t.string   "maxipago_transaction_id"
    t.datetime "authorized_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "value",                   precision: 10, scale: 2
    t.integer  "traveler_id"
  end

  add_index "refunds", ["traveler_id"], name: "index_refunds_on_traveler_id", using: :btree

  create_table "room_type_pricing_policies", force: true do |t|
    t.decimal  "pack_price_3h",                             precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_6h",                             precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_9h",                             precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_12h",                            precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_24h",                            precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_36h",                            precision: 10, scale: 2, default: 0.0
    t.decimal  "pack_price_48h",                            precision: 10, scale: 2, default: 0.0
    t.integer  "pricing_policy_id"
    t.integer  "room_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "extra_price_per_person_for_pack_price_3h",  precision: 10, scale: 2, default: 0.0
    t.decimal  "extra_price_per_person_for_pack_price_6h",  precision: 10, scale: 2, default: 0.0
    t.decimal  "extra_price_per_person_for_pack_price_9h",  precision: 10, scale: 2, default: 0.0
    t.decimal  "extra_price_per_person_for_pack_price_12h", precision: 10, scale: 2, default: 0.0
    t.decimal  "extra_price_per_person_for_pack_price_24h", precision: 10, scale: 2, default: 0.0
    t.decimal  "extra_price_per_person_for_pack_price_36h", precision: 10, scale: 2, default: 0.0
    t.decimal  "extra_price_per_person_for_pack_price_48h", precision: 10, scale: 2, default: 0.0
    t.decimal  "penalty_for_3h_pack_in_percentage",         precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_6h_pack_in_percentage",         precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_9h_pack_in_percentage",         precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_12h_pack_in_percentage",        precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_24h_pack_in_percentage",        precision: 5,  scale: 2, default: 100.0
    t.decimal  "penalty_for_36h_pack_in_percentage",        precision: 5,  scale: 2, default: 66.7
    t.decimal  "penalty_for_48h_pack_in_percentage",        precision: 5,  scale: 2, default: 50.0
  end

  add_index "room_type_pricing_policies", ["pricing_policy_id"], name: "index_room_type_pricing_policies_on_pricing_policy_id", using: :btree
  add_index "room_type_pricing_policies", ["room_type_id"], name: "index_room_type_pricing_policies_on_room_type_id", using: :btree

  create_table "room_types", force: true do |t|
    t.string   "name_pt_br"
    t.string   "name_en"
    t.text     "description_pt_br"
    t.text     "description_en"
    t.integer  "initial_capacity"
    t.integer  "maximum_capacity"
    t.integer  "hotel_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "single_bed_quantity", precision: 2, scale: 0, default: 0
    t.decimal  "double_bed_quantity", precision: 2, scale: 0, default: 0
    t.string   "omnibees_code"
    t.string   "booking_name_pt_br"
    t.string   "booking_name_en"
    t.string   "name_es"
    t.text     "description_es"
    t.string   "booking_name_es"
  end

  add_index "room_types", ["hotel_id"], name: "index_room_types_on_hotel_id", using: :btree

  create_table "rooms", force: true do |t|
    t.string   "number"
    t.integer  "room_type_id"
    t.integer  "hotel_id"
    t.boolean  "is_active",    default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rooms", ["hotel_id"], name: "index_rooms_on_hotel_id", using: :btree
  add_index "rooms", ["is_active"], name: "index_rooms_on_is_active", using: :btree
  add_index "rooms", ["room_type_id"], name: "index_rooms_on_room_type_id", using: :btree

  create_table "search_logs_users", force: true do |t|
    t.string "email"
  end

  add_index "search_logs_users", ["email"], name: "index_search_logs_users_on_email", using: :btree

  create_table "sessions", force: true do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "states", force: true do |t|
    t.string  "initials"
    t.string  "name"
    t.integer "country_id"
  end

  add_index "states", ["country_id"], name: "index_states_on_country_id", using: :btree
  add_index "states", ["initials"], name: "index_states_on_initials", using: :btree

  create_table "travel_requests", force: true do |t|
    t.integer  "hq_consultant_id"
    t.integer  "user_id"
    t.datetime "date_request"
    t.datetime "date_conclusion"
    t.datetime "date_canceled"
    t.integer  "request_status"
    t.integer  "payment_status"
    t.text     "comments_traveler"
    t.text     "comments_consultant"
    t.decimal  "total",               precision: 6, scale: 2, default: 0.0
    t.decimal  "total_paid_hq",       precision: 6, scale: 2, default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contact_traveler"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "cpf"
    t.string   "passaporte"
    t.string   "telephone"
    t.string   "email"
    t.integer  "reservation_status"
  end

  add_index "travel_requests", ["hq_consultant_id"], name: "index_travel_requests_on_hq_consultant_id", using: :btree
  add_index "travel_requests", ["user_id"], name: "index_travel_requests_on_user_id", using: :btree

  create_table "travelers", force: true do |t|
    t.integer  "cost_center_id"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "maxipago_token"
    t.integer  "company_detail_id"
    t.string   "phone_number"
    t.string   "cpf"
    t.string   "passport"
    t.string   "street"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "postal_code"
    t.integer  "number"
    t.string   "complement"
    t.integer  "job_title_id"
  end

  add_index "travelers", ["company_detail_id"], name: "index_travelers_on_company_detail_id", using: :btree

  create_table "uber_vouchers", force: true do |t|
    t.string   "code"
    t.datetime "linked_at"
    t.date     "expiration_date"
    t.integer  "booking_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "uber_vouchers", ["booking_id"], name: "index_uber_vouchers_on_booking_id", using: :btree
  add_index "uber_vouchers", ["code"], name: "index_uber_vouchers_on_code", using: :btree
  add_index "uber_vouchers", ["expiration_date", "booking_id"], name: "index_uber_vouchers_on_expiration_date_and_booking_id", using: :btree
  add_index "uber_vouchers", ["expiration_date"], name: "index_uber_vouchers_on_expiration_date", using: :btree

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "cpf"
    t.string   "passport"
    t.string   "gender"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.string   "street"
    t.integer  "number"
    t.string   "complement"
    t.string   "email",                                                     default: "",    null: false
    t.string   "encrypted_password",                                        default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                             default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "phone_number"
    t.string   "locale"
    t.string   "maxipago_token"
    t.integer  "company_id"
    t.integer  "department_id"
    t.integer  "monthly_budget_centavos"
    t.string   "monthly_budget_currency",                                   default: "BRL", null: false
    t.date     "birth_date"
    t.integer  "budget_for_24_hours_pack_centavos",                         default: 0,     null: false
    t.string   "budget_for_24_hours_pack_currency",                         default: "BRL", null: false
    t.boolean  "is_active",                                                 default: true
    t.boolean  "created_by_affiliate",                                      default: false
    t.integer  "affiliate_id"
    t.boolean  "accept_confirmed_and_invoiced",                             default: false
    t.string   "postal_code"
    t.integer  "emkt_flux"
    t.string   "emkt_day_of_week"
    t.integer  "emkt_flux_type"
    t.string   "refer_code"
    t.decimal  "balance",                           precision: 6, scale: 2, default: 0.0
    t.integer  "traveler_id"
  end

  add_index "users", ["affiliate_id"], name: "index_users_on_affiliate_id", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
