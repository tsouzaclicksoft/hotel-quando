﻿source 'https://rubygems.org'

ruby '2.1.2'

gem 'rails', '4.1.0'

gem 'http_accept_language'

#database
gem 'pg', '0.17.1'
gem 'devise', '3.2.2'
gem 'activerecord-import', '0.4.1'
gem 'money-rails', '0.12.0'

# maxipago api
gem "maxipago", '1.0.1.2a', :git => "https://github.com/viniciusoyama/Ruby-Integration-lib-v2.git", :tag => "1.0.1.2a"

# paypal api
gem 'paypal-sdk-rest'

#new Gem Epayco
gem 'epayco-ruby', :git => "https://github.com/rafex128/epayco-ruby.git"

gem 'tzinfo-data'
gem 'tzinfo'

#token for mobile auth
gem 'jwt'
gem 'rack-cors', :require => 'rack/cors'

# credit card validation
gem 'credit_card_validator', '1.2.0'

# cpf validator
gem "cpf_validator", '0.2.0'

# controllers
gem 'inherited_resources', '1.4.1'
gem 'has_scope', '0.6.0rc'

# server
gem 'rb-readline'
gem 'thin', '1.6.1'
gem 'puma', '2.11.1'
gem 'clockwork', '0.7.3'
gem 'rack-canonical-host', '0.0.8'
#gem 'rack-cors', '0.4.0' # to change the assets header so the font-face works in IE and Firefox
gem 'rack-cors', :require => 'rack/cors'

#ftp
gem 'net-sftp', '2.1.2'

#assets
gem 'jquery-rails', '3.0.4'
gem 'sass-rails', '4.0.5'
gem 'uglifier', '2.3.1'
gem "bootstrap-sass", "3.1.1"
gem 'bourbon', '3.1.8'
gem 'codus_templates', '0.0.1.6', git: 'https://bitbucketreadonly:baixandogems@bitbucket.org/codustecnologia/codus_templates.git', tag: 'v0.0.1.6'
gem "font-awesome-sass"

# atachments
gem 'paperclip', '3.5.4'
gem 'aws-sdk', '1.11.1'

# view
gem "slim", '2.0.2'
gem "html2slim", "0.0.4"
gem "bootstrap-datepicker-rails", '1.5.0'
gem 'simple_form', '3.0.2'
gem 'codus', '0.0.1.7.1'
gem 'show_for', '0.3.0'
gem 'js-routes', '0.9.6'
gem 's3_direct_upload', '0.1.7'
gem 'kaminari'
gem 'jquery-minicolors-rails'

# Create json files with complexity struchture
gem 'rabl'
# With Rabl, Also add either `oj` or `yajl-ruby` as the JSON parser
gem 'oj'
gem 'jbuilder'


# parsers
gem 'nokogiri', '1.5.11'
gem 'premailer-rails', '1.7.0'
gem 'geocoder', '1.1.9'
gem 'route_translator', '3.1.0'
gem 'mail'

# queues
gem 'sidekiq', '2.17.1'
gem 'sinatra', '1.4.4', require: false

# Poros
gem 'virtus', '1.0.3'

# xlsx spreadsheet generation
gem 'axlsx', '~> 2.0.1'

# SOAP
gem 'savon'

# Monitoring
gem 'airbrake', '4.2.1'

gem 'activerecord-session_store'

#heroku api
gem 'platform-api', require: false

# Autocomplete
gem "selectize-rails", :git => 'https://github.com/manuelvanrijn/selectize-rails'

# Sitemap
gem 'sitemap_generator'
gem 'fog-aws'

# HTTParty
gem 'httparty'

# Get timezone from latitude and longitude
gem 'timezone', '~> 1.0'

# Airports' slider
gem "jquery-slick-rails"

# Log gem to keep model changes' history
gem "audited", "~> 4.5"

# development helpers and tests
group :development, :test do
  gem 'factory_girl_rails', '3.4.0'
  gem 'quiet_assets'
  gem 'byebug'
  gem 'dotenv-rails'
  gem 'spring-commands-rspec'
  gem 'faker'
end

# automate deploy
group :development do
  gem 'capistrano', '3.5.0'
  gem 'capistrano-rvm'
  gem 'capistrano-nginx'
  gem 'capistrano3-puma'
  gem 'capistrano-rails'
  gem 'capistrano-rails-db'
  gem 'capistrano-rails-console'
  gem 'capistrano-upload-config'
  gem 'sshkit-sudo'
end

group :test do
  gem 'fakeredis', '0.4.3'
  gem 'fakeweb', '1.3.0'
end

group :production, :staging do
  gem 'rails_12factor', '0.0.2'
end

# New Relic Gems
# see https://devcenter.heroku.com/articles/newrelic#cedar
group :production, :staging do
  gem 'newrelic_rpm'
  gem 'newrelic-redis', '1.4.0'
end

group :test do
  gem 'minitest'
  gem 'rspec-rails', '~> 2.0'
  gem 'capybara'
  gem 'launchy'
  gem 'poltergeist'
  gem 'database_cleaner'
  gem 'timecop', '0.7.1'
  gem 'show_me_the_cookies'
  gem 'capybara-screenshot'
  gem "shoulda-matchers", require: false
  gem 'stub_env'
end

gem 'figaro', '~> 1.1', '>= 1.1.1'
