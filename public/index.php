<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>PEG Madeiras</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	<!--link rel="stylesheet/less" href="less/bootstrap.less" type="text/css" /-->
	<!--link rel="stylesheet/less" href="less/responsive.less" type="text/css" /-->
	<!--script src="js/less-1.3.3.min.js"></script-->
	<!--append ‘#!watch’ to the browser URL, then refresh the page. -->

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
  <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"></head>

<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<?php require_once('includes/menu.php'); ?>

		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="carousel slide" id="carousel-597479">
				<!--ol class="carousel-indicators">
					<li class="active" data-slide-to="0" data-target="#carousel-597479">
					</li>
					<li data-slide-to="1" data-target="#carousel-597479">
                    </li>
                    <li data-slide-to="2" data-target="#carousel-597479">
                    </li>

				</ol-->
				<div class="carousel-inner">
                <div class="item  active"> <a href="plano-de-corte.php"><img alt="" src="img/slid0.jpg"></a>

                </div>
					<div class="item"> <img alt="" src="img/slid3.jpg">
						<!--div class="carousel-caption">
						  <h4>
								First Thumbnail label
							</h4>
							<p>
								Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
							</p>
						</div-->
					</div>
					<div class="item">
						<img alt="" src="img/slid1.jpg">

        </div>
					<div class="item">
						<img alt="" src="img/slid2.jpg">

					</div>
				</div> <a class="left carousel-control" href="#carousel-597479" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#carousel-597479" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-12 column">
        <div id="texto-home">
			<h3>
				Seja Bem-vindo!</h3>
                <!-- entra chamada black friday-->
            <div><a href="http://pegmadeiras.com.br/loja/" target="_blank"><img src="img/banner-black-friday2.jpg" class="img-responsive"></a></div><br>
            <!-- fecha chamada black friday-->
			<p>Somos uma Empresa no ramo de Madeiras, onde o nosso Principal produto é voltado para os segmentos: Moveleiro em geral, Arquitetura, Designers, Cenografia e Stands &amp; Eventos. O Nosso diferencial é a nossa entrega que é uma das mais rápidas do   ramo.Temos todo material em nosso estoque para pronta entrega.</p>
			<p>Uma Empresa séria que pensa no Cliente e no que há de melhor e mais moderno para atendê-lo.</p>
			<p>&nbsp;</p>

          <p align="center"><a href="plano-de-corte.php"><img src="img/banner-corte-home.png" class="img-responsive"></a></p>
            <p>
		  <strong>SOFTWARE DE PROJETOS</strong><br>
		  Com nosso software de projetos é possível fazer apresentações com um nível <strong>profissional de alta qualidade</strong>! Projeto em 3D + Render rápido + Orçamento + Produção + Plano de Corte + Furação. <em>Com nosso software de projetos é possível fazer integração com a CNC</em>. </p>

        </div></div>
	</div><p align="center"><a href="plano-de-corte.php"><img src="img/saiba-mais.png" width="250" height="106"></a></p>
<!-- Destaque máquinas -->
	<?php require_once('includes/destaq-maquinas.php'); ?>

	<!-- Fim destaque máquinas -->
    <hr>
    <!--div class="row clearfix">
		<div class="col-md-12 column" align="center">
        <div  class="taman-video">
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/Xh_IwVtUAFs" frameborder="0" allowfullscreen> </iframe>
        </div></div>
        </div>
        <hr-->
  <h1> <img src="img/ico-conheca.png" width="40" height="40" > CONHEÇA MAIS!</h1>
    <div class="row clearfix">
		<div class="col-md-4 column">
        <div class="thumbnail">
			<img alt="140x140" src="img/destaq4.jpg" class="img-responsive">
			<h2>
				Nossos Produtos
			</h2>
			<p>Nosso produto é voltado para os segmentos, moveleiro em geral, Arquitetura, Designers, Cenografia...</p>
			<p>
				<a class="btn" href="produtos.php">Saiba Mais »</a>
		  </p></div>
		</div>
		<div class="col-md-4 column">
<div class="thumbnail">
			<img alt="140x140" src="img/destaq5.jpg" class="img-responsive">
			<h2>
				A Peg Madeiras
			</h2>
			<p>
				Venhos nos visitar ou ligue para nós, teremos o maior prazer em atendê-lo. Esperamos por você!</p>
			<p>
				<a class="btn" href="a-empresa.php">Saiba mais »</a>
		  </p></div>
		</div>
	  <div class="col-md-4 column">
         <div class="thumbnail">
      <img alt="140x140" src="img/destaq6.jpg" class="img-responsive">

			<h2>
				Catálogo OnLine
			</h2>
			<p>
				Com uma vasta variedade de produtos a Peg Madeiras possui um catálogo onLine para te auxiliar na compra.
		   </p>
			<p>
				<a href="http://pegmadeiras.com.br/loja/" target="_blank">Ver Catálogo »</a>
		</p></div>
	  </div>
	</div>

    	<div class="row clearfix">
            <?php require_once('includes/rodape.php'); ?>
</div>
	</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('.carousel').carousel({interval: 3000});
  });
</script>
</body>
</html>
