(function() {
  var NodeTypes, ParameterMissing, Utils, defaults,
    __hasProp = {}.hasOwnProperty;

  ParameterMissing = function(message) {
    this.message = message;
  };

  ParameterMissing.prototype = new Error();

  defaults = {
    prefix: "",
    default_url_options: {}
  };

  NodeTypes = {"GROUP":1,"CAT":2,"SYMBOL":3,"OR":4,"STAR":5,"LITERAL":6,"SLASH":7,"DOT":8};

  Utils = {
    serialize: function(object, prefix) {
      var element, i, key, prop, result, s, _i, _len;

      if (prefix == null) {
        prefix = null;
      }
      if (!object) {
        return "";
      }
      if (!prefix && !(this.get_object_type(object) === "object")) {
        throw new Error("Url parameters should be a javascript hash");
      }
      if (window.jQuery) {
        result = window.jQuery.param(object);
        return (!result ? "" : result);
      }
      s = [];
      switch (this.get_object_type(object)) {
        case "array":
          for (i = _i = 0, _len = object.length; _i < _len; i = ++_i) {
            element = object[i];
            s.push(this.serialize(element, prefix + "[]"));
          }
          break;
        case "object":
          for (key in object) {
            if (!__hasProp.call(object, key)) continue;
            prop = object[key];
            if (!(prop != null)) {
              continue;
            }
            if (prefix != null) {
              key = "" + prefix + "[" + key + "]";
            }
            s.push(this.serialize(prop, key));
          }
          break;
        default:
          if (object) {
            s.push("" + (encodeURIComponent(prefix.toString())) + "=" + (encodeURIComponent(object.toString())));
          }
      }
      if (!s.length) {
        return "";
      }
      return s.join("&");
    },
    clean_path: function(path) {
      var last_index;

      path = path.split("://");
      last_index = path.length - 1;
      path[last_index] = path[last_index].replace(/\/+/g, "/");
      return path.join("://");
    },
    set_default_url_options: function(optional_parts, options) {
      var i, part, _i, _len, _results;

      _results = [];
      for (i = _i = 0, _len = optional_parts.length; _i < _len; i = ++_i) {
        part = optional_parts[i];
        if (!options.hasOwnProperty(part) && defaults.default_url_options.hasOwnProperty(part)) {
          _results.push(options[part] = defaults.default_url_options[part]);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    },
    extract_anchor: function(options) {
      var anchor;

      anchor = "";
      if (options.hasOwnProperty("anchor")) {
        anchor = "#" + options.anchor;
        delete options.anchor;
      }
      return anchor;
    },
    extract_options: function(number_of_params, args) {
      var last_argument, type;

      last_argument = args[args.length - 1];
      type = this.get_object_type(last_argument);
      if (args.length > number_of_params || (type === "object" && !this.look_like_serialized_model(last_argument))) {
        return args.pop();
      } else {
        return {};
      }
    },
    look_like_serialized_model: function(object) {
      return "id" in object || "to_param" in object;
    },
    path_identifier: function(object) {
      var property;

      if (object === 0) {
        return "0";
      }
      if (!object) {
        return "";
      }
      property = object;
      if (this.get_object_type(object) === "object") {
        property = object.to_param || object.id || object;
        if (this.get_object_type(property) === "function") {
          property = property.call(object);
        }
      }
      return property.toString();
    },
    clone: function(obj) {
      var attr, copy, key;

      if ((obj == null) || "object" !== this.get_object_type(obj)) {
        return obj;
      }
      copy = obj.constructor();
      for (key in obj) {
        if (!__hasProp.call(obj, key)) continue;
        attr = obj[key];
        copy[key] = attr;
      }
      return copy;
    },
    prepare_parameters: function(required_parameters, actual_parameters, options) {
      var i, result, val, _i, _len;

      result = this.clone(options) || {};
      for (i = _i = 0, _len = required_parameters.length; _i < _len; i = ++_i) {
        val = required_parameters[i];
        if (i < actual_parameters.length) {
          result[val] = actual_parameters[i];
        }
      }
      return result;
    },
    build_path: function(required_parameters, optional_parts, route, args) {
      var anchor, opts, parameters, result, url, url_params;

      args = Array.prototype.slice.call(args);
      opts = this.extract_options(required_parameters.length, args);
      if (args.length > required_parameters.length) {
        throw new Error("Too many parameters provided for path");
      }
      parameters = this.prepare_parameters(required_parameters, args, opts);
      this.set_default_url_options(optional_parts, parameters);
      anchor = this.extract_anchor(parameters);
      result = "" + (this.get_prefix()) + (this.visit(route, parameters));
      url = Utils.clean_path("" + result);
      if ((url_params = this.serialize(parameters)).length) {
        url += "?" + url_params;
      }
      url += anchor;
      return url;
    },
    visit: function(route, parameters, optional) {
      var left, left_part, right, right_part, type, value;

      if (optional == null) {
        optional = false;
      }
      type = route[0], left = route[1], right = route[2];
      switch (type) {
        case NodeTypes.GROUP:
          return this.visit(left, parameters, true);
        case NodeTypes.STAR:
          return this.visit_globbing(left, parameters, true);
        case NodeTypes.LITERAL:
        case NodeTypes.SLASH:
        case NodeTypes.DOT:
          return left;
        case NodeTypes.CAT:
          left_part = this.visit(left, parameters, optional);
          right_part = this.visit(right, parameters, optional);
          if (optional && !(left_part && right_part)) {
            return "";
          }
          return "" + left_part + right_part;
        case NodeTypes.SYMBOL:
          value = parameters[left];
          if (value != null) {
            delete parameters[left];
            return this.path_identifier(value);
          }
          if (optional) {
            return "";
          } else {
            throw new ParameterMissing("Route parameter missing: " + left);
          }
          break;
        default:
          throw new Error("Unknown Rails node type");
      }
    },
    visit_globbing: function(route, parameters, optional) {
      var left, right, type, value;

      type = route[0], left = route[1], right = route[2];
      if (left.replace(/^\*/i, "") !== left) {
        route[1] = left = left.replace(/^\*/i, "");
      }
      value = parameters[left];
      if (value == null) {
        return this.visit(route, parameters, optional);
      }
      parameters[left] = (function() {
        switch (this.get_object_type(value)) {
          case "array":
            return value.join("/");
          default:
            return value;
        }
      }).call(this);
      return this.visit(route, parameters, optional);
    },
    get_prefix: function() {
      var prefix;

      prefix = defaults.prefix;
      if (prefix !== "") {
        prefix = (prefix.match("/$") ? prefix : "" + prefix + "/");
      }
      return prefix;
    },
    _classToTypeCache: null,
    _classToType: function() {
      var name, _i, _len, _ref;

      if (this._classToTypeCache != null) {
        return this._classToTypeCache;
      }
      this._classToTypeCache = {};
      _ref = "Boolean Number String Function Array Date RegExp Undefined Null".split(" ");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        name = _ref[_i];
        this._classToTypeCache["[object " + name + "]"] = name.toLowerCase();
      }
      return this._classToTypeCache;
    },
    get_object_type: function(obj) {
      var strType;

      if (window.jQuery && (window.jQuery.type != null)) {
        return window.jQuery.type(obj);
      }
      strType = Object.prototype.toString.call(obj);
      return this._classToType()[strType] || "object";
    },
    namespace: function(root, namespaceString) {
      var current, parts;

      parts = (namespaceString ? namespaceString.split(".") : []);
      if (!parts.length) {
        return;
      }
      current = parts.shift();
      root[current] = root[current] || {};
      return Utils.namespace(root[current], parts.join("."));
    }
  };

  Utils.namespace(window, "Routes");

  window.Routes = {
// activate_admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/:id/activate(.:format)
  activate_admin_company_attendant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/:id/activate(.:format)
  activate_admin_company_attendant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/:id/activate(.:format)
  activate_admin_company_attendant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_region_en => /en/admin/universo-de-empresas/regioes/:id/activate(.:format)
  activate_admin_company_region_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_region_es => /es/admin/universo-de-empresas/regioes/:id/activate(.:format)
  activate_admin_company_region_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_region_pt_br => /admin/universo-de-empresas/regioes/:id/activate(.:format)
  activate_admin_company_region_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_universe_en => /en/admin/universo-de-empresas/empresas/:id/activate(.:format)
  activate_admin_company_universe_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_universe_es => /es/admin/universo-de-empresas/empresas/:id/activate(.:format)
  activate_admin_company_universe_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/:id/activate(.:format)
  activate_admin_company_universe_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_executive_responsible_company_regions => /executivos/universo-de-empresas/regioes/activate(.:format)
  activate_executive_responsible_company_regions_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_executive_responsible_company_universes => /executivos/universo-de-empresas/empresas/activate(.:format)
  activate_executive_responsible_company_universes_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_company_en => /en/admin/empresas-contratantes/:id(.:format)
  admin_booking_pack_contracting_company_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_company_es => /es/admin/empresas-contratantes/:id(.:format)
  admin_booking_pack_contracting_company_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_company_pt_br => /admin/empresas-contratantes/:id(.:format)
  admin_booking_pack_contracting_company_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/:id(.:format)
  admin_company_attendant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/:id(.:format)
  admin_company_attendant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/:id(.:format)
  admin_company_attendant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendants_en => /en/admin/universo-de-empresas/atendimento-empresas(.:format)
  admin_company_attendants_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendants_es => /es/admin/universo-de-empresas/atendimento-empresas(.:format)
  admin_company_attendants_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendants_pt_br => /admin/universo-de-empresas/atendimento-empresas(.:format)
  admin_company_attendants_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_detail_en => /en/admin/empresas/:id(.:format)
  admin_company_detail_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_detail_es => /es/admin/empresas/:id(.:format)
  admin_company_detail_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_detail_pt_br => /admin/empresas/:id(.:format)
  admin_company_detail_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_details_en => /en/admin/empresas(.:format)
  admin_company_details_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_details_es => /es/admin/empresas(.:format)
  admin_company_details_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_details_pt_br => /admin/empresas(.:format)
  admin_company_details_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_region_en => /en/admin/universo-de-empresas/regioes/:id(.:format)
  admin_company_region_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_region_es => /es/admin/universo-de-empresas/regioes/:id(.:format)
  admin_company_region_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_region_pt_br => /admin/universo-de-empresas/regioes/:id(.:format)
  admin_company_region_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_regions_en => /en/admin/universo-de-empresas/regioes(.:format)
  admin_company_regions_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_regions_es => /es/admin/universo-de-empresas/regioes(.:format)
  admin_company_regions_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_regions_pt_br => /admin/universo-de-empresas/regioes(.:format)
  admin_company_regions_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remuneration_en => /en/admin/universo-de-empresas/remuneracao/:id(.:format)
  admin_company_remuneration_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remuneration_es => /es/admin/universo-de-empresas/remuneracao/:id(.:format)
  admin_company_remuneration_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remuneration_pt_br => /admin/universo-de-empresas/remuneracao/:id(.:format)
  admin_company_remuneration_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remunerations_en => /en/admin/universo-de-empresas/remuneracao(.:format)
  admin_company_remunerations_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remunerations_es => /es/admin/universo-de-empresas/remuneracao(.:format)
  admin_company_remunerations_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remunerations_pt_br => /admin/universo-de-empresas/remuneracao(.:format)
  admin_company_remunerations_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universe_en => /en/admin/universo-de-empresas/empresas/:id(.:format)
  admin_company_universe_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universe_es => /es/admin/universo-de-empresas/empresas/:id(.:format)
  admin_company_universe_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/:id(.:format)
  admin_company_universe_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universes_en => /en/admin/universo-de-empresas/empresas(.:format)
  admin_company_universes_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universes_es => /es/admin/universo-de-empresas/empresas(.:format)
  admin_company_universes_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universes_pt_br => /admin/universo-de-empresas/empresas(.:format)
  admin_company_universes_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// all_companies_executive_responsible_company_universes => /executivos/universo-de-empresas/empresas/todas-empresas(.:format)
  all_companies_executive_responsible_company_universes_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"todas-empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// authenticate_as_company_employee => /empresas/funcionarios/:id/acessar-como(.:format)
  authenticate_as_company_employee_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"funcionarios",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"acessar-como",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// authenticate_as_executive_responsible_company_detail => /executivos/empresas/:id/authenticate_as(.:format)
  authenticate_as_executive_responsible_company_detail_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"authenticate_as",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// cancel_company_booking => /empresas/reservas/:id/cancel(.:format)
  cancel_company_booking_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"cancel",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_attendant => /atendimento/mudar-senha(.:format)
  company_attendant_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_attendant_change_password => /atendimento/mudar-senha(.:format)
  company_attendant_change_password_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_attendant_company_attendant_password => /atendimento/password(.:format)
  company_attendant_company_attendant_password_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"password",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_attendant_company_attendant_session => /atendimento/entrar(.:format)
  company_attendant_company_attendant_session_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_attendant_company_detail => /atendimento/empresas/:id(.:format)
  company_attendant_company_detail_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_attendant_company_details => /atendimento/empresas(.:format)
  company_attendant_company_details_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_attendant_root => /atendimento(.:format)
  company_attendant_root_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"atendimento",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_booking => /empresas/reservas/:id(.:format)
  company_booking_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_bookings => /empresas/reservas(.:format)
  company_bookings_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"reservas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_company_password => /empresas/password(.:format)
  company_company_password_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"password",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_company_session => /empresas/entrar(.:format)
  company_company_session_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_credit_card => /empresas/cartoes-de-credito/:id(.:format)
  company_credit_card_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"cartoes-de-credito",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_credit_cards => /empresas/cartoes-de-credito(.:format)
  company_credit_cards_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"cartoes-de-credito",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_department => /empresas/departamentos/:id(.:format)
  company_department_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"departamentos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_departments => /empresas/departamentos(.:format)
  company_departments_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"departamentos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_employee => /empresas/funcionarios/:id(.:format)
  company_employee_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"funcionarios",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_employees => /empresas/funcionarios(.:format)
  company_employees_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"funcionarios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_invoice => /empresas/dados-para-nf/:id(.:format)
  company_invoice_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"dados-para-nf",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_invoices => /empresas/dados-para-nf(.:format)
  company_invoices_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"dados-para-nf",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_root => /empresas(.:format)
  company_root_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// company_update_info => /empresas/atualizar-informacoes(.:format)
  company_update_info_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"atualizar-informacoes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/:id/desativar(.:format)
  deactivate_admin_company_attendant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/:id/desativar(.:format)
  deactivate_admin_company_attendant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/:id/desativar(.:format)
  deactivate_admin_company_attendant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_region_en => /en/admin/universo-de-empresas/regioes/:id/desativar(.:format)
  deactivate_admin_company_region_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_region_es => /es/admin/universo-de-empresas/regioes/:id/desativar(.:format)
  deactivate_admin_company_region_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_region_pt_br => /admin/universo-de-empresas/regioes/:id/desativar(.:format)
  deactivate_admin_company_region_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_universe_en => /en/admin/universo-de-empresas/empresas/:id/desativar(.:format)
  deactivate_admin_company_universe_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_universe_es => /es/admin/universo-de-empresas/empresas/:id/desativar(.:format)
  deactivate_admin_company_universe_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/:id/desativar(.:format)
  deactivate_admin_company_universe_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_executive_responsible_company_regions => /executivos/universo-de-empresas/regioes/deactivate(.:format)
  deactivate_executive_responsible_company_regions_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"deactivate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_executive_responsible_company_universes => /executivos/universo-de-empresas/empresas/deactivate(.:format)
  deactivate_executive_responsible_company_universes_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"deactivate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_company_attendant_company_attendant_session => /atendimento/sair(.:format)
  destroy_company_attendant_company_attendant_session_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"sair",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_company_company_session => /empresas/sair(.:format)
  destroy_company_company_session_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"sair",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/:id/editar(.:format)
  edit_admin_company_attendant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/:id/editar(.:format)
  edit_admin_company_attendant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/:id/editar(.:format)
  edit_admin_company_attendant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_detail_en => /en/admin/empresas/:id/editar(.:format)
  edit_admin_company_detail_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_detail_es => /es/admin/empresas/:id/editar(.:format)
  edit_admin_company_detail_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_detail_pt_br => /admin/empresas/:id/editar(.:format)
  edit_admin_company_detail_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_region_en => /en/admin/universo-de-empresas/regioes/:id/editar(.:format)
  edit_admin_company_region_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_region_es => /es/admin/universo-de-empresas/regioes/:id/editar(.:format)
  edit_admin_company_region_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_region_pt_br => /admin/universo-de-empresas/regioes/:id/editar(.:format)
  edit_admin_company_region_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_remuneration_en => /en/admin/universo-de-empresas/remuneracao/:id/editar(.:format)
  edit_admin_company_remuneration_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_remuneration_es => /es/admin/universo-de-empresas/remuneracao/:id/editar(.:format)
  edit_admin_company_remuneration_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_remuneration_pt_br => /admin/universo-de-empresas/remuneracao/:id/editar(.:format)
  edit_admin_company_remuneration_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_universe_en => /en/admin/universo-de-empresas/empresas/:id/editar(.:format)
  edit_admin_company_universe_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_universe_es => /es/admin/universo-de-empresas/empresas/:id/editar(.:format)
  edit_admin_company_universe_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/:id/editar(.:format)
  edit_admin_company_universe_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_company_attendant_company_attendant_password => /atendimento/password/editar(.:format)
  edit_company_attendant_company_attendant_password_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_company_attendant_company_detail => /atendimento/empresas/:id/editar(.:format)
  edit_company_attendant_company_detail_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_company_company_password => /empresas/password/editar(.:format)
  edit_company_company_password_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_company_department => /empresas/departamentos/:id/editar(.:format)
  edit_company_department_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"departamentos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_company_employee => /empresas/funcionarios/:id/editar(.:format)
  edit_company_employee_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"funcionarios",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_executive_responsible_company_detail => /executivos/empresas/:id/editar(.:format)
  edit_executive_responsible_company_detail_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_executive_responsible_company_region => /executivos/universo-de-empresas/regioes/:id/editar(.:format)
  edit_executive_responsible_company_region_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_executive_responsible_company_universe => /executivos/universo-de-empresas/empresas/:id/editar(.:format)
  edit_executive_responsible_company_universe_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_public_company_detail_en => /en/clients/:id/edit(.:format)
  edit_public_company_detail_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"clients",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"edit",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_public_company_detail_es => /es/clientes/:id/editar(.:format)
  edit_public_company_detail_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"clientes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_public_company_detail_pt_br => /clientes/:id/editar(.:format)
  edit_public_company_detail_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"clientes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// executive_responsible_company_detail => /executivos/empresas/:id(.:format)
  executive_responsible_company_detail_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// executive_responsible_company_details => /executivos/empresas(.:format)
  executive_responsible_company_details_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// executive_responsible_company_region => /executivos/universo-de-empresas/regioes/:id(.:format)
  executive_responsible_company_region_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// executive_responsible_company_regions => /executivos/universo-de-empresas/regioes(.:format)
  executive_responsible_company_regions_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// executive_responsible_company_universe => /executivos/universo-de-empresas/empresas/:id(.:format)
  executive_responsible_company_universe_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// executive_responsible_company_universes => /executivos/universo-de-empresas/empresas(.:format)
  executive_responsible_company_universes_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/novo(.:format)
  new_admin_company_attendant_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/novo(.:format)
  new_admin_company_attendant_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/novo(.:format)
  new_admin_company_attendant_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_region_en => /en/admin/universo-de-empresas/regioes/novo(.:format)
  new_admin_company_region_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_region_es => /es/admin/universo-de-empresas/regioes/novo(.:format)
  new_admin_company_region_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_region_pt_br => /admin/universo-de-empresas/regioes/novo(.:format)
  new_admin_company_region_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_universe_en => /en/admin/universo-de-empresas/empresas/novo(.:format)
  new_admin_company_universe_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_universe_es => /es/admin/universo-de-empresas/empresas/novo(.:format)
  new_admin_company_universe_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/novo(.:format)
  new_admin_company_universe_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_attendant_company_attendant_password => /atendimento/password/novo(.:format)
  new_company_attendant_company_attendant_password_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_attendant_company_attendant_session => /atendimento/entrar(.:format)
  new_company_attendant_company_attendant_session_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_attendant_company_detail => /atendimento/empresas/novo(.:format)
  new_company_attendant_company_detail_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"atendimento",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_company_password => /empresas/password/novo(.:format)
  new_company_company_password_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_company_session => /empresas/entrar(.:format)
  new_company_company_session_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_credit_card => /empresas/cartoes-de-credito/novo(.:format)
  new_company_credit_card_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"cartoes-de-credito",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_department => /empresas/departamentos/novo(.:format)
  new_company_department_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"departamentos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_employee => /empresas/funcionarios/novo(.:format)
  new_company_employee_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"funcionarios",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_company_invoice => /empresas/dados-para-nf/novo(.:format)
  new_company_invoice_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"empresas",false]],[7,"/",false]],[6,"dados-para-nf",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_executive_responsible_company_detail => /executivos/empresas/novo(.:format)
  new_executive_responsible_company_detail_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_executive_responsible_company_region => /executivos/universo-de-empresas/regioes/novo(.:format)
  new_executive_responsible_company_region_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_executive_responsible_company_universe => /executivos/universo-de-empresas/empresas/novo(.:format)
  new_executive_responsible_company_universe_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_cost_center_en => /en/clients/:company_detail_id/cost-center/new(.:format)
  new_public_company_detail_cost_center_en_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"clients",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"cost-center",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_cost_center_es => /es/clientes/:company_detail_id/centro-de-custos/nuevo(.:format)
  new_public_company_detail_cost_center_es_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"clientes",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"centro-de-custos",false]],[7,"/",false]],[6,"nuevo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_cost_center_pt_br => /clientes/:company_detail_id/centro-de-custos/novo(.:format)
  new_public_company_detail_cost_center_pt_br_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"clientes",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"centro-de-custos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_en => /en/clients/new(.:format)
  new_public_company_detail_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"clients",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_es => /es/clientes/nuevo(.:format)
  new_public_company_detail_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"clientes",false]],[7,"/",false]],[6,"nuevo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_job_title_en => /en/clients/:company_detail_id/job-titles/new(.:format)
  new_public_company_detail_job_title_en_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"clients",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"job-titles",false]],[7,"/",false]],[6,"new",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_job_title_es => /es/clientes/:company_detail_id/t%C3%ADtulos-de-trabajo/nuevo(.:format)
  new_public_company_detail_job_title_es_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"clientes",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"t%C3%ADtulos-de-trabajo",false]],[7,"/",false]],[6,"nuevo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_job_title_pt_br => /clientes/:company_detail_id/cargos/novo(.:format)
  new_public_company_detail_job_title_pt_br_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"clientes",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"cargos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_public_company_detail_pt_br => /clientes/novo(.:format)
  new_public_company_detail_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"clientes",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_booking_pack_contracting_company_en => /en/booking_pack_contracting_company(.:format)
  public_booking_pack_contracting_company_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"booking_pack_contracting_company",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_booking_pack_contracting_company_es => /es/booking_pack_contracting_company(.:format)
  public_booking_pack_contracting_company_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"booking_pack_contracting_company",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_booking_pack_contracting_company_pt_br => /booking_pack_contracting_company(.:format)
  public_booking_pack_contracting_company_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"booking_pack_contracting_company",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_cost_centers_en => /en/clients/:company_detail_id/cost-center(.:format)
  public_company_detail_cost_centers_en_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"clients",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"cost-center",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_cost_centers_es => /es/clientes/:company_detail_id/centro-de-custos(.:format)
  public_company_detail_cost_centers_es_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"clientes",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"centro-de-custos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_cost_centers_pt_br => /clientes/:company_detail_id/centro-de-custos(.:format)
  public_company_detail_cost_centers_pt_br_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"clientes",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"centro-de-custos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_en => /en/clients/:id(.:format)
  public_company_detail_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"clients",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_es => /es/clientes/:id(.:format)
  public_company_detail_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"clientes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_job_titles_en => /en/clients/:company_detail_id/job-titles(.:format)
  public_company_detail_job_titles_en_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"clients",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"job-titles",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_job_titles_es => /es/clientes/:company_detail_id/t%C3%ADtulos-de-trabajo(.:format)
  public_company_detail_job_titles_es_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"clientes",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"t%C3%ADtulos-de-trabajo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_job_titles_pt_br => /clientes/:company_detail_id/cargos(.:format)
  public_company_detail_job_titles_pt_br_path: function(_company_detail_id, options) {
  return Utils.build_path(["company_detail_id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"clientes",false]],[7,"/",false]],[3,"company_detail_id",false]],[7,"/",false]],[6,"cargos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_detail_pt_br => /clientes/:id(.:format)
  public_company_detail_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"clientes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_details_en => /en/clients(.:format)
  public_company_details_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"clients",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_details_es => /es/clientes(.:format)
  public_company_details_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"clientes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_company_details_pt_br => /clientes(.:format)
  public_company_details_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"clientes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  }}
;

  window.Routes.options = defaults;

}).call(this);
