(function() {
  var NodeTypes, ParameterMissing, Utils, defaults,
    __hasProp = {}.hasOwnProperty;

  ParameterMissing = function(message) {
    this.message = message;
  };

  ParameterMissing.prototype = new Error();

  defaults = {
    prefix: "",
    default_url_options: {}
  };

  NodeTypes = {"GROUP":1,"CAT":2,"SYMBOL":3,"OR":4,"STAR":5,"LITERAL":6,"SLASH":7,"DOT":8};

  Utils = {
    serialize: function(object, prefix) {
      var element, i, key, prop, result, s, _i, _len;

      if (prefix == null) {
        prefix = null;
      }
      if (!object) {
        return "";
      }
      if (!prefix && !(this.get_object_type(object) === "object")) {
        throw new Error("Url parameters should be a javascript hash");
      }
      if (window.jQuery) {
        result = window.jQuery.param(object);
        return (!result ? "" : result);
      }
      s = [];
      switch (this.get_object_type(object)) {
        case "array":
          for (i = _i = 0, _len = object.length; _i < _len; i = ++_i) {
            element = object[i];
            s.push(this.serialize(element, prefix + "[]"));
          }
          break;
        case "object":
          for (key in object) {
            if (!__hasProp.call(object, key)) continue;
            prop = object[key];
            if (!(prop != null)) {
              continue;
            }
            if (prefix != null) {
              key = "" + prefix + "[" + key + "]";
            }
            s.push(this.serialize(prop, key));
          }
          break;
        default:
          if (object) {
            s.push("" + (encodeURIComponent(prefix.toString())) + "=" + (encodeURIComponent(object.toString())));
          }
      }
      if (!s.length) {
        return "";
      }
      return s.join("&");
    },
    clean_path: function(path) {
      var last_index;

      path = path.split("://");
      last_index = path.length - 1;
      path[last_index] = path[last_index].replace(/\/+/g, "/");
      return path.join("://");
    },
    set_default_url_options: function(optional_parts, options) {
      var i, part, _i, _len, _results;

      _results = [];
      for (i = _i = 0, _len = optional_parts.length; _i < _len; i = ++_i) {
        part = optional_parts[i];
        if (!options.hasOwnProperty(part) && defaults.default_url_options.hasOwnProperty(part)) {
          _results.push(options[part] = defaults.default_url_options[part]);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    },
    extract_anchor: function(options) {
      var anchor;

      anchor = "";
      if (options.hasOwnProperty("anchor")) {
        anchor = "#" + options.anchor;
        delete options.anchor;
      }
      return anchor;
    },
    extract_options: function(number_of_params, args) {
      var last_argument, type;

      last_argument = args[args.length - 1];
      type = this.get_object_type(last_argument);
      if (args.length > number_of_params || (type === "object" && !this.look_like_serialized_model(last_argument))) {
        return args.pop();
      } else {
        return {};
      }
    },
    look_like_serialized_model: function(object) {
      return "id" in object || "to_param" in object;
    },
    path_identifier: function(object) {
      var property;

      if (object === 0) {
        return "0";
      }
      if (!object) {
        return "";
      }
      property = object;
      if (this.get_object_type(object) === "object") {
        property = object.to_param || object.id || object;
        if (this.get_object_type(property) === "function") {
          property = property.call(object);
        }
      }
      return property.toString();
    },
    clone: function(obj) {
      var attr, copy, key;

      if ((obj == null) || "object" !== this.get_object_type(obj)) {
        return obj;
      }
      copy = obj.constructor();
      for (key in obj) {
        if (!__hasProp.call(obj, key)) continue;
        attr = obj[key];
        copy[key] = attr;
      }
      return copy;
    },
    prepare_parameters: function(required_parameters, actual_parameters, options) {
      var i, result, val, _i, _len;

      result = this.clone(options) || {};
      for (i = _i = 0, _len = required_parameters.length; _i < _len; i = ++_i) {
        val = required_parameters[i];
        if (i < actual_parameters.length) {
          result[val] = actual_parameters[i];
        }
      }
      return result;
    },
    build_path: function(required_parameters, optional_parts, route, args) {
      var anchor, opts, parameters, result, url, url_params;

      args = Array.prototype.slice.call(args);
      opts = this.extract_options(required_parameters.length, args);
      if (args.length > required_parameters.length) {
        throw new Error("Too many parameters provided for path");
      }
      parameters = this.prepare_parameters(required_parameters, args, opts);
      this.set_default_url_options(optional_parts, parameters);
      anchor = this.extract_anchor(parameters);
      result = "" + (this.get_prefix()) + (this.visit(route, parameters));
      url = Utils.clean_path("" + result);
      if ((url_params = this.serialize(parameters)).length) {
        url += "?" + url_params;
      }
      url += anchor;
      return url;
    },
    visit: function(route, parameters, optional) {
      var left, left_part, right, right_part, type, value;

      if (optional == null) {
        optional = false;
      }
      type = route[0], left = route[1], right = route[2];
      switch (type) {
        case NodeTypes.GROUP:
          return this.visit(left, parameters, true);
        case NodeTypes.STAR:
          return this.visit_globbing(left, parameters, true);
        case NodeTypes.LITERAL:
        case NodeTypes.SLASH:
        case NodeTypes.DOT:
          return left;
        case NodeTypes.CAT:
          left_part = this.visit(left, parameters, optional);
          right_part = this.visit(right, parameters, optional);
          if (optional && !(left_part && right_part)) {
            return "";
          }
          return "" + left_part + right_part;
        case NodeTypes.SYMBOL:
          value = parameters[left];
          if (value != null) {
            delete parameters[left];
            return this.path_identifier(value);
          }
          if (optional) {
            return "";
          } else {
            throw new ParameterMissing("Route parameter missing: " + left);
          }
          break;
        default:
          throw new Error("Unknown Rails node type");
      }
    },
    visit_globbing: function(route, parameters, optional) {
      var left, right, type, value;

      type = route[0], left = route[1], right = route[2];
      if (left.replace(/^\*/i, "") !== left) {
        route[1] = left = left.replace(/^\*/i, "");
      }
      value = parameters[left];
      if (value == null) {
        return this.visit(route, parameters, optional);
      }
      parameters[left] = (function() {
        switch (this.get_object_type(value)) {
          case "array":
            return value.join("/");
          default:
            return value;
        }
      }).call(this);
      return this.visit(route, parameters, optional);
    },
    get_prefix: function() {
      var prefix;

      prefix = defaults.prefix;
      if (prefix !== "") {
        prefix = (prefix.match("/$") ? prefix : "" + prefix + "/");
      }
      return prefix;
    },
    _classToTypeCache: null,
    _classToType: function() {
      var name, _i, _len, _ref;

      if (this._classToTypeCache != null) {
        return this._classToTypeCache;
      }
      this._classToTypeCache = {};
      _ref = "Boolean Number String Function Array Date RegExp Undefined Null".split(" ");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        name = _ref[_i];
        this._classToTypeCache["[object " + name + "]"] = name.toLowerCase();
      }
      return this._classToTypeCache;
    },
    get_object_type: function(obj) {
      var strType;

      if (window.jQuery && (window.jQuery.type != null)) {
        return window.jQuery.type(obj);
      }
      strType = Object.prototype.toString.call(obj);
      return this._classToType()[strType] || "object";
    },
    namespace: function(root, namespaceString) {
      var current, parts;

      parts = (namespaceString ? namespaceString.split(".") : []);
      if (!parts.length) {
        return;
      }
      current = parts.shift();
      root[current] = root[current] || {};
      return Utils.namespace(root[current], parts.join("."));
    }
  };

  Utils.namespace(window, "Routes");

  window.Routes = {
// activate_admin_affiliate_en => /en/admin/afiliados/:id/activate(.:format)
  activate_admin_affiliate_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_affiliate_es => /es/admin/afiliados/:id/activate(.:format)
  activate_admin_affiliate_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_affiliate_pt_br => /admin/afiliados/:id/activate(.:format)
  activate_admin_affiliate_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_agency_en => /en/admin/agencias/:id/activate(.:format)
  activate_admin_agency_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_agency_es => /es/admin/agencias/:id/activate(.:format)
  activate_admin_agency_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_agency_pt_br => /admin/agencias/:id/activate(.:format)
  activate_admin_agency_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/:id/activate(.:format)
  activate_admin_company_attendant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/:id/activate(.:format)
  activate_admin_company_attendant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/:id/activate(.:format)
  activate_admin_company_attendant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_region_en => /en/admin/universo-de-empresas/regioes/:id/activate(.:format)
  activate_admin_company_region_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_region_es => /es/admin/universo-de-empresas/regioes/:id/activate(.:format)
  activate_admin_company_region_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_region_pt_br => /admin/universo-de-empresas/regioes/:id/activate(.:format)
  activate_admin_company_region_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_universe_en => /en/admin/universo-de-empresas/empresas/:id/activate(.:format)
  activate_admin_company_universe_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_universe_es => /es/admin/universo-de-empresas/empresas/:id/activate(.:format)
  activate_admin_company_universe_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/:id/activate(.:format)
  activate_admin_company_universe_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_executive_responsible_en => /en/admin/universo-de-empresas/executivos/:id/activate(.:format)
  activate_admin_executive_responsible_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_executive_responsible_es => /es/admin/universo-de-empresas/executivos/:id/activate(.:format)
  activate_admin_executive_responsible_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_executive_responsible_pt_br => /admin/universo-de-empresas/executivos/:id/activate(.:format)
  activate_admin_executive_responsible_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_hq_consultant_en => /en/admin/consultores/:id/activate(.:format)
  activate_admin_hq_consultant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_hq_consultant_es => /es/admin/consultores/:id/activate(.:format)
  activate_admin_hq_consultant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_hq_consultant_pt_br => /admin/consultores/:id/activate(.:format)
  activate_admin_hq_consultant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_manager_en => /en/admin/managers/:id/activate(.:format)
  activate_admin_manager_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_manager_es => /es/admin/managers/:id/activate(.:format)
  activate_admin_manager_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_manager_pt_br => /admin/managers/:id/activate(.:format)
  activate_admin_manager_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_places_en => /en/admin/lugares/activate(.:format)
  activate_admin_places_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_places_es => /es/admin/lugares/activate(.:format)
  activate_admin_places_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// activate_admin_places_pt_br => /admin/lugares/activate(.:format)
  activate_admin_places_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"activate",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_affiliate_en => /en/admin/afiliados/:id(.:format)
  admin_affiliate_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_affiliate_es => /es/admin/afiliados/:id(.:format)
  admin_affiliate_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_affiliate_pt_br => /admin/afiliados/:id(.:format)
  admin_affiliate_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_affiliates_en => /en/admin/afiliados(.:format)
  admin_affiliates_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_affiliates_es => /es/admin/afiliados(.:format)
  admin_affiliates_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_affiliates_pt_br => /admin/afiliados(.:format)
  admin_affiliates_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agencies_en => /en/admin/agencias(.:format)
  admin_agencies_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agencies_es => /es/admin/agencias(.:format)
  admin_agencies_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agencies_pt_br => /admin/agencias(.:format)
  admin_agencies_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agency_en => /en/admin/agencias/:id(.:format)
  admin_agency_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agency_es => /es/admin/agencias/:id(.:format)
  admin_agency_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agency_pt_br => /admin/agencias/:id(.:format)
  admin_agency_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agency_reports_en => /en/admin/relatorios-agencias(.:format)
  admin_agency_reports_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"relatorios-agencias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agency_reports_es => /es/admin/relatorios-agencias(.:format)
  admin_agency_reports_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"relatorios-agencias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_agency_reports_pt_br => /admin/relatorios-agencias(.:format)
  admin_agency_reports_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"relatorios-agencias",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_en => /en/admin/reservas/:id(.:format)
  admin_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_es => /es/admin/reservas/:id(.:format)
  admin_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_companies_en => /en/admin/empresas-contratantes(.:format)
  admin_booking_pack_contracting_companies_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_companies_es => /es/admin/empresas-contratantes(.:format)
  admin_booking_pack_contracting_companies_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_companies_pt_br => /admin/empresas-contratantes(.:format)
  admin_booking_pack_contracting_companies_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_company_en => /en/admin/empresas-contratantes/:id(.:format)
  admin_booking_pack_contracting_company_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_company_es => /es/admin/empresas-contratantes/:id(.:format)
  admin_booking_pack_contracting_company_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_contracting_company_pt_br => /admin/empresas-contratantes/:id(.:format)
  admin_booking_pack_contracting_company_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas-contratantes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_en => /en/admin/banco-de-horas/:id(.:format)
  admin_booking_pack_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_es => /es/admin/banco-de-horas/:id(.:format)
  admin_booking_pack_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pack_pt_br => /admin/banco-de-horas/:id(.:format)
  admin_booking_pack_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_packs_en => /en/admin/banco-de-horas(.:format)
  admin_booking_packs_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_packs_es => /es/admin/banco-de-horas(.:format)
  admin_booking_packs_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_packs_pt_br => /admin/banco-de-horas(.:format)
  admin_booking_packs_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_pt_br => /admin/reservas/:id(.:format)
  admin_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_tax_en => /en/admin/taxas/:id(.:format)
  admin_booking_tax_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_tax_es => /es/admin/taxas/:id(.:format)
  admin_booking_tax_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_tax_pt_br => /admin/taxas/:id(.:format)
  admin_booking_tax_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_taxes_en => /en/admin/taxas(.:format)
  admin_booking_taxes_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_taxes_es => /es/admin/taxas(.:format)
  admin_booking_taxes_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_booking_taxes_pt_br => /admin/taxas(.:format)
  admin_booking_taxes_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_bookings_en => /en/admin/reservas(.:format)
  admin_bookings_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_bookings_es => /es/admin/reservas(.:format)
  admin_bookings_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_bookings_pt_br => /admin/reservas(.:format)
  admin_bookings_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_campaign_en => /en/admin/campanhas/:id(.:format)
  admin_campaign_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_campaign_es => /es/admin/campanhas/:id(.:format)
  admin_campaign_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_campaign_pt_br => /admin/campanhas/:id(.:format)
  admin_campaign_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_campaigns_en => /en/admin/campanhas(.:format)
  admin_campaigns_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_campaigns_es => /es/admin/campanhas(.:format)
  admin_campaigns_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_campaigns_pt_br => /admin/campanhas(.:format)
  admin_campaigns_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_change_password_en => /en/admin/mudar-senha(.:format)
  admin_change_password_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_change_password_es => /es/admin/mudar-senha(.:format)
  admin_change_password_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_change_password_pt_br => /admin/mudar-senha(.:format)
  admin_change_password_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_cities_en => /en/admin/cidades(.:format)
  admin_cities_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_cities_es => /es/admin/cidades(.:format)
  admin_cities_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_cities_pt_br => /admin/cidades(.:format)
  admin_cities_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"cidades",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_codus_objects_en => /en/admin/codus/objects(.:format)
  admin_codus_objects_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codus",false]],[7,"/",false]],[6,"objects",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_codus_objects_es => /es/admin/codus/objects(.:format)
  admin_codus_objects_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codus",false]],[7,"/",false]],[6,"objects",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_codus_objects_pt_br => /admin/codus/objects(.:format)
  admin_codus_objects_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"codus",false]],[7,"/",false]],[6,"objects",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/:id(.:format)
  admin_company_attendant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/:id(.:format)
  admin_company_attendant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/:id(.:format)
  admin_company_attendant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendants_en => /en/admin/universo-de-empresas/atendimento-empresas(.:format)
  admin_company_attendants_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendants_es => /es/admin/universo-de-empresas/atendimento-empresas(.:format)
  admin_company_attendants_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_attendants_pt_br => /admin/universo-de-empresas/atendimento-empresas(.:format)
  admin_company_attendants_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_detail_en => /en/admin/empresas/:id(.:format)
  admin_company_detail_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_detail_es => /es/admin/empresas/:id(.:format)
  admin_company_detail_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_detail_pt_br => /admin/empresas/:id(.:format)
  admin_company_detail_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_details_en => /en/admin/empresas(.:format)
  admin_company_details_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_details_es => /es/admin/empresas(.:format)
  admin_company_details_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_details_pt_br => /admin/empresas(.:format)
  admin_company_details_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_region_en => /en/admin/universo-de-empresas/regioes/:id(.:format)
  admin_company_region_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_region_es => /es/admin/universo-de-empresas/regioes/:id(.:format)
  admin_company_region_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_region_pt_br => /admin/universo-de-empresas/regioes/:id(.:format)
  admin_company_region_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_regions_en => /en/admin/universo-de-empresas/regioes(.:format)
  admin_company_regions_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_regions_es => /es/admin/universo-de-empresas/regioes(.:format)
  admin_company_regions_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_regions_pt_br => /admin/universo-de-empresas/regioes(.:format)
  admin_company_regions_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remuneration_en => /en/admin/universo-de-empresas/remuneracao/:id(.:format)
  admin_company_remuneration_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remuneration_es => /es/admin/universo-de-empresas/remuneracao/:id(.:format)
  admin_company_remuneration_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remuneration_pt_br => /admin/universo-de-empresas/remuneracao/:id(.:format)
  admin_company_remuneration_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remunerations_en => /en/admin/universo-de-empresas/remuneracao(.:format)
  admin_company_remunerations_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remunerations_es => /es/admin/universo-de-empresas/remuneracao(.:format)
  admin_company_remunerations_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_remunerations_pt_br => /admin/universo-de-empresas/remuneracao(.:format)
  admin_company_remunerations_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universe_en => /en/admin/universo-de-empresas/empresas/:id(.:format)
  admin_company_universe_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universe_es => /es/admin/universo-de-empresas/empresas/:id(.:format)
  admin_company_universe_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/:id(.:format)
  admin_company_universe_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universes_en => /en/admin/universo-de-empresas/empresas(.:format)
  admin_company_universes_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universes_es => /es/admin/universo-de-empresas/empresas(.:format)
  admin_company_universes_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_company_universes_pt_br => /admin/universo-de-empresas/empresas(.:format)
  admin_company_universes_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_currencies_en => /en/admin/moedas(.:format)
  admin_currencies_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_currencies_es => /es/admin/moedas(.:format)
  admin_currencies_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_currencies_pt_br => /admin/moedas(.:format)
  admin_currencies_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_currency_en => /en/admin/moedas/:id(.:format)
  admin_currency_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_currency_es => /es/admin/moedas/:id(.:format)
  admin_currency_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_currency_pt_br => /admin/moedas/:id(.:format)
  admin_currency_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_device_en => /en/admin/dispositivos/:id(.:format)
  admin_device_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"dispositivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_device_es => /es/admin/dispositivos/:id(.:format)
  admin_device_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"dispositivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_device_pt_br => /admin/dispositivos/:id(.:format)
  admin_device_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"dispositivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_devices_en => /en/admin/dispositivos(.:format)
  admin_devices_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"dispositivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_devices_es => /es/admin/dispositivos(.:format)
  admin_devices_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"dispositivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_devices_pt_br => /admin/dispositivos(.:format)
  admin_devices_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"dispositivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_emkt_en => /en/admin/emkt/:id(.:format)
  admin_emkt_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_emkt_es => /es/admin/emkt/:id(.:format)
  admin_emkt_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_emkt_pt_br => /admin/emkt/:id(.:format)
  admin_emkt_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_emkts_en => /en/admin/emkt(.:format)
  admin_emkts_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_emkts_es => /es/admin/emkt(.:format)
  admin_emkts_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_emkts_pt_br => /admin/emkt(.:format)
  admin_emkts_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_en => /en/admin/mudar-senha(.:format)
  admin_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_es => /es/admin/mudar-senha(.:format)
  admin_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_executive_responsible_en => /en/admin/universo-de-empresas/executivos/:id(.:format)
  admin_executive_responsible_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_executive_responsible_es => /es/admin/universo-de-empresas/executivos/:id(.:format)
  admin_executive_responsible_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_executive_responsible_pt_br => /admin/universo-de-empresas/executivos/:id(.:format)
  admin_executive_responsible_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_executive_responsibles_en => /en/admin/universo-de-empresas/executivos(.:format)
  admin_executive_responsibles_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_executive_responsibles_es => /es/admin/universo-de-empresas/executivos(.:format)
  admin_executive_responsibles_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_executive_responsibles_pt_br => /admin/universo-de-empresas/executivos(.:format)
  admin_executive_responsibles_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_goals_executive_responsible_en => /en/admin/universo-de-empresas/metas-executivos/:id(.:format)
  admin_goals_executive_responsible_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"metas-executivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_goals_executive_responsible_es => /es/admin/universo-de-empresas/metas-executivos/:id(.:format)
  admin_goals_executive_responsible_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"metas-executivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_goals_executive_responsible_pt_br => /admin/universo-de-empresas/metas-executivos/:id(.:format)
  admin_goals_executive_responsible_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"metas-executivos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_goals_executive_responsibles_en => /en/admin/universo-de-empresas/metas-executivos(.:format)
  admin_goals_executive_responsibles_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"metas-executivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_goals_executive_responsibles_es => /es/admin/universo-de-empresas/metas-executivos(.:format)
  admin_goals_executive_responsibles_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"metas-executivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_goals_executive_responsibles_pt_br => /admin/universo-de-empresas/metas-executivos(.:format)
  admin_goals_executive_responsibles_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"metas-executivos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chain_en => /en/admin/redes-hoteleiras/:id(.:format)
  admin_hotel_chain_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chain_es => /es/admin/redes-hoteleiras/:id(.:format)
  admin_hotel_chain_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chain_pt_br => /admin/redes-hoteleiras/:id(.:format)
  admin_hotel_chain_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chains_en => /en/admin/redes-hoteleiras(.:format)
  admin_hotel_chains_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chains_es => /es/admin/redes-hoteleiras(.:format)
  admin_hotel_chains_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chains_pt_br => /admin/redes-hoteleiras(.:format)
  admin_hotel_chains_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_en => /en/admin/hoteis/:id(.:format)
  admin_hotel_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_es => /es/admin/hoteis/:id(.:format)
  admin_hotel_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_en => /en/admin/hoteis/:hotel_id/salas-de-reuniao/:id(.:format)
  admin_hotel_meeting_room_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_es => /es/admin/hoteis/:hotel_id/salas-de-reuniao/:id(.:format)
  admin_hotel_meeting_room_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_pricing_policy_en => /en/admin/salas-de-reuniao/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_meeting_room_pricing_policy_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_pricing_policy_es => /es/admin/salas-de-reuniao/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_meeting_room_pricing_policy_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_pricing_policy_pt_br => /admin/salas-de-reuniao/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_meeting_room_pricing_policy_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_pt_br => /admin/hoteis/:hotel_id/salas-de-reuniao/:id(.:format)
  admin_hotel_meeting_room_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_meeting_offers_price_show_en => /en/admin/salas-de-reuniao/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_meeting_offers_price_show_en_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_meeting_offers_price_show_es => /es/admin/salas-de-reuniao/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_meeting_offers_price_show_es_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_meeting_offers_price_show_pt_br => /admin/salas-de-reuniao/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_meeting_offers_price_show_pt_br_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_offers_price_show_en => /en/admin/quartos/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_offers_price_show_en_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_offers_price_show_es => /es/admin/quartos/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_offers_price_show_es_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_offers_price_show_pt_br => /admin/quartos/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_offers_price_show_pt_br_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_pricing_policy_en => /en/admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_pricing_policy_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_pricing_policy_es => /es/admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_pricing_policy_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_pricing_policy_pt_br => /admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_pricing_policy_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_pt_br => /admin/hoteis/:id(.:format)
  admin_hotel_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_reports_en => /en/admin/relatorios(.:format)
  admin_hotel_reports_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_reports_es => /es/admin/relatorios(.:format)
  admin_hotel_reports_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_reports_pt_br => /admin/relatorios(.:format)
  admin_hotel_reports_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_room_type_en => /en/admin/hoteis/:hotel_id/tipos-de-quartos/:id(.:format)
  admin_hotel_room_type_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_room_type_es => /es/admin/hoteis/:hotel_id/tipos-de-quartos/:id(.:format)
  admin_hotel_room_type_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_room_type_pt_br => /admin/hoteis/:hotel_id/tipos-de-quartos/:id(.:format)
  admin_hotel_room_type_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_search_logs_en => /en/admin/logs-de-busca(.:format)
  admin_hotel_search_logs_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"logs-de-busca",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_search_logs_es => /es/admin/logs-de-busca(.:format)
  admin_hotel_search_logs_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"logs-de-busca",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_search_logs_pt_br => /admin/logs-de-busca(.:format)
  admin_hotel_search_logs_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"logs-de-busca",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotels_en => /en/admin/hoteis(.:format)
  admin_hotels_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotels_es => /es/admin/hoteis(.:format)
  admin_hotels_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotels_pt_br => /admin/hoteis(.:format)
  admin_hotels_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hq_consultant_en => /en/admin/consultores/:id(.:format)
  admin_hq_consultant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hq_consultant_es => /es/admin/consultores/:id(.:format)
  admin_hq_consultant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hq_consultant_pt_br => /admin/consultores/:id(.:format)
  admin_hq_consultant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hq_consultants_en => /en/admin/consultores(.:format)
  admin_hq_consultants_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hq_consultants_es => /es/admin/consultores(.:format)
  admin_hq_consultants_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hq_consultants_pt_br => /admin/consultores(.:format)
  admin_hq_consultants_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_en => /en/admin/managers/:id(.:format)
  admin_manager_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_es => /es/admin/managers/:id(.:format)
  admin_manager_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_password_en => /en/admin/password(.:format)
  admin_manager_password_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"password",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_password_es => /es/admin/contrase%C3%B1a(.:format)
  admin_manager_password_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contrase%C3%B1a",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_password_pt_br => /admin/senha(.:format)
  admin_manager_password_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_pt_br => /admin/managers/:id(.:format)
  admin_manager_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_session_en => /en/admin/entrar(.:format)
  admin_manager_session_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_session_es => /es/admin/entrar(.:format)
  admin_manager_session_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_manager_session_pt_br => /admin/entrar(.:format)
  admin_manager_session_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_managers_en => /en/admin/managers(.:format)
  admin_managers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_managers_es => /es/admin/managers(.:format)
  admin_managers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_managers_pt_br => /admin/managers(.:format)
  admin_managers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_member_get_member_config_en => /en/admin/member_get_member_config/:id(.:format)
  admin_member_get_member_config_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"member_get_member_config",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_member_get_member_config_es => /es/admin/member_get_member_config/:id(.:format)
  admin_member_get_member_config_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"member_get_member_config",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_member_get_member_config_pt_br => /admin/member_get_member_config/:id(.:format)
  admin_member_get_member_config_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"member_get_member_config",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_newsletters_en => /en/admin/newsletters(.:format)
  admin_newsletters_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_newsletters_es => /es/admin/newsletters(.:format)
  admin_newsletters_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_newsletters_pt_br => /admin/newsletters(.:format)
  admin_newsletters_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_offers_en => /en/admin/ofertas(.:format)
  admin_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_offers_es => /es/admin/ofertas(.:format)
  admin_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_offers_pt_br => /admin/ofertas(.:format)
  admin_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_payment_en => /en/admin/pagamentos/:id(.:format)
  admin_payment_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"pagamentos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_payment_es => /es/admin/pagamentos/:id(.:format)
  admin_payment_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"pagamentos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_payment_pt_br => /admin/pagamentos/:id(.:format)
  admin_payment_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"pagamentos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_payments_en => /en/admin/pagamentos(.:format)
  admin_payments_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"pagamentos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_payments_es => /es/admin/pagamentos(.:format)
  admin_payments_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"pagamentos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_payments_pt_br => /admin/pagamentos(.:format)
  admin_payments_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"pagamentos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_place_en => /en/admin/lugares/:id(.:format)
  admin_place_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_place_es => /es/admin/lugares/:id(.:format)
  admin_place_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_place_pt_br => /admin/lugares/:id(.:format)
  admin_place_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_places_en => /en/admin/lugares(.:format)
  admin_places_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_places_es => /es/admin/lugares(.:format)
  admin_places_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_places_pt_br => /admin/lugares(.:format)
  admin_places_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_possible_partner_contact_en => /en/admin/contatos-de-possiveis-parceiros/:id(.:format)
  admin_possible_partner_contact_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_possible_partner_contact_es => /es/admin/contatos-de-possiveis-parceiros/:id(.:format)
  admin_possible_partner_contact_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_possible_partner_contact_pt_br => /admin/contatos-de-possiveis-parceiros/:id(.:format)
  admin_possible_partner_contact_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_possible_partner_contacts_en => /en/admin/contatos-de-possiveis-parceiros(.:format)
  admin_possible_partner_contacts_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_possible_partner_contacts_es => /es/admin/contatos-de-possiveis-parceiros(.:format)
  admin_possible_partner_contacts_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_possible_partner_contacts_pt_br => /admin/contatos-de-possiveis-parceiros(.:format)
  admin_possible_partner_contacts_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_promotional_code_en => /en/admin/codigos-promocionais/:id(.:format)
  admin_promotional_code_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_promotional_code_es => /es/admin/codigos-promocionais/:id(.:format)
  admin_promotional_code_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_promotional_code_pt_br => /admin/codigos-promocionais/:id(.:format)
  admin_promotional_code_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_promotional_codes_en => /en/admin/codigos-promocionais(.:format)
  admin_promotional_codes_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_promotional_codes_es => /es/admin/codigos-promocionais(.:format)
  admin_promotional_codes_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_promotional_codes_pt_br => /admin/codigos-promocionais(.:format)
  admin_promotional_codes_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_pt_br => /admin/mudar-senha(.:format)
  admin_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_root_en => /en/admin(.:format)
  admin_root_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_root_es => /es/admin(.:format)
  admin_root_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_root_pt_br => /admin(.:format)
  admin_root_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"admin",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_state_en => /en/admin/cidades-e-estados/:id(.:format)
  admin_state_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_state_es => /es/admin/cidades-e-estados/:id(.:format)
  admin_state_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_state_pt_br => /admin/cidades-e-estados/:id(.:format)
  admin_state_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_states_en => /en/admin/cidades-e-estados(.:format)
  admin_states_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_states_es => /es/admin/cidades-e-estados(.:format)
  admin_states_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_states_pt_br => /admin/cidades-e-estados(.:format)
  admin_states_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_user_booking_show_en => /en/admin/usuarios/detalhes_da_reserva/:id(.:format)
  admin_user_booking_show_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[6,"detalhes_da_reserva",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_user_booking_show_es => /es/admin/usuarios/detalhes_da_reserva/:id(.:format)
  admin_user_booking_show_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[6,"detalhes_da_reserva",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_user_booking_show_pt_br => /admin/usuarios/detalhes_da_reserva/:id(.:format)
  admin_user_booking_show_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[6,"detalhes_da_reserva",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_user_en => /en/admin/usuarios/:id(.:format)
  admin_user_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_user_es => /es/admin/usuarios/:id(.:format)
  admin_user_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_user_pt_br => /admin/usuarios/:id(.:format)
  admin_user_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_users_en => /en/admin/usuarios(.:format)
  admin_users_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_users_es => /es/admin/usuarios(.:format)
  admin_users_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_users_pt_br => /admin/usuarios(.:format)
  admin_users_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// airports_admin_places_en => /en/admin/lugares/airports(.:format)
  airports_admin_places_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"airports",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// airports_admin_places_es => /es/admin/lugares/airports(.:format)
  airports_admin_places_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"airports",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// airports_admin_places_pt_br => /admin/lugares/airports(.:format)
  airports_admin_places_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"airports",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// authenticate_as_admin_hotel_en => /en/admin/hoteis/:id/authenticate_as(.:format)
  authenticate_as_admin_hotel_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"authenticate_as",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// authenticate_as_admin_hotel_es => /es/admin/hoteis/:id/authenticate_as(.:format)
  authenticate_as_admin_hotel_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"authenticate_as",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// authenticate_as_admin_hotel_pt_br => /admin/hoteis/:id/authenticate_as(.:format)
  authenticate_as_admin_hotel_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"authenticate_as",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// cancel_admin_booking_en => /en/admin/reservas/:id/cancel(.:format)
  cancel_admin_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"cancel",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// cancel_admin_booking_es => /es/admin/reservas/:id/cancelar(.:format)
  cancel_admin_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"cancelar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// cancel_admin_booking_pt_br => /admin/reservas/:id/cancelar(.:format)
  cancel_admin_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"cancelar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// cities_admin_places_en => /en/admin/lugares/cities(.:format)
  cities_admin_places_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"cities",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// cities_admin_places_es => /es/admin/lugares/cities(.:format)
  cities_admin_places_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"cities",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// cities_admin_places_pt_br => /admin/lugares/cities(.:format)
  cities_admin_places_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"cities",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_affiliate_en => /en/admin/afiliados/:id/desativar(.:format)
  deactivate_admin_affiliate_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_affiliate_es => /es/admin/afiliados/:id/desativar(.:format)
  deactivate_admin_affiliate_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_affiliate_pt_br => /admin/afiliados/:id/desativar(.:format)
  deactivate_admin_affiliate_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_agency_en => /en/admin/agencias/:id/desativar(.:format)
  deactivate_admin_agency_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_agency_es => /es/admin/agencias/:id/desativar(.:format)
  deactivate_admin_agency_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_agency_pt_br => /admin/agencias/:id/desativar(.:format)
  deactivate_admin_agency_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/:id/desativar(.:format)
  deactivate_admin_company_attendant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/:id/desativar(.:format)
  deactivate_admin_company_attendant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/:id/desativar(.:format)
  deactivate_admin_company_attendant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_region_en => /en/admin/universo-de-empresas/regioes/:id/desativar(.:format)
  deactivate_admin_company_region_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_region_es => /es/admin/universo-de-empresas/regioes/:id/desativar(.:format)
  deactivate_admin_company_region_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_region_pt_br => /admin/universo-de-empresas/regioes/:id/desativar(.:format)
  deactivate_admin_company_region_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_universe_en => /en/admin/universo-de-empresas/empresas/:id/desativar(.:format)
  deactivate_admin_company_universe_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_universe_es => /es/admin/universo-de-empresas/empresas/:id/desativar(.:format)
  deactivate_admin_company_universe_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/:id/desativar(.:format)
  deactivate_admin_company_universe_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_executive_responsible_en => /en/admin/universo-de-empresas/executivos/:id/desativar(.:format)
  deactivate_admin_executive_responsible_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_executive_responsible_es => /es/admin/universo-de-empresas/executivos/:id/desativar(.:format)
  deactivate_admin_executive_responsible_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_executive_responsible_pt_br => /admin/universo-de-empresas/executivos/:id/desativar(.:format)
  deactivate_admin_executive_responsible_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_hq_consultant_en => /en/admin/consultores/:id/desativar(.:format)
  deactivate_admin_hq_consultant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_hq_consultant_es => /es/admin/consultores/:id/desativar(.:format)
  deactivate_admin_hq_consultant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_hq_consultant_pt_br => /admin/consultores/:id/desativar(.:format)
  deactivate_admin_hq_consultant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_manager_en => /en/admin/managers/:id/desativar(.:format)
  deactivate_admin_manager_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_manager_es => /es/admin/managers/:id/desativar(.:format)
  deactivate_admin_manager_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_manager_pt_br => /admin/managers/:id/desativar(.:format)
  deactivate_admin_manager_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_places_en => /en/admin/lugares/desativar(.:format)
  deactivate_admin_places_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_places_es => /es/admin/lugares/desativar(.:format)
  deactivate_admin_places_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// deactivate_admin_places_pt_br => /admin/lugares/desativar(.:format)
  deactivate_admin_places_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"desativar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_admin_manager_session_en => /en/admin/sair(.:format)
  destroy_admin_manager_session_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"sair",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_admin_manager_session_es => /es/admin/sair(.:format)
  destroy_admin_manager_session_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"sair",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_admin_manager_session_pt_br => /admin/sair(.:format)
  destroy_admin_manager_session_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"sair",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_affiliate_en => /en/admin/afiliados/:id/editar(.:format)
  edit_admin_affiliate_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_affiliate_es => /es/admin/afiliados/:id/editar(.:format)
  edit_admin_affiliate_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_affiliate_pt_br => /admin/afiliados/:id/editar(.:format)
  edit_admin_affiliate_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_agency_en => /en/admin/agencias/:id/editar(.:format)
  edit_admin_agency_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_agency_es => /es/admin/agencias/:id/editar(.:format)
  edit_admin_agency_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_agency_pt_br => /admin/agencias/:id/editar(.:format)
  edit_admin_agency_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_booking_pack_en => /en/admin/banco-de-horas/:id/editar(.:format)
  edit_admin_booking_pack_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_booking_pack_es => /es/admin/banco-de-horas/:id/editar(.:format)
  edit_admin_booking_pack_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_booking_pack_pt_br => /admin/banco-de-horas/:id/editar(.:format)
  edit_admin_booking_pack_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_booking_tax_en => /en/admin/taxas/:id/editar(.:format)
  edit_admin_booking_tax_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_booking_tax_es => /es/admin/taxas/:id/editar(.:format)
  edit_admin_booking_tax_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_booking_tax_pt_br => /admin/taxas/:id/editar(.:format)
  edit_admin_booking_tax_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"taxas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_campaign_en => /en/admin/campanhas/:id/editar(.:format)
  edit_admin_campaign_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_campaign_es => /es/admin/campanhas/:id/editar(.:format)
  edit_admin_campaign_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_campaign_pt_br => /admin/campanhas/:id/editar(.:format)
  edit_admin_campaign_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/:id/editar(.:format)
  edit_admin_company_attendant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/:id/editar(.:format)
  edit_admin_company_attendant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/:id/editar(.:format)
  edit_admin_company_attendant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_detail_en => /en/admin/empresas/:id/editar(.:format)
  edit_admin_company_detail_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_detail_es => /es/admin/empresas/:id/editar(.:format)
  edit_admin_company_detail_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_detail_pt_br => /admin/empresas/:id/editar(.:format)
  edit_admin_company_detail_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_region_en => /en/admin/universo-de-empresas/regioes/:id/editar(.:format)
  edit_admin_company_region_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_region_es => /es/admin/universo-de-empresas/regioes/:id/editar(.:format)
  edit_admin_company_region_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_region_pt_br => /admin/universo-de-empresas/regioes/:id/editar(.:format)
  edit_admin_company_region_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_remuneration_en => /en/admin/universo-de-empresas/remuneracao/:id/editar(.:format)
  edit_admin_company_remuneration_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_remuneration_es => /es/admin/universo-de-empresas/remuneracao/:id/editar(.:format)
  edit_admin_company_remuneration_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_remuneration_pt_br => /admin/universo-de-empresas/remuneracao/:id/editar(.:format)
  edit_admin_company_remuneration_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"remuneracao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_universe_en => /en/admin/universo-de-empresas/empresas/:id/editar(.:format)
  edit_admin_company_universe_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_universe_es => /es/admin/universo-de-empresas/empresas/:id/editar(.:format)
  edit_admin_company_universe_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/:id/editar(.:format)
  edit_admin_company_universe_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_currency_en => /en/admin/moedas/:id/editar(.:format)
  edit_admin_currency_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_currency_es => /es/admin/moedas/:id/editar(.:format)
  edit_admin_currency_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_currency_pt_br => /admin/moedas/:id/editar(.:format)
  edit_admin_currency_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_emkt_en => /en/admin/emkt/:id/editar(.:format)
  edit_admin_emkt_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_emkt_es => /es/admin/emkt/:id/editar(.:format)
  edit_admin_emkt_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_emkt_pt_br => /admin/emkt/:id/editar(.:format)
  edit_admin_emkt_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_executive_responsible_en => /en/admin/universo-de-empresas/executivos/:id/editar(.:format)
  edit_admin_executive_responsible_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_executive_responsible_es => /es/admin/universo-de-empresas/executivos/:id/editar(.:format)
  edit_admin_executive_responsible_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_executive_responsible_pt_br => /admin/universo-de-empresas/executivos/:id/editar(.:format)
  edit_admin_executive_responsible_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_chain_en => /en/admin/redes-hoteleiras/:id/editar(.:format)
  edit_admin_hotel_chain_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_chain_es => /es/admin/redes-hoteleiras/:id/editar(.:format)
  edit_admin_hotel_chain_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_chain_pt_br => /admin/redes-hoteleiras/:id/editar(.:format)
  edit_admin_hotel_chain_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_en => /en/admin/hoteis/:id/editar(.:format)
  edit_admin_hotel_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_es => /es/admin/hoteis/:id/editar(.:format)
  edit_admin_hotel_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_meeting_room_en => /en/admin/hoteis/:hotel_id/salas-de-reuniao/:id/editar(.:format)
  edit_admin_hotel_meeting_room_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_meeting_room_es => /es/admin/hoteis/:hotel_id/salas-de-reuniao/:id/editar(.:format)
  edit_admin_hotel_meeting_room_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_meeting_room_pt_br => /admin/hoteis/:hotel_id/salas-de-reuniao/:id/editar(.:format)
  edit_admin_hotel_meeting_room_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_pricing_policy_en => /en/admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id/editar(.:format)
  edit_admin_hotel_pricing_policy_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_pricing_policy_es => /es/admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id/editar(.:format)
  edit_admin_hotel_pricing_policy_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_pricing_policy_pt_br => /admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id/editar(.:format)
  edit_admin_hotel_pricing_policy_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_pt_br => /admin/hoteis/:id/editar(.:format)
  edit_admin_hotel_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_room_type_en => /en/admin/hoteis/:hotel_id/tipos-de-quartos/:id/editar(.:format)
  edit_admin_hotel_room_type_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_room_type_es => /es/admin/hoteis/:hotel_id/tipos-de-quartos/:id/editar(.:format)
  edit_admin_hotel_room_type_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_room_type_pt_br => /admin/hoteis/:hotel_id/tipos-de-quartos/:id/editar(.:format)
  edit_admin_hotel_room_type_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hq_consultant_en => /en/admin/consultores/:id/editar(.:format)
  edit_admin_hq_consultant_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hq_consultant_es => /es/admin/consultores/:id/editar(.:format)
  edit_admin_hq_consultant_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hq_consultant_pt_br => /admin/consultores/:id/editar(.:format)
  edit_admin_hq_consultant_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_manager_en => /en/admin/managers/:id/editar(.:format)
  edit_admin_manager_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_manager_es => /es/admin/managers/:id/editar(.:format)
  edit_admin_manager_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_manager_password_en => /en/admin/password/editar(.:format)
  edit_admin_manager_password_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_manager_password_es => /es/admin/contrase%C3%B1a/editar(.:format)
  edit_admin_manager_password_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contrase%C3%B1a",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_manager_password_pt_br => /admin/senha/editar(.:format)
  edit_admin_manager_password_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"senha",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_manager_pt_br => /admin/managers/:id/editar(.:format)
  edit_admin_manager_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_member_get_member_config_en => /en/admin/member_get_member_config/:id/editar(.:format)
  edit_admin_member_get_member_config_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"member_get_member_config",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_member_get_member_config_es => /es/admin/member_get_member_config/:id/editar(.:format)
  edit_admin_member_get_member_config_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"member_get_member_config",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_member_get_member_config_pt_br => /admin/member_get_member_config/:id/editar(.:format)
  edit_admin_member_get_member_config_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"member_get_member_config",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_place_en => /en/admin/lugares/:id/editar(.:format)
  edit_admin_place_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_place_es => /es/admin/lugares/:id/editar(.:format)
  edit_admin_place_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_place_pt_br => /admin/lugares/:id/editar(.:format)
  edit_admin_place_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_promotional_code_en => /en/admin/codigos-promocionais/:id/editar(.:format)
  edit_admin_promotional_code_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_promotional_code_es => /es/admin/codigos-promocionais/:id/editar(.:format)
  edit_admin_promotional_code_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_promotional_code_pt_br => /admin/codigos-promocionais/:id/editar(.:format)
  edit_admin_promotional_code_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_state_en => /en/admin/cidades-e-estados/:id/editar(.:format)
  edit_admin_state_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_state_es => /es/admin/cidades-e-estados/:id/editar(.:format)
  edit_admin_state_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_state_pt_br => /admin/cidades-e-estados/:id/editar(.:format)
  edit_admin_state_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_user_en => /en/admin/usuarios/:id/editar(.:format)
  edit_admin_user_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_user_es => /es/admin/usuarios/:id/editar(.:format)
  edit_admin_user_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_user_pt_br => /admin/usuarios/:id/editar(.:format)
  edit_admin_user_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"usuarios",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// generate_token_admin_affiliate_en => /en/admin/afiliados/:id/generate_token(.:format)
  generate_token_admin_affiliate_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"generate_token",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// generate_token_admin_affiliate_es => /es/admin/afiliados/:id/generate_token(.:format)
  generate_token_admin_affiliate_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"generate_token",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// generate_token_admin_affiliate_pt_br => /admin/afiliados/:id/generate_token(.:format)
  generate_token_admin_affiliate_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"generate_token",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// generate_webservice_token_admin_affiliate_en => /en/admin/afiliados/:id/generate_webservice_token(.:format)
  generate_webservice_token_admin_affiliate_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"generate_webservice_token",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// generate_webservice_token_admin_affiliate_es => /es/admin/afiliados/:id/generate_webservice_token(.:format)
  generate_webservice_token_admin_affiliate_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"generate_webservice_token",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// generate_webservice_token_admin_affiliate_pt_br => /admin/afiliados/:id/generate_webservice_token(.:format)
  generate_webservice_token_admin_affiliate_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"generate_webservice_token",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_admin_booking_en => /en/admin/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_admin_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_admin_booking_es => /es/admin/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_admin_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_admin_booking_pt_br => /admin/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_admin_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_admin_booking_en => /en/admin/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_admin_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_admin_booking_es => /es/admin/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_admin_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_admin_booking_pt_br => /admin/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_admin_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// import_admin_newsletters_en => /en/admin/newsletters/import(.:format)
  import_admin_newsletters_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[7,"/",false]],[6,"import",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// import_admin_newsletters_es => /es/admin/newsletters/import(.:format)
  import_admin_newsletters_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[7,"/",false]],[6,"import",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// import_admin_newsletters_pt_br => /admin/newsletters/import(.:format)
  import_admin_newsletters_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[7,"/",false]],[6,"import",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// import_csv_admin_newsletters_en => /en/admin/newsletters/import_csv(.:format)
  import_csv_admin_newsletters_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[7,"/",false]],[6,"import_csv",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// import_csv_admin_newsletters_es => /es/admin/newsletters/import_csv(.:format)
  import_csv_admin_newsletters_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[7,"/",false]],[6,"import_csv",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// import_csv_admin_newsletters_pt_br => /admin/newsletters/import_csv(.:format)
  import_csv_admin_newsletters_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"newsletters",false]],[7,"/",false]],[6,"import_csv",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// mark_all_messages_as_read_admin_possible_partner_contacts_en => /en/admin/contatos-de-possiveis-parceiros/mark_all_messages_as_read(.:format)
  mark_all_messages_as_read_admin_possible_partner_contacts_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[7,"/",false]],[6,"mark_all_messages_as_read",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// mark_all_messages_as_read_admin_possible_partner_contacts_es => /es/admin/contatos-de-possiveis-parceiros/mark_all_messages_as_read(.:format)
  mark_all_messages_as_read_admin_possible_partner_contacts_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[7,"/",false]],[6,"mark_all_messages_as_read",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// mark_all_messages_as_read_admin_possible_partner_contacts_pt_br => /admin/contatos-de-possiveis-parceiros/mark_all_messages_as_read(.:format)
  mark_all_messages_as_read_admin_possible_partner_contacts_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"contatos-de-possiveis-parceiros",false]],[7,"/",false]],[6,"mark_all_messages_as_read",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_affiliate_en => /en/admin/afiliados/novo(.:format)
  new_admin_affiliate_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_affiliate_es => /es/admin/afiliados/novo(.:format)
  new_admin_affiliate_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_affiliate_pt_br => /admin/afiliados/novo(.:format)
  new_admin_affiliate_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"afiliados",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_agency_en => /en/admin/agencias/novo(.:format)
  new_admin_agency_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_agency_es => /es/admin/agencias/novo(.:format)
  new_admin_agency_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_agency_pt_br => /admin/agencias/novo(.:format)
  new_admin_agency_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"agencias",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_booking_pack_en => /en/admin/banco-de-horas/novo(.:format)
  new_admin_booking_pack_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_booking_pack_es => /es/admin/banco-de-horas/novo(.:format)
  new_admin_booking_pack_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_booking_pack_pt_br => /admin/banco-de-horas/novo(.:format)
  new_admin_booking_pack_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"banco-de-horas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_campaign_en => /en/admin/campanhas/novo(.:format)
  new_admin_campaign_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_campaign_es => /es/admin/campanhas/novo(.:format)
  new_admin_campaign_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_campaign_pt_br => /admin/campanhas/novo(.:format)
  new_admin_campaign_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"campanhas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_city_en => /en/admin/cidades/novo(.:format)
  new_admin_city_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_city_es => /es/admin/cidades/novo(.:format)
  new_admin_city_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_city_pt_br => /admin/cidades/novo(.:format)
  new_admin_city_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"cidades",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_attendant_en => /en/admin/universo-de-empresas/atendimento-empresas/novo(.:format)
  new_admin_company_attendant_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_attendant_es => /es/admin/universo-de-empresas/atendimento-empresas/novo(.:format)
  new_admin_company_attendant_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_attendant_pt_br => /admin/universo-de-empresas/atendimento-empresas/novo(.:format)
  new_admin_company_attendant_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"atendimento-empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_region_en => /en/admin/universo-de-empresas/regioes/novo(.:format)
  new_admin_company_region_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_region_es => /es/admin/universo-de-empresas/regioes/novo(.:format)
  new_admin_company_region_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_region_pt_br => /admin/universo-de-empresas/regioes/novo(.:format)
  new_admin_company_region_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"regioes",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_universe_en => /en/admin/universo-de-empresas/empresas/novo(.:format)
  new_admin_company_universe_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_universe_es => /es/admin/universo-de-empresas/empresas/novo(.:format)
  new_admin_company_universe_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_company_universe_pt_br => /admin/universo-de-empresas/empresas/novo(.:format)
  new_admin_company_universe_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"empresas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_currency_en => /en/admin/moedas/novo(.:format)
  new_admin_currency_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_currency_es => /es/admin/moedas/novo(.:format)
  new_admin_currency_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_currency_pt_br => /admin/moedas/novo(.:format)
  new_admin_currency_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_emkt_en => /en/admin/emkt/novo(.:format)
  new_admin_emkt_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_emkt_es => /es/admin/emkt/novo(.:format)
  new_admin_emkt_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_emkt_pt_br => /admin/emkt/novo(.:format)
  new_admin_emkt_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_executive_responsible_en => /en/admin/universo-de-empresas/executivos/novo(.:format)
  new_admin_executive_responsible_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_executive_responsible_es => /es/admin/universo-de-empresas/executivos/novo(.:format)
  new_admin_executive_responsible_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_executive_responsible_pt_br => /admin/universo-de-empresas/executivos/novo(.:format)
  new_admin_executive_responsible_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"universo-de-empresas",false]],[7,"/",false]],[6,"executivos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_chain_en => /en/admin/redes-hoteleiras/novo(.:format)
  new_admin_hotel_chain_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_chain_es => /es/admin/redes-hoteleiras/novo(.:format)
  new_admin_hotel_chain_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_chain_pt_br => /admin/redes-hoteleiras/novo(.:format)
  new_admin_hotel_chain_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_en => /en/admin/hoteis/novo(.:format)
  new_admin_hotel_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_es => /es/admin/hoteis/novo(.:format)
  new_admin_hotel_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_pt_br => /admin/hoteis/novo(.:format)
  new_admin_hotel_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hq_consultant_en => /en/admin/consultores/novo(.:format)
  new_admin_hq_consultant_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hq_consultant_es => /es/admin/consultores/novo(.:format)
  new_admin_hq_consultant_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hq_consultant_pt_br => /admin/consultores/novo(.:format)
  new_admin_hq_consultant_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"consultores",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_en => /en/admin/managers/novo(.:format)
  new_admin_manager_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_es => /es/admin/managers/novo(.:format)
  new_admin_manager_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_password_en => /en/admin/password/novo(.:format)
  new_admin_manager_password_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_password_es => /es/admin/contrase%C3%B1a/novo(.:format)
  new_admin_manager_password_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"contrase%C3%B1a",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_password_pt_br => /admin/senha/novo(.:format)
  new_admin_manager_password_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"senha",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_pt_br => /admin/managers/novo(.:format)
  new_admin_manager_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"managers",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_session_en => /en/admin/entrar(.:format)
  new_admin_manager_session_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_session_es => /es/admin/entrar(.:format)
  new_admin_manager_session_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_manager_session_pt_br => /admin/entrar(.:format)
  new_admin_manager_session_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_place_en => /en/admin/lugares/novo(.:format)
  new_admin_place_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_place_es => /es/admin/lugares/novo(.:format)
  new_admin_place_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_place_pt_br => /admin/lugares/novo(.:format)
  new_admin_place_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_promotional_code_en => /en/admin/codigos-promocionais/novo(.:format)
  new_admin_promotional_code_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_promotional_code_es => /es/admin/codigos-promocionais/novo(.:format)
  new_admin_promotional_code_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_promotional_code_pt_br => /admin/codigos-promocionais/novo(.:format)
  new_admin_promotional_code_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"codigos-promocionais",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_state_en => /en/admin/cidades-e-estados/novo(.:format)
  new_admin_state_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_state_es => /es/admin/cidades-e-estados/novo(.:format)
  new_admin_state_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_state_pt_br => /admin/cidades-e-estados/novo(.:format)
  new_admin_state_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"cidades-e-estados",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// regions_admin_places_en => /en/admin/lugares/regions(.:format)
  regions_admin_places_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"regions",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// regions_admin_places_es => /es/admin/lugares/regions(.:format)
  regions_admin_places_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"regions",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// regions_admin_places_pt_br => /admin/lugares/regions(.:format)
  regions_admin_places_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"lugares",false]],[7,"/",false]],[6,"regions",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// send_confirmation_notice_email_admin_booking_en => /en/admin/reservas/:id/send_confirmation_notice_email(.:format)
  send_confirmation_notice_email_admin_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"send_confirmation_notice_email",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// send_confirmation_notice_email_admin_booking_es => /es/admin/reservas/:id/send_confirmation_notice_email(.:format)
  send_confirmation_notice_email_admin_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"send_confirmation_notice_email",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// send_confirmation_notice_email_admin_booking_pt_br => /admin/reservas/:id/send_confirmation_notice_email(.:format)
  send_confirmation_notice_email_admin_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"send_confirmation_notice_email",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// update_all_admin_currencies_en => /en/admin/moedas/update_all(.:format)
  update_all_admin_currencies_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[6,"update_all",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// update_all_admin_currencies_es => /es/admin/moedas/update_all(.:format)
  update_all_admin_currencies_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[6,"update_all",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// update_all_admin_currencies_pt_br => /admin/moedas/update_all(.:format)
  update_all_admin_currencies_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"moedas",false]],[7,"/",false]],[6,"update_all",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// with_bookings_admin_emkts_en => /en/admin/emkt/with_bookings(.:format)
  with_bookings_admin_emkts_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"with_bookings",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// with_bookings_admin_emkts_es => /es/admin/emkt/with_bookings(.:format)
  with_bookings_admin_emkts_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"with_bookings",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// with_bookings_admin_emkts_pt_br => /admin/emkt/with_bookings(.:format)
  with_bookings_admin_emkts_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"with_bookings",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// without_bookings_admin_emkts_en => /en/admin/emkt/without_bookings(.:format)
  without_bookings_admin_emkts_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"without_bookings",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// without_bookings_admin_emkts_es => /es/admin/emkt/without_bookings(.:format)
  without_bookings_admin_emkts_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"without_bookings",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// without_bookings_admin_emkts_pt_br => /admin/emkt/without_bookings(.:format)
  without_bookings_admin_emkts_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"emkt",false]],[7,"/",false]],[6,"without_bookings",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  }}
;

  window.Routes.options = defaults;

}).call(this);
