(function() {
  var NodeTypes, ParameterMissing, Utils, defaults,
    __hasProp = {}.hasOwnProperty;

  ParameterMissing = function(message) {
    this.message = message;
  };

  ParameterMissing.prototype = new Error();

  defaults = {
    prefix: "",
    default_url_options: {}
  };

  NodeTypes = {"GROUP":1,"CAT":2,"SYMBOL":3,"OR":4,"STAR":5,"LITERAL":6,"SLASH":7,"DOT":8};

  Utils = {
    serialize: function(object, prefix) {
      var element, i, key, prop, result, s, _i, _len;

      if (prefix == null) {
        prefix = null;
      }
      if (!object) {
        return "";
      }
      if (!prefix && !(this.get_object_type(object) === "object")) {
        throw new Error("Url parameters should be a javascript hash");
      }
      if (window.jQuery) {
        result = window.jQuery.param(object);
        return (!result ? "" : result);
      }
      s = [];
      switch (this.get_object_type(object)) {
        case "array":
          for (i = _i = 0, _len = object.length; _i < _len; i = ++_i) {
            element = object[i];
            s.push(this.serialize(element, prefix + "[]"));
          }
          break;
        case "object":
          for (key in object) {
            if (!__hasProp.call(object, key)) continue;
            prop = object[key];
            if (!(prop != null)) {
              continue;
            }
            if (prefix != null) {
              key = "" + prefix + "[" + key + "]";
            }
            s.push(this.serialize(prop, key));
          }
          break;
        default:
          if (object) {
            s.push("" + (encodeURIComponent(prefix.toString())) + "=" + (encodeURIComponent(object.toString())));
          }
      }
      if (!s.length) {
        return "";
      }
      return s.join("&");
    },
    clean_path: function(path) {
      var last_index;

      path = path.split("://");
      last_index = path.length - 1;
      path[last_index] = path[last_index].replace(/\/+/g, "/");
      return path.join("://");
    },
    set_default_url_options: function(optional_parts, options) {
      var i, part, _i, _len, _results;

      _results = [];
      for (i = _i = 0, _len = optional_parts.length; _i < _len; i = ++_i) {
        part = optional_parts[i];
        if (!options.hasOwnProperty(part) && defaults.default_url_options.hasOwnProperty(part)) {
          _results.push(options[part] = defaults.default_url_options[part]);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    },
    extract_anchor: function(options) {
      var anchor;

      anchor = "";
      if (options.hasOwnProperty("anchor")) {
        anchor = "#" + options.anchor;
        delete options.anchor;
      }
      return anchor;
    },
    extract_options: function(number_of_params, args) {
      var last_argument, type;

      last_argument = args[args.length - 1];
      type = this.get_object_type(last_argument);
      if (args.length > number_of_params || (type === "object" && !this.look_like_serialized_model(last_argument))) {
        return args.pop();
      } else {
        return {};
      }
    },
    look_like_serialized_model: function(object) {
      return "id" in object || "to_param" in object;
    },
    path_identifier: function(object) {
      var property;

      if (object === 0) {
        return "0";
      }
      if (!object) {
        return "";
      }
      property = object;
      if (this.get_object_type(object) === "object") {
        property = object.to_param || object.id || object;
        if (this.get_object_type(property) === "function") {
          property = property.call(object);
        }
      }
      return property.toString();
    },
    clone: function(obj) {
      var attr, copy, key;

      if ((obj == null) || "object" !== this.get_object_type(obj)) {
        return obj;
      }
      copy = obj.constructor();
      for (key in obj) {
        if (!__hasProp.call(obj, key)) continue;
        attr = obj[key];
        copy[key] = attr;
      }
      return copy;
    },
    prepare_parameters: function(required_parameters, actual_parameters, options) {
      var i, result, val, _i, _len;

      result = this.clone(options) || {};
      for (i = _i = 0, _len = required_parameters.length; _i < _len; i = ++_i) {
        val = required_parameters[i];
        if (i < actual_parameters.length) {
          result[val] = actual_parameters[i];
        }
      }
      return result;
    },
    build_path: function(required_parameters, optional_parts, route, args) {
      var anchor, opts, parameters, result, url, url_params;

      args = Array.prototype.slice.call(args);
      opts = this.extract_options(required_parameters.length, args);
      if (args.length > required_parameters.length) {
        throw new Error("Too many parameters provided for path");
      }
      parameters = this.prepare_parameters(required_parameters, args, opts);
      this.set_default_url_options(optional_parts, parameters);
      anchor = this.extract_anchor(parameters);
      result = "" + (this.get_prefix()) + (this.visit(route, parameters));
      url = Utils.clean_path("" + result);
      if ((url_params = this.serialize(parameters)).length) {
        url += "?" + url_params;
      }
      url += anchor;
      return url;
    },
    visit: function(route, parameters, optional) {
      var left, left_part, right, right_part, type, value;

      if (optional == null) {
        optional = false;
      }
      type = route[0], left = route[1], right = route[2];
      switch (type) {
        case NodeTypes.GROUP:
          return this.visit(left, parameters, true);
        case NodeTypes.STAR:
          return this.visit_globbing(left, parameters, true);
        case NodeTypes.LITERAL:
        case NodeTypes.SLASH:
        case NodeTypes.DOT:
          return left;
        case NodeTypes.CAT:
          left_part = this.visit(left, parameters, optional);
          right_part = this.visit(right, parameters, optional);
          if (optional && !(left_part && right_part)) {
            return "";
          }
          return "" + left_part + right_part;
        case NodeTypes.SYMBOL:
          value = parameters[left];
          if (value != null) {
            delete parameters[left];
            return this.path_identifier(value);
          }
          if (optional) {
            return "";
          } else {
            throw new ParameterMissing("Route parameter missing: " + left);
          }
          break;
        default:
          throw new Error("Unknown Rails node type");
      }
    },
    visit_globbing: function(route, parameters, optional) {
      var left, right, type, value;

      type = route[0], left = route[1], right = route[2];
      if (left.replace(/^\*/i, "") !== left) {
        route[1] = left = left.replace(/^\*/i, "");
      }
      value = parameters[left];
      if (value == null) {
        return this.visit(route, parameters, optional);
      }
      parameters[left] = (function() {
        switch (this.get_object_type(value)) {
          case "array":
            return value.join("/");
          default:
            return value;
        }
      }).call(this);
      return this.visit(route, parameters, optional);
    },
    get_prefix: function() {
      var prefix;

      prefix = defaults.prefix;
      if (prefix !== "") {
        prefix = (prefix.match("/$") ? prefix : "" + prefix + "/");
      }
      return prefix;
    },
    _classToTypeCache: null,
    _classToType: function() {
      var name, _i, _len, _ref;

      if (this._classToTypeCache != null) {
        return this._classToTypeCache;
      }
      this._classToTypeCache = {};
      _ref = "Boolean Number String Function Array Date RegExp Undefined Null".split(" ");
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        name = _ref[_i];
        this._classToTypeCache["[object " + name + "]"] = name.toLowerCase();
      }
      return this._classToTypeCache;
    },
    get_object_type: function(obj) {
      var strType;

      if (window.jQuery && (window.jQuery.type != null)) {
        return window.jQuery.type(obj);
      }
      strType = Object.prototype.toString.call(obj);
      return this._classToType()[strType] || "object";
    },
    namespace: function(root, namespaceString) {
      var current, parts;

      parts = (namespaceString ? namespaceString.split(".") : []);
      if (!parts.length) {
        return;
      }
      current = parts.shift();
      root[current] = root[current] || {};
      return Utils.namespace(root[current], parts.join("."));
    }
  };

  Utils.namespace(window, "Routes");

  window.Routes = {
// accuse_delayed_checkout_hotel_booking_en => /en/fornecedores/reservas/:id/accuse_delayed_checkout(.:format)
  accuse_delayed_checkout_hotel_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"accuse_delayed_checkout",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// accuse_delayed_checkout_hotel_booking_es => /es/fornecedores/reservas/:id/accuse_delayed_checkout(.:format)
  accuse_delayed_checkout_hotel_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"accuse_delayed_checkout",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// accuse_delayed_checkout_hotel_booking_pt_br => /fornecedores/reservas/:id/accuse_delayed_checkout(.:format)
  accuse_delayed_checkout_hotel_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"accuse_delayed_checkout",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chain_en => /en/admin/redes-hoteleiras/:id(.:format)
  admin_hotel_chain_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chain_es => /es/admin/redes-hoteleiras/:id(.:format)
  admin_hotel_chain_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chain_pt_br => /admin/redes-hoteleiras/:id(.:format)
  admin_hotel_chain_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chains_en => /en/admin/redes-hoteleiras(.:format)
  admin_hotel_chains_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chains_es => /es/admin/redes-hoteleiras(.:format)
  admin_hotel_chains_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_chains_pt_br => /admin/redes-hoteleiras(.:format)
  admin_hotel_chains_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_en => /en/admin/hoteis/:id(.:format)
  admin_hotel_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_es => /es/admin/hoteis/:id(.:format)
  admin_hotel_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_en => /en/admin/hoteis/:hotel_id/salas-de-reuniao/:id(.:format)
  admin_hotel_meeting_room_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_es => /es/admin/hoteis/:hotel_id/salas-de-reuniao/:id(.:format)
  admin_hotel_meeting_room_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_pricing_policy_en => /en/admin/salas-de-reuniao/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_meeting_room_pricing_policy_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_pricing_policy_es => /es/admin/salas-de-reuniao/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_meeting_room_pricing_policy_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_pricing_policy_pt_br => /admin/salas-de-reuniao/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_meeting_room_pricing_policy_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_meeting_room_pt_br => /admin/hoteis/:hotel_id/salas-de-reuniao/:id(.:format)
  admin_hotel_meeting_room_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_meeting_offers_price_show_en => /en/admin/salas-de-reuniao/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_meeting_offers_price_show_en_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_meeting_offers_price_show_es => /es/admin/salas-de-reuniao/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_meeting_offers_price_show_es_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_meeting_offers_price_show_pt_br => /admin/salas-de-reuniao/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_meeting_offers_price_show_pt_br_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_offers_price_show_en => /en/admin/quartos/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_offers_price_show_en_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_offers_price_show_es => /es/admin/quartos/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_offers_price_show_es_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_minimum_offers_price_show_pt_br => /admin/quartos/hoteis/:hotel_id/menor-preco-por-pacote(.:format)
  admin_hotel_minimum_offers_price_show_pt_br_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"menor-preco-por-pacote",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_pricing_policy_en => /en/admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_pricing_policy_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_pricing_policy_es => /es/admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_pricing_policy_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_pricing_policy_pt_br => /admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id(.:format)
  admin_hotel_pricing_policy_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_pt_br => /admin/hoteis/:id(.:format)
  admin_hotel_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_reports_en => /en/admin/relatorios(.:format)
  admin_hotel_reports_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_reports_es => /es/admin/relatorios(.:format)
  admin_hotel_reports_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_reports_pt_br => /admin/relatorios(.:format)
  admin_hotel_reports_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_room_type_en => /en/admin/hoteis/:hotel_id/tipos-de-quartos/:id(.:format)
  admin_hotel_room_type_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_room_type_es => /es/admin/hoteis/:hotel_id/tipos-de-quartos/:id(.:format)
  admin_hotel_room_type_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_room_type_pt_br => /admin/hoteis/:hotel_id/tipos-de-quartos/:id(.:format)
  admin_hotel_room_type_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_search_logs_en => /en/admin/logs-de-busca(.:format)
  admin_hotel_search_logs_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"logs-de-busca",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_search_logs_es => /es/admin/logs-de-busca(.:format)
  admin_hotel_search_logs_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"logs-de-busca",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// admin_hotel_search_logs_pt_br => /admin/logs-de-busca(.:format)
  admin_hotel_search_logs_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"logs-de-busca",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// authenticate_as_admin_hotel_en => /en/admin/hoteis/:id/authenticate_as(.:format)
  authenticate_as_admin_hotel_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"authenticate_as",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// authenticate_as_admin_hotel_es => /es/admin/hoteis/:id/authenticate_as(.:format)
  authenticate_as_admin_hotel_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"authenticate_as",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// authenticate_as_admin_hotel_pt_br => /admin/hoteis/:id/authenticate_as(.:format)
  authenticate_as_admin_hotel_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"authenticate_as",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// count_per_room_hotel_meeting_room_offers_en => /en/fornecedores/salas-de-reuniao/ofertas/count_per_room(.:format)
  count_per_room_hotel_meeting_room_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"count_per_room",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// count_per_room_hotel_meeting_room_offers_es => /es/fornecedores/salas-de-reuniao/ofertas/count_per_room(.:format)
  count_per_room_hotel_meeting_room_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"count_per_room",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// count_per_room_hotel_meeting_room_offers_pt_br => /fornecedores/salas-de-reuniao/ofertas/count_per_room(.:format)
  count_per_room_hotel_meeting_room_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"count_per_room",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// count_per_room_hotel_offers_en => /en/fornecedores/quartos/ofertas/count_per_room(.:format)
  count_per_room_hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"count_per_room",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// count_per_room_hotel_offers_es => /es/fornecedores/quartos/ofertas/count_per_room(.:format)
  count_per_room_hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"count_per_room",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// count_per_room_hotel_offers_pt_br => /fornecedores/quartos/ofertas/count_per_room(.:format)
  count_per_room_hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"count_per_room",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_hotel_hotel_session_en => /en/fornecedores/sair(.:format)
  destroy_hotel_hotel_session_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"sair",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_hotel_hotel_session_es => /es/fornecedores/sair(.:format)
  destroy_hotel_hotel_session_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"sair",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// destroy_hotel_hotel_session_pt_br => /fornecedores/sair(.:format)
  destroy_hotel_hotel_session_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"sair",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_chain_en => /en/admin/redes-hoteleiras/:id/editar(.:format)
  edit_admin_hotel_chain_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_chain_es => /es/admin/redes-hoteleiras/:id/editar(.:format)
  edit_admin_hotel_chain_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_chain_pt_br => /admin/redes-hoteleiras/:id/editar(.:format)
  edit_admin_hotel_chain_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_en => /en/admin/hoteis/:id/editar(.:format)
  edit_admin_hotel_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_es => /es/admin/hoteis/:id/editar(.:format)
  edit_admin_hotel_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_meeting_room_en => /en/admin/hoteis/:hotel_id/salas-de-reuniao/:id/editar(.:format)
  edit_admin_hotel_meeting_room_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_meeting_room_es => /es/admin/hoteis/:hotel_id/salas-de-reuniao/:id/editar(.:format)
  edit_admin_hotel_meeting_room_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_meeting_room_pt_br => /admin/hoteis/:hotel_id/salas-de-reuniao/:id/editar(.:format)
  edit_admin_hotel_meeting_room_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_pricing_policy_en => /en/admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id/editar(.:format)
  edit_admin_hotel_pricing_policy_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_pricing_policy_es => /es/admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id/editar(.:format)
  edit_admin_hotel_pricing_policy_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_pricing_policy_pt_br => /admin/quartos/hoteis/:hotel_id/tabelas-de-precos/:id/editar(.:format)
  edit_admin_hotel_pricing_policy_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tabelas-de-precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_pt_br => /admin/hoteis/:id/editar(.:format)
  edit_admin_hotel_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_room_type_en => /en/admin/hoteis/:hotel_id/tipos-de-quartos/:id/editar(.:format)
  edit_admin_hotel_room_type_en_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_room_type_es => /es/admin/hoteis/:hotel_id/tipos-de-quartos/:id/editar(.:format)
  edit_admin_hotel_room_type_es_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_admin_hotel_room_type_pt_br => /admin/hoteis/:hotel_id/tipos-de-quartos/:id/editar(.:format)
  edit_admin_hotel_room_type_pt_br_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"tipos-de-quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_hotel_password_en => /en/fornecedores/password/editar(.:format)
  edit_hotel_hotel_password_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_hotel_password_es => /es/fornecedores/contrase%C3%B1a/editar(.:format)
  edit_hotel_hotel_password_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"contrase%C3%B1a",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_hotel_password_pt_br => /fornecedores/senha/editar(.:format)
  edit_hotel_hotel_password_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"senha",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_meeting_room_en => /en/fornecedores/salas-de-reuniao/salas/:id/editar(.:format)
  edit_hotel_meeting_room_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_meeting_room_es => /es/fornecedores/salas-de-reuniao/salas/:id/editar(.:format)
  edit_hotel_meeting_room_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_meeting_room_pricing_policy_en => /en/fornecedores/salas-de-reuniao/tabelas_de_precos/:id/editar(.:format)
  edit_hotel_meeting_room_pricing_policy_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_meeting_room_pricing_policy_es => /es/fornecedores/salas-de-reuniao/tabelas_de_precos/:id/editar(.:format)
  edit_hotel_meeting_room_pricing_policy_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_meeting_room_pricing_policy_pt_br => /fornecedores/salas-de-reuniao/tabelas_de_precos/:id/editar(.:format)
  edit_hotel_meeting_room_pricing_policy_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_meeting_room_pt_br => /fornecedores/salas-de-reuniao/salas/:id/editar(.:format)
  edit_hotel_meeting_room_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_pricing_policy_en => /en/fornecedores/quartos/tabelas_de_precos/:id/editar(.:format)
  edit_hotel_pricing_policy_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_pricing_policy_es => /es/fornecedores/quartos/tabelas_de_precos/:id/editar(.:format)
  edit_hotel_pricing_policy_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_pricing_policy_pt_br => /fornecedores/quartos/tabelas_de_precos/:id/editar(.:format)
  edit_hotel_pricing_policy_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_room_type_en => /en/fornecedores/quartos/tipos_de_quartos/:id/editar(.:format)
  edit_hotel_room_type_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_room_type_es => /es/fornecedores/quartos/tipos_de_quartos/:id/editar(.:format)
  edit_hotel_room_type_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// edit_hotel_room_type_pt_br => /fornecedores/quartos/tipos_de_quartos/:id/editar(.:format)
  edit_hotel_room_type_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"editar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_admin_booking_en => /en/admin/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_admin_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_admin_booking_es => /es/admin/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_admin_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_admin_booking_pt_br => /admin/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_admin_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_executive_responsible_booking => /executivos/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_executive_responsible_booking_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_hotel_booking_en => /en/fornecedores/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_hotel_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_hotel_booking_es => /es/fornecedores/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_hotel_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_accept_request_hotel_booking_pt_br => /fornecedores/reservas/:id/hotel_accept_request(.:format)
  hotel_accept_request_hotel_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_accept_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_booking_en => /en/fornecedores/reservas/:id(.:format)
  hotel_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_booking_es => /es/fornecedores/reservas/:id(.:format)
  hotel_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_booking_pt_br => /fornecedores/reservas/:id(.:format)
  hotel_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_bookings_en => /en/fornecedores/reservas(.:format)
  hotel_bookings_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_bookings_es => /es/fornecedores/reservas(.:format)
  hotel_bookings_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_bookings_pt_br => /fornecedores/reservas(.:format)
  hotel_bookings_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_change_password_en => /en/fornecedores/mudar-senha(.:format)
  hotel_change_password_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_change_password_es => /es/fornecedores/mudar-senha(.:format)
  hotel_change_password_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_change_password_pt_br => /fornecedores/mudar-senha(.:format)
  hotel_change_password_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_admin_booking_en => /en/admin/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_admin_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_admin_booking_es => /es/admin/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_admin_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_admin_booking_pt_br => /admin/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_admin_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_executive_responsible_booking => /executivos/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_executive_responsible_booking_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"executivos",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_hotel_booking_en => /en/fornecedores/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_hotel_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_hotel_booking_es => /es/fornecedores/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_hotel_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_deny_request_hotel_booking_pt_br => /fornecedores/reservas/:id/hotel_deny_request(.:format)
  hotel_deny_request_hotel_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"hotel_deny_request",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_en => /en/fornecedores/mudar-senha(.:format)
  hotel_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_es => /es/fornecedores/mudar-senha(.:format)
  hotel_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_hotel_password_en => /en/fornecedores/password(.:format)
  hotel_hotel_password_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"password",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_hotel_password_es => /es/fornecedores/contrase%C3%B1a(.:format)
  hotel_hotel_password_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"contrase%C3%B1a",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_hotel_password_pt_br => /fornecedores/senha(.:format)
  hotel_hotel_password_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_hotel_session_en => /en/fornecedores/entrar(.:format)
  hotel_hotel_session_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_hotel_session_es => /es/fornecedores/entrar(.:format)
  hotel_hotel_session_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_hotel_session_pt_br => /fornecedores/entrar(.:format)
  hotel_hotel_session_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_map_show_api_v1_hotels => /api/v1/hotels/search/map_show/:id(.:format)
  hotel_map_show_api_v1_hotels_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"api",false]],[7,"/",false]],[6,"v1",false]],[7,"/",false]],[6,"hotels",false]],[7,"/",false]],[6,"search",false]],[7,"/",false]],[6,"map_show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_map_show_api_v2_hotels => /api/v2/hotels/search/map_show/:id(.:format)
  hotel_map_show_api_v2_hotels_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"api",false]],[7,"/",false]],[6,"v2",false]],[7,"/",false]],[6,"hotels",false]],[7,"/",false]],[6,"search",false]],[7,"/",false]],[6,"map_show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_en => /en/fornecedores/salas-de-reuniao/salas/:id(.:format)
  hotel_meeting_room_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_es => /es/fornecedores/salas-de-reuniao/salas/:id(.:format)
  hotel_meeting_room_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_offers_creator_logs_en => /en/fornecedores/salas-de-reuniao/relatorios_de_ofertas(.:format)
  hotel_meeting_room_offers_creator_logs_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"relatorios_de_ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_offers_creator_logs_es => /es/fornecedores/salas-de-reuniao/relatorios_de_ofertas(.:format)
  hotel_meeting_room_offers_creator_logs_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"relatorios_de_ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_offers_creator_logs_pt_br => /fornecedores/salas-de-reuniao/relatorios_de_ofertas(.:format)
  hotel_meeting_room_offers_creator_logs_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"relatorios_de_ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_offers_en => /en/fornecedores/salas-de-reuniao/ofertas(.:format)
  hotel_meeting_room_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_offers_es => /es/fornecedores/salas-de-reuniao/ofertas(.:format)
  hotel_meeting_room_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_offers_pt_br => /fornecedores/salas-de-reuniao/ofertas(.:format)
  hotel_meeting_room_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_pricing_policies_en => /en/fornecedores/salas-de-reuniao/tabelas_de_precos(.:format)
  hotel_meeting_room_pricing_policies_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_pricing_policies_es => /es/fornecedores/salas-de-reuniao/tabelas_de_precos(.:format)
  hotel_meeting_room_pricing_policies_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_pricing_policies_pt_br => /fornecedores/salas-de-reuniao/tabelas_de_precos(.:format)
  hotel_meeting_room_pricing_policies_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_pricing_policy_en => /en/fornecedores/salas-de-reuniao/tabelas_de_precos/:id(.:format)
  hotel_meeting_room_pricing_policy_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_pricing_policy_es => /es/fornecedores/salas-de-reuniao/tabelas_de_precos/:id(.:format)
  hotel_meeting_room_pricing_policy_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_pricing_policy_pt_br => /fornecedores/salas-de-reuniao/tabelas_de_precos/:id(.:format)
  hotel_meeting_room_pricing_policy_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_room_pt_br => /fornecedores/salas-de-reuniao/salas/:id(.:format)
  hotel_meeting_room_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_rooms_en => /en/fornecedores/salas-de-reuniao/salas(.:format)
  hotel_meeting_rooms_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_rooms_es => /es/fornecedores/salas-de-reuniao/salas(.:format)
  hotel_meeting_rooms_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_meeting_rooms_pt_br => /fornecedores/salas-de-reuniao/salas(.:format)
  hotel_meeting_rooms_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_offers_en => /en/fornecedores/quartos/ofertas(.:format)
  hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_offers_es => /es/fornecedores/quartos/ofertas(.:format)
  hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_offers_pt_br => /fornecedores/quartos/ofertas(.:format)
  hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_pricing_policies_en => /en/fornecedores/quartos/tabelas_de_precos(.:format)
  hotel_pricing_policies_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_pricing_policies_es => /es/fornecedores/quartos/tabelas_de_precos(.:format)
  hotel_pricing_policies_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_pricing_policies_pt_br => /fornecedores/quartos/tabelas_de_precos(.:format)
  hotel_pricing_policies_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_pricing_policy_en => /en/fornecedores/quartos/tabelas_de_precos/:id(.:format)
  hotel_pricing_policy_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_pricing_policy_es => /es/fornecedores/quartos/tabelas_de_precos/:id(.:format)
  hotel_pricing_policy_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_pricing_policy_pt_br => /fornecedores/quartos/tabelas_de_precos/:id(.:format)
  hotel_pricing_policy_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_pt_br => /fornecedores/mudar-senha(.:format)
  hotel_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"mudar-senha",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_reports_en => /en/fornecedores/relatorios(.:format)
  hotel_reports_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_reports_es => /es/fornecedores/relatorios(.:format)
  hotel_reports_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_reports_pt_br => /fornecedores/relatorios(.:format)
  hotel_reports_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"relatorios",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_offers_creator_logs_en => /en/fornecedores/quartos/relatorios_de_ofertas(.:format)
  hotel_room_offers_creator_logs_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"relatorios_de_ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_offers_creator_logs_es => /es/fornecedores/quartos/relatorios_de_ofertas(.:format)
  hotel_room_offers_creator_logs_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"relatorios_de_ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_offers_creator_logs_pt_br => /fornecedores/quartos/relatorios_de_ofertas(.:format)
  hotel_room_offers_creator_logs_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"relatorios_de_ofertas",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_type_en => /en/fornecedores/quartos/tipos_de_quartos/:id(.:format)
  hotel_room_type_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_type_es => /es/fornecedores/quartos/tipos_de_quartos/:id(.:format)
  hotel_room_type_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_type_pt_br => /fornecedores/quartos/tipos_de_quartos/:id(.:format)
  hotel_room_type_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_type_show_api_v1_hotels => /api/v1/hotels/room_type_details/:id(.:format)
  hotel_room_type_show_api_v1_hotels_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"api",false]],[7,"/",false]],[6,"v1",false]],[7,"/",false]],[6,"hotels",false]],[7,"/",false]],[6,"room_type_details",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_type_show_api_v2_hotels => /api/v2/hotels/room_type_details/:id(.:format)
  hotel_room_type_show_api_v2_hotels_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"api",false]],[7,"/",false]],[6,"v2",false]],[7,"/",false]],[6,"hotels",false]],[7,"/",false]],[6,"room_type_details",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_types_en => /en/fornecedores/quartos/tipos_de_quartos(.:format)
  hotel_room_types_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_types_es => /es/fornecedores/quartos/tipos_de_quartos(.:format)
  hotel_room_types_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_room_types_pt_br => /fornecedores/quartos/tipos_de_quartos(.:format)
  hotel_room_types_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_rooms_en => /en/fornecedores/quartos/quartos(.:format)
  hotel_rooms_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_rooms_es => /es/fornecedores/quartos/quartos(.:format)
  hotel_rooms_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_rooms_pt_br => /fornecedores/quartos/quartos(.:format)
  hotel_rooms_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_root_en => /en/fornecedores(.:format)
  hotel_root_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_root_es => /es/fornecedores(.:format)
  hotel_root_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// hotel_root_pt_br => /fornecedores(.:format)
  hotel_root_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[7,"/",false],[6,"fornecedores",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// meeting_rooms_hotel_bookings_en => /en/fornecedores/reservas/salas-de-reuniao(.:format)
  meeting_rooms_hotel_bookings_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// meeting_rooms_hotel_bookings_es => /es/fornecedores/reservas/salas-de-reuniao(.:format)
  meeting_rooms_hotel_bookings_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// meeting_rooms_hotel_bookings_pt_br => /fornecedores/reservas/salas-de-reuniao(.:format)
  meeting_rooms_hotel_bookings_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_chain_en => /en/admin/redes-hoteleiras/novo(.:format)
  new_admin_hotel_chain_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_chain_es => /es/admin/redes-hoteleiras/novo(.:format)
  new_admin_hotel_chain_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_chain_pt_br => /admin/redes-hoteleiras/novo(.:format)
  new_admin_hotel_chain_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"redes-hoteleiras",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_en => /en/admin/hoteis/novo(.:format)
  new_admin_hotel_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_es => /es/admin/hoteis/novo(.:format)
  new_admin_hotel_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_admin_hotel_pt_br => /admin/hoteis/novo(.:format)
  new_admin_hotel_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"admin",false]],[7,"/",false]],[6,"hoteis",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_hotel_password_en => /en/fornecedores/password/novo(.:format)
  new_hotel_hotel_password_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"password",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_hotel_password_es => /es/fornecedores/contrase%C3%B1a/novo(.:format)
  new_hotel_hotel_password_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"contrase%C3%B1a",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_hotel_password_pt_br => /fornecedores/senha/novo(.:format)
  new_hotel_hotel_password_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"senha",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_hotel_session_en => /en/fornecedores/entrar(.:format)
  new_hotel_hotel_session_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_hotel_session_es => /es/fornecedores/entrar(.:format)
  new_hotel_hotel_session_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_hotel_session_pt_br => /fornecedores/entrar(.:format)
  new_hotel_hotel_session_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"entrar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_en => /en/fornecedores/salas-de-reuniao/salas/novo(.:format)
  new_hotel_meeting_room_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_es => /es/fornecedores/salas-de-reuniao/salas/novo(.:format)
  new_hotel_meeting_room_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_offer_en => /en/fornecedores/salas-de-reuniao/ofertas/novo(.:format)
  new_hotel_meeting_room_offer_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_offer_es => /es/fornecedores/salas-de-reuniao/ofertas/novo(.:format)
  new_hotel_meeting_room_offer_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_offer_pt_br => /fornecedores/salas-de-reuniao/ofertas/novo(.:format)
  new_hotel_meeting_room_offer_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_pricing_policy_en => /en/fornecedores/salas-de-reuniao/tabelas_de_precos/novo(.:format)
  new_hotel_meeting_room_pricing_policy_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_pricing_policy_es => /es/fornecedores/salas-de-reuniao/tabelas_de_precos/novo(.:format)
  new_hotel_meeting_room_pricing_policy_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_pricing_policy_pt_br => /fornecedores/salas-de-reuniao/tabelas_de_precos/novo(.:format)
  new_hotel_meeting_room_pricing_policy_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_meeting_room_pt_br => /fornecedores/salas-de-reuniao/salas/novo(.:format)
  new_hotel_meeting_room_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"salas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_offer_en => /en/fornecedores/quartos/ofertas/novo(.:format)
  new_hotel_offer_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_offer_es => /es/fornecedores/quartos/ofertas/novo(.:format)
  new_hotel_offer_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_offer_pt_br => /fornecedores/quartos/ofertas/novo(.:format)
  new_hotel_offer_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_pricing_policy_en => /en/fornecedores/quartos/tabelas_de_precos/novo(.:format)
  new_hotel_pricing_policy_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_pricing_policy_es => /es/fornecedores/quartos/tabelas_de_precos/novo(.:format)
  new_hotel_pricing_policy_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_pricing_policy_pt_br => /fornecedores/quartos/tabelas_de_precos/novo(.:format)
  new_hotel_pricing_policy_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tabelas_de_precos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_room_type_en => /en/fornecedores/quartos/tipos_de_quartos/novo(.:format)
  new_hotel_room_type_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_room_type_es => /es/fornecedores/quartos/tipos_de_quartos/novo(.:format)
  new_hotel_room_type_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// new_hotel_room_type_pt_br => /fornecedores/quartos/tipos_de_quartos/novo(.:format)
  new_hotel_room_type_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"tipos_de_quartos",false]],[7,"/",false]],[6,"novo",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// pricing_policies_with_meeting_room_id_hotel_meeting_room_offers_en => /en/fornecedores/salas-de-reuniao/ofertas/pricing_policies_with_meeting_room_id(.:format)
  pricing_policies_with_meeting_room_id_hotel_meeting_room_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"pricing_policies_with_meeting_room_id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// pricing_policies_with_meeting_room_id_hotel_meeting_room_offers_es => /es/fornecedores/salas-de-reuniao/ofertas/pricing_policies_with_meeting_room_id(.:format)
  pricing_policies_with_meeting_room_id_hotel_meeting_room_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"pricing_policies_with_meeting_room_id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// pricing_policies_with_meeting_room_id_hotel_meeting_room_offers_pt_br => /fornecedores/salas-de-reuniao/ofertas/pricing_policies_with_meeting_room_id(.:format)
  pricing_policies_with_meeting_room_id_hotel_meeting_room_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"pricing_policies_with_meeting_room_id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// pricing_policies_with_room_types_id_hotel_offers_en => /en/fornecedores/quartos/ofertas/pricing_policies_with_room_types_id(.:format)
  pricing_policies_with_room_types_id_hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"pricing_policies_with_room_types_id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// pricing_policies_with_room_types_id_hotel_offers_es => /es/fornecedores/quartos/ofertas/pricing_policies_with_room_types_id(.:format)
  pricing_policies_with_room_types_id_hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"pricing_policies_with_room_types_id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// pricing_policies_with_room_types_id_hotel_offers_pt_br => /fornecedores/quartos/ofertas/pricing_policies_with_room_types_id(.:format)
  pricing_policies_with_room_types_id_hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"pricing_policies_with_room_types_id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_api_v1_hotel_groups => /affiliate/api/v1/hotel_groups(.:format)
  public_affiliate_api_v1_hotel_groups_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"api",false]],[7,"/",false]],[6,"v1",false]],[7,"/",false]],[6,"hotel_groups",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_api_v1_hotel_list => /affiliate/api/v1/hotel_list(.:format)
  public_affiliate_api_v1_hotel_list_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"api",false]],[7,"/",false]],[6,"v1",false]],[7,"/",false]],[6,"hotel_list",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_api_v1_hotel_offers => /affiliate/api/v1/hotel_offers(.:format)
  public_affiliate_api_v1_hotel_offers_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"api",false]],[7,"/",false]],[6,"v1",false]],[7,"/",false]],[6,"hotel_offers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_api_v1_hotel_room => /affiliate/api/v1/hotels/:hotel_id/rooms/:id(.:format)
  public_affiliate_api_v1_hotel_room_path: function(_hotel_id, _id, options) {
  return Utils.build_path(["hotel_id","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"api",false]],[7,"/",false]],[6,"v1",false]],[7,"/",false]],[6,"hotels",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"rooms",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_api_v1_hotel_rooms => /affiliate/api/v1/hotels/:hotel_id/rooms(.:format)
  public_affiliate_api_v1_hotel_rooms_path: function(_hotel_id, options) {
  return Utils.build_path(["hotel_id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"api",false]],[7,"/",false]],[6,"v1",false]],[7,"/",false]],[6,"hotels",false]],[7,"/",false]],[3,"hotel_id",false]],[7,"/",false]],[6,"rooms",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_en => /en/affiliate/search/hotel-details/:id(/:name)(.:format)
  public_affiliate_hotel_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["name","format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"affiliate",false]],[7,"/",false]],[6,"search",false]],[7,"/",false]],[6,"hotel-details",false]],[7,"/",false]],[3,"id",false]],[1,[2,[7,"/",false],[3,"name",false]],false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_es => /es/affiliate/buscar/detalles-del-hotel/:id(/:name)(.:format)
  public_affiliate_hotel_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["name","format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"affiliate",false]],[7,"/",false]],[6,"buscar",false]],[7,"/",false]],[6,"detalles-del-hotel",false]],[7,"/",false]],[3,"id",false]],[1,[2,[7,"/",false],[3,"name",false]],false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_map_show_en => /en/affiliate/search/map_show/:id(.:format)
  public_affiliate_hotel_map_show_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"affiliate",false]],[7,"/",false]],[6,"search",false]],[7,"/",false]],[6,"map_show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_map_show_es => /es/affiliate/buscar/map_show/:id(.:format)
  public_affiliate_hotel_map_show_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"affiliate",false]],[7,"/",false]],[6,"buscar",false]],[7,"/",false]],[6,"map_show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_map_show_pt_br => /affiliate/pesquisar/map_show/:id(.:format)
  public_affiliate_hotel_map_show_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"pesquisar",false]],[7,"/",false]],[6,"map_show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_pt_br => /affiliate/pesquisar/detalhes-hotel/:id(/:name)(.:format)
  public_affiliate_hotel_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["name","format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"pesquisar",false]],[7,"/",false]],[6,"detalhes-hotel",false]],[7,"/",false]],[3,"id",false]],[1,[2,[7,"/",false],[3,"name",false]],false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_room_type_show_en => /en/affiliate/search/hotel-details/:hotel_id-:hotel_name/room-type-details/:id(.:format)
  public_affiliate_hotel_room_type_show_en_path: function(_hotel_id, _hotel_name, _id, options) {
  return Utils.build_path(["hotel_id","hotel_name","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"affiliate",false]],[7,"/",false]],[6,"search",false]],[7,"/",false]],[6,"hotel-details",false]],[7,"/",false]],[3,"hotel_id",false]],[6,"-",false]],[3,"hotel_name",false]],[7,"/",false]],[6,"room-type-details",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_room_type_show_es => /es/affiliate/buscar/detalles-del-hotel/:hotel_id-:hotel_name/detalles-sobre-el-tipo-de-habitaci%C3%B3n/:id(.:format)
  public_affiliate_hotel_room_type_show_es_path: function(_hotel_id, _hotel_name, _id, options) {
  return Utils.build_path(["hotel_id","hotel_name","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"affiliate",false]],[7,"/",false]],[6,"buscar",false]],[7,"/",false]],[6,"detalles-del-hotel",false]],[7,"/",false]],[3,"hotel_id",false]],[6,"-",false]],[3,"hotel_name",false]],[7,"/",false]],[6,"detalles-sobre-el-tipo-de-habitaci%C3%B3n",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_affiliate_hotel_room_type_show_pt_br => /affiliate/pesquisar/detalhes-hotel/:hotel_id-:hotel_name/detalhes-tipo-de-quarto/:id(.:format)
  public_affiliate_hotel_room_type_show_pt_br_path: function(_hotel_id, _hotel_name, _id, options) {
  return Utils.build_path(["hotel_id","hotel_name","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"pesquisar",false]],[7,"/",false]],[6,"detalhes-hotel",false]],[7,"/",false]],[3,"hotel_id",false]],[6,"-",false]],[3,"hotel_name",false]],[7,"/",false]],[6,"detalhes-tipo-de-quarto",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_en => /en/search/hotel-details/:id(/:name)(.:format)
  public_hotel_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["name","format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"search",false]],[7,"/",false]],[6,"hotel-details",false]],[7,"/",false]],[3,"id",false]],[1,[2,[7,"/",false],[3,"name",false]],false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_es => /es/buscar/detalles-del-hotel/:id(/:name)(.:format)
  public_hotel_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["name","format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"buscar",false]],[7,"/",false]],[6,"detalles-del-hotel",false]],[7,"/",false]],[3,"id",false]],[1,[2,[7,"/",false],[3,"name",false]],false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_map_show_en => /en/search/map_show/:id(.:format)
  public_hotel_map_show_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"search",false]],[7,"/",false]],[6,"map_show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_map_show_es => /es/buscar/map_show/:id(.:format)
  public_hotel_map_show_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"buscar",false]],[7,"/",false]],[6,"map_show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_map_show_pt_br => /pesquisar/map_show/:id(.:format)
  public_hotel_map_show_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"pesquisar",false]],[7,"/",false]],[6,"map_show",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_pt_br => /pesquisar/detalhes-hotel/:id(/:name)(.:format)
  public_hotel_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["name","format"], [2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"pesquisar",false]],[7,"/",false]],[6,"detalhes-hotel",false]],[7,"/",false]],[3,"id",false]],[1,[2,[7,"/",false],[3,"name",false]],false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_room_type_show_en => /en/search/hotel-details/:hotel_id-:hotel_name/room-type-details/:id(.:format)
  public_hotel_room_type_show_en_path: function(_hotel_id, _hotel_name, _id, options) {
  return Utils.build_path(["hotel_id","hotel_name","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"search",false]],[7,"/",false]],[6,"hotel-details",false]],[7,"/",false]],[3,"hotel_id",false]],[6,"-",false]],[3,"hotel_name",false]],[7,"/",false]],[6,"room-type-details",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_room_type_show_es => /es/buscar/detalles-del-hotel/:hotel_id-:hotel_name/detalles-sobre-el-tipo-de-habitaci%C3%B3n/:id(.:format)
  public_hotel_room_type_show_es_path: function(_hotel_id, _hotel_name, _id, options) {
  return Utils.build_path(["hotel_id","hotel_name","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"buscar",false]],[7,"/",false]],[6,"detalles-del-hotel",false]],[7,"/",false]],[3,"hotel_id",false]],[6,"-",false]],[3,"hotel_name",false]],[7,"/",false]],[6,"detalles-sobre-el-tipo-de-habitaci%C3%B3n",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// public_hotel_room_type_show_pt_br => /pesquisar/detalhes-hotel/:hotel_id-:hotel_name/detalhes-tipo-de-quarto/:id(.:format)
  public_hotel_room_type_show_pt_br_path: function(_hotel_id, _hotel_name, _id, options) {
  return Utils.build_path(["hotel_id","hotel_name","id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"pesquisar",false]],[7,"/",false]],[6,"detalhes-hotel",false]],[7,"/",false]],[3,"hotel_id",false]],[6,"-",false]],[3,"hotel_name",false]],[7,"/",false]],[6,"detalhes-tipo-de-quarto",false]],[7,"/",false]],[3,"id",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_all_intermediate_step_hotel_offers_en => /en/fornecedores/quartos/ofertas/remover_todas_confirmacao(.:format)
  remove_all_intermediate_step_hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_todas_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_all_intermediate_step_hotel_offers_es => /es/fornecedores/quartos/ofertas/remover_todas_confirmacao(.:format)
  remove_all_intermediate_step_hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_todas_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_all_intermediate_step_hotel_offers_pt_br => /fornecedores/quartos/ofertas/remover_todas_confirmacao(.:format)
  remove_all_intermediate_step_hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_todas_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_all_offers_hotel_offers_en => /en/fornecedores/quartos/ofertas/remove_all_offers(.:format)
  remove_all_offers_hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remove_all_offers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_all_offers_hotel_offers_es => /es/fornecedores/quartos/ofertas/remove_all_offers(.:format)
  remove_all_offers_hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remove_all_offers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_all_offers_hotel_offers_pt_br => /fornecedores/quartos/ofertas/remove_all_offers(.:format)
  remove_all_offers_hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remove_all_offers",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_hotel_meeting_room_offers_en => /en/fornecedores/salas-de-reuniao/ofertas/remover(.:format)
  remove_hotel_meeting_room_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_hotel_meeting_room_offers_es => /es/fornecedores/salas-de-reuniao/ofertas/remover(.:format)
  remove_hotel_meeting_room_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_hotel_meeting_room_offers_pt_br => /fornecedores/salas-de-reuniao/ofertas/remover(.:format)
  remove_hotel_meeting_room_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_hotel_offers_en => /en/fornecedores/quartos/ofertas/remover(.:format)
  remove_hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_hotel_offers_es => /es/fornecedores/quartos/ofertas/remover(.:format)
  remove_hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_hotel_offers_pt_br => /fornecedores/quartos/ofertas/remover(.:format)
  remove_hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_intermediate_step_hotel_meeting_room_offers_en => /en/fornecedores/salas-de-reuniao/ofertas/remover_confirmacao(.:format)
  remove_intermediate_step_hotel_meeting_room_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_intermediate_step_hotel_meeting_room_offers_es => /es/fornecedores/salas-de-reuniao/ofertas/remover_confirmacao(.:format)
  remove_intermediate_step_hotel_meeting_room_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_intermediate_step_hotel_meeting_room_offers_pt_br => /fornecedores/salas-de-reuniao/ofertas/remover_confirmacao(.:format)
  remove_intermediate_step_hotel_meeting_room_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_intermediate_step_hotel_offers_en => /en/fornecedores/quartos/ofertas/remover_confirmacao(.:format)
  remove_intermediate_step_hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_intermediate_step_hotel_offers_es => /es/fornecedores/quartos/ofertas/remover_confirmacao(.:format)
  remove_intermediate_step_hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// remove_intermediate_step_hotel_offers_pt_br => /fornecedores/quartos/ofertas/remover_confirmacao(.:format)
  remove_intermediate_step_hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"remover_confirmacao",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// room_types_with_active_rooms_hotel_offers_en => /en/fornecedores/quartos/ofertas/room_types_with_active_rooms(.:format)
  room_types_with_active_rooms_hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"room_types_with_active_rooms",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// room_types_with_active_rooms_hotel_offers_es => /es/fornecedores/quartos/ofertas/room_types_with_active_rooms(.:format)
  room_types_with_active_rooms_hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"room_types_with_active_rooms",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// room_types_with_active_rooms_hotel_offers_pt_br => /fornecedores/quartos/ofertas/room_types_with_active_rooms(.:format)
  room_types_with_active_rooms_hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"room_types_with_active_rooms",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// rooms_hotel_bookings_en => /en/fornecedores/reservas/quartos(.:format)
  rooms_hotel_bookings_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[6,"quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// rooms_hotel_bookings_es => /es/fornecedores/reservas/quartos(.:format)
  rooms_hotel_bookings_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[6,"quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// rooms_hotel_bookings_pt_br => /fornecedores/reservas/quartos(.:format)
  rooms_hotel_bookings_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[6,"quartos",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// set_as_no_show_hotel_booking_en => /en/fornecedores/reservas/:id/set_as_no_show(.:format)
  set_as_no_show_hotel_booking_en_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"set_as_no_show",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// set_as_no_show_hotel_booking_es => /es/fornecedores/reservas/:id/set_as_no_show(.:format)
  set_as_no_show_hotel_booking_es_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"set_as_no_show",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// set_as_no_show_hotel_booking_pt_br => /fornecedores/reservas/:id/set_as_no_show(.:format)
  set_as_no_show_hotel_booking_pt_br_path: function(_id, options) {
  return Utils.build_path(["id"], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"reservas",false]],[7,"/",false]],[3,"id",false]],[7,"/",false]],[6,"set_as_no_show",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// set_email_for_last_log_public_affiliate_hotel_search_logs => /affiliate/hotel_search_logs/set_email_for_last_log(.:format)
  set_email_for_last_log_public_affiliate_hotel_search_logs_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[7,"/",false],[6,"affiliate",false]],[7,"/",false]],[6,"hotel_search_logs",false]],[7,"/",false]],[6,"set_email_for_last_log",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// set_email_for_last_log_public_hotel_search_logs => /hotel_search_logs/set_email_for_last_log(.:format)
  set_email_for_last_log_public_hotel_search_logs_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[7,"/",false],[6,"hotel_search_logs",false]],[7,"/",false]],[6,"set_email_for_last_log",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// show_all_hotel_meeting_room_offers_en => /en/fornecedores/salas-de-reuniao/ofertas/listar(.:format)
  show_all_hotel_meeting_room_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"listar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// show_all_hotel_meeting_room_offers_es => /es/fornecedores/salas-de-reuniao/ofertas/listar(.:format)
  show_all_hotel_meeting_room_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"listar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// show_all_hotel_meeting_room_offers_pt_br => /fornecedores/salas-de-reuniao/ofertas/listar(.:format)
  show_all_hotel_meeting_room_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"salas-de-reuniao",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"listar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// show_all_hotel_offers_en => /en/fornecedores/quartos/ofertas/listar(.:format)
  show_all_hotel_offers_en_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"en",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"listar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// show_all_hotel_offers_es => /es/fornecedores/quartos/ofertas/listar(.:format)
  show_all_hotel_offers_es_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"es",false]],[7,"/",false]],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"listar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  },
// show_all_hotel_offers_pt_br => /fornecedores/quartos/ofertas/listar(.:format)
  show_all_hotel_offers_pt_br_path: function(options) {
  return Utils.build_path([], ["format"], [2,[2,[2,[2,[2,[2,[2,[2,[7,"/",false],[6,"fornecedores",false]],[7,"/",false]],[6,"quartos",false]],[7,"/",false]],[6,"ofertas",false]],[7,"/",false]],[6,"listar",false]],[1,[2,[8,".",false],[3,"format",false]],false]], arguments);
  }}
;

  window.Routes.options = defaults;

}).call(this);
