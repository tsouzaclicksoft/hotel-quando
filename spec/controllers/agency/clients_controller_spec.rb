# encoding: utf-8

require 'spec_helper'

describe Agency::ClientsController do

  before(:each) {
    @agency = FactoryGirl.create(:agency)
    @client = FactoryGirl.create(:company, agency: @agency)
    @client_employee = FactoryGirl.create(:user, company: @client)
    @client_of_other_agency = FactoryGirl.create(:company)
    @client_of_other_agency_employee = FactoryGirl.create(:user, company: @client_of_other_agency_employee)
    request.env['warden'].stub :authenticate! => @agency
    controller.stub :current_agency_agency => @agency
  }

  describe 'Requests' do
    describe 'POST authenticate_as_client_employee' do
      it 'should sign in the agency as its client employee' do
        response = post :authenticate_as_client_employee, id: @client.id, employee_id: @client_employee.id
        response.should redirect_to public_root_pt_br_path
        session["warden.user.public_user.key"].first[0].should == @client_employee.id
      end

      it 'should not let the agency sign in as another agency client employee' do
        response = post :authenticate_as_client_employee, id: @client_of_other_agency.id, employee_id: @client_of_other_agency_employee.id
        response.should redirect_to agency_clients_path
        session["warden.user.public_user.key"].should be_nil
      end
    end

    describe 'POST authenticate_as_client' do
      it 'should sign in the agency as its client' do
        response = post :authenticate_as_client, id: @client.id
        response.should redirect_to company_root_path
        session["warden.user.company_company.key"].first[0].should == @client.id
      end

      it 'should not let the agency sign in as another agency client' do
        response = post :authenticate_as_client, id: @client_of_other_agency.id
        response.should redirect_to agency_clients_path
        session["warden.user.company_company.key"].should be_nil
      end
    end
  end
end
