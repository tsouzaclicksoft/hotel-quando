# encoding: utf-8

require 'spec_helper'

describe Hotel::BookingsController do

  before(:each) {
    @hotel = FactoryGirl.create(:hotel)
    request.env['warden'].stub :authenticate! => @hotel
    controller.stub :current_hotel_hotel => @hotel
    @offer = FactoryGirl.create(:offer, hotel: @hotel, checkin_timestamp: DateTime.now.utc - 2.hours)
    @booking = FactoryGirl.create(:booking, offer: @offer, status: Booking::ACCEPTED_STATUSES[:confirmed])
  }

  describe 'Requests' do
    describe 'PUT accuse_delayed_checkout', f:true do
      before(:each) do
        mock_mail = double
        mock_mail.stub(:deliver)
        UserMailer.stub(:send_delayed_checkout_notice).and_return(mock_mail)
      end

      it 'accuses the booking delayed checkout' do
        Booking.any_instance.should_receive(:accuse_delayed_checkout)
        put :accuse_delayed_checkout, id: @booking.id, locale: 'pt_br', :format => :js
      end

      it 'sends an email to the user about the delayed checkout' do
        Booking.any_instance.stub(:accuse_delayed_checkout)
        UserMailer.should_receive(:send_delayed_checkout_notice)
        put :accuse_delayed_checkout, id: @booking.id, locale: 'pt_br', :format => :js
      end

      it "assigns @booking" do
        put :accuse_delayed_checkout, id: @booking.id, locale: 'pt_br', :format => :js
        expect(assigns(:booking)).to be_an_instance_of(Booking)
      end

      it 'assigns @completed_request to true if it succesfully accuses the delay in the checkout' do
        Booking.any_instance.stub(:accuse_delayed_checkout)
        put :accuse_delayed_checkout, id: @booking.id, locale: 'pt_br', :format => :js
        expect(assigns(:completed_request)).to be_true
      end

      it 'assigns @completed_request to false if it dont succesfully accuses the delay in the checkout' do
        Booking.any_instance.stub(:accuse_delayed_checkout).and_raise
        put :accuse_delayed_checkout, id: @booking.id, locale: 'pt_br', :format => :js
        expect(assigns(:completed_request)).to be_false
      end
    end
    context 'maxipago was enabled at booking creation time' do
      describe "PUT set_as_no_show" do
        before(:each) do
          FactoryGirl.create(:payment, booking: @booking)
          mock_mail = double
          mock_mail.stub(:deliver)

          HotelMailer.stub(:no_show_with_charge_successfull_notice).and_return(mock_mail)
          HotelMailer.stub(:noshow_for_already_captured_cc_notice).and_return(mock_mail)
          HotelMailer.stub(:noshow_successfull_charged_notice).and_return(mock_mail)
          HotelMailer.stub(:no_show_with_charge_error_notice).and_return(mock_mail)
          UserMailer.stub(:no_show_with_charge_successfull_notice).and_return(mock_mail)
          UserMailer.stub(:noshow_for_already_captured_cc_notice).and_return(mock_mail)
          UserMailer.stub(:noshow_successfull_charged_notice).and_return(mock_mail)
          UserMailer.stub(:no_show_with_charge_error_notice).and_return(mock_mail)
          AdminMailer.stub(:no_show_with_charge_error_notice).and_return(mock_mail)
        end

        it "assigns @booking" do
          put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
          expect(assigns(:booking)).to be_an_instance_of(Booking)
        end

        it "calls can_be_set_as_no_show? of booking" do
          Booking.any_instance.should_receive(:can_be_set_as_no_show?)
          put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
        end

        context 'booking can be marked as no-show' do
          before(:each) do
            Booking.any_instance.stub(:can_be_set_as_no_show?).and_return(true)
          end

          context 'booking was marked to be paid at the hotel' do
            specify do
              controller.should_receive(:charge_credit_card_for_no_show)
              put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
            end

            context 'charged successfull' do
              before(:each) do
                controller.stub(:charge_credit_card_for_no_show)
              end

              it "sets the booking stats as paid with success", gf: true do
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
                @booking.reload
                @booking.status.should == Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
              end


              it "sends email to user notifying the charge" do
                mailer = double
                mailer.should_receive(:deliver)
                UserMailer.should_receive(:no_show_with_charge_successfull_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end


              it "sends email to hotel " do
                mailer = double
                mailer.should_receive(:deliver)

                HotelMailer.should_receive(:noshow_successfull_charged_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end
            end

            context 'charged not successfull' do
              before(:each) do
                controller.stub(:charge_credit_card_for_no_show).and_raise(Exception, "mensagem")
              end

              it "set booking as noshow with payment error", focus: true do
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
                @booking.reload
                @booking.status.should == Booking::ACCEPTED_STATUSES[:no_show_with_payment_error]
              end

              it "sends email to user" do
                mailer = double
                mailer.should_receive(:deliver)
                UserMailer.should_receive(:no_show_with_charge_error_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end

              it "sends an email to admin" do
                mailer = double
                mailer.should_receive(:deliver)
                AdminMailer.should_receive(:no_show_with_charge_error_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end

              it "sends email to hotel" do
                mailer = double
                mailer.should_receive(:deliver)
                HotelMailer.should_receive(:no_show_with_charge_error_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end
            end
          end #of booking paid at the hotel

          context 'booking was paid online' do
            before(:each) do
              @booking.update(status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
              FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:captured], booking: @booking)
            end

            it 'calls refund_difference_from_booking_total_to_no_show if the booking needs to be refunded' do
              Booking.any_instance.stub(:no_show_refund_value).and_return(100)
              controller.should_not_receive(:charge_credit_card_for_no_show)
              controller.should_receive(:refund_difference_from_booking_total_to_no_show)
              put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
            end

            it 'does not call refund_difference_from_booking_total_to_no_show if the booking does not needs to be refunded' do
              Booking.any_instance.stub(:no_show_refund_value).and_return(0)
              controller.should_not_receive(:charge_credit_card_for_no_show)
              controller.should_not_receive(:refund_difference_from_booking_total_to_no_show)
              put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
            end

            context 'refunded successfull' do
              before(:each) do
                controller.stub(:refund_difference_from_booking_total_to_no_show)
              end

              it "sets the booking status as paid with success" do
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
                @booking.reload
                @booking.status.should == Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
              end

              it "sends email to user notifying the no-show and that the booking value was already captured" do
                mailer = double
                mailer.should_receive(:deliver)

                UserMailer.should_receive(:no_show_for_already_captured_cc_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end


              it "sends email to hotel" do
                mailer = double
                mailer.should_receive(:deliver)

                HotelMailer.should_receive(:noshow_successfull_charged_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end
            end

            context 'refunded not successfull' do
              before(:each) do
                Booking.any_instance.stub(:no_show_refund_value).and_return(100)
                controller.stub(:refund_difference_from_booking_total_to_no_show).and_raise(Hotel::BookingsController::CannotRefundUser, "mensagem")
              end

              it "set booking as noshow with refund error", focus: true do
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
                @booking.reload
                @booking.status.should == Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
                @booking.refund_error.should == true
              end

              it "sends email to user" do
                mailer = double
                mailer.should_receive(:deliver)
                UserMailer.should_receive(:send_no_show_refund_error_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end

              it "sends an email to admin" do
                mailer = double
                mailer.should_receive(:deliver)
                AdminMailer.should_receive(:send_no_show_refund_error_notice).and_return(mailer)
                put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
              end
            end
          end #of booking paid online
        end
      end
    end
    # The difference between the two contexts is that the booking had a payment in the first context
    # in this context, the booking does not have any payments
    context 'maxipago was disabled at booking creation time' do
      describe "PUT set_as_no_show", g:true do
        before(:each) do
          mock_mail = double
          mock_mail.stub(:deliver)
          HotelMailer.stub(:no_show_pending_charge_notice).and_return(mock_mail)
          UserMailer.stub(:no_show_pending_charge_notice).and_return(mock_mail)
          AdminMailer.stub(:no_show_pending_charge_notice).and_return(mock_mail)

          Payment.delete_all
        end

        it "assigns @booking" do
          put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
          expect(assigns(:booking)).to be_an_instance_of(Booking)
        end

        it "calls can_be_set_as_no_show? of booking" do
          Booking.any_instance.should_receive(:can_be_set_as_no_show?)
          put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
        end

        context 'booking can be marked as no-show' do
          before(:each) do
            Booking.any_instance.stub(:can_be_set_as_no_show?).and_return(true)
          end

          specify do
            controller.should_not_receive(:charge_credit_card_for_no_show)
            put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
          end

          it "sets the booking as noshow paid with success" do
            put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
            @booking.reload
            @booking.status.should == Booking::ACCEPTED_STATUSES[:no_show_with_payment_pending]
          end

          it "sends an email to user" do
            mailer = double
            mailer.should_receive(:deliver)
            UserMailer.should_receive(:no_show_pending_charge_notice).and_return(mailer)
            put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
          end

          it "sends an email to hotel" do
            mailer = double
            mailer.should_receive(:deliver)
            HotelMailer.should_receive(:no_show_pending_charge_notice).and_return(mailer)
            put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
          end

          it "sends an email to admin" do
            mailer = double
            mailer.should_receive(:deliver)
            AdminMailer.should_receive(:no_show_pending_charge_notice).and_return(mailer)
            put :set_as_no_show, id: @booking.id, locale: 'pt_br', :format => :js
          end
        end
      end
    end
  end

  describe 'charge_credit_card_for_no_show' do
    # It cant capture an existing payment because the booking no-show value is not the same as the booking total value
    # context 'payment was authorized less than 5 days ago' do
    #   before(:each) do
    #     @payment_failed = FactoryGirl.create(:payment, booking: @booking, status: Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response])
    #     @payment = FactoryGirl.create(:payment, booking: @booking, status: Payment::ACCEPTED_STATUSES[:authorized])
    #     @payment.authorized_at = 3.days.ago
    #     @payment.save!
    #     controller.instance_variable_set("@booking", @booking)

    #   end

    #   it "captures the payment" do
    #     mock_response = OpenStruct.new
    #     mock_response.body = "body"
    #     mock_response.success = true
    #     mock_response.order_id = "123123"
    #     MaxiPagoInterface.should_receive(:direct_sale).with(payment: @payment, no_show: true).and_return(mock_response)

    #     controller.send(:charge_credit_card_for_no_show)
    #   end

    #   it "saves the response and updates the payment" do
    #     mock_response = OpenStruct.new
    #     mock_response.body = "body"
    #     mock_response.success = true
    #     mock_response.order_id = "123123"
    #     MaxiPagoInterface.stub(:direct_sale).and_return(mock_response)
    #     controller.send(:charge_credit_card_for_no_show)
    #     @payment.reload
    #     # Match against a regular expression because the payment already contains a maxipago response
    #     # and the controller appends the new response to the end of the old one,
    #     # so if @payment.maxipago_response was '<abc>', after the credit card is charged, the response will be
    #     # '<abc><new_response>'
    #     /body\z/.should === @payment.maxipago_response
    #     @payment.status.should == Payment::ACCEPTED_STATUSES[:captured]
    #   end

    #   it "raises CannotChargeCreditCard if cannot capture the value" do
    #     mock_response = OpenStruct.new
    #     mock_response.body = "body"
    #     mock_response.success = false
    #     mock_response.order_id = "123123"
    #     MaxiPagoInterface.stub(:direct_sale).and_return(mock_response)
    #     expect {
    #       controller.send(:charge_credit_card_for_no_show)
    #     }.to raise_error(Hotel::BookingsController::CannotChargeCreditCard)

    #   end

    # end


    before(:each) do
      @payment_failed = FactoryGirl.create(:payment, booking: @booking, status: Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response])
      @payment = FactoryGirl.create(:payment, booking: @booking, status: Payment::ACCEPTED_STATUSES[:authorized])
      @payment.authorized_at = 16.days.ago
      @payment.save!
      controller.instance_variable_set("@booking", @booking)
    end

    it "direct sale the payment" do
      mock_response = OpenStruct.new
      mock_response.body = "body"
      mock_response.success = true
      mock_response.order_id = "123123"
      MaxiPagoInterface.should_receive(:direct_sale).and_return(mock_response)
      controller.send(:charge_credit_card_for_no_show)
    end

    it "saves the response and create a new payment" do
      mock_response = OpenStruct.new
      mock_response.body = "body"
      mock_response.success = true
      mock_response.order_id = "234234"
      MaxiPagoInterface.stub(:direct_sale).and_return(mock_response)
      controller.send(:charge_credit_card_for_no_show)
      new_payment = Payment.last
      @payment.id.should_not == new_payment.id
      new_payment.maxipago_response.should == "body"
      new_payment.status.should == Payment::ACCEPTED_STATUSES[:captured]
      new_payment.maxipago_order_id.should == "234234"
    end

    it "raises CannotChargeCreditCard if cannot capture the value" do
      mock_response = OpenStruct.new
      mock_response.body = "body"
      mock_response.success = false
      mock_response.order_id = "123123"
      MaxiPagoInterface.stub(:direct_sale).and_return(mock_response)
      expect {
        controller.send(:charge_credit_card_for_no_show)
      }.to raise_error(Hotel::BookingsController::CannotChargeCreditCard)

    end

  end



  describe 'refund_difference_from_booking_total_to_no_show' do
    before(:each) do
      @payment = FactoryGirl.create(:payment, booking: @booking, status: Payment::ACCEPTED_STATUSES[:captured])
      @payment.save!
      @booking.stub(:no_show_refund_value).and_return(200)
      controller.instance_variable_set("@booking", @booking)
    end

    it "refunds the difference from the booking total value to the no-show value" do
      mock_response = OpenStruct.new
      mock_response.body = "body"
      mock_response.success = true
      mock_response.order_id = "123123"
      MaxiPagoInterface.should_receive(:refund).and_return(mock_response)
      controller.send(:refund_difference_from_booking_total_to_no_show)
    end

    it "saves the response and create a new refund" do
      mock_response = OpenStruct.new
      mock_response.body = "body"
      mock_response.success = true
      mock_response.order_id = "234234"
      MaxiPagoInterface.stub(:refund).and_return(mock_response)
      controller.send(:refund_difference_from_booking_total_to_no_show)
      refund = Refund.first
      refund.maxipago_response.should == "body"
      refund.status.should == Refund::ACCEPTED_STATUSES[:success]
      refund.maxipago_order_id.should == @payment.maxipago_order_id
    end

    it "raises CannotRefundUser if cannot refund the user" do
      mock_response = OpenStruct.new
      mock_response.body = "body"
      mock_response.success = false
      mock_response.order_id = "123123"
      MaxiPagoInterface.stub(:refund).and_return(mock_response)
      expect {
        controller.send(:refund_difference_from_booking_total_to_no_show)
      }.to raise_error(Hotel::BookingsController::CannotRefundUser)

    end

  end


end
