# encoding: utf-8

require 'spec_helper'
require 'ostruct'

describe Public::Bookings::PaymentsController do
  CONTROLLER_NAMESPACE = Public::Bookings::PaymentsController
  before(:each) {
    @booking = FactoryGirl.create(:booking)
    stub_const("Public::Bookings::PaymentsController::SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT", false)
    controller.stub :current_public_user => User::BudgetDecorator.decorate(@booking.user)
  }
  describe 'Requests' do
    before(:each) {
      request.env['warden'].stub :authenticate! => @booking.user
      credit_card = FactoryGirl.build(:credit_card)
      controller.instance_variable_set("@credit_card", credit_card)
    }

    let(:credit_card_params) {
      {
        "billing_name" => "Vinicius Oyama",
        "owner_cpf" =>"37349995881",
        "owner_passport" =>"",
        "temporary_number" =>"9555555555555551",
        "security_code" =>"400",
        "expiration_month" => "3",
        "expiration_year" => "2025"
      }
    }

    it 'checks if the user can afford the booking before each request' do
      subject.stub(:can_current_public_user_afford?).and_return(false)
      new_get_response = get :new, booking_id: @booking.id, locale: 'pt_br'
      controller.flash[:error].should == I18n.t(:message_booking_is_outside_budget_error)
      new_get_response.should redirect_to public_root_pt_br_path

      controller.flash[:error] = nil

      create_post_response = post :create,
                                  booking_id: @booking.id,
                                  locale: 'pt_br'

      controller.flash[:error].should == I18n.t(:message_booking_is_outside_budget_error)
      create_post_response.should redirect_to public_root_pt_br_path
    end

    describe "GET new" do
      it "assigns @credit_card" do
        get :new, booking_id: @booking.id, locale: 'pt_br'
        expect(assigns(:credit_card)).to be_an_instance_of(CreditCard)
      end
    end


    describe "POST create" do
      context 'maxipago enabled' do
        before(:each) do
          controller.stub(:set_a_valid_credit_card_to_use)
          controller.stub(:use_credit_card_to_confirm_booking)
          controller.stub(:use_credit_card_to_pay_booking)
          controller.stub(:create_client_token)
          controller.stub(:create_token_for_credit_card)
          @payment = FactoryGirl.build(:payment, credit_card: @credit_card, booking:  @booking)
          controller.instance_variable_set(:@credit_card, @credit_card)
          controller.instance_variable_set(:@booking, @booking)
          controller.instance_variable_set(:@payment, @payment)
        end

        it "calls create_client_token if client token is empty" do
          controller.should_receive(:create_client_token)
          @booking.user.maxipago_token = ""
          @booking.user.save!
          post :create,
                booking_id: @booking.id,
                locale: 'pt_br',
                pay_at_the_hotel: 'true',
                "credit_card" => credit_card_params
        end
        context 'hotel accepts online payment' do
          describe 'user opting to pay at the hotel' do

            it "calls set_a_valid_credit_card_to_use and use_credit_card_to_confirm_booking" do
              controller.should_receive(:set_a_valid_credit_card_to_use)
              controller.should_receive(:use_credit_card_to_confirm_booking)
              controller.should_not_receive(:use_credit_card_to_pay_booking)
              post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'true',
                    "credit_card" => credit_card_params
            end

            describe 'success' do
              it "renders create if no exception is raised", :now => true do
                post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'true',
                    "credit_card" => credit_card_params
                expect(response).to render_template('create')
              end

              it "sends email confirmation to user" do
                post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'true',
                    "credit_card" => credit_card_params

                ActionMailer::Base.deliveries.last.subject.should == "HotelQuando.com - Reserva confirmada com sucesso - ##{@booking.id}"
              end
            end

            describe 'error' do
              it "renders new with flash error if set_a_valid_credit_card_to_use raises an exeption" do
                controller.stub(:set_a_valid_credit_card_to_use).and_raise(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "stub message")
                post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'true',
                    "credit_card" => credit_card_params

                controller.flash[:error].should == "stub message"
                expect(response).to render_template('new')
              end

              it "renders new with flash error if use_credit_card_to_confirm_booking raises an exeption" do
                controller.stub(:use_credit_card_to_confirm_booking).and_raise(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "stub message")
                post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'true',
                    "credit_card" => credit_card_params

                controller.flash[:error].should == "stub message"
                expect(response).to render_template('new')
              end
            end
          end

          describe 'user opting to pay online' do
            it "calls set_a_valid_credit_card_to_use and use_credit_card_to_confirm_booking" do
              controller.should_receive(:set_a_valid_credit_card_to_use)
              controller.should_receive(:use_credit_card_to_pay_booking)
              controller.should_not_receive(:use_credit_card_to_confirm_booking)
              post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'false',
                    "credit_card" => credit_card_params
            end

            describe 'success' do
              it "renders create if no exception is raised", :now => true do
                post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'false',
                    "credit_card" => credit_card_params
                expect(response).to render_template('create')
              end

              it "sends booking confirmation email to user" do
                post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'false',
                    "credit_card" => credit_card_params

                ActionMailer::Base.deliveries.last.subject.should == "HotelQuando.com - Reserva confirmada com sucesso - ##{@booking.id}"
              end
            end

            describe 'error' do
              it "renders new with flash error if set_a_valid_credit_card_to_use raises an exeption" do
                controller.stub(:set_a_valid_credit_card_to_use).and_raise(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "stub message")
                post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'false',
                    "credit_card" => credit_card_params

                controller.flash[:error].should == "stub message"
                expect(response).to render_template('new')
              end

              it "renders new with flash error if use_credit_card_to_pay_booking raises an exeption" do
                controller.stub(:use_credit_card_to_pay_booking).and_raise(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "stub message")
                post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'false',
                    "credit_card" => credit_card_params

                controller.flash[:error].should == "stub message"
                expect(response).to render_template('new')
              end
            end
          end

        end


        context "hotel doesn't accept online payment" do
          before(:each) do
            @booking.hotel.accept_online_payment = false
            @booking.hotel.save!
          end

          describe "pay_at_the_hotel params doens't make difference" do
            it "calls set_a_valid_credit_card_to_use and use_credit_card_to_confirm_booking" do
              controller.should_receive(:set_a_valid_credit_card_to_use)
              controller.should_receive(:use_credit_card_to_confirm_booking)
              controller.should_not_receive(:use_credit_card_to_pay_booking)
              post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'true',
                    "credit_card" => credit_card_params
            end

            it "calls set_a_valid_credit_card_to_use and use_credit_card_to_confirm_booking" do
              controller.should_receive(:set_a_valid_credit_card_to_use)
              controller.should_receive(:use_credit_card_to_confirm_booking)
              controller.should_not_receive(:use_credit_card_to_pay_booking)
              post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    pay_at_the_hotel: 'false',
                    "credit_card" => credit_card_params
            end

            it "calls set_a_valid_credit_card_to_use and use_credit_card_to_confirm_booking" do
              controller.should_receive(:set_a_valid_credit_card_to_use)
              controller.should_receive(:use_credit_card_to_confirm_booking)
              controller.should_not_receive(:use_credit_card_to_pay_booking)
              post :create,
                    booking_id: @booking.id,
                    locale: 'pt_br',
                    "credit_card" => credit_card_params
            end
          end

          describe 'success' do
            it "renders create if no exception is raised", :now => true do
              post :create,
                  booking_id: @booking.id,
                  locale: 'pt_br',
                  pay_at_the_hotel: 'true',
                  "credit_card" => credit_card_params
              expect(response).to render_template('create')
            end

            it "sends email confirmation to user" do
              post :create,
                  booking_id: @booking.id,
                  locale: 'pt_br',
                  pay_at_the_hotel: 'true',
                  "credit_card" => credit_card_params

              ActionMailer::Base.deliveries.last.subject.should == "HotelQuando.com - Reserva confirmada com sucesso - ##{@booking.id}"
            end
          end

          describe 'error' do
            it "renders new with flash error if set_a_valid_credit_card_to_use raises an exeption" do
              controller.stub(:set_a_valid_credit_card_to_use).and_raise(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "stub message")
              post :create,
                  booking_id: @booking.id,
                  locale: 'pt_br',
                  pay_at_the_hotel: 'true',
                  "credit_card" => credit_card_params

              controller.flash[:error].should == "stub message"
              expect(response).to render_template('new')
            end

            it "renders new with flash error if use_credit_card_to_confirm_booking raises an exeption" do
              controller.stub(:use_credit_card_to_confirm_booking).and_raise(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "stub message")
              post :create,
                  booking_id: @booking.id,
                  locale: 'pt_br',
                  pay_at_the_hotel: 'true',
                  "credit_card" => credit_card_params

              controller.flash[:error].should == "stub message"
              expect(response).to render_template('new')
            end
          end
        end
      end

      context 'maxipago disabled' do
        before(:each) do
          stub_const("Public::Bookings::PaymentsController::SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT", true)
          controller.stub(:set_and_verify_credit_card)
          controller.stub(:confirm_booking_and_create_fake_payment_for_view)
          controller.stub(:send_credit_card_info_to_admin)
        end

        it "calls set_and_verify_credit_card" do
          controller.should_receive(:set_and_verify_credit_card)
          post :create,
                booking_id: @booking.id,
                locale: 'pt_br',
                "credit_card" => credit_card_params
        end

        it 'calls send_credit_card_info_to_admin' do
          controller.should_receive(:send_credit_card_info_to_admin)
          post :create,
                booking_id: @booking.id,
                locale: 'pt_br',
                "credit_card" => credit_card_params
        end


        it "calls confirm_booking_and_create_fake_payment_for_view" do
          controller.should_receive(:confirm_booking_and_create_fake_payment_for_view)
          post :create,
                booking_id: @booking.id,
                locale: 'pt_br',
                "credit_card" => credit_card_params
        end

        describe 'success' do
          it "renders create if no exception is raised", :now => true do
            email_count_before = ActionMailer::Base.deliveries.length
            post :create,
                booking_id: @booking.id,
                locale: 'pt_br',
                "credit_card" => credit_card_params
            ActionMailer::Base.deliveries.length.should == email_count_before + 2
            expect(response).to render_template('create')
          end
        end

        describe 'error' do
          it "renders new with flash error if set_and_verify_credit_card raises an exeption" do
            controller.stub(:set_and_verify_credit_card).and_raise(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "stub message")
            post :create,
                booking_id: @booking.id,
                locale: 'pt_br',
                "credit_card" => credit_card_params

            controller.flash[:error].should == "stub message"
            expect(response).to render_template('new')
          end

        end


      end

    end
  end

  describe "create_token_for_credit_card" do
    it "sets the card token" do
      subject.instance_variable_set(:@credit_card, FactoryGirl.build(:credit_card, maxipago_token: ''))
      MaxiPagoInterface.stub(:generate_credit_card_token_for).and_return("new token")
      subject.send(:create_token_for_credit_card)
      CreditCard.first.maxipago_token.should == "new token"
    end

    it "raise error if can not save the token" do
      credit_card = FactoryGirl.build(:credit_card, maxipago_token: '')
      credit_card.stub(:save).and_return(false)
      subject.instance_variable_set(:@credit_card, credit_card)
      MaxiPagoInterface.stub(:generate_credit_card_token_for)
      expect {
        subject.send(:create_token_for_credit_card)
      }.to raise_error(CONTROLLER_NAMESPACE::CreditCardTokenError, "Desculpe. Não conseguimos cadastrar seu cartão na operadora. Por favor tente novamente ou entre em contato.")
    end
  end

  describe "create_client_token" do
    it "sets the client token" do
      user = @booking.user
      user.maxipago_token = ""
      user.save!
      MaxiPagoInterface.stub(:generate_client_token_for).and_return("new token")
      subject.send(:create_client_token)
      User.first.maxipago_token.should == "new token"
    end

    it "raise error if can not save the token" do
      user = FactoryGirl.build(:user, maxipago_token: '')
      user.stub(:save).and_return(false)
      subject.stub(:current_public_user).and_return(user)
      MaxiPagoInterface.stub(:generate_client_token_for).and_return("new token")
      expect {
        subject.send(:create_client_token)
      }.to raise_error(CONTROLLER_NAMESPACE::ClientTokenError, "Desculpe. Não conseguimos registrar você no nosso serviço de cartões. Por favor tente novamente ou entre em contato.")
    end
  end

  describe "set_a_valid_credit_card_to_use" do
    context 'current public user is not related to a company' do
      context 'without id in the params' do
        before(:each) do
          @mock_params =  HashWithIndifferentAccess.new({
            "credit_card" => {
              "billing_name" => "Meu Nome",
              "owner_cpf" => "373.499.958-81",
              "owner_passport" => "",
              "billing_address1" => "Endereço asdfsd",
              "billing_zipcode" => "71924333",
              "billing_city" => "Guarulhos",
              "billing_state" => "SP",
              "billing_country" => "BR",
              "billing_phone" => "(12) 31231-2321",
              "temporary_number" => "4444 4444 4444 4448",
              "expiration_month" => "3",
              "expiration_year" => "2020"
            }
          })
          @mock_params[:credit_card].stub("permit!").and_return(@mock_params[:credit_card])
          subject.stub(:params).and_return(@mock_params)
          def subject.create_token_for_credit_card
            @credit_card.maxipago_token = "novotoken"
          end
        end

        it "assigns @credit_card for the current_public_user based on parameters" do
          subject.send(:set_a_valid_credit_card_to_use)
          credit_card = subject.instance_variable_get(:@credit_card)
          credit_card.billing_name.should == "Meu Nome"
          credit_card.temporary_number.should == "4444 4444 4444 4448"
          credit_card.user_id = @booking.user_id
          credit_card.id.should == CreditCard.last.id
        end

        it "raises CannotCreateCreditCardError if credit card is not valid" do
          @mock_params[:credit_card][:billing_name] = "umnome"
          expect {
            subject.send(:set_a_valid_credit_card_to_use)
          }.to raise_error(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "Dados do cartão de crédito inválidos. Por favor revise o formulário.")
        end

        context 'valid card params' do
          context 'card already registred' do
            before(:each) do
              @existing_credit_card = CreditCard.new(@mock_params[:credit_card])
              @existing_credit_card.user_id = @booking.user_id
              @existing_credit_card.maxipago_token = 'a3=423'
              @existing_credit_card.save!
            end

            it "doesn't save a new card" do
              subject.send(:set_a_valid_credit_card_to_use)
              CreditCard.count.should == 1
            end

            it "sets the @credit_card as the existing credit card" do
              subject.send(:set_a_valid_credit_card_to_use)
              credit_card = subject.instance_variable_get(:@credit_card)
              credit_card.id.should == @existing_credit_card.id
            end
          end

          context 'card not registred' do
            it "tries to generate a maxipago token" do
              subject.should_receive(:create_token_for_credit_card)
              CreditCard.any_instance.should_receive(:save).and_return(true)
              subject.send(:set_a_valid_credit_card_to_use)
            end

            it "saves the card with a token" do
              subject.send(:set_a_valid_credit_card_to_use)
              CreditCard.count.should == 1
              credit_card = CreditCard.first
              credit_card.maxipago_token.should == "novotoken"
            end
          end

        end

      end


      context 'with id in the params' do
        before(:each) do
          @existing_credit_card = FactoryGirl.create(:credit_card, user: @booking.user)
          @mock_params =  HashWithIndifferentAccess.new({
            "credit_card" => {
              "id" => @existing_credit_card.id.to_s,
              "billing_name" => "",
              "owner_cpf" => "",
              "owner_passport" => "",
              "billing_address1" => "",
              "billing_zipcode" => "",
              "billing_city" => "",
              "billing_state" => "",
              "billing_country" => "",
              "billing_phone" => "",
              "temporary_number" => "",
              "expiration_month" => "",
              "expiration_year" => ""
            }
          })
          @mock_params[:credit_card].stub("permit!").and_return(@mock_params[:credit_card])
          subject.stub(:params).and_return(@mock_params)
        end

        it "assigns @credit_card for the current_public_user based on parameters" do
          subject.send(:set_a_valid_credit_card_to_use)
          credit_card = subject.instance_variable_get(:@credit_card)
          credit_card.id.should == @existing_credit_card.id
        end
      end
    end # of current public user is not related to a company

    context 'current public user is related to a company' do
      before(:each) do
        @booking.user.update(company_id: FactoryGirl.create(:company).id)
      end

      context 'without id in the params' do
        before(:each) do
          @mock_params =  HashWithIndifferentAccess.new({
            "credit_card" => {
              "billing_name" => "Meu Nome",
              "owner_cpf" => "373.499.958-81",
              "owner_passport" => "",
              "billing_address1" => "Endereço asdfsd",
              "billing_zipcode" => "71924333",
              "billing_city" => "Guarulhos",
              "billing_state" => "SP",
              "billing_country" => "BR",
              "billing_phone" => "(12) 31231-2321",
              "temporary_number" => "4444 4444 4444 4448",
              "expiration_month" => "3",
              "expiration_year" => "2020"
            }
          })
          @mock_params[:credit_card].stub("permit!").and_return(@mock_params[:credit_card])
          subject.stub(:params).and_return(@mock_params)
        end

        it "raises" do
          expect{
            subject.send(:set_a_valid_credit_card_to_use)
          }.to raise_error
        end
      end


      context 'with id in the params' do
        before(:each) do
          @company_credit_card = FactoryGirl.create(:credit_card, company_id: @booking.user.company_id)
          @mock_params =  HashWithIndifferentAccess.new({
            "credit_card" => {
              "id" => @company_credit_card.id.to_s
            }
          })
          @mock_params[:credit_card].stub("permit!").and_return(@mock_params[:credit_card])
          subject.stub(:params).and_return(@mock_params)
        end

        it "assigns @credit_card for the current_public_user based on parameters" do
          subject.send(:set_a_valid_credit_card_to_use)
          credit_card = subject.instance_variable_get(:@credit_card)
          credit_card.id.should == @company_credit_card.id
        end
      end
    end # of current public user is related to a company
  end

  describe "use_credit_card_to_confirm_booking" do
    before(:each) do
      @credit_card = FactoryGirl.create(:credit_card, user: @booking.user)
      subject.instance_variable_set("@credit_card", @credit_card)
      payment = subject.instance_variable_set(:@booking, @booking)
      @mock_response = OpenStruct.new
      @mock_response.body = "body"
      @mock_response.is_authorized = true
      @mock_response.order_id = "123123"
      MaxiPagoInterface.stub(authorize_transaction: @mock_response)
    end

    it "assigns and save @payment" do
      subject.send(:use_credit_card_to_confirm_booking)
      payment = subject.instance_variable_get(:@payment)
      payment.credit_card_id.should == @credit_card.id
      payment.user_id.should == @booking.user_id
      payment.booking_id.should == @booking.id
      payment.maxipago_order_id.should == "123123"
    end

    it "saves the response" do
      subject.send(:use_credit_card_to_confirm_booking)
      payment = Payment.last
      payment.maxipago_response.should == "body"
    end

    context 'authorization aproved' do
      it "sets the payment as authorized" do
        subject.send(:use_credit_card_to_confirm_booking)
        payment = Payment.last
        payment.status.should == Payment::ACCEPTED_STATUSES[:authorized]
      end
      it "sets the booking as confirmed" do
        subject.send(:use_credit_card_to_confirm_booking)
        Booking.last.status.should == Booking::ACCEPTED_STATUSES[:confirmed]
      end
    end

    context 'authorization not aproved' do
      before(:each) do
        @mock_response.is_authorized = false
      end

      it "sets the payment as not authorized" do
        begin
          subject.send(:use_credit_card_to_confirm_booking)
        rescue
        end
        Payment.last.status.should == Payment::ACCEPTED_STATUSES[:not_authorized]
      end

      it "raises PaymentFailedError" do
        expect {
          subject.send(:use_credit_card_to_confirm_booking)
        }.to raise_error(CONTROLLER_NAMESPACE::PaymentFailedError, 'Houve um problema com seu cartão, por favor, tente novamente.')
      end
    end

    context 'maxipago responds with a invalid xml', focus: true do
      before(:each) do
        MaxiPagoInterface.stub(:authorize_transaction).and_raise(MaxiPagoInterface::MalformedMaxipagoResponseError, 'my malformed body')
      end

      it "sets the payment as maxipago_response_error" do
        begin
          subject.send(:use_credit_card_to_confirm_booking)
        rescue
        end
        Payment.last.status.should == Payment::ACCEPTED_STATUSES[:maxipago_response_error]
      end

      it "sets the payment body as the response body" do
        begin
          subject.send(:use_credit_card_to_confirm_booking)
        rescue
        end
        Payment.last.maxipago_response.should == 'my malformed body'
      end

      it "raises PaymentFailedError" do
        expect {
          subject.send(:use_credit_card_to_confirm_booking)
        }.to raise_error(CONTROLLER_NAMESPACE::PaymentFailedError, 'Houve um problema de comunicação com nosso servidor de pagamento. Por favor tente novamente.')
      end
    end

  end

  describe 'set_and_verify_credit_card' do
    before(:each) do
      @mock_params =  HashWithIndifferentAccess.new({
        "credit_card" => {
          "billing_name" => "Meu Nome",
          "owner_cpf" => "373.499.958-81",
          "owner_passport" => "",
          "billing_address1" => "Endereço asdfsd",
          "billing_zipcode" => "71924333",
          "billing_city" => "Guarulhos",
          "billing_state" => "SP",
          "billing_country" => "BR",
          "billing_phone" => "(12) 31231-2321",
          "temporary_number" => "4444 4444 4444 4448",
          "expiration_month" => "3",
          "expiration_year" => "2020"
        }
      })
      @mock_params[:credit_card].stub("permit!").and_return(@mock_params[:credit_card])
      subject.stub(:params).and_return(@mock_params)
    end

    it "assigns @credit_card for the current_public_user based on parameters" do
      subject.send(:set_and_verify_credit_card)
      credit_card = subject.instance_variable_get(:@credit_card)
      credit_card.billing_name.should == "Meu Nome"
      credit_card.temporary_number.should == "4444 4444 4444 4448"
      credit_card.user_id = @booking.user_id
      credit_card.should_not be_persisted
    end

    it "raises CannotCreateCreditCardError if credit card is not valid" do
      @mock_params[:credit_card][:billing_name] = "umnome"
      expect {
        subject.send(:set_and_verify_credit_card)
      }.to raise_error(CONTROLLER_NAMESPACE::CannotCreateCreditCardError, "Dados do cartão de crédito inválidos. Por favor revise o formulário.")
    end
  end

  describe 'confirm_booking_and_create_fake_payment_for_view' do
    it 'sets a new payment with authorized status to @payment' do
      subject.instance_variable_set('@booking', @booking)
      subject.send(:confirm_booking_and_create_fake_payment_for_view)
      mock_payment = subject.instance_variable_get('@payment')
      mock_payment.user.should == @booking.user
      mock_payment.status.should == Payment::ACCEPTED_STATUSES[:authorized]
    end

    it 'updates the booking status to confirmed' do
      subject.instance_variable_set('@booking', @booking)
      @booking.update(status: Booking::ACCEPTED_STATUSES[:waiting_for_payment])
      subject.send(:confirm_booking_and_create_fake_payment_for_view)
      @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed]
    end
  end

  describe 'send_credit_card_info_to_admin' do

    it 'should send an email to admin' do
      credit_card = FactoryGirl.build(:credit_card, user: @booking.user)
      subject.instance_variable_set('@booking', @booking)
      subject.instance_variable_set('@credit_card', credit_card)
      mailer_mock = double()
      mailer_mock.should_receive(:deliver)
      AdminMailer.should_receive(:send_credit_card_informations).with(credit_card, @booking).and_return(mailer_mock)
      subject.send(:send_credit_card_info_to_admin)
    end
  end

  describe "use_credit_card_to_pay_booking" do
    before(:each) do
      @credit_card = FactoryGirl.create(:credit_card, user: @booking.user)
      subject.instance_variable_set("@credit_card", @credit_card)
      payment = subject.instance_variable_set(:@booking, @booking)
      @mock_response = OpenStruct.new
      @mock_response.body = "body"
      @mock_response.success = true
      @mock_response.order_id = "123123"
      MaxiPagoInterface.stub(direct_sale: @mock_response)
    end

    it "assigns and save @payment" do
      subject.send(:use_credit_card_to_pay_booking)
      payment = subject.instance_variable_get(:@payment)
      payment.credit_card_id.should == @credit_card.id
      payment.user_id.should == @booking.user_id
      payment.booking_id.should == @booking.id
      payment.maxipago_order_id.should == "123123"
    end

    it "saves the response" do
      subject.send(:use_credit_card_to_pay_booking)
      payment = Payment.last
      payment.maxipago_response.should == "body"
    end

    context 'authorization aproved' do
      it "sets the payment as captured" do
        subject.send(:use_credit_card_to_pay_booking)
        payment = Payment.last
        payment.status.should == Payment::ACCEPTED_STATUSES[:captured]
      end

      it "sets the booking as confirmed and captured" do
        subject.send(:use_credit_card_to_pay_booking)
        Booking.last.status.should == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
      end
    end

    context 'authorization not aproved' do
      before(:each) do
        @mock_response.success = false
      end

      it "sets the payment as not authorized" do
        begin
          subject.send(:use_credit_card_to_pay_booking)
        rescue
        end
        Payment.last.status.should == Payment::ACCEPTED_STATUSES[:not_authorized]
      end

      it "raises PaymentFailedError" do
        expect {
          subject.send(:use_credit_card_to_pay_booking)
        }.to raise_error(CONTROLLER_NAMESPACE::PaymentFailedError, 'Houve um problema com seu cartão, por favor, tente novamente.')
      end
    end

    context 'maxipago responds with a invalid xml', focus: true do
      before(:each) do
        MaxiPagoInterface.stub(:direct_sale).and_raise(MaxiPagoInterface::MalformedMaxipagoResponseError, 'my malformed body')
      end

      it "sets the payment as maxipago_response_error" do
        begin
          subject.send(:use_credit_card_to_pay_booking)
        rescue
        end
        Payment.last.status.should == Payment::ACCEPTED_STATUSES[:maxipago_response_error]
      end

      it "sets the payment body as the response body" do
        begin
          subject.send(:use_credit_card_to_pay_booking)
        rescue
        end
        Payment.last.maxipago_response.should == 'my malformed body'
      end

      it "raises PaymentFailedError" do
        expect {
          subject.send(:use_credit_card_to_pay_booking)
        }.to raise_error(CONTROLLER_NAMESPACE::PaymentFailedError, 'Houve um problema de comunicação com nosso servidor de pagamento. Por favor tente novamente.')
      end
    end
  end


end
