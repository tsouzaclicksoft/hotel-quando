# encoding: utf-8

require 'spec_helper'

describe Company::EmployeesController do

  before(:each) {
    @company = FactoryGirl.create(:company)
    @company_employee = FactoryGirl.create(:user, company: @company)
    @other_company_employee = FactoryGirl.create(:user, company: FactoryGirl.create(:company))
    request.env['warden'].stub :authenticate! => @company
    controller.stub :current_company_company => @company
  }

  describe 'Requests' do
    describe 'POST authenticate_as' do
      it 'should sign in the company as its employee' do
        response = post :authenticate_as, id: @company_employee.id
        response.should redirect_to public_root_pt_br_path
        session["warden.user.public_user.key"].first[0].should == @company_employee.id
      end

      it 'should not let the company sign in as another company client employee' do
        response = post :authenticate_as, id: @other_company_employee.id
        response.should redirect_to company_employees_path
        session["warden.user.public_user.key"].should be_nil
      end
    end

  end
end
