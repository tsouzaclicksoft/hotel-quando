# encoding: utf-8

require 'spec_helper'

describe Payment do

  it "has a valid factory" do
    FactoryGirl.create(:payment).should be_persisted
  end

  it { should belong_to :user }
  it { should belong_to :booking }
  it { should belong_to :credit_card }

  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:booking) }
  it { should validate_presence_of(:credit_card) }

  it "updates authorized_at when status changes to authorized" do
    payment = FactoryGirl.create(:payment)
    payment.authorized_at.should == nil
    payment.status = Payment::ACCEPTED_STATUSES[:authorized]
    payment.save!
    payment.reload
    payment.authorized_at.should_not be_nil
    payment.authorized_at.day.should == DateTime.now.day
  end

  it "updates authorized_at when status changes to captured and authorized_at was not set before" do
    payment = FactoryGirl.create(:payment)
    payment.authorized_at.should == nil
    payment.status = Payment::ACCEPTED_STATUSES[:captured]
    payment.save!
    payment.reload
    payment.authorized_at.should_not be_nil
    payment.authorized_at.day.should == DateTime.now.day
  end

  it "doesn't update authorized_at when status changes and its not authorized or captured" do
    payment = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:authorized])
    payment.authorized_at.day.should == DateTime.now.day
    saved_authorized_at = payment.authorized_at
    payment.status = Payment::ACCEPTED_STATUSES[:canceled]
    payment.save!
    payment.reload
    payment.authorized_at.should == saved_authorized_at
    payment = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:captured])
    payment.authorized_at.day.should == DateTime.now.day
    saved_authorized_at = payment.authorized_at
    payment.status = Payment::ACCEPTED_STATUSES[:canceled]
    payment.save!
    payment.reload
    payment.authorized_at.should == saved_authorized_at
  end

  it "doesn't update authorized_at when status goes from authorized to captured" do
    payment = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:authorized])
    payment.authorized_at.day.should == DateTime.now.day
    saved_authorized_at = payment.authorized_at
    payment.status = Payment::ACCEPTED_STATUSES[:captured]
    payment.save!
    payment.reload
    payment.authorized_at.should eq saved_authorized_at
  end

  it 'updates canceled_at when the status changes to canceled' do
    payment = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:authorized])
    payment.canceled_at.should be_nil
    payment.status = Payment::ACCEPTED_STATUSES[:canceled]
    payment.save!
    payment.reload
    payment.canceled_at.day.should eq DateTime.now.day
  end

end
