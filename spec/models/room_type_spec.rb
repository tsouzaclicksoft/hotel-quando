# encoding: utf-8

require 'spec_helper'

describe RoomType do

  it { should validate_presence_of(:name_pt_br) }

  it { should_not validate_presence_of(:name_en) }

  it { should_not validate_presence_of(:description_pt_br) }

  it { should_not validate_presence_of(:description_en) }

  it { should validate_presence_of(:maximum_capacity) }

  it { should validate_presence_of(:single_bed_quantity) }

  it { should validate_presence_of(:double_bed_quantity) }

  it { should belong_to :hotel }

  it { should have_many :photos }

  it { should have_many :rooms }

  it 'should set the initial capacity to 1' do
    room_type = RoomType.new
    room_type.valid?
    room_type.initial_capacity.should == 1
  end

  it 'can be instantiated' do
    RoomType.new.should be_an_instance_of(RoomType)
  end

  it 'has a valid factory' do
    FactoryGirl.create(:room_type).should be_persisted
  end

  describe 'rooms_with_active_offer_count_per_day' do
    before(:each) do
      hotel = FactoryGirl.create(:hotel)
      room_type = FactoryGirl.create(:room_type, hotel: hotel)
      room1 = FactoryGirl.create(:room, hotel: hotel, room_type: room_type)
      room2 = FactoryGirl.create(:room, hotel: hotel, room_type: room_type)
      room3 = FactoryGirl.create(:room, hotel: hotel, room_type: room_type)

      #room2
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 1.day.from_now, pack_in_hours: 3, room: room2, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 1.day.from_now, pack_in_hours: 6,  room: room2, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 2.day.from_now, room: room2, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 2.day.from_now, pack_in_hours: 6, room: room2, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 2.day.from_now, pack_in_hours: 12, room: room2, status: Offer::ACCEPTED_STATUS[:reserved], room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 4.day.from_now, room: room2, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 5.day.from_now, status: Offer::ACCEPTED_STATUS[:suspended] , room: room2, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 5.day.from_now, pack_in_hours: 6, status: Offer::ACCEPTED_STATUS[:expired] , room: room2, room_type: room_type)

      #room3
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 1.day.from_now, pack_in_hours: 3, room: room3, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 2.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved] , room: room3, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 4.day.from_now, room: room3, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 4.day.from_now, pack_in_hours: 6, room: room3, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 5.day.from_now, room: room3, room_type: room_type)

      #room1
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 1.day.from_now, room: room1, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 2.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved] , room: room1, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 4.day.from_now, room: room1, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 5.day.from_now, pack_in_hours: 6, room: room1, room_type: room_type)
      FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: 5.day.from_now, status: Offer::ACCEPTED_STATUS[:expired] , room: room1, room_type: room_type)
    end

    specify do
      room_type = RoomType.first
      rooms_count = room_type.rooms_with_active_offer_count_per_day(1.day.from_now.month, 1.day.from_now.year)
      rooms_count[1.day.from_now.day].should == '3'
      rooms_count[2.day.from_now.day].should == '1'
      rooms_count[4.day.from_now.day].should == '3'
      rooms_count[5.day.from_now.day].should == '2'
    end
  end

  describe "#bed_configuration_text" do
    it "should return correctly the text for bed configuration in pt_br", :focus => true do
      room_type = RoomType.new
      data = [
        [1,0,'1 Cama de Casal'],
        [1,1,'1 Cama de Casal e 1 de Solteiro'],
        [1,2,'1 Cama de Casal e 2 de Solteiro'],
        [2,0,'2 Camas de Casal'],
        [3,0,'3 Camas de Casal'],
        [2,1,'2 Camas de Casal e 1 de Solteiro'],
        [2,2,'2 Camas de Casal e 2 de Solteiro'],
        [0,1,'1 Cama de Solteiro'],
        [0,2,'2 Camas de Solteiro']
      ]

      data.each do |input|
        room_type.double_bed_quantity = input[0]
        room_type.single_bed_quantity = input[1]
        room_type.bed_configuration_text_pt_br.should == input[2]
      end
    end

    it "should return correctly the text for bed configuration in english", :focus => true do
      room_type = RoomType.new
      data = [
        [1,0,'1 Matrimonial Bed'],
        [1,1,'1 Matrimonial Bed and 1 Single Bed'],
        [1,2,'1 Matrimonial Bed and 2 Single Beds'],
        [2,0,'2 Matrimonial Beds'],
        [3,0,'3 Matrimonial Beds'],
        [2,1,'2 Matrimonial Beds and 1 Single Bed'],
        [2,2,'2 Matrimonial Beds and 2 Single Beds'],
        [0,1,'1 Single Bed'],
        [0,2,'2 Single Beds']
      ]

      data.each do |input|
        room_type.double_bed_quantity = input[0]
        room_type.single_bed_quantity = input[1]
        room_type.bed_configuration_text_en.should == input[2]
      end
    end
  end

end
