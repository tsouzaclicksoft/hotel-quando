# encoding: utf-8

require 'ostruct'

require 'spec_helper'

describe Booking do
  it 'can be instantiated' do
    Booking.new.should be_an_instance_of(Booking)
  end

  it 'has a valid factory' do
    FactoryGirl.create(:booking).should be_persisted
  end

  specify "#tudo_azul_associate!" do
    cpf = "225.140.160-10"
    booking = FactoryGirl.create(:booking)

    booking.tudo_azul_associate!(cpf)
    booking.reload
    booking.user.reload

    expect(booking.uses_tudo_azul).to be_true
    expect(booking.user.cpf).to eq cpf
  end

  specify "#tudo_azul_unassociate!" do
    booking = FactoryGirl.create(:booking)

    booking.tudo_azul_unassociate!
    booking.reload

    expect(booking.uses_tudo_azul).to be_false
  end

  it 'validates the presence of guest name' do
    booking = FactoryGirl.build(:booking)
    booking.guest_name = nil
    booking.should_not be_valid
  end

  it "allows only valid statuses" do
    accepted_statuses = [:waiting_for_payment, :confirmed, :canceled, :no_show_paid_with_success, :no_show_with_payment_error, :confirmed_and_captured, :expired, :no_show_with_payment_pending]
    # ATTTTEEEENTION
    # Before when including a new status you should check if there is any impact on:
    # booking cancelation
    # noshow

    if accepted_statuses.sort != Booking::ACCEPTED_STATUSES.keys.sort
      raise "One does not simple inclue a new booking status. See the test for more info."
    end
  end

  it 'validates the inclusion of status in the accepted statuses hash' do
    booking = FactoryGirl.build(:booking)
    cont = 0
    256.times do | i |
      booking.status = i
      if Booking::ACCEPTED_STATUS_REVERSED.include?(i)
        cont += 1
        booking.should be_valid
      else
        booking.should_not be_valid
      end
    end
    raise 'Not all status were evaluated!' unless cont == Booking::ACCEPTED_STATUS_REVERSED.length
  end

  it "should be able to set is_active for all possible status" do
    Booking::ACCEPTED_STATUSES.each do | status_name, status_code |
      booking = FactoryGirl.build(:booking)
      booking.status = status_code
      expect { booking.save! }.to_not raise_error
    end
  end

  it "has a database rule of checkint/checkout overlap constraint for the same room", focus: true do
    user = FactoryGirl.create(:user)
    reserved_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 05:00:00 +0000'.to_datetime,
        pack_in_hours: 3,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:available],
      )

    booking = FactoryGirl.build(:booking,
      offer_id: reserved_offer.id,
      user_id: user.id,
      hotel_comments: "comment",
      hotel: reserved_offer.hotel,
      room_id: reserved_offer.room_id,
      room_type_id: reserved_offer.room_type_id,
      checkin_date: reserved_offer.checkin_timestamp,
      pack_in_hours: reserved_offer.pack_in_hours,
      status: Booking::ACCEPTED_STATUSES[:waiting_for_payment])

    booking.save!

    conflicting_available_offer1 = FactoryGirl.create( :offer,
      checkin_timestamp: '2020-03-20 05:00:00 +0000'.to_datetime,
      pack_in_hours: 6,
      price: 150.0,
      extra_price_per_person: 100,
      status: Offer::ACCEPTED_STATUS[:available],
      hotel: reserved_offer.hotel,
      room_type: reserved_offer.room_type,
      room: reserved_offer.room
    )

    conflicting_booking = FactoryGirl.build(:booking,
      offer_id: reserved_offer.id,
      user_id: user.id,
      hotel_comments: "comment",
      hotel: reserved_offer.hotel,
      room_id: reserved_offer.room_id,
      room_type_id: reserved_offer.room_type_id,
      checkin_date: reserved_offer.checkin_timestamp,
      pack_in_hours: reserved_offer.pack_in_hours,
      status: Booking::ACCEPTED_STATUSES[:waiting_for_payment])

    expect {
      conflicting_booking.save
    }.to raise_error(ActiveRecord::StatementInvalid)
  end

  describe "try_create", try_create: true do
    # the reserved offer is at '2020-03-20 05:00:00 +0000'
    # pack: 3hours
    # with a ROOM_CLEANING_TIME of 2.hour
    # we have X conflicting offers
    before(:each) do
      stub_const("Offer::ROOM_CLEANING_TIME", 2.hour)
      @reserved_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 05:00:00 +0000'.to_datetime,
        pack_in_hours: 3,
        price: 150.0,
        extra_price_per_person: 100,
        no_show_value: 50,
        status: Offer::ACCEPTED_STATUS[:available],
      )
      @room = @reserved_offer.room
      @hotel = @reserved_offer.hotel
      @room_type = @reserved_offer.room_type
      @user = FactoryGirl.create(:user)
      @booking = FactoryGirl.build(:booking,
        offer_id: @reserved_offer.id,
        user_id: @user.id,
        hotel_comments: "comment",
        hotel: @hotel,
        room_id: @reserved_offer.room_id,
        room_type_id: @reserved_offer.room_type_id,
        checkin_date: @reserved_offer.checkin_timestamp,
        pack_in_hours: @reserved_offer.pack_in_hours,
        status: Booking::ACCEPTED_STATUSES[:waiting_for_payment])

      # available offers
      @conflicting_available_offer1 = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 05:00:00 +0000'.to_datetime,
        pack_in_hours: 6,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:available],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )
      @conflicting_available_offer2 = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 01:00:00 +0000'.to_datetime,
        pack_in_hours: 6,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:available],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      @conflicting_available_offer3 = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 04:00:00 +0000'.to_datetime,
        pack_in_hours: 12,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:available],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      @conflicting_available_offer4 = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-19 22:00:00 +0000'.to_datetime,
        pack_in_hours: 12,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:available],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      @free_available_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-01 22:00:00 +0000'.to_datetime,
        pack_in_hours: 12,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:available],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      # suspended offers
      @conflicting_suspended_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 06:00:00 +0000'.to_datetime,
        pack_in_hours: 6,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:suspended],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      @free_suspended_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-01 22:00:00 +0000'.to_datetime,
        pack_in_hours: 3,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:suspended],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      # reserved

      @free_reserved_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-04 22:00:00 +0000'.to_datetime,
        pack_in_hours: 3,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:reserved],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      # removed by hotel
      @conflicting_removed_by_hotel_offer1 = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 05:00:00 +0000'.to_datetime,
        pack_in_hours: 12,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:removed_by_hotel],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )
      @conflicting_removed_by_hotel_offer2 = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 01:00:00 +0000'.to_datetime,
        pack_in_hours: 12,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:removed_by_hotel],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      @conflicting_removed_by_hotel_offer3 = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 07:00:00 +0000'.to_datetime,
        pack_in_hours: 12,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:removed_by_hotel],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      @conflicting_removed_by_hotel_offer4 = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 02:00:00 +0000'.to_datetime,
        pack_in_hours: 9,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:removed_by_hotel],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      @free_removed_by_hotel_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-01 22:00:00 +0000'.to_datetime,
        pack_in_hours: 6,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:removed_by_hotel],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      # ghosts
      @conflicting_ghost_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 02:00:00 +0000'.to_datetime,
        pack_in_hours: 6,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:ghost],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

      @free_ghost_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-01 23:00:00 +0000'.to_datetime,
        pack_in_hours: 6,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:ghost],
        hotel: @hotel,
        room_type: @room_type,
        room: @room
      )

    end

    it "creates the booking" do
      Booking.count.should == 0
      @booking.try_create(@reserved_offer.price)
      Booking.count.should == 1
      booking = Booking.first
      booking.offer_id.should == @reserved_offer.id
      booking.user_id.should == @user.id
      booking.hotel_comments.should == "comment"
      booking.hotel_id.should == @hotel.id
      booking.room_id.should == @reserved_offer.room_id
      booking.room_type_id.should == @reserved_offer.room_type_id
      booking.checkin_date.should == @reserved_offer.checkin_timestamp.to_date
      booking.pack_in_hours.should == @reserved_offer.pack_in_hours
      booking.status.should == Booking::ACCEPTED_STATUSES[:waiting_for_payment]
      booking.cached_room_type_initial_capacity.should == @reserved_offer.room_type_initial_capacity
      booking.cached_room_type_maximum_capacity.should == @reserved_offer.room_type_maximum_capacity
      booking.cached_offer_extra_price_per_person.should == @reserved_offer.extra_price_per_person
      booking.cached_offer_price.should == @reserved_offer.price
      booking.cached_offer_no_show_value.should == @reserved_offer.no_show_value
    end

    it "change the status of the reserved_offer to reserved" do
      @reserved_offer.status.should == Offer::ACCEPTED_STATUS[:available]
      @booking.try_create(@reserved_offer.price)
      @reserved_offer.reload
      @reserved_offer.status.should == Offer::ACCEPTED_STATUS[:reserved]
    end

    it "updates the available conflicting offers to suspended" do
      @booking.try_create(@reserved_offer.price)
      @conflicting_available_offer1.reload
      @conflicting_available_offer2.reload
      @conflicting_available_offer3.reload
      @conflicting_available_offer4.reload
      @conflicting_available_offer1.status.should == Offer::ACCEPTED_STATUS[:suspended]
      @conflicting_available_offer2.status.should == Offer::ACCEPTED_STATUS[:suspended]
      @conflicting_available_offer3.status.should == Offer::ACCEPTED_STATUS[:suspended]
      @conflicting_available_offer4.status.should == Offer::ACCEPTED_STATUS[:suspended]
    end

    it "doesnt affect non conflicting available offers" do
      @booking.try_create(@reserved_offer.price)
      @free_available_offer.reload
      @free_available_offer.status.should == Offer::ACCEPTED_STATUS[:available]
    end

    it "doesnt change the already reserveds offers" do
      @booking.try_create(@reserved_offer.price)
      @free_reserved_offer.reload
      @free_reserved_offer.status.should == Offer::ACCEPTED_STATUS[:reserved]
    end

    it "doesnt change the already suspendeds offers" do
      @booking.try_create(@reserved_offer.price)
      @conflicting_suspended_offer.reload
      @free_suspended_offer.reload
      @conflicting_suspended_offer.status.should == Offer::ACCEPTED_STATUS[:suspended]
      @free_suspended_offer.status.should == Offer::ACCEPTED_STATUS[:suspended]
    end

    it "doesnt change the already ghosts offers" do
      @booking.try_create(@reserved_offer.price)
      @conflicting_ghost_offer.reload
      @free_ghost_offer.reload
      @conflicting_ghost_offer.status.should == Offer::ACCEPTED_STATUS[:ghost]
      @free_ghost_offer.status.should == Offer::ACCEPTED_STATUS[:ghost]
    end

    it "creates the pending ghosts" do
      Offer.where(status: Offer::ACCEPTED_STATUS[:ghost]).count.should == 2
      @booking.try_create(@reserved_offer.price)
      # it try tro create 54. but 4 are available that will be suspendend and one is the reserved offer
      # conflicting 12hrs offers -> from [19 4:00pm to  20 9:00am] - 18
      # conflicting 9hrs offers -> from 19 [7:00pm to  20 9:00am] - 15
      # conflicting 6hrs offers -> from 19 [10:00pm to  20 9:00am] - 12
      # conflicting 3hrs offers -> from 19 [1:00am to  20 9:00am] - 9
      # total: 54

      Offer.where(status: Offer::ACCEPTED_STATUS[:ghost]).count.should == 49
    end

    it 'updates the removed_by_hotel conflicting offers to ghosts' do
      @booking.try_create(@reserved_offer.price)
      @conflicting_removed_by_hotel_offer1.reload
      @conflicting_removed_by_hotel_offer2.reload
      @conflicting_removed_by_hotel_offer3.reload
      @conflicting_removed_by_hotel_offer4.reload
      @free_removed_by_hotel_offer.reload
      @conflicting_removed_by_hotel_offer1.status.should == Offer::ACCEPTED_STATUS[:ghost]
      @conflicting_removed_by_hotel_offer2.status.should == Offer::ACCEPTED_STATUS[:ghost]
      @conflicting_removed_by_hotel_offer3.status.should == Offer::ACCEPTED_STATUS[:ghost]
      @conflicting_removed_by_hotel_offer4.status.should == Offer::ACCEPTED_STATUS[:ghost]
      @free_removed_by_hotel_offer.status.should == Offer::ACCEPTED_STATUS[:removed_by_hotel]
    end

    it "raises ActiveRecord::Rollback if there is an reservation booking conflict" do
      @booking.try_create(@reserved_offer.price)
      @conflicting_available_offer1.reload
      @conflicting_available_offer1.update_column(:status, Offer::ACCEPTED_STATUS[:available])
      user2 = FactoryGirl.create(:user)
      conflicting_booking = FactoryGirl.build(:booking,
        offer_id: @conflicting_available_offer1.id,
        user_id: @user.id,
        hotel_comments: "comment",
        hotel: @conflicting_available_offer1.hotel,
        room_id: @conflicting_available_offer1.room_id,
        room_type_id: @conflicting_available_offer1.room_type_id,
        checkin_date: @conflicting_available_offer1.checkin_timestamp,
        pack_in_hours: @conflicting_available_offer1.pack_in_hours,
        status: Booking::ACCEPTED_STATUSES[:waiting_for_payment])
      conflicting_booking.try_create(@conflicting_available_offer1.price).should be_nil
    end
  end

  describe 'expire' do
    it 'should change the booking status to expired and release the offers suspended by the booking' do
      booking = FactoryGirl.create(:booking)
      Booking.any_instance.should_receive :release_offers
      booking.expire
      booking.reload.status.should == Booking::ACCEPTED_STATUSES[:expired]
    end

    it 'should inactivate the booking' do
      booking = FactoryGirl.create(:booking)
      booking.expire
      booking.reload.is_active.should == false
    end

    it 'should do nothing and return false if the booking status is not waiting for payment' do
      Booking::ACCEPTED_STATUSES.each do | status_name, status_code |
        next if status_name == :waiting_for_payment
        booking = FactoryGirl.create(:booking, status: status_code)
        Booking.any_instance.should_not_receive :release_offers
        booking.expire.should == false
        booking.reload.status.should == status_code
      end
    end

    it 'should send an email to alert the user that his booking expired' do
      mailer_mock = double()
      mailer_mock.should_receive(:deliver)
      UserMailer.should_receive(:send_booking_expiration_notice).and_return(mailer_mock)
      booking = FactoryGirl.create(:booking)
      booking.expire
    end
  end

  describe 'cancel' do
    it 'should do nothing if the status is not waiting for payment nor confirmed' do
      Booking::ACCEPTED_STATUSES.each do | status_name, status_code |
        next if status_name == :waiting_for_payment || status_name == :confirmed || status_name == :confirmed_and_captured
        booking = FactoryGirl.create(:booking, status: status_code)
        Booking.any_instance.should_not_receive :release_offers

        expect {booking.cancel}.to raise_error(Booking::BookingStatusNotCancellableError)

        booking.reload.status.should == status_code
      end
    end

    context 'maxipago was enabled at booking creation time' do
      after :each do
        Timecop.return
      end

      it 'should change the booking status to canceled and release the offers suspended by the booking, for bookings that are waiting for payment' do
        booking = FactoryGirl.create(:booking)
        Booking.any_instance.should_receive :release_offers
        expect {booking.cancel}.to_not raise_error
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end

      it 'should inactivate the booking' do
        booking = FactoryGirl.create(:booking)
        booking.cancel
        booking.reload.is_active.should == false
      end

      it 'should change the booking status to canceled and release the offers suspended by the booking, for bookings that are confirmed' do
        booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed])
        FactoryGirl.create(:payment, booking: booking).update(authorized_at: Time.now)
        Booking.any_instance.should_receive :release_offers
        expect {booking.cancel}.to_not raise_error
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end

      it 'should do nothing if the booking is outside the cancellation period set by the hotel and cancel the booking if it is inside the cancellation period' do
        minimum_hours_of_notice = 24
        hotel = FactoryGirl.create(:hotel, minimum_hours_of_notice: minimum_hours_of_notice)
        offer = FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: (DateTime.now.beginning_of_day + 1.day))
        booking = FactoryGirl.create(:booking, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed], hotel: hotel)
        FactoryGirl.create(:payment, booking: booking).update(authorized_at: Time.now)
        # Freeze the time on the edge of the cancellation period, while the booking is still cancellable
        Timecop.freeze((offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - 1.day) - 1.minute) do
          expect { booking.cancel }.to_not raise_error
        end
        booking_2 = FactoryGirl.create(:booking, offer: booking.offer, status: Booking::ACCEPTED_STATUSES[:confirmed], hotel: hotel)
        FactoryGirl.create(:payment, booking: booking_2).update(authorized_at: Time.now)
        Booking.any_instance.should_not_receive :release_offers
        # Freeze the time on the edge of the cancellation period, the booking cant be cancelled now
        Timecop.freeze((offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - 1.day) + 1.minute) do
          expect {booking_2.reload.cancel}.to raise_error(Booking::BookingHotelMinimumHoursOfNoticeError)
          booking_2.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed]
        end
      end

      it 'should send an email to the user, telling that he canceled the booking' do
        booking = FactoryGirl.create(:booking)
        FactoryGirl.create(:payment, booking: booking).update(authorized_at: Time.now)
        mailer_mock = double()
        mailer_mock.should_receive(:deliver)
        UserMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
        booking.cancel
      end

      it 'should send an email to the hotel, telling that the user canceled the booking' do
        booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed])
        mailer_mock = double()
        mailer_mock.should_receive(:deliver)
        HotelMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
        booking.cancel
      end
    end

    context 'maxipago was disabled at booking creation time' do
      after :each do
        Timecop.return
      end

      it 'should change the booking status to canceled and release the offers suspended by the booking, for bookings that are waiting for payment' do
        booking = FactoryGirl.create(:booking)
        Booking.any_instance.should_receive :release_offers
        expect {booking.cancel}.to_not raise_error
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end

      it 'should inactivate the booking' do
        booking = FactoryGirl.create(:booking)
        booking.cancel
        booking.reload.is_active.should == false
      end

      it 'should change the booking status to canceled and release the offers suspended by the booking, for bookings that are confirmed' do
        booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed])
        Booking.any_instance.should_receive :release_offers
        expect {booking.cancel}.to_not raise_error
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end

      it 'should do nothing if the booking is outside the cancellation period set by the hotel and cancel the booking if it is inside the cancellation period' do

        minimum_hours_of_notice = 24
        hotel = FactoryGirl.create(:hotel, minimum_hours_of_notice: minimum_hours_of_notice)
        offer = FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: (DateTime.now.beginning_of_day + 1.day))
        booking = FactoryGirl.create(:booking, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed], hotel: hotel)
        # Freeze the time on the edge of the cancellation period, while the booking is still cancellable
        Timecop.freeze((offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - 1.day) - 1.minute) do
          expect { booking.cancel }.to_not raise_error
        end
        booking_2 = FactoryGirl.create(:booking, offer: booking.offer, status: Booking::ACCEPTED_STATUSES[:confirmed], hotel: hotel)
        Booking.any_instance.should_not_receive :release_offers
        # Freeze the time on the edge of the cancellation period, the booking cant be cancelled now
        Timecop.freeze((offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - 1.day) + 1.minute) do
          expect {booking_2.reload.cancel}.to raise_error(Booking::BookingHotelMinimumHoursOfNoticeError)
          booking_2.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed]
        end
      end

      it 'should send an email to the user, telling that he canceled the booking' do
        booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed])
        mailer_mock = double()
        mailer_mock.should_receive(:deliver)
        UserMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
        booking.cancel
      end

      it 'should send an email to the hotel, telling that the user canceled the booking' do
        booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed])
        mailer_mock = double()
        mailer_mock.should_receive(:deliver)
        HotelMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
        booking.cancel
      end

      it 'should send an email to admin telling that the user canceled the booking' do
        booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed])
        mailer_mock = double()
        mailer_mock.should_receive(:deliver)
        AdminMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
        booking.cancel
      end
    end
  end

  describe "set_as_no_show", noshow: true do

    it 'raise error if the booking cannot be set as noshow' do
      offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.utc + 43.hours)
      booking = FactoryGirl.create(:booking, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed])
      booking.stub(:can_be_set_as_no_show?).and_return(false)
      expect {
        booking.set_as_no_show(payment: true)
      }.to raise_error(Exception, "Reserva não pode ser setada como no-show")
    end

    it "set status to no_show_paid_with_success" do
      offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.utc - 2.hours)
      booking = FactoryGirl.create(:booking, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed])
      booking.stub(:can_be_set_as_no_show?).and_return(true)
      booking.set_as_no_show(payment: :success)
      booking.status.should == Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
    end

    it "set status to no_show_with_payment_error if receives false as argument" do
      offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.utc - 2.hours)
      booking = FactoryGirl.create(:booking, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed])
      booking.stub(:can_be_set_as_no_show?).and_return(true)
      booking.set_as_no_show(payment: :error)
      booking.status.should == Booking::ACCEPTED_STATUSES[:no_show_with_payment_error]
    end

    it "set status to no_show_with_payment_pending if receives false as argument" do
      offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.utc - 2.hours)
      booking = FactoryGirl.create(:booking, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed])
      booking.stub(:can_be_set_as_no_show?).and_return(true)
      booking.set_as_no_show(payment: :pending)
      booking.status.should == Booking::ACCEPTED_STATUSES[:no_show_with_payment_pending]
    end
  end

  describe 'total_price' do
    before(:each) do
      @offer1 = FactoryGirl.create(:offer, room_type_initial_capacity: 4, room_type_maximum_capacity: 8, price: 4, extra_price_per_person: 7)
      @offer2 = FactoryGirl.create(:offer, room_type_initial_capacity: 3, room_type_maximum_capacity: 3, price: 3, extra_price_per_person: 7)
      @offer3 = FactoryGirl.create(:offer, room_type_initial_capacity: 1, room_type_maximum_capacity: 2, price: 1, extra_price_per_person: 7)
      @offer4 = FactoryGirl.create(:offer, room_type_initial_capacity: 1, room_type_maximum_capacity: 1, price: 1, extra_price_per_person: 7)
    end

    it "should return the right price for one people" do
      booking1 = FactoryGirl.create(:booking, number_of_people: 1, offer: @offer1)
      booking1.total_price.should == 4

      booking2 = FactoryGirl.create(:booking, number_of_people: 1, offer: @offer2)
      booking2.total_price.should == 3

      booking3 = FactoryGirl.create(:booking, number_of_people: 1, offer: @offer3)
      booking3.total_price.should == 1

      booking4 = FactoryGirl.create(:booking, number_of_people: 1, offer: @offer4)
      booking4.total_price.should == 1
    end

    it "should return the right price for (number_of_people < room_type_initial_capacity) people" do
      booking1 = FactoryGirl.create(:booking, number_of_people: 3, offer: @offer1)
      booking1.total_price.should == 4

      booking2 = FactoryGirl.create(:booking, number_of_people: 2, offer: @offer2)
      booking2.total_price.should == 3
    end

    it "should return the right price for (number_of_people = room_type_initial_capacity) people" do
      booking1 = FactoryGirl.create(:booking, number_of_people: 4, offer: @offer1)
      booking1.total_price.should == 4

      booking2 = FactoryGirl.create(:booking, number_of_people: 3, offer: @offer2)
      booking2.total_price.should == 3

      # booking3 already tested above

      # booking4 already tested above
    end

    it "should return the right price for (room_type_initial_capacity < number_of_people < room_type_maximum_capacity) people" do
      booking1 = FactoryGirl.create(:booking, number_of_people: 6, offer: @offer1)
      # offer_price + (extra_price_per_person * (number_of_people - room_type_initial_capacity) )
      booking1.total_price.should == (4 + (7*(6-4)))
    end

    it "should return the right price for (number_of_people = room_type_maximum_capacity) people" do
      booking1 = FactoryGirl.create(:booking, number_of_people: 8, offer: @offer1)

      booking1.total_price.should == (4 + (7*(8-4)))

      # booking2 already tested above

      booking3 = FactoryGirl.create(:booking, number_of_people: 2, offer: @offer3)
      booking3.total_price.should == 8

      # booking4 already tested above
    end

    it "should not return the price for (number_of_people > room_type_maximum_capacity) people" do
      booking1 = FactoryGirl.create(:booking, offer: @offer1)
      booking1.number_of_people = 30
      booking1.total_price.should == -1

      booking2 = FactoryGirl.create(:booking, offer: @offer2)
      booking2.number_of_people = 30
      booking2.total_price.should == -1

      booking3 = FactoryGirl.create(:booking, offer: @offer3)
      booking3.number_of_people = 30
      booking3.total_price.should == -1

      booking4 = FactoryGirl.create(:booking, offer: @offer4)
      booking4.number_of_people = 30
      booking4.total_price.should == -1
    end
  end

  describe 'release_offers' do
    it 'should change the booking offer status to available' do
      booking = FactoryGirl.create(:booking)
      booking.send(:release_offers)
      booking.reload.offer.status.should == Offer::ACCEPTED_STATUS[:available]
    end

    it 'should do nothing with reserved offers' do
      booking_offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.beginning_of_day + 1.day)
      booking = FactoryGirl.create(:booking, offer: booking_offer)
      conflicting_offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.beginning_of_day + 1.day + 1.hour, hotel: booking_offer.hotel, room: booking_offer.room, status: Offer::ACCEPTED_STATUS[:reserved])
      conflicting_offer_attributes = conflicting_offer.attributes
      booking.send(:release_offers)
      conflicting_offer.reload.attributes.should == conflicting_offer_attributes
    end

    it 'should do nothing with suspended offers that have a conflicting reserved offer' do
      booking_offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.beginning_of_day + 1.day)
      booking = FactoryGirl.create(:booking, offer: booking_offer)
      conflicting_offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.beginning_of_day + 1.day + 1.hour, hotel: booking_offer.hotel, room: booking_offer.room, status: Offer::ACCEPTED_STATUS[:reserved])
      conflicting_suspended_offer = FactoryGirl.create(:offer, checkin_timestamp: DateTime.now.beginning_of_day + 1.day + 2.hour, hotel: booking_offer.hotel, room: booking_offer.room, status: Offer::ACCEPTED_STATUS[:suspended])
      conflicting_suspended_offer_attributes = conflicting_suspended_offer.attributes
      booking.send(:release_offers)
      conflicting_suspended_offer.reload.attributes.should == conflicting_suspended_offer_attributes
    end
  end

  describe 'can_be_set_as_no_show?' do
    specify { Booking::LIMIT_DAY_TO_SET_NOSHOW.should == 4 }
    before(:each) do
      mock_offer = double()
      mock_offer.stub(:checkin_timestamp).and_return('%2020-03-16 14:00:00 +0000'.to_time.utc)
      @booking = Booking.new
      @booking.stub(:offer).and_return(mock_offer)
      @booking.status = @booking.status = Booking::ACCEPTED_STATUSES[:confirmed]
    end

    it "returns false if status is != confirmed" do
      fake_time1 = '%2020-03-20 16:00:00 +0000'.to_time.utc
      Time.stub(:now).and_return(fake_time1)
      @booking.status = Booking::ACCEPTED_STATUSES[:waiting_for_payment]
      @booking.can_be_set_as_no_show?.should == false
      @booking.status = Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
      @booking.can_be_set_as_no_show?.should == false
      @booking.status = Booking::ACCEPTED_STATUSES[:no_show_with_payment_error]
      @booking.can_be_set_as_no_show?.should == false
      @booking.status = Booking::ACCEPTED_STATUSES[:expired]
      @booking.can_be_set_as_no_show?.should == false
    end

    it "returns true if the current time is between the booking checkin and the next 4th day" do
      fake_time1 = '%2020-03-20 16:00:00 +0000'.to_time.utc
      fake_time2 = '%2020-04-04 16:00:00 +0000'.to_time.utc
      fake_time3 = '%2020-03-16 16:00:00 +0000'.to_time.utc
      fake_time4 = '%2020-03-27 16:00:00 +0000'.to_time.utc
      fake_time5 = '%2020-03-28 16:00:00 +0000'.to_time.utc
      Time.stub(:now).and_return(fake_time1)
      @booking.can_be_set_as_no_show?.should == true
      Time.stub(:now).and_return(fake_time2)
      @booking.can_be_set_as_no_show?.should == true
      Time.stub(:now).and_return(fake_time3)
      @booking.can_be_set_as_no_show?.should == true
      Time.stub(:now).and_return(fake_time4)
      @booking.can_be_set_as_no_show?.should == true
      Time.stub(:now).and_return(fake_time5)
      @booking.can_be_set_as_no_show?.should == true
    end

    it "returns false if the current time is before the checkin date" do
      fake_time = '%2020-03-16 13:00:00 +0000'.to_time.utc
      Time.stub(:now).and_return(fake_time)
      @booking.can_be_set_as_no_show?.should == false
    end

    it "returns false if the current time is after the 4th day of the next month" do
      fake_time = '%2020-04-05 01:00:00 +0000'.to_time.utc
      Time.stub(:now).and_return(fake_time)
      @booking.can_be_set_as_no_show?.should == false
    end
  end

  it "should raise an error if it doesn't know if it's status is active" do
    Booking::ACCEPTED_STATUSES.merge!(test: 55)
    booking = FactoryGirl.create(:booking)
    booking.status = 55
    expect { booking.save! }.to raise_error
    Booking::ACCEPTED_STATUSES.delete(:test)
  end

  describe 'methods that return the booking prices' do
    before(:each) do
      hotel = FactoryGirl.create(:hotel, iss_in_percentage: 5)
      @offer = FactoryGirl.create(:offer, price: 30, extra_price_per_person: 10, no_show_value: 40, hotel: hotel)
      @booking = FactoryGirl.create(:booking, offer: @offer, number_of_people: 5)
    end

    describe 'no_show_value' do
      it 'returns the no_show_value that was on the offer at the moment of booking creation time' do
        @offer.update!(no_show_value: 999)
        @booking.no_show_value.should == 40
      end
    end

    describe 'initial_price' do
      it 'returns the initial price that was on the offer at the moment of booking creation time' do
        @offer.update!(price: 999)
        @booking.initial_price.should == 30
      end
    end


    describe 'extra_price_per_person' do
      it 'returns the extra price per person that was on the offer at the moment of booking creation time' do
        @offer.update!(extra_price_per_person: 999)
        @booking.extra_price_per_person.should == 10
      end
    end

    describe 'extra_people_price' do
      it 'returns the extra people price' do
        @booking.extra_people_price.should == 40
      end
    end

    describe 'extra_people_price_without_iss' do
      it 'returns the extra people price without iss' do
        @booking.extra_people_price_without_iss.round(2).should == 38.10
      end
    end

    describe 'total_price_without_iss_and_without_tax' do
      it 'returns the total price without iss' do
        @booking.total_price.should == 70
        @booking.total_price_without_iss_and_without_tax.round(2).should == 66.67
      end
    end

    describe 'extra_price_per_person_without_iss' do
      it 'returns the extra price per person without iss' do
        @booking.extra_price_per_person_without_iss.round(2).should == 9.52
      end
    end
  end

  describe 'cancelable?' do
    before(:each) do
      @booking = FactoryGirl.create(:booking)
      Hotel.where(id: @booking.hotel_id).update_all(minimum_hours_of_notice: 0)
    end
    after :each do
      Timecop.return
    end

    it 'should return false if the booking is outside the cancellation period' do
      @booking.update(hotel: FactoryGirl.create(:hotel, minimum_hours_of_notice: 24))
      # Freeze the time on the edge of the cancellation period, the booking cant be cancelled
      Timecop.freeze((@booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - 1.day) + 1.minute) do
        @booking.cancelable?.should == false
      end
    end

    it 'should return false if the hotel does not accept cancellation' do
      @booking.update(hotel: FactoryGirl.create(:hotel, minimum_hours_of_notice: -1))
      @booking.cancelable?.should == false
    end

    it 'should return false if the booking status is not confirmed (or captured) nor waiting for payment' do
      Booking::ACCEPTED_STATUSES.each do | status_name, status_code |
        next if status_name == :waiting_for_payment || status_name == :confirmed || status_name == :confirmed_and_captured
        @booking.update(status: status_code)
        @booking.cancelable?.should == false
      end
    end

    it 'should return true if the booking can be cancelled' do
      # Freeze the time on the edge of the cancellation period, while the booking is still cancelable
      Timecop.freeze((@booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M -0300').to_datetime - 1.day) - 1.minute) do
        @booking.cancelable?.should == true
      end
    end
  end

  describe 'confirmed_by_maxipago?' do
    # We can know if maxipago was enabled at booking creation time by verifying if it have any payments
    # because when maxipago is disabled, no payments are created
    before(:each) do
      @booking = FactoryGirl.create(:booking)
    end

    it 'should return true if there are any booking payments' do
      FactoryGirl.create(:payment, booking: @booking)
      @booking.confirmed_by_maxipago?.should == true
    end

    it 'should return false if there are no booking payments' do
      @booking.confirmed_by_maxipago?.should == false
    end
  end

  describe 'can_accuse_delayed_checkout?' do
    after :each do
      Timecop.return
    end

    it 'returns true if the booking status is confirmed or paid and if the booking checkout is one hour or more in the past' do
      booking = FactoryGirl.create(:booking)
      Timecop.freeze(booking.offer.checkout_timestamp.strftime('%Y-%m-%d %H:%M:%S -0300').to_time + 1.hour) do
        booking.update(status: Booking::ACCEPTED_STATUSES[:confirmed])
        booking.can_accuse_delayed_checkout?.should == true
        booking.update(status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
        booking.can_accuse_delayed_checkout?.should == true
      end
    end

    it 'returns false if the booking status is not confirmed nor paid' do
      booking = FactoryGirl.create(:booking)
      Timecop.freeze(booking.offer.checkout_timestamp.strftime('%Y-%m-%d %H:%M:%S -0300').to_time + 1.hour) do
        Booking::ACCEPTED_STATUSES.each do | status, status_code |
          next if status.in?([:confirmed, :confirmed_and_captured])
          booking.update(status: status_code)
          booking.can_accuse_delayed_checkout?.should == false
        end
      end
    end

    it 'returns false if the booking checkout is not at least one hour in the past' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed])
      Timecop.freeze(booking.offer.checkout_timestamp.strftime('%Y-%m-%d %H:%M:%S -0300').to_time + 1.hour - 1.minute) do
        booking.can_accuse_delayed_checkout?.should == false
      end
    end

  end

  describe 'accuse_delayed_checkout' do
    it 'calls can_accuse_delayed_checkout? to check if the booking can be accused with delayed checkout' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed])
      booking.should_receive(:can_accuse_delayed_checkout?).and_return(true)
      booking.accuse_delayed_checkout
    end

    it 'raises if the booking cant be accused with delayed checkout' do
      booking = FactoryGirl.create(:booking)
      booking.stub(:can_accuse_delayed_checkout?).and_return(false)
      expect { booking.accuse_delayed_checkout }.to raise_error
    end

    it 'sets the booking delayed checkout flag to true' do
      booking = FactoryGirl.create(:booking)
      booking.stub(:can_accuse_delayed_checkout?).and_return(true)
      booking.accuse_delayed_checkout
      booking.reload
      booking.delayed_checkout_flag.should == true
    end
  end

  describe 'guest_delayed_checkout?' do
    it 'returns the delayed checkout flag value' do
      booking = FactoryGirl.create(:booking)
      booking.delayed_checkout_flag = true
      booking.guest_delayed_checkout?.should == true
      booking.delayed_checkout_flag = false
      booking.guest_delayed_checkout?.should == false
    end
  end

  describe 'booking_tax' do
    it 'returns the booking tax constant if the booking is a new record and booking tax didnt changed' do
      new_booking = FactoryGirl.build(:booking)
      new_booking.booking_tax.should == Booking::BOOKING_TAX_IN_BR_CURRENCY
    end

    it 'returns the stored value if the booking is a new record and the booking tax changed' do
      new_booking_with_changed_tax = FactoryGirl.build(:booking)
      new_booking_with_changed_tax.booking_tax = 12
      new_booking_with_changed_tax.booking_tax.should == 12
    end

    it 'returns the stored value if the booking is already in the database' do
      persisted_booking = FactoryGirl.create(:booking, booking_tax: 12)
      persisted_booking.booking_tax.should == 12
    end
  end
end
