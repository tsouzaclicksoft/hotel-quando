# encoding: utf-8

require 'spec_helper'

describe Refund do

  it "has a valid factory" do
    FactoryGirl.create(:refund).should be_persisted
  end

  it { should belong_to :user }
  it { should belong_to :payment }

  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:payment) }
  it { should validate_presence_of(:value) }


  it "updates authorized_at when status changes to success" do
    refund = FactoryGirl.create(:refund, status: Refund::ACCEPTED_STATUSES[:error])
    refund.authorized_at.should == nil
    refund.status = Refund::ACCEPTED_STATUSES[:success]
    refund.save!
    refund.reload
    refund.authorized_at.should_not be_nil
    refund.authorized_at.day.should == DateTime.now.day
  end


end
