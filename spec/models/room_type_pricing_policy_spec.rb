# encoding: utf-8

require 'spec_helper'

describe RoomTypePricingPolicy do

  it 'can be instantiated' do
    RoomTypePricingPolicy.new.should be_an_instance_of(RoomTypePricingPolicy)
  end

  it 'has a valid factory' do
    FactoryGirl.build(:room_type_pricing_policy).should be_valid
  end

  it { should belong_to(:pricing_policy) }

  it { should belong_to(:room_type) }

  it { should validate_presence_of(:pack_price_3h) }

  it { should validate_presence_of(:pack_price_6h) }

  it { should validate_presence_of(:pack_price_9h) }

  it { should validate_presence_of(:pack_price_12h) }

  it { should validate_presence_of(:pack_price_24h) }

  it { should validate_presence_of(:extra_price_per_person_for_pack_price_3h) }

  it { should validate_presence_of(:extra_price_per_person_for_pack_price_6h) }

  it { should validate_presence_of(:extra_price_per_person_for_pack_price_9h) }

  it { should validate_presence_of(:extra_price_per_person_for_pack_price_12h) }

  it { should validate_presence_of(:extra_price_per_person_for_pack_price_24h) }


  describe 'all_prices_are_zero?' do
    it 'should return true if all prices are zero' do
      room_type_pricing_policy = FactoryGirl.build(:room_type_pricing_policy, pack_price_3h: 0, pack_price_6h: 0, pack_price_9h: 0, pack_price_12h: 0, pack_price_24h: 0, pack_price_36h: 0, pack_price_48h: 0)
      room_type_pricing_policy.all_prices_are_zero?.should == true
    end

    it 'should return false if there is any price that is not zero' do
      room_type_pricing_policy = FactoryGirl.build(:room_type_pricing_policy, pack_price_3h: 1, pack_price_6h: 0, pack_price_9h: 0, pack_price_12h: 0, pack_price_24h: 0, pack_price_36h: 0, pack_price_48h: 0)
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_6h = 1
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_9h = 1
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_12h = 1
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_24h = 1
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_24h = 0
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_3h = 0
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_12h = 0
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_6h = 0
      room_type_pricing_policy.all_prices_are_zero?.should == false
      room_type_pricing_policy.pack_price_9h = 0
      room_type_pricing_policy.all_prices_are_zero?.should == true
    end
  end

end
