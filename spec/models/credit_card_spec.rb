# encoding: utf-8

require 'spec_helper'

describe CreditCard do

  it "has a valid factory" do
    FactoryGirl.create(:credit_card).should be_persisted
  end

  it { should belong_to :user }

  it "should be invalid if flag_name is not set" do
    credit_card = FactoryGirl.build(:credit_card)
    credit_card.maxipago_token = nil
    credit_card.should_not be_valid
  end

  it "should be invalid if flag_name is not set" do
    credit_card = FactoryGirl.build(:credit_card)
    credit_card.flag_name = nil
    credit_card.should_not be_valid
  end

  it 'should be invalid if expiration date is in the past' do
    credit_card = FactoryGirl.build(:credit_card, expiration_year: Date.today.year, expiration_month: (Date.today.month - 1))
    credit_card.should_not be_valid
    credit_card.expiration_month = Date.today.month
    credit_card.should be_valid
  end

  it "should be invalid if billing_name is not set" do
    FactoryGirl.build(:credit_card, billing_name: nil).should_not be_valid
  end

  it 'should be invalid if the name is without last name' do
    FactoryGirl.build(:credit_card, billing_name: 'NomeInvalido').should_not be_valid
  end

  it "should be invalid if last_four_digits is not set" do
    credit_card = FactoryGirl.build(:credit_card)
    credit_card.last_four_digits = nil
    credit_card.should_not be_valid
  end

  it "should be invalid if expiration_year is not set" do
    FactoryGirl.build(:credit_card, expiration_year: nil).should_not be_valid
  end

  it "should be invalid if expiration_month is not set" do
    FactoryGirl.build(:credit_card, expiration_month: nil).should_not be_valid
  end

  it "should be invalid if user is not set" do
    FactoryGirl.build(:credit_card, user: nil).should_not be_valid
  end

  it "should be invalid if billing_address1 is not set" do
    FactoryGirl.build(:credit_card, billing_address1: nil).should_not be_valid
  end

  it "should be invalid if billing_city is not set" do
    FactoryGirl.build(:credit_card, billing_city: nil).should_not be_valid
  end

  it "should be invalid if billing_state is not set" do
    FactoryGirl.build(:credit_card, billing_state: nil).should_not be_valid
  end

  it "should be invalid if billing_zipcode is not set" do
    FactoryGirl.build(:credit_card, billing_zipcode: nil).should_not be_valid
  end

  it "should be invalid if billing_country is not set" do
    FactoryGirl.build(:credit_card, billing_country: nil).should_not be_valid
  end

  it "should be invalid if billing_phone is not set" do
    FactoryGirl.build(:credit_card, billing_phone: nil).should_not be_valid
  end

  it 'should be valid if the owner cpf is present but not the owner passport' do
    FactoryGirl.build(:credit_card, owner_passport: nil).should be_valid
  end

  it 'should be valid if the owner passport is present but not the owner cpf' do
    FactoryGirl.build(:credit_card, owner_cpf: nil).should be_valid
  end

  it 'should validate the owner cpf even if the owner passport is present' do
    FactoryGirl.build(:credit_card, owner_cpf: '123.123.123-89').should_not be_valid
  end

  it 'should be invalid if the owner cpf and the owner passport are not present' do
    FactoryGirl.build(:credit_card, owner_cpf: nil, owner_passport: nil).should_not be_valid
  end

  it 'should return the billing_zipcode without any spaces or hyphens' do
    credit_card = FactoryGirl.create(:credit_card, billing_zipcode: '123456-789 ')
    credit_card.billing_zipcode.should == '123456789'
  end

  it 'should return the billing_phone without any spaces or hyphens or parentheses' do
    credit_card = FactoryGirl.create(:credit_card, billing_phone: '(12) 3456-7890 ')
    credit_card.billing_phone.should == '1234567890'
  end

  it 'should be invalid if the country is not brasil and the state is not ZZ' do
    credit_card = FactoryGirl.build(:credit_card)
    credit_card.billing_country = 'US'
    credit_card.billing_state = 'TO'
    credit_card.should_not be_valid
  end

  it 'should be invalid if the country is brasil and the state is ZZ' do
    credit_card = FactoryGirl.build(:credit_card)
    credit_card.billing_country = 'BR'
    credit_card.billing_state = 'ZZ'
    credit_card.should_not be_valid
  end

  it "validates if temporary_number is valid" do
    credit_card = FactoryGirl.build(:credit_card)
    credit_card.should be_valid
    credit_card.temporary_number = "2432423423423545"
    credit_card.should_not be_valid
    credit_card.should have(1).error_on(:temporary_number)
  end

  describe 'security_code=' do
    credit_card = FactoryGirl.create(:credit_card)
    credit_card.security_code = 342
    credit_card.save!
    credit_card.reload
    credit_card = CreditCard.last
    credit_card.security_code.should == nil
  end

  describe 'temporary_number=' do

    it "doens't save in the database" do
      credit_card = FactoryGirl.create(:credit_card)
      credit_card.temporary_number = "5500005555555559"
      credit_card.save!
      credit_card.reload
      credit_card = CreditCard.last
      credit_card.temporary_number.should == nil
    end

    it "sets the last_four_digits field" do
      credit_card = CreditCard.new
      credit_card.temporary_number = "5500005555555559"
      credit_card.last_four_digits.should == 5559
    end

    it "should set flag_name" do
      credit_card = CreditCard.new
      credit_card.temporary_number = "4444444444444448"
      credit_card.flag_name.should == 'visa'
      credit_card.temporary_number = "5500005555555559"
      credit_card.flag_name.should == 'master_card'
    end
  end

end
