require 'spec_helper'

describe User do
  it { should validate_presence_of(:name) }
  it { should have_many(:credit_cards) }
  it { should have_many(:bookings) }
  it { should have_many(:payments) }

  it 'has a valid factory' do
    FactoryGirl.create(:user).should be_persisted
  end

  specify "first_name" do
    FactoryGirl.build(:user, name: 'jaCinto Test da Silva').first_name.should == "jaCinto"
  end

  specify "last_name" do
    FactoryGirl.build(:user, name: 'jaCinto Test da Silva').last_name.should == "Test da Silva"
  end

  it 'should be invalid if the name is without last name' do
    FactoryGirl.build(:user, name: 'NomeInvalido').should_not be_valid
  end
end
