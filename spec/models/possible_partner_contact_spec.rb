# encoding: utf-8

require 'spec_helper'

describe PossiblePartnerContact do

  it 'can be instantiated' do
    PossiblePartnerContact.new.should be_an_instance_of(PossiblePartnerContact)
  end

  it 'has a valid factory' do
    FactoryGirl.create(:possible_partner_contact).should be_valid
  end

  it 'should be invalid if name is not set' do
    FactoryGirl.build(:possible_partner_contact, name: nil).should_not be_valid
  end

  it 'should be invalid if there are no contacts set' do
    FactoryGirl.build(:possible_partner_contact, phone: nil, email_for_contact: nil).should_not be_valid
  end

  it 'should be valid if there are is one contact set but not the other' do
    FactoryGirl.create(:possible_partner_contact, phone: nil).should be_valid
    FactoryGirl.create(:possible_partner_contact, email_for_contact: nil).should be_valid
  end

  it 'should be invalid if message is not set' do
    FactoryGirl.build(:possible_partner_contact, message: nil).should_not be_valid
  end

  it 'should send an email to admin when saved' do
    AdminMailer.any_instance.should_receive :send_new_possible_partner_contact
    FactoryGirl.create(:possible_partner_contact)
  end

end
