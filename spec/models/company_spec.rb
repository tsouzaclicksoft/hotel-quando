require 'spec_helper'

describe Company do
  it 'has a valid factory' do
    FactoryGirl.create(:company).should be_persisted
  end

  it { should validate_presence_of :name }

  it { should validate_presence_of :email}

  it { should validate_presence_of :cnpj}

  describe 'before saving' do
    it 'sets a password if it is a new record and a password is not present' do
      company = Company.new
      company.name = 'Company Name'
      company.email = 'email@email.com'
      expect{ company.save }.to change{ company.encrypted_password }
      company.password.should_not be_nil
    end
  end
end
