# encoding: utf-8

require 'spec_helper'

describe Offer do

  it 'can be instantiated' do
    Offer.new.should be_an_instance_of(Offer)
  end

  it 'has a valid factory' do
    FactoryGirl.create(:offer).should be_persisted
  end

  it { should validate_presence_of(:checkin_timestamp) }

  it { should validate_presence_of(:pack_in_hours) }

  it { should validate_presence_of(:price) }

  it { should validate_presence_of(:room_type_id) }

  it { should validate_presence_of(:extra_price_per_person) }

  it { should validate_presence_of(:no_show_value) }

  it { should validate_presence_of(:room_id) }

  it { should validate_presence_of(:hotel_id) }

  it { should validate_presence_of(:room_type_initial_capacity) }

  it { should validate_presence_of(:room_type_maximum_capacity) }

  it { should belong_to(:hotel) }

  it { should belong_to(:room) }

  it 'should be invalid if price is not greater than 0' do
    offer = FactoryGirl.build(:offer)
    offer.price = -1
    offer.should_not be_valid
  end

  it "allows only valid statuses" do
    accepted_statuses = [:available, :suspended, :reserved, :removed_by_hotel, :expired, :ghost]

    # ATTTTEEEENTION
    # Before when including a new status you should check if there is any impact on:
    # CreateOfferWorker, logico for generetate insert query for ghosts,
    # booking try_create should update this new status to what
    # if the offer is conflicting with the reserved offer?

    if accepted_statuses.sort != Offer::ACCEPTED_STATUS.keys.sort
      raise "One does not simple inclue a new offer status. See the test for more info."
    end

  end

  describe "has unique index on room, checkin, pack_size" do
    it "doesn't raise an error if there is already an offer with the same combinations"
  end

  it 'should be invalid if extra price per person is not greater than or equal to 0' do
    offer = FactoryGirl.build(:offer)
    offer.extra_price_per_person = -1
    offer.should_not be_valid
  end

  it 'should be invalid if pack in hours is not in the list of accepted lengths' do
    offer = FactoryGirl.build(:offer)
    offer.pack_in_hours = 1
    offer.should_not be_valid
  end

  it 'should set the checkout datetime if it is not present' do
    offer = FactoryGirl.build(:offer, checkout_timestamp: nil)
    offer.save
    offer.reload
    offer.checkout_timestamp.should_not == nil
  end

  it "should skip timezone conversion for checkin_timestamp and checkout_timestamp" do
    offer = FactoryGirl.build(:offer, checkin_timestamp: Time.new(2020, 1, 24, 12,0,0), pack_in_hours: 6)
    offer.save!
    Offer.where("checkin_timestamp = '2020-01-24 12:00:00'").count.should == 1
    Offer.where("checkout_timestamp = '2020-01-24 18:00:00'").count.should == 1
  end

  # describe 'get_conflicting_offers_of' do
  #   it 'should get the conflicting offers' do
  #     hotel = FactoryGirl.create(:hotel)
  #     room_type = FactoryGirl.create(:room_type)
  #     room = FactoryGirl.create(:room)
  #     FactoryGirl.create_list(:offer, 1000, room_type: room_type, hotel: hotel, room: room)
  #     offer = Offer.find 500
  #     ghost = Booking.new.send(:ghost_query_for, offer).split(/\'\);/).map(&:last)
  #     conflicting_offers_ids = Offer.get_conflicting_offers_of offer
  #     #adapting to array, for testings
  #     conflicting_offers_ids = conflicting_offers_ids.slice(1..-2).split(/, /)
  #     conflicting_offers_ids.count.should > 0
  #     conflicting_offers_ids.count.should <= ghost.count
  #     ghost.count.should >= 650
  #   end
  # end

  describe 'self.get_similar_offers_from_pack_in_hours_and_checkin' do
    it 'should get the similar offers' do
      room = FactoryGirl.create(:room)
      hotel = room.hotel
      room_type = room.room_type
      10.times do
        FactoryGirl.create(:offer, room: room, room_type: room_type, hotel: hotel)
      end
      FactoryGirl.reload
      10.times do
        FactoryGirl.create(:offer, room: room, room_type: room_type, hotel: hotel, pack_in_hours: 6)
      end
      FactoryGirl.reload
      10.times do
        FactoryGirl.create(:offer, room: room, room_type: room_type, hotel: hotel, pack_in_hours: 9)
      end
      FactoryGirl.reload
      10.times do
        FactoryGirl.create(:offer, room: room, room_type: room_type, hotel: hotel, pack_in_hours: 12)
      end

      Offer.get_similar_offers_from_pack_in_hours_and_checkin(3, (Offer.where(pack_in_hours: 3)).second.checkin_timestamp).count.should == 8
      Offer.get_similar_offers_from_pack_in_hours_and_checkin(6, (Offer.where(pack_in_hours: 6)).second.checkin_timestamp).count.should == 11
      Offer.get_similar_offers_from_pack_in_hours_and_checkin(9, (Offer.where(pack_in_hours: 9)).second.checkin_timestamp).count.should == 13
      Offer.get_similar_offers_from_pack_in_hours_and_checkin(12, (Offer.where(pack_in_hours: 12)).second.checkin_timestamp).count.should == 11
    end
  end

  describe 'get_conflicting_offers_ids_with_status_in' do
    it 'should get the conflicting offers with the status included in the array' do
      room = FactoryGirl.create(:room)
      hotel = room.hotel
      room_type = room.room_type
      10.times do
        FactoryGirl.create(:offer, room: room, room_type: room_type, hotel: hotel)
      end
      offers = Offer.all
      offers.first.get_conflicting_offers_ids_with_status_in([Offer::ACCEPTED_STATUS[:available]]).count.should == 3
      offers.second.get_conflicting_offers_ids_with_status_in([Offer::ACCEPTED_STATUS[:available]]).count.should == 4
      offers.third.get_conflicting_offers_ids_with_status_in([Offer::ACCEPTED_STATUS[:available]]).count.should == 5
      offers.fourth.get_conflicting_offers_ids_with_status_in([Offer::ACCEPTED_STATUS[:available]]).count.should == 6
      offers.third.get_conflicting_offers_ids_with_status_in([Offer::ACCEPTED_STATUS[:suspended]]).count.should == 0
      offers.third.get_conflicting_offers_ids_with_status_in([Offer::ACCEPTED_STATUS[:reserved]]).count.should == 0
      offers.fourth.get_conflicting_offers_ids_with_status_in([Offer::ACCEPTED_STATUS[:available], Offer::ACCEPTED_STATUS[:reserved], Offer::ACCEPTED_STATUS[:suspended]]).count.should == 6
    end
  end

  describe 'should get correct scope when asked' do
    it 'should get correct hotel id' do
      hotel_1 = FactoryGirl.create(:hotel)
      hotel_2 = FactoryGirl.create(:hotel)
      offer_1_1 = FactoryGirl.create(:offer, :hotel => hotel_1)
      offer_1_2 = FactoryGirl.create(:offer, :hotel => hotel_1)
      offer_2_1 = FactoryGirl.create(:offer, :hotel => hotel_2)

      Offer.by_hotel_id(hotel_1.id).to_a.count.should be 2
      Offer.by_hotel_id(hotel_2.id).to_a.count.should be 1
    end

    it 'should get correct month of a year' do
      offer_1_1 = FactoryGirl.create(:offer, { :checkin_timestamp => Time.new(2020, 1, 24) })
      offer_1_2 = FactoryGirl.create(:offer, { :checkin_timestamp => Time.new(2020, 1, 23) })
      offer_1_3 = FactoryGirl.create(:offer, { :checkin_timestamp => Time.new(2020, 1, 24) })
      offer_2_1 = FactoryGirl.create(:offer, { :checkin_timestamp => Time.new(2020, 2, 1) })
      offer_2_2 = FactoryGirl.create(:offer, { :checkin_timestamp => Time.new(2020, 2, 2) })
      offer_3 = FactoryGirl.create(:offer, { :checkin_timestamp => Time.new(2020, 3, 26) })

      Offer.by_month_of_a_year(1, 2020).to_a.count.should be 3
      Offer.by_month_of_a_year(3, 2020).to_a.count.should be 1
    end

    describe 'offers_count_per_day' do
      before(:each) do
        FactoryGirl.create(:offer, checkin_timestamp: 1.day.from_now)
        FactoryGirl.create(:offer, checkin_timestamp: 1.day.from_now)
        FactoryGirl.create(:offer, checkin_timestamp: 1.day.from_now)
        FactoryGirl.create(:offer, checkin_timestamp: 2.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved] )
        FactoryGirl.create(:offer, checkin_timestamp: 2.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved] )
        FactoryGirl.create(:offer, checkin_timestamp: 2.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved] )
        FactoryGirl.create(:offer, checkin_timestamp: 2.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved] )
        FactoryGirl.create(:offer, checkin_timestamp: 5.day.from_now, status: Offer::ACCEPTED_STATUS[:suspended] )
        FactoryGirl.create(:offer, checkin_timestamp: 5.day.from_now, status: Offer::ACCEPTED_STATUS[:expired] )
      end
      specify do
        offers_query = Offer.offers_count_per_day
        offers_query[5.day.from_now.day].should == 2
        offers_query[2.day.from_now.day].should == 4
        offers_query[1.day.from_now.day].should == 3
      end
    end

    describe '#by_status' do
      before(:each) do
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:available] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:available] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:available] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:reserved] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:reserved] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:reserved] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:reserved] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:suspended] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:expired] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:expired] )
        FactoryGirl.create(:offer, status: Offer::ACCEPTED_STATUS[:ghost] )
      end

      specify do
        available_and_reserved = Offer.by_status([Offer::ACCEPTED_STATUS[:available], Offer::ACCEPTED_STATUS[:reserved]]).to_a.size
        suspended_and_expired = Offer.by_status([Offer::ACCEPTED_STATUS[:suspended], Offer::ACCEPTED_STATUS[:expired]]).to_a.size
        ghost = Offer.by_status([Offer::ACCEPTED_STATUS[:ghost]]).to_a.size
        available_and_reserved.should == 7
        suspended_and_expired.should == 3
        ghost.should == 1
      end
    end

    describe '#by_checkin_range' do
      before(:each) do
        FactoryGirl.create(:offer, checkin_timestamp: 1.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 2.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 3.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 4.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 5.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 6.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 7.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 8.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 8.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 9.day.from_now )
        FactoryGirl.create(:offer, checkin_timestamp: 10.day.from_now )
      end

      specify do

        range_5_days = Offer.by_checkin_range(Date.today, Date.today.next_day(5))
        range_8_days = Offer.by_checkin_range(Date.today, Date.today.next_day(8))
        range_9_days = Offer.by_checkin_range(Date.today, Date.today.next_day(9))

        range_5_days.count.should == 5
        range_8_days.count.should == 9
        range_9_days.count.should == 10
      end
    end

    describe '#by_room_type_id' do
      before(:each) do
        hotel = FactoryGirl.create(:hotel)
        @room_type_1 = FactoryGirl.create(:room_type, hotel: hotel)
        @room_type_2 = FactoryGirl.create(:room_type, hotel: hotel)
        @room_type_3 = FactoryGirl.create(:room_type, hotel: hotel)

        FactoryGirl.create(:offer, room_type: @room_type_1, hotel: hotel)
        FactoryGirl.create(:offer, room_type: @room_type_2, hotel: hotel)
        FactoryGirl.create(:offer, room_type: @room_type_2, hotel: hotel)
        FactoryGirl.create(:offer, room_type: @room_type_3, hotel: hotel)
        FactoryGirl.create(:offer, room_type: @room_type_3, hotel: hotel)
        FactoryGirl.create(:offer, room_type: @room_type_3, hotel: hotel)
      end

      specify do
        room_types_1 = Offer.by_room_type_id([@room_type_1.id]).to_a.size
        room_types_1_2 = Offer.by_room_type_id([@room_type_1.id, @room_type_2.id]).to_a.size
        room_types_1_2_3 = Offer.by_room_type_id([@room_type_1.id, @room_type_2.id, @room_type_3.id]).to_a.size

        room_types_1.should == 1
        room_types_1_2.should == 3
        room_types_1_2_3.should == 6
      end
    end
  end

  describe 'get_number_of_common_hours_to_checkin_and_checkout' do
    it 'should return the number of common hours based on a checkin and checkout' do
      offer = FactoryGirl.create(:offer)
      offer.get_number_of_common_hours_to_checkin_and_checkout((offer.checkin_timestamp + 1.hour), (offer.checkout_timestamp + 1.hour)).should == 2
      offer.get_number_of_common_hours_to_checkin_and_checkout((offer.checkin_timestamp - 2.hour), (offer.checkout_timestamp + 1.hour)).should == 3
      offer = FactoryGirl.create(:offer, pack_in_hours: 6)
      offer.get_number_of_common_hours_to_checkin_and_checkout((offer.checkin_timestamp + 1.hour), (offer.checkout_timestamp - 2.hour)).should == 3
      offer.get_number_of_common_hours_to_checkin_and_checkout((offer.checkin_timestamp + 2.hour), (offer.checkout_timestamp - 2.hour)).should == 2
      offer.get_number_of_common_hours_to_checkin_and_checkout((offer.checkout_timestamp - 1.hour), (offer.checkout_timestamp + 2.hour)).should == 1
    end
  end

  describe 'get_number_of_exceeding_hours_to_checkin_and_checkout' do
    it 'should return the number of exceding hours based on a checkin and checkout' do
      offer = FactoryGirl.create(:offer)
      offer.get_number_of_exceeding_hours_to_checkin_and_checkout((offer.checkin_timestamp + 1.hour), (offer.checkout_timestamp - 1.hour)).should == 2
      offer.get_number_of_exceeding_hours_to_checkin_and_checkout((offer.checkin_timestamp + 1.hour), (offer.checkout_timestamp + 1.hour)).should == 0
      offer.get_number_of_exceeding_hours_to_checkin_and_checkout((offer.checkin_timestamp - 2.hour), (offer.checkout_timestamp + 1.hour)).should == 0
      offer.get_number_of_exceeding_hours_to_checkin_and_checkout((offer.checkin_timestamp - 2.hour), (offer.checkin_timestamp - 2.hour)).should == 3
      offer = FactoryGirl.create(:offer, pack_in_hours: 6)
      offer.get_number_of_exceeding_hours_to_checkin_and_checkout((offer.checkin_timestamp + 1.hour), (offer.checkout_timestamp + 2.hour)).should == 0
      offer.get_number_of_exceeding_hours_to_checkin_and_checkout((offer.checkin_timestamp + 2.hour), (offer.checkout_timestamp - 2.hour)).should == 4
      offer.get_number_of_exceeding_hours_to_checkin_and_checkout((offer.checkin_timestamp - 1.hour), (offer.checkout_timestamp - 2.hour)).should == 1
    end
  end

  describe "generate_insert_ghosts_query_for_conflicting_offers" do
    specify do
      stub_const("Offer::ACCEPTED_LENGTH_OF_PACKS", [3, 6, 12])
      stub_const("Offer::ROOM_CLEANING_TIME", 2.hours)
      offer = Offer.new
      offer.checkin_timestamp = '21/02/2040 - 08:00:00'.to_datetime
      offer.checkout_timestamp = '21/02/2040 - 20:00:00'.to_datetime
      offer.pack_in_hours = 12

      offer.generate_insert_ghosts_query_for_conflicting_offers.squish.should == %Q{
        INSERT INTO offers (
          "checkin_timestamp",
          "checkout_timestamp",
          "pack_in_hours",
          "price",
          "extra_price_per_person",
          "status",
          "room_type_initial_capacity",
          "room_type_maximum_capacity",
          "no_show_value",
          "room_id",
          "room_type_id",
          "hotel_id",
          "log_id"
        )

          SELECT
            "selected_days_and_packs"."datetime" as checkin_timestamp,
            "selected_days_and_packs"."datetime" + ("selected_days_and_packs"."pack_size" || ' hour')::INTERVAL as checkout_timestamp,
            "selected_days_and_packs"."pack_size" as pack_in_hours,
            '0' as price,
            '0' as extra_price_per_person,
            '99' as status,
            '1' as room_type_initial_capacity,
            '1' as room_type_maximum_capacity,
            '0' as no_show_value,
            '' as room_id,
            '' as room_type_id,
            '' as hotel_id,
            '' as log_id
          FROM
          (
            (
            SELECT
             i as datetime,
             3 as pack_size
            FROM
              generate_series('2040-02-21 04:00:00'::timestamp, '2040-02-21 21:59:59'::timestamp, interval '1 hour') i
            )
            UNION(
            SELECT
             i as datetime,
             6 as pack_size
            FROM
              generate_series('2040-02-21 01:00:00'::timestamp, '2040-02-21 21:59:59'::timestamp, interval '1 hour') i
            )
            UNION(
            SELECT
             i as datetime,
             12 as pack_size
            FROM
              generate_series('2040-02-20 19:00:00'::timestamp, '2040-02-21 21:59:59'::timestamp, interval '1 hour') i
            )
          ) as selected_days_and_packs;
      }.squish
    end

    it "creates the right number of ghosts offers", focus: true do
      stub_const("Offer::ROOM_CLEANING_TIME", 2.hours)
      reserved_offer = FactoryGirl.create( :offer,
        checkin_timestamp: '2020-03-20 05:00:00 +0000'.to_datetime,
        pack_in_hours: 3,
        price: 150.0,
        extra_price_per_person: 100,
        status: Offer::ACCEPTED_STATUS[:reserved]
      )
      
      ActiveRecord::Base.connection.execute(reserved_offer.generate_insert_ghosts_query_for_conflicting_offers)
      # see the bookin try create spec to more explanation
      Offer.where(status: Offer::ACCEPTED_STATUS[:ghost]).count.should == 53
    end
  end
end
