# encoding: utf-8

require 'spec_helper'

describe Department do

  it 'has a valid factory' do
    FactoryGirl.create(:department).should be_persisted
  end

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:company) }
end
