# encoding: utf-8

require 'spec_helper'

describe OffersLog do

  it 'can be instantiated' do
    OffersLog.new.should be_an_instance_of(OffersLog)
  end

  it { should belong_to(:hotel) }

  pending "number_of_offers_to_be_generated - Doesnt consider pricing police with zero price"

end
