# encoding: utf-8

require 'spec_helper'

describe Photo do

  it 'can be instantiated' do
    Photo.new.should be_an_instance_of(Photo)
  end

  it 'has a valid factory' do
    FactoryGirl.build(:photo).should be_valid
  end

  it { should belong_to(:hotel) }
  it { should belong_to(:room_type) }

  context 'belongs to a Hotel' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @main_photo = FactoryGirl.create(:photo, hotel: @hotel, is_main: true)
      @photo1 = FactoryGirl.create(:photo, hotel: @hotel)
      @photo2 = FactoryGirl.create(:photo, hotel: @hotel)
      @photo3 = FactoryGirl.create(:photo, hotel: @hotel)
      @photo4 = FactoryGirl.create(:photo, hotel: @hotel)

      @hotel2 = FactoryGirl.create(:hotel)
      @main_photo2 = FactoryGirl.create(:photo, hotel: @hotel2, is_main: true)
    end

    context 'saving a main photo' do
      it "sets all other hotel photos as not main" do
        @main_photo.is_main.should be_true
        @photo4.is_main.should be_false
        @photo4.is_main = true
        @photo4.save!
        @main_photo.reload
        @photo4.reload
        @main_photo.is_main.should be_false
        @photo4.is_main.should be_true
      end

      it "doesnt modify other hotels photos" do
        @photo4.is_main = true
        @photo4.save!
        @main_photo2.is_main.should == true
      end
    end
  end

  context 'destroying a photo' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @main_photo = FactoryGirl.create(:photo, hotel: @hotel, is_main: true)
      @photo1 = FactoryGirl.create(:photo, hotel: @hotel)
    end

    it "allows if it is not a main photo" do
      @photo1.destroy.should be_true
      Photo.count.should == 1
    end

    it "cannot delete if it is a main photo" do
      @main_photo.destroy.should be_false
      Photo.count.should == 2
    end
  end
end
