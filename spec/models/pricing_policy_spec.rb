# encoding: utf-8

require 'spec_helper'

describe PricingPolicy do

  it 'can be instantiated' do
    PricingPolicy.new.should be_an_instance_of(PricingPolicy)
  end

  it 'has a valid factory' do
    FactoryGirl.build(:pricing_policy).should be_valid
  end

  it { should belong_to(:hotel) }

  it { should have_many(:room_type_pricing_policies) }

  it { should validate_presence_of(:name) }

  describe 'completed?' do
    context 'all hotel room types included in pricing policy' do
      it 'return false when the pricing policy is incomplete' do
        hotel = FactoryGirl.create(:hotel)
        room_type = FactoryGirl.create(:room_type, hotel: hotel)
        pricing_policy = FactoryGirl.create(:pricing_policy, hotel: hotel)
        FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy, pack_price_6h: 0)
        pricing_policy.completed?.should == false
      end

      it 'return true when the pricing policy is complete' do
        hotel = FactoryGirl.create(:hotel)
        room_type = FactoryGirl.create(:room_type, hotel: hotel)
        pricing_policy = FactoryGirl.create(:pricing_policy, hotel: hotel)
        FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
        pricing_policy.completed?.should == true
      end
    end

    it 'return false when not all hotel room types included in pricing policy' do
      hotel = FactoryGirl.create(:hotel)
      room_type = FactoryGirl.create(:room_type, hotel: hotel)
      FactoryGirl.create(:room_type, hotel: hotel)
      pricing_policy = FactoryGirl.create(:pricing_policy, hotel: hotel)
      FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
      pricing_policy.completed?.should == false
    end

  end

end
