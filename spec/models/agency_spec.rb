require 'spec_helper'

describe Agency do
  it 'has a valid factory' do
    FactoryGirl.create(:agency).should be_persisted
  end

  it { should validate_presence_of :name }

  it { should validate_presence_of :email}

  describe 'before saving' do
    it 'sets a password if it is a new record and a password is not present' do
      agency = Agency.new
      agency.name = 'Agency Name'
      agency.email = 'email@email.com'
      expect{ agency.save }.to change{ agency.encrypted_password }
      agency.password.should_not be_nil
    end
  end

  describe 'activate!' do
    before(:each) do
      @agency = FactoryGirl.create(:agency, is_active: false)
    end

    it 'activates the agency' do
      @agency.is_active?.should == false
      @agency.activate!
      @agency.reload.is_active?.should == true
    end

    it 'raises an exception if it is not successfull' do
      Agency.any_instance.should_receive(:update!).and_raise
      expect{ @agency.activate! }.to raise_error
    end
  end

  describe 'deactivate!' do
    before(:each) do
      @agency = FactoryGirl.create(:agency)
    end

    it 'deactivates the agency' do
      @agency.deactivate!
      @agency.is_active?.should == false
    end

    it 'raises an exception if it is not successfull' do
      Agency.any_instance.should_receive(:update!).and_raise
      expect{ @agency.deactivate! }.to raise_error
    end
  end
end
