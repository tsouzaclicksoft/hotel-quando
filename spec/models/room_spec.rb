# encoding: utf-8

require 'spec_helper'

describe Room do

  it { should validate_presence_of(:number) }

  it { should belong_to(:hotel) }

  it { should belong_to(:room_type) }

  it 'can be instantiated' do
    Room.new.should be_an_instance_of(Room)
  end

  it 'has a valid factory' do
    FactoryGirl.create(:room).should be_persisted
  end

  it 'should require room type' do
    room = FactoryGirl.build(:room, room_type: nil)
    room.should be_invalid
  end

  describe 'self.save_rooms_from_numbers_array_and_return_invalids' do
    it 'save the rooms from array' do
      hotel = FactoryGirl.create(:hotel)
      room_type = FactoryGirl.create(:room_type)
      numbers_array = ['1', '2', '3', '4', '5']
      Room.save_rooms_from_numbers_array_and_return_invalids(numbers_array, hotel.id, room_type.id)
      Room.count.should be_equal(5)
    end

    it 'return invalid numbers' do
      hotel = FactoryGirl.create(:hotel)
      room_type = FactoryGirl.create(:room_type)
      numbers_array = ['1', '1', '2', '3', '4', '5', '2', '3', '6 7']
      invalids = Room.save_rooms_from_numbers_array_and_return_invalids(numbers_array, hotel.id, room_type.id)
      invalids.length.should be_equal(3)
      invalids.should include('1')
      invalids.should include('2')
      invalids.should include('3')
    end
  end

  describe 'self.create_rooms_from_quantity' do
    it 'should save the desired room quantity' do
      room = FactoryGirl.create(:room)
      Room.create_rooms_from_quantity(20, room.room_type_id, room.hotel_id)
      Room.count.should == 21
    end
  end
end
