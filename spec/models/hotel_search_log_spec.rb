# encoding: utf-8

require 'spec_helper'

describe HotelSearchLog do

  it 'can be instantiated' do
    HotelSearchLog.new.should be_an_instance_of(HotelSearchLog)
  end

  it 'has a valid factory' do
    FactoryGirl.build(:hotel_search_log).should be_valid
  end

  it { should belong_to(:search_logs_user) }

  describe '#email' do
    context 'no search_log_user' do
      specify do
        FactoryGirl.build(:hotel_search_log).email.should == ""
      end
    end

    context 'With search_log_user' do
      specify do
        user = FactoryGirl.create(:search_logs_user)
        FactoryGirl.build(:hotel_search_log, search_logs_user: user).email.should == user.email
      end
    end
  end


  describe '#exact_match_hotels_ids' do
    specify do
      log = FactoryGirl.build(:hotel_search_log, exact_match_hotels_ids: "33-43-12-3-6")
      log.exact_match_hotels_ids.should == [33,43,12,3,6]
    end


    specify do
      log = FactoryGirl.build(:hotel_search_log)
      log.exact_match_hotels_ids.should == []
    end
  end

  describe '#similar_match_hotels_ids' do
    specify do
      log = FactoryGirl.build(:hotel_search_log, similar_match_hotels_ids: "33-43-12-3-6")
      log.similar_match_hotels_ids.should == [33,43,12,3,6]
    end

    specify do
      log = FactoryGirl.build(:hotel_search_log)
      log.similar_match_hotels_ids.should == []
    end
  end

end
