# encoding: utf-8

require 'spec_helper'

describe Hotel do

  it { should have_many :photos }

  it { should have_many :rooms }

  it { should have_many :room_types }

  it { should validate_presence_of(:name) }

  it { should validate_presence_of(:email) }

  it { should validate_presence_of(:description_en) }

  it { should validate_presence_of(:description_pt_br) }

  it { should validate_presence_of(:latitude) }

  it { should validate_presence_of(:longitude) }

  it { should validate_presence_of(:minimum_hours_of_notice) }

  it { should validate_presence_of(:payment_method_pt_br) }

  it { should validate_presence_of(:payment_method_en) }

  it { should validate_presence_of(:state) }

  it { should validate_presence_of(:city) }

  it { should validate_presence_of(:street) }

  it { should validate_presence_of(:number) }

  it 'can be instantiated' do
    Hotel.new.should be_an_instance_of(Hotel)
  end

  it 'has a valid factory' do
    FactoryGirl.create(:hotel).should be_persisted
  end

  it 'should be invalid if minimum hours of notice is less than -1' do
    hotel = FactoryGirl.build(:hotel)
    hotel.minimum_hours_of_notice = -2
    hotel.should be_invalid
  end

  it 'should be invalid if comission is less than 0' do
    hotel = FactoryGirl.build(:hotel)
    hotel.comission_in_percentage = -1
    hotel.should be_invalid
  end

  it 'should be invalid if comission is greater than 100' do
    hotel = FactoryGirl.build(:hotel)
    hotel.comission_in_percentage = 100.2
    hotel.should be_invalid
  end

  it 'should be invalid if ISS is less than 0' do
    hotel = FactoryGirl.build(:hotel)
    hotel.iss_in_percentage = -1
    hotel.should be_invalid
  end

  it 'should be invalid if ISS is greater than 10' do
    hotel = FactoryGirl.build(:hotel)
    hotel.iss_in_percentage = 10.2
    hotel.should be_invalid
  end

  it "should be invalid if category isn't an accepted category" do
    hotel = FactoryGirl.build(:hotel)
    hotel.category = '-1 estrela'
    hotel.should be_invalid
  end

  it "sets password if none is present while saving" do
    hotel = FactoryGirl.build(:hotel, password: nil)
    hotel.save!
  end

  describe "set_new_password" do
    before(:each) {
      hotel = Hotel.new
      Devise.stub_chain(:friendly_token, :first).and_return("pass123")
      Devise.stub_chain(:token_generator, :generate).and_return("raw", "enc")
    }

    it "sets the password" do
      hotel = Hotel.new
      hotel.set_new_password
      hotel.password.should == "pass123"
    end

  end

  describe 'minimum_available_offer_price_for_each_pack' do
    before(:each) do
      offer = FactoryGirl.create(:offer, price: 10.0, pack_in_hours: 6)
      @hotel = offer.hotel
      FactoryGirl.create(:offer, hotel: @hotel, pack_in_hours: 6, price: 11.0)
      FactoryGirl.create(:offer, hotel: @hotel, pack_in_hours: 6, price: 4.0)
      FactoryGirl.create(:offer, hotel: @hotel, pack_in_hours: 3, price: 5.0)
      FactoryGirl.create(:offer, hotel: @hotel, pack_in_hours: 12, price: 50.0)
      FactoryGirl.create(:offer, hotel: @hotel, pack_in_hours: 12, price: 520.0)
    end

    specify do
      minimum_prices = @hotel.minimum_available_offer_price_for_each_pack
      minimum_prices["6"].should == 4.0
      minimum_prices["3"].should == 5.0
      minimum_prices["12"].should == 50.0
    end
  end

  describe 'send_login_and_password' do
    it "calls HotelMailer send_login_and_password" do
      hotel = Hotel.new
      mailer_mock = double()
      mailer_mock.should_receive(:deliver)
      HotelMailer.should_receive(:send_login_and_password).and_return(mailer_mock)
      hotel.send_login_and_password
    end
  end

  describe 'emails_for_notice' do
    it 'should return an array containing the hotel email and the hotel notice emails' do
      hotel = FactoryGirl.create(:hotel)
      hotel.update(notice_emails: 'a@hotel.com, b@hotel.com,, c@hotel.com          ,d@hotel.com', email: 'hotel@hotel.com')
      hotel_emails_for_notice_array = hotel.emails_for_notice
      hotel_emails_for_notice_array.include?('a@hotel.com').should == true
      hotel_emails_for_notice_array.include?('b@hotel.com').should == true
      hotel_emails_for_notice_array.include?('c@hotel.com').should == true
      hotel_emails_for_notice_array.include?('d@hotel.com').should == true
      hotel_emails_for_notice_array.include?('hotel@hotel.com').should == true
    end
  end

  describe '#main_photo_url' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @main_photo = FactoryGirl.create(:photo, hotel: @hotel, is_main: true)
      @photo1 = FactoryGirl.create(:photo, hotel: @hotel)
      @photo2 = FactoryGirl.create(:photo, hotel: @hotel)
    end

    it 'returns the url of the first main photo' do
      @hotel.photos = [@photo1, @main_photo, @photo2]
      @hotel.main_photo_url.should == @main_photo.file.url(:big)
    end

    it 'return the url of the first photo if there is no main photo' do
      @hotel.photos = [@photo1, @photo2]
      @hotel.main_photo_url.should == @photo1.file.url(:big)
    end
  end

  describe 'accepts_booking_cancellation?' do
    it 'returns false if the hotel minimum_hours_of_notice is equal to -1' do
      hotel = FactoryGirl.create(:hotel, minimum_hours_of_notice: -1)
      hotel.accepts_booking_cancellation?.should == false
    end

    it 'returns true if the hotel minimum_hours_of_notice is not equal to -1' do
      hotel = FactoryGirl.create(:hotel, minimum_hours_of_notice: 120)
      hotel.accepts_booking_cancellation?.should == true
    end
  end

  specify '#is_omnibees?' do
    expect(Hotel.new(omnibees_code: '123').is_omnibees?).to be_true
    expect(Hotel.new(omnibees_code: '43123').is_omnibees?).to be_true
    expect(Hotel.new(omnibees_code: '').is_omnibees?).to be_false
    expect(Hotel.new(omnibees_code: nil).is_omnibees?).to be_false
    expect(Hotel.new().is_omnibees?).to be_false
  end
end
