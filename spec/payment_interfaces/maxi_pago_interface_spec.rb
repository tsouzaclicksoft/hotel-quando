# encoding: utf-8

require 'spec_helper'

describe MaxiPagoInterface do
  subject { MaxiPagoInterface }

  shared_examples 'invalid maxipago response' do |method_name|

    let(:malformed_response) {
      {
        body: %Q{
          <?xml version="1.0" encoding="UTF-8"?>
          <body>
          </body>
        }
      }
    }

    describe 'if maxipago returns a malformed_response', focus: true do
      it "throws a MalformedMaxipagoResponseError" do
        @client.should_receive(:execute).and_return(malformed_response)
        expect {
          subject.send(method_name, standard_options)
        }.to raise_error(MaxiPagoInterface::MalformedMaxipagoResponseError, malformed_response[:body])
      end
    end
  end

  context 'stubing client' do
    before(:each) do
      @client = double()
      @client.stub(:use)
      @client.stub(:execute).and_return({
        body: %Q{
          <api-response>
            <errorCode>0</errorCode>
            <errorMessage />
            <command>add-consumer</command>
            <time>1367849110845</time>
            <result>
              <customerId>12590</customerId>
            </result>
          </api-response>
        }
      })
      subject.stub(:get_maxipago_client).and_return(@client)
    end

    describe 'self.direct_sale' do
      let(:standard_options) do
        credit_card = FactoryGirl.create(:credit_card)
        booking = FactoryGirl.create(:booking, user: credit_card.user)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card, booking: booking)
        return {
          payment: payment
        }
      end

      let(:successfull_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode>123456</authCode>
              <orderID>0AF90437:013E8F22A46F:4444:01EC3389</orderID>
              <referenceNum>21312</referenceNum>
              <transactionID>505104</transactionID>
              <transactionTimestamp>1368201012</transactionTimestamp>
              <responseCode>0</responseCode>
              <responseMessage>CAPTURED</responseMessage>
              <avsResponseCode>YYY</avsResponseCode>
              <cvvResponseCode>M</cvvResponseCode>
              <processorCode>A</processorCode>
              <processorMessage>APPROVED</processorMessage>
              <errorMessage />
              <fraudScore>49</fraudScore>
            </transaction-response>
          }
        }
      }

      let(:error_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode />
              <orderID>7F000001:013D16CF1461:F0EF:014EDA77</orderID>
              <referenceNum>2012071201</referenceNum>
              <transactionID>3308</transactionID>
              <transactionTimestamp>1361887302962</transactionTimestamp>
              <responseCode>1</responseCode>
              <responseMessage>DECLINED</responseMessage>
              <avsResponseCode>NNN</avsResponseCode>
              <cvvResponseCode>N</cvvResponseCode>
              <processorCode>D</processorCode>
              <processorMessage>DECLINED</processorMessage>
              <errorMessage />
            </transaction-response>
          }
        }
      }

      include_examples 'invalid maxipago response', :direct_sale

      it "checks the arguments: payment" do
        subject.should_receive(:check_hash_required_keys).with(standard_options, [:payment])
        @client.stub(:execute).and_return(successfull_response)
        subject.direct_sale(standard_options)
      end

      it "should call the use client method" do
        @client.should_receive(:use)
        @client.stub(:execute).and_return(successfull_response)
        subject.direct_sale(standard_options)
      end

      it "calls maxipago api with the correct params" do

        @client.should_receive(:execute).with({
          command: "sale",
          processor_id: MP_PROCESSOR_ID,
          customer_id: standard_options[:payment].credit_card.user.maxipago_token,
          reference_num: standard_options[:payment].id,
          token: standard_options[:payment].credit_card.maxipago_token,
          charge_total: standard_options[:payment].booking.total_price
        }).and_return(successfull_response)
        subject.direct_sale(standard_options)
      end

      describe "returns an object" do
        describe ".body" do
          specify "that returns the xml response" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.direct_sale(standard_options).body.squish.should == successfull_response[:body].squish
          end
        end

        describe ".order_id" do
          specify do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.direct_sale(standard_options).order_id.should == "0AF90437:013E8F22A46F:4444:01EC3389"
          end
        end

        describe ".is_authorized" do
          specify "that returns true if transaction was authorized" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.direct_sale(standard_options).success.should be_true
          end
          specify "that returns false if transaction was not authorized" do
            @client.should_receive(:execute).and_return(error_response)
            subject.direct_sale(standard_options).success.should be_false
          end
        end
      end
    end

    describe 'self.void_transaction' do
      let(:standard_options) do
        credit_card = FactoryGirl.create(:credit_card)
        booking = FactoryGirl.create(:booking, user: credit_card.user)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card, booking: booking)
        payment.update(authorized_at: Time.now)
        return {
          payment: payment
        }
      end

      let(:successfull_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode/>
              <orderID/>
              <referenceNum/>
              <transactionID>680029</transactionID>
              <transactionTimestamp/>
              <responseCode>0</responseCode>
              <responseMessage>VOIDED</responseMessage>
              <avsResponseCode/>
              <cvvResponseCode/>
              <processorCode>A</processorCode>
              <processorMessage>APPROVED</processorMessage>
              <errorMessage/>
            </transaction-response>
          }
        }
      }

      let(:error_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode/>
              <orderID/>
              <referenceNum/>
              <transactionID/>
              <transactionTimestamp>1396901221867</transactionTimestamp>
              <responseCode>1024</responseCode>
              <responseMessage>INVALID REQUEST</responseMessage>
              <avsResponseCode/>
              <cvvResponseCode/>
              <processorCode/>
              <processorMessage/>
              <errorMessage>Transaction to be voided is not in a valid state to void, it must be in a captured, verified, authorized or accepted state.</errorMessage>
            </transaction-response>
          }
        }
      }

      it "checks the arguments: payment" do
        subject.should_receive(:check_hash_required_keys).with(standard_options, [:payment])
        @client.stub(:execute).and_return(successfull_response)
        subject.void_transaction(standard_options)
      end

      it "should call the use client method" do
        @client.should_receive(:use)
        @client.stub(:execute).and_return(successfull_response)
        subject.void_transaction(standard_options)
      end

      it "calls maxipago api with the correct params" do

        @client.should_receive(:execute).with({
          command: "void",
          transaction_id: standard_options[:payment].maxipago_transaction_id
        }).and_return(successfull_response)
        subject.void_transaction(standard_options)
      end

      describe "returns an object" do
        describe ".body" do
          specify "that returns the xml response" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.void_transaction(standard_options).body.squish.should == successfull_response[:body].squish
          end
        end

        describe ".transaction_canceled" do
          specify "that returns true if transaction was canceled" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.void_transaction(standard_options).transaction_canceled.should be_true
          end
          specify "that returns false if transaction was not authorized" do
            @client.should_receive(:execute).and_return(error_response)
            subject.void_transaction(standard_options).transaction_canceled.should be_false
          end
        end
      end
    end

    describe 'self.reverse_transaction' do
      let(:standard_options) do
        credit_card = FactoryGirl.create(:credit_card)
        booking = FactoryGirl.create(:booking, user: credit_card.user)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card, booking: booking)
        payment.update(authorized_at: (Time.now - 1.day))
        return {
          payment: payment
        }
      end

      let(:successfull_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode/>
              <orderID>D10CA722:01453DD4C173:8444:007EB35D</orderID>
              <referenceNum>19</referenceNum>
              <transactionID>680036</transactionID>
              <transactionTimestamp>1396901698</transactionTimestamp>
              <responseCode>0</responseCode>
              <responseMessage>CAPTURED</responseMessage>
              <avsResponseCode/>
              <cvvResponseCode/>
              <processorCode>A</processorCode>
              <processorMessage>APPROVED</processorMessage>
              <errorMessage/>
            </transaction-response>

          }
        }
      }

      let(:error_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode/>
              <orderID/>
              <referenceNum/>
              <transactionID/>
              <transactionTimestamp>1396901654155</transactionTimestamp>
              <responseCode>1024</responseCode>
              <responseMessage>INVALID REQUEST</responseMessage>
              <avsResponseCode/>
              <cvvResponseCode/>
              <processorCode/>
              <processorMessage/>
              <errorMessage>No transactions were found to return against.</errorMessage>
            </transaction-response>
          }
        }
      }

      it "checks the arguments: payment" do
        subject.should_receive(:check_hash_required_keys).with(standard_options, [:payment])
        @client.stub(:execute).and_return(successfull_response)
        subject.reverse_transaction(standard_options)
      end

      it "should call the use client method" do
        @client.should_receive(:use)
        @client.stub(:execute).and_return(successfull_response)
        subject.reverse_transaction(standard_options)
      end

      it "calls maxipago api with the correct params" do

        @client.should_receive(:execute).with({
          command: "reversal",
          order_id: standard_options[:payment].maxipago_order_id,
          reference_num: standard_options[:payment].id,
          charge_total: standard_options[:payment].booking.total_price
        }).and_return(successfull_response)
        subject.reverse_transaction(standard_options)
      end

      describe "returns an object" do
        describe ".body" do
          specify "that returns the xml response" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.reverse_transaction(standard_options).body.squish.should == successfull_response[:body].squish
          end
        end

        describe ".transaction_canceled" do
          specify "that returns true if transaction was authorized" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.reverse_transaction(standard_options).transaction_canceled.should be_true
          end
          specify "that returns false if transaction was not authorized" do
            @client.should_receive(:execute).and_return(error_response)
            subject.reverse_transaction(standard_options).transaction_canceled.should be_false
          end
        end
      end
    end

    describe 'self.refund', f:true do
      let(:standard_options) do
        credit_card = FactoryGirl.create(:credit_card)
        offer = FactoryGirl.create(:offer, pack_in_hours: 6, price: 200, no_show_value: 150)
        booking = FactoryGirl.create(:booking, user: credit_card.user, offer: offer)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card, booking: booking)
        payment.update(authorized_at: (Time.now - 1.day))
        return {
          payment: payment,
          value_to_refund: 2.0 # we must set manually because for offers under 24hrs there is no refund
        }
      end

      let(:error_options_with_zero_refund_value) do
        credit_card = FactoryGirl.create(:credit_card)
        offer = FactoryGirl.create(:offer, pack_in_hours: 12)
        booking = FactoryGirl.create(:booking, user: credit_card.user)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card, booking: booking)
        payment.update(authorized_at: (Time.now - 1.day))
        return {
          payment: payment,
          value_to_refund: booking.no_show_refund_value
        }
      end

      let(:error_options_with_negative_refund_value) do
        credit_card = FactoryGirl.create(:credit_card)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card)
        payment.update(authorized_at: (Time.now - 1.day))
        return {
          payment: payment,
          value_to_refund: -1
        }
      end

      let(:error_options_without_refund_value) do
        credit_card = FactoryGirl.create(:credit_card)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card)
        payment.update(authorized_at: (Time.now - 1.day))
        return {
          payment: payment
        }
      end

      let(:successfull_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode/>
              <orderID>D10CA722:01458FFB669F:63C5:00135839</orderID>
              <referenceNum>76</referenceNum>
              <transactionID>687848</transactionID>
              <transactionTimestamp>1398280329</transactionTimestamp>
              <responseCode>0</responseCode>
              <responseMessage>CAPTURED</responseMessage>
              <avsResponseCode/>
              <cvvResponseCode/>
              <processorCode>A</processorCode>
              <processorMessage>APPROVED</processorMessage>
              <errorMessage/>
            </transaction-response>
          }
        }
      }

      let(:error_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode/>
              <orderID/>
              <referenceNum/>
              <transactionID/>
              <transactionTimestamp>1396901654155</transactionTimestamp>
              <responseCode>1024</responseCode>
              <responseMessage>INVALID REQUEST</responseMessage>
              <avsResponseCode/>
              <cvvResponseCode/>
              <processorCode/>
              <processorMessage/>
              <errorMessage>No transactions were found to return against.</errorMessage>
            </transaction-response>
          }
        }
      }

      it "checks the arguments: payment and value_to_refund" do
        subject.should_receive(:check_hash_required_keys).with(standard_options, [:payment, :value_to_refund])
        @client.stub(:execute).and_return(successfull_response)
        subject.refund(standard_options)
      end

      it 'raises if the refund value is not present' do
        expect { subject.refund(error_options_without_refund_value) }.to raise_error(MaxiPagoInterface::InvalidArgumentsError)
      end

      it 'raises if the refund value is zero' do
        expect { subject.refund(error_options_with_zero_refund_value) }.to raise_error(MaxiPagoInterface::CannotRefundWithoutAValueError, 'Can only make a refund if there is a value to refund')
      end

      it 'raises if the refund value is negative' do
        expect { subject.refund(error_options_with_negative_refund_value) }.to raise_error(MaxiPagoInterface::CannotRefundWithoutAValueError, 'Can only make a refund if there is a value to refund')
      end

      it "should call the use client method" do
        @client.should_receive(:use)
        @client.stub(:execute).and_return(successfull_response)
        subject.refund(standard_options)
      end

      it "calls maxipago api with the correct params" do

        @client.should_receive(:execute).with({
          command: "reversal",
          order_id: standard_options[:payment].maxipago_order_id,
          reference_num: standard_options[:payment].id,
          charge_total: standard_options[:value_to_refund]
        }).and_return(successfull_response)
        subject.refund(standard_options)
      end

      describe "returns an object" do
        describe ".body" do
          specify "that returns the xml response" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.refund(standard_options).body.squish.should == successfull_response[:body].squish
          end
        end

        describe ".success" do
          specify "that returns true if transaction was authorized" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.refund(standard_options).success.should be_true
          end
          specify "that returns false if transaction was not authorized" do
            @client.should_receive(:execute).and_return(error_response)
            subject.refund(standard_options).success.should be_false
          end
        end
      end
    end

    describe 'self.authorize_transaction' do
      let(:standard_options) do
        credit_card = FactoryGirl.create(:credit_card)
        booking = FactoryGirl.create(:booking, user: credit_card.user)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card, booking: booking)
        return {
          payment: payment
        }
      end

      let(:successfull_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode>123456</authCode>
              <orderID>0AF90437:013E8F22A46F:94D9:01EC3389</orderID>
              <referenceNum>21312</referenceNum>
              <transactionID>505104</transactionID>
              <transactionTimestamp>1368201012</transactionTimestamp>
              <responseCode>0</responseCode>
              <responseMessage>AUTHORIZED</responseMessage>
              <avsResponseCode>YYY</avsResponseCode>
              <cvvResponseCode>M</cvvResponseCode>
              <processorCode>A</processorCode>
              <processorMessage>APPROVED</processorMessage>
              <errorMessage />
              <fraudScore>49</fraudScore>
            </transaction-response>
          }
        }
      }

      let(:error_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode />
              <orderID>7F000001:013D16CF1461:F0EF:014EDA77</orderID>
              <referenceNum>2012071201</referenceNum>
              <transactionID>3308</transactionID>
              <transactionTimestamp>1361887302962</transactionTimestamp>
              <responseCode>1</responseCode>
              <responseMessage>DECLINED</responseMessage>
              <avsResponseCode>NNN</avsResponseCode>
              <cvvResponseCode>N</cvvResponseCode>
              <processorCode>D</processorCode>
              <processorMessage>DECLINED</processorMessage>
              <errorMessage />
            </transaction-response>
          }
        }
      }

      include_examples 'invalid maxipago response', :authorize_transaction

      it "checks the arguments: payment" do
        subject.should_receive(:check_hash_required_keys).with(standard_options, [:payment])
        @client.stub(:execute).and_return(successfull_response)
        subject.authorize_transaction(standard_options)
      end

      it "should call the use client method" do
        @client.should_receive(:use)
        @client.stub(:execute).and_return(successfull_response)
        subject.authorize_transaction(standard_options)
      end

      it "calls maxipago api with the correct params" do

        @client.should_receive(:execute).with({
          command: "authorization",
          processor_id: MP_PROCESSOR_ID,
          customer_id: standard_options[:payment].credit_card.user.maxipago_token,
          reference_num: standard_options[:payment].id,
          token: standard_options[:payment].credit_card.maxipago_token,
          charge_total: standard_options[:payment].booking.total_price
        }).and_return(successfull_response)
        subject.authorize_transaction(standard_options)
      end

      describe "returns an object" do
        describe ".body" do
          specify "that returns the xml response" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.authorize_transaction(standard_options).body.squish.should == successfull_response[:body].squish
          end
        end

        describe ".order_id" do
          specify do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.authorize_transaction(standard_options).order_id.should == "0AF90437:013E8F22A46F:94D9:01EC3389"
          end
        end

        describe ".is_authorized" do
          specify "that returns true if transaction was authorized" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.authorize_transaction(standard_options).is_authorized.should be_true
          end
          specify "that returns true if transaction was not authorized" do
            @client.should_receive(:execute).and_return(error_response)
            subject.authorize_transaction(standard_options).is_authorized.should be_false
          end
        end
      end
    end

    describe 'self.capture_transaction' do
      let(:standard_options) do
        credit_card = FactoryGirl.create(:credit_card)
        booking = FactoryGirl.create(:booking, user: credit_card.user)
        payment = FactoryGirl.create(:payment, user: credit_card.user, credit_card: credit_card, booking: booking)
        return {
          payment: payment
        }
      end

      let(:successfull_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode />
              <orderID>0AF90437:013E8F22A46F:94D9:01EC3389</orderID>
              <referenceNum>21312</referenceNum>
              <transactionID>505107</transactionID>
              <transactionTimestamp>1368201109</transactionTimestamp>
              <responseCode>0</responseCode>
              <responseMessage>CAPTURED</responseMessage>
              <avsResponseCode />
              <cvvResponseCode />
              <processorCode>A</processorCode>
              <processorMessage>APPROVED</processorMessage>
              <errorMessage />
            </transaction-response>
          }
        }
      }

      let(:error_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <transaction-response>
              <authCode />
              <orderID>7F000001:013D16CF1461:F0EF:014EDA77</orderID>
              <referenceNum>2012071201</referenceNum>
              <transactionID>3308</transactionID>
              <transactionTimestamp>1361887302962</transactionTimestamp>
              <responseCode>1</responseCode>
              <responseMessage>DECLINED</responseMessage>
              <avsResponseCode>NNN</avsResponseCode>
              <cvvResponseCode>N</cvvResponseCode>
              <processorCode>D</processorCode>
              <processorMessage>DECLINED</processorMessage>
              <errorMessage />
            </transaction-response>
          }
        }
      }

      it "checks the arguments:  payment" do
        subject.should_receive(:check_hash_required_keys).with(standard_options, [:payment])
        @client.stub(:execute).and_return(successfull_response)
        subject.capture_transaction(standard_options)
      end

      it "should call the use client method" do
        @client.should_receive(:use)
        @client.stub(:execute).and_return(successfull_response)
        subject.capture_transaction(standard_options)
      end

      it "calls maxipago api with the correct params" do
        @client.stub(:execute).and_return(successfull_response)
        @client.should_receive(:execute).with({
          command: "capture",
          order_id: standard_options[:payment].maxipago_order_id,
          reference_num: standard_options[:payment].id,
          charge_total: standard_options[:payment].booking.total_price
        })
        subject.capture_transaction(standard_options)
      end

      describe "returns an object" do
        describe ".body" do
          specify "that returns the xml response" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.capture_transaction(standard_options).body.squish.should == successfull_response[:body].squish
          end
        end
        describe ".is_captured" do
          specify "that returns true if transaction was captured" do
            @client.should_receive(:execute).and_return(successfull_response)
            subject.capture_transaction(standard_options).is_captured.should be_true
          end
          specify "that returns false if transaction was not captured" do
            @client.should_receive(:execute).and_return(error_response)
            subject.capture_transaction(standard_options).is_captured.should be_false
          end
        end
      end
    end


    describe 'self.generate_credit_card_token_for' do

      let(:standard_options) do
        credit_card = FactoryGirl.create(:credit_card)

        return {
          credit_card: credit_card
        }
      end

      let(:successfull_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <api-response>
              <errorCode>0</errorCode>
              <errorMessage />
              <command>add-card-onfile</command>
              <time>1367855065607</time>
              <result>
                <token>5e3K+bwTdBg=</token>
              </result>
            </api-response>
          }
        }
      }

      let(:error_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <api-response>
              <errorCode>1</errorCode>
              <errorMessage><![CDATA[credit card already exists in database with this number.]]></errorMessage>
              <time>1367848602198</time>
            </api-response>
          }
        }
      }

      it "checks the arguments: credit_card" do
        subject.should_receive(:check_hash_required_keys).with(standard_options, [:credit_card])
        subject.generate_credit_card_token_for(standard_options)
      end

      it "should call the use client method" do
        @client.should_receive(:use)
        subject.generate_credit_card_token_for(standard_options)
      end

      it "calls maxipago api with correct params", fi:true do
        @client.should_receive(:execute).with({
          command: "add_card_onfile",
          customer_id: standard_options[:credit_card].user.maxipago_token,
          credit_card_number: standard_options[:credit_card].temporary_number,
          expiration_month: "07",
          expiration_year: standard_options[:credit_card].expiration_year,
          billing_name: standard_options[:credit_card].billing_name,
          billing_address1: standard_options[:credit_card].billing_address1,
          billing_city: standard_options[:credit_card].billing_city,
          billing_state: standard_options[:credit_card].billing_state,
          billing_zip: standard_options[:credit_card].billing_zipcode,
          billing_country: standard_options[:credit_card].billing_country,
          billing_phone: standard_options[:credit_card].billing_phone,
          billing_email: standard_options[:credit_card].user.email,
          onfile_permissions: "ongoing"
        }).and_return(successfull_response)
        subject.generate_credit_card_token_for(standard_options)
      end

      it "returns the credit card token if successfull" do
        @client.should_receive(:execute).and_return(successfull_response)
        subject.generate_credit_card_token_for(standard_options).should == "5e3K+bwTdBg="
      end

      it "raises exception if cannot create the credit card token" do
        @client.should_receive(:execute).and_return(error_response)

        expect {
          subject.generate_credit_card_token_for(standard_options)
        }.to raise_error(MaxiPagoInterface::CannotGenerateCustomerTokenError, "credit card already exists in database with this number.")
      end

    end


    describe 'self.generate_client_token_for' do
      let(:standard_options) do
        user = FactoryGirl.create(:user)

        return {
          user: user
        }
      end

      let(:successfull_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <api-response>
              <errorCode>0</errorCode>
              <errorMessage />
              <command>add-consumer</command>
              <time>1367849110845</time>
              <result>
                <customerId>12590</customerId>
              </result>
            </api-response>
          }
        }
      }

      let(:error_response) {
        {
          body: %Q{
            <?xml version="1.0" encoding="UTF-8"?>
            <api-response>
              <errorCode>1</errorCode>
              <errorMessage><![CDATA[consumer already exists in database with this consumerId.]]></errorMessage>
              <time>1367848602198</time>
            </api-response>
          }
        }
      }

      it "does not check the arguments if the user is present in the options" do
        subject.should_not_receive(:check_hash_required_keys).with(standard_options, [:user])
        subject.generate_client_token_for(standard_options)
      end

      it "checks the arguments (client_id, first_name and last_name) if the user is not present in the options" do
        subject.should_receive(:check_hash_required_keys).with({}, [:client_id, :client_first_name, :client_last_name])
        subject.generate_client_token_for({})
      end

      it "should call the use client method" do
        @client.should_receive(:use)
        subject.generate_client_token_for(standard_options)
      end


      it "calls maxipago api with correct params if the user is present in the options" do
        @client.should_receive(:execute).with({
          command: "add_consumer",
          customer_id_ext: standard_options[:user].id.to_s,
          firstname: standard_options[:user].first_name,
          lastname: standard_options[:user].last_name
        }).and_return(successfull_response)
        subject.generate_client_token_for(standard_options)
      end

      it "calls maxipago api with correct params if the user is not present in the options" do
        options = {
          client_id: 123,
          client_first_name: 'Teste',
          client_last_name: 'Teste2'
        }
        @client.should_receive(:execute).with({
          command: "add_consumer",
          customer_id_ext: options[:client_id].to_s,
          firstname: options[:client_first_name],
          lastname: options[:client_last_name]
        }).and_return(successfull_response)
        subject.generate_client_token_for(options)
      end

      it "returns the client token if successfull" do
        @client.should_receive(:execute).and_return(successfull_response)
        subject.generate_client_token_for(standard_options).should == "12590"
      end

      it "raises exception if cannot create the client token" do
        @client.should_receive(:execute).and_return(error_response)
        expect {
          subject.generate_client_token_for(standard_options)
        }.to raise_error(MaxiPagoInterface::CannotGenerateCustomerTokenError, "consumer already exists in database with this consumerId.")
      end
    end

    describe 'self.check_hash_required_keys' do
      specify do
        expect {
          subject.send(:check_hash_required_keys, {key1: 1, key2: 2, key4: 4}, [:key1, :key2, :key3, :key4])
        }.to raise_error(MaxiPagoInterface::InvalidArgumentsError)
      end

      specify do
        expect {
          subject.send(:check_hash_required_keys, {key1: 1, key2: 2, key3: 3, key4: 4}, [:key1, :key2, :key3, :key4])
        }.not_to raise_error
      end
    end
  end

  describe 'self.get_maxipago_client' do
    specify do
      subject.send(:get_maxipago_client).should be_an_instance_of(Maxipago::Client)
    end
  end

  describe 'self.get_api_request' do
    specify do
      subject.send(:get_api_request).should be_an_instance_of(Maxipago::RequestBuilder::ApiRequest)
    end
  end

  describe 'self.get_transaction_request' do
    specify do
      subject.send(:get_transaction_request).should be_an_instance_of(Maxipago::RequestBuilder::TransactionRequest)
    end
  end

  describe 'self.cancel_transaction' do
    before(:each) do
      @booking = FactoryGirl.create(:booking)
      @payment = FactoryGirl.create(:payment, booking: @booking)
      subject.stub(:void_transaction)
      subject.stub(:reverse_transaction)
    end

    it 'calls void_transaction if the payment is being canceled before the end of the day it was created' do
      @payment.update(authorized_at: Time.now)
      subject.should_receive(:void_transaction)
      subject.cancel_transaction({ payment: @payment })
    end

    it 'calls reverse_transaction if the payment is being canceled after the end of the day it was created' do
      @payment.update(authorized_at: (Time.now - 1.day))
      subject.should_receive(:reverse_transaction)
      subject.cancel_transaction({ payment: @payment })
    end

  end

end
