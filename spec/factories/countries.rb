# encoding: utf-8

FactoryGirl.define do
  factory :country do
    iso_initials 'BR'
    name_pt_br 'Brasil'
    name_en 'Brazil'
    name_es 'Brasil'
  end

end
