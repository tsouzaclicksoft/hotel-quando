# encoding: utf-8

FactoryGirl.define do
  factory :state do
    name { Faker::Address.state }
    initials { Faker::Address.state_abbr }
  end
end
