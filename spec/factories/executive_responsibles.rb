# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :executive_responsible do
    name "MyString"
    email "MyString"
    password "MyString"
    goal_contacts 1
    goal_meeting 1
    company_universe nil
    goal_new_companies 1
    goal_companies_actives 1
    goal_reservation 1
  end
end
