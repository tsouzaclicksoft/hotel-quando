# encoding: utf-8

FactoryGirl.define do
  factory :payment do
    status Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response]
    booking
    user { booking.user }
    credit_card { FactoryGirl.create(:credit_card, user: booking.user) }
    maxipago_auth_code "1312"
    maxipago_order_id "343444::3432432::24323423::34432"
    maxipago_response %Q{
                      <?xml version="1.0" encoding="UTF-8"?>
                      <transaction-response>
                        <responseCode>1</responseCode>
                        <responseMessage>AUTHORIZED</responseMessage>
                      </transaction-response>
                     }
  end
end
