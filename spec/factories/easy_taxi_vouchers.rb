# encoding: utf-8

FactoryGirl.define do
  factory :easy_taxi_voucher do
    sequence(:code) { |n| ("CODE #{n}") }
    sequence(:expiration_date) { |n| 1.year.from_now }
    booking nil
  end
end
