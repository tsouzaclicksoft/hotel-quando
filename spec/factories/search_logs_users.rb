FactoryGirl.define do
  factory :search_logs_user do
    sequence(:email) { |n| "user_email#{n}@email.com" }
  end
end
