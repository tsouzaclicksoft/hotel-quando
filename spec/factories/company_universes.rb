# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company_universe do
    name "MyString"
    social_name "MyString"
    cnpj "MyString"
    address "MyString"
    qtd_employees 1
    setor "MyString"
    has_travel_deman false
    qtd_reservations_per_month 1
  end
end
