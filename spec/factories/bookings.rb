# encoding: utf-8

FactoryGirl.define do
  factory :booking do
    status Booking::ACCEPTED_STATUSES[:waiting_for_payment]
    sequence(:guest_name) { |n| "guest_user_#{n}" }
    sequence(:note) { |n| "user note #{n}" }
    sequence(:hotel_comments) { |n| "guest_user_#{n}" }
    number_of_people 1
    delayed_checkout_flag false
    refund_error false
    offer { FactoryGirl.create(:offer) }
    hotel  { offer.hotel }
    room_type { offer.room_type }
    checkin_date { (offer.checkin_timestamp.to_date) }
    room { offer.room }
    pack_in_hours { offer.pack_in_hours }
    date_interval { "'(#{(offer.checkin_timestamp.to_datetime - (Offer::ROOM_CLEANING_TIME - 1.minute)).strftime("%Y-%m-%d %H:%M:%S")},
                       #{(offer.checkout_timestamp.to_datetime + (Offer::ROOM_CLEANING_TIME - 1.minute)).strftime("%Y-%m-%d %H:%M:%S")})'"}
    user { FactoryGirl.create(:user) }
    phone_number '1111111111'

    cached_room_type_initial_capacity { offer.room_type_initial_capacity }
    cached_room_type_maximum_capacity { offer.room_type_maximum_capacity  }
    cached_offer_extra_price_per_person { offer.extra_price_per_person }
    cached_offer_price { offer.price }
    cached_offer_no_show_value { offer.no_show_value }


    trait :omnibees do

      omnibees_offer { FactoryGirl.create(:omnibees_offer) }
      sequence(:omnibees_code) { |n| n.to_s }
      hotel  { omnibees_offer.hotel }
      room_type { omnibees_offer.room_type }
      checkin_date { (omnibees_offer.checkin_timestamp.to_date) }
      room { FactoryGirl.create(:room, hotel: hotel, room_type: room_type) }
      pack_in_hours { omnibees_offer.pack_in_hours }
      date_interval { "'(#{(omnibees_offer.checkin_timestamp.to_datetime - (Offer::ROOM_CLEANING_TIME - 1.minute)).strftime("%Y-%m-%d %H:%M:%S")},
                         #{(omnibees_offer.checkout_timestamp.to_datetime + (Offer::ROOM_CLEANING_TIME - 1.minute)).strftime("%Y-%m-%d %H:%M:%S")})'"}
    end
  end
end
