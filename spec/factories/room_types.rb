FactoryGirl.define do
  factory :room_type do
    sequence(:name_pt_br) { |n| "Room type br #{n}" }
    sequence(:name_en) { |n| "Room type en #{n}" }
    sequence(:description_pt_br) { |n| "Description pt_br #{n}" }
    sequence(:description_en) { |n| "Description en #{n}" }
    maximum_capacity 15
    single_bed_quantity 3
    double_bed_quantity 2
    hotel


    trait :omnibees do
      hotel { FactoryGirl.create(:hotel, :omnibees) }
      sequence(:omnibees_code) { |n| n.to_s }
    end
  end
end
