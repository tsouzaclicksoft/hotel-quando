# encoding: utf-8

FactoryGirl.define do
  factory :city do
    name { Faker::Address.city }
    state
  end

end
