FactoryGirl.define do
  factory :omnibees_offer do
    sequence(:checkin_timestamp) { |n| (DateTime.now.beginning_of_day + 2.day +  n.hours) }
    pack_in_hours 3
    checkout_timestamp { checkin_timestamp + pack_in_hours.hours }
    price 150.0
    hotel { FactoryGirl.create(:hotel, :omnibees) }
    room_type { FactoryGirl.create(:room_type, :omnibees, :hotel => hotel) }
  end
end
