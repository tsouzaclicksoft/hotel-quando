FactoryGirl.define do
  factory :hotel_search_log do
    latitude 23.1
    longitude 23.1
    checkin_date '12/12/2014'
    checkin_hour 23
    number_of_people 2
    length_of_pack 3
    sequence(:address) { |n| "Rua das bananas #{n}" }
    city_name 'Paraná'
    state_name 'São Paulo'
  end
end
