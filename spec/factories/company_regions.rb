# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company_region do
    name "MyString"
    city nil
    is_active false
  end
end
