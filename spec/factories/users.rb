# encoding: utf-8
FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "User #{n}" }
    sequence(:email) { |n| "user_email#{n}@email.com" }
    password '123456789'
    password_confirmation '123456789'
    cpf "287.388.435-52"
    passport "85323372605"
    gender "male"
    country "BR"
    state "São Paulo"
    city 'São Paulo'
    street "Rua das bananeiras"
    number 123
    complement ""
    locale 'pt_br'
    maxipago_token "f=34sa"
    birth_date { Date.today - 25.years }

    trait(:employee) do
      budget_for_24_hours_pack 200
      monthly_budget 1000
      company
    end
  end
end
