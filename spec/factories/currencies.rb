FactoryGirl.define do
  factory :currency do
    name 'Real'
    country { create(:country) }
    value BigDecimal.new(1)
    money_sign 'R$'
    code 'BRL'
  end
end
