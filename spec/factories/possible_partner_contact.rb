# encoding: utf-8
FactoryGirl.define do
  factory :possible_partner_contact do
    sequence(:name) { |n| "Hotel name #{n}" }
    sequence(:email_for_contact) { |n| "email#{n}@email.com" }
    sequence(:message) { |n| "Message #{n}" }
    sequence(:contact_name) { |n| "Name #{n}" }
    phone '(11) 1234-5678'
    locale 'pt_br'
    type_of_contact PossiblePartnerContact::TYPES_OF_CONTACT[:hotel_contact]
    admin_visualized false
  end
end
