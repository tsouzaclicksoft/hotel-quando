# encoding: utf-8

FactoryGirl.define do
  factory :credit_card do
    flag_name 'visa'
    owner_cpf '643.582.080-53'
    owner_passport '31195855'
    billing_name "João da silva"
    billing_address1 'Rua das bananeiras'
    billing_city 'São Paulo'
    billing_state 'SP'
    billing_zipcode '12312-312'
    billing_country 'BR'
    billing_phone '(11) 1233-3321'
    last_four_digits 4356
    security_code 342
    expiration_year 2030
    expiration_month 07
    temporary_number "4444444444444448"
    maxipago_token "a3=423"
    user
  end
end
