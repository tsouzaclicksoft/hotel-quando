# encoding: utf-8

FactoryGirl.define do
  factory :refund do
    status Refund::ACCEPTED_STATUSES[:success]
    payment { FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:refunded],
      booking: FactoryGirl.create(:booking, pack_in_hours: 36, cached_offer_price: 250, cached_offer_no_show_value: 100))}
    user { payment.user }
    value { payment.booking.no_show_refund_value}
    maxipago_auth_code { payment.maxipago_auth_code}
    maxipago_order_id { payment.maxipago_order_id }
    maxipago_response %Q{
                      <?xml version="1.0" encoding="UTF-8"?>
                      <transaction-response>
                        <responseCode>1</responseCode>
                        <responseMessage>AUTHORIZED</responseMessage>
                      </transaction-response>
                     }
  end
end
