# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company_remuneration do
    remuneration_by_new_company ""
    remuneration_by_reservation ""
  end
end
