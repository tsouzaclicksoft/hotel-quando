FactoryGirl.define do
  factory :room_type_pricing_policy do
    pack_price_3h  '1'
    pack_price_6h  '2'
    pack_price_9h  '3'
    pack_price_12h '4'
    pack_price_24h '5'
    pack_price_36h '6'
    pack_price_48h '7'
    extra_price_per_person_for_pack_price_3h  '10'
    extra_price_per_person_for_pack_price_6h  '20'
    extra_price_per_person_for_pack_price_9h  '30'
    extra_price_per_person_for_pack_price_12h '40'
    extra_price_per_person_for_pack_price_24h '50'
    extra_price_per_person_for_pack_price_36h '60'
    extra_price_per_person_for_pack_price_48h '70'
    pricing_policy
    room_type { FactoryGirl.create(:room_type, hotel: pricing_policy.hotel) }

  end
end
