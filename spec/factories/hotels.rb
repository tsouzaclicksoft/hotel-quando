# encoding: utf-8

FactoryGirl.define do
  factory :hotel do
    name { Faker::Company.name }
    description_en        "Description in en"
    description_es        "Description in es"
    description_pt_br     "Description in pt_br"
    payment_method_pt_br  "Payment method in pt_br"
    payment_method_en     "Payment method in en"
    payment_method_es     "Payment method in es"

    latitude  { Faker::Address.latitude  }
    longitude { Faker::Address.longitude }
    category  { "#{Faker::Number.between(2, 5)} estrelas" }
    iss_in_percentage { Faker::Number.between(0, 10) }
    country { FactoryGirl.create(:country, iso_initials: 'BR') }
    currency_symbol { FactoryGirl.create(:currency).money_sign }
    minimum_hours_of_notice { Faker::Number.number(2) }
    password { Faker::Internet.password }
    password_confirmation { Faker::Internet.password }
    email { Faker::Internet.email }
    login { Faker::Internet.user_name }
    street { Faker::Address.street_name }
    number { Faker::Number.number(2) }
    accept_online_payment true
    state Faker::Address.state_abbr
    city

    trait :omnibees do
      sequence(:omnibees_code) { |n| n.to_s }
    end
  end
end
