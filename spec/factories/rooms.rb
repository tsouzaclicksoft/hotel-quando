FactoryGirl.define do
  factory :room do
    sequence(:number) { |n| "#{n}a" }
    hotel
    room_type { FactoryGirl.create(:room_type, :hotel => hotel)}
  end
end
