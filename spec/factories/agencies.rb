FactoryGirl.define do
  factory :agency do
    name  { Faker::Company.name }
    login { Faker::Internet.user_name }
    comission_in_percentage { Faker::Number.decimal(2) }
    password { Faker::Internet.password }
    password_confirmation { Faker::Internet.password }
    observations { Faker::Lorem.paragraph }
    email { Faker::Internet.email }
    sign_email { Faker::Internet.email }
  end
end
