FactoryGirl.define do
  factory :pricing_policy do
    sequence(:name) { |n| "Pricing policy #{n}" }
    hotel
  end
end
