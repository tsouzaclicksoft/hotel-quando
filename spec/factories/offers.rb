FactoryGirl.define do
  factory :offer do
    sequence(:checkin_timestamp) { |n| (DateTime.now.beginning_of_day + 2.day +  n.hours) }
    sequence(:checkin_timestamp_day_of_week) { |n| (DateTime.now.beginning_of_day + 1.day +  n.hours).cwday }
    pack_in_hours 3
    checkout_timestamp { checkin_timestamp + pack_in_hours.hours }
    price 150.0
    extra_price_per_person 100
    room_type_initial_capacity 1
    room_type_maximum_capacity 15
    status Offer::ACCEPTED_STATUS[:available]
    no_show_value 150
    hotel
    room_type { FactoryGirl.create(:room_type, :hotel => hotel) }
    room { FactoryGirl.create(:room, room_type: room_type, :hotel => hotel) }
    log_id { 1 }
  end
end
