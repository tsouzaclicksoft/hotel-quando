# encoding: utf-8
FactoryGirl.define do
  factory :company do
    sequence(:name) { |n| "Company #{n}" }
    sequence(:responsible_name) { |n| "Responsible #{n}" }
    sequence(:email) { |n| "company_email#{n}@email.com" }
    password '123456789'
    password_confirmation '123456789'
    cnpj "26.146.865/0001-54"
    country "Brasil"
    state "São Paulo"
    city 'São Paulo'
    street "Rua das bananeiras"
    number 123
    complement ""
    maxipago_token "f=34sa"
    agency
  end
end
