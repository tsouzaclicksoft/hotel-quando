# encoding: utf-8

FactoryGirl.define do
  factory :department do
    sequence(:name) { |n| "Department #{n}" }
    company
  end

end
