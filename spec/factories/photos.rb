# encoding: utf-8

FactoryGirl.define do
  factory :photo do
    sequence(:title_pt_br) { |n| "Title pt #{n}" }
    sequence(:title_en) { |n| "Title en #{n}" }
    file { test_uploaded_file("codus.jpg") }
    is_main false
  end
end
