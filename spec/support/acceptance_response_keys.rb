class AcceptanceResponseKeys

  def self.hotel_preview
    [:cep, :city_id, :complement, :id, :latitude, :longitude, :name, :number, :rooms_url, :self_url, :state, :street]
  end

end
