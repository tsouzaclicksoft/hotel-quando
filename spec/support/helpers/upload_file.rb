module RSpec::Helpers
  module UploadFile
    # Helper method to create upload files for testing
    def test_uploaded_file(file_name, content_type='image/jpg')
      Rack::Test::UploadedFile.new(fixture_file_path(file_name), content_type)
    end

    # Create default file name for attaching fixture files in tests
    def fixture_file_path(file_name)
      ::Rails.root.join('spec', 'fixtures', file_name).to_s
    end

    def load_fixture(file_path)
      File.open(fixture_file_path(file_path)).read.encode("UTF-8")
    end
  end
end
