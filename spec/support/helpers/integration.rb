# encoding: utf-8

module RSpec::Helpers
  module Integration
    def should_be_on(path)
      URI.parse(current_url).path.should == path
    end

    def should_not_be_on(path)
      URI.parse(current_url).path.should_not == path
    end

    def wait_for_ajax_end(options = {})
      options = {selector: '.cpy_ajax_end', timeout: 10}.merge(options)
      count = 0;
      begin
        while(all(options[:selector]).count == 0)
          count += 1
          if(count > options[:timeout] * 20)
            fail "Waiting for ajax end: You are waiting for #{options[:selector]} to be visible, but it exceeded the timeout of #{options[:timeout]} seconds."
          end
          sleep(0.05)
        end
      rescue
        if(count > options[:timeout] * 20)
          puts "Waiting for ajax end: You are waiting for #{options[:selector]} to be visible, but it exceeded the timeout of #{options[:timeout]} seconds."
        else
          sleep(0.05)
          count += 1
          retry
        end
      end
    end

    def wait_for_element_to_be_hidden(selector, timeout = Capybara.default_wait_time)
      count = 0;
      begin
        while(all(selector).count > 0)
          count += 1
          if(count > timeout * 20)
            fail "You are waiting for #{selector} to be visible, but it exceeded the timeout of #{timeout} seconds."
          end
          sleep(0.05)
        end
      rescue
        if(count > timeout * 20)
          fail "You are waiting for #{selector} to be visible, but it exceeded the timeout of #{timeout} seconds."
        end
        sleep(0.05)
        count += 1
        retry
      end
    end

    def wait_for_element_to_be_visible(selector, timeout = Capybara.default_wait_time)
      count = 0;

      begin
        while(all(selector).count == 0)
          count += 1
          if(count > timeout * 20)
            fail "You are waiting for #{selector} to be visible, but it exceeded the timeout of #{timeout} seconds."
          end
          sleep(0.05)
        end
      rescue
        if(count > timeout * 20)
          fail "You are waiting for #{selector} to be visible, but it exceeded the timeout of #{timeout} seconds."
        end
        sleep(0.05)
        count += 1
        retry
      end
    end

    def sign_in_admin
      name = ENV["ADMIN_PANEL_USER_LOGIN"]
      password = ENV["ADMIN_PANEL_USER_PASSWORD"]

      if page.driver.respond_to?(:basic_auth)
        page.driver.basic_auth(name, password)
      elsif page.driver.respond_to?(:basic_authorize)
        page.driver.basic_authorize(name, password)
      elsif page.driver.respond_to?(:browser) && page.driver.browser.respond_to?(:basic_authorize)
        page.driver.browser.basic_authorize(name, password)
      else
        raise "Error in admin HTTP login."
      end
    end

    def sign_in_hotel(hotel)
      visit hotel_root_path
      fill_in 'hotel_hotel_login', with: hotel.login
      fill_in 'hotel_hotel_password', with: '123456789'
      click_button 'Entrar'
      page.should have_content("Bem-vindo ao Hotel Quando.")
    end

    def sign_in_agency(agency)
      visit agency_root_path
      fill_in 'agency_agency_email', with: agency.email
      fill_in 'agency_agency_password', with: '123456789'
      click_button 'Entrar'
      page.should have_content("Bem-vindo ao Hotel Quando.")
    end

    def sign_in_company(company)
      visit company_root_path
      fill_in 'company_company_email', with: company.email
      fill_in 'company_company_password', with: '123456789'
      click_button 'Entrar'
      page.should have_content("Bem-vindo ao Hotel Quando.")
    end

    def sign_in_user(user, try=nil)
      user.confirmed_at = DateTime.now
      user.save!
      visit new_public_user_session_path
      fill_in 'public_user_email', with: user.email
      fill_in 'public_user_password', with: '123456789'
      page.find('.cpy-submit-button').click
      unless try
        page.should have_content("Bem-vindo ao Hotel Quando.")
      else
        page.should have_content("Login ou senha inválidos.")
      end
    end

    def ensure_clicking_on_element_that_hides_after_click(element_to_click)
      # Sometimes, if you click on an element (button of a modal, for example) the clicking may not trigger the js function
      # clicking on the element this way, ensure that capybara will keep clicking on the element while it is visible
      begin
        200.times do
          page.find(element_to_click).click
        end
      rescue
      end
    end

  end
end
