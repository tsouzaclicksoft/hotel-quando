RSpec.shared_examples "a default response api" do |http_status|

  it "Does return http status #{http_status}" do
    expect(response.status).to eq(http_status)
  end

  it "Does return content-type JSON" do
    expect(response.content_type).to eq("application/json")
  end

end

RSpec.shared_examples "a list for api" do |total, object_attributes=nil|

  it_behaves_like "a default response api"

  it "Does return body with total equal #{total}" do
    expect(response_json[:total]).to eq(total)
  end

  # it "Does return body with items #{itens.empty? ? 'empty' : 'not empty'}" do
  #   expect(response_json[:items]).to eq(itens)
  # end
  
  it "Does return itens with attributes #{object_attributes}" do
    expect(response_json[:items].first.keys).to match_array(object_attributes)
  end

end
