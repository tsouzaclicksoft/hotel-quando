require "spec_helper"

RSpec.describe "PublicAffiliate::Api::V1::bookings", type: :routing do

  it "GET /affiliate/api/v1/bookings" do
    expect(get: public_affiliate_api_v1_bookings_path).to \
      route_to(controller: "public_affiliate/api/v1/bookings", action: "index", format: "json")
  end

  it "GET /affiliate/api/v1/bookings/:id" do
    expect(get: public_affiliate_api_v1_booking_path(1)).to \
      route_to(controller: "public_affiliate/api/v1/bookings", action: "show", format: "json", id: "1")
  end

  it "DELETE /affiliate/api/v1/bookings/:id" do
    expect(delete: public_affiliate_api_v1_booking_path(1)).to \
      route_to(controller: "public_affiliate/api/v1/bookings", action: "destroy", format: "json", id: "1")
  end

  it "POST /affiliate/api/v1/bookings" do
    expect(post: public_affiliate_api_v1_bookings_path).to \
      route_to(controller: "public_affiliate/api/v1/bookings", action: "create", format: "json")
  end

end
