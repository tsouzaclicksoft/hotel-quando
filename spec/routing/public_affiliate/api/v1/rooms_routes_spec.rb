require "spec_helper"

RSpec.describe "PublicAffiliate::Api::V1::Rooms", type: :routing do

  it "GET /affiliate/api/v1/hotel/:hotel_id/rooms/:id" do
    expect(get: public_affiliate_api_v1_hotel_room_path(1, 2)).to \
      route_to(controller: "public_affiliate/api/v1/rooms", action: "show", format: "json", hotel_id: "1", id: "2")
  end

  it "GET /affiliate/api/v1/hotel/:hotel_id/rooms" do
    expect(get: public_affiliate_api_v1_hotel_rooms_path(1)).to \
      route_to(controller: "public_affiliate/api/v1/rooms", action: "index", format: "json", hotel_id: "1")
  end
end
