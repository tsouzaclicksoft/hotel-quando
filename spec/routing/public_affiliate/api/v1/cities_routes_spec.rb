require "spec_helper"

RSpec.describe "PublicAffiliate::Api::V1::Cities", type: :routing do

  it "GET /affiliate/api/v1/cities" do
    expect(get: public_affiliate_api_v1_cities_path).to \
      route_to(controller: "public_affiliate/api/v1/cities", action: "index", format: "json")
  end

end
