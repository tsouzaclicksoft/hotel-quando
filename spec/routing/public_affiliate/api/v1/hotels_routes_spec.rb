require "spec_helper"

RSpec.describe "PublicAffiliate::Api::V1::Hotels", type: :routing do

  it "GET /affiliate/api/v1/hotels" do
    expect(get: public_affiliate_api_v1_hotels_path).to \
      route_to(controller: "public_affiliate/api/v1/hotels", action: "index", format: "json")
  end

  it "GET /affiliate/api/v1/hotels/:id" do
    expect(get: public_affiliate_api_v1_hotel_path(1)).to \
      route_to(controller: "public_affiliate/api/v1/hotels", action: "show", format: "json", id: "1")
  end

end
