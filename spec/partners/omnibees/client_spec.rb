require 'spec_helper'


describe Omnibees::Client do


  describe "#get_offers" do
    let(:params) {
      {
        number_of_guests: 1,
        check_in_datetime: '12/12/2015 12:00 +0300'.to_datetime,
        hotel_codes: [975, 980],
        pack_in_hours: 12
      }
    }

    before(:each) do
      stub_const('Omnibees::Client::RequestXMLGenerator::GetOffers', Class.new do
        def initialize(params)
        end

        def generate
          'fakexml'
        end
      end)
    end

    it "returns the xml for available offers" do
      expect(subject).to receive(:call_action).with(:get_hotel_avail).and_return("testbody")
      expect(subject.get_offers(params)).to eq("testbody")
    end

    it 'raises if any of required params are missing' do
      expect {
        subject.get_offers(number_of_guests: 3)
      }.to raise_error("You must provide all this parameteres: #{[:number_of_guests, :check_in_datetime, :pack_in_hours, :hotel_codes].join(',')}")
    end
  end


  describe '#make_booking' do
    let(:params) {
      {
        booking: {},
        credit_card: {}
      }
    }

    before(:each) do
      stub_const('Omnibees::Client::RequestXMLGenerator::MakeBooking', Class.new do
        def initialize(params)
        end

        def generate
          'xml'
        end
      end)
    end

    it 'raises if any of required params are missing' do
      expect {
        subject.make_booking({})
      }.to raise_error("You must provide all this parameteres: #{[:booking].join(',')}")
    end

    it 'returns the xml for a booking' do
      expect(subject).to receive(:call_action).with(:send_hotel_res).and_return("testbody")
      expect(subject.make_booking(params)).to eq("testbody")
    end
  end

  describe "#check_params!" do
    it "raises if any of items on array are missing from params" do
      expect do
        subject.send(:check_params!, [:param1, :param2, :param3], { param1: 1, param3: 3})
      end.to raise_error Omnibees::Client::MissingParameterError
    end

    it "does nothing if all required params are present" do
      expect do
        subject.send(:check_params!, [:param1, :param2, :param3], { param1: 1, param2: 2, param3: 3})
      end.to_not raise_error Omnibees::Client::MissingParameterError
    end
  end
end
