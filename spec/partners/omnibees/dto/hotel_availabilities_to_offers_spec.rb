# encoding: utf-8
require 'spec_helper'

describe Omnibees::DTO::HotelAvailabilitiesToOffers do
  before(:each) do
    @hotel1 = FactoryGirl.create(:hotel, omnibees_code: '980', iss_in_percentage: 0)
    @hotel2 = FactoryGirl.create(:hotel, omnibees_code: '975', iss_in_percentage: 0)
    @hotel3 = FactoryGirl.create(:hotel, omnibees_code: '1053', iss_in_percentage: 0)
    allow(Omnibees::DTO::Filters::HotelOmnibeesConfig).to receive(:filter) { |offers| offers }
    allow(Omnibees::DTO::Filters::NonConflictingOffers).to receive(:filter) { |offers| offers }
  end

  describe '.parse#to_offers' do
    let(:xml_2h_2rt) { load_fixture('omnibees/hotel_availabilities_1.xml') }
    let(:xml_1h_8rt_12rp) { load_fixture('omnibees/hotel_availabilities_2.xml') }
    let(:params) { { check_in_datetime: "12/12/2015 15:00 +0000".to_datetime, pack_in_hours: 3, number_of_guests: 2 } }
    it "checks the required params"
    context 'something raises error' do
      subject {
        dto = Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt)
        allow(dto).to receive(:persist_room_types!).and_raise("Error")
        dto
      }

      it "notifies Airbrake" do
        expect(Airbrake).to receive(:notify).and_return('')
        subject.to_offers(params)
      end

      it "returns an empty array" do
        allow(Airbrake).to receive(:notify).and_return('')
        expect(subject.to_offers(params)).to eq([])
      end
    end

    context 'persisting the roomtypes' do
      it "persists the room types if not exists" do
        expect(RoomType.count).to be(0)
        Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt).to_offers(params)
        expect(RoomType.count).to be(2)
      end

      it "saves the fields according to the language" do
        Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt).to_offers(params)
        room_type_en = RoomType.find_by(omnibees_code: '5203')
        room_type_pt = RoomType.find_by(omnibees_code: '2645')
        expect(room_type_en.description_pt_br).to eq('en description')
        expect(room_type_en.name_pt_br).to eq('SUITE SENIOR CASAL')
        expect(room_type_en.description_en).to eq('en description')
        expect(room_type_pt.description_pt_br).to eq('descricao pt')
        expect(room_type_pt.name_pt_br).to eq('Luxo Cama Casal')
        expect(room_type_pt.description_en).to be_blank
      end

      it "sets the maximum and minimum correctly" do
        Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt).to_offers(params)
        room_type_en = RoomType.find_by(omnibees_code: '5203')
        room_type_pt = RoomType.find_by(omnibees_code: '2645')
        expect(room_type_en.initial_capacity).to be(1)
        expect(room_type_pt.initial_capacity).to be(1)
        expect(room_type_en.maximum_capacity).to be(4)
        expect(room_type_pt.maximum_capacity).to be(3)
      end

      it "updates roomtype if already exists" do
        room_type_en = FactoryGirl.create(:room_type, hotel: @hotel1, omnibees_code: '5203', description_en: 'fakedesk', name_en: 'fakename')
        Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt).to_offers(params)
        room_type_en.reload
        expect(room_type_en.description_en).to eq('fakedesk')
        expect(room_type_en.name_en).to eq('fakename')
      end

      it "saves multiple roomtypes" do
        expect(RoomType.count).to be(0)
        Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_1h_8rt_12rp).to_offers(params)
        expect(RoomType.count).to be(8)
      end
    end

    context "returning offers" do
      it "sets the offer fields" do
        offers = Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt).to_offers(params)
        expect(offers.size).to eq(2)
        offers.each do |offer|
          expect(offer.checkin_timestamp).to eq(params[:check_in_datetime])
          expect(offer.pack_in_hours).to eq(params[:pack_in_hours])
          expect(offer.number_of_guests).to eq(params[:number_of_guests])
        end
        en_offer = offers.find { |o| o.room_type.omnibees_code == '5203'}
        pt_offer = offers.find { |o| o.room_type.omnibees_code == '2645'}
        expect(en_offer.hotel_id).to eq(@hotel1.id)
        expect(en_offer.room_type_xml.squish).to eq(%Q{
          <RoomType>
              <AmenitiesType nil="true"/>
              <NumberOfUnits>40</NumberOfUnits>
              <Occupancies>
                  <Occupancy>
                      <AgeQualifyingCode>Adult</AgeQualifyingCode>
                      <MaxAge nil="true"/>
                      <MaxOccupancy>3</MaxOccupancy>
                      <MinAge nil="true"/>
                      <MinOccupancy>2</MinOccupancy>
                  </Occupancy>
                  <Occupancy>
                      <AgeQualifyingCode>Child</AgeQualifyingCode>
                      <MaxAge nil="true"/>
                      <MaxOccupancy>1</MaxOccupancy>
                      <MinAge nil="true"/>
                      <MinOccupancy>1</MinOccupancy>
                  </Occupancy>
              </Occupancies>
              <RoomDescription>
                  <Description>en description</Description>
                  <Language>en</Language>
              </RoomDescription>
              <RoomID>5203</RoomID>
              <RoomName>SUITE SENIOR CASAL</RoomName>
          </RoomType>
        }.squish)
        expect(en_offer.room_rate_xml.squish).to eq(%Q{<RoomRate> <AdvanceBookingRestriction nil=\"true\"/> <Discount nil=\"true\"/> <EffectiveDate>2015-06-05T00:00:00</EffectiveDate> <ExpireDate>2015-06-06T00:00:00</ExpireDate> <GroupCode nil=\"true\"/> <PromotionCode nil=\"true\"/> <RatePlanID>23552</RatePlanID> <RatesType> <Rates> <Rate> <EffectiveDate>2015-06-05T00:00:00</EffectiveDate> <MaxLOS nil=\"true\"/> <MinLOS nil=\"true\"/> <Total> <AmountAfterTax>0</AmountAfterTax> <AmountBeforeTax>70.00</AmountBeforeTax> <AmountIncludingMarkup>true</AmountIncludingMarkup> <AmountIsPackage>false</AmountIsPackage> <ChargeType>PerRoomPerNight</ChargeType> <CurrencyCode>BRL</CurrencyCode> </Total> </Rate> </Rates> </RatesType> <RoomID>5203</RoomID> <RoomStayCandidateRPH>0</RoomStayCandidateRPH> <ServiceRPHs> <ServiceRPH> <IsPerRoom>true</IsPerRoom> <RPH>3072</RPH> </ServiceRPH> <ServiceRPH> <IsPerRoom>false</IsPerRoom> <RPH>3073</RPH> </ServiceRPH> </ServiceRPHs> <Total> <AmountAfterTax>85.80</AmountAfterTax> <AmountBeforeTax>70.00</AmountBeforeTax> <AmountIncludingMarkup>true</AmountIncludingMarkup> <AmountIsPackage>false</AmountIsPackage> <ChargeType>PerRoomPerStay</ChargeType> <CurrencyCode>BRL</CurrencyCode> </Total> </RoomRate>}.squish)
        expect(en_offer.rate_plan_xml.squish).to eq(%Q{
<RatePlanType>
    <AdditionalDetailsType>
        <AdditionalDetails>
            <AdditionalDetail>
                <DetailDescription>
                    <Description>Check-in 12.00 bla bla.</Description>
                    <Language>en</Language>
                    <Name>General Policy</Name>
                </DetailDescription>
            </AdditionalDetail>
        </AdditionalDetails>
    </AdditionalDetailsType>
    <CancelPenalties>
        <CancelPenalty>
            <AmountPercent>
                <Amount>0</Amount>
                <CurrencyCode>BRL</CurrencyCode>
                <NmbrOfNights>0</NmbrOfNights>
                <Percent>100.00</Percent>
            </AmountPercent>
            <DeadLine>
                <AbsoluteDeadline nil="true"/>
                <OffsetDropTime>BeforeArrival</OffsetDropTime>
                <OffsetUnitMultiplier>1</OffsetUnitMultiplier>
                <TimeUnitType>Day</TimeUnitType>
            </DeadLine>
            <Duration nil="true"/>
            <End nil="true"/>
            <NonRefundable>false</NonRefundable>
            <PenaltyDescription>
                <Description>No onus to 24 hours before check-in.</Description>
                <Language>en</Language>
                <Name>Cancellation Policy </Name>
            </PenaltyDescription>
            <Start nil="true"/>
        </CancelPenalty>
    </CancelPenalties>
    <EffectiveDate nil="true"/>
    <ExpireDate nil="true"/>
    <Guarantees>
        <Guarantee>
            <Duration nil="true"/>
            <End nil="true"/>
            <GuaranteeDescription>
                <Description>Guarantee the first night.</Description>
                <Language>en</Language>
                <Name>No Show Guarantee</Name>
            </GuaranteeDescription>
            <GuaranteesAcceptedType nil="true"/>
            <Start nil="true"/>
        </Guarantee>
        <Guarantee>
            <Duration nil="true"/>
            <End nil="true"/>
            <GuaranteeDescription nil="true"/>
            <GuaranteesAcceptedType>
                <GuaranteesAccepted>
                    <GuaranteeAccepted>
                        <GuaranteeTypeCode>CreditCard</GuaranteeTypeCode>
                        <PaymentCard nil="true"/>
                        <RPH>0</RPH>
                    </GuaranteeAccepted>
                    <GuaranteeAccepted>
                        <GuaranteeTypeCode>DirectBill</GuaranteeTypeCode>
                        <PaymentCard nil="true"/>
                        <RPH>0</RPH>
                    </GuaranteeAccepted>
                    <GuaranteeAccepted>
                        <GuaranteeTypeCode>Voucher</GuaranteeTypeCode>
                        <PaymentCard nil="true"/>
                        <RPH>0</RPH>
                    </GuaranteeAccepted>
                    <GuaranteeAccepted>
                        <GuaranteeTypeCode>PrePay</GuaranteeTypeCode>
                        <PaymentCard nil="true"/>
                        <RPH>0</RPH>
                    </GuaranteeAccepted>
                </GuaranteesAccepted>
            </GuaranteesAcceptedType>
            <Start nil="true"/>
        </Guarantee>
    </Guarantees>
    <MealsIncluded>
        <Breakfast>true</Breakfast>
        <Dinner>false</Dinner>
        <ID>3071</ID>
        <Lunch>false</Lunch>
        <MealPlanCode>BedBreakfast</MealPlanCode>
        <MealPlanIndicator>true</MealPlanIndicator>
    </MealsIncluded>
    <Offers nil="true"/>
    <RatePlanDescription>
        <Description nil="true"/>
        <Language>en</Language>
    </RatePlanDescription>
    <RatePlanID>23552</RatePlanID>
    <RatePlanInclusions>
        <RatePlanInclusion>
            <Duration nil="true"/>
            <End nil="true"/>
            <ID>3077</ID>
            <RatePlanInclusionDesciption>
                <Description>Biking</Description>
                <Language>en</Language>
                <Name>Biking</Name>
            </RatePlanInclusionDesciption>
            <Start nil="true"/>
        </RatePlanInclusion>
        <RatePlanInclusion>
            <Duration nil="true"/>
            <End nil="true"/>
            <ID>3078</ID>
            <RatePlanInclusionDesciption>
                <Description>Welcome Pack </Description>
                <Language>en</Language>
                <Name>Welcome Pack </Name>
            </RatePlanInclusionDesciption>
            <Start nil="true"/>
        </RatePlanInclusion>
    </RatePlanInclusions>
    <RatePlanName>SUMMER PROMO</RatePlanName>
    <TaxPolicies>
        <TaxPolicy>
            <CurrencyCode>BRL</CurrencyCode>
            <Description>Rate ISS </Description>
            <ID>47</ID>
            <IsPerNight>false</IsPerNight>
            <IsPerPerson>false</IsPerPerson>
            <IsPerRoom>true</IsPerRoom>
            <IsPerStay>true</IsPerStay>
            <IsValuePercentage>true</IsValuePercentage>
            <Name>Rate ISS </Name>
            <Value>5.00</Value>
        </TaxPolicy>
    </TaxPolicies>
</RatePlanType>}.squish)
        expect(pt_offer.hotel_id).to eq(@hotel2.id)
      end

      context 'there is no bookings' do
        it "returns the cheapest offer for each roomtype setting all relevant attributes (case 1)" do
          offers = Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt).to_offers(params)
          expect(offers.size).to eq(2)
          en_offer = offers.find { |o| o.room_type.omnibees_code == '5203'}
          pt_offer = offers.find { |o| o.room_type.omnibees_code == '2645'}
          expect(pt_offer.price.to_f).to eq(105.00)
          expect(en_offer.price.to_f).to eq(85.80)
        end

        it "returns the cheapest offer for each roomtype setting all relevant attributes (case 2)" do
          offers = Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_1h_8rt_12rp).to_offers(params)
          expect(offers.size).to eq(8)
          expect(offers.find { |o| o.room_type.omnibees_code == '16719'}.price.to_f).to eq(408.45)
          expect(offers.find { |o| o.room_type.omnibees_code == '16713'}.price.to_f).to eq(529.10)
          expect(offers.find { |o| o.room_type.omnibees_code == '2982'}.price.to_f).to eq(529.10)
          expect(offers.find { |o| o.room_type.omnibees_code == '2984'}.price.to_f).to eq(570.55)
          expect(offers.find { |o| o.room_type.omnibees_code == '16714'}.price.to_f).to eq(612.08)
          expect(offers.find { |o| o.room_type.omnibees_code == '16712'}.price.to_f).to eq(225.50)
          expect(offers.find { |o| o.room_type.omnibees_code == '4270'}.price.to_f).to eq(650.00)
          expect(offers.find { |o| o.room_type.omnibees_code == '2985'}.price.to_f).to eq(770.00)
        end
      end

      context 'there are conflicting bookings' do
        it "returns the offer if there is rooms left (case with 3 rooms left for en offer)" do
          room_type_en = FactoryGirl.create(:room_type, hotel: @hotel3, omnibees_code: '5203')
          7.times do |i|
            offer = FactoryGirl.create(:offer, hotel: @hotel1, room_type: room_type_en)
            FactoryGirl.create(:booking, offer: offer, status: 2)
          end
          offers = Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt).to_offers(params)
          expect(offers.size).to eq(2)
          en_offer = offers.find { |o| o.room_type.omnibees_code == '5203'}
          pt_offer = offers.find { |o| o.room_type.omnibees_code == '2645'}
          expect(pt_offer.price.to_f).to eq(105.00)
          expect(en_offer.price.to_f).to eq(85.80)
        end

        it "uses filters objects before returning the offers" do
          parser = Omnibees::DTO::HotelAvailabilitiesToOffers.parse(xml_2h_2rt)
          allow(parser).to receive(:filter_criterias).and_return([Class.new do
            def self.filter(offers)
              [1,2,3,4]
            end
          end,
          Class.new do
            def self.filter(offers)
              offers = offers + [66]
              offers
            end
          end])
          expect(parser.to_offers(params)).to eq([1,2,3,4, 66])

        end
      end
    end
  end
end
