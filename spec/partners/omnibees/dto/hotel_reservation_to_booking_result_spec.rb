# encoding: utf-8
require 'spec_helper'

describe Omnibees::DTO::HotelReservationToBookingResult do

  describe '.parse' do
    context 'xml has reservation id' do
      let(:successfull_booking) { load_fixture('omnibees/hotel_reservation_ok_1.xml') }
      subject { Omnibees::DTO::HotelReservationToBookingResult.parse(successfull_booking) }

      specify "confirmed?" do
        expect(subject.confirmed?).to be_true
      end

      specify "reservation_id" do
        expect(subject.reservation_id).to eq('RES005101-1053')
      end

      specify "response_xml" do
        expect(subject.response_xml.to_s.squish).to eq(successfull_booking.squish)
      end

    end

    context "xml doesn't have reservation id" do
      let(:error_booking) { '<noop></noop>' }

      subject { Omnibees::DTO::HotelReservationToBookingResult.parse(error_booking) }

      specify "confirmed?" do
        expect(subject.confirmed?).to be_false
      end

      specify "reservation_id" do
        expect(subject.reservation_id).to eq('')
      end

      specify "response_xml" do
        expect(subject.response_xml).to eq(error_booking)
      end

    end
  end
end
