# encoding: utf-8
require 'spec_helper'

describe Omnibees::DTO::Filters::HotelOmnibeesConfig do
  subject { Omnibees::DTO::Filters::HotelOmnibeesConfig }

  it "returns the original array if hotel accetps everything" do
    check_in = '12/12/2019 12:00 +0000'.to_datetime
    omn_offer = FactoryGirl.build(:omnibees_offer, checkin_timestamp: check_in)
    omn_offer2 = FactoryGirl.build(:omnibees_offer, checkin_timestamp: check_in)
    offers = subject.filter([omn_offer, omn_offer2])
    expect(offers.size).to eq(2)
  end

  it "Checks if hotel support the offer checkin hour for the pack" do
    check_in = '12/12/2019 12:00 +0000'.to_datetime
    omn_offer = FactoryGirl.build(:omnibees_offer, pack_in_hours: 6, checkin_timestamp: check_in)
    omn_offer2 = FactoryGirl.build(:omnibees_offer, pack_in_hours: 9, checkin_timestamp: check_in)
    omn_offer3 = FactoryGirl.build(:omnibees_offer, pack_in_hours: 12, checkin_timestamp: check_in)

    omn_offer.hotel.omnibees_config = { accepted_hours_for_6h_pack: [8,9,10,11,13,14,15] }
    offers = subject.filter([omn_offer, omn_offer2, omn_offer3])
    expect(offers.size).to eq(2)
  end

  it "checks if hotel supports the pack" do
    check_in = '12/12/2019 12:00 +0000'.to_datetime
    omn_offer = FactoryGirl.build(:omnibees_offer, pack_in_hours: 6, checkin_timestamp: check_in)
    omn_offer2 = FactoryGirl.build(:omnibees_offer, pack_in_hours: 9, checkin_timestamp: check_in)
    omn_offer3 = FactoryGirl.build(:omnibees_offer, pack_in_hours: 12, checkin_timestamp: check_in)
    omn_offer.hotel.omnibees_config = {accepts_12h_pack: false}
    omn_offer3.hotel.omnibees_config = {accepts_12h_pack: false}
    offers = subject.filter([omn_offer, omn_offer2, omn_offer3])
    expect(offers.size).to eq(2)
  end

end
