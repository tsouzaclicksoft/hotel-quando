# encoding: utf-8
require 'spec_helper'

describe Omnibees::DTO::Filters::NonConflictingOffers do
  subject { Omnibees::DTO::Filters::NonConflictingOffers }
  before(:each) do
    @check_in = '12/12/2019 12:00 +0000'.to_datetime
    #random booking in a random hotel/roomtype/room
    FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in))
    # random bookings for different room types on the same hotel
    @hotel = FactoryGirl.create(:hotel, :omnibees)
    @rt1 = FactoryGirl.create(:room_type, :omnibees, hotel: @hotel) # 5 rooms
    @rt2 = FactoryGirl.create(:room_type, :omnibees, hotel: @hotel) # 10 rooms
    @rt3 = FactoryGirl.create(:room_type, :omnibees, hotel: @hotel) # 9 rooms
    @rt4 = FactoryGirl.create(:room_type, :omnibees, hotel: @hotel) # 1 room
  end


  context 'no conflicting booking' do
    before(:each) do
      # considering a total of 10 rooms per room type per day
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in, hotel: @hotel, room_type: @rt2))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in, hotel: @hotel, room_type: @rt2))
      @omn_offer = FactoryGirl.build(:omnibees_offer, hotel: @hotel, room_type: @rt3, checkin_timestamp: @check_in)
      @omn_offer.room_type_xml = "<rt><NumberOfUnits>40</NumberOfUnits></rt>"
    end

    describe '.filter' do
      it "returns all the offers" do
        filtered_offs = subject.filter([@omn_offer,@omn_offer,@omn_offer])
        expect(filtered_offs.size).to eq(3)
      end
    end

    specify '#total_rooms' do
      expect(subject.new([]).send(:total_rooms, @omn_offer)).to eq(10)
    end

    specify '#conflict_bookings_count' do
      expect(subject.new([]).send(:conflict_bookings_count, @omn_offer)).to eq(0)
    end
  end

  context 'conflicting bookings with room available' do
    before(:each) do
      #conflict
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in, hotel: @hotel, room_type: @rt1))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in, hotel: @hotel, room_type: @rt1))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: (@check_in - 1.hours), hotel: @hotel, room_type: @rt1))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: (@check_in - 2.hours), hotel: @hotel, room_type: @rt1))
      #nonconflict
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: (@check_in - 10.hours), hotel: @hotel, room_type: @rt1))
      @omn_offer = FactoryGirl.build(:omnibees_offer, hotel: @hotel, room_type: @rt1, checkin_timestamp: @check_in)
      @omn_offer.room_type_xml = "<rt><NumberOfUnits>15</NumberOfUnits></rt>"
    end

    describe '.filter' do
      it "returns all the offers" do
        filtered_offs = subject.filter([@omn_offer,@omn_offer,@omn_offer])
        expect(filtered_offs.size).to eq(3)
      end
    end

    specify '#total_rooms' do
      expect(subject.new([]).send(:total_rooms, @omn_offer)).to eq(5)
    end

    specify '#conflict_bookings_count' do
      expect(subject.new([]).send(:conflict_bookings_count, @omn_offer)).to eq(4)
    end
  end


  context 'conflicting bookings without room available' do
    before(:each) do
      # rt1 full
      # 5 conflicting
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in - 4.hours, hotel: @hotel, room_type: @rt1, pack_in_hours: 6))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in, hotel: @hotel, room_type: @rt1))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in, hotel: @hotel, room_type: @rt1))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: (@check_in - 1.hours), hotel: @hotel, room_type: @rt1))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: (@check_in - 2.hours), hotel: @hotel, room_type: @rt1))
      # 1 nonconflict
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: (@check_in - 10.hours), hotel: @hotel, room_type: @rt1))

      #rt2 1 available
      # 8 conflicting
      6.times { FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in, hotel: @hotel, room_type: @rt2)) }
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in-7.hours, hotel: @hotel, room_type: @rt2, pack_in_hours: 12))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in-5.hours, hotel: @hotel, room_type: @rt2, pack_in_hours: 6))
      # 2 non conflicting
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: (@check_in - 10.hours), hotel: @hotel, room_type: @rt2))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: (@check_in - 4.hours), hotel: @hotel, room_type: @rt2))

      #rt3 full
      # 9 conflicting
      7.times { FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in, hotel: @hotel, room_type: @rt3)) }
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in-8.hours, hotel: @hotel, room_type: @rt3, pack_in_hours: 9))
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in-1.hours, hotel: @hotel, room_type: @rt3, pack_in_hours: 6))

      #rt4 free
      # 1 nonconflicting
      FactoryGirl.create(:booking, :omnibees, omnibees_offer: FactoryGirl.create(:omnibees_offer, checkin_timestamp: @check_in+4.hours, hotel: @hotel, room_type: @rt4, pack_in_hours: 6))

      @omn_offer1 = FactoryGirl.build(:omnibees_offer, hotel: @hotel, room_type: @rt1, checkin_timestamp: @check_in)
      @omn_offer1.room_type_xml = "<rt><NumberOfUnits>14</NumberOfUnits></rt>"
      @omn_offer2 = FactoryGirl.build(:omnibees_offer, hotel: @hotel, room_type: @rt2, checkin_timestamp: @check_in)
      @omn_offer2.room_type_xml = "<rt><NumberOfUnits>30</NumberOfUnits></rt>"
      @omn_offer3 = FactoryGirl.build(:omnibees_offer, hotel: @hotel, room_type: @rt3, checkin_timestamp: @check_in)
      @omn_offer3.room_type_xml = "<rt><NumberOfUnits>27</NumberOfUnits></rt>"
      @omn_offer4 = FactoryGirl.build(:omnibees_offer, hotel: @hotel, room_type: @rt4, checkin_timestamp: @check_in)
      @omn_offer4.room_type_xml = "<rt><NumberOfUnits>4</NumberOfUnits></rt>"
    end

    describe '.filter' do
      it "returns all the offers" do
        filtered_offs = subject.filter([@omn_offer1,@omn_offer2,@omn_offer3,@omn_offer4])
        expect(filtered_offs).to eq([@omn_offer2,@omn_offer4])
      end
    end

    specify '#total_rooms' do
      expect(subject.new([]).send(:total_rooms, @omn_offer1)).to eq(5)
      expect(subject.new([]).send(:total_rooms, @omn_offer2)).to eq(10)
      expect(subject.new([]).send(:total_rooms, @omn_offer3)).to eq(9)
      expect(subject.new([]).send(:total_rooms, @omn_offer4)).to eq(1)
    end

    specify '#conflict_bookings_count' do
      expect(subject.new([]).send(:conflict_bookings_count, @omn_offer1)).to eq(5)
      expect(subject.new([]).send(:conflict_bookings_count, @omn_offer2)).to eq(8)
      expect(subject.new([]).send(:conflict_bookings_count, @omn_offer3)).to eq(9)
      expect(subject.new([]).send(:conflict_bookings_count, @omn_offer4)).to eq(0)
    end
  end

end
