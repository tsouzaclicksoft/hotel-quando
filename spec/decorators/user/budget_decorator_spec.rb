require 'spec_helper'

describe User::BudgetDecorator do
  describe 'a decorated user' do
    before(:each) do
      @user = FactoryGirl.create(:user, :employee, monthly_budget: 250, budget_for_24_hours_pack: 250)
      @decorated_user = User::BudgetDecorator.decorate(@user)
    end

    describe 'can_afford?' do
      it 'returns true if the user is not a company employee' do
        user = FactoryGirl.create(:user)
        decorated_user = User::BudgetDecorator.decorate(user)
        decorated_user.can_afford?(pack_in_hours: 12, price_without_tax: 9999999999999).should == true
      end

      it 'returns true if the price is on the budget' do
        @decorated_user.can_afford?(pack_in_hours: 12, price_without_tax: 250*0.65).should == true
      end

      it 'returns true if the offer is on the budget' do
        offer = FactoryGirl.create(:offer, price: 0.69*240, pack_in_hours: 12)
        @decorated_user.can_afford?(pack_in_hours: 12, offer: offer).should == true
      end

      it 'returns false if the price is outside the budget' do
        @decorated_user.can_afford?(pack_in_hours: 12, price_without_tax: 250).should == false
        @decorated_user.can_afford?(pack_in_hours: 12, price_without_tax: 240).should == false
      end

      it 'returns false if the offer is outside the budget' do
        offer = FactoryGirl.create(:offer, price: 240, pack_in_hours: 12, extra_price_per_person: 10, room_type_maximum_capacity: 10)
        @decorated_user.can_afford?(pack_in_hours: 12, offer: offer, number_of_people: 2).should == false
      end

      it 'returns false if there is already a created booking compromising the budget' do
        Timecop.travel(Time.now.beginning_of_month)
        @decorated_user.can_afford?(pack_in_hours: 12, price_without_tax: 10).should == true
        offer = FactoryGirl.create(:offer, price: 250, checkin_timestamp: Time.now + 3.days)
        FactoryGirl.create(:booking, user: @decorated_user, status: Booking::ACCEPTED_STATUSES[:confirmed], offer: offer)
        @decorated_user.can_afford?(pack_in_hours: 12, price_without_tax: 10, reload: true).should == false
        Timecop.return
      end
    end

    describe 'available_budget_in_current_month' do
      it 'returns the monthly budget of the user minus the total_spent_in_current_month' do
        @decorated_user.stub(:total_spent_in_current_month).and_return(200)
        @decorated_user.available_budget_in_current_month.to_f.should == 50
      end
    end

    describe 'total_spent_in_current_month' do
      before(:each) do
        offer = FactoryGirl.create(:offer, price: 250, checkin_timestamp: Time.now + 3.days)
        FactoryGirl.create(:booking, user: @decorated_user, status: Booking::ACCEPTED_STATUSES[:confirmed], offer: offer, booking_tax: 10)
      end

      it 'gets the price sum of the bookings with the checkin on the current month' do
        @decorated_user.total_spent_in_current_month.to_f.should == 260
      end
    end
  end
end
