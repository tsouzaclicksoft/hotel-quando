# encoding: utf-8

require 'spec_helper'

describe ProcessOffersRemovalWorker do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    @policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
    @room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    @room = FactoryGirl.create(:room, room_type: @room_type, hotel: @hotel)
    @room_type_policy = FactoryGirl.create(:room_type_pricing_policy, pricing_policy: @policy, room_type: @room_type)
    @dummy_params = ActiveSupport::HashWithIndifferentAccess.new
    @dummy_params = {"pricing_policy_id"=>"#{@policy.id}", "initial_date"=>"01/03/2020", "final_date"=>"01/03/2020", "selected_days_hash"=>{"1"=>"monday", "2"=>"tuesday", "3"=>"wednesday", "4"=>"thursday", "5"=>"friday", "6"=>"saturday", "7"=>"sunday"}, "room_types_ids"=>["#{@room_type.id}"], "rooms_ids"=>["#{@room.id}"], "selected_pack_lengths"=>["3"], "action"=>"create", "controller"=>"hotel/offers"}
    @log = FactoryGirl.create(:offers_log, hotel: @hotel)
  end

  it 'should remove the available offers' do
    FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: ((@dummy_params['initial_date'].to_date) + 6.hours), checkout_timestamp: ((@dummy_params['initial_date'].to_date) + 9.hours), room: @room, room_type: @room_type, hotel: @hotel)
    FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: ((@dummy_params['initial_date'].to_date) + 7.hours), checkout_timestamp: ((@dummy_params['initial_date'].to_date) + 10.hours), room: @room, room_type: @room_type, hotel: @hotel)
    Offer.where(status: Offer::ACCEPTED_STATUS[:removed_by_hotel]).count.should == 0

    worker = ProcessOffersRemovalWorker.new
    worker.perform(@log.id, @hotel.id, @dummy_params)
    Offer.where(status: Offer::ACCEPTED_STATUS[:removed_by_hotel]).count.should == 2
  end

  it 'should only remove the available offers that are specified in the params' do
    FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: ((@dummy_params['initial_date'].to_date) + 6.hours), checkout_timestamp: ((@dummy_params['initial_date'].to_date) + 9.hours), room: @room, room_type: @room_type, hotel: @hotel)
    FactoryGirl.create(:offer, pack_in_hours: 6, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: ((@dummy_params['initial_date'].to_date) + 6.hours), checkout_timestamp: ((@dummy_params['initial_date'].to_date) + 12.hours), room: @room, room_type: @room_type, hotel: @hotel)
    Offer.where(status: Offer::ACCEPTED_STATUS[:removed_by_hotel]).count.should == 0

    worker = ProcessOffersRemovalWorker.new
    worker.perform(@log.id, @hotel.id, @dummy_params)
    Offer.where(status: Offer::ACCEPTED_STATUS[:removed_by_hotel]).count.should == 1
    Offer.where(status: Offer::ACCEPTED_STATUS[:available]).count.should == 1
  end

  it 'should change the status of suspended offers to ghost' do
    FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:suspended], checkin_timestamp: ((@dummy_params['initial_date'].to_date) + 6.hours), checkout_timestamp: ((@dummy_params['initial_date'].to_date) + 9.hours), room: @room, room_type: @room_type, hotel: @hotel)
    Offer.count.should == 1

    worker = ProcessOffersRemovalWorker.new
    worker.perform(@log.id, @hotel.id, @dummy_params)
    Offer.where("status = #{Offer::ACCEPTED_STATUS[:ghost]}").count.should == 1
    Offer.where("status = #{Offer::ACCEPTED_STATUS[:suspended]}").count.should == 0
  end

  it 'should do nothing with reserved offers' do
    FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:reserved], checkin_timestamp: ((@dummy_params['initial_date'].to_date) + 6.hours), checkout_timestamp: ((@dummy_params['initial_date'].to_date) + 9.hours), room: @room, room_type: @room_type, hotel: @hotel)

    Offer.count.should == 1
    worker = ProcessOffersRemovalWorker.new
    worker.perform(@log.id, @hotel.id, @dummy_params)
    Offer.count.should == 1
  end

end
