# encoding: utf-8

require 'spec_helper'

describe CreateOffersWorker do
  let(:worker){ CreateOffersWorker.new }

  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    @policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
    @room_type1 = FactoryGirl.create(:room_type, hotel: @hotel)
    @room11 = FactoryGirl.create(:room, room_type: @room_type1, hotel: @hotel)
    @room12 = FactoryGirl.create(:room, room_type: @room_type1, hotel: @hotel)
    @room13 = FactoryGirl.create(:room, room_type: @room_type1, hotel: @hotel)
    @room14 = FactoryGirl.create(:room, room_type: @room_type1, hotel: @hotel)
    @room_type2 = FactoryGirl.create(:room_type, hotel: @hotel)
    @room21 = FactoryGirl.create(:room, room_type: @room_type2, hotel: @hotel)
    @room22 = FactoryGirl.create(:room, room_type: @room_type2, hotel: @hotel)
    @room_type_policy1 = FactoryGirl.create(:room_type_pricing_policy, pricing_policy: @policy, room_type: @room_type1)
    @room_type_policy2 = FactoryGirl.create(:room_type_pricing_policy, pricing_policy: @policy, room_type: @room_type2)
    @log = OffersLog.new
    # 47 days - 13 non selected days of week = 33
    # 4 rooms
    # 6 packs/hours
    @log.form_params = {
      "pricing_policy_id"=>@policy.id,
      "initial_date"=> "01/03/2020",
      "final_date"=> "15/04/2020",
      "selected_days_hash" =>{"1"=>"monday", "2"=>"tuesday", "5"=>"friday", "6"=>"saturday", "7"=>"sunday"},
      "room_types_ids"=> [@room_type1.id, @room_type2.id],
      "rooms_ids" => [@room11.id, @room12.id, @room13.id, @room21.id],
      "selected_pack_lengths" =>["3", "6", "9", "12", "24", "36", "48"],
      "pack" => {
        "6"=>[4, 5, 20],
        "9"=>[8, 9],
        "12"=>[17]
      }
    }
    @log.hotel_id = @hotel.id
    @log.save!

    worker.instance_variable_set("@formated_params", {
      :initial_date=> "01/03/2020".to_date,
      :final_date=> "15/04/2020".to_date,
      rooms_ids: Room.all.pluck(:id),
      :selected_week_days_hash=> {1 => true, 2 => true, 5=>true, 6=>true, 7=>true},
      :selected_week_days_codes_array => [1,2,5,6,7],
      :selected_hours_per_pack => {
        6 =>[4, 5, 20],
        9 =>[8, 9],
        12 =>[17]
      }
    })
    worker.instance_variable_set("@selected_pricing_policy", @policy)
  end

  describe 'get_formated_params' do
    before(:each) do
      @hash = ActiveSupport::HashWithIndifferentAccess.new
      @hash = {"pricing_policy_id"=>"1", "initial_date"=>"01/03/2020", "final_date"=>"01/03/2020", "selected_days_hash"=>{"7"=>"sunday", "4"=>"thursday"}, "days_of_week"=>["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"], "room_types_ids"=>["1"], "rooms_ids"=>["1", "4", "6"], "selected_pack_lengths"=>["3", "6", "9", "12", "24", "36", "48"], "pack"=>{"3 horas"=>["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"]}, "action"=>"create", "controller"=>"hotel/offers"}
      @result = {
        :initial_date=>"Sun, 01 Mar 2020".to_date,
        :final_date=>"Sun,
        01 Mar 2020".to_date,
        :rooms_ids => [1, 4, 6],
        :selected_week_days_hash=>{7=>true, 4=>true},
        :selected_week_days_codes_array => [7, 4],
        :selected_hours_per_pack => {
          3 =>[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
        }
      }
    end

    it "should return a hash with initial date" do
      worker.get_formated_params(@hash)[:initial_date].should == @result[:initial_date]
    end
    it "should return a hash with final date" do
      worker.get_formated_params(@hash)[:final_date].should == @result[:final_date]
    end

    it "should return a hash with selected_days_hash" do
      worker.get_formated_params(@hash)[:selected_week_days_hash].should == @result[:selected_week_days_hash]
    end

    it "should return an array with the day codes" do
      worker.get_formated_params(@hash)[:selected_week_days_codes_array].should == @result[:selected_week_days_codes_array]
    end

    it "should return a hash with selected_hours_per_pack" do
      worker.get_formated_params(@hash)[:selected_hours_per_pack].should == @result[:selected_hours_per_pack]
    end

    it 'should get formatted params' do
      expect(worker.get_formated_params(@hash)).to eq(@result)
    end

    it "should return the rooms ids" do
      worker.get_formated_params(@hash)[:rooms_ids].should == @result[:rooms_ids]
    end

  end

  describe "get_room_types_array" do
    before(:each) do
      @result = {:initial_date=>"Sun, 01 Mar 2020".to_date, :final_date=>"Sun, 01 Mar 2020".to_date, :selected_week_days_hash=>{7=>true}, :selected_hours_per_pack=>{3=>[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]}}
    end

    it "return an array with 2 roomtypes hash" do
      worker.get_room_types_array.count.should == 2
    end

    it "should return the roomtypes ids" do
      worker.get_room_types_array.map { |rt| rt[:id] }.sort.should == [@room_type2.id, @room_type1.id].sort
    end

    it "should return the roomtypes initial capacity" do
      worker.get_room_types_array.map { |rt| rt[:initial_capacity] }.should == [@room_type1.initial_capacity, @room_type2.initial_capacity]
    end

    it "should return the roomtypes maximum capacity" do
      worker.get_room_types_array.map { |rt| rt[:maximum_capacity] }.should == [@room_type1.maximum_capacity, @room_type2.maximum_capacity]
    end

    it "should return the roomtypes rooms ids" do
      worker.get_room_types_array.map { |rt| rt[:rooms].sort }.sort.should == [
        [@room22.id, @room21.id].sort,
        [@room14.id, @room13.id, @room12.id, @room11.id].sort
      ].sort
    end

    describe "returns the price per pack" do
      it "return all prices with non zero" do
        worker.get_room_types_array.first[:price_per_pack].should == {
          3 => 1,
          6 => 2,
          9 => 3,
          12 => 4
        }
      end

      it "return removes prices with zeros" do
        @room_type_policy1.pack_price_6h = 0
        @room_type_policy1.pack_price_24h = 0
        @room_type_policy1.save!

        worker.get_room_types_array.last[:price_per_pack].should == {
          3 => 1,
          9 => 3,
          12 => 4
        }
      end
    end

    describe "returns the now show penalty per pack" do
      it "return all prices with non zero" do
        worker.get_room_types_array.first[:no_show_penalty_per_pack].should == {
          3 => 1,
          6 => 2,
          9 => 3,
          12 => 4
        }
      end

      it "return removes prices with zeros" do
        @room_type_policy1.pack_price_6h = 0
        @room_type_policy1.pack_price_24h = 0
        @room_type_policy1.save!

        worker.get_room_types_array.last[:no_show_penalty_per_pack].should == {
          3 => 1,
          9 => 3,
          12 => 4
        }
      end
    end
  end

  describe 'perform' do
    it "should call connectio execute twice passing first update and then create query" do
      worker.stub(:get_formated_params)
      worker.stub(:get_room_types_array)
      worker.should_receive(:generate_update_query).and_return("update-query")
      worker.should_receive(:generate_insert_query).and_return("insert-query")
      worker.should_receive(:execute_query).with("update-query")
      worker.should_receive(:execute_query).with("insert-query")
      worker.perform(@log.id, @hotel.id, @log.form_params)
    end

    it "saves the used room types array", focus: true do
      worker.perform(@log.id, @log.hotel_id, @log.form_params)
      @log.reload
      JSON.parse(@log.logged_room_types_and_prices_used).sort {|h| h['id']}.should == [
        {
          "id"=>@room_type2.id,
          "extra_price_per_person"=> {
            "3"=>"10.0",
            "6"=>"20.0",
            "9"=>"30.0",
            "12"=>"40.0"
          },
          "initial_capacity"=>1,
          "maximum_capacity"=>15,
          "rooms"=>[@room21.id],
          "price_per_pack"=> {
            "3"=>"1.0",
            "6"=>"2.0",
            "9"=>"3.0",
            "12"=>"4.0"
          },
          "no_show_penalty_per_pack"=> {
            "3"=>"1.0",
            "6"=>"2.0",
            "9"=>"3.0",
            "12"=>"4.0"
          }
        },
        {
          "id"=>@room_type1.id,
          "extra_price_per_person"=> {
            "3"=>"10.0",
            "6"=>"20.0",
            "9"=>"30.0",
            "12"=>"40.0"
          },
          "initial_capacity"=>1,
          "maximum_capacity"=>15,
          "rooms"=>[@room13.id, @room12.id, @room11.id],
          "price_per_pack"=> {
            "3"=>"1.0",
            "6"=>"2.0",
            "9"=>"3.0",
            "12"=>"4.0"
          },
         "no_show_penalty_per_pack"=> {
            "3"=>"1.0",
            "6"=>"2.0",
            "9"=>"3.0",
            "12"=>"4.0"
          }
        }
      ].sort {|h| h['id']}
    end

    describe 'integration examples' do
      context 'with no previous offers' do
        it "creates acording the form params" do
          worker.perform(@log.id, @log.hotel_id, @log.form_params)
          Offer.count.should == 33*4*6
          Offer.where("checkin_timestamp = '2020-03-20 08:00:00 +0000' AND room_id = #{@room11.id}").count.should == 1
          Offer.where("checkin_timestamp = '2020-03-17 08:00:00 +0000' AND room_id = #{@room11.id}").count.should == 1
          Offer.where("checkin_timestamp = '2020-03-19 08:00:00 +0000' AND room_id = #{@room11.id}").count.should == 0
          Offer.where("checkin_timestamp = '2020-03-18 08:00:00 +0000' AND room_id = #{@room11.id}").count.should == 0
          Offer.where("checkin_timestamp = '2020-03-20 09:00:00 +0000'").count.should == 4 # four rooms
          Offer.where("checkin_timestamp = '2020-03-20 10:00:00 +0000'").count.should == 0 # four rooms
        end
      end

      context 'with previous offers' do
        before(:each) do
          FactoryGirl.create( :offer,
            checkin_timestamp: '2020-03-20 05:00:00 +0000'.to_datetime,
            checkout_timestamp: '2020-03-20 08:00:00 +0000'.to_datetime,
            pack_in_hours: 3,
            price: 150.0,
            extra_price_per_person: 100,
            status: Offer::ACCEPTED_STATUS[:available],
            hotel: @hotel,
            room_type: @room_type1,
            room: @room11
          )
          FactoryGirl.create( :offer,
            checkin_timestamp: '2020-03-20 05:00:00 +0000'.to_datetime,
            checkout_timestamp: '2020-03-20 11:00:00 +0000'.to_datetime,
            pack_in_hours: 6,
            price: 150.0,
            extra_price_per_person: 100,
            status: Offer::ACCEPTED_STATUS[:available],
            hotel: @hotel,
            room_type: @room_type1,
            room: @room11
          )
          FactoryGirl.create( :offer,
            checkin_timestamp: '2020-03-19 05:00:00 +0000'.to_datetime,
            checkout_timestamp: '2020-03-20 11:00:00 +0000'.to_datetime,
            pack_in_hours: 6,
            price: 150.0,
            extra_price_per_person: 100,
            status: Offer::ACCEPTED_STATUS[:available],
            hotel: @hotel,
            room_type: @room_type1,
            room: @room11
          )
        end

        it "should create if not exists" do
          Offer.count.should == 3
          worker.perform(@log.id, @log.hotel_id, @log.form_params)
          Offer.count.should == 33*4*6 + 2
        end

        context "updating the existing offers acording with the form params" do
          it "updates the available with the price" do
            offer1 = Offer.where("checkin_timestamp = '2020-03-20 05:00:00 +0000' AND pack_in_hours=6 AND room_id = #{@room11.id}").first
            offer2 = Offer.where("checkin_timestamp = '2020-03-20 05:00:00 +0000' AND pack_in_hours=3 AND room_id = #{@room11.id}").first
            offer3 = Offer.where("checkin_timestamp = '2020-03-19 05:00:00 +0000' AND pack_in_hours=6 AND room_id = #{@room11.id}").first
            offer1.price.should == 150
            worker.perform(@log.id, @log.hotel_id, @log.form_params)
            offer1.reload
            offer2.reload
            offer3.reload
            offer1.status.should == Offer::ACCEPTED_STATUS[:available]
            offer2.status.should == Offer::ACCEPTED_STATUS[:available]
            offer3.status.should == Offer::ACCEPTED_STATUS[:available]
            offer1.price.should == 2
            offer2.price.should == 150
            offer3.price.should == 150 # wday not selected
          end

          it "updates the suspended with the price and keep suspended" do
            offer1 = FactoryGirl.create( :offer,
              checkin_timestamp: '2020-03-20 17:00:00 +0000'.to_datetime,
              pack_in_hours: 12,
              price: 160.0,
              extra_price_per_person: 100,
              status: Offer::ACCEPTED_STATUS[:suspended],
              hotel: @hotel,
              room_type: @room_type1,
              room: @room11
            )

            offer2 = FactoryGirl.create( :offer,
              checkin_timestamp: '2020-03-19 17:00:00 +0000'.to_datetime,
              pack_in_hours: 12,
              price: 160.0,
              extra_price_per_person: 100,
              status: Offer::ACCEPTED_STATUS[:suspended],
              hotel: @hotel,
              room_type: @room_type1,
              room: @room11
            )

            worker.perform(@log.id, @log.hotel_id, @log.form_params)
            Offer.where(status: Offer::ACCEPTED_STATUS[:suspended]).count.should == 2
            offer1.reload
            offer2.reload
            offer1.price.should == 4
            offer2.price.should == 160 # wday not selected
          end

          it "updates the ghost and change it to suspended" do
            offer1 = FactoryGirl.create( :offer,
              checkin_timestamp: '2020-03-20 17:00:00 +0000'.to_datetime,
              pack_in_hours: 12,
              price: 90.0,
              extra_price_per_person: 100,
              status: Offer::ACCEPTED_STATUS[:ghost],
              hotel: @hotel,
              room_type: @room_type1,
              room: @room11
            )

            offer2 = FactoryGirl.create( :offer,
              checkin_timestamp: '2020-03-19 17:00:00 +0000'.to_datetime,
              pack_in_hours: 12,
              price: 80.0,
              extra_price_per_person: 100,
              status: Offer::ACCEPTED_STATUS[:ghost],
              hotel: @hotel,
              room_type: @room_type1,
              room: @room11
            )
            Offer.where(status: Offer::ACCEPTED_STATUS[:ghost]).count.should == 2
            worker.perform(@log.id, @log.hotel_id, @log.form_params)
            Offer.where(status: Offer::ACCEPTED_STATUS[:ghost]).count.should == 1
            Offer.where(status: Offer::ACCEPTED_STATUS[:suspended]).count.should == 1
            offer1.reload
            offer2.reload
            offer1.price.should == 4
            offer2.price.should == 80 # wday not selected
          end

          it "updates the removed_by_hotel and change it to available" do
            offer1 = FactoryGirl.create( :offer,
              checkin_timestamp: '2020-03-23 17:00:00 +0000'.to_datetime,
              pack_in_hours: 12,
              price: 320.0,
              extra_price_per_person: 100,
              status: Offer::ACCEPTED_STATUS[:removed_by_hotel],
              hotel: @hotel,
              room_type: @room_type1,
              room: @room11
            )

            offer2 = FactoryGirl.create( :offer,
              checkin_timestamp: '2020-03-18 17:00:00 +0000'.to_datetime,
              pack_in_hours: 12,
              price: 450.0,
              extra_price_per_person: 100,
              status: Offer::ACCEPTED_STATUS[:removed_by_hotel],
              hotel: @hotel,
              room_type: @room_type1,
              room: @room11
            )
            Offer.where(status: Offer::ACCEPTED_STATUS[:removed_by_hotel]).count.should == 2
            worker.perform(@log.id, @log.hotel_id, @log.form_params)
            Offer.where(status: Offer::ACCEPTED_STATUS[:removed_by_hotel]).count.should == 1
            offer1.reload
            offer2.reload
            offer1.status.should == Offer::ACCEPTED_STATUS[:available]
            offer2.status.should == Offer::ACCEPTED_STATUS[:removed_by_hotel]
            offer1.price.should == 4
            offer2.price.should == 450 # wday not selected
          end
        end
      end
    end
  end

  describe "update_query_for_offers_with_status" do
    specify do
      update_set_values = { column1: "alo", column2: "ttt", column3: 43.12}
      status_array = [2, 5,7]
      pack_size = 45
      selected_hours = [3,13,16]
      room_type_info_hash = { id: 333, rooms: [3,4,5,67]}
      worker.update_query_for_offers_with_status(
        update_set_values,
        status_array,
        pack_size,
        room_type_info_hash,
        selected_hours).squish.should == %Q{
          UPDATE offers
          SET
            column1='alo',column2='ttt',column3='43.12'
          WHERE
            (checkin_timestamp BETWEEN '2020-03-01 00:00:00' AND '2020-04-15 23:59:59') AND
            (EXTRACT(HOUR FROM checkin_timestamp)) IN (3,13,16) AND
            (checkin_timestamp_day_of_week IN (1,2,5,6,7)) AND
            (pack_in_hours='45') AND
            (room_id IN (3,4,5,67)) AND
            (room_type_id='333') AND
            (status IN (2,5,7));
      }.squish
    end
  end

end
