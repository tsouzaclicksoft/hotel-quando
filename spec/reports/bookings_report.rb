# encoding: utf-8

require 'spec_helper'

describe 'BookingsReport' do
  before(:each) do
    @bookings = 5.times.map { FactoryGirl.create(:booking) }
  end

  describe '#initialize' do
    it 'is an instance of BookingsReport' do
      expect(BookingsReport.new(@bookings)).to be_a BookingsReport
    end
  end

  describe '#bookings' do
    it 'returns all bookings specified' do
      expect(BookingsReport.new(@bookings).bookings).to eq(@bookings)
    end
  end

  describe '#attributes' do
    booking_attribute_names = ["Nome do titular", "Hotel", "Oferta", "Tipo de quarto", "Quarto", "Valor total", "Status", "Pacote em horas"]
    it 'returns an array of booking attribute names to export' do
      expect(BookingsReport.new(@bookings).attributes).to eq(booking_attribute_names)
    end
  end

  describe '#attribute_values' do
    array_of_checkin_datetime = []
    booking_attribute_values = []

    (1..5).each do |number|
      array_of_checkin_datetime << (DateTime.now.beginning_of_day + 2.day + number.hours).strftime("%m/%d/%Y %k:%M:%S")
    end

    (0..4).each do |counter|
      booking_attribute_values << ["guest_user_" + (counter + 1).to_s, "Hotel " + (counter + 1).to_s, array_of_checkin_datetime[counter], "Room type br " + (counter + 1).to_s, (counter + 1).to_s + "a", 150.0, "Aguardando pagamento", 3]
    end

    it 'returns an array of arrays of booking values' do
      BookingsReport.new(@bookings).attribute_values.each.with_index(0) do |booking_values, counter|
        expect(booking_values).to eq(booking_attribute_values[counter])
      end
    end
  end

  describe '#export' do
    array_of_checkin_datetime = []
    booking_attribute_values = []

    (1..5).each do |number|
      array_of_checkin_datetime << (DateTime.now.beginning_of_day + 2.day + number.hours).strftime("%m/%d/%Y %k:%M:%S")
    end

    (0..4).each do |counter|
      booking_attribute_values << ["guest_user_" + (counter + 1).to_s, "Hotel " + (counter + 1).to_s, array_of_checkin_datetime[counter], "Room type br " + (counter + 1).to_s, (counter + 1).to_s + "a", 150.0, "Aguardando pagamento", 3]
    end

    it 'returns a package with correct booking values' do
      BookingsReport.new(@bookings).export.workbook.worksheets.each.with_index(0) do |worksheet, counter|
        expect(worksheet.rows[0].cells.map(&:value)).to include("Nome do titular", "Hotel", "Oferta", "Tipo de quarto", "Quarto", "Valor total", "Status", "Pacote em horas")
        expect(worksheet.rows[counter + 1].cells.map(&:value)).to eq(booking_attribute_values[counter])
      end
    end
  end
end
