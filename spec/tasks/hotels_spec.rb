# encoding: utf-8

require 'spec_helper'
require 'rake'

describe 'hotels' do
  before :all do
    Rake.application.rake_require "tasks/hotels"
    Rake::Task.define_task(:environment)
  end

  describe 'update_offers_cache_count' do
    def run_rake_task
      Rake::Task["hotels:update_offers_cache_count"].reenable
      Rake.application.invoke_task "hotels:update_offers_cache_count"
    end

    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
      @room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      @room = FactoryGirl.create(:room, room_type: @room_type, hotel: @hotel)
      @room_type_policy = FactoryGirl.create(:room_type_pricing_policy, pricing_policy: @policy, room_type: @room_type)
      FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 3.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:offer, pack_in_hours: 9, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 6.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:offer, pack_in_hours: 9, status: Offer::ACCEPTED_STATUS[:suspended], checkin_timestamp: 7.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 5.days.from_now, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:offer, pack_in_hours: 6, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 5.days.from_now, room: @room, room_type: @room_type, hotel: @hotel)
    end

    specify do
      @hotel.reload
      @hotel.active_offers_count.should == nil
      run_rake_task
      @hotel.reload
      @hotel.active_offers_count.should == 4

    end

  end
end
