# encoding: utf-8

require 'spec_helper'
require 'rake'

describe 'offers' do
  before :all do
    Rake.application.rake_require "tasks/offers"
    Rake::Task.define_task(:environment)
  end

  describe 'expire_olds' do
    def run_rake_task
      Rake::Task["offers:expire_olds"].reenable
      Rake.application.invoke_task "offers:expire_olds"
    end

    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
      @policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
      @room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      @room = FactoryGirl.create(:room, room_type: @room_type, hotel: @hotel)
      @room_type_policy = FactoryGirl.create(:room_type_pricing_policy, pricing_policy: @policy, room_type: @room_type)
      FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 3.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:offer, pack_in_hours: 9, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 6.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:offer, pack_in_hours: 9, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 7.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 5.days.from_now, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:offer, pack_in_hours: 6, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 5.days.from_now, room: @room, room_type: @room_type, hotel: @hotel)
      offer = FactoryGirl.create(:offer, pack_in_hours: 6, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 10.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:booking, pack_in_hours: 6, status: Booking::ACCEPTED_STATUSES[:canceled], offer: offer)

    end

    specify do
      Offer.by_status(Offer::ACCEPTED_STATUS[:available]).count.should == 6
      Offer.count.should == 6
      run_rake_task
      Offer.by_status(Offer::ACCEPTED_STATUS[:available]).count.should == 2
      Offer.by_status(Offer::ACCEPTED_STATUS[:expired]).count.should == 1
      Offer.by_status(Offer::ACCEPTED_STATUS[:waiting_removal]).count.should == 3
    end


    it "doesn't expire offers newer than 3 hours ago" do
      FactoryGirl.create(:offer, pack_in_hours: 6, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 2.hours.ago, room: @room, room_type: @room_type, hotel: @hotel)
      run_rake_task
      Offer.by_status(Offer::ACCEPTED_STATUS[:available]).count.should == 3
      Offer.by_status(Offer::ACCEPTED_STATUS[:waiting_removal]).count.should == 3
    end

  end
end
