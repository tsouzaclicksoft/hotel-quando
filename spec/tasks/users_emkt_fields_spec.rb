# encoding: utf-8

require 'spec_helper'
require 'rake'

describe 'users' do
  before :all do
    Rake.application.rake_require "tasks/users_emkt_fields"
    Rake::Task.define_task(:environment)
  end

  context 'with nil past_emkt_date' do
    let!(:user) { FactoryGirl.create(:user, past_emkt_date: nil)}

    it 'should be updated to contain past_emkt_date' do
      run_rake_task
      user.reload
      expect(user.past_emkt_date).to be_present
    end
  end

  context 'with nil emkt_flux' do
    let!(:user) { FactoryGirl.create(:user, emkt_flux: nil) }
    let!(:user_with_booking) { FactoryGirl.create(:user, emkt_flux: nil) }
    let!(:currency) { FactoryGirl.create(:currency) }
    let!(:booking) { FactoryGirl.create(:booking, user_id: user_with_booking.id, currency_code: currency.code) }

    it 'should be updated to be equal to 1 if user has booking' do
      run_rake_task
      user_with_booking.reload
      expect(user_with_booking.emkt_flux).to be_eql(1)
      expect(user_with_booking).to be_persisted
    end

    it 'should be updated to be equal to 2 if user does not have booking' do
      run_rake_task
      user.reload
      expect(user.emkt_flux).to be_eql(2)
      expect(user).to be_persisted
    end
  end

  context 'with nil emkt_flux_type' do
    let!(:user) { FactoryGirl.create(:user, emkt_flux_type: nil) }
    let!(:user_with_booking) { FactoryGirl.create(:user, emkt_flux_type: nil) }
    let!(:currency) { FactoryGirl.create(:currency) }
    let!(:booking) { FactoryGirl.create(:booking, user_id: user_with_booking.id, currency_code: currency.code) }

    it 'should be updated to be equal to 1 if user has booking' do
      run_rake_task
      user_with_booking.reload
      expect(user_with_booking.emkt_flux).to be_eql(1)
      expect(user_with_booking).to be_persisted
    end

    it 'should be updated to be equal to 2 if user does not have booking' do
      run_rake_task
      user.reload
      expect(user.emkt_flux).to be_eql(2)
      expect(user).to be_persisted
    end
  end


  def has_booking?(user)
    (Booking.where(user_id: user.id).exists?)
  end

  def run_rake_task
    Rake::Task["users_emkt_fields:update"].reenable
    Rake.application.invoke_task "users_emkt_fields:update"
  end
end
