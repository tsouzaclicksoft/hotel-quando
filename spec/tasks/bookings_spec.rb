# encoding: utf-8

require 'spec_helper'
require 'rake'

describe 'bookings' do
  before :all do
    Rake.application.rake_require "tasks/bookings"
    Rake::Task.define_task(:environment)
  end

  describe 'expire_bookings' do
    def run_rake_task
      Rake::Task["bookings:expire_bookings"].reenable
      Rake.application.invoke_task "bookings:expire_bookings"
    end

    it "should expire the bookings that are waiting for payment for more than 20 minutes" do
      booking = FactoryGirl.create(:booking)
      booking.created_at = DateTime.now - 21.minutes
      booking.save
      Booking.any_instance.should_receive :expire
      run_rake_task
    end

    describe 'mail sending' do
      before(:each) do
        @booking = FactoryGirl.create(:booking)
        @booking.created_at = DateTime.now - 21.minutes
        @booking.save
        @mailer_mock = double()
        @mailer_mock.should_receive(:deliver)
      end

      it "should send an email alerting the user that his booking expired, if the booking was created by an user" do
        UserMailer.should_receive(:send_booking_expiration_notice).and_return(@mailer_mock)
        run_rake_task
      end

      it "should send an email alerting the agency that it's client's booking expired, if the booking was created by an agency" do
        @booking.update!(created_by_agency: true)
        UserMailer.should_not_receive(:send_booking_expiration_notice)
        AgencyMailer.should_receive(:send_booking_expiration_notice).and_return(@mailer_mock)
        run_rake_task
      end
    end
  end
end
