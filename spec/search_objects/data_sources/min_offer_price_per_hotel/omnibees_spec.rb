require 'spec_helper'

describe DataSources::MinOfferPricePerHotel::Omnibees do
  before(:each) do
    @checkin = '12/12/2015 13:00 +0300'.to_datetime
    @hotel_1 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244", omnibees_code: '1234')
    @hotel_2 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @hotel_3 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244", omnibees_code: '333')
    @hotel_4 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
  end

  subject { DataSources::MinOfferPricePerHotel::Omnibees.new({number_of_guests: 1, latitude: "-23.5625897", longitude: "-46.6549244", check_in_datetime: @checkin, pack_in_hours: 3}) }

  describe '#results' do
    before(:each) do
      offers = [
        Offer.new(price: 100.0, hotel: @hotel_1),
        Offer.new(price: 2.0, hotel: @hotel_3),
        Offer.new(price: 30.0, hotel: @hotel_3),
        Offer.new(price: 50.0, hotel: @hotel_3),
        Offer.new(price: 99.0, hotel: @hotel_1)
      ]
      allow_any_instance_of(Omnibees::DTO::HotelAvailabilitiesToOffers).to receive(:to_offers).and_return(offers)
      allow_any_instance_of(Omnibees::Client).to receive(:get_offers).and_return("")
      @offer_1 = offers[-1]
      @offer_2 = offers[1]
    end

    describe 'exact results' do
      it "returns the info about the cheapest offer for each hotel" do
        expect(subject.results.exacts.count).to eq(2)
        results = subject.results.exacts
        expect(results[0]).to eq({ hotel: @hotel_1, min_offer: @offer_1 })
        expect(results[1]).to eq({ hotel: @hotel_3, min_offer: @offer_2 })
      end
    end

    describe 'similar results' do
      it "is empty" do
        expect(subject.results.similars.count).to eq(0)
      end
    end
  end

  describe '#nearest_hotels' do
    it "select only Omnibees hotels" do
      expect(subject.send(:nearest_hotels).map(&:id).sort).to eq([@hotel_1.id, @hotel_3.id])
    end
  end
end
