require 'spec_helper'

describe DataSources::MinOfferPricePerHotel::HQ do
  before(:each) do
    @checkin = '12/12/2015 13:00 +0300'.to_datetime
    @hotel_1 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @hotel_2 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @hotel_3 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @hotel_4 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @offer_with_higher_price = FactoryGirl.create(:offer, pack_in_hours:3, price: 60, hotel: @hotel_1, checkin_timestamp: @checkin)
    @offer_with_normal_price = FactoryGirl.create(:offer, pack_in_hours:3, price: '50', hotel: @hotel_1, checkin_timestamp: @checkin)

    @offer_that_doesnt_match_filter = FactoryGirl.create(:offer, pack_in_hours:3, price: '10', hotel: @hotel_1, checkin_timestamp: (@checkin + 1.day))

    @offer_with_higher_price_2 = FactoryGirl.create(:offer, pack_in_hours:3, price: 100.00, hotel: @hotel_2, checkin_timestamp: @checkin)
    @offer_with_normal_price_2 = FactoryGirl.create(:offer, pack_in_hours:3, price: '70', hotel: @hotel_2, checkin_timestamp: @checkin)
    @similar_offer1 = FactoryGirl.create(:offer, pack_in_hours: 3, price: '70', hotel: @hotel_4, checkin_timestamp: @checkin + 1.hour)
    @similar_offer2 = FactoryGirl.create(:offer, pack_in_hours: 6, price: '90', hotel: @hotel_4, checkin_timestamp: @checkin - 1.hour)
    @similar_offer3 = FactoryGirl.create(:offer, pack_in_hours: 6, price: '70', hotel: @hotel_2, checkin_timestamp: @checkin)
  end

  describe '#results' do
    subject { DataSources::MinOfferPricePerHotel::HQ.new({number_of_guests: 1, latitude: "-23.5625897", longitude: "-46.6549244", check_in_datetime: @checkin, pack_in_hours: 3}) }

    specify 'exact results' do
      expect(subject.results.exacts.count).to eq(2)
      results = subject.results.exacts.sort_by { |h| h[:min_price] }
      expect(results[0]).to eq({ hotel: @hotel_2, min_offer: @offer_with_normal_price_2})
      expect(results[1]).to eq({ hotel: @hotel_1, min_offer: @offer_with_normal_price})
    end

    describe 'similar results' do
      it "returns only for hotels not listed on exact matches" do
        expect(subject.results.similars.count).to eq(1)
        expect(subject.results.similars[0]).to eq({ hotel: @hotel_4, min_offer: @similar_offer1 })
      end
    end

  end


end
