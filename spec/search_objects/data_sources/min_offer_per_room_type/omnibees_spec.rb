require 'spec_helper'

describe DataSources::MinOfferPerRoomType::Omnibees do
  before(:each) do
    @checkin = '12/12/2015 13:00 +0300'.to_datetime
    @hotel_1 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @rt1 = FactoryGirl.create(:room_type, hotel: @hotel_1, omnibees_code: '111')
    @rt2 = FactoryGirl.create(:room_type, hotel: @hotel_1, omnibees_code: '222')
    @rt3 = FactoryGirl.create(:room_type, hotel: @hotel_1, omnibees_code: '333')
    @non_match_offer = FactoryGirl.create(:offer, pack_in_hours:3, checkin_timestamp: @checkin)
  end

  describe '#results' do
    before(:each) do
      offers = [
        Offer.new(price: 100.0, hotel: @hotel_1, room_type: @rt1),
        Offer.new(price: 2.0, hotel: @hotel_1, room_type: @rt2),
        Offer.new(price: 30.0, hotel: @hotel_1, room_type: @rt3),
      ]
      allow_any_instance_of(Omnibees::DTO::HotelAvailabilitiesToOffers).to receive(:to_offers).and_return(offers)
      allow_any_instance_of(Omnibees::Client).to receive(:get_offers).and_return("")
    end

    subject { DataSources::MinOfferPerRoomType::Omnibees.new({number_of_guests: 1, hotel_id: @hotel_1.id, check_in_datetime: @checkin, pack_in_hours: 3}) }

    describe 'exact results' do
      it "returns the info about the cheapest offer for each hotel" do
        expect(subject.results.exacts.count).to eq(3)
        results = subject.results.exacts.sort_by { |h| h[:min_offer].price }
        expect(results[0][:min_offer].price).to eq(2.0)
        expect(results[1][:min_offer].price).to eq(30.0)
        expect(results[2][:min_offer].price).to eq(100.0)
      end
    end
  end

end
