require 'spec_helper'

describe DataSources::MinOfferPerRoomType::HQ do
  before(:each) do
    @checkin = '12/12/2015 13:00 +0300'.to_datetime
    @hotel_1 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")

    @non_match_offer = FactoryGirl.create(:offer, pack_in_hours:3, checkin_timestamp: @checkin)
  end

  describe '#results' do

    subject { DataSources::MinOfferPerRoomType::HQ.new({number_of_guests: 1, hotel_id: @hotel_1.id, check_in_datetime: @checkin, pack_in_hours: 3}) }

    specify 'exact results' do
      @offer1 = FactoryGirl.create(:offer, pack_in_hours:3, price: '50', hotel: @hotel_1, checkin_timestamp: @checkin)
      @offer2 = FactoryGirl.create(:offer, pack_in_hours:3, price: '70', hotel: @hotel_1, checkin_timestamp: @checkin)
      @offer3 = FactoryGirl.create(:offer, pack_in_hours:3, price: '90', hotel: @hotel_1, checkin_timestamp: @checkin)
      @similar_offer = FactoryGirl.create(:offer, pack_in_hours:6, price: '90', hotel: @hotel_1, checkin_timestamp: @checkin)

      expect(subject.results.exacts.count).to eq(3)
      results = subject.results.exacts.sort_by { |h| h[:min_offer].price }
      expect(results[0][:min_offer].price).to eq(50)
      expect(results[1][:min_offer].price).to eq(70)
      expect(results[2][:min_offer].price).to eq(90)
    end

    describe 'similar results' do
      before(:each) do
        @similar_offer = FactoryGirl.create(:offer, pack_in_hours:3, price: '90', hotel: @hotel_1, checkin_timestamp: @checkin -1.hour)
      end

      it "is empty if params exact is not empty" do
        allow(subject).to receive(:generate_exacts_results).and_return([2134])
        expect(subject.results.similars).to be_empty
      end
      it "returns only for hotels not listed on exact matches" do
        allow(subject).to receive(:generate_exacts_results).and_return([])
        expect(subject.results.similars.count).to eq(1)
        expect(subject.results.similars[0][:min_offer].price).to eq(90)
      end

      specify "i can get similar results by calling fill_similar_results" do

        allow(subject).to receive(:generate_exacts_results).and_return([2134])
        expect(subject.results.similars).to be_empty
        subject.fill_similar_results

        expect(subject.results.similars.count).to eq(1)
        expect(subject.results.similars[0][:min_offer].price).to eq(90)
      end
    end

  end


end
