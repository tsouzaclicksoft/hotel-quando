require 'spec_helper'

describe OffersSearch::Hotels do
  subject { OffersSearch::Hotels.new({number_of_guests: 1,latitude: 4,longitude: 3,check_in_datetime: DateTime.now,pack_in_hours: 3}) }
  before(:each) do
    result = OpenStruct.new
    result.exacts = [{ hotel: "ehotelhq", min_price: 111.11}]
    result.similars = [{ hotel: "shotelhq", min_price: 111.11}]
    allow_any_instance_of(DataSources::MinOfferPricePerHotel::HQ).to receive(:results).and_return(result)

    result = OpenStruct.new
    result.exacts = [{ hotel: "ehotelomb", min_price: 111.11}]
    result.similars = [{ hotel: "shotelomb", min_price: 111.11}]
    allow_any_instance_of(DataSources::MinOfferPricePerHotel::Omnibees).to receive(:results).and_return(result)
  end

  describe "#results" do
    it "returns the concatened results from each datasource" do
      expect(Set.new(subject.results.exacts)).to eq(Set.new([{ hotel: "ehotelhq", min_price: 111.11}, { hotel: "ehotelomb", min_price: 111.11}]))
      expect(Set.new(subject.results.similars)).to eq(Set.new([{ hotel: "shotelhq", min_price: 111.11}, { hotel: "shotelomb", min_price: 111.11}]))
    end
  end
end
