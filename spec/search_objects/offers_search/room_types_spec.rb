require 'spec_helper'

describe OffersSearch::RoomTypes do
  subject { OffersSearch::RoomTypes.new({number_of_guests: 1,hotel_id: 3,check_in_datetime: DateTime.now,pack_in_hours: 3}) }

  describe "#results" do
    describe 'exact' do
      before(:each) do

        result = OpenStruct.new
        result.exacts = [{ room_type: "eroom_typehq", min_ofer: "minoffer"}]
        result.similars = [{ room_type: "sroom_typehq", min_ofer: "minoffer"}]
        allow_any_instance_of(DataSources::MinOfferPerRoomType::HQ).to receive(:results).and_return(result)

        result = OpenStruct.new
        result.exacts = [{ room_type: "eroom_typeomb", min_ofer: "minoffer"}]
        result.similars = [{ room_type: "sroom_typeomb", min_ofer: "minoffer"}]
        allow_any_instance_of(DataSources::MinOfferPerRoomType::Omnibees).to receive(:results).and_return(result)

      end
      it "returns the result using the hq datasource if hotel is not from omnibees" do
        allow(subject).to receive(:hotel).and_return(Hotel.new)
        expect(Set.new(subject.results.exacts)).to eq(Set.new([{ room_type: "eroom_typehq", min_ofer: "minoffer"}]))
      end

      it "returns the result using the omnibees datasource if hotel is from omnibees" do
        allow(subject).to receive(:hotel).and_return(Hotel.new(omnibees_code: '123'))
        expect(Set.new(subject.results.exacts)).to eq(Set.new([{ room_type: "eroom_typeomb", min_ofer: "minoffer"}]))
      end
    end

    describe 'similar' do
      it "returns datasource result if exacts results is empty" do
        result = OpenStruct.new
        result.exacts = []
        result.similars = [{ room_type: "sroom_typehq", min_ofer: "minoffer"}]
        allow(subject).to receive(:hotel).and_return(Hotel.new)

        allow_any_instance_of(DataSources::MinOfferPerRoomType::HQ).to receive(:results).and_return(result)
        expect(subject.results.exacts.count).to be(0)
        expect(Set.new(subject.results.similars)).to eq(Set.new([{ room_type: "sroom_typehq", min_ofer: "minoffer"}]))
      end
    end
  end
end
