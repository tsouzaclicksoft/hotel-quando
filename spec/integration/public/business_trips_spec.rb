# encoding: utf-8
require 'spec_helper'

story 'As an company I can send a message to admins' do
  before(:each) do
    visit public_get_business_trips_path
  end

  scenario 'when I submit the form, a message should be sent to admins' do
    AdminMailer.any_instance.should_receive :send_new_possible_partner_contact
    fill_in 'company_name', with: 'Company 1'
    fill_in 'contact_name', with: 'Contact 1'
    fill_in 'email_for_contact', with: 'company1@company.com'
    fill_in 'phone', with: '(11) 1234-5678'
    fill_in 'message', with: 'Mensagem'
    page.find('.cpy-submit-button').click()
    page.should have_content('Mensagem enviada com sucesso!')
  end

  scenario 'when I submit the form, a message should be saved to database' do
    fill_in 'company_name', with: 'Company 1'
    fill_in 'contact_name', with: 'Contact 1'
    fill_in 'email_for_contact', with: 'company1@company.com'
    fill_in 'phone', with: '(11) 1234-5678'
    fill_in 'message', with: 'Mensagem'
    page.find('.cpy-submit-button').click()
    page.should have_content('Mensagem enviada com sucesso!')
    PossiblePartnerContact.count.should == 1
  end

  context 'with javascript disabled' do
    scenario 'I should see a message if I submit the form with errors' do
      fill_in 'company_name', with: 'Company 1'
      fill_in 'email_for_contact', with: 'company1@company.com'
      fill_in 'phone', with: '(11) 1234-5678'
      fill_in 'contact_name', with: 'Contact name'
      page.find('.cpy-submit-button').click()
      page.should have_content('Não pudemos enviar sua menssagem pois o formulário não foi preenchido corretamente. Por favor corrija os errors e tente novamente.')
      visit public_get_business_trips_path
      fill_in 'email_for_contact', with: 'company1@company.com'
      fill_in 'phone', with: '(11) 1234-5678'
      fill_in 'message', with: 'Mensagem'
      fill_in 'contact_name', with: 'Contact name'
      page.find('.cpy-submit-button').click()
      page.should have_content('Não pudemos enviar sua menssagem pois o formulário não foi preenchido corretamente. Por favor corrija os errors e tente novamente.')
      visit public_get_business_trips_path
      fill_in 'company_name', with: 'Company 1'
      fill_in 'message', with: 'Mensagem'
      fill_in 'contact_name', with: 'Contact name'
      page.find('.cpy-submit-button').click()
      page.should have_content('Não pudemos enviar sua menssagem pois o formulário não foi preenchido corretamente. Por favor corrija os errors e tente novamente.')
      visit public_get_business_trips_path
      fill_in 'company_name', with: 'Company 1'
      fill_in 'email_for_contact', with: 'company1@company.com'
      fill_in 'phone', with: '(11) 1234-5678'
      fill_in 'message', with: 'Mensagem'
      page.find('.cpy-submit-button').click()
      page.should have_content('Não pudemos enviar sua menssagem pois o formulário não foi preenchido corretamente. Por favor corrija os errors e tente novamente.')
      visit public_get_business_trips_path
      PossiblePartnerContact.count.should == 0
    end
  end

  context 'with javascript enabled', js: true do
    scenario 'the submit button should not be enabled if there are errors in the form' do
      page.find('.cpy-submit-button')['disabled'].should == 'disabled'
      fill_in 'company_name', with: 'Company 1'
      page.find('.cpy-submit-button')['disabled'].should == 'disabled'
      fill_in 'phone', with: '(11) 1234-5678'
      page.find('.cpy-submit-button')['disabled'].should == 'disabled'
      fill_in 'message', with: 'Mensagem valida'
      page.find('.cpy-submit-button')['disabled'].should == 'disabled'
      fill_in 'email_for_contact', with: 'email@invalido'
      page.find('.cpy-submit-button')['disabled'].should == 'disabled'
      fill_in 'email_for_contact', with: 'email@valido.com'
      page.find('.cpy-submit-button')['disabled'].should == 'disabled'
      fill_in 'contact_name', with: 'Contact name'
      page.find('.cpy-submit-button')['disabled'].should_not == 'disabled'
    end
  end
end
