# encoding: utf-8
require 'spec_helper'
require 'fakeweb'
include ActionView::Helpers::NumberHelper

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# This tests need to be runned using rake db:test:clone_structure #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

story 'As a visitor' do
  context 'when visiting the hotel details page and try to reserve offer' do
    before(:each) do
      room = FactoryGirl.create(:room)
      insert_query = %Q{
                        INSERT INTO offers
                            (checkin_timestamp, checkout_timestamp,\
                             extra_price_per_person, hotel_id, log_id, no_show_value,\
                             pack_in_hours, price, room_id, room_type_id, room_type_initial_capacity,\
                             room_type_maximum_capacity, status)
                          VALUES
                            ('#{(DateTime.now.beginning_of_day + 1.day).utc.strftime("%Y-%m-%d %H:%M:%S")}', '#{(DateTime.now.beginning_of_day + 1.day + 3.hours).utc.strftime("%Y-%m-%d %H:%M:%S")}',\
                             '1','#{room.hotel.id}', '1', '1',\
                             '3', '1', '#{room.id}', '#{room.room_type.id}', '1',\
                             '3', '#{ Offer::ACCEPTED_STATUS[:available] }' ),
                            ('#{(DateTime.now.beginning_of_day + 1.day + 1.hour).utc.strftime("%Y-%m-%d %H:%M:%S")}', '#{(DateTime.now.beginning_of_day + 1.day + 4.hours).utc.strftime("%Y-%m-%d %H:%M:%S")}',\
                             '1','#{room.hotel.id}', '1', '1',\
                             '3', '1', '#{room.id}', '#{room.room_type.id}', '1',\
                             '3', '#{ Offer::ACCEPTED_STATUS[:available] }' ),
                            ('#{(DateTime.now.beginning_of_day + 1.day + 3.hour).utc.strftime("%Y-%m-%d %H:%M:%S")}', '#{(DateTime.now.beginning_of_day + 1.day + 6.hours).utc.strftime("%Y-%m-%d %H:%M:%S")}',\
                             '1','#{room.hotel.id}', '1', '1',\
                             '3', '1', '#{room.id}', '#{room.room_type.id}', '1',\
                             '3', '#{ Offer::ACCEPTED_STATUS[:available] }' ),
                            ('#{(DateTime.now.beginning_of_day + 1.day + 4.hour).utc.strftime("%Y-%m-%d %H:%M:%S")}', '#{(DateTime.now.beginning_of_day + 1.day + 7.hours).utc.strftime("%Y-%m-%d %H:%M:%S")}',\
                             '1','#{room.hotel.id}', '1', '1',\
                             '3', '1', '#{room.id}', '#{room.room_type.id}', '1',\
                             '3', '#{ Offer::ACCEPTED_STATUS[:available] }' );
                        }
      ActiveRecord::Base.connection.execute(insert_query)
      @offer = Offer.where(checkin_timestamp: "#{(DateTime.now.beginning_of_day + 1.day).utc.strftime("%Y-%m-%d %H:%M:%S")}").first
      @conflicting_offer = Offer.where(checkin_timestamp: "#{(DateTime.now.beginning_of_day + 1.day + 1.hour).utc.strftime("%Y-%m-%d %H:%M:%S")}").first
      @conflicting_offer_2 = Offer.where(checkin_timestamp: "#{(DateTime.now.beginning_of_day + 1.day + 3.hour).utc.strftime("%Y-%m-%d %H:%M:%S")}").first
      @not_conflicting_offer = Offer.where(checkin_timestamp: "#{(DateTime.now.beginning_of_day + 1.day + 4.hour).utc.strftime("%Y-%m-%d %H:%M:%S")}").first
      @user = FactoryGirl.create(:user)
      reset_session!
      visit public_hotel_path(checkin_date: @offer.checkin_timestamp.to_date, checkin_hour: @offer.checkin_timestamp.hour, length_of_pack: @offer.pack_in_hours, number_of_people: "1", id: @offer.hotel.id, name: @offer.hotel.name)
    end

    context "not logged in" do
      before(:each) do
        page.find('.cpy-reserve-offer-button').click
        @requested_booking_creation_path = new_public_booking_path(checkin_timestamp: @offer.checkin_timestamp.strftime("%Y-%m-%d %H:%M:%S +0000"), pack_in_hours: @offer.pack_in_hours, price: @offer.price, room_type_id: @offer.room_type_id, number_of_people: "1")
      end

      def check_if_user_was_successfully_redirected
        uri = URI.parse(current_url)
        "#{uri.path}?#{uri.query}".should == @requested_booking_creation_path
      end

      scenario 'I should see the booking flow sign in and sign up page instead of the normal one' do
        page.should have_css('.booking-registration-header')
      end

      scenario 'I can sign in and be redirected to create a new booking' do
        fill_in 'sign-in-public_user_email', with: @user.email
        fill_in 'sign-in-public_user_password', with: @user.password
        page.find('.cpy-submit-login-button').click
        check_if_user_was_successfully_redirected
      end

      scenario 'I can register myself and be redirected to create a new booking' do
        fill_in 'sign-up-public_user_name', with: 'User 1'
        fill_in 'sign-up-public_user_email', with: 'user1@email.com'
        fill_in 'sign-up-public_user_phone', with: '(11) 1234-4321'
        fill_in 'sign-up-public_user_password', with: '12344321'
        fill_in 'sign-up-public_user_password_confirmation', with: '12344321'
        fill_in 'sign-up-public_user_birth_date', with: '12/12/1980'
        page.find('.cpy-submit-registration-button').click
        check_if_user_was_successfully_redirected
      end

      scenario 'if I make a mistake filling the form, after correcting the errors I should be redirected to create a new booking' do
        page.find('.cpy-submit-registration-button').click
        fill_in 'sign-up-public_user_name', with: 'User 1'
        fill_in 'sign-up-public_user_email', with: 'user1@email.com'
        fill_in 'sign-up-public_user_phone', with: '(11) 1234-4321'
        fill_in 'sign-up-public_user_birth_date', with: '12/12/1980'
        fill_in 'sign-up-public_user_password', with: '12344321'
        fill_in 'sign-up-public_user_password_confirmation', with: '12344321'
        page.find('.cpy-submit-registration-button').click
        check_if_user_was_successfully_redirected
      end
    end

    scenario 'when the booking is outside my budget, I should not see the button to reserve it' do
      @user.update(monthly_budget: -1, company: FactoryGirl.create(:company))
      sign_in_user @user
      page.should_not have_css('.cpy-reserve-offer-button')
    end

    context "already logged in and visiting the create new booking step", focus: true do
      before(:each) do
        sign_in_user @user
        page.find('.cpy-reserve-offer-button').click
      end

      scenario 'the page should have the informations of the offer that I am trying to reserve', js: true do
        page.should have_content(@offer.checkin_timestamp.strftime('%d/%m/%Y'))
        page.should have_content(@offer.checkin_timestamp.strftime('%H:%M'))
        page.should have_content(@offer.pack_in_hours.to_s + ' horas')
        page.should have_content(@offer.room_type.name_pt_br)
        page.should_not have_content(@conflicting_offer.checkin_timestamp.strftime('%H:%M'))
      end

      scenario 'the number of people dropdown should have the booking price' do
        options_that_select_should_have = []
        (@offer.room_type_initial_capacity..@offer.room_type_maximum_capacity).to_a.each do | i |
          if i == 1
            options_that_select_should_have << "1 pessoa - #{number_to_currency @offer.price + Booking::BOOKING_TAX_IN_BR_CURRENCY}"
          else
            price = @offer.price + Booking::BOOKING_TAX_IN_BR_CURRENCY + ((i - 1) * @offer.extra_price_per_person) # booking total price
            options_that_select_should_have << "#{i} pessoas - #{number_to_currency price}"
          end
        end
        page.should have_select('booking_number_of_people', with_options: options_that_select_should_have)
      end

      context "I can fill the guest name, number of people, and notes" do
        context 'withing my budget' do
          describe "with valid data" do
            before(:each) do
              fill_in 'booking_guest_name', with: 'Name Lastname'
              fill_in 'booking_note', with: 'Note note observation'
              page.find('.cpy-submit-button').click
            end

            scenario "the booking should be created" do
              @user.bookings.count.should == 1
            end

            scenario "the booking's offer and conflicting offers should be suspended" do
              @offer.reload.status.should == Offer::ACCEPTED_STATUS[:reserved]
              @conflicting_offer.reload.status.should == Offer::ACCEPTED_STATUS[:suspended]
              @conflicting_offer_2.reload.status.should == Offer::ACCEPTED_STATUS[:suspended]
              @not_conflicting_offer.reload.status.should == Offer::ACCEPTED_STATUS[:available]
            end

            scenario "I shoud be redirected to payment step" do
              should_be_on new_public_booking_payments_path(booking_id: Booking.first.id)
            end
          end

          describe "with invalid data" do
            before(:each) do
              fill_in 'booking_guest_name', with: ''
              page.find('.cpy-submit-button').click
            end

            scenario "the booking should not be saved to the database" do
              Booking.count.should == 0
            end

            scenario "I should see errors" do
              page.should have_content('não pode ficar em branco')
            end

          end
        end # of within my budget

        context 'outside my budget' do
          describe "with valid data" do
            before(:each) do
              @user.update(company: FactoryGirl.create(:company), monthly_budget: -1)
              fill_in 'booking_guest_name', with: 'Name Lastname'
              fill_in 'booking_note', with: 'Note note observation'
              page.find('.cpy-submit-button').click
            end

            scenario "the booking should not be created" do
              @user.bookings.count.should == 0
            end

            scenario "the booking's offer and conflicting offers should be unchanged" do
              @offer.reload.status.should_not == Offer::ACCEPTED_STATUS[:reserved]
              @conflicting_offer.reload.status.should_not == Offer::ACCEPTED_STATUS[:suspended]
              @conflicting_offer_2.reload.status.should_not == Offer::ACCEPTED_STATUS[:suspended]
            end

            scenario "I should not be redirected to payment step" do
              should_be_on public_root_pt_br_path
            end
          end

          describe "with invalid data" do
            before(:each) do
              fill_in 'booking_guest_name', with: ''
              page.find('.cpy-submit-button').click
            end

            scenario "the booking should not be saved to the database" do
              Booking.count.should == 0
            end

            scenario "I should see errors" do
              page.should have_content('não pode ficar em branco')
            end
          end
        end # of outside my budget
      end
    end

  end

  context "visiting the payment step" do
    context 'MaxiPago enabled' do
      before(:each) do
        stub_const("Public::Bookings::PaymentsController::SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT", false)
        room = FactoryGirl.create(:room)

        insert_query = %Q{
                          INSERT INTO offers
                              (checkin_timestamp, checkout_timestamp,\
                               extra_price_per_person, hotel_id, log_id, no_show_value,\
                               pack_in_hours, price, room_id, room_type_id, room_type_initial_capacity,\
                               room_type_maximum_capacity, status)
                            VALUES
                              ('#{(DateTime.now.beginning_of_day + 1.day).utc.strftime("%Y-%m-%d %H:%M:%S")}', '#{(DateTime.now.beginning_of_day + 1.day + 3.hours).utc.strftime("%Y-%m-%d %H:%M:%S")}',\
                               '1','#{room.hotel.id}', '1', '1',\
                               '3', '1', '#{room.id}', '#{room.room_type.id}', '1',\
                               '1', '#{ Offer::ACCEPTED_STATUS[:available] }' )
                          }
        ActiveRecord::Base.connection.execute(insert_query)
        FactoryGirl.create(:country)
        @booking = FactoryGirl.create(:booking, offer: Offer.first)
        reset_session!
        sign_in_user(@booking.user)
        visit new_public_booking_payments_path(booking_id: @booking.id)
      end

      scenario "I should see the price description" do
        page.should have_content(number_to_currency @booking.initial_price_without_iss)
        page.should have_content(number_to_currency @booking.extra_price_per_person_without_iss)
        page.should have_content("#{@booking.hotel.iss_in_percentage.to_s.gsub('.',',')} %")
        page.should have_content(number_to_currency @booking.total_price)
        visit new_public_booking_payments_path(booking_id: @booking.id)
        page.should have_content(number_to_currency @booking.booking_tax)
      end

      context 'I am a company employee' do
        before(:each) do
          @user = @booking.user
          @user.update(company: FactoryGirl.create(:company), monthly_budget: @booking.total_price + 1000, budget_for_24_hours_pack: @booking.total_price + 1000)
          @user.credit_cards.delete_all
          @company_credit_card = FactoryGirl.create(:credit_card, company: @user.company)
          visit new_public_booking_payments_path(booking_id: @booking.id)
        end

        scenario 'I should not see the new credit card form' do
          page.should_not have_content('Inserir novo cartão de crédito')
          page.should_not have_css('#credit_card_billing_name')
        end

        scenario 'I should not see the option to select to pay online or at the hotel' do
          page.should_not have_css('.cpy-pay-at-the-hotel-radio-button')
          page.should_not have_css('.cpy-pay-online-radio-button')
        end

        scenario 'the existing credit card select should have the company credit card' do
          page.all('#credit_card_id option').count.should == 2 #prompt and option
          page.all('#credit_card_id option')[1]['value'].should.to_s == @company_credit_card.id.to_s
        end
      end

      context "I can fill the owner name, CPF, credit card number, CVV, month and year" do
        describe 'chosing to pay online' do
          before(:each) do
            choose("pay_at_the_hotel", option: 'false')
          end

          describe "with valid data" do
            context 'using an existing credit card' do
              before(:each) do
                @existing_credit_card = FactoryGirl.create(:credit_card, user: @booking.user)
                visit new_public_booking_payments_path(booking_id: @booking.id)
                all('#credit_card_id option')[1].select_option
                choose("pay_at_the_hotel", option: 'false')
              end

              context 'and successfull transaction happens' do
                before(:each) do
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <api-response>
                              <errorCode>0</errorCode>
                              <errorMessage></errorMessage>
                              <command>add-consumer</command>
                              <time>1367849110845</time>
                              <result>
                                <customerId>12590</customerId>
                                <token>5e3K+bwTdBg=</token>
                              </result>
                            </api-response>
                          }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::ApiRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <transaction-response>
                              <orderID>1289</orderID>
                              <responseCode>0</responseCode>
                            </transaction-response>
                           }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )

                end

                scenario "the payment should be created" do
                  page.find('.cpy-submit-button').click
                  Payment.count.should == 1
                  Payment.first.status.should == Payment::ACCEPTED_STATUSES[:captured]
                end

                scenario 'the booking should be confirmed' do
                  page.find('.cpy-submit-button').click
                  @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
                end

                scenario "I shoud be redirected to final step" do
                  page.find('.cpy-submit-button').click
                  should_be_on(public_booking_payments_path(booking_id: @booking.id))
                end

                scenario 'I should receive an email about the booking confirmation' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  UserMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end

                scenario 'the hotel should receive an email about the booking confirmation' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  HotelMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end
              end

              context 'and unsuccessfull transaction happens' do
                before(:each) do
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <api-response>
                              <errorCode>0</errorCode>
                              <errorMessage></errorMessage>
                              <command>add-consumer</command>
                              <time>1367849110845</time>
                              <result>
                                <customerId>12590</customerId>
                                <token>5e3K+bwTdBg=</token>
                              </result>
                            </api-response>
                           }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::ApiRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <transaction-response>
                              <responseCode>1</responseCode>
                            </transaction-response>
                           }

                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )

                end

                scenario "the payment should be created" do
                  page.find('.cpy-submit-button').click
                  Payment.count.should == 1
                  Payment.first.status.should == Payment::ACCEPTED_STATUSES[:not_authorized]
                end

                scenario 'the booking should not be confirmed' do
                  page.find('.cpy-submit-button').click
                  @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:waiting_for_payment]
                end

                scenario "I shoud be redirected to payment step with error message" do
                  page.find('.cpy-submit-button').click
                  should_be_on(public_booking_payments_path(booking_id: @booking.id))
                  page.should have_css('.alert-danger')
                end

                scenario 'I should receive an email about the booking payment failure' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  UserMailer.should_receive(:send_payment_failure_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end

              end

            end

            context 'registering a new credit card' do
              before(:each) do
                fill_in 'credit_card_billing_name', with: 'Name LastName'
                fill_in 'credit_card_owner_cpf', with: '471.352.521-95'
                fill_in 'credit_card_billing_address1', with: 'Rua das bananeiras, 123 - apt 11'
                fill_in 'credit_card_billing_zipcode', with: '123456-789'
                select 'Brasil', from: 'credit_card_billing_country'
                all('#credit_card_billing_state option')[0].select_option
                fill_in 'credit_card_billing_city', with: 'City'
                fill_in 'credit_card_billing_phone', with: '(11) 98762-5134'
                fill_in 'credit_card_temporary_number', with: '4444 4444 4444 4448'
                select '2024', from: 'credit_card_expiration_year'
                select 'Dez', from: 'credit_card_expiration_month'

              end

              context 'and successfull transaction happens' do
                before(:each) do
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <api-response>
                              <errorCode>0</errorCode>
                              <errorMessage></errorMessage>
                              <command>add-consumer</command>
                              <time>1367849110845</time>
                              <result>
                                <customerId>12590</customerId>
                                <token>5e3K+bwTdBg=</token>
                              </result>
                            </api-response>
                          }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::ApiRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <transaction-response>
                              <orderID>1289</orderID>
                              <responseCode>0</responseCode>
                            </transaction-response>
                           }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                end

                scenario "user should be registred in maxipago" do
                  page.find('.cpy-submit-button').click
                  @booking.user.reload.maxipago_token.should_not be_blank
                end

                scenario "credit card should be registred in maxipago" do
                  page.find('.cpy-submit-button').click
                  CreditCard.first.maxipago_token.should_not be_blank
                end

                scenario "the payment should be created" do
                  page.find('.cpy-submit-button').click
                  Payment.count.should == 1
                  Payment.first.status.should == Payment::ACCEPTED_STATUSES[:captured]
                end

                scenario 'the booking should be confirmed' do
                  page.find('.cpy-submit-button').click
                  @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
                end

                scenario "I shoud be redirected to final step" do
                  page.find('.cpy-submit-button').click
                  should_be_on(public_booking_payments_path(booking_id: @booking.id))
                end

                scenario 'I should receive an email about the booking confirmation' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  UserMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end

                scenario 'the hotel should receive an email about the booking confirmation' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  HotelMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end
              end

              context 'and unsuccessfull transaction happens' do
                before(:each) do
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <api-response>
                              <errorCode>0</errorCode>
                              <errorMessage></errorMessage>
                              <command>add-consumer</command>
                              <time>1367849110845</time>
                              <result>
                                <customerId>12590</customerId>
                                <token>5e3K+bwTdBg=</token>
                              </result>
                            </api-response>
                           }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::ApiRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <transaction-response>
                              <responseCode>1</responseCode>
                            </transaction-response>
                           }

                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                end

                scenario "the payment should be created" do
                  page.find('.cpy-submit-button').click
                  Payment.count.should == 1
                  Payment.first.status.should == Payment::ACCEPTED_STATUSES[:not_authorized]
                end

                scenario 'the booking should not be confirmed' do
                  page.find('.cpy-submit-button').click
                  @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:waiting_for_payment]
                end

                scenario "I shoud be redirected to payment step with error message" do
                  page.find('.cpy-submit-button').click
                  should_be_on(public_booking_payments_path(booking_id: @booking.id))
                  page.should have_css('.alert-danger')
                end

                scenario 'I should receive an email about the payment failure' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  UserMailer.should_receive(:send_payment_failure_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end
              end
            end
          end

          describe "with invalid data" do
            before(:each) do
              fill_in 'credit_card_billing_name', with: 'Name'
              fill_in 'credit_card_owner_cpf', with: '471.352.521-95'
              fill_in 'credit_card_billing_address1', with: 'Rua das bananeiras, 123 - apt 11'
              fill_in 'credit_card_billing_zipcode', with: '123456-789'
              select 'Brasil', from: 'credit_card_billing_country'
              all('#credit_card_billing_state option')[0].select_option
              fill_in 'credit_card_billing_city', with: 'City'
              fill_in 'credit_card_billing_phone', with: '(11) 98762-5134'
              fill_in 'credit_card_temporary_number', with: '1111 1111 1111 1111'
              page.find('.cpy-submit-button').click
            end

            scenario "I should see errors" do
              page.should have_css('.help-block.error')
            end

            scenario 'a payment should not be created' do
              Payment.count.should == 0
            end

            scenario 'the booking status should not change' do
              @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:waiting_for_payment]
            end
          end
        end

        context 'chosing to pay at the hotel' do
          before(:each) do
            choose("pay_at_the_hotel", option: 'true')
          end

          describe "with valid data" do
            context 'using an existing credit card' do
              before(:each) do
                @existing_credit_card = FactoryGirl.create(:credit_card, user: @booking.user)
                visit new_public_booking_payments_path(booking_id: @booking.id)
                choose("pay_at_the_hotel", option: 'true')
                all('#credit_card_id option')[1].select_option
              end

              context 'and successfull transaction happens' do
                before(:each) do
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <api-response>
                              <errorCode>0</errorCode>
                              <errorMessage></errorMessage>
                              <command>add-consumer</command>
                              <time>1367849110845</time>
                              <result>
                                <customerId>12590</customerId>
                                <token>5e3K+bwTdBg=</token>
                              </result>
                            </api-response>
                          }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::ApiRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <transaction-response>
                              <orderID>1289</orderID>
                              <responseCode>0</responseCode>
                            </transaction-response>
                           }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                end

                scenario "the payment should be created" do
                  page.find('.cpy-submit-button').click
                  Payment.count.should == 1
                  Payment.first.status.should == Payment::ACCEPTED_STATUSES[:authorized]
                end

                scenario 'the booking should be confirmed' do
                  page.find('.cpy-submit-button').click
                  @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed]
                end

                scenario "I shoud be redirected to final step" do
                  page.find('.cpy-submit-button').click
                  should_be_on(public_booking_payments_path(booking_id: @booking.id))
                end

                scenario 'I should receive an email about the booking confirmation' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  UserMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end

                scenario 'the hotel should receive an email about the booking confirmation' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  HotelMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end
              end

              context 'and unsuccessfull transaction happens' do
                before(:each) do
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <api-response>
                              <errorCode>0</errorCode>
                              <errorMessage></errorMessage>
                              <command>add-consumer</command>
                              <time>1367849110845</time>
                              <result>
                                <customerId>12590</customerId>
                                <token>5e3K+bwTdBg=</token>
                              </result>
                            </api-response>
                           }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::ApiRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <transaction-response>
                              <responseCode>1</responseCode>
                            </transaction-response>
                           }

                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                end

                scenario "the payment should be created" do
                  page.find('.cpy-submit-button').click
                  Payment.count.should == 1
                  Payment.first.status.should == Payment::ACCEPTED_STATUSES[:not_authorized]
                end

                scenario 'the booking should not be confirmed' do
                  page.find('.cpy-submit-button').click
                  @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:waiting_for_payment]
                end

                scenario "I shoud be redirected to payment step with error message" do
                  page.find('.cpy-submit-button').click
                  should_be_on(public_booking_payments_path(booking_id: @booking.id))
                  page.should have_css('.alert-danger')
                end

                scenario 'I should receive an email about the payment failure' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  UserMailer.should_receive(:send_payment_failure_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end

              end

            end

            context 'registering a new credit card' do
              before(:each) do
                fill_in 'credit_card_billing_name', with: 'Name LastName'
                fill_in 'credit_card_owner_cpf', with: '471.352.521-95'
                fill_in 'credit_card_billing_address1', with: 'Rua das bananeiras, 123 - apt 11'
                fill_in 'credit_card_billing_zipcode', with: '123456-789'
                select 'Brasil', from: 'credit_card_billing_country'
                all('#credit_card_billing_state option')[0].select_option
                fill_in 'credit_card_billing_city', with: 'City'
                fill_in 'credit_card_billing_phone', with: '(11) 98762-5134'
                fill_in 'credit_card_temporary_number', with: '4444 4444 4444 4448'
                select '2024', from: 'credit_card_expiration_year'
                select 'Dez', from: 'credit_card_expiration_month'
              end

              context 'and successfull transaction happens' do
                before(:each) do
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <api-response>
                              <errorCode>0</errorCode>
                              <errorMessage></errorMessage>
                              <command>add-consumer</command>
                              <time>1367849110845</time>
                              <result>
                                <customerId>12590</customerId>
                                <token>5e3K+bwTdBg=</token>
                              </result>
                            </api-response>
                          }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::ApiRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <transaction-response>
                              <orderID>1289</orderID>
                              <responseCode>0</responseCode>
                            </transaction-response>
                           }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                end

                scenario "user should be registred in maxipago" do
                  page.find('.cpy-submit-button').click
                  @booking.user.reload.maxipago_token.should_not be_blank
                end

                scenario "credit card should be registred in maxipago" do
                  page.find('.cpy-submit-button').click
                  CreditCard.first.maxipago_token.should_not be_blank
                end

                scenario "the payment should be created" do
                  page.find('.cpy-submit-button').click
                  Payment.count.should == 1
                  Payment.first.status.should == Payment::ACCEPTED_STATUSES[:authorized]
                end

                scenario 'the booking should be confirmed' do
                  page.find('.cpy-submit-button').click
                  @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed]
                end

                scenario "I shoud be redirected to final step" do
                  page.find('.cpy-submit-button').click
                  should_be_on(public_booking_payments_path(booking_id: @booking.id))
                end

                scenario 'I should receive an email about the booking confirmation' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  UserMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end

                scenario 'the hotel should receive an email about the booking confirmation' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  HotelMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end
              end

              context 'and unsuccessfull transaction happens' do
                before(:each) do
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <api-response>
                              <errorCode>0</errorCode>
                              <errorMessage></errorMessage>
                              <command>add-consumer</command>
                              <time>1367849110845</time>
                              <result>
                                <customerId>12590</customerId>
                                <token>5e3K+bwTdBg=</token>
                              </result>
                            </api-response>
                           }
                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::ApiRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                  body = %Q{
                            <?xml version="1.0" encoding="UTF-8"?>
                            <transaction-response>
                              <responseCode>1</responseCode>
                            </transaction-response>
                           }

                  FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                        body: body,
                                        status: ["200", "OK"] )
                end

                scenario "the payment should be created" do
                  page.find('.cpy-submit-button').click
                  Payment.count.should == 1
                  Payment.first.status.should == Payment::ACCEPTED_STATUSES[:not_authorized]
                end

                scenario 'the booking should not be confirmed' do
                  page.find('.cpy-submit-button').click
                  @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:waiting_for_payment]
                end

                scenario "I shoud be redirected to payment step with error message" do
                  page.find('.cpy-submit-button').click
                  should_be_on(public_booking_payments_path(booking_id: @booking.id))
                  page.should have_css('.alert-danger')
                end

                scenario 'I should receive an email about the payment failure' do
                  mailer_mock = double
                  mailer_mock.should_receive(:deliver)
                  UserMailer.should_receive(:send_payment_failure_notice).and_return(mailer_mock)
                  page.find('.cpy-submit-button').click
                end
              end
            end
          end

          describe "with invalid data" do
            before(:each) do
              fill_in 'credit_card_billing_name', with: 'Name'
              fill_in 'credit_card_owner_cpf', with: '471.352.521-95'
              fill_in 'credit_card_billing_address1', with: 'Rua das bananeiras, 123 - apt 11'
              fill_in 'credit_card_billing_zipcode', with: '123456-789'
              select 'Brasil', from: 'credit_card_billing_country'
              all('#credit_card_billing_state option')[0].select_option
              fill_in 'credit_card_billing_city', with: 'City'
              fill_in 'credit_card_billing_phone', with: '(11) 98762-5134'
              fill_in 'credit_card_temporary_number', with: '1111 1111 1111 1111'
              page.find('.cpy-submit-button').click
            end

            scenario "I should see errors" do
              page.should have_css('.help-block.error')
            end

            scenario 'a payment should not be created' do
              Payment.count.should == 0
            end

            scenario 'the booking status should not change' do
              @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:waiting_for_payment]
            end
          end
        end

      end
    end # of maxipago enabled

    context 'MaxiPago disabled' do
      before(:each) do
        stub_const("Public::Bookings::PaymentsController::SENDING_CREDIT_CARD_TO_MANUAL_PAYMENT", true)
        room = FactoryGirl.create(:room)

        insert_query = %Q{
                          INSERT INTO offers
                              (checkin_timestamp, checkout_timestamp,\
                               extra_price_per_person, hotel_id, log_id, no_show_value,\
                               pack_in_hours, price, room_id, room_type_id, room_type_initial_capacity,\
                               room_type_maximum_capacity, status)
                            VALUES
                              ('#{(DateTime.now.beginning_of_day + 1.day).utc.strftime("%Y-%m-%d %H:%M:%S")}', '#{(DateTime.now.beginning_of_day + 1.day + 3.hours).utc.strftime("%Y-%m-%d %H:%M:%S")}',\
                               '1','#{room.hotel.id}', '1', '1',\
                               '3', '1', '#{room.id}', '#{room.room_type.id}', '1',\
                               '1', '#{ Offer::ACCEPTED_STATUS[:available] }' )
                          }
        ActiveRecord::Base.connection.execute(insert_query)
        FactoryGirl.create(:country)
        @booking = FactoryGirl.create(:booking, offer: Offer.first)
        reset_session!
        sign_in_user(@booking.user)
        visit new_public_booking_payments_path(booking_id: @booking.id)
      end

      scenario "I should see the price description" do
        page.should have_content(number_to_currency @booking.initial_price_without_iss)
        page.should have_content(number_to_currency @booking.extra_price_per_person_without_iss)
        page.should have_content("#{@booking.hotel.iss_in_percentage.to_s.gsub('.',',')} %")
        page.should have_content(number_to_currency @booking.total_price)
        visit new_public_booking_payments_path(booking_id: @booking.id)
        page.should have_content(number_to_currency @booking.booking_tax)
      end

      scenario 'I should not have the option to select an existing credit card' do
        FactoryGirl.create(:credit_card, user: @booking.user)
        visit new_public_booking_payments_path(booking_id: @booking.id)
        page.should_not have_css('.existing-credit-card-select')
      end

      scenario 'I should not have the option to pay online' do
        visit new_public_booking_payments_path(booking_id: @booking.id)
        page.should_not have_css('.cpy-pay-online-radio-button')
      end

      context "I can fill the owner name, CPF, credit card number, CVV, month and year" do

        describe "with valid data" do

          before(:each) do
            fill_in 'credit_card_billing_name', with: 'Name LastName'
            fill_in 'credit_card_owner_cpf', with: '471.352.521-95'
            fill_in 'credit_card_billing_address1', with: 'Rua das bananeiras, 123 - apt 11'
            fill_in 'credit_card_billing_zipcode', with: '123456-789'
            select 'Brasil', from: 'credit_card_billing_country'
            all('#credit_card_billing_state option')[0].select_option
            fill_in 'credit_card_billing_city', with: 'City'
            fill_in 'credit_card_billing_phone', with: '(11) 98762-5134'
            fill_in 'credit_card_temporary_number', with: '4444 4444 4444 4448'
            select '2024', from: 'credit_card_expiration_year'
            select 'Dez', from: 'credit_card_expiration_month'
          end

          scenario 'admin should receive an email with my credit card data' do
            AdminMailer.any_instance.should_receive(:send_credit_card_informations)
            page.find('.cpy-submit-button').click
          end

          scenario 'my booking should be confirmed' do
            page.find('.cpy-submit-button').click
            @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed]
          end

          scenario 'I should be redirected to the final step' do
            page.find('.cpy-submit-button').click
            should_be_on(public_booking_payments_path(booking_id: @booking.id))
          end

          scenario 'I should receive an email about the booking confirmation' do
            mailer_mock = double
            mailer_mock.should_receive(:deliver)
            UserMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
            page.find('.cpy-submit-button').click
          end

          scenario 'the hotel should receive an email about the booking confirmation' do
            mailer_mock = double
            mailer_mock.should_receive(:deliver)
            HotelMailer.should_receive(:send_booking_confirmation_notice).and_return(mailer_mock)
            page.find('.cpy-submit-button').click
          end

        end

        describe "with invalid data" do
          before(:each) do
            fill_in 'credit_card_billing_name', with: 'Name'
            fill_in 'credit_card_owner_cpf', with: '471.352.521-95'
            fill_in 'credit_card_billing_address1', with: 'Rua das bananeiras, 123 - apt 11'
            fill_in 'credit_card_billing_zipcode', with: '123456-789'
            select 'Brasil', from: 'credit_card_billing_country'
            all('#credit_card_billing_state option')[0].select_option
            fill_in 'credit_card_billing_city', with: 'City'
            fill_in 'credit_card_billing_phone', with: '(11) 98762-5134'
            fill_in 'credit_card_temporary_number', with: '1111 1111 1111 1111'
            page.find('.cpy-submit-button').click
          end

          scenario "I should see errors" do
            page.should have_css('.help-block.error')
          end

          scenario 'admin should not receive an email about my payment' do
            AdminMailer.any_instance.should_not_receive(:send_credit_card_informations)
            page.find('.cpy-submit-button').click
          end

          scenario 'the booking status should not change' do
            @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:waiting_for_payment]
          end
        end

      end
    end # of maxipago disabled

  end # of visiting payment step

end
