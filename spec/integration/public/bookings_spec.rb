# encoding: utf-8
require 'spec_helper'
require 'ostruct'
include ActionView::Helpers::NumberHelper

story 'As an user I can manage my bookings' do
  before(:each) do
    @user = FactoryGirl.create(:user)
    sign_in_user(@user)
  end

  context 'visiting the booking listing' do
    scenario 'I should see my bookings' do
      bookings = []
      5.times do
        bookings << FactoryGirl.create(:booking, user: @user)
      end
      visit public_bookings_path
      5.times do | i |
        page.should have_content(bookings[i].hotel.name)
      end
    end

    scenario 'I should not see other users bookings' do
      user_booking = FactoryGirl.create(:booking, user: @user)
      other_user_booking = FactoryGirl.create(:booking)
      visit public_bookings_path
      page.should have_content(user_booking.hotel.name)
      page.should_not have_content(other_user_booking.hotel.name)
    end

    scenario 'I can filter bookings', js: true do
      booking_1 = FactoryGirl.create(:booking, user: @user)
      booking_2 = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed])
      visit public_bookings_path
      page.execute_script("$('#by_status').val('#{booking_1.status}')")
      page.find('.cpy-filter-button').click
      page.should have_content(booking_1.hotel.name)
      page.should_not have_content(booking_2.hotel.name)
      page.execute_script("$('#by_status').val('#{booking_2.status}')")
      page.find('.cpy-filter-button').click
      page.should have_content(booking_2.hotel.name)
      page.should_not have_content(booking_1.hotel.name)
      page.find('.cpy-remove-filter-button').click
      page.should have_content(booking_1.hotel.name)
      page.should have_content(booking_2.hotel.name)
    end

    scenario 'I can cancel bookings that are waiting for payment' do
      offer = FactoryGirl.create(:offer)
      booking = FactoryGirl.create(:booking, user: @user, offer: offer)
      visit public_bookings_path
      page.find('.cpy-cancel-button').click
      page.should have_content('A reserva foi cancelada com sucesso.')
      booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
    end

    context 'maxipago was enabled at booking creation time' do
      context 'succesfull transaction' do
        before(:each) do
          maxipago_stub_response = OpenStruct.new
          maxipago_stub_response.body = 'body'
          maxipago_stub_response.transaction_canceled = true
          MaxiPagoInterface.stub(:cancel_transaction).and_return(maxipago_stub_response)
        end

        scenario 'I can cancel confirmed bookings' do
          offer = FactoryGirl.create(:offer)
          booking = FactoryGirl.create(:booking, offer: offer, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed])
          MaxiPagoInterface.should_receive(:cancel_transaction)
          FactoryGirl.create(:payment, booking: booking).update(authorized_at: Time.now)
          visit public_bookings_path
          page.find('.cpy-cancel-button').click
          page.should have_content('A reserva foi cancelada com sucesso.')
          booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
        end

        scenario 'I can cancel confirmed and paid bookings' do
          offer = FactoryGirl.create(:offer)
          booking = FactoryGirl.create(:booking, offer: offer, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
          MaxiPagoInterface.should_receive(:cancel_transaction)
          FactoryGirl.create(:payment, booking: booking).update(authorized_at: Time.now)
          visit public_bookings_path
          page.find('.cpy-cancel-button').click
          page.should have_content('A reserva foi cancelada com sucesso.')
          booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
        end
      end

      context 'transaction error' do
        before(:each) do
          maxipago_stub_response = OpenStruct.new
          maxipago_stub_response.body = 'body'
          maxipago_stub_response.transaction_canceled = false
          MaxiPagoInterface.should_receive(:cancel_transaction).and_return(maxipago_stub_response)
          @offer = FactoryGirl.create(:offer)
          @booking = FactoryGirl.create(:booking, offer: @offer, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
          @payment = FactoryGirl.create(:payment, booking: @booking,status: Payment::ACCEPTED_STATUSES[:captured])
          @payment.update!(authorized_at: Time.now)
        end

        scenario 'the database should be unchanged' do
          visit public_bookings_path
          page.find('.cpy-cancel-button').click
          @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
          @payment.reload.status.should == Payment::ACCEPTED_STATUSES[:captured]
          @payment.canceled_at.should be_nil
        end

        scenario 'I should see an alert' do
          visit public_bookings_path
          page.find('.cpy-cancel-button').click
          page.should_not have_content('A reserva foi cancelada com sucesso.')
          page.should have_css('.alert-danger')
        end
      end
    end

    context 'maxipago was disabled at booking creation time' do
      before(:each) do
        stub_const("MaxiPagoInterface", double())
      end

      scenario 'I can cancel confirmed bookings' do
        booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed])
        mailer_mock = double
        mailer_mock.should_receive(:deliver)
        AdminMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
        visit public_bookings_path
        page.find('.cpy-cancel-button').click
        page.should have_content('A reserva foi cancelada com sucesso.')
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end

      scenario 'I can cancel bookings that are waiting for payment' do
        booking = FactoryGirl.create(:booking, user: @user)
        visit public_bookings_path
        page.find('.cpy-cancel-button').click
        page.should have_content('A reserva foi cancelada com sucesso.')
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end
    end

    scenario 'I should not see the button to cancel booking for canceled bookings' do
      booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:canceled])
      visit public_bookings_path
      page.all('.cpy-cancel-button').count.should == 0
    end

    scenario 'I should not see the button to cancel booking for expired bookings' do
      booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:expired])
      visit public_bookings_path
      page.all('.cpy-cancel-button').count.should == 0
    end

    scenario 'I can confirm a booking using my credit card' do
      booking = FactoryGirl.create(:booking, user: @user)
      visit public_bookings_path
      first('.cpy-confirm-booking-button').click
      should_be_on(new_public_booking_payments_path(booking_id: booking.id))
    end

    scenario 'I cannot confirm a booking if it is outside my budget' do
      booking = FactoryGirl.create(:booking, user: @user)
      visit public_bookings_path
      @user.update(monthly_budget: -1, company: FactoryGirl.create(:company))
      first('.cpy-confirm-booking-button').click
      should_be_on(public_root_path)
      page.should have_content I18n.t(:message_booking_is_outside_budget_error)
    end

    scenario 'I should not see the button to confirm the booking if it is outside my budget' do
      booking = FactoryGirl.create(:booking, user: @user)
      @user.update(monthly_budget: -1, company: FactoryGirl.create(:company))
      visit public_bookings_path
      page.should_not have_css('.cpy-confirm-booking-button')
    end

  end

  context 'visiting the booking details page' do

    scenario 'I should see the details of a booking', js: true do
      booking = FactoryGirl.create(:booking, user: @user)
      visit public_booking_path(booking.id)
      page.should have_content(booking.guest_name)
      page.should have_content(booking.number_of_people)
      page.should have_content(booking.room_type.name_pt_br)
      page.should have_content("#{booking.pack_in_hours}h")
      page.should have_content(booking.hotel.name)
      page.should have_content(booking.offer.checkin_timestamp.strftime("%d/%m/%Y"))
      page.should have_content(booking.offer.checkin_timestamp.strftime("%H:%M"))
      page.should have_content(booking.offer.checkout_timestamp.strftime("%d/%m/%Y"))
      page.should have_content(booking.offer.checkout_timestamp.strftime("%H:%M"))
      page.should have_content(booking.note)
      page.should have_content(I18n.t "bookings.#{Booking::ACCEPTED_STATUS_REVERSED[booking.status]}")
      page.should have_content(I18n.l booking.created_at)
      page.should have_content(number_to_currency(booking.offer.price))
      page.should have_content(number_to_currency(booking.offer.extra_price_per_person * (booking.number_of_people - 1)))
      page.should have_content(number_to_currency((booking.offer.extra_price_per_person * (booking.number_of_people - 1)) + booking.offer.price))
    end

    scenario 'I can cancel a booking that is waiting for payment' do
      booking = FactoryGirl.create(:booking, user: @user)
      visit public_booking_path(booking.id)
      page.find('.cpy-cancel-button').click
      page.should have_content('A reserva foi cancelada com sucesso.')
      booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
    end

    context 'maxipago was enabled at booking creation time' do
      context 'succesfull transaction' do
        before(:each) do
          maxipago_stub_response = OpenStruct.new
          maxipago_stub_response.body = 'body'
          maxipago_stub_response.transaction_canceled = true
          MaxiPagoInterface.stub(:cancel_transaction).and_return(maxipago_stub_response)
        end

        scenario 'I can cancel a confirmed booking' do
          booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed])
          FactoryGirl.create(:payment, booking: booking).update(authorized_at: Time.now)
          visit public_booking_path(booking.id)
          page.find('.cpy-cancel-button').click
          page.should have_content('A reserva foi cancelada com sucesso.')
          booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
        end

        scenario 'I can cancel a confirmed and paid booking' do
          booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed])
          FactoryGirl.create(:payment, booking: booking).update(authorized_at: Time.now)
          MaxiPagoInterface.should_receive(:cancel_transaction)
          visit public_booking_path(booking.id)
          page.find('.cpy-cancel-button').click
          page.should have_content('A reserva foi cancelada com sucesso.')
          booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
        end
      end

      context 'transaction error' do
        before(:each) do
          maxipago_stub_response = OpenStruct.new
          maxipago_stub_response.body = 'body'
          maxipago_stub_response.transaction_canceled = false
          MaxiPagoInterface.should_receive(:cancel_transaction).and_return(maxipago_stub_response)
          @offer = FactoryGirl.create(:offer)
          @booking = FactoryGirl.create(:booking, offer: @offer, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
          @payment = FactoryGirl.create(:payment, booking: @booking,status: Payment::ACCEPTED_STATUSES[:captured])
          @payment.update!(authorized_at: Time.now)
        end

        scenario 'the database should be unchanged' do
          visit public_bookings_path
          page.find('.cpy-cancel-button').click
          @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
          @payment.reload.status.should == Payment::ACCEPTED_STATUSES[:captured]
          @payment.canceled_at.should be_nil
        end

        scenario 'I should see an alert' do
          visit public_bookings_path
          page.find('.cpy-cancel-button').click
          page.should_not have_content('A reserva foi cancelada com sucesso.')
          page.should have_css('.alert-danger')
        end
      end
    end

    context 'maxipago was disabled at booking creation time' do
      before(:each) do
        stub_const("MaxiPagoInterface", double())
      end

      scenario 'I can cancel a confirmed booking' do
        booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed])
        mailer_mock = double()
        mailer_mock.should_receive(:deliver)
        AdminMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
        visit public_booking_path(booking.id)
        page.find('.cpy-cancel-button').click
        page.should have_content('A reserva foi cancelada com sucesso.')
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end

      scenario 'I can cancel a confirmed and paid booking' do
        mailer_mock = double()
        mailer_mock.should_receive(:deliver)
        AdminMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
        booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
        visit public_booking_path(booking.id)
        page.find('.cpy-cancel-button').click
        page.should have_content('A reserva foi cancelada com sucesso.')
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end

      scenario 'I can cancel a booking that is waiting for payment' do
        booking = FactoryGirl.create(:booking, user: @user)
        visit public_booking_path(booking.id)
        page.find('.cpy-cancel-button').click
        page.should have_content('A reserva foi cancelada com sucesso.')
        booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end
    end

    scenario 'I should not see the button to cancel a booking for a canceled booking' do
      booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:canceled])
      visit public_booking_path(booking.id)
      page.all('.cpy-cancel-button').count.should == 0
    end

    scenario 'I should not see the button to cancel a booking for a expired booking' do
      booking = FactoryGirl.create(:booking, user: @user, status: Booking::ACCEPTED_STATUSES[:expired])
      visit public_booking_path(booking.id)
      page.all('.cpy-cancel-button').count.should == 0
    end

    scenario 'I can confirm a booking using my credit card' do
      booking = FactoryGirl.create(:booking, user: @user)
      visit public_booking_path(booking.id)
      page.find('.cpy-confirm-booking-button').click
      should_be_on(new_public_booking_payments_path(booking_id: booking.id))
    end

  end

  context 'editing a booking' do
    scenario 'with a valid guest name' do
      user_booking = FactoryGirl.create(:booking, user: @user)
      visit edit_public_booking_path(user_booking.id)
      fill_in 'booking[guest_name]', with: 'Novo guest name'
      find('.cpy-submit-button').click
      should_be_on public_booking_path(user_booking.id)
      within('.booking_guest_name'){ page.should have_content('Novo guest name') }
    end

    scenario 'with an invalid guest name' do
      user_booking = FactoryGirl.create(:booking, user: @user)
      visit edit_public_booking_path(user_booking.id)
      fill_in 'booking[guest_name]', with: ''
      find('.cpy-submit-button').click
      should_be_on public_booking_path(user_booking.id)
      within('span.error'){ page.should have_content('não pode ficar em branco') }
    end

    scenario 'with a different comment' do
      user_booking = FactoryGirl.create(:booking, user: @user, number_of_people: 3)
      visit edit_public_booking_path(user_booking.id)
      fill_in 'booking[note]', with: "Novo comentário"
      find('.cpy-submit-button').click
      should_be_on public_booking_path(user_booking.id)
      within('.attributes-wrapper.booking_note'){ page.should have_content("Novo comentário") }
    end

  end

end
