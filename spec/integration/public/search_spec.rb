# encoding: utf-8
require 'spec_helper'
include ActionView::Helpers::NumberHelper
# sem js
story 'As an user I can search hotels' do

  context 'if there are matches to my search' do
    before(:each) do
      @hotel_1 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
      @hotel_2 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
      @hotel_3 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
      @hotel_4 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
      offer_with_higher_price = FactoryGirl.create(:offer, hotel: @hotel_1)
      @offer_with_normal_price = FactoryGirl.create(:offer, price: '50', hotel: @hotel_1, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
      @offer_that_doesnt_match_filter = FactoryGirl.create(:offer, price: '10', hotel: @hotel_1, checkin_timestamp: (offer_with_higher_price.checkin_timestamp + 1.day))
      @offer_that_doesnt_match_filter_2 = FactoryGirl.create(:offer, checkin_timestamp: (offer_with_higher_price.checkin_timestamp + 4.hours))
      offer_with_higher_price_2 = FactoryGirl.create(:offer, hotel: @hotel_2, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
      @offer_with_normal_price_2 = FactoryGirl.create(:offer, price: '70', hotel: @hotel_2, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
      @similar_offer1 = FactoryGirl.create(:offer, pack_in_hours: 6, price: '70', hotel: @hotel_4, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
      @similar_offer2 = FactoryGirl.create(:offer, pack_in_hours: 6, price: '70', hotel: @hotel_2, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
      visit public_root_path
      address_coordinates_to_search = [-23.5651727, -46.65228159999998]
      find(:css, '.cpy-latitude-field', visible: false).set(address_coordinates_to_search[0])
      find(:css, '.cpy-longitude-field', visible: false).set(address_coordinates_to_search[1])
      find(:css, '.cpy-city-name', visible: false).set('tanganmandapio')
      find(:css, '.cpy-state-name', visible: false).set('SP')
      find(:css, '.cpy-address', visible: false).set('rua das bananas')
      find(:css, '.cpy-pack-length', visible: false).set(3)
      find(:css, '.cpy-checkin-date', visible: false).set(@offer_with_normal_price.checkin_timestamp.strftime("%d/%m/%Y"))
      select "#{@offer_with_normal_price.checkin_timestamp.strftime("%k:00").gsub(' ','')}", from: 'by_checkin_hour'

      page.find('.cpy-search-button').click
    end

    scenario 'I can see the hotels that have offers that match what I am looking for' do
      page.should have_content(@hotel_1.name)
      page.should have_content(@hotel_2.name)
      page.should have_no_content(@hotel_3.name)
      page.should have_content(@hotel_4.name)
      page.should have_content("#{number_to_currency(@offer_with_normal_price.price)}")
      page.should have_content("#{number_to_currency(@offer_with_normal_price_2.price)}")
      page.should_not have_content("#{number_to_currency(@offer_that_doesnt_match_filter.price)}")
      page.should_not have_content("#{number_to_currency(@offer_that_doesnt_match_filter_2.price)}")

      page.should have_content("0,39 KM")
    end

    scenario "The hotel with exact offers cannot appear in similar match" do
      within(".cpy-exact-matches-list") do
        page.should have_content(@hotel_2.name)
        page.should have_no_content(@hotel_4.name)
      end
      within(".cpy-similar-matches-list") do
        page.should have_no_content(@hotel_2.name)
        page.should have_content(@hotel_4.name)
      end
    end

    scenario "My search must be logged and save all the search data" do
      log = HotelSearchLog.last
      log.latitude.should == -23.5651727
      log.longitude.should == -46.6522816
      log.checkin_date.should == @offer_with_normal_price.checkin_timestamp.strftime("%d/%m/%Y")
      log.checkin_hour.should == @offer_with_normal_price.checkin_timestamp.strftime("%k").to_i
      log.number_of_people.should == 1
      log.length_of_pack.should == 3
      log.city_name.should == 'tanganmandapio'
      log.state_name.should == 'SP'
      log.exact_match_hotels_ids.should == [@hotel_1.id, @hotel_2.id]
      log.similar_match_hotels_ids.should == [@hotel_4.id]
    end
  end

  scenario "The search must be saved and apear in the search modal", pending:true, js: true do
    @hotel_1 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @hotel_2 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @hotel_3 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    @hotel_4 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    offer_with_higher_price = FactoryGirl.create(:offer, hotel: @hotel_1)
    @offer_with_normal_price = FactoryGirl.create(:offer, price: '50', hotel: @hotel_1, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
    @offer_that_doesnt_match_filter = FactoryGirl.create(:offer, price: '10', hotel: @hotel_1, checkin_timestamp: (offer_with_higher_price.checkin_timestamp + 1.day))
    @offer_that_doesnt_match_filter_2 = FactoryGirl.create(:offer, checkin_timestamp: (offer_with_higher_price.checkin_timestamp + 4.hours))
    offer_with_higher_price_2 = FactoryGirl.create(:offer, hotel: @hotel_2, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
    @offer_with_normal_price_2 = FactoryGirl.create(:offer, price: '70', hotel: @hotel_2, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
    @similar_offer1 = FactoryGirl.create(:offer, pack_in_hours: 6, price: '70', hotel: @hotel_4, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
    @similar_offer2 = FactoryGirl.create(:offer, pack_in_hours: 6, price: '70', hotel: @hotel_2, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
    visit public_root_path
    address_coordinates_to_search = [-23.5651727, -46.65228159999998]
    find(:css, '.cpy-latitude-field', visible: false).set(address_coordinates_to_search[0])
    find(:css, '.cpy-longitude-field', visible: false).set(address_coordinates_to_search[1])
    find(:css, '.cpy-city-name', visible: false).set('tanganmandapio')
    find(:css, '.cpy-state-name', visible: false).set('SP')
    find(:css, '.cpy-address', visible: false).set('rua das bananas')
    find(:css, '.cpy-pack-length', visible: false).set(3)
    find(:css, '.cpy-checkin-date', visible: false).set(@offer_with_normal_price.checkin_timestamp.strftime("%d/%m/%Y"))
    select "#{@offer_with_normal_price.checkin_timestamp.strftime("%k:00").gsub(' ','')}", from: 'by_checkin_hour'
    page.execute_script "$('.cpy-search-button').removeClass('ng-hide')"
    page.execute_script "$('.cpy-search-button').removeAttr('disabled')"
    page.execute_script "$('.cpy-search-button').prop('disabled', false)"
    page.find(:css, '.cpy-search-button').click
    click_link('Buscar hotéis')
    wait_for_element_to_be_visible("#search-box-modal", 5)
    within("#search-box-modal") do
      page.find('.cpy-search-button').click
    end
    page.should have_content(@hotel_1.name)
    page.should have_content("#{number_to_currency(@offer_with_normal_price.price)}")
    page.should have_content("#{number_to_currency(@offer_with_normal_price_2.price)}")
    page.should_not have_content("#{number_to_currency(@offer_that_doesnt_match_filter.price)}")
    page.should_not have_content("#{number_to_currency(@offer_that_doesnt_match_filter_2.price)}")

    page.should have_content("0,39 KM")
  end

  context 'if there are no matches to my search', js: true do
    before(:each) do
      hotel_1 = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
      offer_with_normal_price = FactoryGirl.create(:offer, price: '50', hotel: hotel_1)
      visit public_root_path
      address_coordinates_to_search = [23.5651727, 46.65228159999998]
      find(:css, '.cpy-latitude-field', visible: false).set(address_coordinates_to_search[0])
      find(:css, '.cpy-longitude-field', visible: false).set(address_coordinates_to_search[1])
      find(:css, '.cpy-city-name', visible: false).set('tanganmandapio')
      find(:css, '.cpy-state-name', visible: false).set('SP')
      find(:css, '.cpy-address', visible: false).set('rua das bananas')
      find(:css, '.cpy-pack-length', visible: false).set(3)
      select "#{offer_with_normal_price.checkin_timestamp.strftime("%k:00").gsub(' ','')}", from: 'by_checkin_hour'
      find(:css, '.cpy-checkin-date', visible: false).set(offer_with_normal_price.checkin_timestamp.strftime("%d/%m/%Y"))
      find(:css, '.cpy-checkin-date-calendar-input').set(offer_with_normal_price.checkin_timestamp.strftime("%d/%m/%Y"))


      page.execute_script "$('.cpy-search-button').removeClass('ng-hide')"
      page.execute_script "$('.cpy-search-button').removeAttr('disabled')"
      page.execute_script "$('.cpy-search-button').prop('disabled', false)"
      select "#{offer_with_normal_price.checkin_timestamp.strftime("%k:00").gsub(' ','')}", from: 'by_checkin_hour'
      page.find('.cpy-search-button').click
    end

    scenario 'I should see an no-match error page' do
      within('.all-matches-hotels') do
        page.should have_content('Desculpe. Não encontramos nenhum resultado para a sua busca.')
        page.should have_content('O número de hotéis cresce a cada dia!')
      end
    end

    context 'I can register my email atached to the search log' do
      scenario 'I can see a success message' do
        fill_in 'subscribe_search[email]', with: 'vinicius.teste@email.com'
        page.find('.cpy-subscribe-button').click
        page.should have_content("Inscrição efetuada com sucesso. Assim que encontrarmos um hotel nós avisaremos você.")
      end

      scenario 'it shows the errors' do
        page.find('.cpy-subscribe-button').click
        page.should have_content('Email não pode ficar em branco')
      end

      context 'no previous email' do
        scenario 'My email is registred and the search log is atached to the user registred' do
          fill_in 'subscribe_search[email]', with: 'vinicius.teste@email.com'
          page.find('.cpy-subscribe-button').click
          wait_for_ajax_end
          created_user = SearchLogsUser.where(email: 'vinicius.teste@email.com').first
          created_user.email.should == 'vinicius.teste@email.com'
          HotelSearchLog.last.search_logs_user_id.should == created_user.id
        end
      end

      context 'with previous email' do
        scenario 'The existing email is used and the search log is atached to the user registred' do
          user1 = FactoryGirl.create(:search_logs_user)
          user2 = FactoryGirl.create(:search_logs_user)
          SearchLogsUser.count.should == 2
          fill_in 'subscribe_search[email]', with: user2.email
          page.find('.cpy-subscribe-button').click
          wait_for_ajax_end
          HotelSearchLog.last.search_logs_user_id.should == user2.id
          SearchLogsUser.count.should == 2
        end
      end

    end
  end

  scenario 'when I search for something, a log should be saved to the database' do
    hotel = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
    visit public_root_path
    address_coordinates_to_search = [-23.5651727, -46.65228159999998]
    find(:css, '.cpy-latitude-field', visible: false).set(address_coordinates_to_search[0])
    find(:css, '.cpy-longitude-field', visible: false).set(address_coordinates_to_search[1])
    find(:css, '.cpy-city-name', visible: false).set('tanganmandapio')
    find(:css, '.cpy-state-name', visible: false).set('SP')
    find(:css, '.cpy-address', visible: false).set('rua das bananas')
    find(:css, '.cpy-pack-length', visible: false).set(6)
    find('.cpy-checkin-date', visible: false).set "01/01/2020"
    select '9:00', from: 'by_checkin_hour'
    select '4', from: 'by_number_of_people'
    page.find('.cpy-search-button').click
    HotelSearchLog.count.should == 1
    log = HotelSearchLog.first
    log.latitude.should >= (address_coordinates_to_search[0] - 0.00001)
    log.longitude.should <= (address_coordinates_to_search[0] - 0.00001)
    log.latitude.should >= (address_coordinates_to_search[1] + 0.00001)
    log.longitude.should <= (address_coordinates_to_search[1] + 0.00001)
    log.checkin_date.should == '01/01/2020'
    log.checkin_hour.should == 9
    log.number_of_people.should == 4
    log.length_of_pack.should == 6
  end

  scenario 'the submit button should be disabled if I left the checkin date blank or in the past and the address blank', js: true do
    visit public_root_path
    expect { page.find('.cpy-search-button') }.to raise_error
    page.find('.cpy-disabled-search-button').should be_visible
    page.find('.cpy-disabled-search-button')['disabled'].should == 'disabled'
    find('.cpy-checkin-date', visible: false).set "#{(DateTime.now - 1.day).strftime("%d/%m/%Y")}"
    expect { page.find('.cpy-search-button') }.to raise_error
  end


  context 'when I click to see the details of an hotel' do

    scenario 'I should see the offer price that I saw on the search page' do
      hotel = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244")
      offer_with_higher_price = FactoryGirl.create(:offer, hotel: hotel)
      offer_with_normal_price = FactoryGirl.create(:offer, price: '50', hotel: hotel, checkin_timestamp: offer_with_higher_price.checkin_timestamp)
      offer_with_higher_price_with_same_room_type = FactoryGirl.create(:offer, price: '520', hotel: hotel, checkin_timestamp: offer_with_higher_price.checkin_timestamp, room_type: offer_with_normal_price.room_type)
      visit public_root_path
      address_coordinates_to_search = [-23.5651727, -46.65228159999998]
      find(:css, '.cpy-latitude-field', visible: false).set(address_coordinates_to_search[0])
      find(:css, '.cpy-longitude-field', visible: false).set(address_coordinates_to_search[1])
      find(:css, '.cpy-city-name', visible: false).set('tangamandapio')
      find(:css, '.cpy-state-name', visible: false).set('SP')
      find(:css, '.cpy-address', visible: false).set('rua das bananas')
      find(:css, '.cpy-pack-length', visible: false).set(3)
      find('.cpy-checkin-date', visible: false).set offer_with_normal_price.checkin_timestamp.strftime("%d/%m/%Y")
      select "#{offer_with_normal_price.checkin_timestamp.strftime("%k:00").gsub(' ','')}", from: 'by_checkin_hour'
      page.find('.cpy-search-button').click
      page.find('.cpy-hotel-details-link').click

      page.should have_content("#{number_to_currency(offer_with_normal_price.price)}")
      page.should have_content("#{number_to_currency(offer_with_higher_price.price)}")
      page.should_not have_content("#{number_to_currency(offer_with_higher_price_with_same_room_type.price)}")
      page.should have_content("#{offer_with_normal_price.room_type.name_pt_br}")
      page.should have_content("#{offer_with_higher_price.room_type.name_pt_br}")
    end

    scenario 'I should see the hotel minimum hours of notice, if the hotel accept booking cancellation' do
      hotel = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244", minimum_hours_of_notice: 24)
      offer = FactoryGirl.create(:offer, hotel: hotel)

      visit public_root_path
      address_coordinates_to_search = [-23.5651727, -46.65228159999998]
      find(:css, '.cpy-latitude-field', visible: false).set(address_coordinates_to_search[0])
      find(:css, '.cpy-longitude-field', visible: false).set(address_coordinates_to_search[1])
      find(:css, '.cpy-city-name', visible: false).set('tanganmandapio')
      find(:css, '.cpy-state-name', visible: false).set('SP')
      find(:css, '.cpy-address', visible: false).set('rua das bananas')
      find(:css, '.cpy-pack-length', visible: false).set(3)
      find('.cpy-checkin-date', visible: false).set offer.checkin_timestamp.strftime("%d/%m/%Y")
      select "#{offer.checkin_timestamp.strftime("%k:00").gsub(' ','')}", from: 'by_checkin_hour'
      page.find('.cpy-search-button').click
      page.find('.cpy-hotel-details-link').click
      page.should have_content('Atenção: cancelamento da reserva com até 24 horas de antecedência.')
      page.should_not have_content('Atenção: este hotel não aceita cancelamento de reservas.')
    end

    scenario 'I should a warning if the hotel does not accept booking cancellation' do
      hotel = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244", minimum_hours_of_notice: -1)
      offer = FactoryGirl.create(:offer, hotel: hotel)

      visit public_root_path
      address_coordinates_to_search = [-23.5651727, -46.65228159999998]
      find(:css, '.cpy-latitude-field', visible: false).set(address_coordinates_to_search[0])
      find(:css, '.cpy-longitude-field', visible: false).set(address_coordinates_to_search[1])
      find(:css, '.cpy-city-name', visible: false).set('tanganmandapio')
      find(:css, '.cpy-state-name', visible: false).set('SP')
      find(:css, '.cpy-address', visible: false).set('rua das bananas')
      find(:css, '.cpy-pack-length', visible: false).set(3)
      find('.cpy-checkin-date', visible: false).set offer.checkin_timestamp.strftime("%d/%m/%Y")
      select "#{offer.checkin_timestamp.strftime("%k:00").gsub(' ','')}", from: 'by_checkin_hour'
      page.find('.cpy-search-button').click
      page.find('.cpy-hotel-details-link').click
      page.should_not have_content('Atenção: cancelamento da reserva com até')
      page.should_not have_content('horas de antecedência.')
      page.should have_content('Atenção: este hotel não aceita cancelamento de reservas.')
    end

    specify 'I see the offers sorted by price' do
      hotel = FactoryGirl.create(:hotel, latitude: "-23.5625897", longitude: "-46.6549244", minimum_hours_of_notice: -1)
      offer1 = FactoryGirl.create(:offer, hotel: hotel, price: 200)
      offer2 = FactoryGirl.create(:offer, hotel: hotel, price: 400, checkin_timestamp: offer1.checkin_timestamp)
      offer3 = FactoryGirl.create(:offer, hotel: hotel, price: 100, checkin_timestamp: offer1.checkin_timestamp)

      visit public_root_path
      address_coordinates_to_search = [-23.5651727, -46.65228159999998]
      find(:css, '.cpy-latitude-field', visible: false).set(address_coordinates_to_search[0])
      find(:css, '.cpy-longitude-field', visible: false).set(address_coordinates_to_search[1])
      find(:css, '.cpy-city-name', visible: false).set('tanganmandapio')
      find(:css, '.cpy-state-name', visible: false).set('SP')
      find(:css, '.cpy-address', visible: false).set('rua das bananas')
      find(:css, '.cpy-pack-length', visible: false).set(3)
      find('.cpy-checkin-date', visible: false).set offer1.checkin_timestamp.strftime("%d/%m/%Y")
      select "#{offer1.checkin_timestamp.strftime("%k:00").gsub(' ','')}", from: 'by_checkin_hour'
      page.find('.cpy-search-button').click
      page.find('.cpy-hotel-details-link').click

      page.has_selector?('tbody tr:nth-child(1)', text: offer3.price.to_f)
      page.has_selector?('tbody tr:nth-child(2)', text: offer1.price.to_f)
      page.has_selector?('tbody tr:nth-child(3)', text: offer2.price.to_f)
    end

  end

end
