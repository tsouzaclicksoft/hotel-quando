# encoding: utf-8
require 'spec_helper'

story 'As a Visitor I want to sign up' do
  before(:each) do
    visit new_public_user_registration_path
  end

  describe 'with invalid parameters' do

    it 'should_not have blank name' do
      page.find('#public_user_email').set("email@email.com")
      page.find('#public_user_password').set("temp123")
      page.find('#public_user_password_confirmation').set("temp123")
      page.find('.cpy-submit-button').click
      page.should have_content("Nome não pode ficar em branco")
    end

    it 'should_not have invalid email' do
      page.find('#public_user_name').set("test name")
      page.find('#public_user_email').set("emailemail.com")
      page.find('#public_user_password').set("temp123")
      page.find('#public_user_password_confirmation').set("temp123")
      page.find('.cpy-submit-button').click
      page.should have_content("E-mail não é válido")
    end

    it 'should_not have blank password' do
      page.find('#public_user_name').set("test name")
      page.find('#public_user_email').set("email@email.com")
      page.find('#public_user_password').set("")
      page.find('#public_user_password_confirmation').set("temp123")
      page.find('.cpy-submit-button').click
      page.should have_content("Senha não pode ficar em branco")
    end

    it 'should_not have invalid password confirmation' do
      page.find('#public_user_name').set("test name")
      page.find('#public_user_email').set("email@email.com")
      page.find('#public_user_password').set("temp123")
      page.find('#public_user_password_confirmation').set("null")
      page.find('.cpy-submit-button').click
      page.should have_content("Confirmação de senha não está de acordo")
    end
  end

  describe 'with valid parameters' do

    before(:each) do
      visit public_root_path
      visit new_public_user_registration_path
      page.find('#public_user_name').set("test name")
      page.find('#public_user_birth_date').set("12/10/1960")
      page.find('#public_user_email').set("email@email.com")
      page.find('#public_user_password').set("temp123")
      page.find('#public_user_password_confirmation').set("temp123")
    end

    it "should register the user with pt-br language" do
      page.find('#public_user_locale').select("Português (Brasil)")
      page.find('.cpy-submit-button').click
      page.should have_content('Bem-vindo ao Hotel Quando! Você se registrou com sucesso.')
      User.count.should == 1
    end

    it "should register the user with en language" do
      page.find('#public_user_locale').select("English")
      page.find('.cpy-submit-button').click
      page.should have_content('Welcome to Hotel Quando! You have signed up successfully.')
      User.count.should == 1
    end

    it "should not redirect to home" do
      page.find('.cpy-submit-button').click
      should_not_be_on public_root_path
    end
  end
end

story "As a Registered User I want to reset my password," do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @user.confirmed_at = Time.now.utc
    @user.save!
    visit new_public_user_password_path
  end

  it 'with a invalid email' do
    page.find('#public_user_email').set("email_muito_loco@email.com")
    page.find('.cpy-submit-button').click
    page.should have_content("E-mail não encontrado")
  end

  it 'with a valid email' do
    page.find('#public_user_email').set("#{@user.email}")
    page.find('.cpy-submit-button').click
    should_be_on new_public_user_session_path
  end
end

story "As a Registered User I want to sign in" do
  before(:each) do
    reset_session!
  end

  describe 'with valid parameters' do

    it "should redirect to user bookings" do
      user = FactoryGirl.create(:user)
      user.password = "temp123"
      user.password_confirmation = "temp123"
      user.save!
      visit public_bookings_path

      page.find('#public_user_email').set(user.email)
      page.find('#public_user_password').set("temp123")
      page.find('.cpy-submit-button').click

      should_be_on public_bookings_path
    end

    it 'should sign in redirect to previous url' do
      @user = FactoryGirl.create(:user)
      @user.confirmed_at = Time.now.utc
      @user.save!
      visit public_bookings_path
      should_be_on new_public_user_session_path
      page.find('#public_user_email').set("#{@user.email}")
      page.find('#public_user_password').set("#{@user.password}")
      page.find('.cpy-submit-button').click
      should_be_on public_bookings_path
    end

    it 'should not redirect to home' do
      @user = FactoryGirl.create(:user)
      @user.confirmed_at = Time.now.utc
      @user.save!
      visit public_root_path
      visit new_public_user_session_path
      page.find('#public_user_email').set("#{@user.email}")
      page.find('#public_user_password').set("#{@user.password}")
      page.find('.cpy-submit-button').click
      should_not_be_on public_root_path
    end
  end
end

story "As a logged Registered User I want to sign out" do
  before(:each) do
    reset_session!
  end

  it 'should not redirect to home' do
    @user = FactoryGirl.create(:user)
    @user.confirmed_at = Time.now.utc
    @user.save!
    visit public_root_path
    visit new_public_user_session_path
    page.find('#public_user_email').set("#{@user.email}")
    page.find('#public_user_password').set("#{@user.password}")
    page.find('.cpy-submit-button').click
    visit public_root_path
    page.find(".cpy-log-out-link").click
    should_not_be_on public_root_path
  end

  it 'should redirect to bookings page if I log in right after log out', r:true do
    @user = FactoryGirl.create(:user)
    @user.confirmed_at = Time.now.utc
    @user.save!
    visit public_root_path
    visit new_public_user_session_path
    page.find('#public_user_email').set("#{@user.email}")
    page.find('#public_user_password').set("#{@user.password}")
    page.find('.cpy-submit-button').click
    visit public_root_path
    page.find(".cpy-log-out-link").click

    page.find('#public_user_email').set("#{@user.email}")
    page.find('#public_user_password').set("#{@user.password}")
    page.find('.cpy-submit-button').click

    should_be_on public_bookings_path
  end
end

story 'As a Registered User I want to edit my account, visiting my profile page' do
  context 'as a physical person' do
    before(:each) do
      @user = FactoryGirl.create(:user)
      sign_in_user(@user)

      visit "/perfil"
    end


    describe 'with invalid inputs.' do

      context 'With an invalid name' do
        it 'should return me an error message and remain in the same page' do
          fill_in "Nome", with: ''
          page.find('.cpy-submit-button').click
          should_be_on public_user_profile_path
          page.should have_content('Nome não pode ficar em branco')
        end
      end

      context 'With an invalid current password', :js => true do
        it 'should return me an error message and remain in the same page' do
          ensure_clicking_on_element_that_hides_after_click('.cpy-change-password-link')

          fill_in 'Senha atual', with: 'senha-errada'
          page.find('.cpy-submit-button').click
          should_be_on public_user_profile_path
          page.should have_content('não é válido')
        end
      end

      context 'With unmatched new password and password confirmation', :js => true do
        it 'should should return me an error message, remain in the same page and not change the password' do
          ensure_clicking_on_element_that_hides_after_click('.cpy-change-password-link')

          fill_in 'Senha atual', with: @user.password
          fill_in 'Nova senha', with: 'nova-senha'
          fill_in 'Confirmação de senha', with: 'nova-senha-errada'
          page.find('.cpy-submit-button').click
          should_be_on public_user_profile_path

          page.should have_content('não está de acordo')
        end
      end

      context 'With a short new password', :js => true do
        it 'should should return me an error message, remain in the same page and not change the password' do
          ensure_clicking_on_element_that_hides_after_click('.cpy-change-password-link')

          fill_in 'Senha atual', with: @user.password
          fill_in 'Nova senha', with: 'nwpw'
          fill_in 'Confirmação de senha', with: 'nwpw'
          page.find('.cpy-submit-button').click
          should_be_on public_user_profile_path

          page.should have_content('é muito curto (mínimo: 5 caracteres)')
        end
      end

    end

    describe 'with valid inputs.' do

      before(:each) do
        @user = FactoryGirl.create(:user)
        @user.password = "temp123"
        @user.password_confirmation = "temp123"
        @user.save!
        reset_session!
        visit public_root_path
        page.find('.cpy-sign-in-link').click

        page.find('#public_user_email').set("#{@user.email}")
        page.find('#public_user_password').set("#{@user.password}")
        page.find('.cpy-submit-button').click

        #não consegui simular hover no menu pra entrar no perfil
        visit '/perfil'
      end

      context 'When I change my name' do
        it 'should remain in the same page and change my name' do
          fill_in "Nome", with: 'Novo Nome'
          page.find('.cpy-submit-button').click

          should_be_on public_user_profile_path
          page.should have_content('Conta editada com sucesso')
        end
      end

      context 'When I change my password', :js => true, f:true do
        it 'should remain in the same page and my password should be changed' do

          ensure_clicking_on_element_that_hides_after_click('.cpy-change-password-link')

          fill_in 'user_current_password', with: @user.password
          fill_in 'user_password', with: 'nova-senha'
          fill_in 'user_password_confirmation', with: 'nova-senha'
          old_user_encrypted_password = @user.reload.encrypted_password
          page.find('.cpy-submit-button').click
          @user.reload.encrypted_password.should_not == old_user_encrypted_password

          page.should have_content('Conta editada com sucesso')
          should_be_on public_user_profile_path

          reset_session!
          sign_in_user(@user, "fail")
        end
      end

    end
  end

end
