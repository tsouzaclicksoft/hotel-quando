# encoding: utf-8
require 'spec_helper'

story 'As an company administrator I can manage my account,' do
  before(:each) do
    @company = FactoryGirl.create(:company)
    sign_in_company(@company)
  end

  scenario 'I can see my info' do
    visit company_root_path
    page.should have_content("#{@company.name}")
  end

  scenario 'I can change my password' do
    visit company_root_path
    click_link 'Atualizar informações da empresa'
    fill_in 'company_current_password', with: '123456789'
    fill_in 'company_password', with: '123123123'
    fill_in 'company_password_confirmation', with: '123123123'
    page.find('.cpy-submit-button').click
    click_link 'Sair'
    visit company_root_path
    fill_in 'company_company_email', with: @company.email
    fill_in 'company_company_password', with: '123123123'
    click_button 'Entrar'
    page.should have_content("Bem-vindo ao Hotel Quando.")
  end

  scenario 'I can recover my password' do
    ActionMailer::Base.deliveries.clear
    click_link 'Sair'
    click_link 'Esqueceu a senha?'
    fill_in 'company_company_email', with: @company.email
    page.find('.cpy-submit-button').click
    ActionMailer::Base.deliveries.should have(1).email
    ActionMailer::Base.deliveries.first.to.should == [ @company.email ]
  end
end
