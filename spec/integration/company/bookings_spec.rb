# encoding: utf-8
require 'spec_helper'

story 'As a company, I can view my created bookings' do
  before(:each) do
    @company = FactoryGirl.create(:company)
    @company2 = FactoryGirl.create(:company)
    @employee = FactoryGirl.create(:user, company: @company)
    @employee2 = FactoryGirl.create(:user, company: @company2)
    sign_in_company @company
  end

  context 'without bookings' do
    scenario 'I should see a message telling me that there wasnt any bookings found' do
      visit company_bookings_path
      page.should have_content 'Nenhuma reserva encontrada'
    end
  end

  context 'with bookings' do
    context 'I can list bookings' do
      before(:each) do
        @company_booking = FactoryGirl.create(:booking, user: @employee, company: @company, status: Booking::ACCEPTED_STATUSES[:canceled])
        @other_company_booking = FactoryGirl.create(:booking, user: @employee2, company: @company2, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
        visit company_bookings_path
      end

      scenario 'I can list the bookings' do
        within '.cpy-bookings-list' do
          page.should have_content I18n.l(@company_booking.offer.checkin_timestamp)
          page.should have_content @company_booking.user.name
          page.should have_content I18n.t(Booking::ACCEPTED_STATUS_REVERSED[@company_booking.status])
          page.should have_content @company_booking.hotel.name

          page.should_not have_content I18n.l(@other_company_booking.offer.checkin_timestamp)
          page.should_not have_content @other_company_booking.user.name
          page.should_not have_content I18n.t(Booking::ACCEPTED_STATUS_REVERSED[@other_company_booking.status])
          page.should_not have_content @other_company_booking.hotel.name
        end
      end
    end

    context 'I want to see the bookings details' do
      before(:each) do
        @company_booking = FactoryGirl.create(:booking, user: @employee, company: @company)
        @booking_not_related_to_company = FactoryGirl.create(:booking)
      end

      scenario 'I can see the bookings related to my company' do
        visit company_booking_path(@company_booking.id)
        page.should have_content @company_booking.guest_name
      end

      scenario 'I cant view the details of the bookings that are not related to my company' do
        expect{
          visit company_booking_path(@booking_not_related_to_company.id)
        }.to raise_error
      end

      scenario 'I can cancel a created booking' do
        visit company_booking_path(@company_booking.id)
        BookingCancellationSrv.should_receive(:call).and_call_original
        page.find('.cpy-cancel-booking').click
        @company_booking.reload
        @company_booking.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end
    end
  end
end
