# encoding: utf-8
require 'spec_helper'
story 'As an company manager I can manage credit cards,' do
  before(:each) do
    FactoryGirl.create(:country)
    FactoryGirl.create(:state)
    @company = FactoryGirl.create(:company)
    sign_in_company @company
  end

  scenario 'I can list credit cards' do
    credit_card = FactoryGirl.create(:credit_card, company: @company, user: nil)
    visit company_credit_cards_path
    page.should have_content(credit_card.billing_name)
    page.should have_content(credit_card.last_four_digits)
  end

  scenario 'I should see a message if there are no employees registered' do
    visit company_credit_cards_path
    page.should have_content('Nenhum cartão de crédito cadastrado.')
  end

  scenario 'I can include new credit cards' do
    MaxiPagoInterface.stub(:generate_client_token_for).and_return(123)
    MaxiPagoInterface.stub(:generate_credit_card_token_for).and_return(123)
    visit new_company_credit_card_path
    fill_in 'credit_card_billing_name', with: 'Name LastName'
    fill_in 'credit_card_owner_cpf', with: '471.352.521-95'
    fill_in 'credit_card_billing_address1', with: 'Rua das bananeiras, 123 - apt 11'
    fill_in 'credit_card_billing_zipcode', with: '123456-789'
    select 'Brasil', from: 'credit_card_billing_country'
    all('#credit_card_billing_state option')[0].select_option
    fill_in 'credit_card_billing_city', with: 'City'
    fill_in 'credit_card_billing_phone', with: '(11) 98762-5134'
    fill_in 'credit_card_temporary_number', with: '4444 4444 4444 4448'
    select '2024', from: 'credit_card_expiration_year'
    select 'Dez', from: 'credit_card_expiration_month'
    page.find('.cpy-submit-button').click
    CreditCard.count.should == 1
    created_credit_card = CreditCard.first
    created_credit_card.company_id.should == @company.id
    created_credit_card.last_four_digits.should.to_s == '4448'
  end

  scenario 'I should see error messages when including new credit cards with invalid information' do
    visit new_company_credit_card_path
    fill_in 'credit_card_billing_name', with: ''
    page.find('.cpy-submit-button').click
    page.should have_css('.credit_card_billing_name.has-error')
  end

  scenario 'I should not see other companies credit_cards' do
    other_company_credit_card = FactoryGirl.create(:credit_card)
    visit company_credit_cards_path
    page.should_not have_content(other_company_credit_card.last_four_digits)
  end

  context 'visiting the credit card details page' do
    before(:each) do
      @credit_card = FactoryGirl.create(:credit_card, company: @company)
      visit company_credit_card_path(@credit_card)
    end

    scenario 'I can see the credit card informations' do
      page.should have_content(@credit_card.billing_name)
      page.should have_content(@credit_card.last_four_digits)
      page.should have_content(@credit_card.owner_cpf)
      page.should have_content(@credit_card.billing_address1)
    end

  end
end
