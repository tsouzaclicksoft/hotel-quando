# encoding: utf-8
require 'spec_helper'
story 'As an company manager I can manage departments,' do
  before(:each) do
    @company = FactoryGirl.create(:company)
    sign_in_company @company
  end

  scenario 'I can list departments' do
    FactoryGirl.create_list(:department, 5, company: @company)
    visit company_departments_path
    Department.all.each do | department |
      page.should have_content(department.name)
    end
  end

  scenario 'I should see a message if there are no departments registered' do
    visit company_departments_path
    page.should have_content('Nenhum departamento cadastrado')
  end

  scenario 'I can include new departments' do
    visit company_departments_path
    page.find('.cpy-add-link').click
    fill_in 'department_name', with: 'Departamento Teste'
    page.find('.cpy-submit-button').click
    Department.count.should == 1
    created_department = Department.first
    created_department.company_id.should == @company.id
  end

  scenario 'I should see error messages when including new departments with invalid information' do
    visit company_departments_path
    page.find('.cpy-add-link').click
    fill_in 'department_name', with: ''
    page.find('.cpy-submit-button').click
    page.should have_css('.department_name.has-error')
  end

  scenario 'I should not see other companies departments' do
    department = FactoryGirl.create(:department, company: @company)
    other_company_department = FactoryGirl.create(:department)
    visit company_departments_path
    page.should_not have_content(other_company_department.name)
  end

  context 'visiting the department details page' do
    before(:each) do
      @department = FactoryGirl.create(:department, company: @company)
      visit company_department_path(@department)
    end

    scenario 'I can see the department informations' do
      page.should have_content("#{@department.name}")
    end

    scenario 'I can see the user list for the department' do
      department_employees = FactoryGirl.create_list(:user, 3, department: @department, company: @company)
      visit company_department_path(@department)

      other_department_employees = FactoryGirl.create_list(:user, 3, company: @company)

      department_employees.each do | user |
        page.should have_content user.email
      end

      other_department_employees.each do | user |
        page.should_not have_content user.email
      end
    end

  end
end
