# encoding: utf-8
require 'spec_helper'
story 'As an company manager I can manage employees,' do
  before(:each) do
    @company = FactoryGirl.create(:company)
    sign_in_company @company
  end

  context 'visiting the user list' do

    context 'without employees' do
      scenario 'I should see a message if there are no employees registered' do
        visit company_employees_path
        page.should have_content('Não há usuários cadastrados')
      end
    end

    context 'with employees' do
      before(:each) do
        @employee = FactoryGirl.create(:user, company: @company)
        visit company_employees_path
      end

      scenario 'I can list employees' do
        page.should have_content(@employee.email)
      end

      scenario 'I can access as one of my employees' do
        page.find('.cpy-log-in-as-employee').click
        should_be_on public_root_pt_br_path
        page.should have_content 'Você está acessando o sistema como ' + @employee.name
      end

    end

  end

  scenario 'I can include new employees' do
    visit company_employees_path
    FactoryGirl.create(:department, company: @company)
    page.find('.cpy-add-link').click
    fill_in 'user_name',        with: 'Funcionário Teste'
    fill_in 'user_email',       with: 'email@empresa.com'
    page.find('.cpy-submit-button').click
    User.count.should == 1
    created_employee = User.first
    created_employee.company_id.should == @company.id
  end

  scenario 'I should see error messages when including new employees with invalid information' do
    visit company_employees_path
    page.find('.cpy-add-link').click
    fill_in 'user_name', with: ''
    fill_in 'user_email', with: ''
    page.find('.cpy-submit-button').click
    page.should have_css('.user_name.has-error')
    page.should have_css('.user_email.has-error')
  end

  scenario 'I should not see other companies employees' do
    department = FactoryGirl.create(:department, company: @company)
    other_company_department = FactoryGirl.create(:department)
    visit company_employees_path
    page.should_not have_content(other_company_department.name)
  end

  context 'visiting the employee details page' do
    before(:each) do
      department = FactoryGirl.create(:department, company: @company)
      @employee = FactoryGirl.create(:user, company: @company, department: department)
      visit company_employee_path(@employee)
    end

    scenario 'I can see the employee informations' do
      page.should have_content("#{@employee.name}")
      page.should have_content("#{@employee.email}")
    end

    scenario 'I can access as the employee' do
      page.find('.cpy-log-in-as-employee').click
      should_be_on public_root_pt_br_path
      page.should have_content 'Você está acessando o sistema como ' + @employee.name
    end

  end
end
