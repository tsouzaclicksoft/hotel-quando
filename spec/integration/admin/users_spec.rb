# encoding: utf-8
require 'spec_helper'

story 'As an administrator I can view users,' do
  before(:each) do
    sign_in_admin
    FactoryGirl.create(:city)
  end

  context 'visiting the user list' do
    scenario 'I can list users' do
      user_1 = FactoryGirl.create(:user)
      user_2 = FactoryGirl.create(:user)
      user_3 = FactoryGirl.create(:user)
      visit admin_users_path
      page.should have_content(user_1.name)
      page.should have_content(user_2.name)
      page.should have_content(user_3.name)
    end

    scenario 'I can filter users' do
      FactoryGirl.create(:country)
      user_1 = FactoryGirl.create(:user)
      user_2 = FactoryGirl.create(:user, name: user_1.name + 'abc', cpf: '717.128.824-22')
      user_3 = FactoryGirl.create(:user)
      user_4 = FactoryGirl.create(:user, country: 'US')
      user_5 = FactoryGirl.create(:user, state: 'ES')
      user_6 = FactoryGirl.create(:user, city: 'Outra cidade')
      visit admin_users_path
      select Country.find_by_iso_initials(user_1.country).name_pt_br, from: 'by_country'
      page.find('.cpy-filter-button').click
      page.should have_content(user_1.name)
      page.should have_content(user_2.name)
      page.should have_content(user_3.name)
      page.should_not have_content(user_4.name)
      fill_in 'by_state', with: user_1.state.downcase
      page.find('.cpy-filter-button').click
      page.should have_content(user_1.name)
      page.should have_content(user_2.name)
      page.should have_content(user_3.name)
      page.should_not have_content(user_5.name)
      fill_in 'by_city', with: user_1.city.downcase
      page.find('.cpy-filter-button').click
      page.should have_content(user_1.name)
      page.should have_content(user_2.name)
      page.should have_content(user_3.name)
      page.should_not have_content(user_6.name)
      fill_in 'by_name', with: user_1.name.downcase
      page.find('.cpy-filter-button').click
      page.should have_content(user_1.name)
      page.should have_content(user_2.name)
      page.should_not have_content(user_3.name)
      fill_in 'by_passport', with: user_1.passport.to_s
      page.find('.cpy-filter-button').click
      page.should have_content(user_1.name)
      fill_in 'by_cpf', with: user_1.cpf.to_s
      page.find('.cpy-filter-button').click
      page.should have_content(user_1.name)
      page.should_not have_content(user_2.name)

    end

  end
end
