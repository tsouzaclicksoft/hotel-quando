# encoding: utf-8
require 'spec_helper'

story "As an administrator I can see the hotels reports," do
  before(:each) do
    sign_in_admin
    FactoryGirl.create(:city)
  end

  context "visiting the reports page" do

    context "without any hotel registered" do

      scenario "I should see a message if there are no hotels registered" do
        visit admin_hotel_reports_path
        page.should have_content("Nenhum hotel cadastrado")
      end
    end

    context "with hotels registered," do

      context "if there are no bookings," do

        scenario "I should see a message" do
          hotel = FactoryGirl.create(:hotel)
          visit admin_hotel_reports_path
          select(hotel.name, :from => "hotel_id")
          select("Fatura Dia 10", :from => "invoice_type")
          select("#{I18n.t('date.month_names')[Date.today.month]} #{Date.today.year}", :from => "date")
          page.find(".cpy-generate-button").click

          page.should have_content("O hotel selecionado não possui informações suficientes para gerar o relatório")
        end
      end

      context "if there are bookings," do
        before(:each) do
          @hotel1 = FactoryGirl.create(:hotel, comission_in_percentage: 10, iss_in_percentage: 5)
          @hotel2 = FactoryGirl.create(:hotel, comission_in_percentage: 12, iss_in_percentage: 6)

          @room_type1 = FactoryGirl.create(:room_type, hotel: @hotel1)
          @room_type2 = FactoryGirl.create(:room_type, hotel: @hotel2)

          @offer1 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 1.day - 3.hours, pack_in_hours: 3, price: 11)
          @offer2 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 3.hours, pack_in_hours: 3, price: 12)
          @offer3 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 3.hours, pack_in_hours: 3, price: 13)
          @offer4 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 3.hours, pack_in_hours: 3, price: 14)

          @offer5 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 1.day - 3.hours, pack_in_hours: 3, no_show_value: 111)
          @offer6 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.month).end_of_month - 3.hours, pack_in_hours: 3, no_show_value: 112)
          @offer7 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 3.hours, pack_in_hours: 3, no_show_value: 113)
          @offer8 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: Time.now.utc.beginning_of_month - 1.month + 9.days - 3.hours, pack_in_hours: 3, no_show_value: 114)
          @offer9 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 1.day - 3.hours, pack_in_hours: 3, no_show_value: 115)

          @offer10 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.beginning_of_month - 1.month), pack_in_hours: 3, no_show_value: 115, price: 50)
          @offer11 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.beginning_of_month - 1.month + 10.days), pack_in_hours: 3, no_show_value: 115, price: 60)

          @booking1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @room_type1, offer: @offer1, number_of_people: 1, booking_tax: 1)
          @booking2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @room_type1, offer: @offer2, number_of_people: 1, booking_tax: 1)
          @booking3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @room_type1, offer: @offer3, number_of_people: 1, booking_tax: 1)
          @booking4 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @room_type2, offer: @offer4, number_of_people: 1, booking_tax: 1)

          @no_show1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type1, offer: @offer5, number_of_people: 1, booking_tax: 1)
          @no_show2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type1, offer: @offer6, number_of_people: 1, booking_tax: 1)
          @no_show3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type1, offer: @offer7, number_of_people: 1, booking_tax: 1)
          @no_show4 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type1, offer: @offer8, number_of_people: 1, booking_tax: 1)
          @no_show5 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type1, offer: @offer9, number_of_people: 1, booking_tax: 1)

          @booking_paid_online1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @room_type1, offer: @offer10, number_of_people: 1, booking_tax: 1)
          @booking_paid_online2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @room_type1, offer: @offer11, number_of_people: 1, booking_tax: 1)

          visit admin_hotel_reports_path
        end

        context "When choosing the 'tenth day' invoice," do
          before(:each) do
            select(@hotel1.name, :from => "hotel_id")
            select("Fatura Dia 10", :from => "invoice_type")
            select("#{I18n.t('date.month_names')[Date.today.month]} #{Date.today.year}", :from => "date")
            page.find(".cpy-generate-button").click
          end

          scenario "I should see the bookings list" do
            page.should have_no_css(".cpy-pagination-links")
            within(".cpy-bookings-paid-at-the-hotel-list") do
              page.should have_no_content(@booking1.guest_name)
              page.should have_no_content(@booking2.guest_name)
              page.should have_content(@booking3.guest_name)
              page.should have_no_content(@booking4.guest_name)
              page.should have_no_content(@no_show1.guest_name)
              page.should have_no_content(@no_show2.guest_name)
              page.should have_no_content(@no_show3.guest_name)
              page.should have_no_content(@no_show4.guest_name)
              page.should have_no_content(@no_show5.guest_name)
            end
          end

          scenario "I should see the no-shows list" do
            page.should have_no_css(".cpy-pagination-links")
            within(".cpy-no-shows-list") do
              page.should have_no_content(@booking1.guest_name)
              page.should have_no_content(@booking2.guest_name)
              page.should have_no_content(@booking3.guest_name)
              page.should have_no_content(@booking4.guest_name)
              page.should have_no_content(@no_show1.guest_name)
              page.should have_content(@no_show2.guest_name)
              page.should have_no_content(@no_show3.guest_name)
              page.should have_no_content(@no_show4.guest_name)
              page.should have_no_content(@no_show5.guest_name)
            end
          end

          scenario 'I should see the bookings paid online list' do
            page.should have_no_css(".cpy-pagination-links")
            within(".cpy-bookings-paid-online-list") do
              page.should have_content(@booking_paid_online1.guest_name)
              page.should_not have_content(@booking_paid_online2.guest_name)
            end
          end

          scenario "I should see the correct overview information" do
            within(".cpy-bookings-info") do
              within(".cpy-total") { page.should have_content(1) }
              within(".cpy-total-value") { page.should have_content("R$ 13,00") }
              within(".cpy-total-value-without-iss") { page.should have_content("R$ 12,35") }
              within(".cpy-comission") { page.should have_content("R$ 1,24") }
              within(".cpy-booking-tax") { page.should have_content("R$ 1,00") }
            end

            within(".cpy-bookings-paid-online-info") do
              within(".cpy-total") { page.should have_content(1) }
              within(".cpy-total-value") { page.should have_content("R$ 50,00") }
              within(".cpy-total-value-without-iss") { page.should have_content("R$ 47,50") }
              within(".cpy-credit-card-tax") { page.should have_content("R$ 1,50") }
              within(".cpy-comission") { page.should have_content("R$ 4,75") }
              within(".cpy-booking-tax") { page.should have_content("R$ 1,00") }
              within(".cpy-total-value-to-be-transferred-to-the-hotel") { page.should have_content("R$ 43,75") }
            end

            within(".cpy-no-shows-info") do
              within(".cpy-total") { page.should have_content(1) }
              within(".cpy-total-value") { page.should have_content("R$ 112,00") }
              within(".cpy-total-value-without-iss") { page.should have_content("R$ 106,40") }
              within(".cpy-credit-card-tax") { page.should have_content("R$ 3,36") }
              within(".cpy-comission") { page.should have_content("R$ 10,64") }
              within(".cpy-booking-tax") { page.should have_content("R$ 1,00") }
              within(".cpy-total-to-transfer-to-the-hotel") { page.should have_content("R$ 98,00") }
            end

            within(".cpy-totalizing") do
              within(".cpy-total-value") { page.should have_content("R$ 175,00") }
              within(".cpy-total-value-without-iss") { page.should have_content("R$ 166,25") }

              within(".cpy-comission") { page.should have_content("R$ 16,63") }
              within(".cpy-booking-tax") { page.should have_content("R$ 3,00") }
              within(".cpy-total-to-receive-from-the-hotel") { page.should have_content("R$ 1,24") }
              within(".cpy-total-credit-card-tax") { page.should have_content("R$ 4,86") }
              within('.cpy-total-to-pay-to-the-hotel') { page.should have_content("R$ 141,75") }

              within('.cpy-balance') { page.should have_content("-R$ 139,52") }
            end
          end
        end

        context "When choosing the 'end of month' invoice," do
          before(:each) do
            select(@hotel1.name, :from => "hotel_id")
            select("Fatura Final de Mês", :from => "invoice_type")
            select("#{I18n.t('date.month_names')[Date.today.month]} #{Date.today.year}", :from => "date")
            page.find(".cpy-generate-button").click
          end

          scenario "I should see the no-shows list" do
            page.should have_no_css(".cpy-pagination-links")
            within(".cpy-no-shows-list") do
              page.should have_no_content(@booking1.guest_name)
              page.should have_no_content(@booking2.guest_name)
              page.should have_no_content(@booking3.guest_name)
              page.should have_no_content(@booking4.guest_name)
              page.should have_no_content(@no_show1.guest_name)
              page.should have_no_content(@no_show2.guest_name)
              page.should have_no_content(@no_show3.guest_name)
              page.should have_no_content(@no_show5.guest_name)
              page.should have_content(@no_show4.guest_name)
            end
          end

          scenario "I should see the bookings paid online list" do
            page.should have_no_css(".cpy-pagination-links")
            within(".cpy-bookings-paid-online-list") do
              page.should have_no_content(@booking2.guest_name)
              page.should have_no_content(@booking3.guest_name)
              page.should have_no_content(@booking4.guest_name)
              page.should have_no_content(@no_show1.guest_name)
              page.should have_no_content(@no_show2.guest_name)
              page.should have_no_content(@no_show3.guest_name)
              page.should have_no_content(@no_show5.guest_name)
              page.should have_no_content(@no_show4.guest_name)
              page.should have_no_content(@booking_paid_online1.guest_name)
              page.should have_content(@booking_paid_online2.guest_name)
            end
          end

          scenario "I should see the no-shows overview information" do
            within(".cpy-no-shows-info") do
              within(".cpy-total") { page.should have_content(1) }
              within(".cpy-total-value") { page.should have_content("R$ 114,00") }
              within(".cpy-total-value-without-iss") { page.should have_content("R$ 108,30") }
              within(".cpy-credit-card-tax") { page.should have_content("R$ 3,42") }
              within(".cpy-comission") { page.should have_content("R$ 10,83") }
              within(".cpy-booking-tax") { page.should have_content("R$ 1,00") }
              within(".cpy-total-to-transfer-to-the-hotel") { page.should have_content("R$ 99,75") }
            end
          end

          scenario "I should see the bookings paid online overview information" do
            within(".cpy-bookings-paid-online-info") do
              within(".cpy-total") { page.should have_content(1) }
              within(".cpy-total-value") { page.should have_content("R$ 60,00") }
              within(".cpy-total-value-without-iss") { page.should have_content("R$ 57,00") }
              within(".cpy-credit-card-tax") { page.should have_content("R$ 1,80") }
              within(".cpy-comission") { page.should have_content("R$ 5,70") }
              within(".cpy-booking-tax") { page.should have_content("R$ 1,00") }
              within(".cpy-total-value-to-be-transferred-to-the-hotel") { page.should have_content("R$ 52,50") }
            end
          end

          scenario 'I should see the correct totalizing information' do
            within(".cpy-totalizing") do
              within(".cpy-total-value") { page.should have_content("R$ 174,00") }
              within(".cpy-total-value-without-iss") { page.should have_content("R$ 165,30") }
              within(".cpy-comission") { page.should have_content("R$ 16,53") }
              within(".cpy-booking-tax") { page.should have_content("R$ 2,00") }
              within(".cpy-total-credit-card-tax") { page.should have_content("R$ 5,22") }
              within('.cpy-total-to-pay-to-the-hotel') { page.should have_content("R$ 152,25") }
            end
          end

          scenario 'I should not see the bookings paid at the hotel info' do
            page.should have_no_css(".cpy-bookings-info")
          end

        end
      end
    end
  end
end
