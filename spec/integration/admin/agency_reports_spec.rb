# encoding: utf-8
require 'spec_helper'

story "As an administrator I can see the agency reports," do
  before(:each) do
    sign_in_admin
  end

  context "visiting the reports page" do

    context "without any agency registered" do

      scenario "I should see a message" do
        visit admin_agency_reports_path
        page.should have_content("Nenhuma agência cadastrada")
      end
    end

    context "with agencies registered," do

      context "if there are no bookings," do

        scenario "I should see a message" do
          agency = FactoryGirl.create(:agency)
          visit admin_agency_reports_path
          select(agency.name, :from => "agency_id")
          select("Fatura Dia 10", :from => "invoice_type")
          select("#{I18n.t('date.month_names')[Date.today.month]} #{Date.today.year}", :from => "date")
          page.find(".cpy-generate-button").click

          page.should have_content("A agência selecionada não possui informações suficientes para gerar o relatório")
        end
      end

      context "if there are bookings," do
        before(:each) do
          @agency = FactoryGirl.create(:agency, comission_in_percentage: 10)

          @hotel1 = FactoryGirl.create(:hotel, comission_in_percentage: 10, iss_in_percentage: 5)
          @hotel2 = FactoryGirl.create(:hotel, comission_in_percentage: 10, iss_in_percentage: 5)
          @hotel3 = FactoryGirl.create(:hotel, comission_in_percentage: 0, iss_in_percentage: 0)

          @room_type1 = FactoryGirl.create(:room_type, hotel: @hotel1)
          @room_type2 = FactoryGirl.create(:room_type, hotel: @hotel2)

          @offer1 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 1.day - 3.hours, pack_in_hours: 3, price: 11)
          @offer2 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 3.hours, pack_in_hours: 3, price: 12)
          @offer3 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 3.hours, pack_in_hours: 3, price: 13)

          @offer5 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 1.day - 3.hours, pack_in_hours: 3, no_show_value: 111)
          @offer6 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.month).end_of_month - 3.hours, pack_in_hours: 3, no_show_value: 112)
          @offer7 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 3.hours, pack_in_hours: 3, no_show_value: 113)

          @offer8 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: Time.now.utc.beginning_of_month - 1.month + 9.days - 3.hours, pack_in_hours: 3, no_show_value: 114)
          @offer9 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 1.day - 3.hours, pack_in_hours: 3, no_show_value: 115)
          @offer10 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.beginning_of_month - 1.month), pack_in_hours: 3, no_show_value: 115, price: 50)
          @offer11 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.beginning_of_month - 1.month + 10.days), pack_in_hours: 3, no_show_value: 115, price: 60)

          @offer12 = FactoryGirl.create(:offer, hotel: @hotel3, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 3.hours, pack_in_hours: 3, price: 10)
          @offer13 = FactoryGirl.create(:offer, hotel: @hotel3, checkin_timestamp: (Time.now.beginning_of_month - 1.month + 10.days), pack_in_hours: 3, price: 10)


          @booking1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @offer1.room_type, offer: @offer1, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency), created_by_agency: true)
          @booking2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @offer2.room_type, offer: @offer2, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency))
          @booking3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @offer3.room_type, offer: @offer3, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency), created_by_agency: true)
          @booking4 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @offer12.room_type, offer: @offer12, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency), created_by_agency: true)

          @no_show1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer5.room_type, offer: @offer5, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency))
          @no_show2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer6.room_type, offer: @offer6, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency), created_by_agency: true)
          @no_show3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer7.room_type, offer: @offer7, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency))
          @no_show4 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer8.room_type, offer: @offer8, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency), created_by_agency: true)
          @no_show5 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer9.room_type, offer: @offer9, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency))

          @booking_paid_online1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @offer10.room_type, offer: @offer10, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency), created_by_agency: true)
          @booking_paid_online2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @offer11.room_type, offer: @offer11, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency))
          @booking_paid_online3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @offer13.room_type, offer: @offer13, number_of_people: 1, booking_tax: 1, company: FactoryGirl.create(:company, agency: @agency), created_by_agency: true)

          visit admin_agency_reports_path
        end

        context "When choosing the 'tenth day' invoice," do
          before(:each) do
            select(@agency.name, :from => "agency_id")
            select("Fatura Dia 10", :from => "invoice_type")
            select("#{I18n.t('date.month_names')[Date.today.month]} #{Date.today.year}", :from => "date")
            page.find(".cpy-generate-button").click
          end

          scenario "I should see the bookings list" do
            page.should have_content(@booking3.guest_name)
            page.should have_content(@booking4.guest_name)
            page.should have_content(@no_show2.guest_name)
            page.should have_content(@booking_paid_online1.guest_name)
          end

          scenario "I should see the correct overview information" do
            within(".cpy-agency-invoice") do
              within(".cpy-bookings-created-by-agency") { page.should have_content(4) }
              within(".cpy-bookings-created-by-agency-clients") { page.should have_content(0) }
              within(".cpy-total-value") { page.should have_content("R$ 185,00") }
              within(".cpy-hotelquando-comission") { page.should have_content("R$ 16,63") }
              within(".cpy-booking-tax") { page.should have_content("R$ 4,00") }

              within('.cpy-agency-comission') { page.should have_content("R$ 1,66") }
            end
          end
        end

        context "When choosing the 'end of month' invoice," do
          before(:each) do
            select(@agency.name, :from => "agency_id")
            select("Fatura Final de Mês", :from => "invoice_type")
            select("#{I18n.t('date.month_names')[Date.today.month]} #{Date.today.year}", :from => "date")
            page.find(".cpy-generate-button").click
          end

          scenario "I should see the bookings list" do
            page.should have_content(@no_show4.guest_name)
            page.should have_content(@booking_paid_online2.guest_name)
            page.should have_content(@booking_paid_online3.guest_name)
          end

          scenario 'I should see the correct totalizing information' do
            within(".cpy-agency-invoice") do
              within(".cpy-bookings-created-by-agency") { page.should have_content(2) }
              within(".cpy-bookings-created-by-agency-clients") { page.should have_content(1) }
              within(".cpy-total-value") { page.should have_content("R$ 184,00") }
              within(".cpy-hotelquando-comission") { page.should have_content("R$ 16,53") }
              within(".cpy-booking-tax") { page.should have_content("R$ 3,00") }
              within('.cpy-agency-comission') { page.should have_content("R$ 1,65") }
            end
          end

        end
      end
    end
  end
end
