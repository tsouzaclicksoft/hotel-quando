# encoding: utf-8
require 'spec_helper'

story 'As an administrator I can see possible partner contacts,' do
  before(:each) do
    sign_in_admin
  end

  context 'visiting the possible partner contacts list' do
    scenario 'I can see the contacts' do
      contact = FactoryGirl.create(:possible_partner_contact)
      visit admin_possible_partner_contacts_path
      page.should have_content(contact.name)
    end

    scenario 'I can filter the contacts' do
      contact_1 = FactoryGirl.create(:possible_partner_contact)
      contact_2 = FactoryGirl.create(:possible_partner_contact, locale: 'en', admin_visualized: true)
      visit admin_possible_partner_contacts_path
      find(:css, '.cpy-visualized-false').set(true)
      page.find('.cpy-filter-button').click
      page.should have_content(contact_1.name)
      page.should_not have_content(contact_2.name)
      page.find('.cpy-remove-filter-button').click
      find(:css, '.cpy-visualized-true').set(true)
      page.find('.cpy-filter-button').click
      page.should have_content(contact_2.name)
      page.should_not have_content(contact_1.name)
      page.find('.cpy-remove-filter-button').click
      select 'EN', from: 'by_locale'
      page.find('.cpy-filter-button').click
      page.should have_content(contact_2.name)
      page.should_not have_content(contact_1.name)
      page.find('.cpy-remove-filter-button').click
      select 'PT-BR', from: 'by_locale'
      page.find('.cpy-filter-button').click
      page.should have_content(contact_1.name)
      page.should_not have_content(contact_2.name)
      page.find('.cpy-remove-filter-button').click
      page.should have_content(contact_1.name)
      page.should have_content(contact_2.name)
      contact_3 = FactoryGirl.create(:possible_partner_contact, type_of_contact: PossiblePartnerContact::TYPES_OF_CONTACT[:company_contact])
      visit admin_possible_partner_contacts_path
      page.should have_content(contact_1.name)
      page.should have_content(contact_2.name)
      page.should have_content(contact_3.name)
      find(:css, '.cpy-contact-type-company').set(true)
      page.find('.cpy-filter-button').click
      page.should_not have_content(contact_1.name)
      page.should_not have_content(contact_2.name)
      page.should have_content(contact_3.name)
    end

    scenario 'I can see contacts paginated' do
      50.times do
        FactoryGirl.create(:possible_partner_contact)
      end
      visit admin_possible_partner_contacts_path
      page.should have_css('.cpy-pagination-links')
    end

    scenario 'I can mark all messages as read', js: true do
      10.times do
        FactoryGirl.create(:possible_partner_contact)
      end
      PossiblePartnerContact.where(admin_visualized: false).count.should == 10
      visit admin_possible_partner_contacts_path
      page.execute_script 'window.confirm = function () { return true }'
      page.find('.cpy-mark-all-as-read-button').click
      PossiblePartnerContact.where(admin_visualized: false).count.should == 0
    end
  end

  context 'visiting the contact details page' do
    before(:each) do
      @contact = FactoryGirl.create(:possible_partner_contact)
      visit admin_possible_partner_contact_path(@contact.id)
    end

    scenario 'I can see the details of the contact' do
      @contact.attributes.each do | attribute, value |
        next if ['id', 'admin_visualized', 'created_at', 'updated_at', 'locale'].include?(attribute)
        page.should have_content(value)
      end
    end
  end

end
