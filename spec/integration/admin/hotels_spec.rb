# encoding: utf-8
require 'spec_helper'
include ActionView::Helpers::NumberHelper

story 'As an administrator I can manage hotels,' do
  before(:each) do
    sign_in_admin
    FactoryGirl.create(:city)
  end

  scenario 'I can list hotels' do
    5.times do
      FactoryGirl.create(:hotel)
    end
    visit admin_hotels_path
    hotel_ids = Hotel.all.pluck(:id)
    hotel_ids.each do | id |
      page.should have_content("#{Hotel.find(id).name}")
    end
  end

  scenario 'I should see a message if there are no hotels registered' do
    visit admin_hotels_path
    page.should have_content('Nenhum hotel cadastrado')
  end

  scenario 'I can include new hotels', js:true do
    state = FactoryGirl.create(:state)
    city = FactoryGirl.create(:city, state: state)
    visit admin_hotels_path
    page.find('.cpy-add-link').click
    fill_in 'hotel_name', with: 'Hotel Teste'
    fill_in 'hotel_description_en', with: 'Descrição'
    fill_in 'hotel_description_pt_br', with: 'Descrição'
    fill_in 'hotel_payment_method_pt_br', with: 'Método'
    fill_in 'hotel_payment_method_en', with: 'Método'
    page.execute_script("$('#hotel_iss_in_percentage').val(5)")
    page.execute_script("$('#hotel_comission_in_percentage').val(22)")
    fill_in 'hotel_email', with: 'hotel@email.com'
    fill_in 'hotel_login', with: 'hotel@email.com'
    page.execute_script("$('#hotel_city_id').val('#{city.id}')")
    page.execute_script("$('#hotel_state').val('SP')")
    fill_in 'hotel_street', with: '123'
    fill_in 'hotel_number', with: '123'
    fill_in 'hotel_latitude', with: '123'
    fill_in 'hotel_longitude', with: '123'
    page.execute_script("$('#hotel_minimum_hours_of_notice').val(24)")
    select '5 estrelas', from: 'hotel_category'
    page.find('.cpy-submit-button').click
    Hotel.count.should == 1
  end

  scenario 'when including new hotel a new password should be sent', js:true do
    reset_email
  end

  scenario 'when including new hotel a new password should be sent', js:true do

    state = FactoryGirl.create(:state)
    city = FactoryGirl.create(:city, state: state)

    visit new_admin_hotel_path
    fill_in 'hotel_name', with: 'Hotel Teste'
    fill_in 'hotel_description_en', with: 'Descrição'
    fill_in 'hotel_description_pt_br', with: 'Descrição'
    fill_in 'hotel_payment_method_pt_br', with: 'Método'
    fill_in 'hotel_payment_method_en', with: 'Método'
    page.execute_script("$('#hotel_city_id').val('#{city.id}')")
    page.execute_script("$('#hotel_state').val('SP')")
    fill_in 'hotel_street', with: '123'
    fill_in 'hotel_number', with: '123'
    fill_in 'hotel_latitude', with: '123'
    fill_in 'hotel_longitude', with: '123'
    page.execute_script("$('#hotel_minimum_hours_of_notice').val(24)")
    select '5 estrelas', from: 'hotel_category'
    fill_in 'hotel_email', with: 'a@hotel.email.com'
    fill_in 'hotel_login', with: 'alogin'
    page.find('.cpy-submit-button').click
    sleep(10)
    last_email.to.should include "a@hotel.email.com"
  end

  scenario 'I can edit hotel informations' do
    FactoryGirl.create(:hotel)
    visit admin_hotels_path
    page.find('.cpy-edit-hotel-link').click
    fill_in 'hotel_name', with: 'Hotel 123'
    page.find('.cpy-submit-button').click
    page.should have_content("Hotel 123")
  end

  scenario 'I can see the hotel informations' do
    hotel = FactoryGirl.create(:hotel)
    visit admin_hotel_path(hotel)
    page.should have_content("#{hotel.name}")
    page.should have_content("#{hotel.login}")
    page.should have_content("#{hotel.description_pt_br}")
    page.should have_content("#{hotel.description_en}")
    page.should have_content("#{hotel.minimum_hours_of_notice} hora")
    page.should have_content("5,5%")
    page.should have_content("22,22%")
    page.should have_content("#{hotel.email}")
    page.should have_content("#{hotel.street}, #{hotel.number}")
    page.should have_content("#{hotel.city.name} - #{hotel.city.state.initials}")
    Hotel.where(id: hotel.id).update_all(minimum_hours_of_notice: -1)
    visit admin_hotel_path(hotel)
    page.should have_content('Não aceita cancelamento')
  end

  scenario 'I should see errors when including new hotels without the required information', js: true do
    visit admin_hotels_path
    page.find('.cpy-add-link').click
    page.find('.cpy-submit-button').click
    page.should have_content('Por favor, insira o nome do hotel.')
    page.should have_content('Por favor, insira o e-mail do hotel.')
    page.should have_content('Por favor, insira a descrição em português.')
    page.should have_content('Por favor, insira a descrição em inglês.')
    page.should have_content('Por favor, insira a latitude.')
    page.should have_content('Por favor, insira a longitude.')
    page.should have_content('Por favor, insira a antecedência mínima.')
    page.should have_content('Por favor, insira o método de pagamento em inglês.')
    page.should have_content('Por favor, insira o método de pagamento em português.')
  end

  scenario 'I should see errors when including new hotels with invalid information', js: true do
    visit admin_hotels_path
    page.find('.cpy-add-link').click
    fill_in 'hotel_name', with: 'Hotel Teste'
    fill_in 'hotel_description_en', with: 'Descrição'
    fill_in 'hotel_description_pt_br', with: 'Descrição'
    fill_in 'hotel_payment_method_pt_br', with: 'Método'
    fill_in 'hotel_payment_method_en', with: 'Método'
    page.execute_script("$('#hotel_iss_in_percentage').val(-1.5)")
    page.execute_script("$('#hotel_comission_in_percentage').val(250)")
    fill_in 'hotel_latitude', with: '123'
    fill_in 'hotel_longitude', with: '123'
    page.execute_script("$('#hotel_minimum_hours_of_notice').val('')")
    select '5 estrelas', from: 'hotel_category'
    page.find('.cpy-submit-button').click
    page.should have_content('Por favor, insira a antecedência mínima.')
    page.should have_content('deve ser maior ou igual a 0')
    page.should have_content('deve ser menor ou igual a 100')
  end

  context 'visiting the details page of a hotel,' do
    before(:each) do
      @hotel = FactoryGirl.create(:hotel)
    end

    context 'I want to see the room types for the hotel,' do
      scenario 'I can see the room types for the hotel' do
        room_type = FactoryGirl.create(:room_type, hotel: @hotel)
        visit admin_hotel_path(@hotel.id)
        page.should have_content(room_type.full_name_pt_br)
      end

      scenario 'I can see the details of a room type' do
        room_type = FactoryGirl.create(:room_type, hotel: @hotel)
        visit admin_hotel_path(@hotel.id)
        page.find('.cpy-room-type-show-button').click
        page.should have_content(room_type.name_pt_br)
        page.should have_content(room_type.name_en)
        page.should have_content(room_type.description_pt_br)
        page.should have_content(room_type.description_en)
      end

      scenario 'I can edit the hotel room type' do
        room_type = FactoryGirl.create(:room_type, hotel: @hotel)
        old_room_type_attributes = room_type.attributes
        visit admin_hotel_path(@hotel.id)
        page.find('.cpy-room-type-edit-button').click
        fill_in 'room_type_name_pt_br', with: 'Room type br 123'
        page.find('.cpy-submit-button').click
        room_type.reload.attributes.each do | name, value |
          next if name.to_s == 'updated_at'
          if name.to_s == 'name_pt_br'
            value.should == 'Room type br 123'
          else
            value.should eq old_room_type_attributes[name]
          end
        end
      end

    end

    context 'I want to see the pricing policies the hotel,' do
      scenario 'I can see the pricing policies for the hotel' do
        pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
        visit admin_hotel_path(@hotel.id)
        page.should have_content(pricing_policy.name)
      end

      scenario 'I can see the prices of a pricing policy of the hotel' do
        pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
        room_type = FactoryGirl.create(:room_type, hotel: @hotel)
        room_type_pricing_policy = FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
        visit admin_hotel_pricing_policy_path(hotel_id: @hotel.id, id: pricing_policy.id)
        page.should have_content(pricing_policy.name)
        Offer::ACCEPTED_LENGTH_OF_PACKS.each do | length |
          page.should have_content(number_to_currency room_type_pricing_policy.send("pack_price_#{length}h"))
        end
      end

    end

    context "I want to see the hotel's lowest offer prices for each pack," do
      scenario 'I can see the prices in the hotel details page' do
        offer_1 = FactoryGirl.create(:offer, hotel: @hotel, price: 10)
        offer_2 = FactoryGirl.create(:offer, hotel: @hotel, price: 20, room: offer_1.room, room_type: offer_1.room_type)
        visit admin_hotel_path(@hotel.id)
        page.should have_content(number_to_currency offer_1.price)
        page.should_not have_content(number_to_currency offer_2.price)
        offer_2.update(pack_in_hours: 6)
        visit admin_hotel_path(@hotel.id)
        page.should have_content(number_to_currency offer_1.price)
        page.should have_content(number_to_currency offer_1.price)
      end

      scenario 'if there is more than one offer with the lowest price, I can see those offers' do
        offer_1 = FactoryGirl.create(:offer, hotel: @hotel, price: 10)
        offer_2 = FactoryGirl.create(:offer, hotel: @hotel, price: 10, room: offer_1.room, room_type: offer_1.room_type)
        visit admin_hotel_path(@hotel.id)
        page.find('.cpy-show-offers-with-lowest-price-button').click
        page.should have_content(offer_1.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
        page.should have_content(offer_2.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      end

    end

  end
end
