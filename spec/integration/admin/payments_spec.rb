# encoding: utf-8
require 'spec_helper'

story 'As an administrator I can view payments,' do
  before(:each) do
    sign_in_admin
  end

  context 'visiting the payment list' do
    scenario 'I can list payments' do
      payment_1 = FactoryGirl.create(:payment)
      payment_2 = FactoryGirl.create(:payment)
      payment_3 = FactoryGirl.create(:payment)
      visit admin_payments_path
      page.should have_content(payment_1.user.name)
      page.should have_content(payment_2.user.name)
      page.should have_content(payment_3.user.name)
    end

    scenario 'I can filter payments' do
      payment_1 = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response])
      payment_2 = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:waiting_for_maxipago_response])
      payment_3 = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:authorized], booking: payment_2.booking)
      payment_4 = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:not_authorized], user: payment_3.user)
      payment_5 = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:captured], credit_card: payment_1.credit_card)
      visit admin_payments_path
      fill_in 'by_id', with: payment_1.id
      page.find('.cpy-filter-button').click
      page.should have_content(payment_1.user.name)
      page.should_not have_content(payment_2.user.name)
      page.should_not have_content(payment_3.user.name)
      page.should_not have_content(payment_4.user.name)
      page.should_not have_content(payment_5.user.name)
      page.all("tbody tr").count.should eql(1)
      page.find('.cpy-remove-filter-button').click
      fill_in 'by_user_id', with: payment_3.user.id
      page.find('.cpy-filter-button').click
      page.should have_content(payment_2.user.name)
      page.should have_content(payment_3.user.name)
      page.should have_content(payment_4.user.name)
      page.should_not have_content(payment_1.user.name)
      page.should_not have_content(payment_5.user.name)
      page.all("tbody tr").count.should eql(3)
      select 'Autorizado', from: 'by_status'
      page.find('.cpy-filter-button').click
      page.should have_content(payment_3.user.name)
      page.should_not have_content(payment_1.user.name)
      page.should_not have_content(payment_5.user.name)
      page.all("tbody tr").count.should eql(1)
      page.find('.cpy-remove-filter-button').click
      fill_in 'by_credit_card_id', with: payment_1.credit_card.id
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(2)
      page.should have_content(payment_1.user.name)
      page.should have_content(payment_5.user.name)
      page.find('.cpy-remove-filter-button').click
      fill_in 'by_booking_id', with: payment_2.booking.id
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(2)
      page.should have_content(payment_2.user.name)
      page.should have_content(payment_3.user.name)
      page.find('.cpy-remove-filter-button').click
      page.all("tbody tr").count.should eql(5)
    end
  end

  context 'visiting the payments details page' do
    before(:each) do
      @payment = FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:authorized])
      visit admin_payment_path(@payment.id)
    end

    scenario 'I can see the payment details' do
      page.should have_content(@payment.user.name)
      page.should have_content(I18n.l @payment.created_at)
      page.should have_content('Autorizado')
      page.should have_content(@payment.credit_card.billing_name)
      page.should have_content(@payment.credit_card.owner_cpf)
      page.should have_content(@payment.credit_card.billing_phone)
      page.should have_content(@payment.credit_card.billing_address1)
      page.should have_content(@payment.credit_card.billing_city)
      page.should have_content(@payment.credit_card.billing_state)
      page.should have_content(@payment.credit_card.billing_country)
      page.should have_content(@payment.credit_card.billing_zipcode)
      page.should have_content(I18n.t @payment.credit_card.flag_name)
      page.should have_content(@payment.credit_card.last_four_digits)
    end

    scenario "I can see a link to the payment's booking" do
      page.find('.cpy-link-to-payment-booking').click
      should_be_on(admin_booking_path(@payment.booking.id))
    end

    scenario "I can see a link to the payment's user" do
      page.find('.cpy-link-to-payment-user').click
      should_be_on(admin_user_path(@payment.user.id))
    end

    scenario "I can see a link to the payment's hotel " do
      page.find('.cpy-link-to-payment-hotel').click
      should_be_on(admin_hotel_path(@payment.booking.hotel.id))
    end
  end
end
