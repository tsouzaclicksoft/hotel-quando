# encoding: utf-8
require 'spec_helper'

story 'As an administrator I can view companies,' do
  before(:each) do
    sign_in_admin
  end

  context 'visiting the company list' do
    scenario 'I can list companies' do
      FactoryGirl.create_list(:company, 10)
      visit admin_companies_path
      Company.all.each do | company |
        page.should have_content company.email
      end
    end
  end

  context 'visiting a company details page' do
    before(:each) do
      @company = FactoryGirl.create(:company)
      @company_employees = FactoryGirl.create_list(:user, 5, company: @company)
      visit admin_company_path(@company)
    end

    scenario 'I can view the company informations' do
      page.should have_content(@company.id)
      page.should have_content(@company.name)
      page.should have_content(@company.phone_number)
      page.should have_content(@company.responsible_name)
      page.should have_content(@company.email)
      page.should have_content(@company.agency.name)
    end

    scenario 'I can view the company employees list' do
      @company_employees.each do | employee |
        page.should have_content(employee.name)
      end
    end
  end
end
