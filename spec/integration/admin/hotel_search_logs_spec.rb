# encoding: utf-8
require 'spec_helper'
include ActionView::Helpers::NumberHelper

story 'As an administrator I can see the hotel search logs,' do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    sign_in_admin
  end

  context 'visiting the hotel search logs list' do
    context 'without any search registred' do
      scenario 'I should see a message telling me that there are not any registered hotel search log' do
        visit admin_hotel_search_logs_path
        page.should have_content("Nenhuma busca foi efetuada.")
      end
    end

    scenario 'I can see the search logs list' do
      search_logs = []
      5.times do
        search_logs << FactoryGirl.create(:hotel_search_log)
      end
      visit admin_hotel_search_logs_path
      search_logs.each do | hotel_search_log |
        page.should have_content(hotel_search_log.address)
      end
    end
  end

end
