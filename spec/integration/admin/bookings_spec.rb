# encoding: utf-8
require 'spec_helper'
include ActionView::Helpers::NumberHelper

story 'As an administrator I can see bookings,' do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    sign_in_admin
  end

  context 'visiting the booking list' do
    context 'without any registered bookings' do
      scenario 'I should see a message telling me that there are not any registered bookings' do
        visit admin_bookings_path
        page.should have_content("Nenhuma reserva cadastrada.")
      end
    end

    scenario 'I can see the bookings for any hotel' do
      bookings = []
      5.times do
        bookings << FactoryGirl.create(:booking)
      end
      visit admin_bookings_path
      bookings.each do | booking |
        page.should have_content(booking.id)
        page.should have_content(booking.hotel.name)
        page.should have_content((booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute).strftime("%d/%m/%Y %H:%M"))
      end
    end

    scenario 'I can filter bookings' do
      bookings = []
      hotel = @hotel
      hotel_2 = FactoryGirl.create(:hotel)
      2.times do | i |
        offer = FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: (DateTime.now + (i + 1).days))
        bookings << FactoryGirl.create(:booking, offer: offer, room_type: offer.room_type, delayed_checkout_flag: true)
        offer = FactoryGirl.create(:offer, hotel: hotel_2, checkin_timestamp: (DateTime.now + (i + 1).days))
        bookings << FactoryGirl.create(:booking, offer: offer, room_type: offer.room_type, refund_error: true, created_by_agency: true)
      end

      2.times do | i |
        offer = FactoryGirl.create(:offer, hotel: hotel, checkin_timestamp: (DateTime.now + (i + 1).days), pack_in_hours: 6)
        bookings << FactoryGirl.create(:booking, offer: offer, room_type: offer.room_type, delayed_checkout_flag: true, created_by_agency: true)
        offer = FactoryGirl.create(:offer, hotel: hotel_2, checkin_timestamp: (DateTime.now + (i + 1).days), pack_in_hours: 6)
        bookings << FactoryGirl.create(:booking, offer: offer, room_type: offer.room_type, refund_error: true)
      end
      visit admin_bookings_path
      fill_in :by_id, with: bookings[0].id
      page.find('.cpy-filter-button').click()
      page.all("tbody tr").count.should eql(1)
      page.find('.cpy-remove-filter-button').click
      page.all("tbody tr").count.should eql(8)
      fill_in 'by_checkin_date[initial_date]', with: (DateTime.now + 1.day).strftime('%d/%m/%Y')
      fill_in 'by_checkin_date[final_date]', with: (DateTime.now + 1.day).strftime('%d/%m/%Y')
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
      page.find('.cpy-remove-filter-button').click
      page.find('.cpy-length-of-pack-6h').set(true)
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
      page.find('.cpy-remove-filter-button').click
      fill_in 'by_guest_name', with: bookings[2].guest_name
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(1)
      page.find('.cpy-remove-filter-button').click
      select hotel.name, from: 'by_hotel_id'
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
      page.find('.cpy-remove-filter-button').click
      select hotel_2.name, from: 'by_hotel_id'
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
      page.find('.cpy-remove-filter-button').click
      select 'Reservas com atraso no checkout', from: 'by_checkout_delay'
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
      page.find('.cpy-remove-filter-button').click
      select 'Reservas sem atraso no checkout', from: 'by_checkout_delay'
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
      page.find('.cpy-remove-filter-button').click
      select 'Todas', from: 'by_checkout_delay'
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(8)
      page.find('.cpy-remove-filter-button').click
      select 'Reservas com erro no reembolso', from: 'by_refund_error'
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
      page.find('.cpy-remove-filter-button').click
      select 'Reservas sem erro no reembolso', from: 'by_refund_error'
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
      page.find('.cpy-remove-filter-button').click
      select 'Todas', from: 'by_checkout_delay'
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(8)
      page.find('.cpy-agency-creation-filter').set(true)
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(4)
    end
  end

  context 'visiting the booking details page' do
    scenario 'I can see the details of a booking' do
      booking = FactoryGirl.create(:booking)
      visit admin_booking_path(booking.id)
      page.should have_content("Aguardando pagamento")
      page.should have_content(booking.guest_name)
      page.should have_content(booking.number_of_people)
      page.should have_content(booking.hotel.name)
      page.should have_content(booking.room_type.name_pt_br)
      page.should have_content(booking.room.number)
      page.should have_content("#{booking.pack_in_hours}h")
      page.should have_content(booking.offer.checkin_timestamp.strftime("%d/%m/%Y"))
      page.should have_content(booking.offer.checkin_timestamp.strftime("%H:%M"))
      page.should have_content(booking.offer.checkout_timestamp.strftime("%d/%m/%Y"))
      page.should have_content(booking.offer.checkout_timestamp.strftime("%H:%M"))
      page.should have_content(booking.user.name)
      page.should have_content(booking.user.email)
      page.should have_content(I18n.l booking.created_at)
      page.should have_content(number_to_currency(booking.offer.price))
      page.should have_content(number_to_currency(booking.offer.extra_price_per_person * (booking.number_of_people - 1)))
      page.should have_content(number_to_currency((booking.offer.extra_price_per_person * (booking.number_of_people - 1)) + booking.offer.price))
    end
  end

end
