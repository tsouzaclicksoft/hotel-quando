# encoding: utf-8
require 'spec_helper'
story 'As an administrator I can manage agencies,' do
  before(:each) do
    sign_in_admin
  end

  context 'visiting the agency index' do
    scenario 'I can list agencies' do
      FactoryGirl.create_list(:agency, 5)
      visit admin_agencies_path
      Agency.all.each do | agency |
        page.should have_content(agency.name)
      end
    end

    scenario 'I should see a message if there are no Agencies registered' do
      visit admin_agencies_path
      page.should have_content('Nenhuma agência cadastrada')
    end

    scenario 'I can deactivate agencies' do
      agency = FactoryGirl.create(:agency)
      visit admin_agencies_path
      page.find('.cpy-deactivate-agency-button').click
      agency.reload.is_active?.should == false
    end

    scenario 'I can activate agencies' do
      agency = FactoryGirl.create(:agency, is_active: false)
      agency.is_active?.should == false
      visit admin_agencies_path
      page.find('.cpy-activate-agency-button').click
      agency.reload.is_active?.should == true
    end
  end

  scenario 'I can include new agencies' do
    visit admin_agencies_path
    page.find('.cpy-add-link').click
    fill_in 'agency_name', with: 'Agência Teste'
    fill_in 'agency_email', with: 'agencia@email.com'
    fill_in 'agency_observations', with: 'Observações'
    page.find('.cpy-submit-button').click
    Agency.count.should == 1
  end

  scenario 'when I include a new agency a password should be sent' do
    visit new_admin_agency_path
    fill_in 'agency_name', with: 'Agência Teste'
    fill_in 'agency_email', with: 'agencia@email.com'
    fill_in 'agency_observations', with: 'Observações'
    page.find('.cpy-submit-button').click
    Agency.count.should == 1
    last_email.to.should include "agencia@email.com"
  end

  scenario 'I can edit agency informations' do
    agency = FactoryGirl.create(:agency)
    visit edit_admin_agency_path(agency.id)
    fill_in 'agency_name', with: 'Agencia 123'
    page.find('.cpy-submit-button').click
    agency.reload.name.should == 'Agencia 123'
  end

  scenario 'I should see error messages when including new agencies with invalid information' do
    visit admin_agencies_path
    page.find('.cpy-add-link').click
    fill_in 'agency_name', with: ''
    page.find('.cpy-submit-button').click
    page.should have_css('.agency_name.has-error')
  end

  context 'visiting the agency show' do
    before(:each) do
      @agency = FactoryGirl.create(:agency, comission_in_percentage: 12.00)
      visit admin_agency_path(@agency)
    end

    scenario 'I can see the agency informations' do
      page.should have_content("#{@agency.name}")
      page.should have_content("#{@agency.observations}")
      page.should have_content("#{@agency.email}")
      page.should have_content('12,00%')
    end

    scenario 'I can deactivate the agency' do
      page.find('.cpy-deactivate-agency-button').click
      @agency.reload.is_active?.should == false
    end


    scenario 'I can activate agencies' do

      @agency.is_active = false
      @agency.save!
      visit admin_agency_path(@agency)
      page.find('.cpy-activate-agency-button').click
      @agency.reload.is_active?.should == true
    end
  end
end
