# encoding: utf-8
require 'spec_helper'

story 'As an administrator I can manage offers,' do
  before(:each) do
    sign_in_admin
  end

  scenario 'I should see a message if there is no offer as result' do
    visit admin_offers_path
    page.should have_content("Não existem ofertas que batam com o critério de busca aplicado.")
  end

  context 'visiting offer index' do
    before (:each) do

      @hotel = FactoryGirl.create(:hotel)
      @room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      @offers_log  = FactoryGirl.create(:offers_log, hotel: @hotel)
      @room_type_2 = FactoryGirl.create(:room_type, hotel: @hotel)
      @room_type_3 = FactoryGirl.create(:room_type, hotel: @hotel)
      @room_type_4 = FactoryGirl.create(:room_type, hotel: @hotel)
      @hotel2 = FactoryGirl.create(:hotel)
      @room_type22 = FactoryGirl.create(:room_type, hotel: @hotel2)
      @offers_log2  = FactoryGirl.create(:offers_log, hotel: @hotel2)

      FactoryGirl.create(:offer, pack_in_hours: 3,checkin_timestamp: 1.day.from_now, status: Offer::ACCEPTED_STATUS[:available], hotel: @hotel, room_type: @room_type_2, log_id: @offers_log.id )
      FactoryGirl.create(:offer, pack_in_hours: 3,checkin_timestamp: 1.day.from_now, status: Offer::ACCEPTED_STATUS[:available], hotel: @hotel2, room_type: @room_type22, log_id: @offers_log2.id )
      FactoryGirl.create(:offer, pack_in_hours: 6,checkin_timestamp: 1.day.from_now, status: Offer::ACCEPTED_STATUS[:available], hotel: @hotel, room_type: @room_type_3, log_id: @offers_log.id )
      FactoryGirl.create(:offer, pack_in_hours: 12,checkin_timestamp: 1.day.from_now, status: Offer::ACCEPTED_STATUS[:available], hotel: @hotel, room_type: @room_type_4, log_id: @offers_log.id )

      FactoryGirl.create(:offer, pack_in_hours: 12,checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved], hotel: @hotel, room_type: @room_type_4, log_id: @offers_log.id )
      FactoryGirl.create(:offer, pack_in_hours: 9,checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved], hotel: @hotel2, room_type: @room_type22, log_id: @offers_log2.id )
      FactoryGirl.create(:offer, pack_in_hours: 9,checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:suspended], hotel: @hotel, room_type: @room_type_2, log_id: @offers_log.id )
      FactoryGirl.create(:offer, pack_in_hours: 12,checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:expired], hotel: @hotel, room_type: @room_type_3, log_id: @offers_log.id )

      FactoryGirl.create(:offer, pack_in_hours: 12,checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:ghost], hotel: @hotel, room_type: @room_type_2, log_id: @offers_log.id )

      visit admin_offers_path
    end

    scenario 'I should see all offers that are: available, suspended or reserved' do
      page.all('tbody tr').size.should == 7
    end

    scenario 'I can filter offers by checkin range' do
      page.find("input[name='by_checkin_range[initial_date]']").set("#{Date.today.strftime("%d/%m/%Y")}")
      page.find("input[name='by_checkin_range[final_date]']").set("#{Date.today.next_day(5).strftime("%d/%m/%Y")}")
      page.find('input.btn-success').click()

      page.all('tbody tr').size.should == 4
    end

    scenario 'I can filter offers by checkin range and statuses' do
      page.find("input[name='by_checkin_range[initial_date]']").set("#{Date.today.strftime("%d/%m/%Y")}")
      page.find("input[name='by_checkin_range[final_date]']").set("#{Date.today.next_day(12).strftime("%d/%m/%Y")}")
      page.find("input.cpy-status-#{Offer::ACCEPTED_STATUS[:reserved]}").set(true) # 2 offers
      page.find("input.cpy-status-#{Offer::ACCEPTED_STATUS[:available]}").set(true) # 4 offers
      page.find('input.btn-success').click()

      page.all('tbody tr').size.should == 6
    end


    scenario 'I can filter offers by pack size' do
      page.find("input.cpy-pack_in_hour-3").set(true) # 2 offers
      page.find("input.cpy-pack_in_hour-6").set(true) # 4 offers
      page.find('input.btn-success').click()
      page.all('tbody tr').size.should == 3
    end


    scenario 'I can filter offers by hotel' do
      select @hotel.name, from: 'by_hotel_id'
      page.find('input.btn-success').click()
      page.all('tbody tr').size.should == 5
      select @hotel2.name, from: 'by_hotel_id'
      page.find('input.btn-success').click()
      page.all('tbody tr').size.should == 2
    end

  end

end
