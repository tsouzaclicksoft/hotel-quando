# encoding: utf-8
require 'spec_helper'

story 'As an hotel administrator I can manage room types,' do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    sign_in_hotel(@hotel)
  end

  scenario 'I can list room types' do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    visit hotel_room_types_path
    page.should have_content("#{room_type.name_pt_br}")
  end

  scenario 'I should see a message if there are no room types registered' do
    visit hotel_room_types_path
    page.should have_content('Nenhum tipo de quarto cadastrado')
  end

  scenario 'I can include new room types' do
    visit hotel_room_types_path
    page.find('.cpy-add-link').click
    fill_in 'room_type_name_pt_br', with: 'Room type teste'
    fill_in 'room_type_name_en', with: 'Room type teste'
    fill_in 'room_type_description_pt_br', with: 'Descrição'
    fill_in 'room_type_description_en', with: 'Descrição'
    select '15', from: 'room_type_maximum_capacity'
    select '0', from: 'room_type_single_bed_quantity'
    select '2', from: 'room_type_double_bed_quantity'
    page.find('.cpy-submit-button').click
    page.should have_content('Room type teste')
    visit hotel_room_types_path
    page.should have_content('Room type teste')
  end

  scenario 'I can edit room type informations' do
    FactoryGirl.create(:room_type, hotel: @hotel)
    visit hotel_room_types_path
    page.find('.cpy-edit-button').click
    fill_in 'room_type_name_pt_br', with: 'Room type 123'
    page.find('.cpy-submit-button').click
    page.should have_content("Room type 123")
    visit hotel_room_types_path
    page.should have_content("Room type 123")
  end

  scenario 'I should see errors when including new room types without the required information' do
    visit hotel_room_types_path
    page.find('.cpy-add-link').click
    page.find('.cpy-submit-button').click
    page.should have_content('Por favor, insira o nome do tipo de quarto em português.')
    page.should_not have_content('Por favor, insira o nome do tipo de quarto em inglês.')
    page.should_not have_content('Por favor, insira a descrição em português.')
    page.should_not have_content('Por favor, insira a descrição em inglês.')
    page.should have_content('O tipo de quarto deve ter pelo menos uma cama.')
  end

  scenario 'I can include new room types and rooms at the same time, including the room numbers myself', js: true do
    visit hotel_room_types_path
    page.find('.cpy-add-link').click
    fill_in 'room_type_name_pt_br', with: 'Room type teste'
    fill_in 'room_type_name_en', with: 'Room type teste'
    fill_in 'room_type_description_pt_br', with: 'Descrição'
    fill_in 'room_type_description_en', with: 'Descrição'
    select '15', from: 'room_type_maximum_capacity'
    select '2', from: 'room_type_single_bed_quantity'
    select '0', from: 'room_type_double_bed_quantity'
    page.find('.cpy-change-room-registration-method').click
    fill_in 'room_numbers', with: '1, 2,3,  4,5,5a'
    page.find('.cpy-submit-button').click
    visit hotel_rooms_path
    page.should have_content '1'
    page.should have_content '2'
    page.should have_content '3'
    page.should have_content '4'
    page.should have_content '5'
    page.should have_content '5a'
  end

  scenario 'I can include new room types and rooms at the same time, selecting the quantity of rooms to include', js: true do
    visit hotel_room_types_path
    page.find('.cpy-add-link').click
    fill_in 'room_type_name_pt_br', with: 'Room type teste'
    fill_in 'room_type_name_en', with: 'Room type teste'
    fill_in 'room_type_description_pt_br', with: 'Descrição'
    fill_in 'room_type_description_en', with: 'Descrição'
    select '15', from: 'room_type_maximum_capacity'
    select '2', from: 'room_type_single_bed_quantity'
    select '0', from: 'room_type_double_bed_quantity'
    select '5', from: 'room_quantity'
    page.find('.cpy-submit-button').click
    visit hotel_rooms_path
    page.should have_content '1'
    page.should have_content '2'
    page.should have_content '3'
    page.should have_content '4'
    page.should have_content '5'
  end

  scenario "I shouldn't see other hotel's room types" do
    other_hotel_room_type = FactoryGirl.create(:room_type)
    visit hotel_room_types_path
    page.should_not have_content other_hotel_room_type.name_pt_br
  end
end
