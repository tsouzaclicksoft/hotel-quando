# encoding: utf-8
require 'spec_helper'

story 'As an hotel administrator I can manage offers,' do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    @room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    sign_in_hotel(@hotel)
  end

  context 'visiting offer list' do
    before (:each) do
      @offers_log  = FactoryGirl.create(:offers_log, hotel: @hotel)
      @room_type_2 = FactoryGirl.create(:room_type, hotel: @hotel)
      @room_type_3 = FactoryGirl.create(:room_type, hotel: @hotel)
      @room_type_4 = FactoryGirl.create(:room_type, hotel: @hotel)

      FactoryGirl.create(:offer, checkin_timestamp: 1.day.from_now, status: Offer::ACCEPTED_STATUS[:available], hotel: @hotel, room_type: @room_type_2, log_id: @offers_log.id )
      FactoryGirl.create(:offer, checkin_timestamp: 1.day.from_now, status: Offer::ACCEPTED_STATUS[:available], hotel: @hotel, room_type: @room_type_3, log_id: @offers_log.id )
      FactoryGirl.create(:offer, checkin_timestamp: 1.day.from_now, status: Offer::ACCEPTED_STATUS[:available], hotel: @hotel, room_type: @room_type_3, log_id: @offers_log.id )
      FactoryGirl.create(:offer, checkin_timestamp: 1.day.from_now, status: Offer::ACCEPTED_STATUS[:available], hotel: @hotel, room_type: @room_type_4, log_id: @offers_log.id )

      FactoryGirl.create(:offer, checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved], hotel: @hotel, room_type: @room_type_4, log_id: @offers_log.id )
      FactoryGirl.create(:offer, checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:reserved], hotel: @hotel, room_type: @room_type_4, log_id: @offers_log.id )
      FactoryGirl.create(:offer, checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:suspended], hotel: @hotel, room_type: @room_type_2, log_id: @offers_log.id )
      FactoryGirl.create(:offer, checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:expired], hotel: @hotel, room_type: @room_type_3, log_id: @offers_log.id )

      FactoryGirl.create(:offer, checkin_timestamp: 10.day.from_now, status: Offer::ACCEPTED_STATUS[:ghost], hotel: @hotel, room_type: @room_type_2, log_id: @offers_log.id )

      visit show_all_hotel_offers_path
    end

    scenario 'I should see all offers in first entry' do
      page.all('tbody td.center').size.should == 9*5 # (9 offers and 5 columns)
    end

    scenario 'I can filter offers by checkin range and room types' do
      page.find("input[name='by_checkin_range[initial_date]']").set("#{Date.today.strftime("%d/%m/%Y")}")
      page.find("input[name='by_checkin_range[final_date]']").set("#{Date.today.next_day(5).strftime("%d/%m/%Y")}")
      page.find("input.cpy-room-type-#{@room_type_2.id}").set(true)
      page.find("input.cpy-room-type-#{@room_type_4.id}").set(true)
      page.find('input.btn-success').click()

      page.all('tbody td.center').size.should == 2*5 # (2 offers and 5 columns)
    end

    scenario 'I can filter offers by checkin range and statuses' do
      page.find("input[name='by_checkin_range[initial_date]']").set("#{Date.today.strftime("%d/%m/%Y")}")
      page.find("input[name='by_checkin_range[final_date]']").set("#{Date.today.next_day(12).strftime("%d/%m/%Y")}")
      page.find("input.cpy-status-#{Offer::ACCEPTED_STATUS[:reserved]}").set(true) # 2 offers
      page.find("input.cpy-status-#{Offer::ACCEPTED_STATUS[:available]}").set(true) # 4 offers
      page.find('input.btn-success').click()

      page.all('tbody td.center').size.should == 6*5 # (4 offers and 5 columns)
    end

    scenario 'I can filter offers by room_type and statuses' do
      page.find("input.cpy-room-type-#{@room_type_3.id}").set(true)
      page.find("input.cpy-status-#{Offer::ACCEPTED_STATUS[:available]}").set(true) # get 2
      page.find("input.cpy-status-#{Offer::ACCEPTED_STATUS[:suspended]}").set(true) # get 0
      page.find('input.btn-success').click()

      page.all('tbody td.center').size.should == 2*5 # (3 offers and 5 columns)
    end

  end

  context 'visiting offer creation form' do
    scenario 'I can create new offers', js: true do

      FactoryGirl.create(:room, room_type: @room_type, hotel: @hotel)
      FactoryGirl.create(:room_type_pricing_policy, room_type: @room_type, pricing_policy: FactoryGirl.create(:pricing_policy, hotel: @hotel))
      expect(CreateOffersWorker).to receive(:perform_async).once
      visit new_hotel_offer_path

      page.find("input[name='initial_date']").set("20/11/2020")
      page.find("input[name='final_date']").set("27/11/2020")
      page.find('.cpy-monday').click
      page.find('.button-to-rooms-tab').click
      page.find('.button-to-pack-selection-tab').click
      page.find('.cpy-pack-3-hours-checkin-02-hours').click
      page.find('.cpy-submit-button').click
      # Because of a bug in capybara, this is the only way to ensure that the button clicking will be succesfull
      begin
        200.times do
          page.find('.modal button.btn-success').click
        end
      rescue
      end
      current_path.should == hotel_offers_path
      page.should have_content('A criação das ofertas está sendo processada. Consulte a seção de relatórios para visualizar os processamentos.')

    end

    scenario 'I should see only one alert if there are no rooms registered, but there are pricing policies' do
      FactoryGirl.create(:room_type_pricing_policy, room_type: @room_type, pricing_policy: FactoryGirl.create(:pricing_policy, hotel: @hotel))
      visit new_hotel_offer_path
      page.should_not have_content('Adicionando ofertas')
      page.should_not have_content('Nenhuma tabela de tarifas cadastrada')
      page.should have_content('Nenhum quarto cadastrado')
    end

    scenario 'I should see only one alert if there are no pricing policies registered, but there are rooms' do
      FactoryGirl.create(:room, room_type: @room_type, hotel: @hotel)
      visit new_hotel_offer_path
      page.should_not have_content('Adicionando ofertas')
      page.should_not have_content('Nenhum quarto cadastrado')
      page.should have_content('Nenhuma tabela de tarifas cadastrada')
    end

    scenario 'I should see both alerts if there are neither rooms nor pricing policies registered' do
      visit new_hotel_offer_path
      page.should_not have_content('Adicionando ofertas')
      page.should have_content('Nenhum quarto cadastrado')
      page.should have_content('Nenhuma tabela de tarifas cadastrada')
    end
  end

  context 'visiting offer removal form' do
    scenario 'I can remove offers', js: true do
      offer = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type)
      offer2 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, room: offer.room)
      offer3 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type)
      offer4 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, room: offer.room, status: Offer::ACCEPTED_STATUS[:suspended])
      offer5 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, room: offer.room, status: Offer::ACCEPTED_STATUS[:reserved])

      expect(ProcessOffersRemovalWorker).to receive(:perform_async).once
      visit remove_hotel_offers_path

      fill_in 'initial_date', with: ((offer.checkin_timestamp.to_date).strftime '%d/%m/%Y')
      fill_in 'final_date', with: ((offer.checkin_timestamp.to_date + 2.days).strftime '%d/%m/%Y')
      page.find('.button-to-rooms-tab').click
      page.should have_content("2 Quartos com ofertas")
      page.find('.cpy-room-type').click
      page.first('.cpy-room').click
      page.find('.button-to-pack-selection-tab').click
      page.find('.cpy-submit-button').click
      # Because of a bug in capybara, this is the only way to ensure that the button clicking will be succesfull
      begin
        200.times do
          page.find('.modal button.btn-success').click
        end
      rescue
      end

      page.find('.cpy-submit-button').click

      begin
        200.times do
          page.find('.modal button.btn-success').click
        end
      rescue
      end
    end

    scenario 'I can see the intermediate step', js: true do
      offer = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type)
      offer2 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, room: offer.room)
      offer3 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, room: offer.room, status: Offer::ACCEPTED_STATUS[:reserved])
      visit remove_hotel_offers_path
      fill_in 'initial_date', with: ((offer.checkin_timestamp.to_date).strftime '%d/%m/%Y')
      fill_in 'final_date', with: ((offer.checkin_timestamp.to_date + 2.days).strftime '%d/%m/%Y')
      page.find('.button-to-rooms-tab').click
      page.find('.cpy-room-type').click
      page.find('.cpy-room').click
      page.find('.button-to-pack-selection-tab').click
      page.find('.cpy-submit-button').click

      # Because of a bug in capybara, this is the only way to ensure that the button clicking will be succesfull
      begin
        200.times do
          page.find('.modal button.btn-success').click
        end
      rescue
      end

      page.should have_content '2 ofertas serão removidas;'
      page.should have_content '1 oferta não poderá ser removida.'
      page.should have_content offer3.checkin_timestamp.strftime '%d/%m/%Y - %H:%M:%S'
    end
  end
end
