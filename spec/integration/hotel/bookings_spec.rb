# encoding: utf-8
require 'spec_helper'
include ActionView::Helpers::NumberHelper

RSpec.configure do |c|
  c.filter_run_excluding :broken => true
end

story 'As an hotel administrator I can see bookings,', :broken => true do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    sign_in_hotel(@hotel)
  end

  context 'visiting the booking list' do
    context 'without any registered bookings for my hotel' do
      scenario 'I should see a message telling me that I dont have any bookings' do
        visit hotel_bookings_path
        page.should have_content("Seu hotel ainda não possui nenhuma reserva através do nosso sistema.")
      end
    end

    scenario 'I can see the bookings for my hotel' do
      bookings = []
      5.times do
        room_type = FactoryGirl.create(:room_type, hotel: @hotel)
        bookings << FactoryGirl.create(:booking, room_type: room_type)
      end
      @hotel.bookings.count.should == 5
      visit hotel_bookings_path
      bookings.each do | booking |
        page.should have_content(booking.id)
        page.should have_content((booking.date_interval.first + Offer::ROOM_CLEANING_TIME - 1.minute).strftime("%d/%m/%Y %H:%M"))
      end
    end

    scenario 'I can filter bookings' do
      bookings = []
      2.times do | i |
        room_type = FactoryGirl.create(:room_type, hotel: @hotel)
        offer = FactoryGirl.create(:offer, room_type: room_type, hotel: @hotel, checkin_timestamp: (DateTime.now + (i + 1).days))
        bookings << FactoryGirl.create(:booking, room_type: room_type, offer: offer)
      end
      2.times do | i |
        room_type = FactoryGirl.create(:room_type, hotel: @hotel)
        offer = FactoryGirl.create(:offer, room_type: room_type, hotel: @hotel, checkin_timestamp: (DateTime.now + (i + 1).days), pack_in_hours: 6)
        bookings << FactoryGirl.create(:booking, room_type: room_type, offer: offer)
      end
      visit hotel_bookings_path
      fill_in :by_id, with: bookings[0].id
      page.find('.cpy-filter-button').click()
      page.all("tbody tr").count.should eql(1)
      page.find('.cpy-remove-filter-button').click
      page.all("tbody tr").count.should eql(4)
      fill_in 'by_checkin_date[initial_date]', with: (DateTime.now + 1.day).strftime('%d/%m/%Y')
      fill_in 'by_checkin_date[final_date]', with: (DateTime.now + 1.day).strftime('%d/%m/%Y')
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(2)
      page.find('.cpy-remove-filter-button').click
      fill_in 'by_room_numbers', with: "#{bookings[0].room.number}       ,,, #{bookings[3].room.number}"
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(2)
      page.find('.cpy-remove-filter-button').click
      page.find('.cpy-length-of-pack-6h').set(true)
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(2)
      page.find('.cpy-remove-filter-button').click
      page.find(".cpy-room-type-#{bookings[0].room_type_id}").set(true)
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(1)
      page.find('.cpy-remove-filter-button').click
      fill_in 'by_guest_name', with: bookings[2].guest_name
      page.find('.cpy-filter-button').click
      page.all("tbody tr").count.should eql(1)
    end
  end

  context 'visiting the booking details page' do
    scenario 'I can see the details of a booking' do
      room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      booking = FactoryGirl.create(:booking, room_type: room_type)
      visit hotel_booking_path(booking.id)
      page.should have_content("Aguardando pagamento")
      page.should have_content(booking.guest_name)
      page.should have_content(booking.number_of_people)
      page.should have_content(booking.room_type.name_pt_br)
      page.should have_content(booking.room.number)
      page.should have_content("#{booking.pack_in_hours}h")
      page.should have_content(booking.offer.checkin_timestamp.strftime("%d/%m/%Y"))
      page.should have_content(booking.offer.checkin_timestamp.strftime("%H:%M"))
      page.should have_content(booking.offer.checkout_timestamp.strftime("%d/%m/%Y"))
      page.should have_content(booking.offer.checkout_timestamp.strftime("%H:%M"))
      page.should have_content(booking.note)
      page.should have_content(booking.user.name)
      page.should have_content(booking.user.email)
      page.should have_content(I18n.l booking.created_at)
      page.should have_content(number_to_currency(booking.offer.price))
      page.should have_content(number_to_currency(booking.offer.extra_price_per_person * (booking.number_of_people - 1)))
      page.should have_content(number_to_currency((booking.offer.extra_price_per_person * (booking.number_of_people - 1)) + booking.offer.price))

    end

    scenario "I should not see the no-show button if its not available", noshow: true do
      room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      offer = FactoryGirl.create(:offer, hotel: @hotel, room_type: room_type, checkin_timestamp: DateTime.now.utc + 45.hours)
      booking = FactoryGirl.create(:booking, room_type: room_type, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed])
      visit hotel_booking_path(booking)

      page.should have_no_css(".cpy-set-as-no-show-button")
    end

    context 'I can set a booking as no-show', js: true do
      context 'maxipago was enabled in booking creation time' do
        context 'the user decided to pay at the hotel' do
          before(:each) do
            room_type = FactoryGirl.create(:room_type, hotel: @hotel)
            offer = FactoryGirl.create(:offer, hotel: @hotel, room_type: room_type, checkin_timestamp: DateTime.now.utc - 25.hours)
            @booking = FactoryGirl.create(:booking, room_type: room_type, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed])
            FactoryGirl.create(:payment, status: Payment::ACCEPTED_STATUSES[:authorized])
          end


          context "with successful payment" do
            before(:each) do
              body = %Q{
                <?xml version="1.0" encoding="UTF-8"?>
                <transaction-response>
                  <errorCode>0</errorCode>
                  <orderID>1289</orderID>
                  <responseCode>0</responseCode>
                </transaction-response>
               }
              FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                    body: body,
                                    status: ["200", "OK"] )
              @mailer_mock = double()
              @mailer_mock.should_receive(:deliver)
              visit hotel_booking_path(@booking)
            end

            scenario 'page should have the success message' do
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end
              page.should have_content("A cobrança do No-Show foi efetuada com sucesso. Em alguns instantes será mandado um e-mail para o usuário notificando-o sobre o No-Show e o pagamento.")
            end

            scenario 'booking status should change to no show with succesfull payment' do
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end
              @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
            end

            scenario 'I should receive an email about the successful payment' do
              HotelMailer.should_receive(:noshow_successfull_charged_notice).and_return(@mailer_mock)
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end
            end

            scenario 'User should receive an email about the no-show payment' do
              UserMailer.should_receive(:no_show_with_charge_successfull_notice).and_return(@mailer_mock)
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end
            end
          end

          context "with payment error", focus: true do
            before(:each) do
              body = %Q{
                <?xml version="1.0" encoding="UTF-8"?>
                <transaction-response>
                  <orderID>1289</orderID>
                  <responseCode>1</responseCode>
                </transaction-response>
               }
              FakeWeb.register_uri( :post, Maxipago::RequestBuilder::TransactionRequest::URL,
                                    body: body,
                                    status: ["200", "OK"] )
              @mailer_mock = double()
              @mailer_mock.should_receive(:deliver)
              visit hotel_booking_path(@booking)
            end

            scenario 'page should have an error message' do
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end

              page.should have_content("Houve um erro na cobrança do No-Show, favor verificar a situação do cliente quanto ao pagamento. Em alguns instantes será mandado um e-mail para o usuário notificando-o sobre o No-Show e o erro no pagamento.")
            end

            scenario 'booking status should change to no show with payment error' do
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end
              @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:no_show_with_payment_error]
            end

            scenario 'I should receive an email about the failed payment' do
              HotelMailer.should_receive(:no_show_with_charge_error_notice).and_return(@mailer_mock)
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end
            end

            scenario 'User should receive an email about the failed payment' do
              UserMailer.should_receive(:no_show_with_charge_error_notice).and_return(@mailer_mock)
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end
            end

            scenario 'Admin should receive an email about the failed payment' do
              AdminMailer.should_receive(:no_show_with_charge_error_notice).and_return(@mailer_mock)
              page.find(".cpy-set-as-no-show-button").click
              page.find('.bootbox button.btn-success').click
              wait_for_ajax_end
            end
          end
        end

        context 'the user decided to pay online' do
          before(:each) do
            # Mocking the maxipago inteface to ensure that the user will not be charged again
            stub_const("MaxiPagoInterface", double())
            room_type = FactoryGirl.create(:room_type, hotel: @hotel)
            offer = FactoryGirl.create(:offer, hotel: @hotel, room_type: room_type, checkin_timestamp: DateTime.now.utc - 25.hours)
            @booking = FactoryGirl.create(:booking, room_type: room_type, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
            @payment = FactoryGirl.create(:payment, booking: @booking, status: Payment::ACCEPTED_STATUSES[:captured])
          end

          scenario 'the booking status should change to no-show' do
            visit hotel_booking_path(@booking)
            page.find(".cpy-set-as-no-show-button").click
            page.find('.bootbox button.btn-success').click
            wait_for_ajax_end
            @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:no_show_paid_with_success]
            page.should have_content("A cobrança do No-Show foi efetuada com sucesso. Em alguns instantes será mandado um e-mail para o usuário notificando-o sobre o No-Show e o pagamento.")
          end

        end
      end

      context 'maxipago was disabled at booking creation time', f:true do
        before(:each) do
          stub_const("MaxiPagoInterface", double())
          room_type = FactoryGirl.create(:room_type, hotel: @hotel)
          offer = FactoryGirl.create(:offer, hotel: @hotel, room_type: room_type, checkin_timestamp: DateTime.now.utc - 25.hours)
          @booking = FactoryGirl.create(:booking, room_type: room_type, offer: offer, status: Booking::ACCEPTED_STATUSES[:confirmed])
          # zero payments
          @mailer_mock = double()
          @mailer_mock.should_receive(:deliver)
          visit hotel_booking_path(@booking)
        end

        scenario "admin should receive an email telling about the no show" do
          AdminMailer.should_receive(:no_show_pending_charge_notice).and_return(@mailer_mock)
          page.find(".cpy-set-as-no-show-button").click
        end

        scenario "user should receive an email telling about the no show" do
          HotelMailer.should_receive(:no_show_pending_charge_notice).and_return(@mailer_mock)
          page.find(".cpy-set-as-no-show-button").click
        end

        scenario "hotel should receive an email telling about the no show" do
          UserMailer.should_receive(:no_show_pending_charge_notice).and_return(@mailer_mock)
          page.find(".cpy-set-as-no-show-button").click
        end

        scenario "I should see a success message" do
          page.find(".cpy-set-as-no-show-button").click
          ensure_clicking_on_element_that_hides_after_click('.bootbox button.btn-success')
          wait_for_ajax_end
          page.should have_content("A cobrança do No-Show será efetuada pelo HotelQuando. Em alguns instantes será enviado um e-mail para o usuário notificando-o sobre o No-Show e o pagamento.")
        end

        scenario 'the booking status should change to no show payment pending' do
          page.find(".cpy-set-as-no-show-button").click
          ensure_clicking_on_element_that_hides_after_click('.bootbox button.btn-success')
          @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:no_show_with_payment_pending]
        end

      end

    end

  end

end
