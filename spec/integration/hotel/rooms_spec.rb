# encoding: utf-8
require 'spec_helper'

story 'As an hotel administrator I can manage rooms,' do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel)
    sign_in_hotel(@hotel)
  end

  scenario 'I can list rooms' do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    5.times do
      FactoryGirl.create(:room, room_type: room_type, hotel: @hotel)
    end
    visit hotel_rooms_path
    5.times do |i|
      page.should have_content("#{i + 1}")
    end
  end

  scenario 'room creation should not be visible if there are no room types registered' do
    visit hotel_rooms_path
    page.should have_content('Nenhum tipo de quarto cadastrado, cadastre um tipo de quarto primeiro para poder cadastrar quartos.')
    page.should_not have_css('.cpy-submit-button')
  end

  scenario 'I should see a message if there are no rooms registered' do
    visit hotel_rooms_path
    page.should have_content('Nenhum quarto cadastrado')
  end

  scenario 'filter should not be enabled if there are no rooms registered' do
    visit hotel_rooms_path
    page.find('.cpy-filter-button')['disabled'].should == 'disabled'
  end

  scenario 'I should see rooms paginated' do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    rooms_array = []
    50.times do | i |
      rooms_array[i] = FactoryGirl.create(:room, room_type: room_type, hotel: @hotel)
    end

    visit hotel_rooms_path

    page.should have_css('.cpy-pagination-links')

  end
  context 'I can include new rooms,' do
    before(:each) do
      room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      visit hotel_rooms_path
      select "#{room_type.name_pt_br}", from: 'room_type'
    end

    scenario 'selecting the quantity from the dropdown', js: true do
      select 10, from: 'room_quantity'
      page.find('.cpy-submit-button').click
      10.times do |i|
        page.should have_content("#{i + 1}")
      end
    end

    scenario 'including the room numbers myself', js: true do
      page.find('.cpy-change-room-registration-method').click
      fill_in 'room_numbers', with: '1, 2, 3, 4, 5, 6, 7, 8, 9, 10'
      page.find('.cpy-submit-button').click
      10.times do |i|
        page.should have_content("#{i + 1}")
      end
    end

  end

  # user cannot disable rooms anymore
  # context 'I can disable rooms', js:true do
  #   scenario "With no reserved offers", error: true do
  #     @policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
  #     @room_type = FactoryGirl.create(:room_type, hotel: @hotel)
  #     @room = FactoryGirl.create(:room, room_type: @room_type, hotel: @hotel)
  #     @room_type_policy = FactoryGirl.create(:room_type_pricing_policy, pricing_policy: @policy, room_type: @room_type)
  #     FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 3.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
  #     FactoryGirl.create(:offer, pack_in_hours: 9, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 6.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
  #     FactoryGirl.create(:offer, pack_in_hours: 9, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 7.days.ago, room: @room, room_type: @room_type, hotel: @hotel)
  #     FactoryGirl.create(:offer, pack_in_hours: 3, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 5.days.from_now, room: @room, room_type: @room_type, hotel: @hotel)
  #     FactoryGirl.create(:offer, pack_in_hours: 6, status: Offer::ACCEPTED_STATUS[:available], checkin_timestamp: 5.days.from_now, room: @room, room_type: @room_type, hotel: @hotel)

  #     room_type = FactoryGirl.create(:room_type, hotel: @hotel)
  #     room = FactoryGirl.create(:room, room_type: room_type, hotel: @hotel)
  #     visit hotel_rooms_path
  #     page.should have_content("Ativo")
  #     page.find(".js-deactivate-room-#{room.id}").click

  #     # Because of a bug in capybara, this is the only way to ensure that the button clicking will be succesfull
  #     begin
  #       200.times do
  #         page.find('.modal button.btn-success').click
  #       end
  #     rescue
  #     end
  #     wait_for_element_to_be_visible(".cpy-room-changed", 60.seconds)
  #     page.should have_content("Inativo")
  #   end

  #   pending "With reserved offers"
  # end

  # user cannot enable rooms anymore
  # scenario 'I can enable rooms (working alone, but not with other specs)', js: true, error: true do
  #   room_type = FactoryGirl.create(:room_type, hotel: @hotel)
  #   room = FactoryGirl.create(:room, is_active: false,room_type: room_type, hotel: @hotel)
  #   visit hotel_rooms_path
  #   page.should have_content("Inativo")
  #   page.find(".js-activate-room-#{room.id}").click

  #   # Because of a bug in capybara, this is the only way to ensure that the button clicking will be succesfull
  #   begin
  #     200.times do
  #       page.find('.modal button.btn-success').click
  #     end
  #   rescue
  #   end
  #   wait_for_element_to_be_visible(".cpy-room-changed", 60.seconds)
  #   page.should have_content("Ativo")
  # end

  scenario 'I can filter rooms by room type' do
    room_type1 = FactoryGirl.create(:room_type, hotel: @hotel)
    room_type2 = FactoryGirl.create(:room_type, hotel: @hotel)
    rooms_array = []
    5.times do |i|
      rooms_array[i] = FactoryGirl.create(:room, room_type: room_type1, hotel: @hotel)
    end
    5.times do |i|
      rooms_array[i + 5] = FactoryGirl.create(:room, room_type: room_type2, hotel: @hotel)
    end

    visit hotel_rooms_path


    page.find('.filter').should have_content("#{room_type1.name_pt_br}")
    page.find('.filter').should have_content("#{room_type2.name_pt_br}")

    find(:css, ".cpy-room-type-#{room_type1.id}").set(true)
    page.find('.cpy-filter-button').click
    5.times do |i|
      page.find('.cpy-room-list').should have_css("tr.cpy-room-#{rooms_array[i].id}")
      page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i + 5].id}")
    end
    visit hotel_rooms_path
    find(:css, ".cpy-room-type-#{room_type2.id}").set(true)
    page.find('.cpy-filter-button').click
    5.times do |i|
      page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i].id}")
      page.find('.cpy-room-list').should have_css("tr.cpy-room-#{rooms_array[i + 5].id}")
    end
  end

  scenario 'I can filter rooms by room number' do
    room_type1 = FactoryGirl.create(:room_type, hotel: @hotel)
    room_type2 = FactoryGirl.create(:room_type, hotel: @hotel)
    rooms_array = []
    5.times do |i|
      rooms_array[i] = FactoryGirl.create(:room, number: (i+1), room_type: room_type1, hotel: @hotel)
    end
    5.times do |i|
      rooms_array[i + 5] = FactoryGirl.create(:room, number: (i+1), room_type: room_type2, hotel: @hotel)
    end
    visit hotel_rooms_path
    5.times do |i|
      fill_in 'by_number', with: (i + 1)
      page.find('.cpy-filter-button').click
      page.find('.cpy-room-list').should have_content("#{room_type1.name_pt_br}")
      page.find('.cpy-room-list').should have_content("#{room_type2.name_pt_br}")
      page.find('.cpy-room-list').should have_css("tr.cpy-room-#{rooms_array[i].id}")
      unless (i == 4)
        page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i + 1].id}")
        page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i + 1 + 5].id}")
      end
      unless (i == 0)
        page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i - 1].id}")
      end
      page.find('.cpy-room-list').should have_css("tr.cpy-room-#{rooms_array[i + 5].id}")
      page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i + 4].id}")
    end
  end

  scenario 'I can filter rooms by room type and number' do
    room_type1 = FactoryGirl.create(:room_type, hotel: @hotel)
    room_type2 = FactoryGirl.create(:room_type, hotel: @hotel)
    rooms_array = []
    5.times do |i|
      rooms_array[i] = FactoryGirl.create(:room, room_type: room_type1, hotel: @hotel)
    end
    5.times do |i|
      rooms_array[i + 5] = FactoryGirl.create(:room, room_type: room_type2, hotel: @hotel)
    end
    visit hotel_rooms_path
    find(:css, ".cpy-room-type-#{room_type1.id}").set(true)
    5.times do |i|
      fill_in 'by_number', with: (rooms_array[i].number)
      page.find('.cpy-filter-button').click
      page.find('.cpy-room-list').should have_css("tr.cpy-room-#{rooms_array[i].id}")
      page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i + 1].id}")
      unless (i == 0)
        page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i - 1].id}")
      end
    end
    find(:css, ".cpy-room-type-#{room_type1.id}").set(false)
    find(:css, ".cpy-room-type-#{room_type2.id}").set(true)
    5.times do |i|
      fill_in 'by_number', with: (rooms_array[i + 5].number)
      page.find('.cpy-filter-button').click
      page.find('.cpy-room-list').should have_css("tr.cpy-room-#{rooms_array[i + 5].id}")
      unless (i == 4)
        page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i + 6].id}")
      end
      unless (i == 0)
        page.find('.cpy-room-list').should_not have_css("tr.cpy-room-#{rooms_array[i - 4].id}")
      end
    end
  end

end
