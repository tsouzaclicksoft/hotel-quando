# encoding: utf-8
require 'spec_helper'

story 'As an hotel administrator I can manage my account,' do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel, state: 'SP')
    sign_in_hotel(@hotel)
  end

  scenario 'I can see my info' do
    visit hotel_root_path
    page.should have_content("#{@hotel.name}")
    page.should have_content("#{@hotel.description_pt_br}")
    page.should have_content("#{@hotel.description_en}")
    page.should have_content("22,22%")
    page.should have_content("5,5%")
    page.should have_content("#{@hotel.minimum_hours_of_notice} hora")
    page.should have_content("#{@hotel.street}, #{@hotel.number}")
    page.should have_content("#{@hotel.city.name} - #{@hotel.city.state.initials}")
    Hotel.where(id: @hotel.id).update_all(minimum_hours_of_notice: -1)
    visit hotel_root_path
    page.should have_content('Não aceita cancelamento')
  end

  scenario 'I can change my password' do
    visit hotel_root_path
    click_link 'Alterar Senha'
    fill_in 'hotel_current_password', with: '123456789'
    fill_in 'hotel_password', with: '123123123'
    fill_in 'hotel_password_confirmation', with: '123123123'
    page.find('.cpy-submit-button').click
    click_link 'Sair'
    visit hotel_root_path
    fill_in 'hotel_hotel_login', with: @hotel.login
    fill_in 'hotel_hotel_password', with: '123123123'
    click_button 'Entrar'
    page.should have_content("Bem-vindo ao Hotel Quando.")
  end

  scenario 'I can recover my password' do
    ActionMailer::Base.deliveries.clear
    click_link 'Sair'
    click_link 'Esqueceu a senha?'
    fill_in 'hotel_hotel_login', with: @hotel.login
    page.find('.cpy-submit-button').click
    ActionMailer::Base.deliveries.should have(1).email
    ActionMailer::Base.deliveries.first.to.should == [ @hotel.email ]
  end
end
