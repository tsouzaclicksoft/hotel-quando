# encoding: utf-8
require 'spec_helper'
include ActionView::Helpers::NumberHelper

story 'As an hotel administrator I can manage pricing policies,', pending: true do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel, state: 'SP')
    sign_in_hotel(@hotel)
  end

  scenario 'I can list pricing policies' do
    pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
    visit hotel_pricing_policies_path
    page.should have_content("#{pricing_policy.name}")
  end

  scenario 'I should see a message if there are no pricing policies registered' do
    visit hotel_pricing_policies_path
    page.should have_content('Nenhuma tabela de tarifas cadastrada.')
  end

  scenario 'creation should not be visible if there are no room types registered' do
    visit hotel_pricing_policies_path
    page.find('.cpy-add-link').click
    page.should have_content('Nenhum tipo de quarto cadastrado, cadastre um tipo de quarto primeiro para poder cadastrar uma tabela de tarifas. ')
    page.should_not have_css('#pricing_policy_name')
  end

  scenario 'I can create new pricing policies' do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    visit hotel_pricing_policies_path
    page.find('.cpy-add-link').click
    fill_in 'pricing_policy_name', with: 'Teste'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_pack_price_3h', with: '10'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_pack_price_6h', with: '20'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_pack_price_9h', with: '30'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_pack_price_12h', with: '40'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_pack_price_24h', with: '50'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_extra_price_per_person_for_pack_price_3h', with: '100'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_extra_price_per_person_for_pack_price_6h', with: '200'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_extra_price_per_person_for_pack_price_9h', with: '300'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_extra_price_per_person_for_pack_price_12h', with: '400'
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_extra_price_per_person_for_pack_price_24h', with: '500'
    page.find('.cpy-submit-button').click
    page.should have_content('Teste')
    page.should have_content("#{room_type.name_pt_br}")
    page.should have_content('R$ 10,00')
    page.should have_content('R$ 20,00')
    page.should have_content('R$ 30,00')
    page.should have_content('R$ 40,00')
    page.should have_content('R$ 50,00')
    page.should have_content('R$ 60,00')
    page.should have_content('R$ 70,00')
    page.should have_content('R$ 100,00')
    page.should have_content('R$ 200,00')
    page.should have_content('R$ 300,00')
    page.should have_content('R$ 400,00')
    page.should have_content('R$ 500,00')
    page.should have_content('R$ 600,00')
    page.should have_content('R$ 700,00')
    PricingPolicy.count.should == 1
  end

  scenario 'I can see the details of a pricing policy' do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
    FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
    visit hotel_pricing_policies_path
    page.find('.cpy-show-button').click
    page.should have_content("#{pricing_policy.name}")
    page.should have_content("#{room_type.name_pt_br}")
    page.should have_content('R$ 1,00')
    page.should have_content('R$ 2,00')
    page.should have_content('R$ 3,00')
    page.should have_content('R$ 4,00')
    page.should have_content('R$ 5,00')
    page.should have_content('R$ 6,00')
    page.should have_content('R$ 7,00')
    page.should have_content('R$ 10,00')
    page.should have_content('R$ 20,00')
    page.should have_content('R$ 30,00')
    page.should have_content('R$ 40,00')
    page.should have_content('R$ 50,00')
    page.should have_content('R$ 60,00')
    page.should have_content('R$ 70,00')
  end

  scenario 'I can edit pricing policies' do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
    FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
    visit hotel_pricing_policies_path
    page.find('.cpy-edit-button').click
    page.should have_content("#{room_type.name_pt_br}")
    fill_in 'pricing_policy_room_type_pricing_policies_attributes_0_pack_price_3h', with: '8'
    page.find('.cpy-submit-button').click
    page.should have_content('R$ 8,00')
  end

  scenario 'I can include prices to room types that are added after the pricing table creation' do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
    room_type_policy = FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
    new_room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    visit hotel_pricing_policies_path
    page.find('.cpy-edit-button').click
    page.should have_content("#{new_room_type.name_pt_br}")
    page.find(".cpy-#{new_room_type.name_pt_br.gsub(' ','-')}-3h").set('8')
    page.find('.cpy-submit-button').click
    page.should have_content("#{room_type.name_pt_br}")
    page.should have_content("#{new_room_type.name_pt_br}")
    page.should have_content('R$ 8,00')
    number_to_currency(room_type_policy.reload.pack_price_3h).should_not == 'R$ 8,00'
    page.should have_content(number_to_currency(room_type_policy.reload.pack_price_3h))
    page.should have_content('-')
  end

  scenario "when creating a price policy, I should be alerted that if I set a price to 0, I won't be able to create offers to the combination" do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    visit hotel_pricing_policies_path
    page.find('.cpy-add-link').click
    page.should have_content('Atenção: Não será possível criar ofertas para as combinações que estiverem com preço R$ 0,00.')
  end

  scenario "when editing a price policy, I should be alerted that if I set a price to 0, I won't be able to create offers to the combination" do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
    FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
    visit hotel_pricing_policies_path
    page.find('.cpy-edit-button').click
    page.should have_content('Atenção: Não será possível criar ofertas para as combinações que estiverem com preço R$ 0,00.')
  end

  scenario "when editing a price policy, I should be alerted that the offers won't be automatically updated" do
    room_type = FactoryGirl.create(:room_type, hotel: @hotel)
    pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
    FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
    visit hotel_pricing_policies_path
    page.find('.cpy-edit-button').click
    page.should have_content('As ofertas que foram cadastradas com esta tabela de tarifas não serão atualizadas automaticamente.')
  end

  context 'with all room types included in the pricing policy' do
    scenario 'I should see an icon alerting me if the pricing policy is incomplete' do
      room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
      FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy, pack_price_6h: 0)
      visit hotel_pricing_policies_path
      page.should have_css('.icon-remove')
      page.should_not have_css('.icon-ok')
    end

    scenario 'I should see an icon telling me that the pricing policy is complete' do
      room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
      FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
      visit hotel_pricing_policies_path
      page.should have_css('.icon-ok')
      page.should_not have_css('.icon-remove')
    end
  end

  context 'without all room types included in the pricing policy' do
    scenario 'I should see an icon alerting me that the pricing policy is incomplete' do
      room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      room_type = FactoryGirl.create(:room_type, hotel: @hotel)
      pricing_policy = FactoryGirl.create(:pricing_policy, hotel: @hotel)
      FactoryGirl.create(:room_type_pricing_policy, room_type: room_type, pricing_policy: pricing_policy)
      visit hotel_pricing_policies_path
      page.should have_css('.icon-remove')
      page.should_not have_css('.icon-ok')
    end
  end

end
