# encoding: utf-8
require 'spec_helper'

story "As an hotel administrator I can see the reports," do
  before(:each) do
    @hotel = FactoryGirl.create(:hotel, comission_in_percentage: 10, iss_in_percentage: 5)
    sign_in_hotel(@hotel)
  end

  context "visiting the reports page" do

    context "without bookings" do

      scenario "I should see a message" do
        visit hotel_reports_path
        page.should have_content("O hotel não possui no-shows ou reservas confirmadas no mês atual para gerar o relatório.")
      end
    end

    context "with bookings," do
      before(:each) do
        @room_type = FactoryGirl.create(:room_type, hotel: @hotel)

        @offer1 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc, pack_in_hours: 3, price: 11)
        @offer2 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc, pack_in_hours: 6, price: 12)
        @offer3 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc - 1.month, pack_in_hours: 9, price: 13)
        @offer4 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc - 2.month, pack_in_hours: 12, price: 14)

        @offer5 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc, pack_in_hours: 3, no_show_value: 111)
        @offer6 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc, pack_in_hours: 6, no_show_value: 112)
        @offer7 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc - 1.month, pack_in_hours: 12, no_show_value: 113)
        @offer8 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc - 2.month, pack_in_hours: 12, no_show_value: 114)

        @offer9 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc, pack_in_hours: 6, price: 20)
        @offer10 = FactoryGirl.create(:offer, hotel: @hotel, room_type: @room_type, checkin_timestamp: Time.now.utc - 1.month, pack_in_hours: 12, price: 30)

        @booking1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @room_type, offer: @offer1, number_of_people: 1)
        @booking2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @room_type, offer: @offer2, number_of_people: 1)
        @booking3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @room_type, offer: @offer3, number_of_people: 1)
        @booking4 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @room_type, offer: @offer4, number_of_people: 1)

        @no_show1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type, offer: @offer5, number_of_people: 1)
        @no_show2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type, offer: @offer6, number_of_people: 1)
        @no_show3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type, offer: @offer7, number_of_people: 1)
        @no_show4 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @room_type, offer: @offer8, number_of_people: 1)

        @booking_paid_online1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @room_type, offer: @offer9, number_of_people: 1)
        @booking_paid_online2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @room_type, offer: @offer10, number_of_people: 1)

        visit hotel_reports_path
      end

      scenario "I should see the bookings paginated list" do
        within(".cpy-bookings-list") do
          page.should have_content(@booking1.guest_name)
          page.should have_content(@booking2.guest_name)
          page.should have_no_content(@booking3.guest_name)
          page.should have_no_content(@booking4.guest_name)
          page.should have_no_content(@no_show1.guest_name)
          page.should have_no_content(@no_show2.guest_name)
          page.should have_no_content(@no_show3.guest_name)
          page.should have_no_content(@no_show4.guest_name)
        end

        page.should have_css('.cpy-pagination-links')
      end

      scenario "I should see the no-shows paginated list" do
        within(".cpy-no-shows-list") do
          page.should have_no_content(@booking1.guest_name)
          page.should have_no_content(@booking2.guest_name)
          page.should have_no_content(@booking3.guest_name)
          page.should have_no_content(@booking4.guest_name)
          page.should have_content(@no_show1.guest_name)
          page.should have_content(@no_show2.guest_name)
          page.should have_no_content(@no_show3.guest_name)
          page.should have_no_content(@no_show4.guest_name)
        end

        page.should have_css('.cpy-pagination-links')
      end

      scenario "I should see the correct overview information" do
        within(".cpy-bookings-info") do
          within(".cpy-total") { page.should have_content(3) }
          within(".cpy-total-paid-at-the-hotel") { page.should have_content(2) }
          within(".cpy-total-paid-online") { page.should have_content(1) }
          within(".cpy-total-value") { page.should have_content("R$ 43,00") }
          within(".cpy-total-value-without-iss") { page.should have_content("R$ 40,95") }
          within(".cpy-credit-card-tax") { page.should have_content("R$ 0,60") }
          # within(".cpy-comission") { page.should have_content("R$ 4,10") }
          within(".cpy-final-total-value") { page.should have_content("R$ 36,26") }
        end

        within(".cpy-no-shows-info") do
          within(".cpy-total") { page.should have_content(2) }
          within(".cpy-total-value") { page.should have_content("R$ 223,00") }
          within(".cpy-total-value-without-iss") { page.should have_content("R$ 212,38") }
          within(".cpy-credit-card-tax") { page.should have_content("R$ 6,69") }
          # within(".cpy-comission") { page.should have_content("R$ 21,24") }
          within(".cpy-final-total-value") { page.should have_content("R$ 184,45") }
        end

        within(".cpy-totalizing") do
          within(".cpy-total-value") { page.should have_content("R$ 266,00") }
          within(".cpy-total-value-without-iss") { page.should have_content("R$ 253,33") }
          within(".cpy-total-credit-card-tax") { page.should have_content("R$ 7,29") }
          # within(".cpy-comission") { page.shoul d have_content("R$ 25,33") }
          within(".cpy-final-total") { page.should have_content("R$ 220,71") }
        end
      end

      scenario "I should see the statistics reports" do
        within(".cpy-income-per-pack") do
          within("tbody > tr:nth-child(1)") do
            page.should have_content("3h")
            page.should have_content("R$ 122,00")
          end

          within("tbody > tr:nth-child(2)") do
            page.should have_content("6h")
            page.should have_content("R$ 144,00")
          end

          within("tbody > tr:nth-child(3)") do
            page.should have_content("9h")
            page.should have_content("R$ 0,00")
          end

          within("tbody > tr:nth-child(4)") do
            page.should have_content("12h")
            page.should have_content("R$ 0,00")
          end
        end
      end
    end
  end
end
