require "spec_helper"

RSpec.describe PublicAffiliate::Api::V1::RoomsController do

  describe "GET " do
    
    context "When hotel's id not exists" do
    
      it "Does return http status 404" do
        get :index, { hotel_id: 99 }
        expect(response.status).to eq(404)
      end
    end
    
    context "When hotel's id exists" do
    
      let(:hotel) do
        FactoryGirl.create :hotel
      end
      
      it "Does return http status 200" do
        get :index, { hotel_id: hotel.id}
        expect(response.status).to eq(200)
      end
      
      it "Does return content-type JSON" do
        get :index, { hotel_id: hotel.id }
        expect(response.content_type).to eq("application/json")
      end
    end
  end

  describe ".show" do
    
    context "When hotel's id not exists" do
    
      it "Does return http status 404" do
        get :show, { hotel_id: 999,  id: 999 }
        expect(response.status).to eq(404)
      end
    end

    context "When hotel's id exists and room's id not exists" do
    
      let(:hotel) do
        FactoryGirl.create(:hotel)
      end
    
      it "Does return http status 404" do
        get :show, { hotel_id: hotel.id, id: 99 }
        expect(response.status).to eq(404)
      end
    end
    
  #   context "When id exists on database" do
  #   
  #     let!(:room) do
  #       FactoryGirl.create :room
  #     end
  #   
  #     before do
  #       get :show, { id: room.id }
  #     end
  #   
  #     it "Does return http status 200" do
  #       expect(response.status).to eq(200)
  #     end
  #   
  #     it "Does return content-type JSON" do
  #       expect(response.content_type).to eq("application/json")
  #     end
  #   end
  end

end
