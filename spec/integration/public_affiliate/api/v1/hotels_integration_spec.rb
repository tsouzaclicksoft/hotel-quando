require "spec_helper"

RSpec.describe PublicAffiliate::Api::V1::HotelsController do

  describe "GET /affiliate/api/v1/hotels" do

    context "When send invalid params" do

      before do
        expect_any_instance_of(OffersSearch::Hotels).to receive(:results!).and_raise(ArgumentError.new("invalid_request"))
        get "/affiliate/api/v1/hotels"
      end
      
      it_behaves_like "a default response api", 400
    
      it "Does return caused by on body" do
        expect(response.body).to eq("invalid_request")
      end
    end

    context "When valid params" do
      
      context "When not exists hotels" do
    
        let(:results) do
          result = OpenStruct.new
          result.exacts = []
          result
        end
        
        before do
          expect_any_instance_of(OffersSearch::Hotels).to receive(:results!).and_return(results)
          get "/affiliate/api/v1/hotels"
        end
        
        it_behaves_like "a default response api", 200
        
        it "Does return body with total equal zero" do
          expect(response_json[:total]).to be_zero
        end
        
        it "Does return body with items empty" do
          expect(response_json[:items]).to match_array([])
        end
      end
    
      context "When exist hotels" do
        
        let(:results) do
          result = OpenStruct.new
          result.exacts = hotels
          result
        end
        
        let(:hotels) do
          FactoryGirl.build_list(:hotel, 10, id: 1)
        end
        
        before do
          expect_any_instance_of(OffersSearch::Hotels).to receive(:results!).and_return(results)
          expect(HotelDistanceSorter).to receive(:sort).and_return(hotels)
          get "/affiliate/api/v1/hotels"
        end
        
        it_behaves_like "a default response api", 200
        
        it "Does return body with total with size found" do
          expect(response_json[:total]).to eq(10)
        end
        
        it "Does return body with items not empty" do
          expect(response_json[:items]).to_not be_empty
        end
        
        it "Does return itens with attributes #{AcceptanceResponseKeys.hotel_preview}" do
          expect(response_json[:items].first.keys).to match_array(AcceptanceResponseKeys.hotel_preview)
        end
      end
    end
  end

  describe "GET /affiliate/api/v1/hotels/:id" do
    
    before do
      get "/affiliate/api/v1/hotels/#{hotel.id}"
    end
    
    context "When hotel's id not exists" do
       
      let(:hotel) do
        Hotel.new(id: 99)
      end
      
      it_behaves_like "a default response api", 404
    end
    
    context "When hotel's id exists" do
    
      let!(:hotel) do
        FactoryGirl.create :hotel
      end
    
      before do
        get "/affiliate/api/v1/hotels/#{hotel.id}"
      end
    
      it_behaves_like "a default response api", 200
    end
  end
end
