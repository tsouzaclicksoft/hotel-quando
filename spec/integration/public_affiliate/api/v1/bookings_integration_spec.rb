require "spec_helper"

RSpec.describe PublicAffiliate::Api::V1::BookingsController do

  describe ".index" do
    
  end

  describe "DELETE /affiliate/api/v1/bookings/:id" do

    before do
      delete "/affiliate/api/v1/bookings/#{booking.id}"
    end
    
    context "When booking's id not exists" do

      let(:booking) do
        Booking.new(id: 99)
      end
      
      it_behaves_like "a default response api", 404
    end
    
    context "When booking removed with success" do
      
      let(:booking) do
        FactoryGirl.create :booking
      end
      
      it_behaves_like "a default response api", 200
    end
  end

  describe "GET /affiliate/api/v1/bookings/:id" do
    
    before do 
      get "/affiliate/api/v1/bookings/#{booking.id}"
    end
    
    context "When booking's id not exists" do
    
      let(:booking) do
        Booking.new(id: 99)  
      end
    
      it_behaves_like "a default response api", 404
    end
    
    context "When booking's id exists" do
    
      let!(:booking) do
        FactoryGirl.create :booking
      end
    
      it_behaves_like "a default response api", 200
    end
  end

end
