# encoding: utf-8
require 'spec_helper'
story 'As an agency manager I can manage clients,' do
  before(:each) do
    @agency = FactoryGirl.create(:agency)
    sign_in_agency @agency
  end

  scenario 'I can list clients' do
    FactoryGirl.create_list(:company, 5, agency: @agency)
    visit agency_clients_path
    User.all.each do | user |
      page.should have_content(user.name)
    end
  end

  scenario 'I should see a message if there are no clients registered' do
    visit agency_clients_path
    page.should have_content('Nenhum cliente cadastrado')
  end

  scenario 'I can include new clients' do
    visit agency_clients_path
    page.find('.cpy-add-link').click
    fill_in 'client_name', with: 'Empresa Teste'
    fill_in 'client_responsible_name', with: 'Responsável Teste'
    fill_in 'client_email', with: 'empresa@email.com'
    fill_in 'client_cnpj', with: '26.146.865/0001-54'
    page.find('.cpy-submit-button').click
    Company.count.should == 1
    created_company = Company.first
    created_company.agency_id.should == @agency.id
  end

  scenario 'when including new client a new password should be sent' do

    visit new_agency_client_path
    fill_in 'client_name', with: 'Empresa Teste'
    fill_in 'client_responsible_name', with: 'Responsável Teste'
    fill_in 'client_email', with: 'empresa@email.com'
    fill_in 'client_cnpj', with: '26.146.865/0001-54'
    page.find('.cpy-submit-button').click
    Company.count.should == 1
    last_email.to.should include "empresa@email.com"
  end

  scenario 'I should see error messages when including new clients with invalid information' do
    visit agency_clients_path
    page.find('.cpy-add-link').click
    fill_in 'client_name', with: ''
    page.find('.cpy-submit-button').click
    page.should have_css('.client_name.has-error')
  end

  scenario 'I should not see other agencies clients' do
    client = FactoryGirl.create(:company, agency: @agency)
    other_agency_client = FactoryGirl.create(:company)
    visit agency_clients_path
    page.should_not have_content(other_agency_client.name)
  end

  scenario 'I can log in as one of my clients' do
    client = FactoryGirl.create(:company, agency: @agency)
    visit agency_clients_path
    page.find('.cpy-log-in-as-client').click
    should_be_on(company_root_path)
    page.should have_content "Você está acessando o sistema como #{client.name}"
  end

  context 'visiting the client details page' do
    before(:each) do
      @client = FactoryGirl.create(:company, agency: @agency)
      @client_employee = FactoryGirl.create(:user, company_id: @client.id)
      @other_agency_client = FactoryGirl.create(:company)
      @other_agency_client_employee = FactoryGirl.create(:user, company_id: @other_agency_client.id)
      visit agency_client_path(@client)
    end

    scenario 'I can log in as my clients' do
      page.find('.cpy-log-in-as-client').click
      should_be_on(company_root_path)
      page.should have_content "Você está acessando o sistema como #{@client.name}"
    end

    scenario 'I can see the client informations' do
      page.should have_content("#{@client.name}")
      page.should have_content("#{@client.email}")
    end

    scenario 'I can log in as one of my client employees' do
      page.find('.cpy-log-in-as-client-employee').click
      should_be_on(public_root_path)
      page.should have_content "Você está acessando o sistema como #{@client.name}"
    end

    scenario 'I cant log in as a client of other agency', js:true do
      original_url = page.find('.cpy-log-in-as-client-employee')[:href]
      modified_url = original_url.gsub(@client.id.to_s, @other_agency_client.id.to_s).gsub(@client_employee.id.to_s, @other_agency_client_employee.id.to_s)
      page.execute_script("$('.cpy-log-in-as-client-employee').prop('href', '#{modified_url}')")
      page.execute_script("$('.cpy-log-in-as-client-employee').prop('target', '')")
      page.execute_script("$('.cpy-log-in-as-client-employee').click()")
      should_not_be_on public_root_path
      page.should have_content('Você não tem permissões para accessar esta conta!')
    end
  end
end
