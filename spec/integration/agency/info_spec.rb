# encoding: utf-8
require 'spec_helper'

story 'As an agency administrator I can manage my account,' do
  before(:each) do
    @agency = FactoryGirl.create(:agency)
    sign_in_agency(@agency)
  end

  scenario 'I can see my info' do
    visit agency_root_path
    page.should have_content("#{@agency.name}")
  end

  scenario 'I can change my password' do
    visit agency_root_path
    click_link 'Alterar Senha'
    fill_in 'agency_current_password', with: '123456789'
    fill_in 'agency_password', with: '123123123'
    fill_in 'agency_password_confirmation', with: '123123123'
    page.find('.cpy-submit-button').click
    click_link 'Sair'
    visit agency_root_path
    fill_in 'agency_agency_email', with: @agency.email
    fill_in 'agency_agency_password', with: '123123123'
    click_button 'Entrar'
    page.should have_content("Bem-vindo ao Hotel Quando.")
  end

  scenario 'I can recover my password' do
    ActionMailer::Base.deliveries.clear
    click_link 'Sair'
    click_link 'Esqueceu a senha?'
    fill_in 'agency_agency_email', with: @agency.email
    page.find('.cpy-submit-button').click
    ActionMailer::Base.deliveries.should have(1).email
    ActionMailer::Base.deliveries.first.to.should == [ @agency.email ]
  end
end
