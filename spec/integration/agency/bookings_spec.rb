# encoding: utf-8
require 'spec_helper'

story 'As an agency I can view my created bookings' do
  before(:each) do
    @agency = FactoryGirl.create(:agency)
    @company = FactoryGirl.create(:company, agency: @agency)
    @company2 = FactoryGirl.create(:company, agency: @agency)
    @user_related_to_company = FactoryGirl.create(:user, company: @company)
    @user_related_to_company2 = FactoryGirl.create(:user, company: @company2)
    sign_in_agency @agency
  end

  context 'without bookings' do
    scenario 'I should see a message telling me that there wasnt any bookings found' do
      visit agency_bookings_path
      page.should have_content 'Nenhuma reserva encontrada'
    end
  end

  context 'with bookings' do
    context 'visiting the booking index' do
      before(:each) do
        @agency_created_booking = FactoryGirl.create(:booking, user: @user_related_to_company, company: @company, created_by_agency: true, status: Booking::ACCEPTED_STATUSES[:canceled])
        Timecop.travel(DateTime.now - 10.days) do
          @agency_created_booking2 = FactoryGirl.create(:booking, user: @user_related_to_company2, company: @company2, created_by_agency: true, status: Booking::ACCEPTED_STATUSES[:confirmed])
        end

        Timecop.travel(DateTime.now + 10.days) do
          @cancelable_booking = FactoryGirl.create(:booking, user: @user_related_to_company2, company: @company2, created_by_agency: true, status: Booking::ACCEPTED_STATUSES[:confirmed])
        end

        @client_created_booking = FactoryGirl.create(:booking, user: @user_related_to_company, created_by_agency: false, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], company: @company)
        visit agency_bookings_path
      end

      scenario 'I can cancel the bookings' do
        BookingCancellationSrv.should_receive(:call).and_call_original
        page.find('.cpy-cancel-button').click
        @cancelable_booking.reload
        @cancelable_booking.status.should == Booking::ACCEPTED_STATUSES[:canceled]
        @cancelable_booking.canceled_by_agency?.should == true
      end

      scenario 'I can list the bookings' do
        within '.cpy-bookings-list' do
          page.should have_content @agency_created_booking.guest_name
          page.should have_content @agency_created_booking.user.company.name
          page.should have_content @agency_created_booking.hotel.name

          page.should have_content @agency_created_booking2.guest_name
          page.should have_content @agency_created_booking2.user.company.name
          page.should have_content @agency_created_booking2.hotel.name

          page.should_not have_content @client_created_booking.guest_name
          page.should_not have_content @client_created_booking.hotel.name
        end
      end

      scenario 'I can filter bookings' do
        fill_in 'by_checkout_date[initial_date]', with: @agency_created_booking.offer.checkout_timestamp.strftime('%d/%m/%Y')
        fill_in 'by_checkout_date[final_date]', with: @agency_created_booking.offer.checkout_timestamp.strftime('%d/%m/%Y')
        page.find('.cpy-filter-button').click
        page.all('.cpy-bookings-list tr').count.should == 1
        page.find('.cpy-remove-filter-button').click
        page.all('.cpy-bookings-list tr').count.should == 3

        fill_in 'by_id', with: @agency_created_booking2.id
        page.find('.cpy-filter-button').click
        page.all('.cpy-bookings-list tr').count.should == 1
        page.find('.cpy-remove-filter-button').click

        select I18n.t(Booking::ACCEPTED_STATUS_REVERSED[@agency_created_booking2.status]), from: 'by_status'
        page.find('.cpy-filter-button').click
        page.all('.cpy-bookings-list tr').count.should == 2
        page.find('.cpy-remove-filter-button').click

        select @company2.name, from: 'by_company_id'
        page.find('.cpy-filter-button').click
        page.all('.cpy-bookings-list tr').count.should == 2
      end
    end

    context 'visiting the bookings details page' do
      before(:each) do
        Timecop.travel(DateTime.now + 10.days) do
          @agency_booking = FactoryGirl.create(:booking, user: @user_related_to_company, company: @company, created_by_agency: true)
        end
        @booking_not_related_to_agency1 = FactoryGirl.create(:booking, user: @user_related_to_company, company: @company, created_by_agency: false)
        @booking_not_related_to_agency2 = FactoryGirl.create(:booking, company: FactoryGirl.create(:company))
      end

      scenario 'I can see the bookings related to my agency' do
        visit agency_booking_path(@agency_booking.id)
        page.should have_content @agency_booking.guest_name
      end

      scenario 'I cant view the details of the bookings that are not related to my agency' do
        expect{
          visit agency_booking_path(@booking_not_related_to_agency1.id)
        }.to raise_error
        expect{
          visit agency_booking_path(@booking_not_related_to_agency2.id)
        }.to raise_error
      end

      scenario 'I can cancel a client created booking' do
        visit agency_booking_path(@agency_booking.id)
        BookingCancellationSrv.should_receive(:call).and_call_original
        page.find('.cpy-cancel-button').click
        @agency_booking.reload
        @agency_booking.status.should == Booking::ACCEPTED_STATUSES[:canceled]
        @agency_booking.canceled_by_agency?.should == true
      end
    end
  end
end
