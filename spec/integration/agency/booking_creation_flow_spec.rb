# encoding: utf-8
require 'spec_helper'
include ActionView::Helpers::NumberHelper

story 'As a agency trying to book an offer to a client' do
  before(:each) do
    @agency = FactoryGirl.create(:agency)
    @company = FactoryGirl.create(:company, agency: @agency)
    @offer = FactoryGirl.create(:offer)
    FactoryGirl.create(:user, company: @company, monthly_budget: 20000, budget_for_24_hours_pack: 20000)
    sign_in_agency @agency
    visit agency_client_path(@company)
    page.find('.cpy-log-in-as-client-employee').click
    should_be_on public_root_path
  end

  context 'visiting the hotel details page' do
    before(:each) do
      visit public_hotel_path(checkin_date: @offer.checkin_timestamp.to_date,
                              checkin_hour: @offer.checkin_timestamp.hour,
                              length_of_pack: @offer.pack_in_hours,
                              number_of_people: "1", id: @offer.hotel.id,
                              name: @offer.hotel.name)
    end

    context 'when i click to reserve an offer' do
      before(:each) do
        page.find('.cpy-reserve-offer-button').click
      end

      context 'at the new booking page' do

        scenario 'the page should have the informations of the offer that I am trying to book' do
          page.should have_content(@offer.checkin_timestamp.strftime('%d/%m/%Y'))
          page.should have_content(@offer.checkin_timestamp.strftime('%H:%M'))
          page.should have_content(@offer.pack_in_hours.to_s + ' horas')
          page.should have_content(@offer.room_type.name_pt_br)
        end

        scenario 'the number of people dropdown should have the booking price' do
          options_that_select_should_have = []
          (@offer.room_type_initial_capacity..@offer.room_type_maximum_capacity).to_a.each do | i |
            if i == 1
              options_that_select_should_have << "1 pessoa - #{number_to_currency @offer.price + Booking::BOOKING_TAX_IN_BR_CURRENCY}"
            else
              price = @offer.price + Booking::BOOKING_TAX_IN_BR_CURRENCY + ((i - 1) * @offer.extra_price_per_person)
              options_that_select_should_have << "#{i} pessoas - #{number_to_currency price}"
            end
          end
          page.should have_select('booking_number_of_people', with_options: options_that_select_should_have)
        end

        context 'after filling all the fields and submit the form' do
          before(:each) do
            fill_in 'booking_guest_name', with: 'Name Lastname'
            fill_in 'booking_note', with: 'Note note observation'
            page.find('.cpy-submit-button').click
          end

          scenario 'the company booking should be created' do
            @company.bookings.count.should == 1
          end

          scenario 'the booking should have the created_by_agency flag set to true' do
            @company.bookings.first.created_by_agency?.should == true
          end

          scenario "I should be redirected to payment step" do
            should_be_on new_public_booking_payments_path(booking_id: Booking.first.id)
          end

        end # of filling all the fields and submit the form
      end # of at the new booking page
    end # of when I click to reserve an offer
  end # of visiting the hotel details page
end
