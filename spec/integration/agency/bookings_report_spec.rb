# encoding: utf-8
require 'spec_helper'

story "As an agency I can see the booking reports," do
  before(:each) do
    @agency = FactoryGirl.create(:agency, comission_in_percentage: 10)
    sign_in_agency @agency
  end

  context "visiting the reports page" do

    before(:each) do
      @company = FactoryGirl.create(:company, agency: @agency)

      @hotel1 = FactoryGirl.create(:hotel, comission_in_percentage: 10, iss_in_percentage: 5)
      @hotel2 = FactoryGirl.create(:hotel, comission_in_percentage: 10, iss_in_percentage: 5)
      @hotel3 = FactoryGirl.create(:hotel, comission_in_percentage: 0, iss_in_percentage: 0)

      @room_type1 = FactoryGirl.create(:room_type, hotel: @hotel1)
      @room_type2 = FactoryGirl.create(:room_type, hotel: @hotel2)

      @offer1 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 1.day - 3.hours, pack_in_hours: 3, price: 11)
      @offer2 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 3.hours, pack_in_hours: 3, price: 12)
      @offer3 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 3.hours, pack_in_hours: 3, price: 13)

      @offer5 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.months).end_of_month - 1.day - 3.hours, pack_in_hours: 3, no_show_value: 111)
      @offer6 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.utc.beginning_of_month - 2.month).end_of_month - 3.hours, pack_in_hours: 3, no_show_value: 112)
      @offer7 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 3.hours, pack_in_hours: 3, no_show_value: 113)

      @offer8 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: Time.now.utc.beginning_of_month - 1.month + 9.days - 3.hours, pack_in_hours: 3, no_show_value: 114)
      @offer9 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 1.day - 3.hours, pack_in_hours: 3, no_show_value: 115)
      @offer10 = FactoryGirl.create(:offer, hotel: @room_type1.hotel, room_type: @room_type1, checkin_timestamp: (Time.now.beginning_of_month - 1.month), pack_in_hours: 3, no_show_value: 115, price: 50)
      @offer11 = FactoryGirl.create(:offer, hotel: @room_type2.hotel, room_type: @room_type2, checkin_timestamp: (Time.now.beginning_of_month - 1.month + 10.days), pack_in_hours: 3, no_show_value: 115, price: 60)

      @offer12 = FactoryGirl.create(:offer, hotel: @hotel3, checkin_timestamp: (Time.now.utc.beginning_of_month - 1.month).end_of_month - 3.hours, pack_in_hours: 3, price: 10)
      @offer13 = FactoryGirl.create(:offer, hotel: @hotel3, checkin_timestamp: (Time.now.beginning_of_month - 1.month + 10.days), pack_in_hours: 3, price: 10)


      @booking1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @offer1.room_type, offer: @offer1, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company, created_by_agency: true)
      @booking2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @offer2.room_type, offer: @offer2, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company)
      @booking3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @offer3.room_type, offer: @offer3, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company, created_by_agency: true)
      @booking4 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], room_type: @offer12.room_type, offer: @offer12, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company, created_by_agency: true)

      @no_show1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer5.room_type, offer: @offer5, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company)
      @no_show2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer6.room_type, offer: @offer6, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company, created_by_agency: true)
      @no_show3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer7.room_type, offer: @offer7, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company)
      @no_show4 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer8.room_type, offer: @offer8, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company, created_by_agency: true)
      @no_show5 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], room_type: @offer9.room_type, offer: @offer9, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company)

      @booking_paid_online1 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @offer10.room_type, offer: @offer10, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company, created_by_agency: true)
      @booking_paid_online2 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @offer11.room_type, offer: @offer11, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company)
      @booking_paid_online3 = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], room_type: @offer13.room_type, offer: @offer13, number_of_people: 1, booking_tax: 1, user: FactoryGirl.create(:user, company: @company), company: @company, created_by_agency: true)

      visit agency_bookings_report_path
    end

    context "When choosing the 'tenth day' report," do
      before(:each) do
        select("Relatório Dia 10", :from => "report_type")
        select("#{I18n.t('date.month_names')[Date.today.month]} #{Date.today.year}", :from => "tenth_day_in_report_month")
        page.find(".cpy-generate-button").click
      end

      scenario "I should see the bookings list" do
        within('.cpy-booking-listing') do
          page.should have_content(@booking3.user.name)
          page.should have_content(@booking4.user.name)
          page.should have_content(@no_show2.user.name)
          page.should have_content(@booking_paid_online1.user.name)
        end
      end

      scenario "I should see the correct overview information" do
        within(".cpy-agency-report") do
          within(".cpy-total-number-of-bookings") { page.should have_content(4) }
          within(".cpy-total-value") { page.should have_content("R$ 185,00") }
          within('.cpy-agency-comission') { page.should have_content("R$ 1,66") }
        end
      end

      scenario 'I should see the correct total per client information' do
        within('.cpy-total-per-client') do
          page.should have_content(@booking3.user.name)
          page.should have_content('R$ 14,00')

          page.should have_content(@booking4.user.name)
          page.should have_content('R$ 11,00')

          page.should have_content(@no_show2.user.name)
          page.should have_content('R$ 113,00')

          page.should have_content(@booking_paid_online1.user.name)
          page.should have_content('R$ 51,00')
        end
      end
    end

    context "When choosing the 'end of month' report," do
      before(:each) do
        select("Relatório Final de Mês", :from => "report_type")
        select("#{I18n.t('date.month_names')[Date.today.month]} #{Date.today.year}", :from => "tenth_day_in_report_month")
        page.find(".cpy-generate-button").click
      end

      scenario "I should see the bookings list" do
        within('.cpy-booking-listing') do
          page.should have_content(@no_show4.user.name)
          page.should have_content(@booking_paid_online2.user.name)
          page.should have_content(@booking_paid_online3.user.name)
        end
      end

      scenario 'I should see the correct totalizing information' do
        within(".cpy-agency-report") do
          within(".cpy-total-number-of-bookings") { page.should have_content(3) }
          within(".cpy-total-value") { page.should have_content("R$ 184,00") }
          within('.cpy-agency-comission') { page.should have_content("R$ 1,65") }
        end
      end

      scenario 'I should see the correct total per client information' do
        within('.cpy-total-per-client') do
          page.should have_content(@no_show4.user.name)
          page.should have_content("R$ 115,00")

          page.should have_content(@booking_paid_online2.user.name)
          page.should have_content("R$ 61,00")

          page.should have_content(@booking_paid_online3.user.name)
          page.should have_content("R$ 11,00")
        end
      end

    end
  end

end
