# encoding: utf-8
require 'spec_helper'

story 'As an agency administrator I want to log in,' do
  before(:each) do
    @agency = FactoryGirl.create(:agency)
    visit agency_root_path
    should_be_on(new_agency_agency_session_path)
  end

  context 'using valid informations' do
    before(:each) do
      fill_in 'agency_agency_email', with: @agency.email
      fill_in 'agency_agency_password', with: 123456789
    end

    context 'my agency is active' do
      scenario 'I can log in' do
        page.find('.cpy-submit-button').click
        page.should have_content('Bem-vindo ao Hotel Quando.')
        should_be_on(agency_root_path)
      end
    end

    context 'my agency is inactive' do
      before(:each) do
        @agency.deactivate!
        page.find('.cpy-submit-button').click
      end

      scenario 'I should see a message telling me that I cant log in' do
        page.should have_content('Sua agência foi desativada pelo administrador')
        should_be_on(new_agency_agency_session_path)
      end
    end
  end
end
