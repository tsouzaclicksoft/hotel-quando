# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara/poltergeist'
require 'capybara-screenshot/rspec'
require 'shoulda-matchers'
require 'fakeweb'
require "savon/mock/spec_helper"

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)

module RSpec::Core::DSL
  alias :story :describe
end

include RSpec::Helpers::UploadFile

ActiveRecord::ConnectionAdapters::ConnectionPool.class_eval do
  def current_connection_id
    # Thread.current.object_id
    Thread.main.object_id
  end
end

RSpec.configure do |config|
  config.alias_example_to :scenario
  config.include(Capybara::DSL)
  config.include(Savon::SpecHelper)
  config.include(StubEnv::Helpers)
  config.include(RSpec::Helpers::Integration, :type => :request)
  config.include(RSpec::Helpers::MailerMacros, :type => :request)
  config.include(Devise::TestHelpers, :type => :controller)
  config.include(ShowMeTheCookies, :type => :request)
  config.fixture_path = "#{::Rails.root}/spec/fixtures"
  config.use_transactional_fixtures = false
  config.infer_base_class_for_anonymous_controllers = false
  config.order = "random"

  # set Savon in and out of mock mode
  config.before(:all) { savon.mock!   }
  config.after(:all)  { savon.unmock! }

  config.before :each do
    stub_env('MP_ID', '123123')
    stub_env('MP_APIKEY', '123123')
    stub_env('ADMIN_PANEL_USER_LOGIN', 'admin')
    stub_env('ADMIN_PANEL_USER_PASSWORD', 'admin')
    stub_env('AWS_ACCESS_KEY_ID', "123123")
    stub_env('AWS_SECRET_ACCESS_KEY', "123123")
    stub_env('GOOGLE_MAPS_API_KEY', "123123")
    stub_env('OMNIBEES_USERNAME', "omnbuser")
    stub_env('OMNIBEES_PASSWORD', "omnbpass")
    stub_env('OMNIBEES_PROXY_ADDRESS', "http://fakeomnibeesproxy.com")
    stub_env('OMNIBEES_WSDL_ENDPOINT', "http://fakeomnibees.com?wsdl")
    FakeWeb.register_uri(:any, "http://fakeomnibees.com?wsdl", :body => load_fixture('omnibees/wsdl_definition.xml'))
  end
  Capybara.register_driver :poltergeist do |app|
    options = {
      :js_errors => false,
      :timeout => 120,
      :debug => false,
      :phantomjs_options => ['--load-images=no', '--disk-cache=false'],
      :inspector => true,
    }
    Capybara::Poltergeist::Driver.new(app, options) #, debug: true, window_size: [1300, 1000]) #, debug: true, window_size: [1300, 1000])
  end

  Capybara.javascript_driver = :poltergeist

  config.before(:suite) do
    FileUtils.remove_dir(Rails.root.join("public", "uploads/test").to_s, true)
    FileUtils.remove_dir(Rails.root.join("tmp", "capybara").to_s, true)
  end

  config.before(:each) do
    # FakeWeb.allow_net_connect = false
    ActionMailer::Base.deliveries.clear
    FactoryGirl.reload
    I18n.locale = :pt_br
  end

  config.before(:each, :type => :request) do
    expire_cookies
  end

end

def response_json
  JSON.parse(response.body, symbolize_names: true)
end

