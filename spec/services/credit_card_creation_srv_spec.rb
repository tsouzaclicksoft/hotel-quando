# encoding: utf-8

require 'spec_helper'

describe CreditCardCreationSrv do
  subject { CreditCardCreationSrv }

  before(:each) do
    @credit_card_owner = FactoryGirl.create(:user)
    @credit_card_params = {
      "billing_name" => "Meu Nome",
      "owner_cpf" => "373.499.958-81",
      "owner_passport" => "",
      "billing_address1" => "Endereço asdfsd",
      "billing_zipcode" => "71924333",
      "billing_city" => "Guarulhos",
      "billing_state" => "SP",
      "billing_country" => "BR",
      "billing_phone" => "(12) 31231-2321",
      "temporary_number" => "4444 4444 4444 4448",
      "expiration_month" => "3",
      "expiration_year" => "2020"
    }

    MaxiPagoInterface.stub(:generate_client_token_for).and_return('client_token')
    MaxiPagoInterface.stub(:generate_credit_card_token_for).and_return('credit_card_token')
  end

  it 'creates the credit card' do
    subject.call(credit_card_owner: @credit_card_owner, credit_card_attributes: @credit_card_params)
    CreditCard.count.should == 1
    created_credit_card = CreditCard.last
    created_credit_card.owner.should == @credit_card_owner
    created_credit_card.maxipago_token.should == 'credit_card_token'
  end

  it 'creates the token for the credit card owner if he doenst have one' do
    @credit_card_owner.update(maxipago_token: nil)
    subject.call(credit_card_owner: @credit_card_owner, credit_card_attributes: @credit_card_params)
    @credit_card_owner.reload.maxipago_token.should == 'client_token'
  end

  it 'does not change the credit card owner token if he already have one' do
    @credit_card_owner.update(maxipago_token: 'existing_token')
    subject.call(credit_card_owner: @credit_card_owner, credit_card_attributes: @credit_card_params)
    @credit_card_owner.reload.maxipago_token.should == 'existing_token'
  end
end
