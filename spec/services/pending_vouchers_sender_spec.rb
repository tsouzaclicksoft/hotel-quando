# encoding: utf-8

require 'spec_helper'

describe PendingVouchersSender do
  subject { PendingVouchersSender }
  before(:each) do
    stub_const("VoucherConsumer", Class.new do
      def self.consume(voucher, booking)
        voucher.update_column(:booking_id, booking.id)
        true
      end
    end)
  end

  describe '#run' do
    it 'sends email to all pending bookings that doesnt have vouchers' do
      bookings = FactoryGirl.create_list(:booking, 3, created_at: DateTime.current.in_time_zone - 13.hour, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
      available_vouchers = FactoryGirl.create_list(:easy_taxi_voucher, 2)
      voucher_already_used = FactoryGirl.create(:easy_taxi_voucher, booking: bookings.first)
      expect {
        (subject.new).run
      }.to change { ActionMailer::Base.deliveries.count }.by(2)
    end

    it 'does not send email if the voucher cannot be consumed' do
      booking = FactoryGirl.create(:booking)
      FactoryGirl.create(:easy_taxi_voucher, expiration_date: booking.checkin_date)
      expect {
        (subject.new).run
      }.to_not change { ActionMailer::Base.deliveries.count }
    end
  end
end
