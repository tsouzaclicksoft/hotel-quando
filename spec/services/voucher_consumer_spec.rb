# encoding: utf-8

require 'spec_helper'

describe VoucherConsumer do
  subject { VoucherConsumer }
  let(:booking) { FactoryGirl.create(:booking) }
  let(:voucher) { FactoryGirl.create(:easy_taxi_voucher) }
  describe '.consume' do
    after :each do
      Timecop.return
    end

    it "returns true if the voucher is linked with the booking" do
      allow(booking).to receive(:cancelable?).and_return(false)
      expect(voucher.booking).to be_nil
      freeze_date = DateTime.now
      Timecop.freeze freeze_date
      expect(subject.consume(voucher, booking)).to be_true
      voucher.reload
      expect(voucher.booking_id).to eq(booking.id)
      expect(voucher.linked_at.to_i).to eq(freeze_date.to_i)
    end

    it "returns false the booking can be cancelled" do
      allow(booking).to receive(:cancelable?).and_return(true)
      expect(voucher.booking).to be_nil
      expect(subject.consume(voucher, booking)).to be_false
      voucher.reload
      expect(voucher.booking).to be_nil
    end
    it "returns false if any voucher validation fails" do
      FactoryGirl.create(:easy_taxi_voucher, booking_id: booking.id)
      allow(booking).to receive(:cancelable?).and_return(true)
      expect(voucher.booking).to be_nil
      expect(subject.consume(voucher, booking)).to be_false
      voucher.reload
      expect(voucher.booking).to be_nil
    end
  end
end
