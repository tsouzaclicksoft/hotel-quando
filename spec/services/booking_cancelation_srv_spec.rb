# encoding: utf-8

require 'spec_helper'

describe BookingCancellationSrv do
  subject { BookingCancellationSrv }

  before(:each) do
    @booking = FactoryGirl.create(:booking)
  end

  it 'can cancel bookings that are waiting for payment' do
    subject.call(booking: @booking)
    @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
  end

  context 'maxipago was enabled at booking creation time' do
    context 'succesfull transaction' do
      before(:each) do
        maxipago_stub_response = OpenStruct.new
        maxipago_stub_response.body = 'body'
        maxipago_stub_response.transaction_canceled = true
        MaxiPagoInterface.stub(:cancel_transaction).and_return(maxipago_stub_response)
      end

      it 'can cancel confirmed bookings' do
        MaxiPagoInterface.should_receive(:cancel_transaction)
        FactoryGirl.create(:payment, booking: @booking).update(authorized_at: Time.now)
        @booking.update(status: Booking::ACCEPTED_STATUSES[:confirmed])
        subject.call(booking: @booking)
        @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end

      it 'can cancel confirmed and paid bookings' do
        MaxiPagoInterface.should_receive(:cancel_transaction)
        FactoryGirl.create(:payment, booking: @booking).update(authorized_at: Time.now)
        @booking.update(status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
        subject.call(booking: @booking)
        @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
      end
    end

    context 'transaction error' do
      before(:each) do
        maxipago_stub_response = OpenStruct.new
        maxipago_stub_response.body = 'body'
        maxipago_stub_response.transaction_canceled = false
        MaxiPagoInterface.should_receive(:cancel_transaction).and_return(maxipago_stub_response)
        @booking.update(status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured])
        @payment = FactoryGirl.create(:payment, booking: @booking,status: Payment::ACCEPTED_STATUSES[:captured])
        @payment.update!(authorized_at: Time.now)
      end

      specify 'the database should be unchanged' do
        subject.call(booking: @booking)
        @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:confirmed_and_captured]
        @payment.reload.status.should == Payment::ACCEPTED_STATUSES[:captured]
        @payment.canceled_at.should be_nil
      end
    end
  end

  context 'maxipago was disabled at booking creation time' do
    before(:each) do
      stub_const("MaxiPagoInterface", double())
    end

    it 'can cancel confirmed bookings' do
      @booking.update(status: Booking::ACCEPTED_STATUSES[:confirmed])
      mailer_mock = double
      mailer_mock.should_receive(:deliver)
      AdminMailer.should_receive(:send_booking_cancellation_notice).and_return(mailer_mock)
      subject.call(booking: @booking)
      @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
    end

    it 'can cancel bookings that are waiting for payment' do
      @booking.update(status: Booking::ACCEPTED_STATUSES[:waiting_for_payment])
      subject.call(booking: @booking)
      @booking.reload.status.should == Booking::ACCEPTED_STATUSES[:canceled]
    end
  end
end
