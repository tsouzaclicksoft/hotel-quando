# encoding: utf-8

require "spec_helper"
include ActionView::Helpers::NumberHelper

describe AdminMailer do
  describe 'send_new_possible_partner_contact' do
    it 'should send an email with the correct informations, for companies' do
      possible_partner_contact = FactoryGirl.create(:possible_partner_contact, :type_of_contact => PossiblePartnerContact::TYPES_OF_CONTACT[:company_contact])
      email = AdminMailer.send_new_possible_partner_contact(possible_partner_contact)
      email.header.encoded.should match('empresa')
      email.to[0].should match(AdminMailer::MAIL_TO_SEND_COMPANY_PARTNERSHIPS)
    end

    it 'should send an email with the correct information, for hotels' do
      possible_partner_contact = FactoryGirl.create(:possible_partner_contact, :type_of_contact => PossiblePartnerContact::TYPES_OF_CONTACT[:hotel_contact])
      email = AdminMailer.send_new_possible_partner_contact(possible_partner_contact)
      email.header.encoded.should match('hotel')
      email.to[0].should match(AdminMailer::MAIL_TO_SEND_HOTEL_PARTNERSHIPS)
    end
  end

  describe "no_show_pending_charge_notice" do
    it 'should have the right booking informations' do
      booking = FactoryGirl.create(:booking)
      email = AdminMailer.no_show_pending_charge_notice(booking)
      email.body.should have_content(booking.id)
      email.body.should have_content(booking.user.id)
      email.body.should have_content(booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M'))
      email.body.should have_content(booking.hotel.id)
      email.body.should have_content(booking.hotel.name)
      email.body.should have_content(booking.user.name)
      email.body.should have_content(booking.user.email)
      email.body.should have_content(number_to_currency booking.no_show_value)
    end

    it 'should send the email to the right place' do
      booking = FactoryGirl.create(:booking)
      email = AdminMailer.no_show_pending_charge_notice(booking)
      email.to[0].should == AdminMailer::MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS
    end
  end

  describe "no_show_with_charge_error_notice" do
    it 'should have the right booking informations' do
      booking = FactoryGirl.create(:booking)
      email = AdminMailer.no_show_with_charge_error_notice(booking)
      email.body.should have_content(booking.id)
      email.body.should have_content(booking.user.id)
      email.body.should have_content(booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M'))
      email.body.should have_content(booking.hotel.id)
      email.body.should have_content(booking.hotel.name)
      email.body.should have_content(booking.user.name)
      email.body.should have_content(booking.user.email)
      email.body.should have_content(number_to_currency booking.no_show_value)
    end

    it 'should send the email to the right place' do
      booking = FactoryGirl.create(:booking)
      email = AdminMailer.no_show_with_charge_error_notice(booking)
      email.to[0].should == AdminMailer::MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS
    end
  end

  describe "send_no_show_refund_error_notice" do
    it 'should have the right booking informations' do
      booking = FactoryGirl.create(:booking)
      email = AdminMailer.send_no_show_refund_error_notice(booking)
      email.body.should have_content(booking.user.id)
      email.body.should have_content(booking.user.name)
      email.body.should have_content(booking.user.email)
      email.body.should have_content(booking.id)
      email.body.should have_content(booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M'))
      email.body.should have_content(number_to_currency booking.total_price)
      email.body.should have_content(number_to_currency booking.no_show_value)
      email.body.should have_content(number_to_currency booking.no_show_refund_value)
    end

    it 'should send the email to the right place' do
      booking = FactoryGirl.create(:booking)
      email = AdminMailer.send_no_show_refund_error_notice(booking)
      email.to[0].should == AdminMailer::MAIL_TO_SEND_CREDIT_CARD_INFORMATIONS
    end
  end
end
