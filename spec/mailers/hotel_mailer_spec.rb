# encoding: utf-8

require "spec_helper"
include ActionView::Helpers::NumberHelper

describe HotelMailer do
  before(:each) do
    @hotel = FactoryGirl.create :hotel
    @hotel.password = "pass123123"
  end

  describe 'send_login_and_password' do

    before(:each) do
      @mail = HotelMailer.send_login_and_password(@hotel)
    end
    #ensure that the subject is correct
    it 'renders the subject' do
      @mail.subject.should == 'HotelQuando.com.br - Senha de acesso'
    end

    #ensure that the receiver is correct
    it 'sends to hotel email email' do
      @mail.to.should == [@hotel.email]
    end

    #ensure that the sender is correct
    it 'renders the sender email' do
      @mail.from.should == ["naoresponda@hotelquando.net"]
    end

    #ensure that the @name variable appears in the email body
    it 'assigns name' do
      @mail.body.encoded.should match(@hotel.name)
    end

    it 'assigns password' do
      @mail.body.encoded.should match("pass123123")
    end
  end

  describe "noshow_successfull_charged_notice" do
    it 'should send an email to the correct emails' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], hotel: @hotel)
      email = HotelMailer.noshow_successfull_charged_notice(booking)
      email.to.should == @hotel.emails_for_notice
    end

    it 'should send an email with correct booking informations' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], hotel: @hotel)
      email = HotelMailer.noshow_successfull_charged_notice(booking)
      email.body.encoded.should match(booking.guest_name)
      email.body.encoded.should match(booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      email.body.encoded.should match("#{booking.pack_in_hours.to_s + ' horas'}")
      email.body.encoded.should match(booking.user.name)
      email.body.encoded.should match('Debitado com sucesso')
      email.body.should have_link('', :href => hotel_bookings_url)
    end
  end

  describe "no_show_pending_charge_notice" do
    it 'should send an email to the correct emails' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], hotel: @hotel)
      email = HotelMailer.noshow_successfull_charged_notice(booking)
      email.to.should == @hotel.emails_for_notice
    end

    it 'should send an email with correct booking informations' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], hotel: @hotel)
      email = HotelMailer.no_show_pending_charge_notice(booking)
      email.body.encoded.should match(booking.guest_name)
      email.body.encoded.should match(booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      email.body.encoded.should match("#{booking.pack_in_hours.to_s + ' horas'}")
      email.body.encoded.should match(booking.user.name)
      email.body.encoded.should match('Pagamento pendente')
      email.body.should have_link('', :href => hotel_bookings_url)
    end
  end

  describe "no_show_with_charge_error_notice" do
    it 'should send an email to the correct emails' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], hotel: @hotel)
      email = HotelMailer.noshow_successfull_charged_notice(booking)
      email.to.should == @hotel.emails_for_notice
    end

    it 'should send an email with correct booking informations' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:no_show_paid_with_success], hotel: @hotel)
      email = HotelMailer.no_show_pending_charge_notice(booking)
      email.body.encoded.should match(booking.guest_name)
      email.body.encoded.should match(booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      email.body.encoded.should match("#{booking.pack_in_hours.to_s + ' horas'}")
      email.body.encoded.should match(booking.user.name)
      email.body.encoded.should match('Pagamento pendente')
      email.body.should have_link('', :href => hotel_bookings_url)
    end
  end

  describe 'send_booking_cancellation_notice', :now => true do
    it 'should send an email with correct informations' do
      booking = FactoryGirl.create(:booking, hotel: @hotel, status: Booking::ACCEPTED_STATUSES[:canceled])
      email = HotelMailer.send_booking_cancellation_notice(booking)

      email.body.encoded.should match(booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      email.body.encoded.should match("#{booking.pack_in_hours.to_s + ' horas'}")
      email.body.should have_link('', :href => hotel_bookings_url)
    end

    it 'should raise if the booking status is not canceled' do
      hotel = FactoryGirl.create(:hotel)
      Booking::ACCEPTED_STATUSES.each do | status_name, status_code |
        next if (status_name.to_s == 'canceled')
        booking = FactoryGirl.create(:booking, status: status_code, hotel: hotel)
        expect { HotelMailer.send_booking_cancellation_notice(booking) }.to raise_error
      end
    end

  end

  describe 'send_booking_confirmation_notice', :now => true do
    it 'should send an email with correct informations if the booking is confirmed' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed], hotel: @hotel)
      email = HotelMailer.send_booking_confirmation_notice(booking)
      email.to.should == @hotel.emails_for_notice
      email.body.encoded.should_not match("e paga")
      email.body.encoded.should match(booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      email.body.encoded.should match("#{booking.pack_in_hours.to_s + ' horas'}")
      email.body.should have_link('', :href => hotel_bookings_url)
    end

    it 'should send an email with correct informations if the booking is confirmed and captured' do
      booking = FactoryGirl.create(:booking, status: Booking::ACCEPTED_STATUSES[:confirmed_and_captured], hotel: @hotel)
      email = HotelMailer.send_booking_confirmation_notice(booking)
      email.body.encoded.should match("e paga")
      email.to.should == @hotel.emails_for_notice
      email.body.encoded.should match(booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      email.body.encoded.should match("#{booking.pack_in_hours.to_s + ' horas'}")
      email.body.should have_link('', :href => hotel_bookings_url)
    end

  end
end
