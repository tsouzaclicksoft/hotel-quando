# encoding: utf-8

require "spec_helper"
include ActionView::Helpers::NumberHelper

describe AgencyMailer do
  before(:each) do
    @agency = FactoryGirl.create(:agency)
    @agency.password = "123123"
    @client = FactoryGirl.create(:company, agency: @agency)
    @user_related_to_client = FactoryGirl.create(:user, company: @client)
    @booking = FactoryGirl.create(:booking, company: @client, user: @user_related_to_client, created_by_agency: true)
  end

  specify 'send_login_and_password' do
    mail = AgencyMailer.send_login_and_password(@agency)
    mail.to[0].should == @agency.email
    mail.body.encoded.should match(@agency.email)
    mail.body.encoded.should match(@agency.password)
    mail.body.encoded.should match(agency_root_url)
  end

  specify 'send_booking_creation_notice' do
    mail = AgencyMailer.send_booking_creation_notice(@booking)
    mail.to[0].should == @agency.email
    mail.subject.should == "HotelQuando.com - Nova reserva para #{@client.name}"
    mail.body.encoded.should match('Nova Reserva')
    mail.body.encoded.should match(@client.name)
  end

  specify 'send_booking_confirmation_notice', :now => true do
    email = AgencyMailer.send_booking_confirmation_notice(@booking)
    email.body.should have_content("#{@booking.hotel.minimum_hours_of_notice} horas antes do horário de checkin.")
    email.body.should have_content(@client.name)
    email.body.should have_content(@booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M'))
    email.body.should have_content(@booking.offer.checkout_timestamp.strftime('%d/%m/%Y %H:%M'))
    email.body.should have_content(number_to_currency @booking.total_price)
    email.body.should have_content(@booking.hotel.name)
    email.body.should have_content(@booking.hotel.address)
    email.body.should have_content(@booking.room_type.full_name_pt_br)
    email.body.should have_content(@booking.guest_name)
  end

  specify 'send_booking_expiration_notice' do
    mail = AgencyMailer.send_booking_expiration_notice(@booking)
    mail.subject.should == "HotelQuando.com - A reserva para #{@client.name} foi expirada"
    mail.body.encoded.should match(@client.name)
    mail.body.encoded.should match(I18n.l @booking.checkin_date)
    mail.body.encoded.should match(@booking.hotel.name)
  end

end
