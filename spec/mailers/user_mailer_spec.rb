# encoding: utf-8

require "spec_helper"
include ActionView::Helpers::NumberHelper

describe UserMailer do
  before(:each) do
    @booking = FactoryGirl.create(:booking)
  end

  describe 'send_booking_expiration_notice' do
  	it 'should send the email in portuguese if the user locale is pt_br' do
      mail = UserMailer.send_booking_expiration_notice(@booking)
      mail.subject.should == 'HotelQuando.com - Reserva Expirada'
      mail.body.encoded.should match("Sua reserva expirou")
    end

    it 'should send the email in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      mail = UserMailer.send_booking_expiration_notice(@booking)
      mail.subject.should == 'HotelQuando.com - Booking Expired'
      mail.body.encoded.should match("Your booking expired")
    end
  end


  describe 'send_no_show_refund_error_notice' do
    it 'should send the email in portuguese if the user locale is pt_br' do
      mail = UserMailer.send_no_show_refund_error_notice(@booking)
      mail.subject.should == "HotelQuando.com - Erro no reembolso"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Devido ao não comparecimento em")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      mail.body.should have_content(number_to_currency @booking.no_show_refund_value)
      mail.body.encoded.should match("Por favor verificar o quanto antes sua situação em relação ao reembolso.")
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end

    it 'should send the email in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      mail = UserMailer.send_no_show_refund_error_notice(@booking)
      mail.subject.should == "HotelQuando.com - Refund error"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Because of the no-show in")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%m/%d/%Y - %H:%M'))
      mail.body.should have_content(number_to_currency @booking.no_show_refund_value)
      mail.body.encoded.should match("Please verify as soon as possible your refund situation.")
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end
  end


  describe 'no_show_with_charge_error_notice' do
    it 'should send the email in portuguese if the user locale is pt_br' do
      mail = UserMailer.no_show_with_charge_error_notice(@booking)
      mail.subject.should == "HotelQuando.com - Erro ao cobrar no-show"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Devido ao não comparecimento em")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      mail.body.should have_content(number_to_currency @booking.no_show_value)
      mail.body.encoded.should match("Por favor verificar o quanto antes sua situação em relação ao pagamento.")
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end

    it 'should send the email in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      mail = UserMailer.no_show_with_charge_error_notice(@booking)
      mail.subject.should == "HotelQuando.com - Error charging your booking's no-show"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Because of the no-show in")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%m/%d/%Y - %H:%M'))
      mail.body.should have_content(number_to_currency @booking.no_show_value)
      mail.body.encoded.should match("Please verify as soon as possible your payment situation.")
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end
  end


  describe 'no_show_for_already_captured_cc_notice' do
    it 'should send the email in portuguese if the user locale is pt_br' do
      mail = UserMailer.no_show_for_already_captured_cc_notice(@booking)
      mail.subject.should == "HotelQuando.com - No-Show pago com sucesso"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Devido ao não comparecimento em")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      mail.body.should_not have_content(number_to_currency @booking.no_show_value)
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end

    it 'should send the email in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      mail = UserMailer.no_show_for_already_captured_cc_notice(@booking)
      mail.subject.should == "HotelQuando.com - No-Show successfully paid"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Because of the no-show in")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%m/%d/%Y - %H:%M'))
      mail.body.should_not have_content(number_to_currency @booking.no_show_value)
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end
  end

  describe 'no_show_with_charge_successfull_notice' do
    it 'should send the email in portuguese if the user locale is pt_br' do
      mail = UserMailer.no_show_with_charge_successfull_notice(@booking)
      mail.subject.should == "HotelQuando.com - No-Show pago com sucesso"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Devido ao não comparecimento em")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      mail.body.should have_content(number_to_currency @booking.no_show_value)
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end

    it 'should send the email in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      mail = UserMailer.no_show_with_charge_successfull_notice(@booking)
      mail.subject.should == "HotelQuando.com - No-Show successfully paid"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Because of the no-show in")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%m/%d/%Y - %H:%M'))
      mail.body.should have_content(number_to_currency @booking.no_show_value)
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end
  end

  describe 'no_show_pending_charge_notice' do
    it 'should send the email in portuguese if the user locale is pt_br' do
      mail = UserMailer.no_show_pending_charge_notice(@booking)
      mail.subject.should == "HotelQuando.com - Hotel acusou no-show"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Devido ao não comparecimento em")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      mail.body.should have_content(number_to_currency @booking.no_show_value)
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end

    it 'should send the email in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      mail = UserMailer.no_show_pending_charge_notice(@booking)
      mail.subject.should == "HotelQuando.com - Your booking's no-show will be charged"
      mail.to.should == [@booking.user.email]
      mail.body.encoded.should match("Because of the no-show in")
      mail.body.encoded.should match(@booking.offer.checkin_timestamp.strftime('%m/%d/%Y - %H:%M'))
      mail.body.should have_content(number_to_currency @booking.no_show_value)
      mail.body.encoded.should match(@booking.hotel.name)
      mail.body.encoded.should match(public_bookings_url)
    end
  end


  describe 'send_booking_cancellation_notice' do
    it 'should send the email in portuguese if the user locale is pt_br' do
      mail = UserMailer.send_booking_cancellation_notice(@booking)
      mail.subject.should == 'HotelQuando.com - Reserva Cancelada'
      mail.body.encoded.should match('Sua reserva foi cancelada')
    end

    it 'should send the email in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      mail = UserMailer.send_booking_cancellation_notice(@booking)
      mail.subject.should == 'HotelQuando.com - Booking Canceled'
      mail.body.encoded.should match('Your booking was canceled')
    end
  end

  # describe 'send_booking_creation_notice' do
  #   it 'should send the email in portuguese if the user locale is pt_br' do
  #     mail = UserMailer.send_booking_creation_notice(@booking)
  #     mail.subject.should == 'HotelQuando.com - Nova Reserva'
  #     mail.body.encoded.should match('Nova Reserva')
  #   end

  #   it 'should send the email in english if the user locale is en' do
  #     @booking.user.update(locale: 'en')
  #     mail = UserMailer.send_booking_creation_notice(@booking)
  #     mail.subject.should == 'HotelQuando.com - New Booking'
  #     mail.body.encoded.should match('New Booking')
  #   end
  # end

  describe 'send_booking_confirmation_notice', :now => true do
    it 'should have the right informations in portuguese if the user locale is pt_br' do
      email = UserMailer.send_booking_confirmation_notice(@booking)
      email.body.should have_content("#{@booking.hotel.minimum_hours_of_notice} horas antes do horário de checkin.")
      email.body.should have_content(@booking.offer.checkin_timestamp.strftime('%d/%m/%Y %H:%M'))
      email.body.should have_content(@booking.offer.checkout_timestamp.strftime('%d/%m/%Y %H:%M'))
      email.body.should have_content(number_to_currency @booking.total_price)
      email.body.should have_content(@booking.hotel.name)
      email.body.should have_content(@booking.note)
      email.body.should have_content(@booking.hotel.address)
      email.body.should have_content(@booking.room_type.full_name_pt_br)
      email.body.should have_content(@booking.guest_name)
    end

    it 'should have the right informations in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      email = UserMailer.send_booking_confirmation_notice(@booking)
      email.body.should have_content("#{@booking.hotel.minimum_hours_of_notice} hours before the checkin.")
      email.body.should have_content(@booking.offer.checkin_timestamp.strftime('%m/%d/%Y %H:%M'))
      email.body.should have_content(@booking.offer.checkout_timestamp.strftime('%m/%d/%Y %H:%M'))
      email.body.should have_content(number_to_currency @booking.total_price)
      email.body.should have_content(@booking.hotel.name)
      email.body.should have_content(@booking.note)
      email.body.should have_content(@booking.hotel.address)
      email.body.should have_content(@booking.room_type.full_name_en)
      email.body.should have_content(@booking.guest_name)
    end
  end

  describe 'send_delayed_checkout_notice', f: true do
    it 'should have the right informations in portuguese if the user locale is pt_br' do
      email = UserMailer.send_delayed_checkout_notice(@booking)
      email.body.should have_content("#{@booking.pack_in_hours} horas")
      email.body.should have_content(@booking.offer.checkin_timestamp.strftime('%d/%m/%Y - %H:%M'))
      email.body.should have_content(@booking.offer.checkout_timestamp.strftime('%d/%m/%Y - %H:%M'))
      email.body.should have_content("Código da reserva: #{@booking.id}")
      email.body.should have_content(@booking.hotel.name)
      email.body.should have_content('atraso no checkout')
    end

    it 'should have the right informations in english if the user locale is en' do
      @booking.user.update(locale: 'en')
      email = UserMailer.send_delayed_checkout_notice(@booking)
      email.body.should have_content("#{@booking.pack_in_hours} hours")
      email.body.should have_content(@booking.offer.checkin_timestamp.strftime('%m/%d/%Y - %H:%M'))
      email.body.should have_content(@booking.offer.checkout_timestamp.strftime('%m/%d/%Y - %H:%M'))
      email.body.should have_content("Booking ID: #{@booking.id}")
      email.body.should have_content(@booking.hotel.name)
      email.body.should have_content("delay in the booking's checkout")
    end
  end

  describe 'emails_to_send_booking_notices' do
    it 'returns an array with the booking guest email and the user email' do
      @booking.update(guest_email: 'email_teste@email.com')
      array_with_booking_emails = UserMailer.send(:new).send(:emails_to_send_booking_notices, @booking)
      array_with_booking_emails.length.should == 2
      @booking.guest_email.should be_in(array_with_booking_emails)
      @booking.user.email.should be_in(array_with_booking_emails)
    end

    it 'returns only one email if the user email and the guest email are the same' do
      @booking.update(guest_email: @booking.user.email)
      array_with_booking_emails = UserMailer.send(:new).send(:emails_to_send_booking_notices, @booking)
      array_with_booking_emails.length.should == 1
      @booking.user.email.should be_in(array_with_booking_emails)
    end
  end

  describe 'is_a_valid_email?' do
    before(:each) do
      @user_mailer_instance = UserMailer.send(:new)
    end

    it 'returns true if the supplied email string match a simple mail pattern chars(@)chars(.)chars' do
      valid_email_string = 'email@email.com'
      @user_mailer_instance.send(:is_a_valid_email?, valid_email_string).should == true
    end

    it 'returns false if the string doesnt match the mail pattern' do
      invalid_email_strings_array = []
      invalid_email_strings_array << 'emailemail.com'
      invalid_email_strings_array << ''
      invalid_email_strings_array << 'email@emailcom'
      invalid_email_strings_array << 'emailemailcom'
      invalid_email_strings_array << '@email.com'
      invalid_email_strings_array.each do | string |
        @user_mailer_instance.send(:is_a_valid_email?, string).should == false
      end
    end

  end

end
